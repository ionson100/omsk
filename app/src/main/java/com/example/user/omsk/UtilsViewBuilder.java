package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogShowDataVisit;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.models.DebtDuplet;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.pupper.Pupper;
import com.example.user.omsk.report.ItemView;
import com.example.user.omsk.report.Itoger;
import com.example.user.omsk.report.ItogetView;

import java.util.Date;
import java.util.List;

import static com.example.user.omsk.R.string.concurent_actions;

class UtilsViewBuilder {


    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UtilsViewBuilder.class);
    static View getItemStoryVisit(List<MOrderType> mOrderTypes,
                                  List<MVisitResults> mVisitResultses,
                                  final MVisitStoryBase storyBase,
                                  final Context context,
                                  List<MProduct> mProductList,
                                  boolean isStoryCore,
                                  boolean storyNotCore,
                                  final Activity activity, final boolean isAss) {


        View mView = LayoutInflater.from(context).inflate(R.layout.item_list_visit_story, null);
        if (storyBase.getPoint() == null) {
            return mView;
        }
        try {

            String mOrderTypeDebt = Linq.toStream(mOrderTypes).firstOrDefault(new Predicate<MOrderType>() {
                @Override
                public boolean apply(MOrderType t) {
                    return t.index == 1;
                }
            }).idu;
            double totalDebt = Utils.getDebt(storyBase.debtList);
            for (DebtDuplet debtDuplet : storyBase.getDupletProductList()) {
                totalDebt = totalDebt + debtDuplet.amount;
            }
            if (totalDebt > 0) {
                Pupper total_debt = (Pupper) mView.findViewById(R.id.total_debt);
                total_debt.setVisibility(View.VISIBLE);
                total_debt.setPairString(context.getString(R.string.itogo_refund), Utils.getStringDecimal(totalDebt));
            }
            if (isStoryCore) {

            } else {
                if (storyBase.order_type_id != null && storyBase.order_type_id.equals(mOrderTypeDebt) && storyBase.status) {
                    Pupper status = (Pupper) mView.findViewById(R.id.status_refunt);
                    status.setPairString(context.getString(R.string.asasddasd), "");
                    status.setVisibility(View.VISIBLE);
                }
            }
            ItemView.addDupletDebt(storyBase, context, mView);
            ItemView.addDebtList(storyBase, context, mView);
            ImageView button_gps = (ImageView) mView.findViewById(R.id.fix_gps);
            if (storyBase.latitude == 0 || storyBase.longitude == 0) {
                button_gps.setVisibility(View.GONE);
            } else {
                button_gps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPoint point = storyBase.getPoint();
                        if (point.id != 0) {
                            SettingsGpsMap gpsMap = SettingsGpsMap.core();
                            gpsMap.fix_latitude = storyBase.latitude;
                            gpsMap.fix_longitude = storyBase.longitude;
                            gpsMap.point_latitude = point.latitude;
                            gpsMap.point_longitude = point.longitude;
                            gpsMap.point_name = point.name;
                            gpsMap.stateSystem = Settings.getStateSystem();
                            SettingsGpsMap.save();
                            if (isAss) {
                                if(MainActivity.isStateVisitIsVisit()){
                                    Settings.showFragment(StateSystem.VISIT_MAP_ROUTE_STORY_ASS, (Activity) context);
                                }else {
                                    Settings.showFragment(StateSystem.MAP_ROUTE_STORY_ASS, (Activity) context);
                                }

                            } else {
                                Settings.showFragment(StateSystem.MAP_ROUTE_STORY, (Activity) context);
                            }

                        }
                    }
                });
            }
            ItemView.addSaleProduct(storyBase, context, mView);
            ItemView.addBalanceProduct(storyBase, context, mProductList, mView);
            ItemView.addFreeProduct(storyBase, context, mView);
            ItemView.addActionProduct(storyBase, context, mView);
            if (storyBase.getFreeProductList().size() > 0 || storyBase.getActionProductList().size() > 0) {
                Pupper member = (Pupper) mView.findViewById(R.id.member_name);
                member.setVisibility(View.VISIBLE);
                member.setPairString(context.getString(R.string.member_pupper), storyBase.member_action);
            }
            TextView namePoint = (TextView) mView.findViewById(R.id.visit_story_name_point);
            TextView nameTrader = (TextView) mView.findViewById(R.id.visit_story_trader);
            if (storyBase.trader != null) {
                nameTrader.setVisibility(View.VISIBLE);
                nameTrader.setText(storyBase.trader);
            }
            if (storyBase.concurent_actions != null && storyBase.concurent_actions.trim().length() > 0) {
                Pupper concurentAction = (Pupper) mView.findViewById(R.id.concurent_action);
                concurentAction.setVisibility(View.VISIBLE);
                concurentAction.setPairString(context.getString(concurent_actions), storyBase.concurent_actions);
            }
            Pupper description = (Pupper) mView.findViewById(R.id.visit_description);
            description.setPairString(R.string.commentary, storyBase.comment);

            Pupper startDate = (Pupper) mView.findViewById(R.id.visit_story_start_date);
            Date d_tart=Utils.intToDate(storyBase.start);
            startDate.setPairString(R.string.visit_start, Utils.simpleDateFormatE(d_tart));
            if(storyBase.start==0){
                startDate.setVisibility(View.GONE);
            }
            Pupper finisDate = (Pupper) mView.findViewById(R.id.visit_story_finish_date);
            Date d_finish=Utils.intToDate(storyBase.finish);

            if(storyBase.start==0){
                finisDate.setPairString("Время визита:", Utils.simpleDateFormatE(d_finish));
            }else {
                finisDate.setPairString(R.string.finish_visit, Utils.simpleDateFormatE(d_finish));
            }


            Pupper amountProduct = (Pupper) mView.findViewById(R.id.visit_story_amount_poduct);

            Pupper itogo = (Pupper) mView.findViewById(R.id.visit_story_itogo);

            MVisitResults types = Linq.toStream(mVisitResultses).firstOrDefault(new Predicate<MVisitResults>() {
                @Override
                public boolean apply(MVisitResults t) {
                    return t.idu.equals(storyBase.visit_result_id);
                }
            });

            Pupper visitResult = (Pupper) mView.findViewById(R.id.visit_story_visit_result);

            visitResult.setPairString(R.string.visit_result_titl, types.name);

            Pupper visitOrder = (Pupper) mView.findViewById(R.id.visit_story_visit_order);
            Pupper manyArm = (Pupper) mView.findViewById(R.id.visit_story_visit_price_hadler);
            manyArm.setVisibility(View.GONE);

            Pupper debitState = (Pupper) mView.findViewById(R.id.debt1);

            debitState.setPairString(R.string.debt_text, Utils.getStringDecimal(storyBase.getPoint().debt));

            Pupper debitReturn = (Pupper) mView.findViewById(R.id.debt2);

            if (storyBase.getPoint().debt == 0) {
                debitState.setVisibility(View.GONE);
            }

            double resDebt = Utils.getDebt(storyBase.debtList);

            debitReturn.setPairString(R.string.return_debt, Utils.getStringDecimal(resDebt));
            if (resDebt == 0) {
                debitReturn.setVisibility(View.GONE);
            }

            MPoint point = storyBase.getPoint();

            namePoint.setText(point.name);

            double count = 0;
            double itogoP = 0;
            for (MProduct mSaleRowsStory : storyBase.getSalesProductList()) {
                count = count + mSaleRowsStory.getAmount_core();
                itogoP = itogoP + (mSaleRowsStory.getAmount_core() * mSaleRowsStory.price);
            }

            if (types.index != 0) {
                amountProduct.setPairString(R.string.itogo_count_products1, Utils.getStringDecimal(count));

                String string = Linq.toStream(mOrderTypes).firstOrDefault(new Predicate<MOrderType>() {
                    @Override
                    public boolean apply(MOrderType t) {
                        return t.index == 0;
                    }
                }).idu;

                if (storyBase.order_type_id != null && storyBase.order_type_id.equals(string)) {
                    itogo.setPairString(R.string.itogo_price3, Utils.getStringDecimal(itogoP));
                } else {
                    itogo.setPairString(R.string.itogo_price43, Utils.getStringDecimal(itogoP));
                }

                MOrderType type = Linq.toStream(mOrderTypes).firstOrDefault(new Predicate<MOrderType>() {
                    @Override
                    public boolean apply(MOrderType t) {
                        return t.idu.equals(storyBase.order_type_id);
                    }
                });

                if (type == null) {
                    visitOrder.setPairString(R.string.visit_order_type, "не найден");

                } else {
                    visitOrder.setPairString(R.string.visit_order_type, type.name);
                }

            } else {
                visitOrder.setVisibility(View.GONE);
                manyArm.setVisibility(View.GONE);
                amountProduct.setVisibility(View.GONE);
                itogo.setVisibility(View.GONE);
            }
            /////////////////////////// пробитые чекиж
            Pupper check_current = (Pupper) mView.findViewById(R.id.check_current);
            Pupper check_debt = (Pupper) mView.findViewById(R.id.check_debt);
            StringBuilder stringBuilder = new StringBuilder("");
            for (MChecksData checksData : storyBase.getCheckList()) {
                if (checksData.type_check == MChecksData.TYPE_CHECK_CACH && !checksData.isReturn) {
                    check_current.setPairString(context.getString(R.string.asasss), Utils.getStringDecimal(checksData.totalPrice) + " руб.");
                    check_current.setVisibility(View.VISIBLE);
                }
                if (checksData.type_check == MChecksData.TYPE_CHECK_DEBT && !checksData.isReturn) {
                    Date d=Utils.intToDate(checksData.debt_data);
                    stringBuilder.
                            append("Долг за ").
                            append(Utils.simpleDateFormatForRouteE(d)).
                            append(" ").
                            append(Utils.getStringDecimal(checksData.totalPrice)).
                            append(" руб.\n");
                }
            }
            if (stringBuilder.length() > 0) {
                check_debt.setVisibility(View.VISIBLE);
                check_debt.setPairString(context.getString(R.string.asasf), stringBuilder.toString());
                check_debt.getTitul().setGravity(Gravity.TOP);
                check_debt.getValue().setGravity(Gravity.LEFT);
            }

            List<MVisitPresent> list=storyBase.getPresnt();
            if(list.size()>0){
               View  vv= mView.findViewById(R.id.panel_present);
                vv.setVisibility(View.VISIBLE);
                LinearLayout layoutPresent= (LinearLayout) mView.findViewById(R.id.panel_present_core);
                for (MVisitPresent present : list) {
                    View view = LayoutInflater.from(context).inflate(R.layout.item_present_story, null);
                    TextView name = (TextView) view.findViewById(R.id.name_present);
                    TextView amount = (TextView) view.findViewById(R.id.amount_present);
                    name.setText(present.getName());
                    amount.setText(String.valueOf(present.amount));
                    layoutPresent.addView(view);
                }
            }

        } catch (Exception ex) {
            log.error(ex);
            mView = LayoutInflater.from(context).inflate(R.layout.item_error, null);
            StackTraceElement[] stackTraceElements = ex.getStackTrace();
            StringBuilder builder = new StringBuilder();
            builder.append("Exception: ").append(ex.getClass().getName()).append("\n")
                    .append("Message: ").append(ex.getMessage()).append("\nStacktrace:\n");
            for (StackTraceElement element : stackTraceElements) {
                builder.append("\t").append(element.toString()).append("\n");
            }
            TextView messageError = (TextView) mView.findViewById(R.id.error_message);
            messageError.setText(builder.toString());

            TextView titul = (TextView) mView.findViewById(R.id.titul_error);
            titul.setText("Ошибка обработки: " + storyBase.getPoint().name);
        }
        mView.setTag(storyBase.getPoint());
        return mView;

    }


    static View getItemTotal(final Context context, List<MVisitStoryBase> mVisitStoryList, Itogoficator itogoficator, boolean isHistoryCore) {

        Itoger itoger = new Itoger(mVisitStoryList);

        View view = LayoutInflater.from(context).inflate(R.layout.item_itogo_visit, null);

        ItogetView.addFreeProduct(context, itoger, view);

        ItogetView.addActionsProduct(context, itoger, view);

        ItogetView.addSaleProduct(context, itoger, view);

        ItogetView.addDebetProduct(context, itoger, view, isHistoryCore);

        ItogetView.addPresent(context, mVisitStoryList, view);

        ItogetView.addReturmRefunds(context, itoger, view);

        ItogetView.addCompyteTotalPrice(context, itoger, view);

        Pupper pointsT = (Pupper) view.findViewById(R.id.itoger_amount_points);
        int count = itoger.getPointCount();
        pointsT.setPairString(R.string.itogo_pointers, "" + count);

        return view;
    }

}

class UtilsViewBuilderNew {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UtilsViewBuilderNew.class);
    final static boolean[] run = {false};

    static View getItemStoryVisit(final MVisitStoryBase storyBase, final Context context, final List<MProduct> mProductList, final boolean isStoryCore, final boolean storyNotCore, final Activity activity) {

        View mView = LayoutInflater.from(context).inflate(R.layout.item_list_visit_story_new, null);

        try {
            double resDebt = Utils.getDebt(storyBase.debtList);
            MPoint point = storyBase.getPoint();
            Pupper start = (Pupper) mView.findViewById(R.id.start_pupper);
            Pupper finish = (Pupper) mView.findViewById(R.id.finish_pupper);
            Date d_start=Utils.intToDate(storyBase.start);
            start.setPairString(R.string.visit_start, Utils.simpleDateFormatE(d_start));
            if(storyBase.start==0){
                start.setVisibility(View.GONE);
            }
            Date d_finish=Utils.intToDate(storyBase.finish);
            if(storyBase.start==0){
                finish.setPairString("Время визита:", Utils.simpleDateFormatE(d_finish));
            }else {
                finish.setPairString(R.string.finish_visit, Utils.simpleDateFormatE(d_finish));
            }

            TextView namePoint = (TextView) mView.findViewById(R.id.point_name);
            namePoint.setText(point.name);
            final View finalMView = mView;
            mView.findViewById(R.id.image_view).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (run[0]) return;
                    run[0] = true;
                    finalMView.setBackgroundResource(R.color.colore97);
                    DialogShowDataVisit dialogShowDataVisit = new DialogShowDataVisit();
                    View mView = LayoutInflater.from(context).inflate(R.layout.item_list_visit_story_for_dialog, null);
                    try {
                        DialogShowDataVisit.getItemStoryVisit(mView, storyBase, context, mProductList, isStoryCore, storyNotCore, activity);
                    } catch (Exception ex) {
                        log.error(ex);
                        mView = LayoutInflater.from(context).inflate(R.layout.item_error, null);
                        StackTraceElement[] stackTraceElements = ex.getStackTrace();
                        StringBuilder builder = new StringBuilder();
                        builder.append("Exception: ").append(ex.getClass().getName()).append("\n")
                                .append("Message: ").append(ex.getMessage()).append("\nStacktrace:\n");
                        for (StackTraceElement element : stackTraceElements) {
                            builder.append("\t").append(element.toString()).append("\n");
                        }
                        TextView messageError = (TextView) mView.findViewById(R.id.error_message);
                        messageError.setText(builder.toString());

                        TextView titul = (TextView) mView.findViewById(R.id.titul_error);
                        titul.setText("Ошибка обработки: " + storyBase.getPoint().name);
                    }
                    dialogShowDataVisit.activateView(mView);
                    dialogShowDataVisit.setIActionsE(new IActionE() {
                        @Override
                        public void action(Object o) {
                            finalMView.setBackgroundColor(Color.TRANSPARENT);
                            run[0] = false;
                        }
                    });
                    dialogShowDataVisit.show(((AppCompatActivity) activity).getSupportFragmentManager(), "sdsodi");
                }
            });
        } catch (Exception ex) {
            log.error(ex);
            run[0] = false;
            mView = LayoutInflater.from(context).inflate(R.layout.item_error, null);
            StackTraceElement[] stackTraceElements = ex.getStackTrace();
            StringBuilder builder = new StringBuilder();
            builder.append("Exception: ").append(ex.getClass().getName()).append("\n")
                    .append("Message: ").append(ex.getMessage()).append("\nStacktrace:\n");
            for (StackTraceElement element : stackTraceElements) {
                builder.append("\t").append(element.toString()).append("\n");
            }
            TextView messageError = (TextView) mView.findViewById(R.id.error_message);
            messageError.setText(builder.toString());

            TextView titul = (TextView) mView.findViewById(R.id.titul_error);
            titul.setText("Ошибка обработки: " + storyBase.getPoint().name);
        }
        mView.setTag(storyBase.getPoint());
        return mView;
    }
}

