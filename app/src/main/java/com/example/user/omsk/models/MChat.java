package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

// хранилище сообщений чата
@Table("chat")
public class MChat implements Serializable, IUsingGuidId {

    public transient static String BRODCAST_MESSAGE = "sdjj";

    public transient static String BRODCAST_ACTION = "dsfhhjjf";

    @PrimaryKey("id")
    public transient int id;

    @SerializedName("id")
    @Column("idu")
    public String idu;

    @SerializedName("date")
    @Column("date")
    public Date date = null;

    @SerializedName("user_name")
    @Column("user_name")
    public String user_name;

    @SerializedName("user_function")
    @Column("user_function")
    public String user_function;

    @SerializedName("text")
    @Column("text")
    public String text;

    @SerializedName("common")
    @Column("common")
    public boolean common;

    @SerializedName("owner")
    @Column("owner")
    public boolean owner;

    public String get_id() {
        return idu;
    }
}
