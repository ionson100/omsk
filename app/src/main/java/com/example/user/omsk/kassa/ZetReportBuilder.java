package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.atol.drivers.fptr.Fptr;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MSummaKKM;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.List;

public class ZetReportBuilder {

    private Fptr mFptr;

    public ZetReportBuilder(Fptr fptr) {

        this.mFptr = fptr;
    }

    public ZetReport build() throws Exception {
        ZetReport mzReport = new ZetReport();
        mzReport.date = Utils.curDate();
        mzReport.uuid = SettingsUser.core().getUuidDevice();
        mzReport.id = Utils.getUuid();
///////////////////////////////////////////////////////////////////////////////////Сумма наличности в ККМ (Summ)
        if (mFptr.put_RegisterNumber(10) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }

        mzReport.cash = mFptr.get_Summ();
////////////////////////////////////////////////////////////////////////////////////Выручка (Summ)
        if (mFptr.put_RegisterNumber(11) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }
        mzReport.gain = mFptr.get_Summ();
//////////////////////////////////////////////////////////////////////////////////  сумма сменного итога приход
        if (mFptr.put_RegisterNumber(12) < 0) {
            checkError();
        }
        if (mFptr.put_OperationType(0) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }
        mzReport.receipt = mFptr.get_Summ();
/////////////////////////////////////////////////////////////////////////////////////сумма сменного итога расход
        if (mFptr.put_RegisterNumber(12) < 0) {
            checkError();
        }
        if (mFptr.put_OperationType(2) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }
        mzReport.return_receipt = mFptr.get_Summ();
////////////////////////////////////////////////////////////////////////////////////Номер смены (Session)21
        if (mFptr.put_RegisterNumber(53) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }


        mzReport.session = mFptr.get_Session();
        //////////////////////////////////////////
        if (mFptr.put_RegisterNumber(52) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }
        mzReport.doc_number = mFptr.get_DocNumber();
        //////////////////////////////////// не отправленные в офд
        if (mFptr.put_RegisterNumber(44) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }
        mzReport.notSendToOfd = mFptr.get_Count();
        ////////////////////////////////////////////// дата первого не отправленного
        if (mzReport.notSendToOfd > 0) {
            if (mFptr.put_RegisterNumber(45) < 0) {
                checkError();
            }
            if (mFptr.GetRegister() < 0) {
                checkError();
            }
            mzReport.firstDateOfd = mFptr.get_Date();
        }


////////////////////////////////////////////////////////////////////////////////////////Необнуляемая сумма после последней перерегистрации + сменный итог текущей смены (Summ)
        if (mFptr.put_RegisterNumber(14) < 0) {
            checkError();
        }
        if (mFptr.put_OperationType(0) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }
        mzReport.not_null_summ = mFptr.get_Summ();
//////////////////////////////////////////////////////////////////////////////////////////номер кассового аппарата
        if (mFptr.put_RegisterNumber(22) < 0) {
            checkError();
        }
        if (mFptr.GetRegister() < 0) {
            checkError();
        }
        mzReport.number_kkm = mFptr.get_SerialNumber();
/////////////////////////////////////////////////////////
        return mzReport;

    }

    private void checkError() throws Exception {
        int rc = mFptr.get_ResultCode();
        if (rc < 0) {
            String rd = mFptr.get_ResultDescription(), bpd = null;
            if (rc == -6) {
                bpd = mFptr.get_BadParamDescription();
            }
            if (bpd != null) {
                throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
            } else {
                throw new Exception(String.format("[%d] %s", rc, rd));
            }
        }
    }

    public static void clearReport() {
        ISession session = Configure.getSession();
        List<MVisitStory> visitStoryList =  session.getList(MVisitStory.class, null);
        for (MVisitStory visitStory : visitStoryList) {
            for (MChecksData checksData : visitStory.getCheckList()) {
                checksData.is_zreport = true;

            }
            session.update(visitStory);

        }
        List<MChecks> mChecksList = session.getList(MChecks.class, null);
        {
            for (MChecks mChecks : mChecksList) {
                mChecks.checkData.is_zreport = true;
                session.update(mChecks);
            }
        }
        Settings mSettings = Settings.core();
        if (mSettings.getVisit() != null && mSettings.getVisit().point != null) {
            for (MChecksData checksData : mSettings.getVisit().checksDataList) {
                checksData.is_zreport = true;
            }
            if (mSettings.getVisit().checksDataList.size() > 0) {
                Settings.save();
            }
        }
        checkSummKKM();

    }

    public static void checkSummKKM(){
        List<MSummaKKM> kkms=Configure.getSession().getList(MSummaKKM.class,null);
        if(kkms==null) return;
        if(kkms.size()>0){
            MSummaKKM kkm=kkms.get(0);
            kkm.sumkkm=0;
            Configure.getSession().update(kkm);
        }else {
            MSummaKKM kkm=new MSummaKKM();
            kkm.sumkkm=0d;
            Configure.getSession().insert(kkm);
        }
    }
}
