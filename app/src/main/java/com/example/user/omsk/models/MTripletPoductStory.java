package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.Where;

import java.io.Serializable;

// история продуктов проданых из стационарного ларька элемент объекта истории продажи из стац ларька.
@Where("ping = " + PINGATOR.sale_item_product_story)
@Table("sale_item_product")//sale_item_product_story
public class MTripletPoductStory extends MTripletPoductBase implements Serializable, IPing {
    @Column("ping")
    public int ping = PINGATOR.sale_item_product_story;
}

