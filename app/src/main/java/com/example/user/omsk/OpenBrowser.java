package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class OpenBrowser {
    public static void open(Activity activity, Settings mSettings) {
        String url = Utils.HTTP + mSettings.url + "/client/tracks/?guid=" + SettingsUser.core().getUuidDevice();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent);
    }

    public static void openYtube(Activity activity) {
        String url = "http://www.youtube.com/channel/UCJG2NFXlFR9pwgzIQNWunZA";
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent);
    }
}
