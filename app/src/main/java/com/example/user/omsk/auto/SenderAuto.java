package com.example.user.omsk.auto;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.senders.IActionResponse;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.senders.UtilsSender;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

//import com.example.user.omsk.models.MError;

/**
 * ion100 on 23.10.2017.
 */

public class SenderAuto {

    private static final org.apache.log4j.Logger log = Logger.getLogger(SenderAuto.class);
    private MAuto mAuto;
    private MyApplication activity;
    private Settings settings;
    private boolean isOpen;

    public void send(MAuto mAuto, MyApplication activity, Settings settings) {

        this.mAuto = mAuto;
        this.activity = activity;
        this.settings = settings;
        try {
            new SenderWorkerTack().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                Toast.makeText(activity, result, Toast.LENGTH_LONG).show();

                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + settings.url + "/client_way/:    " + result;
                log.error(msg);
               // Configure.getSession().insert(new MError(msg));
                new SenderErrorPOST().send(activity, msg);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            return UtilsSender.postRequest(activity, Utils.HTTP + settings.url + "/client_way/", getJson(), new IActionResponse() {
                @Override
                public void invoke(String str, int status) {

                    mAuto.isSender = false;
                    Configure.getSession().update(mAuto);
                }
            });
        }
    }

    private String getJson() {
        Gson gson = Utils.getGson();
        String res = "";
        if (mAuto.date1==null && mAuto.date2==null) {//open
            isOpen = true;
            TempOpen open = new TempOpen();
            open.uuid = SettingsUser.core().getUuidDevice();
            open.open = 1;
            open.auto = mAuto.number;
            open.id = mAuto.uid;
            open.start = mAuto.date1;
            open.km_start = mAuto.km1;
            open.address1 = mAuto.address1;
            res = gson.toJson(open);
        }
        if (mAuto.date1==null && mAuto.date2==null) {//close
            isOpen = false;
            TempClose close = new TempClose();
            close.uuid = SettingsUser.core().getUuidDevice();
            close.open = 0;
            close.id = mAuto.uid;
            close.finish = mAuto.date2;
            close.km_finish = mAuto.km2;
            close.address2 = mAuto.address2;
            List<MVisitStory> stories = Configure.getSession().getList(MVisitStory.class, null);
            if (stories.size() > 0) {
                int i = (stories.size() / 2) - 1;
                if (i < 0) {
                    i = 0;
                }
                close.address1 = stories.get(i).getPoint().address;
            } else {
                close.address1 = close.address2;
            }
            res = gson.toJson(close);


        }
        return res;
    }

    static    class TempOpen implements Serializable {
        public String uuid;
        public int open;
        public String auto;
        public String id;
        public Date start = null;
        public int km_start;
        public String address1;

    }

    static    class TempClose implements Serializable {
        public String uuid;
        public int open;
        public String id;
        public Date finish = null;
        public int km_finish;
        public String address1;
        public String address2;

    }
}
