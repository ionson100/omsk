package com.example.user.omsk;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public enum Itogoficator {
    currentDay,// показ пройденых визитов за день
    storyPoint,// история по точку
    storyDey   // история за выбраный день, 1 или 3 месяца
}
