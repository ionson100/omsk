package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;

// запуск фрагмента выбора точки для посещения
public class StartSearchPointFragment {
    public static void start(Settings mSettings, Activity activity) {
        if (ValidateDay.validate(mSettings, activity)) {
            // mSettings.setVisit(new MVisit());
            Settings.showFragment(StateSystem.SEARCH_POINT, activity);
        }
    }
}
