package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

//список задач
@Table("task")
public class MTask implements IUsingGuidId {

    @PrimaryKey("id")
    public int id;

    @Column("idu")
    public String idu;

    @Column("message")
    public String message;

    public String get_id() {
        return idu;
    }
}
