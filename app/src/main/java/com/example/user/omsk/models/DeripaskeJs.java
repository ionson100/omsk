package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeripaskeJs implements Serializable {
    public double amount;
    public double amount_max;
    public List<String> products = new ArrayList<>();
    public Map<String, Double> map = new HashMap<>();
}
