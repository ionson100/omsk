package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.os.AsyncTask;

import com.example.user.omsk.BitmapCache;
import com.example.user.omsk.Certificate;
import com.example.user.omsk.models.MPromotion;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;


public class SenderDownloadPOST {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderDownloadPOST.class);
    private String filename;
    private Settings settings;
    private MPromotion mPromotion;
    private Activity activity;

    public void send(MPromotion mPromotion,  Activity activity) {
        this.mPromotion = mPromotion;
        this.activity = activity;

        this.filename = mPromotion.file_name;
        this.settings = Settings.core();
        new SenderWorkerTack().execute();
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            File f = new File(Utils.getPromotionDirectory() + "/" + mPromotion.file_name);
            if (f.exists()) {
                BitmapCache.cache(mPromotion);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            int count;
            try {
                URL url = new URL(Utils.HTTP + settings.url + "/static/promo/" + URLEncoder.encode(filename, "UTF-8"));
                HttpsURLConnection conection = (HttpsURLConnection) url.openConnection();
                HttpsURLConnection.setDefaultHostnameVerifier(new UtilsSender.NullHostNameVerifier());
                conection.setSSLSocketFactory(UtilsSender.getSslSocketFactory(activity));
                //////////////////////////////////////////////////////////////////addin
                conection.setInstanceFollowRedirects(false);
                String cert = Certificate.getCertificateSHA1Fingerprint(activity);
                conection.setRequestProperty("Content-user", cert);
                conection.setReadTimeout(15000 /*milliseconds*/);
                conection.setConnectTimeout(20000 /* milliseconds */);
                conection.setRequestMethod("GET");
                conection.connect();
                int lenghtOfFile = conection.getContentLength() + 100;

                InputStream input = null;
                OutputStream output = null;
                try {
                    input = conection.getInputStream();
                    output = new FileOutputStream(Utils.getPromotionDirectory() + "/" + filename);
                    byte data[] = new byte[lenghtOfFile];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                }finally {
                    if(input!=null){
                        input.close();
                    }
                    if(output!=null){
                        output.close();
                    }
                }




            } catch (Exception e) {
                log.error("\n download file:" + filename + " " + e.getMessage());
            }

            return null;
        }
    }
}

