package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;

public class MBasePlanPointProducts {
    @Column("product_id")
    public String product_id;
    @Column("amount")
    public double amount;
    @Column("point_id")
    public String point_id;
    @PrimaryKey("id")
    int id;
}
