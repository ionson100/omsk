package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.LoggerE;
import com.example.user.omsk.MainActivity;
//import com.example.user.omsk.models.MError;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsChat;
import com.example.user.omsk.SettingsGpsMap;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.SettingsStart;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.UrlList;
import com.example.user.omsk.Utils;
import com.example.user.omsk.action.SettingsStorageAction;
import com.example.user.omsk.addin.SettingsNewOrder;
import com.example.user.omsk.refill.SettingsAddinStock;
import com.example.user.omsk.route.SettingsRoute;
import com.example.user.omsk.setting.IListItem;
import com.example.user.omsk.setting.Item;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class UtilsSender {

    public static org.apache.log4j.Logger log = Logger.getLogger(UtilsSender.class);

    /**
     * @param urlStr     строка запроса
     * @param iAction200 действие при коле 200
     * @return результат действия
     */

    public static String postRequest(final Context application, String urlStr, String json, IActionResponse iAction200) {

        String result = null;
        String cert = com.example.user.omsk.Certificate.getCertificateSHA1Fingerprint(application);
        if (json == null) {
           throw  new RuntimeException(" params joson is null");
        }

        String ssres = "";
        HttpsURLConnection conn = null;
        try {
            final URL url = new URL(urlStr);
            LoggerE.logI("POST ++++ URL:" + urlStr + " BODY:" + json);

            conn = (HttpsURLConnection) url.openConnection();
            ////////////////////////////////////////////////////////////////// addin
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            conn.setSSLSocketFactory(getSslSocketFactory(application));
            //////////////////////////////////////////////////////////////////addin
            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-user", cert);
            conn.setReadTimeout(15000 /*milliseconds*/);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(json);
            wr.flush();
            wr.close();
            conn.connect();
            final int status = conn.getResponseCode();
            int ch;
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                while ((ch = reader.read()) != -1) {
                    sb.append((char) ch);
                }
                reader.close();
            } catch (IOException ignored) {
            }
            ssres = sb.toString();
            if (status == 302) {
                result = application.getString(R.string.pioidasf);
            } else if (status == 201) {
                if (urlStr.indexOf("message_client") == -1) {
                    result = application.getString(R.string.asdd);
                    SettingsUser.core().setAutorise(false);
                    log.error("201 - " + urlStr);
                    //Configure.getSession().insert(new MError());
                }

            } else if (status == 200) {
                LoggerE.logI(" JSON ++++ " + ssres);
                iAction200.invoke(ssres, status);
            } else {

                if (status == 205) {
                    final String finalSsres = ssres;
                    try {
                        MainActivity.mainActivityInstance.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                log.error(url.toString()+" 205 "+finalSsres);
                                Utils.messageBox(application.getString(R.string.assasss), finalSsres, MainActivity.mainActivityInstance, null);

                            }
                        });
                    } catch (Exception ignored) {

                    }

                }
                result = status + " - " + ssres + " - " + urlStr;
            }


        } catch (Exception ex) {
            log.error(ex);
            result = ex.getMessage();

        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return result;
    }

    /**
     * @param urlStr     строка запроса
     * @param iAction200 действие при коле 200
     * @return результат действия
     */
    @NonNull
    public static String getRequest(final MyApplication application, final String urlStr, IActionResponse iAction200) {
        String result = null;
        String cert = com.example.user.omsk.Certificate.getCertificateSHA1Fingerprint(application);
        OutputStream os = null;
        int status = 0;
        InputStream is = null;
        HttpsURLConnection conn = null;
        String res = "";
        try {
            LoggerE.logI("GET +++++ " + urlStr);
            final URL url = new URL(urlStr);
            conn = (HttpsURLConnection) url.openConnection();
            ////////////////////////////////////////////////////////////////// addin
            HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
            conn.setSSLSocketFactory(getSslSocketFactory(application));
            //////////////////////////////////////////////////////////////////addin

            conn.setInstanceFollowRedirects(false);
            conn.setRequestProperty("Content-user", cert);
            conn.setReadTimeout(15000 /*milliseconds*/);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.connect();
            status = conn.getResponseCode();
            is = conn.getInputStream();
            int ch;
            StringBuilder sb = new StringBuilder();
            while ((ch = is.read()) != -1) {
                sb.append((char) ch);
            }
            res = sb.toString().trim();
            if (status == 302) {
                result = "302 - " + application.getString(R.string.rtrettrt);
            } else if (status == 201) {
                result = application.getString(R.string.asdd);
                SettingsUser.core().setAutorise(false);
                log.error("201 - " + urlStr);
                //Configure.getSession().insert(new MError());
            } else if (status == 200) {
                iAction200.invoke(res, status);
            } else {

                if (status == 205) {
                    final String finalRes = res;
                    try {
                        MainActivity.mainActivityInstance.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                log.error(url.toString()+" 205 "+finalRes);
                                Utils.messageBox(application.getString(R.string.assasss), finalRes, MainActivity.mainActivityInstance, null);
                            }
                        });
                    } catch (Exception ignored) {

                    }
                }
                result = status + " - " + res + " - " + urlStr;
            }
        } catch (Exception ex) {
            log.error(ex);
            result = ex.getMessage();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public static void sendJournalError(Settings mSettings, MyApplication application, String strings) {



       new SenderErrorPOST().send(application,strings);
    }

    // подмена не доверенного сертификата
    public static SSLSocketFactory getSslSocketFactory(Context application) throws CertificateException, IOException,
            KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = new BufferedInputStream(application.getAssets().open("littlesvr2.crt"));
        Certificate ca = cf.generateCertificate(caInput);
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, tmf.getTrustManagers(), null);
        return context.getSocketFactory();
    }

    public static void sendSettings(Settings mSettings, MyApplication application) {

        List<Object> list = new ArrayList<>();
        list.add(mSettings);
        list.add(SettingsUser.core());
        list.add(SettingsRoute.core());
        list.add(SettingsAddinStock.core());
        list.add(SettingsChat.core());
        list.add(SettingsGpsMap.core());
        list.add(SettingsKassa.core());
        list.add(SettingsNewOrder.core());
        list.add(SettingsStart.core());
        list.add(SettingsStorageAction.core());
        Gson s=Utils.getGson();
        String str=s.toJson(list);
        new SenderErrorPOST().send(application,str);
    }

    // для верификации носта ssl с которым держим связь
    public static class NullHostNameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            IListItem f = new UrlList();
            for (Item item : f.getList()) {
                if (item.value.toString().trim().equals(hostname)) {
                    return true;
                }
            }
            return false;
        }
    }
}

