package com.example.user.omsk.refill;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.FactoryProduct;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.ListAdapterStock;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MProductGroup;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderOrderPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Action;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

// оформление заявок на борт
public class RefillStock extends AppCompatActivity implements ListAdapterStock.OnRowDataClick, ListAdapterStock.OnRowNavigateClick {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RefillStock.class);
    private ListAdapterAddinStock adapterLock;

    private SettingsAddinStock mLackSettings;
    private List<MProduct> mProductList;
    private List<MProductGroup> mProductGroups;
    private ListView lackListView;
    private ListView productListView;
    private UUID delete, update, order;
    private Button btCreateLack;
    private TextView mTextStatus;
    private Button btSendToServer;
    private ImageButton btMoveLeft;
    private Settings mSettings;
    private boolean showStock;
    private ListAdapterStock listAdapterStock;

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo aMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        final int position = aMenuInfo.position;
        menu.add(getString(R.string.remove_product)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                for (MProduct mProduct : mLackSettings.mLastListNew) {
                    addToListProduct(mProduct);
                }

                mLackSettings.mLastListNew.clear();
                Collections.sort(mProductList, new Comparator<MProduct>() {
                    @Override
                    public int compare(MProduct lhs, MProduct rhs) {
                        return lhs.name.compareTo(rhs.name);
                    }
                });
                activateLack();
                showAll();
                SettingsAddinStock.save();

                return true;
            }
        });
        menu.add(getString(R.string.remove_product_one)).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MProduct mProduct = mLackSettings.mLastListNew.get(position);
                mLackSettings.mLastListNew.remove(mProduct);
                addToListProduct(mProduct);
                Collections.sort(mProductList, new Comparator<MProduct>() {
                    @Override
                    public int compare(MProduct lhs, MProduct rhs) {
                        return lhs.name.compareTo(rhs.name);
                    }
                });
                activateLack();
                showAll();
                SettingsAddinStock.save();
                return true;
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lack);
        try {
            mSettings = Settings.core();
            init();

            String s1 = Utils.simpleDateFormatForCommentsE(Utils.curDate());
            String s2 = Utils.simpleDateFormatForCommentsE(mLackSettings.mLastkDateNew_22);

            if (!s1.equals(s2)) {
                btCreateLack.setVisibility(View.VISIBLE);
                mLackSettings.mLastListNew.clear();
                mLackSettings.mLastkDateNew_22 = null;
                SettingsAddinStock.save();
                mTextStatus.setText(R.string.waiting_lack);
                btSendToServer.setVisibility(View.GONE);
                btMoveLeft.setVisibility(View.GONE);

            } else {
                for (final MProduct mProduct : mLackSettings.mLastListNew) {

                    MProduct product = Linq.toStream(mProductList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                        @Override
                        public boolean apply(MProduct t) {
                            return t.idu.equals(mProduct.idu);
                        }
                    });

                    if (product != null) {
                        mProductList.remove(product);
                    }
                }
                btCreateLack.setVisibility(View.GONE);
                mTextStatus.setText(getString(R.string.lack_status) + Utils.simpleDateFormatForCommentsE(mLackSettings.mLastkDateNew_22));
                if (!mLackSettings.mStatusSender) {
                    btSendToServer.setVisibility(View.VISIBLE);
                    btMoveLeft.setVisibility(View.VISIBLE);
                } else {
                    btSendToServer.setVisibility(View.GONE);
                    btMoveLeft.setVisibility(View.GONE);

                }
            }
            findViewById(R.id.bt_lack_show_stocl).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RefillStock.this.showStock = true;
                    showStock();
                }
            });
            activateLack();
            showAll();
            activateABC();
        } catch (Exception e) {
            log.error(e);
            Utils.sendMessage("RefillStock:" + Utils.getErrorTrace(e),  (MyApplication) this.getApplication());
            this.finish();
        }
    }

    private void showStock() {

        List<MProduct> list = Linq.toStream(mProductList).where(new Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.amountSelf > 0 || t.amountSelf2 > 0;
            }
        }).toList();
        activateListProduct(new ArrayList<Object>(list));
    }

    private void init() {
        final Activity activity = this;
        mTextStatus = (TextView) findViewById(R.id.text_status);
        mLackSettings = SettingsAddinStock.core();
        mProductList = Configure.getSession().getList(MProduct.class, null);
        mProductGroups = Configure.getSession().getList(MProductGroup.class, null);
        TextView textBalance = (TextView) findViewById(R.id.text_balance_stock);
        double totalSelf = 0d;
        for (MProduct product : mProductList) {
            totalSelf = totalSelf + product.amountSelfCore();
        }

        textBalance.setText(getString(R.string.compute_self_stock) + " " + Utils.getStringDecimal(totalSelf));
        Collections.sort(mProductGroups, new Comparator<MProductGroup>() {
            @Override
            public int compare(MProductGroup lhs, MProductGroup rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        Utils.pricecometr(mProductList, mSettings);
        List<MProduct> products = new ArrayList<>();
        for (MProduct product : mProductList) {
            if (product.price == 0 || product.isArchive) {
                products.add(product);
            }
        }
        mProductList.removeAll(products);

        Collections.sort(mProductList, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });


        findViewById(R.id.bt_lack_show_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RefillStock.this.showStock = false;
                showAll();
            }
        });
        btSendToServer = (Button) findViewById(R.id.bt_lack_to_server);
        btSendToServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLackSettings.mLastListNew.size() == 0) {
                    Toast.makeText(activity, "Поле заявки не заполнено", Toast.LENGTH_SHORT).show();
                } else {

                    MProduct product = Linq.toStream(mLackSettings.mLastListNew).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                        @Override
                        public boolean apply(MProduct t) {
                            return t.getAmount_core() == 0;
                        }
                    });
                    if (product != null) {
                        Toast.makeText(activity, "Продукт:" + product.name + " не заполнен", Toast.LENGTH_SHORT).show();
                    } else {
                        new SenderOrderPOST().send(mLackSettings, mSettings.url, activity, btMoveLeft, btSendToServer);
                    }
                }
            }
        });
        findViewById(R.id.bt_close_lack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btCreateLack = (Button) findViewById(R.id.bt_lack_date);
        btCreateLack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLackSettings.mLastListNew.clear();
                mLackSettings.mLastkDateNew_22 = Utils.curDate();
                mLackSettings.mStatusSender = false;
                mProductList = Configure.getSession().getList(MProduct.class, null);
                Utils.pricecometr(mProductList, mSettings);
                List<MProduct> products = new ArrayList<>();
                for (MProduct product : mProductList) {
                    if (product.price == 0) {
                        products.add(product);
                    }
                }
                mProductList.removeAll(products);
                Collections.sort(mProductList, new Comparator<MProduct>() {
                    @Override
                    public int compare(MProduct lhs, MProduct rhs) {
                        return lhs.name.compareTo(rhs.name);
                    }
                });
                activateLack();
                showAll();
                mTextStatus.setText(getText(R.string.lack_status) + Utils.simpleDateFormatForCommentsE(mLackSettings.mLastkDateNew_22));
                btCreateLack.setVisibility(View.GONE);
                btSendToServer.setVisibility(View.VISIBLE);
                btMoveLeft.setVisibility(View.VISIBLE);
            }
        });

        btMoveLeft = (ImageButton) findViewById(R.id.bt_lack_left);
        btMoveLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<MProduct> mProducts = Linq.toStream(mProductList).where(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                    @Override
                    public boolean apply(MProduct t) {
                        return t.isSelect;
                    }
                }).toList();
                for (MProduct mProduct : mProducts) {
                    addToLack(mProduct);
                }
                for (MProduct mProduct : mProducts) {
                    removeProductList(mProduct);
                }
                if (mProducts.size() != 0) {

                    activateLack();
                    SettingsAddinStock.save();
                }
            }
        });
        findViewById(R.id.bt_lack_richt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        lackListView = (ListView) findViewById(R.id.list_lack);
        productListView = (ListView) findViewById(R.id.list_product_lack);


        lackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {

                if (!mLackSettings.mStatusSender) {
                    DialogEditCountProductForAddinStock dialog = new DialogEditCountProductForAddinStock();
                    dialog.setProduct((MProduct) view.getTag()).setActionDelete(new IActionE() {
                        @Override
                        public void action(Object o) {
                            MProduct mProduct = (MProduct) o;
                            mLackSettings.mLastListNew.remove(mProduct);
                            addToListProduct(mProduct);
                            Collections.sort(mProductList, new Comparator<MProduct>() {
                                @Override
                                public int compare(MProduct lhs, MProduct rhs) {
                                    return lhs.name.compareTo(rhs.name);
                                }
                            });
                            activateLack();
                            showAll();
                            SettingsAddinStock.save();
                        }
                    }).setActionSave(new IActionE() {
                        @Override
                        public void action(Object o) {
                            activateLack();
                            SettingsAddinStock.save();
                        }
                    }).show(getSupportFragmentManager(), "sdsd");
                }
            }
        });
        lackListView.setOnCreateContextMenuListener(this);

        final EditText lack_search_product_edit = (EditText) findViewById(R.id.lack_search_product_edit);
        lack_search_product_edit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
        lack_search_product_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    showAll();
                } else {
                    showSearch(s.toString());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    lack_search_product_edit.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    //Assign your image again to the view, otherwise it will always be gone even if the text is 0 again.
                    lack_search_product_edit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
                }
            }
        });
    }

    private void removeProductList(final MProduct mProduct) {

        MProduct product = Linq.toStream(mProductList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.idu.equals(mProduct.idu);
            }
        });

        if (product != null) {
            mProductList.remove(mProduct);
        }
    }

    private void activateLack() {

        adapterLock = new ListAdapterAddinStock(this, R.layout.item_lock, new ArrayList<>(mLackSettings.mLastListNew));
        Parcelable index = null;
        try {
            index = lackListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        lackListView.setAdapter(adapterLock);
        if (index != null) {
            lackListView.onRestoreInstanceState(index);
        }
    }

    private void activateListProduct(List<Object> objects) {


        if (showStock == false) {
            RefillStock.this.showStock = false;
            findViewById(R.id.haeaderSelfStock).setVisibility(View.VISIBLE);
            findViewById(R.id.haeaderSelfStock2).setVisibility(View.GONE);
        } else {

            findViewById(R.id.haeaderSelfStock).setVisibility(View.VISIBLE);
            findViewById(R.id.haeaderSelfStock2).setVisibility(View.VISIBLE);

        }

        Parcelable index = null;
        try {
            index = productListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        listAdapterStock = new ListAdapterStock(this, R.layout.myrow_date_sale_product_fix, objects, new ArrayList<MProductBase>(mLackSettings.mLastListNew), true);//mSettings,
        listAdapterStock.showDataSelfStock = true;
        listAdapterStock.showDataSelf2Stock = true;
        listAdapterStock.setShowBort(showStock);
        listAdapterStock.setOnRowDataClick(this).setOnRowNavigateClick(this);
        productListView.setAdapter(listAdapterStock);
        if (index != null) {
            productListView.onRestoreInstanceState(index);
        }
    }

    private void activateListProduct(List<Object> objects,String strSearch) {


        if (showStock == false) {
            RefillStock.this.showStock = false;
            findViewById(R.id.haeaderSelfStock).setVisibility(View.VISIBLE);
            findViewById(R.id.haeaderSelfStock2).setVisibility(View.GONE);
        } else {

            findViewById(R.id.haeaderSelfStock).setVisibility(View.VISIBLE);
            findViewById(R.id.haeaderSelfStock2).setVisibility(View.VISIBLE);

        }

        Parcelable index = null;
        try {
            index = productListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        listAdapterStock = new ListAdapterStock(this, R.layout.myrow_date_sale_product_fix, objects, new ArrayList<MProductBase>(mLackSettings.mLastListNew), true,strSearch);//mSettings,
        listAdapterStock.showDataSelfStock = true;
        listAdapterStock.showDataSelf2Stock = true;
        listAdapterStock.setShowBort(showStock);
        listAdapterStock.setOnRowDataClick(this).setOnRowNavigateClick(this);
        productListView.setAdapter(listAdapterStock);
        if (index != null) {
            productListView.onRestoreInstanceState(index);
        }
    }

    private void activateABC() {

        MyListViewAbc mListViewE = (MyListViewAbc) findViewById(R.id.list_abc_lack);
        mListViewE.setIAction(new IActionE<String>() {
            @Override
            public void action(String o) {
                showProducABC(o);
            }
        });

    }

    private void showAll() {

        Linq.toStream(mProductList).forEach(new Action<MProduct>() {
            @Override
            public void action(MProduct mProduct) {
                mProduct.isSelect = false;
            }
        });

        List<Object> objectsnew = new ArrayList<>();
        for (MProductGroup mProduct : mProductGroups) {
            mProduct.isSelect = false;
            objectsnew.add(mProduct);
        }
        activateListProduct(objectsnew);
    }

    public void showNotAll(List<MProduct> mProduct) {
        List<Object> objectsnew = new ArrayList<>();
        for (MProduct p : mProduct) {
            p.isSelect = false;
            objectsnew.add(p);
        }
        activateListProduct(objectsnew);
    }

    private void showSearch(String str) {
        Linq.toStream(mProductList).forEach(new Action<MProduct>() {
            @Override
            public void action(MProduct mProduct) {
                mProduct.isSelect = false;
            }
        });
        List<Object> objects = new ArrayList<Object>(FactoryProduct.getSearchResult(mProductList, str));
        activateListProduct(objects,str);
    }


    private void showProducABC(String str) {
        Linq.toStream(mProductList).forEach(new Action<MProduct>() {
            @Override
            public void action(MProduct mProduct) {
                mProduct.isSelect = false;
            }
        });
        List<Object> objects = new ArrayList<Object>(FactoryProduct.getListAbs(mProductList, str));
        activateListProduct(objects);

    }

    @Override
    public void onRowDataClick(MProductBase mProduct, View view, List<Object> objects) {

        mProduct.isSelect = !mProduct.isSelect;
        // CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox_d);
        // checkBox.setChecked(mProduct.isSelect);
        CheckBox myCheck = (CheckBox) view.findViewById(R.id.checkbox_d);
        myCheck.setChecked(mProduct.isSelect);
        //listAdapterStock.notifyAddin(mProduct.idu);
    }

    @Override
    public void onRowNavigateClick(MProductGroup mProductGroup, List<Object> objects) {
        mProductGroup.isSelect = !mProductGroup.isSelect;

        List<Object> objectList;
        if (mProductGroup.isSelect) {
            objectList = AddinData(objects, mProductGroup);
        } else {
            objectList = RemoveData(objects, mProductGroup);
        }
        activateListProduct(objectList);
    }

    private List<Object> RemoveData(List<Object> objects, MProductGroup mProductGroup) {

        List<Object> objectListDelete = new ArrayList<>();
        for (Object object : objects) {
            if (object instanceof MProductGroup) {
                if (((MProductGroup) object).parent_id != null && ((MProductGroup) object).parent_id.equals(mProductGroup.idu)) {
                    objectListDelete.add(object);
                }
            } else {
                if (((MProduct) object).group_id.equals(mProductGroup.idu)) {
                    objectListDelete.add(object);
                }
            }
        }
        for (Object o : objectListDelete) {
            objects.remove(o);
        }
        return objects;
    }

    private List<Object> AddinData(List<Object> objects, MProductGroup mProductGroup) {

        List<MProduct> mProductList1 = FactoryProduct.getListForMenu(mProductList, mProductGroup.idu);
        int index = 0;
        for (Object object : objects) {
            index++;
            if (object instanceof MProductGroup) {
                MProductGroup sd = (MProductGroup) object;
                if (sd.idu.equals(mProductGroup.idu)) {
                    break;
                }
            }
        }
        objects.addAll(index, new ArrayList<Object>(mProductList1));
        return objects;
    }

    private void addToLack(final MProduct mProduct) {
        mProduct.isSelect = false;

        MProduct product = Linq.toStream(mLackSettings.mLastListNew).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.idu.equals(mProduct.idu);
            }
        });

        if (product == null) {
            mProduct.setAmountCore(10d);
            mLackSettings.mLastListNew.add(mProduct);
        }
    }

    private void addToListProduct(final MProduct mProduct) {
        mProduct.isSelect = false;
        mProduct.setAmountCore(0);// = 0;

        MProduct product = Linq.toStream(mProductList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.idu.equals(mProduct.idu);
            }
        });

        if (product == null) {
            mProductList.add(mProduct);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
