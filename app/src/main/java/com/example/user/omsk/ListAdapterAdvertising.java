package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.omsk.models.MPromotion;

import java.util.List;


public class ListAdapterAdvertising extends ArrayAdapter<MPromotion> {

    private final int mResource;
    private final String strSearch;

    public ListAdapterAdvertising(Context context, int resource, List<MPromotion> objects,String strSearch) {
        super(context, resource, objects);
        this.mResource = resource;
        this.strSearch = strSearch;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;

        if (mView == null) {
            mView = LayoutInflater.from(getContext()).inflate(mResource, null);
        }
        MPromotion p = getItem(position);

        TextView name = (TextView) mView.findViewById(R.id.name_promotion);
        TextView description = (TextView) mView.findViewById(R.id.description_promotion);
        ImageView imageView = (ImageView) mView.findViewById(R.id.image_promotion);
        name.setText(p.name);

        if (p.description == null || p.description.trim().length() == 0) {
            description.setVisibility(View.GONE);
        } else {
            description.setText(p.description);
        }

        if (p.images != null) {
            Bitmap bitmap = getBitmapFromByteArray(p.images);
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
        Utils.drawSearchText(name,strSearch);

        return mView;
    }

    private static Bitmap getBitmapFromByteArray(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

}
