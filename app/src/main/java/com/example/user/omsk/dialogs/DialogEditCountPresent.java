package com.example.user.omsk.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.myTextView.MyButton;

import org.apache.log4j.Logger;

/**
 * Created by USER on 11.04.2018.
 */

public class DialogEditCountPresent extends DialogFragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(DialogEditCountPresent.class);
    public IActionE<Integer> iActionE;
    public int amount=0;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit_presetn, null);
        TextView title = (TextView) v.findViewById(R.id.title_dalog);
        title.setText("Редактирование подарков");
        final EditText editText= (EditText) v.findViewById(R.id.edit_present);
        editText.setText(String.valueOf(amount));
        MyButton myButton= (MyButton) v.findViewById(R.id.save_present);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    int i=Integer.parseInt(editText.getText().toString());
                    if(iActionE!=null){
                        iActionE.action(i);
                    }
                    dismiss();
                }catch (Exception e){

                    log.error(e);
                }

            }
        });
        v.findViewById(R.id.delete_present).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(iActionE!=null){
                        iActionE.action(0);
                    }
                    dismiss();
                }catch (Exception e){

                    log.error(e);
                }
            }
        });

        builder.setView(v);
        return builder.create();
    }
}
