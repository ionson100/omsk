package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MyReportField;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.UserField;


@Table("zreport")
public class MZReport {
    //CREATE TABLE "zreport" ("id" INTEGER PRIMARY KEY  NOT NULL ,"report" TEXT DEFAULT (null) )
    public MZReport(ZetReport mZetReprot) {
        this.mZetReprot = mZetReprot;
    }

    public MZReport() {
    }

    @PrimaryKey("id")
    public int id;

    @Column("report")
    @UserField(IUserType = MyReportField.class)
    public ZetReport mZetReprot;

//    @Column("isSender")
//    public boolean isSender=false;
}
