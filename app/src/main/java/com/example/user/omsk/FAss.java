package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitStoryAss;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class FAss extends Fragment {

    private View mView;
    private ListView mListView;

    public FAss() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_fass, container, false);
        mListView = (ListView) mView.findViewById(R.id.list_ass_visit);
        createList();
        return mView;
    }

    private void createList() {
        List<MVisitStoryAss> listOld = Configure.getSession().getList(MVisitStoryAss.class, null);
        Collections.sort(listOld, new Comparator<MVisitStoryAss>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MVisitStoryAss lhs, MVisitStoryAss rhs) {
                return Integer.compare(lhs.finish,rhs.finish);
            }
        });
        ListAdapterAssVisit listAdapterAssVisit = new ListAdapterAssVisit(getActivity(), R.layout.item_list_ass_visit, listOld, new ListAdapterAssVisit.IActionAss() {
            @Override
            public void visit(String idVisit) {

                MVisitStoryAss f = Configure.getSession().get(MVisitStoryAss.class, idVisit);
                if (f == null) {
                    Toast.makeText(getActivity(), "Не могу найти визит", Toast.LENGTH_SHORT).show();
                } else {
                    MVisit mVisit = f.getMVisit(getContext());
                    mVisit.isAss = true;
                    mVisit.setStart(Utils.dateToInt(new Date()));
                    Settings.core().setVisit(mVisit);
                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                }
            }

            @Override
            public void delete(final String idVisit) {
                Utils.messageBox(getString(R.string.delete_ass), getString(R.string.message_ass), getActivity(), new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        deleteVisitAss(idVisit);
                        createList();
                    }
                });
            }
        });
        mListView.setAdapter(listAdapterAssVisit);
    }

    public static void deleteVisitAss(String idVisit) {
        ISession ses = Configure.getSession();
        MVisitStoryAss old = ses.get(MVisitStoryAss.class, idVisit);
        Configure.getSession().delete(old);
    }

    public static void deleteVisitAss(String idVisit, ISession iSession) {

        MVisitStoryAss old = iSession.get(MVisitStoryAss.class, idVisit);
        Configure.getSession().delete(old);
    }
}
