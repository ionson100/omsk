package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DialogShowDataCheck extends DialogFragment {

    private List<MProductBase> mProductList = new ArrayList<>();

    public void setmProductList(List<MProductBase> products) {
        this.mProductList = products;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_show_data_check, null);
        ((TextView) mView.findViewById(R.id.title_dalog)).setText(getString(R.string.jjkdsd));
        builder.setView(mView);
        LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.panel_base);
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Collections.sort(mProductList, new Comparator<MProductBase>() {
            @Override
            public int compare(MProductBase lhs, MProductBase rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        for (MProductBase product : mProductList) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_product_story, null);
            TextView name = (TextView) view.findViewById(R.id.name_s);
            TextView amountE = (TextView) view.findViewById(R.id.amount_s);
            TextView priceE = (TextView) view.findViewById(R.id.price_s);
            TextView itogo = (TextView) view.findViewById(R.id.itogo_s);
            name.setText("  " + product.name);
            amountE.setText(Utils.getStringDecimal(product.getAmount_core()));
            priceE.setText(Utils.getStringDecimal(product.price));
            itogo.setText(Utils.getStringDecimal(product.getAmount_core() * product.price));
            linearLayout.addView(view);
        }
        return builder.create();
    }
}
