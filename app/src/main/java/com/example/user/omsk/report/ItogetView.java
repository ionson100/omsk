package com.example.user.omsk.report;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.R;
import com.example.user.omsk.TempSale;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.pupper.Pupper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.id;

public class ItogetView {

    public static void addDebetProduct(Context context, Itoger itoger, View view, boolean isHistoryCore) {
        List<MProduct> list = itoger.getMapItogoForDebt(isHistoryCore);
        LinearLayout panelTable2 = (LinearLayout) view.findViewById(R.id.panel_all_saleProdict_debt);
        Pupper amountTE = (Pupper) view.findViewById(R.id.itoger_amount_amount_debt);
        Pupper priceTd = (Pupper) view.findViewById(R.id.itoger_price_debt);
        if (list.size() > 0) {
            double totalAmount = 0d;
            double totalPrice = 0d;
            for (MProduct product : list) {
                View vvv = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) vvv.findViewById(R.id.name_s);
                TextView amountE = (TextView) vvv.findViewById(R.id.amount_s);
                TextView priceE = (TextView) vvv.findViewById(R.id.price_s);
                TextView itogo = (TextView) vvv.findViewById(R.id.itogo_s);
                name.setText("  " + product.name);
                amountE.setText(Utils.getStringDecimal(product.getAmount_core()));
                priceE.setText(Utils.getStringDecimal(product.price));
                itogo.setText(Utils.getStringDecimal((product.price * product.getAmount_core())));
                panelTable2.addView(vvv);
                totalAmount = totalAmount + product.getAmount_core();
                totalPrice = totalPrice + (product.getAmount_core() * product.price);
            }
            if (isHistoryCore == false) {
                amountTE.setPairString(R.string.itogo_count_products_debt, Utils.getStringDecimal(totalAmount));
                priceTd.setPairString(R.string.itogo_price4, Utils.getStringDecimal(totalPrice));
            } else {
                amountTE.setPairString(R.string.itogo_count_products_debt2, Utils.getStringDecimal(totalAmount));
                priceTd.setPairString(R.string.itogo_price42, Utils.getStringDecimal(totalPrice));

            }
        } else {
            view.findViewById(R.id.panel_all_saleProdict_debt_label).setVisibility(View.GONE);
            panelTable2.setVisibility(View.GONE);
            amountTE.setVisibility(View.GONE);
            priceTd.setVisibility(View.GONE);
        }
    }

    public static void addSaleProduct(Context context, Itoger itoger, View view) {
        List<MProduct> listAction = itoger.getMapItogoForSale();
        Pupper priceT = (Pupper) view.findViewById(R.id.itoger_price);
        Pupper amountT = (Pupper) view.findViewById(R.id.itoger_amount_amount);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.panel_all_saleProdict);
        Pupper nameT = (Pupper) view.findViewById(R.id.itoger_amount_name);
        if (listAction.size() > 0) {
            double totalAmountSale = 0;
            double totalPrice = 0d;
            linearLayout.setVisibility(View.VISIBLE);
            for (MProduct product : listAction) {
                View viewSale = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) viewSale.findViewById(R.id.name_s);
                TextView amountE = (TextView) viewSale.findViewById(R.id.amount_s);
                TextView priceE = (TextView) viewSale.findViewById(R.id.price_s);
                TextView itogo = (TextView) viewSale.findViewById(R.id.itogo_s);
                name.setText(product.name);
                amountE.setText(Utils.getStringDecimal(product.getAmount_core()));
                priceE.setText(Utils.getStringDecimal(product.price));
                itogo.setText(Utils.getStringDecimal(product.getAmount_core() * product.price));
                totalAmountSale = totalAmountSale + product.getAmount_core();
                totalPrice = totalPrice + product.getAmount_core() * product.price;
                linearLayout.addView(viewSale);
            }

            priceT.setPairString(R.string.itogo_price3, Utils.getStringDecimal(totalPrice));
            nameT.setPairString(R.string.itogo_name, Integer.toString(listAction.size()));
            amountT.setPairString(R.string.itogo_count_products, Utils.getStringDecimal(totalAmountSale));
        } else {
            nameT.setVisibility(View.GONE);
            priceT.setVisibility(View.GONE);
            amountT.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            view.findViewById(R.id.panel_all_saleProdict_label).setVisibility(View.GONE);
        }
    }

    public static void addActionsProduct(Context context, Itoger itoger, View view) {
        List<MProduct> listAction = itoger.getMapItogoForAction();
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.panel_action_products);
        Pupper total_actions = (Pupper) view.findViewById(R.id.itoger_amount_actions);
        if (listAction.size() > 0) {
            double totalAmountActions = 0;
            for (MProduct product : listAction) {
                View viewSale = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) viewSale.findViewById(R.id.name_s);
                TextView amountE = (TextView) viewSale.findViewById(R.id.amount_s);
                TextView priceE = (TextView) viewSale.findViewById(R.id.price_s);
                TextView itogo = (TextView) viewSale.findViewById(R.id.itogo_s);
                name.setText(product.name);
                totalAmountActions = totalAmountActions + product.getAmount_core();
                amountE.setText(Utils.getStringDecimal(product.getAmount_core()));
                priceE.setText(Utils.getStringDecimal(0d));
                itogo.setText(Utils.getStringDecimal(0d));
                linearLayout.addView(viewSale);
            }
            total_actions.setPairString(R.string.extract_actions, Utils.getStringDecimal(totalAmountActions));
        } else {
            total_actions.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            view.findViewById(R.id.panel_action_products_label).setVisibility(View.GONE);
        }
    }

    public static void addFreeProduct(Context context, Itoger itoger, View view) {
        List<MProduct> presentList = itoger.getMapItogoForFree();
        LinearLayout panelTablePresent = (LinearLayout) view.findViewById(R.id.panel_all_saleProdict_present);
        Pupper amountPtesent = (Pupper) view.findViewById(R.id.itoger_amount_present);
        if (presentList.size() > 0) {
            panelTablePresent.setVisibility(View.VISIBLE);
            double amountPresent = 0;
            for (MProduct product : presentList) {
                View viewSale = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) viewSale.findViewById(R.id.name_s);
                TextView amountE = (TextView) viewSale.findViewById(R.id.amount_s);
                TextView priceE = (TextView) viewSale.findViewById(R.id.price_s);
                TextView itogo = (TextView) viewSale.findViewById(R.id.itogo_s);
                name.setText("  " + product.name);
                amountE.setText(Utils.getStringDecimal(product.getAmount_core()));
                priceE.setText(Utils.getStringDecimal(0d));
                itogo.setText(Utils.getStringDecimal((0d)));
                panelTablePresent.addView(viewSale);
                amountPresent = amountPresent + product.getAmount_core();
            }
            amountPtesent.setPairString(R.string.itogo_present, Utils.getStringDecimal(amountPresent));
        } else {
            panelTablePresent.setVisibility(View.GONE);
            amountPtesent.setVisibility(View.GONE);
            view.findViewById(R.id.panel_all_saleProdict_present_label).setVisibility(View.GONE);
        }
    }


    public static void addTempSale(final TempSale temp, List<TempSale> tempSaleList) {

        TempSale tempSale1 = Linq.toStream(tempSaleList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<TempSale>() {
            @Override
            public boolean apply(TempSale t) {
                return t.product_id.equals(temp.product_id);
            }
        });
        if (tempSale1 == null) {
            tempSaleList.add(temp);
        } else {
            if (tempSale1.price != temp.price) {
                tempSaleList.add(temp);
            } else {
                tempSale1.amount = tempSale1.amount + temp.amount;
            }
        }
    }

    public static void addReturmRefunds(Context context, Itoger itoger, View view) {
        Pupper itoger_refund_hand = (Pupper) view.findViewById(R.id.itoger_refund_hand);
        double refundTotal = itoger.getDebtMoneyForStory();
        if (refundTotal > 0) {
            itoger_refund_hand.setPairString(R.string.itogo_refund, Utils.getStringDecimal(refundTotal));
        } else {
            itoger_refund_hand.setVisibility(View.GONE);
        }
    }

    public static void addCompyteTotalPrice(Context context, Itoger itoger, View view) {
        double totalMain = itoger.getTotalMain();
        Pupper itoger_amount_price_hand = (Pupper) view.findViewById(R.id.itoger_amount_price_hand);
        itoger_amount_price_hand.setPairString(R.string.itogo_hand, Utils.getStringDecimal(totalMain));//
    }

    public static void addPresent(Context context, List<MVisitStoryBase> mVisitStoryList, View view) {

        Map<String,MVisitPresent> map=new HashMap<>();

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if(mVisitStoryBase==null) continue;
            for (MVisitPresent present : mVisitStoryBase.getPresnt()) {
                if(map.containsKey(present.id)){
                    int d=map.get(present.id).amount;
                    map.get(present.id).amount=d+present.amount;
                }else {
                    MVisitPresent vp=new MVisitPresent();
                    vp.visit_id=present.visit_id;
                    vp.id=present.id;
                    vp.amount=present.amount;
                    map.put(present.id,vp);
                }
            }
        }
        if(map.size()>0){
            view.findViewById(R.id.panel_present).setVisibility(View.VISIBLE);
            LinearLayout layoutPresent= (LinearLayout) view.findViewById(R.id.panel_present_core);
            for (MVisitPresent present : map.values()) {
                View viewPresent = LayoutInflater.from(context).inflate(R.layout.item_present_story, null);
                TextView name = (TextView) viewPresent.findViewById(R.id.name_present);
                TextView amount = (TextView) viewPresent.findViewById(R.id.amount_present);
                name.setText(present.getName());
                amount.setText(String.valueOf(present.amount));
                layoutPresent.addView(viewPresent);
            }
        }
    }
}
