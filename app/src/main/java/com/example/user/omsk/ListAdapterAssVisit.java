package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStoryAss;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.List;


public class ListAdapterAssVisit extends ArrayAdapter<MVisitStoryAss> {

    public interface IActionAss {
        void visit(String isVisit);

        void delete(String isVisit);
    }


   // private List<MVisitStoryAss> mVisitStoryOldList;
   // private Context mContext;
    private int resource;
    private IActionAss iActionAss;
    private List<MProduct> mProductList;
    private List<MOrderType> mOrderTypes;
    private List<MVisitResults> mVisitResultses;
    //private Settings mSettings;


    public ListAdapterAssVisit(Context context, int resource, List<MVisitStoryAss> objects, IActionAss iActionAss) {
        super(context, resource, objects);
       // mVisitStoryOldList = objects;
        this.resource = resource;
        //this.mContext = context;
        this.iActionAss = iActionAss;
        ISession session = Configure.getSession();
        mProductList = session.getList(MProduct.class, null);
        mOrderTypes = session.getList(MOrderType.class, null);
        mVisitResultses = session.getList(MVisitResults.class, null);
      //  mSettings = Settings.core();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;

        final MVisitStoryAss p = getItem(position);
        if (p != null) {

            mView = LayoutInflater.from(getContext()).inflate(resource, null);
            mView.findViewById(R.id.bt_ass_visit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iActionAss.visit(p.idu);
                }
            });

            mView.findViewById(R.id.bt_ass_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iActionAss.delete(p.idu);
                }
            });

            LinearLayout ass_visit_body = (LinearLayout) mView.findViewById(R.id.ass_visit_body);

            View view = UtilsViewBuilder.getItemStoryVisit(mOrderTypes, mVisitResultses, p, getContext(), mProductList, false, false, (Activity) getContext(), true);
            ass_visit_body.addView(view);

        }
        return mView;
    }
}

