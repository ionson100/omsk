//package com.example.user.omsk.models;
///********************************************************************
// * Copyright © 2016-2017 OOO Bitnic                                 *
// * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
// * ******************************************************************/
//
//import com.example.user.omsk.LoggerE;
//import com.example.user.omsk.orm2.Column;
//import com.example.user.omsk.orm2.PrimaryKey;
//import com.example.user.omsk.orm2.Table;
//
//import static com.example.user.omsk.Utils.getErrorTrace;
//
//// хранилище ошибок
//@Table("error")
//public class MError {
//
//    @PrimaryKey("id")
//    public int id;
//
//    @Column("message")
//    public String message;
//
//    public MError(String message) {
//
//        LoggerE.logI("___ERROR___ " + message);
//        this.message = message;
//    }
//
//    public MError(Exception ex) {
//
//        String s = getErrorTrace(ex);
//        LoggerE.logI("___ERROR___ " + s);
//        this.message = s;
//    }
//
//    public MError() {
//    }
//}
