package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.google.gson.Gson;

public class SenderLogin {


    private final Settings settings;
    private MyApplication application;

    public SenderLogin() {
        this.settings = Settings.core();
    }

    public String sendLogin(MyApplication application, String user, String pwd) {
        this.application = application;
        String param = getParams(user, pwd);
        return sendCore(param);
    }

    private String getParams(String email, String pwd) {
        return "[\"" + email.trim() + "\",\"" + pwd.trim() + "\",\"" + SettingsUser.core().getUuidDevice() + "\"]";
    }

    private String sendCore(String message) {
        final Settings mSettings = Settings.core();
        final String[] res = {null};

        res[0] = UtilsSender.postRequest(application, Utils.HTTP + settings.url + "/handshake/", message, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {


                Gson sd = Utils.getGson();
                user user = sd.fromJson(str, user.class);
                SettingsUser.core().setUserRegion(user.user, user.region);
                mSettings.setStartLatitude(Utils.LATITUDE);
                mSettings.setStartLongitude(Utils.LONGITUDE);
                Settings.save();

            }
        });
        return res[0];
    }

    class user {
        public String user;
        public String region;
    }
}
