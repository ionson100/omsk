package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

// хранилище приоритетов продажи ( получаем с сервера)
@Table("priority")
public class MPriority implements IUsingGuidId {
    @PrimaryKey("id")
    public transient int id;

    @SerializedName("id")
    @Column("idu")
    public String idu;

    @SerializedName("value")
    @Column("value")
    public int value;

    @SerializedName("name")
    @Column("name")
    public String name;

    public MPriority() {
    }

    public MPriority(String idu, int value, String name) {
        this.idu = idu;
        this.value = value;
        this.name = name;
    }

    public String get_id() {
        return idu;
    }
}
