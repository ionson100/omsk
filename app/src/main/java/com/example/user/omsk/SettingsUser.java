package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;

public class SettingsUser implements Serializable {


    /**
     * признак авторизации пользователя
     */
    private boolean isAutorise = false;

    private String user;

    private String region;

    public void setUserRegion(String user, String region) {
        this.user = user;
        this.region = region;
    }

    public boolean isAutorise() {
        return isAutorise;
    }

    public void setAutorise(boolean autorise) {
        isAutorise = autorise;
        Reanimator.save(SettingsUser.class);
    }

    public static SettingsUser core() {
        return (SettingsUser) Reanimator.get(SettingsUser.class);
    }

    private String uuidDevice;

    // гуид устройства
    public String getUuidDevice() {
        return uuidDevice;
    }

    public void setUuidDevice(String uuidDevice) {
        this.uuidDevice = uuidDevice;

        Reanimator.save(SettingsUser.class);
    }

    // название пользователя
    public String getUserName() {
        return user;
    }

    // регион пользователя
    public String getRegion() {
        return region;
    }
}
