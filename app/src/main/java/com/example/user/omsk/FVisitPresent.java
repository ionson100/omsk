package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.user.omsk.models.MProduct;

import java.util.Collections;
import java.util.Comparator;

public class FVisitPresent extends Fragment {

    Settings mSettings;
    //private List<MStockRows> mStockRowsList;
    //private PupperPartial mTotalProduct;
    //private PupperPartial mTotalAmount;
   // private PupperPartial mTotalPrice;
    private ListView mListView;
    private View mView;

    public FVisitPresent() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_present, container, false);
        mSettings = Settings.core();
        mView.findViewById(R.id.button_regiest_add_sale_product).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.VISIT_STOCK_PRESENT, getActivity());
            }
        });
        mView.findViewById(R.id.button_regiest_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            }
        });
        //mTotalProduct = (PupperPartial) mView.findViewById(R.id.reqiest_total_product);
        // mTotalAmount = (PupperPartial) mView.findViewById(R.id.reqiest_total_count);
        // mTotalPrice = (PupperPartial) mView.findViewById(R.id.reqiest_total_price_text);
        Transceiver.subscribe(TransceiveStaticVariables.F2323, new Transceiver.ITransceiver() {
            @Override
            public void action(Object o) {
                if (o != null) {
                    MProduct paierPresent = null;
                    for (MProduct paier : mSettings.getVisit().selectProductForPresent) {
                        if (paier.get_id().equals(o.toString())) {
                            paierPresent = paier;  //TODO axtung
                        }
                    }
                    if (paierPresent != null) {
                        mSettings.getVisit().selectProductForPresent.remove(paierPresent);
                    }
                } else {
                    mSettings.getVisit().selectProductForPresent.clear();
                }
                Settings.save();
                activateListProduct();
            }
        });
        activateListProduct();
        mView.findViewById(R.id.bt_help_free).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showHelpNote(getActivity(),  "note_action.html");
            }
        });
        return mView;
    }

    @SuppressLint("SetTextI18n")
    private void activateListProduct() {
        mListView = (ListView) mView.findViewById(R.id.list_selectted_product);
        if (mSettings.getVisit().selectProductForPresent != null) {
            Collections.sort(mSettings.getVisit().selectProductForPresent, new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            ListAdapterForSelectedProducts sd = new ListAdapterForSelectedProducts(getActivity(),
                    R.layout.item_list_product_selected_for_sale_2, mSettings.getVisit().selectProductForPresent, new IActionE<View>() {
                @Override
                public void action(View v) {

                }
            });
            mListView.setAdapter(sd);

//            mTotalProduct.setPairString(getString(R.string.itogo_poduct_naimenovany), String.valueOf(mSettings.getVisit().selectProductForPresent.size()));
//            double count = 0;
//            double price = 0;
//            for (MProduct mPaier : mSettings.getVisit().selectProductForPresent) {
//                count = count + mPaier.getAmount_core();
//                price = price + mPaier.getTotalPrice();
//            }
//            mTotalAmount.setPairString(getString(R.string.itogo_count_products1), Utils.getStringDecimal(count));
//            mTotalPrice.setPairString(getString(R.string.itogo_price3), Utils.getStringDecimal(price) + getString(R.string.rub));

        }
    }

    @Override
    public void onDestroy() {
        Transceiver.cancelSubscribe(TransceiveStaticVariables.F2323);
        super.onDestroy();
    }
}
