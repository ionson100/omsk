package com.example.user.omsk.freesql;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.example.user.omsk.LoggerE;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.List;

public class FreeSqlActivity extends Activity {

    private SettingSql mSettingSql;
    private EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_sql);
        mSettingSql = SettingSql.core();

        mEditText = (EditText) findViewById(R.id.sql_text);
        if (mSettingSql.sql.length() != 0) {
            mEditText.setText(mSettingSql.sql);
        }
        mEditText.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    sqlExecute();

                    return true;
                }
                return false;
            }
        });

    }

    private void sqlExecute() {
        try {
            String sql = mEditText.getText().toString().trim();
            if (sql.length() == 0) return;
            mSettingSql.sql = sql;
            SettingSql.save();
            LoggerE.logI("____freeSql____:" + sql);
            Cursor cursor = Configure.getSession().getSqLiteDatabase().rawQuery(mEditText.getText().toString(), null);
            int i = cursor.getColumnCount();
            List<String> fieldStrings = new ArrayList<>(i);
            List<List<String>> listData = new ArrayList<>();
            for (int ii = 0; ii < i; ii++) {
                fieldStrings.add(cursor.getColumnName(ii));
            }
            while (cursor.moveToNext()) {

                List<String> tempList = new ArrayList<>(i);
                for (int ii = 0; ii < i; ii++) {

                    tempList.add(cursor.getString(ii));
                }
                listData.add(tempList);
            }
            cursor.close();
            activateTable(fieldStrings, listData);
        } catch (Exception e) {
            Utils.messageBox(getString(R.string.cvbb), e.getMessage(), this, null);
        }

    }

    private void activateTable(List<String> fieldStrings, List<List<String>> listData) {

        try {
            FreeTables s = (FreeTables) findViewById(R.id.sql_table);
            s.activate(fieldStrings, listData);
        } catch (Exception e) {
            Utils.messageBox(getString(R.string.cvbbbb), e.getMessage(), this, null);
        }

    }
}
