package com.example.user.omsk.senders;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

public interface IActionResponse {
    void invoke(String str, int status);
}
