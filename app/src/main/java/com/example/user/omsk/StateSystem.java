package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

/**
 * Состояние системы
 */
public class StateSystem {

    public static final int HOME = 0;// домашняя страница
    public static final int SIMPLE_MAP = 1;// простой показ карты
    public static final int SETTINGS = 2;// настройки
    public static final int CHAT = 4; // чат
    public static final int POINT = 5;// показ  описания точки
    public static final int POINT_ADD = 6;// добавление точки
    public static final int POINT_EDIT = 7;// редактирование точки
    public static final int CONTRAGENT_ADD = 8;// добавление контрагента
    public static final int CONTRAGENT_EDIT = 9;// редактирование контрагента
    public static final int EDIT_MAP = 10;//  показ карты для  определения  координат точки
    public static final int SHOW_MAP = 11;// карта  просмотр положения точки
    public static final int SHOW_STOCK_SIMPLE = 12;// показ склада не из визита
    public static final int VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE = 13;// показ склада  из визита для продажи товара
    public static final int SHOW_POINT_SELECT_POINT_OR_AGENT = 14;//?? похоже не используется
    public static final int ERROR = 15;// страница  ошибок
    public static final int VISIT_ERROR = 6001;// страница ошибок из визита
    public static final int VISIT_HOME = 19;// дом. стр. визита
    public static final int VISIT_START = 20;// параметры точки из визита
    public static final int VISIT_BALANCE = 21;// визит страница остатков
    public static final int VISIT_SALE = 3;// визит страница продажи
    public static final int VISIT_COMMIT = 22;// страница окончания визита
    public static final int CONTACT_ADD = 23;// добавление контакта
    public static final int CONTACT_EDIT = 24;// редактирование контакта
    public static final int VISIT_SHOW_SETTINGS = 30;// настройки из визита
    public static final int VISIT_SHOW_CHAT = 31;// чат из визита
    public static final int VISIT_SHOW_MAP = 32;// карта из визита
    public static final int VISIT_SHOW_POINT = 33;// характеристики  точки из визита ( ничего делать нельзя)
    public static final int VISIT_SHOW_STOCK = 34;
    public static final int VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE = 35;
    public static final int EDIT_MAP_SETTINGS = 47;// карта для выбора центра показа  карты
    public static final int SEARCH_POINT = 100;// страница точек
    public static final int POINT_ADD_SEARCH = 101;// добавление точки из страницы выбора точек
    public static final int POINT_EDIT_SEARCH = 102;// редактирование точки из показа всех точек
    public static final int CONTRAGENT_ADD_SEARCH = 103;// добавление контрагента из страницы показа точек
    public static final int CONTRAGENT_EDIT_SEARCH = 104;// редактирование контрагента из страницы просмотра точек
    public static final int ADVERTISING = 111;// рекламные материалы
    public static final int VISIT_ADVERTISING = 200;// рекламные материалы из визита
    public static final int VISIT_REFUND = 300;// страница внесения суммы  в счет прошлого долга
    public static final int POINT_STORY = 4000;// история по точке
    public static final int VISIT_POINT_STORY_FOR = 4001;// история по точке из визита
    public static final int POINT_STORY_FOR_PREVIEW = 4002;// история  по точке из страницы характеристики точки
    public static final int VISIT_SHOW_MAP_FROM_POINT = 9001;// показывать карту в карту входим из описания точки
    public static final int ASS_POINT = 9002;// показываем все визиты не принятые сервером
    public static final int VISIT_ASS_POINT = 9003;// показываем все визиты не принятые сервером из визита
    public static final int VISIT_PRESENT = 9100;// показываем список подарков
    public static final int VISIT_STOCK_PRESENT = 9101;// показываем склад для выбора подарков
    public static final int VISIT_PRESENT_TYPES_SHOW = 9102; // показать тпы фри продкутов из визита
    public static final int PRESENT_TYPES_SHOW = 566778;// показать типы фри продуктов
    public static final int VISIT_ACYIONS_TYPES_SHOW = 19102;// показать типы акций из визита
    public static final int ACYIONS_TYPES_SHOW = 29102;// тпы акций из меню
    public static final int VISIT_ACTION = -12;// добавления продукта по акции
    public static final int VISIT_STOCK_ACTION = -13;// получение- выбор продукта со склада для акции
    public static final int MAP_ROUTE = -12000; // показ карты с цепочкой маршрута
    public static final int VISIT_MAP_ROUTE = -12001;// показ карты фактического  места визита и точки в маршруте
    public static final int MAP_ROUTE_STORY = -12002;// показ карты фиксации визита и тоски визита в отображении истории визитов

    public static final int MAP_ROUTE_STORY_ASS = 2000;
    public static final int VISIT_MAP_ROUTE_STORY_ASS = 2001;

}
