package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserNamesSettings implements Serializable {
    // статус пользователя 0 статус позволено все 1- заперещена отгрузка в долг
    public int status;
    public boolean idHideNoteShow;

    public static UserNamesSettings core() {
        return (UserNamesSettings) Reanimator.get(UserNamesSettings.class);
    }

    public static void save() {
        Reanimator.save(UserNamesSettings.class);
    }

    private final List<String> strings = new ArrayList<>();

    public void addEmail(String email) {
        if (!strings.contains(email)) {
            strings.add(email);
        }
    }

    public List<String> getEmailList() {
        return strings;
    }
}
