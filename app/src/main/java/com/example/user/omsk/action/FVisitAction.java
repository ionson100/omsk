package com.example.user.omsk.action;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogEditCountProduct;
import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.TransceiveStaticVariables;
import com.example.user.omsk.Transceiver;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FVisitAction extends Fragment {

    private List<MAction> mActionList;
    private View mView;
    private ListView mListView;
    Settings mSettings;

    public FVisitAction() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mSettings = Settings.core();
        mView = inflater.inflate(R.layout.fragment_visit_action, container, false);
        mListView = (ListView) mView.findViewById(R.id.list_selectted_product);
        mView.findViewById(R.id.button_to_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            }
        });
        mView.findViewById(R.id.button_action_product).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        start();

        Transceiver.subscribe(TransceiveStaticVariables.F2323, new Transceiver.ITransceiver() {
            @Override
            public void action(Object o) {
                if (o != null) {
                    MProduct prod = null;
                    for (MProduct product : mSettings.getVisit().selectProductForActions) {
                        if (product.get_id().equals(o.toString())) {
                            prod = product;
                        }
                    }
                    if (prod != null) {
                        mSettings.getVisit().selectProductForActions.remove(prod);
                    }
                } else {
                    mSettings.getVisit().selectProductForActions.clear();
                }

                Settings.save();
                ((LinearLayout) mView.findViewById(R.id.panel_action)).removeAllViews();
                start();
            }
        });

        mView.findViewById(R.id.bt_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showHelpNote(getActivity(),  "note_action.html");
            }
        });
        return mView;
    }

    void start() {
        mActionList = ActionValidate.validate(mSettings.getVisit(), getActivity());// Configure.getSession().getList(MAction.class,null);//

        List<MAction> actions = new ArrayList<>();

        for (MAction mAction : mActionList) {
            if (mAction.currentDeripaskeJs.amount > 0) {
                actions.add(mAction);
            }
        }

        ActionValidate.showmenAction((LinearLayout) mView.findViewById(R.id.panel_action), actions, getActivity());
        List<MProduct> mActionProducts = mSettings.getVisit().selectProductForActions;
        ListAdapterActionProduct listAdapterActionProduct = new ListAdapterActionProduct(getContext(), R.layout.item_action_product, mActionProducts);
        mListView.setAdapter(listAdapterActionProduct);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Transceiver.cancelSubscribe(TransceiveStaticVariables.F2323);
    }

    class ListAdapterActionProduct extends ArrayAdapter<MProduct> {

        private final int resource;

        public ListAdapterActionProduct(Context context, int resource, List<MProduct> objects) {
            super(context, resource, objects);
            this.resource = resource;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;
            mView = LayoutInflater.from(getContext()).inflate(resource, null);
            final MProduct p = getItem(position);
            TextView name, price, amount, total;

            name = (TextView) mView.findViewById(R.id.action_name);
            price = (TextView) mView.findViewById(R.id.action_price);
            amount = (TextView) mView.findViewById(R.id.action_amount);
            total = (TextView) mView.findViewById(R.id.action_total);
            final MAction s = Linq.toStream(mActionList).firstOrDefault(new Predicate<MAction>() {
                @Override
                public boolean apply(MAction t) {
                    return t.idu.equals(p.rule_id);
                }
            });

            MProduct mProduct = p;
            String productName = getString(R.string.werr);
            String actionName = getString(R.string.dgdg);
            if (productName != null) {
                productName = mProduct.name;
            }
            if (s != null) {
                actionName = s.name;
            }

            name.setText(productName + "\n" + actionName);

            price.setText(Utils.getStringDecimal(p.price));

            amount.setText(Utils.getStringDecimal(p.getAmount_core()));

            total.setText(Utils.getStringDecimal(p.getAmount_core() * p.price));

            mView.setTag(p);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogEditCountProduct dialog = new DialogEditCountProduct();
                    dialog.setActionId(s);
                    dialog.setProduct(p).show(getActivity().getSupportFragmentManager(), "edsfsdfsdf");
                }
            });

            return mView;
        }


    }


}
