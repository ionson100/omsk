package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.R;
import com.example.user.omsk.models.MAlertPoint;
import com.example.user.omsk.models.MPoint;

public class DialogAlertPoint extends DialogFragment {

    public final static int CREATE = 1;
    public final static int DELETE = 2;

    public interface IActionAlertDeleteCreate {
        void Action(MPoint mPoint, int action);
    }

    private MPoint mPoint;
    private IActionAlertDeleteCreate iActionAlertDeleteCreate;

    public DialogAlertPoint setPoint(MPoint mPoint) {

        this.mPoint = mPoint;
        return this;
    }

    public DialogAlertPoint setAction(IActionAlertDeleteCreate iActionAlertDeleteCreate) {

        this.iActionAlertDeleteCreate = iActionAlertDeleteCreate;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MAlertPoint mAlertPoint = mPoint.getAlertPoint();

        if (mAlertPoint == null) {
            mAlertPoint = new MAlertPoint();
            mAlertPoint.point_id = mPoint.idu;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_alert_point, null);

        ((TextView) v.findViewById(R.id.title_dalog)).setText(getString(R.string.jhajsas));
        builder.setView(v);
        final EditText editText = (EditText) v.findViewById(R.id.alert_message);
        editText.setText(mAlertPoint.message);
        v.findViewById(R.id.alert_commit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        final MAlertPoint finalMAlertPoint = mAlertPoint;
        v.findViewById(R.id.alert_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.alert_error), Toast.LENGTH_SHORT).show();
                } else {
                    finalMAlertPoint.message = editText.getText().toString();
                    mPoint.saveOrUpdate(finalMAlertPoint);
                    iActionAlertDeleteCreate.Action(mPoint, CREATE);
                    dismiss();
                }
            }
        });

        Button deleteBt = (Button) v.findViewById(R.id.alert_delete);
        View ss = v.findViewById(R.id.alert_delete2);
        if (mAlertPoint.id == 0) {
            ss.setVisibility(View.GONE);
        } else {
            ss.setVisibility(View.VISIBLE);
        }
        deleteBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iActionAlertDeleteCreate.Action(mPoint, DELETE);
                mPoint.deleteAlert(finalMAlertPoint);
                dismiss();
            }
        });


        return builder.create();
    }
}
