package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.dialogs.DialogEditPercentGroup;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ListAdapterForPlan extends ArrayAdapter<TempPlan> {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ListAdapterForPlan.class);
    private Map<String, View> mMapPointIdView = new HashMap<>();
    private Context context;
    private final int mResource;
    private final List<TempPlan> mList;
    private final List<MBasePlanPair> mPlanPairList;
    private final Activity mActivity;
    private final boolean mIisOld;
    private double mTot;
    private PlaneBase mPlanE;

    public ListAdapterForPlan(Context context, int resource, List<TempPlan> list, boolean isOld, Activity activity) {
        super(context, resource, list);
        this.context = context;
        this.mResource = resource;
        this.mList = list;
        this.mIisOld = isOld;
        mPlanPairList = UtilsPlan.getPlanPairList(isOld);//
        this.mActivity = activity;
        if (mPlanE == null) {
            List<PlaneBase> planEs = UtilsPlan.getPlanE(isOld);
            if (planEs.size() != 1) {
                mPlanE = new PlanE();
            } else {
                mPlanE = planEs.get(0);
            }
        } else {
        }
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View mView = convertView;
        final TempPlan p = getItem(position);
        if (p != null) {
            String key = "ewwe";
            if (p.mPoint != null) {
                key = p.mPoint.idu;
            }
            if (mMapPointIdView.containsKey(key)) {
                return mMapPointIdView.get(key);
            }
            if (p.mPoint != null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                mView = vi.inflate(mResource, null);
                TextView textViewName = (TextView) mView.findViewById(R.id.plan_name);
                TextView textViewAcb = (TextView) mView.findViewById(R.id.plan_akb);
                TextView textViewTotal = (TextView) mView.findViewById(R.id.plan_total);
                textViewTotal.setText(String.valueOf((int) p.mAllSalePachka) + "/" + "\n" + String.valueOf(p.mMedium_line));
                TextView textViewTotalRub = (TextView) mView.findViewById(R.id.plan_total_rub);
                double totalRub = 0;
                MBasePlanPair pair = Linq.toStream(mPlanPairList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MBasePlanPair>() {
                    @Override
                    public boolean apply(MBasePlanPair t) {
                        return t.point_id.equals(p.mPoint.idu);
                    }
                });
                if (pair != null) {
                    totalRub = pair.price;
                }
                textViewTotalRub.setText(Utils.getStringDecimal(totalRub));
                textViewAcb.setBackgroundResource(p.getColorABK());
                textViewName.setText(p.mPoint.name);
                textViewAcb.setText(String.valueOf(p.mPlanACB) + "/\n" + Utils.getStringDecimal(p.getPercentAKB()) + "%");
                int i = Utils.getWidthPlanMML(p.MMLMap.size(), getContext());
                List<String> df = new ArrayList<>(p.MMLMap.keySet());
                Collections.sort(df, new Comparator<String>() {
                    @Override
                    public int compare(String lhs, String rhs) {
                        return lhs.compareTo(rhs);
                    }
                });


                for (String s : df) {
                    double mml_amount = Utils.getMmlAmount(s, mIisOld);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(i, LinearLayout.LayoutParams.MATCH_PARENT);
                    int value = (int) getContext().getResources().getDimension(R.dimen.tablemagious);
                    params.setMargins(value, 0, 0, 0);
                    TextView textView = (TextView) vi.inflate(R.layout.item_text_plan, null);
                    textView.setLayoutParams(params);
                    if (p.getColorMML(mIisOld) == R.color.d5caed) {
                        textView.setBackgroundResource(p.getColorMmlItems(s, mml_amount));//mPlanE.mml_amount
                    } else {
                        textView.setBackgroundResource(p.getColorMML(mIisOld));
                    }

                    textView.setGravity(Gravity.CENTER);

                    double d = p.MMLMap.get(s);
                    textView.setText(String.valueOf(mml_amount) + "/\n" + String.valueOf((int) d));//(int) mPlanE.mml_amount
                    textView.setPadding(10, 0, 0, 0);
                    ((LinearLayout) mView).addView(textView);
                }
                mMapPointIdView.put(key, mView);
            } else {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                mView = vi.inflate(R.layout.item_plan_itogo, null);


                Pupper plan_core_ACB = (Pupper) mView.findViewById(R.id.plan_core_ACB);
                plan_core_ACB.setPairString(R.string.akb_titul, String.valueOf(mPlanE.akb));


                LinearLayout panel1 = (LinearLayout) mView.findViewById(R.id.panel1);

                if (mIisOld == false) {
                    List<ModelMML> mmls = Configure.getSession().getList(ModelMML.class, "1=1 order by mml_name");
                    for (ModelMML mml : mmls) {
                        Pupper pupper = new Pupper(context);
                        pupper.setPairString(mml.mmlName, String.valueOf(mml.mmlPachek));
                        panel1.addView(pupper);
                    }
                } else {
                    List<ModelMMLOld> mmls = Configure.getSession().getList(ModelMMLOld.class, "1=1 order by mml_name");
                    for (ModelMMLOld mml : mmls) {
                        Pupper pupper = new Pupper(context);
                        pupper.setPairString(mml.mmlName, String.valueOf(mml.mmlPachek));
                        panel1.addView(pupper);
                    }
                }


                Pupper plan_core_MML = (Pupper) mView.findViewById(R.id.plan_core_MML);
                plan_core_MML.setPairString(R.string.mml_titul, String.valueOf(100000)); // по точке пачек

                Pupper plan_core_min_count_mml = (Pupper) mView.findViewById(R.id.plan_core_min_count_mml);
                plan_core_min_count_mml.setPairString(R.string.min_mml, String.valueOf(mPlanE.mml_group));


                Pupper plan_core__palanACB = (Pupper) mView.findViewById(R.id.plan_core__palanACB);
                plan_core__palanACB.setPairString(R.string.point_akb, String.valueOf(mPlanE.akb_points));


                LinearLayout panel2 = (LinearLayout) mView.findViewById(R.id.panel2);

                if (mIisOld == false) {
                    List<ModelMML> mmls = Configure.getSession().getList(ModelMML.class, "1=1 order by mml_name");
                    for (ModelMML mml : mmls) {
                        Pupper pupper = new Pupper(context);
                        pupper.setPairString(mml.mmlName, String.valueOf(mml.mmlPoint));
                        panel2.addView(pupper);
                    }
                } else {
                    List<ModelMMLOld> mmls = Configure.getSession().getList(ModelMMLOld.class, "1=1 order by mml_name");
                    for (ModelMMLOld mml : mmls) {
                        Pupper pupper = new Pupper(context);
                        pupper.setPairString(mml.mmlName, String.valueOf(mml.mmlPoint));
                        panel2.addView(pupper);
                    }
                }


                Pupper plan_core__palanMML = (Pupper) mView.findViewById(R.id.plan_core__palanMML);
                plan_core__palanMML.setPairString(R.string.point_mml, String.valueOf(mPlanE.mml_points));

                Pupper plan_core__total_patchek = (Pupper) mView.findViewById(R.id.plan_core__total_patchek);
                plan_core__total_patchek.setPairString(R.string.plan_sale, String.valueOf(mPlanE.amount));

                Pupper plan_total_point = (Pupper) mView.findViewById(R.id.plan_total_point);
                plan_total_point.setPairString(R.string.points_visit, String.valueOf(mList.size() - 1));
                Pupper plan_total_sale_patchek = (Pupper) mView.findViewById(R.id.plan_total_sale_patchek);

                if (mTot == 0) {
                    /////////////////////////////////////////////////story_sales_point
                    if (!mIisOld) {

                        mTot = 0;
                        for (TempPlan tempPlan : mList) {
                            mTot = mTot + tempPlan.mAllSalePachka;
                        }


                    } else {
                        List<MBasePlanPointProducts> dff = UtilsPlan.getListMPlanPointProducts(mIisOld);
                        for (MBasePlanPointProducts pp : dff) {
                            mTot = mTot + pp.amount;
                        }
                    }
                }
                {
                    double del = mPlanE.amount / 100d;
                    double percent = mTot / del;
                    plan_total_sale_patchek.setPairString(R.string.sale_patchek, String.valueOf(mTot) + " (" + Utils.getStringDecimal(percent) + "%)");
                }

                Pupper plan_total_price = (Pupper) mView.findViewById(R.id.plan_total_price);
                double totalRub = 0;
                for (TempPlan tempPlan : mList) {
                    totalRub += tempPlan.mTotalRub;
                }
                plan_total_price.setPairString(R.string.plan_saled_price, Utils.getStringDecimal(totalRub));

                Pupper plan_total_AKB = (Pupper) mView.findViewById(R.id.plan_total_AKB);
                {
                    int iakb = 0;
                    for (int i = 0; i < mList.size() - 1; i++) {
                        if (mList.get(i).getColorABK() == R.color.caede7) {
                            iakb++;
                        }
                    }
                    double del = ((double) mPlanE.akb_points / (double) 100);
                    double percent = 0d;
                    if (del != 0) {
                        percent = iakb / del;
                    }

                    plan_total_AKB.setPairString(R.string.commit_akb, String.valueOf(iakb) + " (" + Utils.getStringDecimal(percent) + "%)");
                }

                Pupper plan_total_mml = (Pupper) mView.findViewById(R.id.plan_total_mml);
                {
//                    int iakb = 0;
//                    for (int i = 0; i < mList.size() - 1; i++) {
//                        if (mList.get(i).getColorMML(mIisOld) == R.color.green_plan) {
//                            iakb++;
//                        }
//                    }
//                    double del = ((double) mPlanE.mml_points / (double) 100);
                    double percent = 0d;
//                    if (del != 0) {
//                        percent = iakb / del;
//                    }

                    ///////////////////////////// new
                    int total = (mList.size() - 1) * Utils.getMmlCount(mIisOld);
                    int totalGreen = 0;
                    percent = 0;

                    for (int i = 0; i < mList.size() - 1; i++) {
                        totalGreen = totalGreen + mList.get(i).getGreenItem(mIisOld);
                    }

                    if (totalGreen != 0) {
                        double perOne = (double) total / 100d;
                        percent = (double) totalGreen / perOne;
                    }


                    plan_total_mml.setPairString(R.string.commit_mml, String.valueOf(total) + ":" + String.valueOf(totalGreen) + " (" + Utils.getStringDecimal(percent) + "%)");
                }
                Pupper mediumLine = (Pupper) mView.findViewById(R.id.medium_line);
                {
                    double medium = 0d;
                    if (mList.size() > 0) {
                        int ii = 0;
                        for (int i = 0; i < mList.size() - 1; i++) {
                            ii = ii + mList.get(i).mMedium_line;
                        }
                        medium = ((double) ii) / ((double) (mList.size() - 1));//Средняя линия
                    }
                    mediumLine.setPairString(R.string.medium_line, String.valueOf(Utils.round(medium, 2)));
                }
                mMapPointIdView.put(key, mView);
                /////////////////////////////////////////////////// вычисление долгов
                double debt_1c = 0d;
                double debt_new = 0d;
                double debt_total = 0d;
                double debt_pachka = 0d;
                List<MPoint> mPointList = Configure.getSession().getList(MPoint.class, " debt > 0 ");
                for (MPoint point : mPointList) {
                    debt_1c = debt_1c + point.debt;
                }
                /////
                List<MDebt> debtList = Configure.getSession().getList(MDebt.class, null);

                List<MVisitStory> visitStoryList =  Configure.getSession().getList(MVisitStory.class, null);

                List<String> stringList = new ArrayList<>();
                for (MVisitStory visitStory : visitStoryList) {
                    if (visitStory.status == false) {
                        stringList.addAll(visitStory.debtList);
                    }
                }

                for (MDebt mDebt : debtList) {
                    if (stringList.contains(mDebt.order_id)) {
                        continue;
                    }
                    double sd = (mDebt.price * mDebt.amount);
                    debt_new = debt_new + sd;
                    debt_pachka = debt_pachka + mDebt.amount;
                }
                //////////////////// свои
                String debtString = Configure.getSession().getList(MOrderType.class, " index1 = 1 ").get(0).idu;
                for (MVisitStory visitStory : visitStoryList) {
                    if (visitStory.order_type_id != null && visitStory.order_type_id.equals(debtString)) {
                        for (MProduct product : visitStory.getSalesProductList()) {
                            debt_new = debt_new + product.getAmount_core() * product.price;
                            debt_pachka = debt_pachka + product.getAmount_core();
                        }
                    }
                }

                Pupper pupper1C = (Pupper) mView.findViewById(R.id.debt_1C);
                Pupper pupperNew = (Pupper) mView.findViewById(R.id.debt_new);
                Pupper pupperTotal = (Pupper) mView.findViewById(R.id.debt_total);
                Pupper pupperTotalPachka = (Pupper) mView.findViewById(R.id.debt_total_packa);
                pupperTotalPachka.setPairString(getContext().getString(R.string.erewr), Utils.getStringDecimal(debt_pachka));

                if (debt_1c == 0) {
                    pupper1C.setVisibility(View.GONE);
                    pupperNew.setVisibility(View.GONE);
                    pupperTotal.setPairString(getContext().getString(R.string.itogodolg), Utils.getStringDecimal(debt_new));
                } else {
                    pupper1C.setPairString(getContext().getString(R.string.retert), Utils.getStringDecimal(debt_1c));
                    pupperNew.setPairString(getContext().getString(R.string.dsfffff), Utils.getStringDecimal(debt_new));
                    pupperTotal.setPairString(getContext().getString(R.string.itogodolg), Utils.getStringDecimal(debt_new + debt_1c));
                }
                if (mIisOld) {
                    pupperTotalPachka.setVisibility(View.GONE);
                    pupper1C.setVisibility(View.GONE);
                    pupperNew.setVisibility(View.GONE);
                    pupperTotal.setVisibility(View.GONE);
                }
                final View finalMView = mView;
                final Button t2 = (Button) mView.findViewById(R.id.link_percent);

                final boolean[] isShow = {true};
                t2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isShow[0] == false) return;
                        isShow[0] = false;
                        DialogEditPercentGroup dialog = new DialogEditPercentGroup();
                        dialog.setIsOld(mIisOld);
                        dialog.setmIAction(new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                printGroup(finalMView);
                                mActivity.recreate();
                            }
                        });

                        dialog.setmIActionResume(new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                isShow[0] = true;
                            }
                        });
                        dialog.show(((Plan) mActivity).getSupportFragmentManager(), "455sdsd");
                    }
                });
                printGroup(mView);


                {

                    Map<String, Integer> map = new HashMap<>();
                    for (String s : Utils.getStringMml(mIisOld)) {
                        map.put(s, 0);
                    }


                //    int total = (mList.size() - 1) * Utils.getMmlCount(mIisOld);
                    for (int i = 0; i < mList.size() - 1; i++) {
                        mList.get(i).addMmlPlan(map, mIisOld);
                    }
                    LinearLayout panel3 = (LinearLayout) mView.findViewById(R.id.panel3);

                    List<String> list = new ArrayList<>(map.keySet());
                    Collections.sort(list, new Comparator<String>() {
                        @Override
                        public int compare(String lhs, String rhs) {
                            return lhs.compareTo(rhs);
                        }
                    });
                    for (String s : list) {
                        int d = map.get(s);
                        Pupper pupper = new Pupper(context);
                        double percent = 0d;

                        double t1 = Utils.getMmlPoint(s, mIisOld);
                        if (t1 == 0) continue;
                        double one = t1 / 100d;
                        percent = (double) d / one;

                        pupper.setPairString(s, String.valueOf(t1) + "/" + String.valueOf((double) d) + " (" + Utils.getStringDecimal(percent) + " %)");
                        panel3.addView(pupper);
                    }
                }


            }
        }
        return mView;
    }

    private void printGroup(View mView) {
        try {
            BuilderGroups.builder(mIisOld, mView, getContext(), mTot);
        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FStock:" + Utils.getErrorTrace(ex),  (MyApplication) mActivity.getApplication());
        }
    }
}
