package com.example.user.omsk.history;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.LoggerE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitStoryPointDay;
import com.example.user.omsk.models.TempComonProduct;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.senders.IActionResponse;
import com.example.user.omsk.senders.TempVisitStory;
import com.example.user.omsk.senders.UtilsSender;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SenderHistoryDay {

    public static org.apache.log4j.Logger log = Logger.getLogger(SenderHistoryDay.class);
    private Activity mActivity;
    private Date mDateStart;
    private Date mDateFinish;
    private Settings mSettings;
    private ISenderHistoryDay mISenderHistoryDay;
    private List<MVisitStoryPointDay> mStoryVisitPointDays;

    public void send(Activity activity, Date dateStart, Date dateFinish, Settings mSettings, ISenderHistoryDay iSenderHistoryDay) {

        this.mActivity = activity;

        this.mDateStart = dateStart;
        this.mDateFinish = dateFinish;
        this.mSettings = mSettings;
        this.mISenderHistoryDay = iSenderHistoryDay;
        new SenderJobHistoryDey().execute();

    }

    interface ISenderHistoryDay {
        void activate(List<MVisitStoryPointDay> list);
    }

    private class SenderJobHistoryDey extends AsyncTask<Void, Void, String> {


        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(mActivity, mSettings)) {
                cancel(true);
                Toast.makeText(mActivity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(mActivity, mActivity.getString(R.string.get_data), new IActionE<View>() {
                @Override
                public void action(View v) {
                }
            });
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {

            if (isCancelled()) return;

            if (dialog != null) {
                dialog.cancel();
            }
            mISenderHistoryDay.activate(mStoryVisitPointDays);

            if (aBoolean != null) {

                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/route_history_client/:  " + aBoolean;
                Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
                log.error(msg);                // new SenderErrorPOST().send(mActivity, msg, Settings.core());

            }


        }


        @Override
        protected String doInBackground(Void... params) {

            if (isCancelled()) return null;

            final String[] result = {null};

            String str = null;
            try {
                str = Utils.HTTP + mSettings.url + "/route_history_client/?guid=" + URLEncoder.encode(SettingsUser.core().getUuidDevice(),
                        "UTF-8") + "&start=" + String.valueOf(mDateStart.getTime() / 1000) + "&finish=" + String.valueOf(mDateFinish.getTime() / 1000);
            } catch (UnsupportedEncodingException e) {

                result[0] = e.getMessage();
                return result[0];
            }


            result[0] = UtilsSender.getRequest((MyApplication) mActivity.getApplication(), str, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {


                    if (str.toLowerCase().equals("ok")) {
                    } else {
                        App(str);
                    }


                }
            });
            return result[0];


        }

        private void App(String res) {

            LoggerE.logI(res);
            Gson sd3 = Utils.getGson();
            Type listType = new TypeToken<ArrayList<TempStoryDay>>() {
            }.getType();

            List<TempStoryDay> list = sd3.fromJson(res, listType);

            mStoryVisitPointDays = new ArrayList<>();

            ISession ses = Configure.getSession();
            for (TempStoryDay tempStoryDay : list) {
                MVisitStoryPointDay day = TempStoryDay.getMVisitStoryPointDay(tempStoryDay, ses);
                mStoryVisitPointDays.add(day);
            }

            ses.execSQL( " delete from  visit_present where visit_finish > 0");
            for (MVisitStoryPointDay ss : mStoryVisitPointDays) {
                ses.insert(ss);
                for (MVisitPresent gift : ss.gifts) {
                    gift.visit_finish=ss.finish;

                    ses.insert(gift);
                }
            }

        }
    }

}

class TempStoryDay {
    private boolean status;
    private double refund;
    private String comment;
    private String point_id;
    private String result;
    private int date;

    private double payment;
    private String member_action;
    private String trader;
    private int date_start;

    private String payment_type;
    private String point_name;

    private List<TempComonProduct> products;
    private List<TempComonProduct> free_products = new ArrayList<>();
    private List<TempComonProduct> promo_products = new ArrayList<>();
    private ArrayList<TempVisitStory.TempRefunds> refunds = new ArrayList<>();

    public List<MVisitPresent> gifts=new ArrayList<>();

    TempStoryDay() {
        products = new ArrayList<>();
    }

    static MVisitStoryPointDay getMVisitStoryPointDay(TempStoryDay d, ISession ses) {

        MVisitStoryPointDay t = new MVisitStoryPointDay();
        t.gifts=d.gifts;
        t.refund = d.refund;
        t.status = d.status;
        t.comment = d.comment;
        t.point_id = d.point_id;
        t.visit_result_id = d.result;
        t.finish = d.date;
        t.start = d.date_start;
        t.payment = d.payment;
        t.member_action = d.member_action;
        t.trader = d.trader;
        t.order_type_id = d.payment_type;
        t.point_name = d.point_name;

        for (TempComonProduct product : d.products) {

            MProduct p = ses.get(MProduct.class, product.id);
            if (p == null) {
                p=new MProduct();
                p.name = "Не найден";
            }
            p.setAmountCore(product.amount);
            p.price = product.price;
            t.getSalesProductList().add(p);
        }

        for (TempComonProduct product : d.free_products) {
            MProduct p = ses.get(MProduct.class, product.id);
            if (p == null) {
                p=new MProduct();
                p.name = "Не найден";
            }
            p.rule_id = product.promo;
            p.setAmountCore(product.amount);
            p.price = product.price;
            t.getFreeProductList().add(p);
        }
        for (TempComonProduct product : d.promo_products) {
            MProduct p = ses.get(MProduct.class, product.id);
            if (p == null) {
                p=new MProduct();
                p.name = "Не найден";
            }
            p.rule_id = product.promo;
            p.setAmountCore(product.amount);
            p.price = product.price;
            t.getActionProductList().add(p);
        }



        t.setDupletCore(TempVisitStory.getDuplet(d.refunds));
        return t;
    }
}