package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.kassa.MChecks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MChecksData implements Serializable {

    public final static int TYPE_CHECK_CACH = 0;
    public final static int TYPE_CHECK_DEBT = 1;
    public final static int TYPE_CHECK_RETURN = 2;
    public final static int TYPE_CHECK_CONSTRUCTOR = 3;
    public final static int TYPE_CHECK_RETURN_CONSTRUCTOR = 4;
    // сылка на хозяина при показе в работе с чеками
    public transient MChecks mChecksLink;
    public String checkUUIDDelete;
    public String sms;
    public String email;
    public int notify;

    // для частичных чеков;
    public String id_partial;

    public MChecksData() {

        idu = Utils.getUuid();
    }


    public String idu;
    public transient String errorText;
    public int checkNumberDelete;
    public Date date = null;
    public Date date_delete = null;
    /**
     *
     */
    public String visit_id;
    /**
     *
     */
//    public int type_check;
    /**
     * лист продуктов по чеку
     */
    public List<MProductBase> productList = new ArrayList<>();
    /**
     * номер чека
     */
    public int checkNumber;
    /**
     * номер ккм
     */
    public String kmmNumber;
    /**
     * производился ли z отчет
     */
    public boolean is_zreport;
    /**
     * новер документа из кассового апп
     */
    public int docNumber;
    /**
     * Когда отдали в долг
     */
    public int  debt_data ;
    /**
     * индентификатор одера ид из таблицы MDebt
     */
    public String order_debt_id;
    /**
     * сделали ли чеку анулирование
     */
    public boolean isReturn;
    /**
     * чек на продажу за нал или чек за возврат долга
     */
    public int type_check;
    // последний ли это чек
    public boolean isLast;

    public double totalAmount;

    public double totalPrice;

    public MChecksData clone() {
        //todo изменить ссылку
        MChecksData ss = new MChecksData();
        ss.checkUUIDDelete = this.checkUUIDDelete;
        ss.errorText = this.errorText;
        ss.checkNumberDelete = this.checkNumberDelete;
        ss.date = this.date;
        ss.idu = this.idu;
        ss.id_partial = this.id_partial;
        ss.date_delete = this.date_delete;
        ss.visit_id = this.visit_id;
        ss.productList = this.productList;
        ss.checkNumber = this.checkNumber;
        ss.kmmNumber = this.kmmNumber;
        ss.is_zreport = this.is_zreport;
        ss.docNumber = this.docNumber;
        ss.debt_data = this.debt_data;
        ss.order_debt_id = this.order_debt_id;
        ss.isReturn = this.isReturn;
        ss.type_check = this.type_check;
        ss.isLast = this.isLast;
        return ss;
    }

}
