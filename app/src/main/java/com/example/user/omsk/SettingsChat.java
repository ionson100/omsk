package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;
import java.util.Date;

public class SettingsChat implements Serializable {

    public static SettingsChat core() {
        return (SettingsChat) Reanimator.get(SettingsChat.class);
    }

    public static void save() {
        Reanimator.save(SettingsChat.class);
    }

    private volatile Date last_date_chat_new_22 = null;

    public Date getLast_date_chat_new_22() {
        return last_date_chat_new_22;
    }

    public void setLast_date_chat_new_22(Date value) {
        last_date_chat_new_22 = value;
        SettingsChat.save();
    }
}
