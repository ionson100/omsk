package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * строит итоговую таблицу продаж по группам в итоговой форме отчета
 * Created by USER on 17.02.2017.
 */

public class BuilderGroups {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BuilderGroups.class);
    public static Map<String, GroupDouble> getMap() {
        Gson gson = Utils.getGson();
        Map<String, GroupDouble> actionList = gson.fromJson(Settings.core().jsonGroup, new TypeToken<Map<String, GroupDouble>>() {
        }.getType());
        return actionList;
    }


    public static void builder(boolean isOld, View view, Context context, double total) {

        try {
            View panel = view.findViewById(R.id.group_panel);
            View panelItems = view.findViewById(R.id.panel_items_groups);
            ((LinearLayout) panelItems).removeAllViews();
            Map<String, GroupDouble> plansMap = getMap();
            PlaneBase planeBase = null;
            ISession session = Configure.getSession();
            if (isOld) {
                List<PlanEOld> planEOld = session.getList(PlanEOld.class, null);
                if (planEOld.size() > 0) {
                    planeBase = planEOld.get(0);
                }

            } else {
                List<PlanE> planE = session.getList(PlanE.class, null);
                if (planE.size() > 0) {
                    ;
                    planeBase = planE.get(0);
                }

            }
            if (plansMap.size() > 0) {
                panel.setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.link_percent).setVisibility(View.GONE);
                panel.setVisibility(View.GONE);
                return;
            }

            List<GroupDouble> sdd = new ArrayList<>();

            for (Map.Entry<String, GroupDouble> ss : plansMap.entrySet()) {
                GroupDouble groupDouble =ss.getValue();
                groupDouble.grup_id = ss.getKey();
                sdd.add(groupDouble);
            }


            Collections.sort(sdd, new Comparator<GroupDouble>() {
                @Override
                public int compare(GroupDouble lhs, GroupDouble rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });

            FactoryPlan factoryPlan = new FactoryPlan();
            for (GroupDouble s : sdd) {
                View item = LayoutInflater.from(context).inflate(R.layout.item_plan_groups, null);
                TextView name = (TextView) item.findViewById(R.id.name_group);
                name.setText(s.name);
                TextView plan = (TextView) item.findViewById(R.id.plan_group);
                int ss = (int) ((planeBase.amount / 100) * s.percent);
                plan.setText(String.valueOf((int) Math.round(s.percent)) + "/" + String.valueOf(ss));
                double salePach = factoryPlan.getGroupProductAmount(isOld, s.grup_id);
                TextView fact = (TextView) item.findViewById(R.id.fact_group);
                double res = salePach / (total / 100);
                double sss = (salePach / (ss)) * 100;
                fact.setText(String.valueOf((int) Math.round(res)) + "/" + String.valueOf((int) salePach) + " - " + String.valueOf((int) sss) + "%");
                ((LinearLayout) panelItems).addView(item);
            }
        } catch (Exception ignored) {
            log.error(ignored);
        }
    }
}
