package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MChat;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MDistributionchannel;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MPointType;
import com.example.user.omsk.models.MPresent;
import com.example.user.omsk.models.MPrice;
import com.example.user.omsk.models.MPriceType;
import com.example.user.omsk.models.MPriority;
import com.example.user.omsk.models.MPriorityProduct;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductConstructor;
import com.example.user.omsk.models.MProductGroup;
import com.example.user.omsk.models.MPromotion;
import com.example.user.omsk.models.MTask;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.action.TempFreePresent;
import com.example.user.omsk.auto.Auto;
import com.example.user.omsk.plan.PlanE;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SynchroniseJsonObject {

    public Auto auto;
    public String city;
    public String ofd_url = "ofdp.platformaofd.ru";
    public int ofd_port = 21101;
    public String dadata = "daaa944d66e9f24f1f3e4d0ea926dfdc2da01e97";

    public String user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36";
    public List<String> route;
    public String ipping = "104.219.251.211";
    public Date current_time = null;
    public double latitude;
    public double longitude;
    public String password = "312312";
    public List<MPriority> priorities;
    public int last_update;
    public List<MVisitResults> visit_results;
    public List<MPromotion> promotions;
    public List<MDistributionchannel> channels;
    public List<MPoint> points;
    public List<MProduct> products;
    public List<MProductConstructor> fake_products;
    public List<MProductGroup> product_groups;
    public List<MPrice> prices;
    public List<MPointType> point_types;
    public List<MPriceType> price_types;
    public List<MOrderType> order_types;
    public List<MAction> promos = new ArrayList<>();
    public List<MChat> messages;
    public List<MPriorityProduct> current_priorities;
    public List<TempFreePresent> fp_reasons = new ArrayList<>();
    public List<MDebt> debts = new ArrayList<>();
    //поле которое показывает что этот пользователь торгует из стационарного ларька (true)
    //public boolean stationary;
    // поле которое характеризует цену  сигарет стационарного ларька
    public String prise_type;
    public List<MTask> tasks = new ArrayList<>();
    @SerializedName("plan")
    public PlanE plan;
    public int status;
    public String version;

    public List<MPresent> gifts=new ArrayList<>();
}

