package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogNetworkConnect;
import com.example.user.omsk.setting.Reanimator;
import com.example.user.omsk.setting.Settingion;
import com.example.user.omsk.setting.iListenerСhanges;


public class FSetting extends Fragment {

    private Settings mSettings;
    private View mView;

    public FSetting() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_setting, container, false);
        Transceiver.send(TransceiveStaticVariables.MAP, null);
        mSettings = Settings.core();
        try {
            final Settingion mSettingsEngine = (Settingion) mView.findViewById(R.id.setting_panel);
            mSettingsEngine.setModelClass(Settings.class, getActivity());


            Reanimator.onSetListenerСhanges(new iListenerСhanges() {
                @Override
                public void OnCallListen(Class aClass, String fieldName, Object oldValue, Object newValue) {
//                    if(aClass==Settings.class&&fieldName.equals("useYandexGPS")){
//                        ((MainActivity)getActivity()).onResumeCoore();
//                    }
                }
            });


            EditText et_latitude = (EditText) mView.findViewById(R.id.e_latitude);
            EditText et_longitude = (EditText) mView.findViewById(R.id.e_longitude);
            et_latitude.setText(Double.toString(mSettings.getStartLatitude()));
            et_longitude.setText(Double.toString(mSettings.getStartLongitude()));
            et_latitude.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    double re = 0;
                    try {
                        re = Double.parseDouble(s.toString().replace(",", "."));

                    } catch (Exception ignored) {

                    }
                    mSettings.setStartLatitude(re);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            et_longitude.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    double re = 0;
                    try {
                        re = Double.parseDouble(s.toString().replace(",", "."));
                    } catch (Exception ignore) {
                    }
                    mSettings.setStartLongitude(re);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            mView.findViewById(R.id.btMap).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.EDIT_MAP_SETTINGS, getActivity());
                }
            });

            EditText editText = (EditText) mView.findViewById(R.id.price_border);
            editText.setText(String.valueOf(mSettings.maxSummPrice));
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    double i = 0;
                    try {
                        i = Double.parseDouble(s.toString());
                    } catch (Exception ex) {

                    }
                    mSettings.maxSummPrice = i;
                    mSettings.save();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            publicatorVersionProg();

            mView.findViewById(R.id.setting_wifi).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getActivity().startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                }
            });

            mView.findViewById(R.id.setting_gps).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });

            mView.findViewById(R.id.setting_date).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
                }
            });


            mView.findViewById(R.id.setting_internet).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                }
            });

            mView.findViewById(R.id.setting_bluetooth).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS));
                }
            });

            mView.findViewById(R.id.setting_connect).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogNetworkConnect connect = new DialogNetworkConnect();
                    connect.show(getActivity().getSupportFragmentManager(), "zsasd");
                }
            });


        } catch (Exception ex) {
            Utils.sendMessage("FSettings:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;
    }

    private void publicatorVersionProg() {
        TextView versionLocal = (TextView) mView.findViewById(R.id.version_local);
        TextView versionServer = (TextView) mView.findViewById(R.id.version_server);
        versionLocal.setText("local: " + mSettings.currentVersionName);
        versionServer.setText("server: " + mSettings.serverVersionName);
    }

    @Override
    public void onDestroy() {
        Reanimator.onSetListenerСhanges(null);
        super.onDestroy();

    }
}
