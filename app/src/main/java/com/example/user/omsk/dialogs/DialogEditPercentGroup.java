package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.plan.GroupDouble;
import com.example.user.omsk.plan.PlanE;
import com.example.user.omsk.plan.PlanEOld;
import com.example.user.omsk.plan.PlaneBase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class DialogEditPercentGroup extends DialogFragment {

    private boolean mIsOld;
    private IActionE mIAction;
    private IActionE mIActionResume;
    private List<EditText> mEditTextList = new ArrayList<>();

    public void setmIActionResume(IActionE mIActionResume) {
        this.mIActionResume = mIActionResume;
    }

    public void setmIAction(IActionE mIAction) {
        this.mIAction = mIAction;
    }

    public void setIsOld(boolean isOld) {
        this.mIsOld = isOld;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;

        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit_percent_group, null);
        ((TextView) mView.findViewById(R.id.title_dalog)).setText(getString(R.string.jksdsd));
        final LinearLayout panel = (LinearLayout) mView.findViewById(R.id.panel_edit_percent);
        Gson gson = Utils.getGson();
        final Map<String, GroupDouble> actionList = gson.fromJson(Settings.core().jsonGroup, new TypeToken<Map<String, GroupDouble>>() {
        }.getType());
        List<String> list = new ArrayList<>(actionList.keySet());
        Collections.sort(list);
        for (String s : list) {

            final GroupDouble groupDouble = actionList.get(s);
            View item = LayoutInflater.from(getActivity()).inflate(R.layout.item_edit_percent_group, null);
            TextView textView = (TextView) item.findViewById(R.id.edit_name);
            EditText editText = (EditText) item.findViewById(R.id.edit_percent);
            editText.setTag(s);
            mEditTextList.add(editText);
            textView.setText(groupDouble.name);
            editText.setText(String.valueOf((int) groupDouble.percent));
            panel.addView(item);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int i = 0;
                    try {
                        i = Integer.parseInt(s.toString());
                    } catch (Exception ignored) {
                    }
                    groupDouble.percent = i;

                }

                @Override
                public void afterTextChanged(Editable s) {

                    for (EditText text : mEditTextList) {
                        text.setError(null);
                    }
                }
            });


        }


        builder.setView(mView);

        mView.findViewById(R.id.edit_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                getActivity().recreate();
            }
        });

        mView.findViewById(R.id.edit_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double re = 0;

                for (Map.Entry<String, GroupDouble> ss : actionList.entrySet()) {
                    re = re + ss.getValue().percent;
                }

                if (re == 100) {
                    String string = Utils.getGson().toJson(actionList);
                    Settings.core().jsonGroup = string;
                    Settings.save();
                    dismiss();
                    if (mIAction != null) {
                        mIAction.action(null);
                    }
                } else {
                    for (EditText text : mEditTextList) {
                        text.setError(getString(R.string.lkwe));
                    }
                }
            }
        });

        TextView t2 = (TextView) mView.findViewById(R.id.link_percent_default);
        t2.setMovementMethod(LinkMovementMethod.getInstance());
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaneBase planeBase = null;
                if (mIsOld == false) {
                    List<PlanE> planEs = Configure.getSession().getList(PlanE.class, null);
                    if (planEs.size() > 0) {
                        planeBase = planEs.get(0);
                    }

                } else {
                    List<PlanEOld> planEOlds = Configure.getSession().getList(PlanEOld.class, null);
                    if (planEOlds.size() > 0) {
                        planeBase = planEOlds.get(0);
                    }

                }
                if (planeBase == null) return;
                for (String s : planeBase.plansMap.keySet()) {
                    for (EditText editText : mEditTextList) {
                        String string = (String) editText.getTag();
                        if (string.equals(s)) {
                            editText.setText(String.valueOf((int) planeBase.plansMap.get(s).percent));
                            break;
                        }
                    }
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mIActionResume.action(null);
    }
}
