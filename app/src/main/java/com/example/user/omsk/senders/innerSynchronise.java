package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;

import com.example.user.omsk.FactoryColor;
import com.example.user.omsk.LoggerE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsChat;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.SynchroniseJsonObject;
import com.example.user.omsk.UserNamesSettings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.action.TempFreePresent;
import com.example.user.omsk.auto.AutoNummber;
import com.example.user.omsk.auto.MAuto;
import com.example.user.omsk.history.History;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MChat;
import com.example.user.omsk.models.MComments;
import com.example.user.omsk.models.MContact;
import com.example.user.omsk.models.MContactor;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MDistributionchannel;
import com.example.user.omsk.models.MFreePresentTYpes;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MPointType;
import com.example.user.omsk.models.MPresent;
import com.example.user.omsk.models.MPrice;
import com.example.user.omsk.models.MPriceType;
import com.example.user.omsk.models.MPriority;
import com.example.user.omsk.models.MPriorityProduct;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductConstructor;
import com.example.user.omsk.models.MProductGroup;
import com.example.user.omsk.models.MPromotion;
import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.models.MSaleRows;
import com.example.user.omsk.models.MStockRows;
import com.example.user.omsk.models.MTask;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.orm2.Logger;
import com.example.user.omsk.plan.GroupDouble;
import com.example.user.omsk.plan.SenderPlanPointsProductOld;
import com.example.user.omsk.refill.SettingsAddinStock;
import com.example.user.omsk.route.SettingsRoute;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class innerSynchronise {


    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderPlanPointsProductOld.class);
    private Settings mSettings;

    innerSynchronise(Settings settings) {
        this.mSettings = settings;
    }

    public void action(String msg, boolean isDeleteAll, boolean isUserAvtorise, SenderSynchronizationPOST.IActionPercent iActionPercent, MainActivity activity) {//
        final ISession ses = Configure.getSession();

        iActionPercent.action(11);

        SynchroniseJsonObject res = null;
        try {
            ses.beginTransaction();
            //ses.deleteTable("advertising;");
            ses.deleteTable("chat");
            ses.deleteTable("contact");
            ses.deleteTable("contactor");
            ses.deleteTable("distributionchannel");
            ses.deleteTable("order_type");
            ses.deleteTable("point_type");
            ses.deleteTable("price");
            ses.deleteTable("price_types");
            ses.deleteTable("product");
            ses.deleteTable("product_group");
            ses.deleteTable("romotion");
            ses.deleteTable("sale_rows");
            ses.deleteTable("visit_results");
            ses.deleteTable("priority");
            ses.deleteTable("priority_product");
            ses.deleteTable("agent");
            ses.deleteTable("point");
            ses.deleteTable("route");
            ses.deleteTable("comments");
            ses.deleteTable("plan");
            ses.deleteTable("present_types");
            ses.deleteTable("action");
            ses.deleteTable("stock_rows");
            ses.deleteTable("task");
            ses.deleteTable("story_visit_point");
            ses.deleteTable("product_constructor");
            ses.deleteTable("story_new_order");

            ////////////////////////
            //Utils.setSalesProducts();
            Utils.setSalesProducts();


            History.mStartStoryVisit = null;


            if (isDeleteAll) {
                SettingsAddinStock s = SettingsAddinStock.core();
                s.mLastkDateNew_22 = null;
                s.mStatusSender = false;
                s.mLastListNew.clear();
                s.save();

                ses.deleteTable("visit_present");
                ses.deleteTable("present");
                ses.deleteTable("dolgi");

                //////////////////////////////
                ses.deleteTable("start_route");
                MTemplatePropety mTemplatePropety = new MTemplatePropety();
                mTemplatePropety.date = Utils.curDate();
                ses.insert(mTemplatePropety);
                ////////////////////////////////

                ses.deleteTable("summ_kkm");
                ses.deleteTable("temp_plan");
                ses.deleteTable("sale_item_product");
                //ses.deleteTable("sale_item");
                //ses.deleteTable("error");
                ses.deleteTable("task_point");
                SettingsRoute.core().clear().save();
                ses.deleteTable("photo");
                ses.deleteTable("video");
                ses.deleteTable("plan_pair");
                ses.deleteTable("plan_points");
                ses.deleteTable("plan_mml");
                ses.deleteTable("old_plan_pair");
                ses.deleteTable("old_plan_points");
                ses.deleteTable("old_plan");
                ses.deleteTable("old_plan_mml");
                ses.deleteTable("visit_story");
                ses.deleteTable("checks");

            } else {
                List<MTemplatePropety> mTemplatePropetyList = Configure.getSession().getList(MTemplatePropety.class, null);
                if (mTemplatePropetyList.size() > 0) {
                    MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
                    mTemplatePropety.successSynchronize = false;
                    Configure.getSession().update(mTemplatePropety);
                }
            }

            iActionPercent.action(15);
            LoggerE.logI(String.valueOf(System.currentTimeMillis()));
            Gson sd3 = Utils.getGson();
            res = sd3.fromJson(msg, SynchroniseJsonObject.class);


            ////////////////////////////////////update auto;


            if (isDeleteAll == true && isUserAvtorise == false) {

                if (res.auto != null) {

                    mSettings.uid_auto = "";
                    mSettings.useAuto = res.auto.waybill;
                    ses.deleteTable("auto", "  date2<>0 and is_sender = 0");
                    if (mSettings.useAuto) {
                        MAuto mAuto = new MAuto();
                        mAuto.useAuto = res.auto.waybill;
                        mAuto.lastKm = res.auto.last_km;
                        if (res.auto.auto != null) {

                            for (Object[] objects : res.auto.auto) {
                                mAuto.autoNummbers.add(new AutoNummber((String) objects[0], (double) objects[1], (double) objects[2]));
                            }
                            mSettings.uid_auto = mAuto.uid;
                            ses.insert(mAuto);
                        }
                        if (mAuto.autoNummbers.size() == 0) {
                            mSettings.useAuto = false;
                            mSettings.uid_auto = "";
                        } else {
                            mAuto.autoNummbers.add(0, new AutoNummber("Выбрать номер.", 0, 0));
                            ses.update(mAuto);
                        }
                    }

                } else {
                    mSettings.useAuto = false;
                    mSettings.uid_auto = "";
                }
            }


            mSettings.city = "(" + res.city + ")";
            mSettings.password = res.password;
            mSettings.dadata = res.dadata;

            Logger.logI(String.valueOf(System.currentTimeMillis()));
            iActionPercent.action(20);

            updateOFD(res);
            iActionPercent.action(25);
            updateSettingsKassa(res);
            updateFakeProducts(res.fake_products, ses);
            updateIpPing(res.ipping);
            if (isDeleteAll) {
                updateDebt(res, ses);
            }
            iActionPercent.action(30);
            updateActions(res, ses);
            //updateStationaryShop(res);
            updateFreePresentTypes(res, ses);
            updateTask(res.tasks, ses);
            iActionPercent.action(45);
            updateUserAgent(res.user_agent);
            updateStatusUser(res.status);
            updateCurrentTime(res);
            updatePlan(res, ses);
            updateGeoDefault(res);
            iActionPercent.action(50);
            updateVersion(res);
            updatePriorities(res.priorities, ses);
            updateVisitTypes(res.visit_results, ses);
            updatePromotion(res.promotions, ses);
            updateChannel(res.channels, ses);
            iActionPercent.action(65);
            updateProduct(res.products, ses);//bulk
            updateProductGroup(res.product_groups, ses);
            updatePointType(res.point_types, ses);
            updatePriceType(res.price_types, ses);
            updateOrderType(res.order_types, ses);
            updatePrice(res.prices, ses);//bulk
            updateContactCore(res.points, ses);
            iActionPercent.action(70);
            updateContragentCore(res.points, ses);
            updatePoint(res.points, ses);//bulk
            mSettings.setLastSynchronization(res.last_update);
            updateSaleRows(res.points, ses);
            updateStockRows(res.points, ses);
            updateChat(res.messages, ses);
            SettingsChat.core().setLast_date_chat_new_22(Utils.curDate());
            updatePriorityProduct(res.current_priorities, ses);
            updateRoute(res.route, ses);

            updatePresetn(res.gifts, ses);


            iActionPercent.action(85);
            updateComments(res.points, ses);
            FactoryColor.refreshData();
            ses.commitTransaction();
            ses.endTransaction();
            iActionPercent.action(99);

            Settings.save();

            LoggerE.logI(String.valueOf(System.currentTimeMillis()));

        } catch (Exception ex) {
            ses.endTransaction();
            log.error(ex);
            throw new RuntimeException(ex.getMessage());

        } finally {

            activity.sendBroadcast(new Intent(MainActivity.FAB));
            res = null;
            System.gc();
        }
    }

    private void updatePresetn(List<MPresent> gifts, ISession ses) {
        for (MPresent gift : gifts) {
            ses.insert(gift);
        }
    }

    private void updateOFD(SynchroniseJsonObject res) {
        mSettings.ofd_port = res.ofd_port;
        mSettings.ofd_url = res.ofd_url;
    }

    private void updateSettingsKassa(SynchroniseJsonObject res) {
        SettingsKassa settingsKassa = SettingsKassa.core();
        settingsKassa.constructorList.clear();
        SettingsKassa.save();
    }

    private void updateFakeProducts(List<MProductConstructor> fake_products, ISession ses) {
        if (fake_products == null || fake_products.size() == 0) return;
        Configure.bulk(MProductConstructor.class, fake_products, ses);
    }

    private void updateIpPing(String ipping) {
        mSettings.ipPing = ipping;
    }

    private void updateDebt(SynchroniseJsonObject res, ISession ses) {
        Configure.bulk(MDebt.class, res.debts, ses);
    }

    private void updateActions(SynchroniseJsonObject res, ISession iSession) {

        for (MAction promo : res.promos) {
            iSession.insert(promo);
        }
    }


    private void updateFreePresentTypes(SynchroniseJsonObject res, ISession ses) {

        for (TempFreePresent fp_reason : res.fp_reasons) {
            MFreePresentTYpes mPresentTYpes = new MFreePresentTYpes(fp_reason.id, fp_reason.name, fp_reason.name, fp_reason.archive);
            Configure.getSession().insert(mPresentTYpes);
        }
    }

    private void updateTask(List<MTask> tasks, ISession ses) {
        List<MTask> mTasks = ses.getList(MTask.class, null);
        for (final MTask task : tasks) {
            MTask res = Linq.toStream(mTasks).firstOrDefault(new Predicate<MTask>() {
                @Override
                public boolean apply(MTask t) {
                    return t.idu.equals(task.idu);
                }
            });
            if (res == null) {
                ses.insert(task);
            } else {
                res.message = task.message;
                ses.update(res);
            }
        }
    }

    private void updateUserAgent(String user_agent) {
        mSettings.user_agent = user_agent;
    }

    private void updateStatusUser(int status) {
        UserNamesSettings userNamesSettings = UserNamesSettings.core();
        userNamesSettings.status = status;
        UserNamesSettings.save();
    }


    private void updatePlan(SynchroniseJsonObject res, ISession ses) {

        if (res.plan != null) {

            ses.insert(res.plan);
            res.plan.insertMML(ses);
            if (mSettings.jsonGroup == null || mSettings.jsonGroup.length() == 0 || res.plan.plansMap.size() == 0) {
                mSettings.jsonGroup = Utils.getGson().toJson(res.plan.plansMap);
            } else {
                Gson gson = Utils.getGson();
                Map<String, GroupDouble> actionList = gson.fromJson(mSettings.jsonGroup, new TypeToken<Map<String, GroupDouble>>() {
                }.getType());
                if (actionList.size() != res.plan.plansMap.size()) {
                    mSettings.jsonGroup = Utils.getGson().toJson(res.plan.plansMap);
                } else {
                    List<String> listsave = new ArrayList<>(actionList.keySet());
                    List<String> listserver = new ArrayList<>(res.plan.plansMap.keySet());
                    for (String s : listsave) {
                        if (!listserver.contains(s)) {
                            mSettings.jsonGroup = Utils.getGson().toJson(res.plan.plansMap);
                            return;
                        }
                    }
                    for (String s : listserver) {
                        actionList.get(s).name = res.plan.plansMap.get(s).name;
                    }
                    mSettings.jsonGroup = Utils.getGson().toJson(actionList);
                }
            }
        }
    }

    private void updateCurrentTime(SynchroniseJsonObject res) {
        mSettings.current_time_22 = res.current_time;
    }

    private void updateGeoDefault(SynchroniseJsonObject res) {
        if (res.latitude == 0 || res.longitude == 0) return;
        if (mSettings.getStartLatitude() != Utils.LATITUDE || mSettings.getStartLongitude() != Utils.LONGITUDE)
            return;
        mSettings.setStartLatitude(res.latitude);
        mSettings.setStartLongitude(res.longitude);
    }

    private void updateComments(List<MPoint> points, ISession ses) {
        if (points == null) return;
        for (MPoint point : points) {
            if (point.mComments == null) continue;
            for (MComments mComment : point.mComments) {
                mComment.point_id = point.get_id();
                ses.insert(mComment);
            }
        }
    }

    private void updateRoute(List<String> route, ISession ses) {
        if (route == null) return;
        for (String s : route) {
            MRoute d = new MRoute();
            d.point_id = s;
            ses.insert(d);
        }
    }

    private void updateVersion(SynchroniseJsonObject res) {
        mSettings.serverVersionName = res.version;
    }

    private void updatePriorityProduct(List<MPriorityProduct> currentPriorities, ISession ses) {
        if (currentPriorities == null || currentPriorities.size() == 0) return;
        Configure.bulk(MPriorityProduct.class, currentPriorities, ses);
    }

    private void updatePriorities(List<MPriority> priorities, ISession ses) {
        if (priorities == null || priorities.size() == 0) return;
        Configure.bulk(MPriority.class, priorities, ses);
    }


    private void updateChat(List<MChat> messages, ISession ses) {
        if (messages == null) return;
        Configure.bulk(MChat.class, messages, ses);
    }

    private void updateStockRows(List<MPoint> points, ISession ses) {
        for (MPoint point : points) {
            if (point.stock_rows == null || point.stock_rows.size() == 0) continue;

            for (MStockRows stock_row : point.stock_rows) {
                stock_row.point_id = point.idu;
                ses.insert(stock_row);
            }
        }
    }

    private void updateSaleRows(List<MPoint> points, ISession ses) {
        for (MPoint point : points) {
            if (point.last_sale == null || point.last_sale.mSaleRowsList == null || point.last_sale.mSaleRowsList.size() == 0)
                continue;
            Date date = point.last_sale.date;


            for (MSaleRows mSaleRows : point.last_sale.mSaleRowsList) {
                mSaleRows.date = date;
                mSaleRows.point_id = point.idu;
                ses.insert(mSaleRows);
            }
        }
    }

    private void updateContactCore(List<MPoint> mPointList, ISession ses) {
        if (mPointList == null || mPointList.size() == 0) return;
        for (MPoint mPoint : mPointList) {
            List<MContact> mContacts = mPoint.contacts;
            updateContact(mContacts, ses, mPoint.idu);
        }
    }

    private void updateContragentCore(List<MPoint> mPointList, ISession ses) {
        if (mPointList == null || mPointList.size() == 0) return;
        Set<String> strings = new HashSet<>();
        for (MPoint mPoint : mPointList) {
            if (mPoint.mAgent == null) continue;
            mPoint.agent_id = mPoint.mAgent.idu;

            if (!strings.contains(mPoint.mAgent.get_id())) {
                ses.insert(mPoint.mAgent);
                strings.add(mPoint.mAgent.get_id());
            }
        }
    }

    private void updateContact(List<MContact> mContactList, ISession ses, String point_id) {

        if (mContactList == null || mContactList.size() == 0) return;
        Set<String> strings = new HashSet<>();
        for (MContact contact : mContactList) {
            if (strings.contains(contact.get_id())) {
                continue;
            }
            MContactor contactor = new MContactor();
            contactor.contact_id = contact.idu;
            contactor.point_id = point_id;
            ses.insert(contact);
            strings.add(contact.get_id());
            ses.insert(contactor);
        }
    }

    private void updateVisitTypes(List<MVisitResults> mVisitTypesList, ISession ses) {
        if (mVisitTypesList == null || mVisitTypesList.size() == 0) return;
        Configure.bulk(MVisitResults.class, mVisitTypesList, ses);
    }

    private void updateChannel(List<MDistributionchannel> mChannelList, ISession ses) {
        if (mChannelList == null || mChannelList.size() == 0) return;
        Configure.bulk(MDistributionchannel.class, mChannelList, ses);
    }

    private void updateOrderType(List<MOrderType> mOrderTypeList, ISession ses) {
        if (mOrderTypeList == null || mOrderTypeList.size() == 0) return;
        Configure.bulk(MOrderType.class, mOrderTypeList, ses);
    }

    private void updatePriceType(List<MPriceType> mPriceTypeList, ISession ses) {
        if (mPriceTypeList == null || mPriceTypeList.size() == 0) return;
        Configure.bulk(MPriceType.class, mPriceTypeList, ses);
    }

    private void updatePointType(List<MPointType> mPointTypeList, ISession ses) {
        if (mPointTypeList == null || mPointTypeList.size() == 0) return;
        Configure.bulk(MPointType.class, mPointTypeList, ses);
    }

    private void updatePrice(List<MPrice> mPriceList, ISession ses) {
        if (mPriceList == null || mPriceList.size() == 0) return;
        Configure.bulk(MPrice.class, mPriceList, ses);
    }

    private void updateProductGroup(List<MProductGroup> mProductGroupList, ISession ses) {
        if (mProductGroupList == null || mProductGroupList.size() == 0) return;
        Configure.bulk(MProductGroup.class, mProductGroupList, ses);
    }

    private void updateProduct(List<MProduct> mProductList, ISession ses) {
        if (mProductList == null || mProductList.size() == 0) return;
        Configure.bulk(MProduct.class, mProductList, ses);
    }

    private void updatePoint(List<MPoint> mPointList, ISession ses) {
        if (mPointList == null || mPointList.size() == 0) return;
        Configure.bulk(MPoint.class, mPointList, ses);
    }

    private void updatePromotion(List<MPromotion> mPromotionList, ISession ses) {
        if (mPromotionList == null || mPromotionList.size() == 0) return;
        for (MPromotion promotion : mPromotionList) {
            ses.insert(promotion);
        }
    }
}


