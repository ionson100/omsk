package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

import java.util.ArrayList;
import java.util.List;


class FiscalisationKkt {

    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(FiscalisationKkt.class);

    public static void action(final TextView log, final Settings mSettings, final Activity activity) {

     //   final SettingsKassa mSettingsKassa = SettingsKassa.core();
        new AsyncTask<Void, String, Void>() {
            String errorString;
            private Fptr fptr;
            final ProgressDialog[] dialog = new ProgressDialog[1];
            final List<String> stringList = new ArrayList<String>();

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected void onPreExecute() {
                dialog[0] = Utils.factoryDialog(activity, "Фискализация кассы", null);
                dialog[0].show();
                log.setText("");
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                if (dialog[0] != null) {
                    dialog[0].cancel();
                    dialog[0] = null;
                }
                if (errorString != null) {
                    Utils.messageBox(activity.getString(R.string.error), errorString, activity, null);
                } else {
                    Utils.messageBox("Ok", "Успешно", activity, null);
                    log.setText("");
                }

            }

            @Override
            protected Void doInBackground(Void... params) {
                fptr = new Fptr();

                try {
                    fptr.create(activity);
                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) { // загрузка настроек
                        checkError();
                    }

                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {// устанока соединения
                        checkError();
                    }
                    publishProgress("OK");

                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    publishProgress("Ввод паспорта...");
                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Установка парамера фискализации");
                    if (fptr.put_Mode(IFptr.MODE_FISCAL_MEMORY) < 0) {
                        checkError();
                    }
                    if (fptr.SetMode() < 0) {
                        checkError();
                    }
                } catch (Exception e) {
                    logE.error(e);
                    errorString = e.getMessage();
                    publishProgress(e.getMessage());
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - FiscalisationKkt\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }

        }.execute();
    }
}
