package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DebtDuplet implements Serializable {
    public Date date ;
    public double amount;
    public double price;
    public double product_id;
    public List<MTriplet> tripletList = new ArrayList<>();
}
