package com.example.user.omsk.fotocontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.orientationimage.RotateImage;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderDownloadImagePointPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.addin.AddinEditPoint;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;

/**
 * TODO: document your custom view class.
 */
public class MyViewFotoPoint extends LinearLayout {


    private LinearLayout panelFoto;
    private MPoint mPoint;
    private boolean isPrevy;
    private Activity activity;


    public static String getPhotoDirectoryPoint() {
        String patch = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/point";
        File file = new File(patch);
        if (file.exists() == false) {
            file.mkdir();
        }
        return patch;
    }

    public static String getPhotoDirectoryTempPoint() {

        String patch = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/temppoint";
        File file = new File(patch);
        if (file.exists() == false) {
            file.mkdir();
        }

        return patch;
    }


    public MyViewFotoPoint(Context context) {
        super(context);

    }

    public void activate(MPoint mPoint, boolean isPrevy, Activity activity) {
        this.mPoint = mPoint;
        this.isPrevy = isPrevy;
        this.activity = activity;
        init();
    }

    @Override
    protected void onCreateContextMenu(ContextMenu menu) {
        super.onCreateContextMenu(menu);
    }

    public MyViewFotoPoint(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public MyViewFotoPoint(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }


    private void init() {


        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = vi.inflate(R.layout.partial_foto_point, null);
        this.addView(view, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        panelFoto = (LinearLayout) view.findViewById(R.id.control_panel_images_point);
        Button button = (Button) view.findViewById(R.id.control_bt_image_point);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String patch = getLastFileName(mPoint);
                if (patch != null) {
                    Uri outputFileUri = Uri.fromFile(new File(patch));
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.putExtra("photo", true);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    activity.startActivityForResult(cameraIntent, AddinEditPoint.CAMERA_REQUEST);
                }

            }
        });

        Button buttonUpdate = (Button) view.findViewById(R.id.bt_update);


        buttonUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });

        if (mPoint.stateObject) {
            buttonUpdate.setVisibility(GONE);
        }

        loadFilesFromServer();

        if (isPrevy == false) {
            button.setVisibility(VISIBLE);
        } else {
            button.setVisibility(GONE);
        }
        showImages();

    }

    public void loadFilesFromServer() {

        File[] d = new File(getPhotoDirectoryPoint()).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.indexOf(mPoint.idu) != -1;
            }
        });

        if (d!=null&&d.length == 0) {
            for (int i = -1; i < 11; i++) {
                if (i == -1) {
                    new SenderDownloadImagePointPOST().send(mPoint.idu,  activity);
                } else {
                    new SenderDownloadImagePointPOST().send(mPoint.idu + "(" + i + ")",  activity);
                }

            }
        }


    }

    void addContextMenu(final View image) {


        image.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = (String) image.getTag();
                File file = new File(str);
                if (file.exists() == false) {

                    Toast.makeText(activity, "Файла не существует", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    String as = "file://" + "/" + str;
                    intent.setDataAndType(Uri.parse(as), "image/*");
                    activity.startActivity(intent);
//
                }

            }
        });


        if (isPrevy) return;
        image.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(R.string.show).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        String str = (String) image.getTag();
                        File file = new File(str);
                        if (file.exists() == false) {

                            Toast.makeText(activity, "Файла не существует", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            String as = "file://" + "/" + str;
                            intent.setDataAndType(Uri.parse(as), "image/*");
                            activity.startActivity(intent);

                        }
                        return true;
                    }
                });


                menu.add(R.string.delete_photo).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        final String str = (String) image.getTag();
                        final File file = new File(str);
                        if (file.exists()) {
                            if (str.indexOf("/point/") != -1) {
                                Utils.messageBox(activity.getString(R.string.warning), activity.getString(R.string.sadasdsd), activity, new IActionE() {
                                    @Override
                                    public void action(Object o) {

                                        if (Settings.core().getVisit().point.deletePhotos.contains(str) == false) {
                                            Settings.core().getVisit().point.deletePhotos.add(str);
                                        }
                                        showImages();

                                    }
                                });
                            } else {
                                if (file.delete()) {
                                    showImages();
                                }
                            }
                        }
                        return true;
                    }
                });


//                menu.add(R.string.update_photo).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//
//                       update();
//
//                        return true;
//                    }
//                });

            }
        });

    }

    public void showImages() {


        panelFoto.removeAllViews();
        List<String> listDel = mPoint.deletePhotos;
        {
            final File f = new File(getPhotoDirectoryPoint());
            File[] files = f.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().indexOf(mPoint.idu) != -1;
                }
            });
            if(files==null){
                return;
            }
            for (final File file : files) {
                if (!file.exists()) continue;
                if (listDel.contains(file.getPath())) {
                } else {
                    ImageView view = (ImageView) LayoutInflater.from(activity).inflate(R.layout.image_view_present, null);

                    Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
                    Bitmap res = RotateImage.rotationImage(bitmap, file.getAbsolutePath());
                    if (res == null) {
                        res = bitmap;
                    }
                    view.setImageBitmap(res);
                    view.setTag(file.getPath());
                    addContextMenu(view);
                    panelFoto.addView(view);
                }
            }
        }
        {
            if (isPrevy == false) {
                final File f = new File(getPhotoDirectoryTempPoint());


                File[] files = f.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.getName().indexOf(mPoint.idu) != -1;
                    }
                });

                if(files==null){
                    return;
                }
                for (final File file : files) {
                    if (!file.exists() || file.length() < 10) continue;
                    ImageView view = (ImageView) LayoutInflater.from(activity).inflate(R.layout.image_view_present, null);

                    Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
                    Bitmap res = RotateImage.rotationImage(bitmap, file.getAbsolutePath());
                    if (res == null) {
                        res = bitmap;
                    }
                    view.setImageBitmap(res);
                    view.setTag(file.getPath());
                    addContextMenu(view);
                    panelFoto.addView(view);
                }

            }
        }
    }


    public void update() {
        Utils.messageBox(activity.getString(R.string.warning), activity.getString(R.string.sadasds2323), activity, new IActionE() {
            @Override
            public void action(Object o) {

//                File[] files1 = new File(getPhotoDirectoryTempPoint()).listFiles(new FileFilter() {
//                    @Override
//                    public boolean accept(File pathname) {
//                        return pathname.getName().indexOf(mPoint.idu) != -1;
//                    }
//                });
//
//                for (File file : files1) {
//                    file.delete();
//                }

                File[] files2 = new File(getPhotoDirectoryPoint()).listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.getName().indexOf(mPoint.idu) != -1;
                    }
                });
                if(files2==null){
                    return;
                }
                for (File file : files2) {
                    file.delete();
                }
                panelFoto.removeAllViews();

                for (int i = -1; i < 11; i++) {
                    if (i == -1) {
                        new SenderDownloadImagePointPOST().send(mPoint.idu,  activity);
                    } else {
                        new SenderDownloadImagePointPOST().send(mPoint.idu + "(" + i + ")",  activity);
                    }
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void copy() {

        File filePoint = new File(getPhotoDirectoryPoint());

        File file = new File(getPhotoDirectoryTempPoint());

        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().indexOf(mPoint.idu) != -1;
            }
        });

        if(files==null){
            return;
        }
        if (files.length > 0) {

            for (File f : files) {

                File[] filesP = filePoint.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.getName().indexOf(mPoint.idu) != -1;
                    }
                });

                String addin = "";

                int i = -1;

                if(filesP==null){
                    return;
                }
                if (filesP.length == 0) {

                } else {
                    while (new File(getPhotoDirectoryPoint() + "/" + mPoint.idu + "(" + (++i) + ")" + ".jpg").exists()) {
                    }
                    addin = "(" + i + ")";
                }
                try {
                    Utils.copyDirectoryOneLocationToAnotherLocation(f, new File(getPhotoDirectoryPoint() + "/" + mPoint.idu + addin + ".jpg"));
                    MPointImage pointImage = new MPointImage();
                    pointImage.point_id = mPoint.idu;
                    pointImage.name = getPhotoDirectoryPoint() + "/" + mPoint.idu + addin + ".jpg";
                    Configure.getSession().insert(pointImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        File[] list=new File(getPhotoDirectoryTempPoint()).listFiles();
        if(file!=null){
            for (File fildel : list ) {
                if (fildel != null) {
                    fildel.delete();
                }
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void save(List<String> deletePhoto, ISession session) {
        copy();
        if(deletePhoto!=null){
            for (String pp : deletePhoto) {
                MPointImage image = new MPointImage();
                image.is_delet = true;
                image.name = pp;
                session.insert(image);
            }
        }

    }


    public static void deleteTempFiles() {
        File f = new File(getPhotoDirectoryTempPoint());

        File[] files = f.listFiles();
        if(files==null){
            return;
        }

        for (File file : files) {
            file.delete();
        }


    }


    public String getLastFileName(final MPoint mPoint) {
        String res = null;
        File file = new File(getPhotoDirectoryTempPoint());

        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().indexOf(mPoint.idu) != -1;
            }
        });

        String addin = "";
        if (files!=null&&files.length == 0) {

        } else {
            int i = -1;
            while (new File(getPhotoDirectoryTempPoint() + "/" + mPoint.idu + "(" + (++i) + ")" + ".jpg").exists()) {
            }
            addin = "(" + i + ")";
        }

        res = getPhotoDirectoryTempPoint() + "/" + mPoint.idu + addin + ".jpg";
        return res;
    }


    public boolean isValidateCount() {
        if (panelFoto.getChildCount() == 0) {
            return false;
        }
        return true;
    }
}
