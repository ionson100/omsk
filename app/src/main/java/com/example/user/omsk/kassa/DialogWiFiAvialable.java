package com.example.user.omsk.kassa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;

import java.util.List;

public class DialogWiFiAvialable extends DialogFragment {

    public IActionE<String> iActionE;
    private BroadcastReceiver mWifiScanReceiver;
    private WifiManager mWifiManager;
    private View mView;
    private Button btSave;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final String[] select = {""};
        mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_wi_fi_avialavle, null);

        mWifiScanReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                    List<ScanResult> mScanResults = mWifiManager.getScanResults();
                    mView.findViewById(R.id.progressBar3).setVisibility(View.GONE);
                    RadioGroup radioGroup = (RadioGroup) mView.findViewById(R.id.radio_wifi);
                    for (ScanResult mScanResult : mScanResults) {
                        final RadioButton x = (RadioButton) LayoutInflater.from(getActivity()).inflate(R.layout.dialog_wi_fi_avialavle_rb, null);
                        x.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked) {
                                    select[0] = x.getText().toString();
                                    btSave.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                        x.setText(mScanResult.SSID);
                        radioGroup.addView(x);
                    }
                    int ss = mScanResults.size();
                }
            }
        };

        mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        getActivity().registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        mWifiManager.startScan();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        builder.setView(mView);
        final AlertDialog xc = builder.create();

        mView.findViewById(R.id.bt_close2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                xc.dismiss();
            }
        });

        btSave = (Button) mView.findViewById(R.id.bt_save2);


        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iActionE.action(select[0]);
                xc.dismiss();
            }
        });


        return xc;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mWifiScanReceiver);
        mWifiManager.disconnect();
    }
}
