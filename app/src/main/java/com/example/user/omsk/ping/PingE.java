package com.example.user.omsk.ping;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import java.io.IOException;

public class PingE {

    public static boolean ping(String host) {
        int returnVal = 0;
        try {

            boolean isWindows = System.getProperty("os.name").toLowerCase().contains("win");

            ProcessBuilder processBuilder = new ProcessBuilder("ping", isWindows ? "-n" : "-c", "1", host);
            Process prc = processBuilder.start();
            returnVal = prc.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return returnVal == 0;
    }
}

