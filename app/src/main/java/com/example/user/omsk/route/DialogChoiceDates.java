package com.example.user.omsk.route;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class DialogChoiceDates extends DialogFragment {

    private boolean mIsCreate;
    private boolean mIsSender;
    private Route mRouter;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       // List<MPoint> mPointList = Configure.getSession().getList(MPoint.class, null);
       // Settings mSettings = Settings.core();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        vi = LayoutInflater.from(getActivity());
        View v = vi.inflate(R.layout.dialog_datepicker, null);
        final DatePicker datePicker = (DatePicker) v.findViewById(R.id.datePicker);
        GregorianCalendar now = new GregorianCalendar();
        GregorianCalendar today = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        today.add(Calendar.DAY_OF_MONTH, 1);
        final long logdel = today.getTime().getTime();
        builder.setView(v);
        builder.setNegativeButton(getString(R.string.route_not_confirm2), new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }

        });
        builder.setPositiveButton(getString(R.string.confirm), new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (mIsCreate) {
                    if (!mIsSender) {
                        mRouter.reload(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                    } else {
                        mRouter.reloadSender(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                    }
                    dialog.cancel();
                } else {
                    int day = datePicker.getDayOfMonth();
                    int month = datePicker.getMonth();
                    int year = datePicker.getYear();
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, day);
                    if (calendar.getTime().getTime() < logdel) {
                        Utils.messageBox(getString(R.string.error), getString(R.string.er34), getActivity(), null);
                    } else {
                        if (!mIsSender) {
                            mRouter.reload(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                        } else {
                            mRouter.reloadSender(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                        }
                        dialog.cancel();
                    }
                }
            }
        });
        return builder.create();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mRouter = (Route) activity;
    }

    public void isSender(boolean b) {

        this.mIsSender = b;
    }

    public void setmIsCreate(boolean mIsCreate) {
        this.mIsCreate = mIsCreate;
    }
}
