package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

import java.io.Serializable;
import java.util.Date;

@Table("dolgi")
public class MDebt implements Serializable {

    @PrimaryKey("id")
    public int id;

    @Column("point_id")
    public String point_id;

    @Column("visit_id")
    public String order_id;


    @Column("product_id")
    public String product_id;

    @Column("date")
    public int date;

    @Column("amount")
    public double amount;

    @Column("price")
    public double price;

    @Column("status")
    public boolean status;

    public MProduct getProduct() {
        MProduct product = Configure.getSession().get(MProduct.class, product_id);
        if (product == null) {
            product = new MProduct();
            product.name = "Продукт не найден";
        }
        return product;
    }
}
