package com.example.user.omsk.addin;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.user.omsk.FEditAddPoint;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;


public class AddinEditPoint {

    public static final int CAMERA_REQUEST = 1888;

    public static void addin(final Activity activity, View view, final MPoint mPoint, FEditAddPoint fragment) {

        LinearLayout panel = (LinearLayout) view.findViewById(R.id.addin_panel);
        panel.setVisibility(View.VISIBLE);
        EditText fio = (EditText) view.findViewById(R.id.e_fio);
        final EditText fioTel = (EditText) view.findViewById(R.id.e_fio_tel);
        final EditText inn = (EditText) view.findViewById(R.id.e_inn);

        fio.setText(mPoint.fio);
        fio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPoint.fio = s.toString();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        fioTel.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {

            private boolean backspacingFlag = false;
            private boolean editedFlag = false;
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - fioTel.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");

                if (!editedFlag) {

                    if (phone.length() >= 6 && !backspacingFlag) {

                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
                        fioTel.setText(ans);
                        fioTel.setSelection(fioTel.getText().length() - cursorComplement);
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3);
                        fioTel.setText(ans);
                        fioTel.setSelection(fioTel.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });

        fioTel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String dd = s.toString().replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
                if (dd.length() > 5)
                    mPoint.telephone_fio = "8" + dd;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        fioTel.setText(mPoint.telephone_fio);

        inn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String s1 = s.toString();
                if (s1 != null && s1.length() > 5) {
                    if (ValidateINN.isValidINN(s.toString())) {
                        mPoint.inn = s.toString();
                    } else {
                        inn.setError("Не верный формат");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        inn.setText(mPoint.inn);

    }
}
