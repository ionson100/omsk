package com.example.user.omsk.visitstoryworker;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;




public class WriterVisitStory {
    private static final String charset="utf-8";
    public static org.apache.log4j.Logger log = Logger.getLogger(WriterVisitStory.class);

    public void writeVisitStory() throws IOException {
        List<MVisitStory> stories = Configure.getSession().getList(MVisitStory.class, null);
        if (stories.size() == 0) return;
        Gson gson = Utils.getGson();
        String sjson = gson.toJson(stories);
        SimpleDateFormat dateFormat = new SimpleDateFormat(MyApplication.formatDateVisit);
        String str = dateFormat.format(new Date());
        String file = String.format(MyApplication.csvfile, str);
        File f = new File(file);
        if (f.getParentFile().exists() == false) {
            f.getParentFile().mkdir();
        }
        log.info(sjson);
        f.createNewFile();
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(f, false), charset));
            writer.write(sjson);
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {
                log.error(ex);
            }
        }
    }
    public List<MVisitStory> readWisitStory(String pach) throws IOException {
        List<MVisitStory> res=new ArrayList<>();
        if(pach==null) return res;
        File file=new File(pach);
        if(file.exists()==false) return res;
        FileInputStream fileInputStream = null;
        try{
            fileInputStream = new FileInputStream(file);
            byte[] buffer = new byte[fileInputStream.available()];
            int length = fileInputStream.read(buffer);
            String json= new String(buffer, 0, length,charset);
            Type listType = new TypeToken<ArrayList<MVisitStory>>(){}.getType();
            res = Utils.getGson().fromJson(json, listType);
            return res;
        }finally {
            if(fileInputStream!=null){
                fileInputStream.close();
            }
        }



    }
}
