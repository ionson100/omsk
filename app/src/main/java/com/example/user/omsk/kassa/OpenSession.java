package com.example.user.omsk.kassa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

/**
 * ion100 on 06.07.2017.
 */

class OpenSession {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(OpenSession.class);
    public static synchronized void Open(final Activity activity) {


        final String[] error = {null};
        new AsyncTask<Void, Void, Void>() {
            private ProgressDialog dialog;
            Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected void onPreExecute() {
                dialog = Utils.factoryDialog(activity, "Открытие смены", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                if (error[0] != null) {
                    Utils.messageBox(activity.getString(R.string.error), error[0], activity, null);
                } else {
                    Utils.messageBox(activity.getString(R.string.ok), activity.getString(R.string.cvsdf), activity, null);
                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                fptr = new Fptr();

                fptr.create(activity.getApplication());

                try {

                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }

                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }

                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }

                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError();
                    }

                    if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                        checkError();
                    }
                    if (fptr.SetMode() < 0) {
                        checkError();
                    }

                    if (fptr.OpenSession() < 0) {
                        checkError();
                    }

                } catch (Exception ex) {
                    log.error(ex);
                    error[0] = ex.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), Settings.core().kkmNumber + "\n" +
                            "Ошибка ккм file  - OpenSession\n" + ex.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }
        }.execute();


    }
}

