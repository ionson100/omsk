package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

// получение отпечатка приложения, предпологаю что для каждого релиза оно свое
public class Certificate {

    public static org.apache.log4j.Logger log = Logger.getLogger(Certificate.class);
    public static String getCertificateSHA1Fingerprint(Context context) {
        if (context == null)
            return "";
        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();
        int flags = PackageManager.GET_SIGNATURES;

        PackageInfo packageInfo = null;

        try {
            packageInfo = pm.getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
            log.error(e);
        }
        android.content.pm.Signature[] signatures = packageInfo.signatures;

        byte[] cert = signatures[0].toByteArray();

        InputStream input = new ByteArrayInputStream(cert);

        CertificateFactory cf = null;
        try {
            cf = CertificateFactory.getInstance("X509");


        } catch (CertificateException e) {
            log.error(e);
        }
        X509Certificate c = null;
        try {
            c = (X509Certificate) cf.generateCertificate(input);
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(c.getPublicKey().getEncoded());

            if(publicKey!=null){
                for (byte aPublicKey : publicKey) {
                    String appendString = Integer.toHexString(0xFF & aPublicKey);
                    if (appendString.length() == 1) hexString.append("0");
                    hexString.append(appendString);
                }
            }



        } catch (NoSuchAlgorithmException e1) {
           log.error(e1);
        }
        return hexString.toString();
    }

}
