package com.example.user.omsk.myTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class MyBoldTextView extends android.support.v7.widget.AppCompatTextView {


    public MyBoldTextView(Context context) {
        super(context);
        init();
    }

    public MyBoldTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyBoldTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        Typeface typeFace = Typeface.createFromAsset(getContext().getAssets(), "fonts/GloberSemiBold.otf");//GloberRegular.otf
        this.setTypeface(typeFace);
    }

}
