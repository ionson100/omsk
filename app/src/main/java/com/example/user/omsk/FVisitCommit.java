package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.dialogs.DialogPrintDoubleCheck;
import com.example.user.omsk.kassa.DialogActionCheck;
import com.example.user.omsk.kassa.UtilsKassa;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPhoto;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MSummaKKM;
import com.example.user.omsk.models.MVideo;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.myTextView.MyButton;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.reloadgps.DialogReloadGps;
import com.example.user.omsk.reloadgps.IActionReloadGps;
import com.example.user.omsk.senders.SenderErrorPOST;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

//import com.example.user.omsk.models.MError;


public class FVisitCommit extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FVisitCommit.class);

    final Handler myHandler = new Handler();
    boolean mFirsStart = true;
    private EditText mCommentaryEditText;
    private CheckBox mCbCommit;
    private Settings mSettings;
    private View mView;
    private MapVisit mMapVisit;
    private Button mBtCheck;
    MyButton bt_help;

    public FVisitCommit() {
    }

    public void actionGPS() {
        try {
            if (mSettings.getVisit().id == 0) {
                validateGps();
            }
        } catch (Exception ignored) {
            log.error(ignored);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onResume() {
        super.onResume();
        Settings.save();
        if (mMapVisit != null) {
            mMapVisit.destory();
            mMapVisit = null;
        }
        validateGps();

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapVisit != null) {
            mMapVisit.destory();
            mMapVisit = null;
        }
    }

    private void validateGps() {

        if (mSettings.getVisit().latitude == 0 || mSettings.getVisit().longitude == 0) {
            mView.findViewById(R.id.panel_debt).setVisibility(View.GONE);
            return;
        }
        final TextView textView = (TextView) mView.findViewById(R.id.text_reload_gps);
        mView.findViewById(R.id.panel_debt).setVisibility(View.VISIBLE);

        double dis = Utils.getDistance(mSettings.getVisit().latitude, mSettings.getVisit().longitude, mSettings.getVisit().point.latitude, mSettings.getVisit().point.longitude);
        if (dis > (Utils.MAX_DIS + 1)) {

            textView.setText(getString(R.string.sddsd) + Utils.getStringDecimal(dis) + " м.");
            textView.setVisibility(View.VISIBLE);
            final Button button = (Button) mView.findViewById(R.id.button_reload_gps);
            if (mSettings.getVisit().isFirstVisit) {
                button.setVisibility(View.VISIBLE);
            }
            final View panelDebit = mView.findViewById(R.id.panel_debt);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!CheckGps.check(FVisitCommit.this)) {
                        return;
                    }
                    DialogReloadGps dialogReloadGps = new DialogReloadGps();
                    dialogReloadGps.setmActivity(getActivity());
                    dialogReloadGps.show(getActivity().getSupportFragmentManager(), "sdh");
                    dialogReloadGps.setmIActionReloadGps(new IActionReloadGps() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void action(final double dis, double latitude, double longitude) {
                            mSettings.getVisit().latitude = latitude;
                            mSettings.getVisit().longitude = longitude;
                            Settings.save();
                            if (dis <= Utils.MAX_DIS) {
                                panelDebit.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        panelDebit.setVisibility(View.GONE);
                                    }
                                });
                            } else {
                                textView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        textView.setText(getString(R.string.sddsd) + Utils.getStringDecimal(dis) + " м.");
                                        textView.setVisibility(View.VISIBLE);
                                    }
                                });
                                button.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSettings.getVisit().id == 0) {
                                            button.setVisibility(View.VISIBLE);
                                        } else {
                                            button.setVisibility(View.GONE);
                                        }
                                    }
                                });
                            }

                            if (mMapVisit != null) {
                                mMapVisit.destory();
                                mMapVisit = null;
                                System.gc();
                                mMapVisit = new MapVisit(mView, getActivity(), mSettings.getVisit(), true);
                            }
                        }
                    });
                }
            });
        } else {

            mView.findViewById(R.id.panel_debt).setVisibility(View.GONE);

        }

        mMapVisit = new MapVisit(mView, getActivity(), mSettings.getVisit(), true);

        if(mSettings.getVisit().point.latitude==0d||mSettings.getVisit().point.longitude==0d){
            mView.findViewById(R.id.panel_debt).setVisibility(View.GONE);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private synchronized void gpsAction(final View panelDebit, final TextView textView, final Button button, final double dis) {


        if (dis <= Utils.MAX_DIS) {

            panelDebit.post(new Runnable() {
                @Override
                public void run() {
                    panelDebit.setVisibility(View.GONE);
                }
            });

        } else {

            textView.post(new Runnable() {
                @Override
                public void run() {
                    textView.setText(getString(R.string.sddsd) + Utils.getStringDecimal(dis) + " м.");
                    textView.setVisibility(View.VISIBLE);
                }
            });
            button.post(new Runnable() {
                @Override
                public void run() {
                    if (mSettings.getVisit().id == 0) {
                        button.setVisibility(View.VISIBLE);
                    } else {
                        button.setVisibility(View.GONE);
                    }
                }
            });

        }

        if (mMapVisit != null) {
            mMapVisit.destory();
            mMapVisit = null;
            mMapVisit = new MapVisit(mView, getActivity(), mSettings.getVisit(), true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_visit_commit, container, false);

        try {

            mSettings = Settings.core();
            mBtCheck = (Button) mView.findViewById(R.id.bt_check);

            bt_help= (MyButton) mView.findViewById(R.id.button_double_check_help);

            bt_help.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.showHelpNote(getActivity(),  "note_double_check");
                }
            });


            final boolean[] run = {true};
            mBtCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public synchronized void onClick(View v) {
                    if (!run[0]) return;
                    run[0] = false;



                    double summ=0d;
                    double nal=Utils.getNallCore();
                    List<MSummaKKM> kkms= Configure.getSession().getList(MSummaKKM.class,null);
                    if(kkms.size()==0){
                    }else {
                        summ=kkms.get(0).sumkkm;
                    }

                    if(summ>0&&summ>nal){
                        final DialogPrintDoubleCheck doubleCheck=new DialogPrintDoubleCheck();
                        doubleCheck.mIActionENoOk=new IActionE() {
                            @Override
                            public void action(Object o) {
                                doubleCheck.dismiss();
                                run[0] = true;

                            }
                        };
                        doubleCheck.mIActionEOk=new IActionE() {
                            @Override
                            public void action(Object o) {

                                doubleCheck.dismiss();
                                printable();
                            }
                        };
                        doubleCheck.mIActionEDismiss=new IActionE() {
                            @Override
                            public void action(Object o) {
                                run[0] = true;
                                bt_help.setVisibility(View.VISIBLE);
                            }
                        };
                        doubleCheck.sumKKT=summ;
                        doubleCheck.sumReal=nal;
                        doubleCheck.show(getActivity().getSupportFragmentManager(),"asass");
                    }else {
                        printable();
                    }



                }

                private void printable() {
                    DialogActionCheck dialogActionCheck = new DialogActionCheck();
                    dialogActionCheck.setmSettings(mSettings);
                    dialogActionCheck.setActionE(new IActionE() {
                        @Override
                        public void action(Object o) {
                            mView.findViewById(R.id.panel_not_sender).setVisibility(View.GONE);
                            MChecksData checksData = (MChecksData) o;
                            if (checksData.type_check == MChecksData.TYPE_CHECK_CACH) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mView.findViewById(R.id.radio0).setVisibility(View.GONE);
                                        mView.findViewById(R.id.radio1).setVisibility(View.GONE);
                                        mSettings.getVisit().isNotSender = true;

                                        Settings.save();
                                        run[0] = true;
                                    }
                                });
                            }
                        }
                    });
                    dialogActionCheck.setActionEDismis(new IActionE() {
                        @Override
                        public void action(Object o) {
                            run[0] = true;
                        }
                    });
                    dialogActionCheck.show(getActivity().getSupportFragmentManager(), "djsdjjj");
                }
            });
            ////  чистка списков
            List<MProduct> deleteProduct = new ArrayList<>();
            for (MProduct product : mSettings.getVisit().selectProductForActions) {
                if (product.getAmount_core() == 0) {
                    deleteProduct.add(product);
                }
            }
            for (MProduct product : deleteProduct) {
                mSettings.getVisit().selectProductForActions.remove(product);
            }

            deleteProduct = new ArrayList<>();

            for (MProduct product : mSettings.getVisit().selectProductForPresent) {
                if (product.getAmount_core() == 0) {
                    deleteProduct.add(product);
                }
            }

            for (MProduct product : deleteProduct) {
                mSettings.getVisit().selectProductForPresent.remove(product);
            }

            deleteProduct = new ArrayList<>();

            for (MProduct product : mSettings.getVisit().selectProductForSale) {
                if (product.getAmount_core() == 0) {
                    deleteProduct.add(product);
                }
            }

            for (MProduct product : deleteProduct) {
                mSettings.getVisit().selectProductForSale.remove(product);
            }
            if (mSettings.getVisit() == null) {
                Settings.showFragment(StateSystem.HOME, getActivity());
                return mView;
            }

            List<MVisitResults> mVisitResultsList = Configure.getSession().getList(MVisitResults.class, null);
            List<MOrderType> mOrderTypeList = Configure.getSession().getList(MOrderType.class, null);

            if (mSettings.getVisit().selectProductForSale.size() > 0) {
                MVisitResults r = Linq.toStream(mVisitResultsList).firstOrDefault(new Predicate<MVisitResults>() {
                    @Override
                    public boolean apply(MVisitResults t) {
                        return t.index == 1;
                    }
                });
                mSettings.getVisit().visit_result_id = r.idu;
                if (mSettings.getVisit().order_type_id == null || mSettings.getVisit().order_type_id.trim().length() == 0) {
                    MOrderType r1 = Linq.toStream(mOrderTypeList).firstOrDefault(new Predicate<MOrderType>() {
                        @Override
                        public boolean apply(MOrderType t) {
                            return t.index == 0;
                        }
                    });
                    mSettings.getVisit().order_type_id = r1.idu;
                }

            } else {
                MVisitResults r = Linq.toStream(mVisitResultsList).firstOrDefault(new Predicate<MVisitResults>() {
                    @Override
                    public boolean apply(MVisitResults t) {
                        return t.index == 0;
                    }
                });
                mSettings.getVisit().visit_result_id = r.idu;
                mSettings.getVisit().order_type_id = null;
            }

            mCbCommit = (CheckBox) mView.findViewById(R.id.check_commit);
            mCommentaryEditText = (EditText) mView.findViewById(R.id.commit_commentary);
            mCommentaryEditText.setText(mSettings.getVisit().comment);
            activateRadioResResVisit();
            activateButtom();
            mCommentaryEditText.setText(mSettings.getVisit().comment);

            if (mSettings.getVisit().selectProductForSale.size() > 0 || mSettings.getVisit().debtList.size() > 0) {

            }

            if (mSettings.getVisit().debtList.size() > 0 && mSettings.getVisit().selectProductForSale.size() == 0) {
                mView.findViewById(R.id.new_panel_price_type).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.radio0).setVisibility(View.GONE);
                mView.findViewById(R.id.radio1).setVisibility(View.GONE);
            }

            if (Utils.isDelay(mSettings.getVisit()) && mSettings.getVisit().debtList.size() > 0) {
                mBtCheck.setVisibility(View.VISIBLE);
            }

            if (UtilsKassa.getMCheckDataCache(mSettings.getVisit().checksDataList) != null) {
                mView.findViewById(R.id.radio0).setVisibility(View.GONE);
                mView.findViewById(R.id.radio1).setVisibility(View.GONE);
            }

            if (mSettings.getVisit().isNotSender) {
                mView.findViewById(R.id.panel_not_sender).setVisibility(View.GONE);
            }

        } catch (Exception ex) {
            Utils.sendMessage("FVisitCommit:" + Utils.getErrorTrace(ex), (MyApplication) getActivity().getApplication());
            log.error(ex);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
        }

        return mView;
    }


    private void activateRadioResResVisit() {
        List<MVisitResults> mVisitTypesList = Configure.getSession().getList(MVisitResults.class, null);
        MVisitResults firstMVisitTypes = new MVisitResults();
        firstMVisitTypes.index = -1;
        firstMVisitTypes.name = getString(R.string.not_selected_visit);
        firstMVisitTypes.idu = "";
        mVisitTypesList.add(firstMVisitTypes);
        Collections.sort(mVisitTypesList, new Comparator<MVisitResults>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MVisitResults lhs, MVisitResults rhs) {
                return Integer.compare(lhs.index, rhs.index);
            }
        });

        if (mSettings.getVisit().selectProductForSale.size() == 0) {
            // panel.setVisibility(View.GONE);
            for (MVisitResults mVisitTypes : mVisitTypesList) {
                if (mVisitTypes.index == 0) {
                    mSettings.getVisit().visit_result_id = mVisitTypes.get_id();
                    activateSelectSaleType(false);
                }
            }
        }
        if (mSettings.getVisit().selectProductForSale.size() > 0) {
            for (MVisitResults mVisitTypes : mVisitTypesList) {
                if (mVisitTypes.index == 1) {
                    mSettings.getVisit().visit_result_id = mVisitTypes.get_id();
                    activateSelectSaleType(true);
                }
            }
        }
    }

    private void activateSelectSaleType(boolean b) {
        if (!b) {
            mView.findViewById(R.id.new_panel_price_type).setVisibility(View.GONE);
            return;
        }
        if (mSettings.getVisit().order_type_id == null) {
            return;
        }
       // List<MOrderType> mOrderTypeList = Configure.getSession().getList(MOrderType.class, null);
        final RadioButton radioButton0 = (RadioButton) mView.findViewById(R.id.radio0);
        final RadioButton radioButton1 = (RadioButton) mView.findViewById(R.id.radio1);
        if (mSettings.getVisit().order_type_id.equals(Configure.getSession().getList(MOrderType.class, " index1 = 0 ").get(0).idu)) {
            radioButton0.setChecked(true);
            mBtCheck.setVisibility(View.VISIBLE);
        } else if (mSettings.getVisit().order_type_id.equals(Configure.getSession().getList(MOrderType.class, " index1 = 1 ").get(0).idu)) {
            radioButton1.setChecked(true);
            mBtCheck.setVisibility(View.GONE);
        } else {
            return;
        }

        radioButton0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mSettings.getVisit().order_type_id = Configure.getSession().getList(MOrderType.class, " index1 = 0 ").get(0).idu;
                    Settings.save();
                }
                if (radioButton0.isChecked()) {
                    mBtCheck.setVisibility(View.VISIBLE);
                }
            }
        });

        radioButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mSettings.getVisit().order_type_id = Configure.getSession().getList(MOrderType.class, " index1 = 1 ").get(0).idu;

                    if (mSettings.getVisit().selectProductForActions.size() > 0) {
                        String msg = getString(R.string.anton);
                        Utils.messageBoxE(getActivity(), getString(R.string.warning), msg, getString(R.string.assa133), getString(R.string.button23), new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                radioButton0.setChecked(true);
                            }
                        }, new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                mSettings.getVisit().selectProductForActions.clear();
                            }
                        }, true);
                    }
                    Settings.save();
                    if (mSettings.getVisit().debtList.size() == 0) {
                        mBtCheck.setVisibility(View.GONE);
                    } else {
                        mBtCheck.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void activateButtom() {
        mCommentaryEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().comment = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        mView.findViewById(R.id.to_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCbCommit.isChecked()) {
                    if (mSettings.getVisit().id == 0) {
                        Utils.deleteDdirectoryPhotoPoint(mSettings.getVisit().point.get_id());
                    }
                    Settings.showFragment(StateSystem.HOME, getActivity());
                    Toast.makeText(getActivity().getBaseContext(), getString(R.string.no_visit_commit), Toast.LENGTH_LONG).show();
                } else {
                    if (validateE()) {
                        try{
                            Utils.confirmCommitVisit(getActivity(),  null, new IActionE<View>() {
                                @Override
                                public void action(View v) {

                                    if (prepareVisit()) {
                                        Settings.showFragment(StateSystem.HOME, getActivity());
                                        Toast.makeText(getActivity().getBaseContext(), getString(R.string.visit_commit), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }catch (Exception ex){
                            log.error(ex);
                            if (prepareVisit()) {
                                Settings.showFragment(StateSystem.HOME, getActivity());
                                Toast.makeText(getActivity().getBaseContext(), getString(R.string.visit_commit), Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                }
            }
        });

        mView.findViewById(R.id.go_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            }
        });
    }

    private boolean validateE() {

        if (mSettings.getVisit().longitude == 0 || mSettings.getVisit().latitude == 0) {

            Utils.messageBoxE(getActivity(), getString(R.string.error_label), getString(R.string.error_commit_visit), "Настройки GPS", "Всё равно закрыть посещение", new IActionE<View>() {
                @Override
                public void action(View v) {
                    Intent viewIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(viewIntent);
                }
            }, new IActionE<View>() {
                @Override
                public void action(View v) {
                    try{
                        //todo нехватка памяти выгружает  не находит stringi
                        Utils.confirmCommitVisit(getActivity(),  null, new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                if (prepareVisit()) {
                                    Settings.showFragment(StateSystem.HOME, getActivity());
                                    Toast.makeText(getActivity().getBaseContext(), getString(R.string.visit_commit), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }catch (Exception ex){
                        log.error(ex);
                        if (prepareVisit()) {
                            Settings.showFragment(StateSystem.HOME, getActivity());
                            Toast.makeText(getActivity().getBaseContext(), getString(R.string.visit_commit), Toast.LENGTH_LONG).show();
                        }
                    }

                }
            }, false);
            return false;
        }

        if (mSettings.getVisit().visit_result_id == null || mSettings.getVisit().visit_result_id.equals("")) {
            Utils.messageBox(getString(R.string.error_label), getString(R.string.error_selected_visit_result), getActivity(), null);
            return false;
        }
        if (mSettings.getVisit().selectProductForSale.size() > 0 && (mSettings.getVisit().order_type_id == null || mSettings.getVisit().order_type_id.trim().length() == 0)) {
            Utils.messageBox(getString(R.string.error_label), getString(R.string.error_oreder_type), getActivity(), null);
            return false;
        }
      //  MOrderType dd = Configure.getSession().get(MOrderType.class, mSettings.getVisit().order_type_id);
        return true;
    }

    private boolean prepareVisit() {

        ISession ses = Configure.getSession();
        mSettings.getVisit().setFinish(Utils.dateToInt(new Date()));
        MVisit mVisit1 = mSettings.getVisit();
        try {
            MVisit mVisit = mSettings.getVisit();


            if (mVisit.id == 0) {
                final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                int ii = 0;
                int iii = 0;
                if (manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    ii = 10;
                }
                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    iii = 5;
                }
                mVisit.stateLocation = ii + iii;
            }
            boolean second = false;

            MVisitStory mVisitStory = mSettings.getVisit().getVisitStory();

            ses.beginTransaction();
            mVisitStory.date_pessimise=new Date().getTime();
           // List<MVisitStory> visitStoryList0 = Configure.getSession().getList(MVisitStory.class, null);
            if (mVisitStory.id == 0) {

                Configure.getSession().insert(mVisitStory);
            } else {
                Configure.getSession().update(mVisitStory);
            }

            List<MPhoto> mPhotos = Configure.getSession().getList(MPhoto.class, null);
            for (String path : mVisit.listPhotoPath) {
                if (!mPhotos.contains(path)) {
                    MPhoto mPhoto = new MPhoto();
                    mPhoto.path = path;
                    mPhoto.visit_id = mVisitStory.idu;
                    mPhoto.point_id = mVisit.point.get_id();
                    Configure.getSession().insert(mPhoto);
                }
            }

            List<MVideo> mVideos = Configure.getSession().getList(MVideo.class, null);

            for (String s : mVisit.listVideoPath) {
                if (!mVideos.contains(s)) {
                    MVideo mVideo = new MVideo();
                    mVideo.path = s;
                    mVideo.visit_id = mVisitStory.idu;
                    mVideo.point_id = mVisit.point.get_id();
                    Configure.getSession().insert(mVideo);
                }
            }

            if (mVisit.isAss) {
                FAss.deleteVisitAss(mVisit.idu, ses);
            }

            for (MVisitPresent present : mVisit.selectPresents) {
                present.visit_id=mVisitStory.idu;
                if(present._id==0){
                    ses.insert(present);
                }else {
                    ses.update(present);
                }
            }

            Utils.setSalesProducts();
            ses.commitTransaction();
            ses.endTransaction();
            return true;

        } catch (Exception ex) {
            ses.endTransaction();
            mSettings.visitFromError = mVisit1;
            new SenderErrorPOST().send((MyApplication) getActivity().getApplication(), "Событие окончания визита: " + ex.getMessage());
            log.error("Событие окончания визита: " + Utils.getErrorTrace(ex));
            Toast.makeText(getActivity(), "Ошибка Событие окончания визита:" + ex.getMessage(), Toast.LENGTH_LONG).show();
            Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            return false;

        } finally {
            Settings.save();
            getActivity().sendBroadcast(new Intent(MainActivity.FAB));
        }
    }
}
