package com.example.user.omsk.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.user.omsk.R;

/**
 * ion100 on 22.12.2017.
 */

public class DialogNetworkConnect extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_connect, null);
        builder.setView(v);

        TextView textView = (TextView) v.findViewById(R.id.title_dalog);
        textView.setText("Определение подключения");
        TextView textView1 = (TextView) v.findViewById(R.id.title1);
        TextView textView2 = (TextView) v.findViewById(R.id.title2);
        v.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = networkInfo.isConnected();
        networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isMobileConn = networkInfo.isConnected();
        textView1.setText("Wifi соединение:      " + isWifiConn);
        textView2.setText("Mobile соединение: " + isMobileConn);
        return builder.create();
    }

}
