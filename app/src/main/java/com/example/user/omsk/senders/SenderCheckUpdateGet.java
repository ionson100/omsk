package com.example.user.omsk.senders;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * ion100 on 12.12.2017.
 */

public class SenderCheckUpdateGet {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderCheckUpdateGet.class);
    private Activity activity;
    private Settings mSettings;
    private IActionE<String> iActionE;

    public void send(Activity activity, IActionE<String> iActionE) {

        this.activity = activity;
        this.mSettings = Settings.core();
        this.iActionE = iActionE;
        new SenderWorkerTack().execute();
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = Utils.factoryDialog(activity, "Получение данных", null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String string) {
            if (string == null) {
                Settings.save();
                iActionE.action(null);
            } else {
                Toast.makeText(activity, "Ошибка: " + string, Toast.LENGTH_SHORT).show();
                log.error(string);
            }
            if (dialog != null) {
                dialog.cancel();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String str = "";
            final String[] result = {null};

            try {
                str = Utils.HTTP + mSettings.url + "/app_ver/?guid=" + URLEncoder.encode(SettingsUser.core().getUuidDevice(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return e.getMessage();
            }

            result[0] = UtilsSender.getRequest((MyApplication) activity.getApplication(), str, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {

                    try {
                        app(str);
                    } catch (Exception ex) {
                        result[0] = ex.getMessage();
                    }

                }
            });
            return result[0];
        }

        private void app(String str) {
            mSettings.serverVersionName = str;
        }
    }
}
