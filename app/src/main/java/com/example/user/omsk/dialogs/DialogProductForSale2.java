package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.CalculateKeyBoard;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.action.SettingsStorageAction;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.PupperPartial;

import java.util.ArrayList;
import java.util.List;


public class DialogProductForSale2 extends DialogFragment {

    private MAction mAction = SettingsStorageAction.core().mAction_new;
    private MProductBase mProduct;
    private View mView;
    private Settings mSettings;
    private EditText mEditText;
    private double mAmount = 0;
    private String mRuleId;
    private IActionE<MProductBase> iActionE;

    public void setiActionE(IActionE<MProductBase> actionE) {
        this.iActionE = actionE;
    }

    public DialogProductForSale2() {
    }

    public void setmRuleId(String mRuleId) {
        this.mRuleId = mRuleId;
    }

    public void setParametr(MProductBase mProduct, View view, Settings settings) {

        this.mProduct = mProduct;
        this.mView = view;
        this.mSettings = settings;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Dialog dialog;
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_count_product_for_sale, null);
        builder.setView(mView);
        PupperPartial name_product = (PupperPartial) mView.findViewById(R.id.name_dialog);

        final TextView itogo_product = (TextView) mView.findViewById(R.id.itogo_dialog);

        TextView title_dalog = (TextView) mView.findViewById(R.id.title_dalog);

        final PupperPartial price_dialog = (PupperPartial) mView.findViewById(R.id.price_dialog);
        mEditText = (EditText) mView.findViewById(R.id.et_amount_dialog);
        name_product.setPairString("Продукт:", mProduct.getName());


        price_dialog.setPairString(getString(R.string.price), String.valueOf(mProduct.getPrice()));


        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
            builder.setTitle(R.string.title_action_dialog);
        } else if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            //builder.setTitle();
            title_dalog.setText(getString(R.string.title_balance_dialog));

            ((LinearLayout) mView).removeView(price_dialog);
        }
        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
            title_dalog.setText(getString(R.string.title_present_dialog));

        }
        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
            PupperPartial maxAmountBonus = (PupperPartial) mView.findViewById(R.id.amount_bonus);
            maxAmountBonus.setPairString(getString(R.string.max_bonus), String.valueOf(mProduct.amount_action_max));
            maxAmountBonus.setVisibility(View.VISIBLE);
            builder.setTitle(R.string.title_action_dialog);
        } else if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            title_dalog.setText(getString(R.string.title_balance_dialog));
            ((LinearLayout) mView).removeView(price_dialog);
        } else {
            title_dalog.setText(getString(R.string.title_sale_dialog));
        }

        mEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    parseamount(((EditText) v).getText().toString(), itogo_product);

                    okey();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
                }
                return false;
            }
        });
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                parseamount(s.toString(), itogo_product);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
                    mProduct.rule_id = mRuleId;
                }
            }
        });
        mEditText.setText(mProduct.getAmount().equals("") ? "0" : mProduct.getAmount());
        mView.findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mView.findViewById(R.id.btOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okey();
            }
        });


        dialog = builder.create();

        new CalculateKeyBoard(mEditText, mView, mSettings, false, getActivity());

        return dialog;
    }

    private void parseamount(String s, TextView itogo_product) {
        try {
            mAmount = Double.parseDouble(s.toString().replace(",", "."));
        } catch (Exception ex) {
            mAmount = 0;
        }
        itogo_product.setText(getString(R.string.itogo) + Utils.getStringDecimal(mAmount * mProduct.price) + getString(R.string.rub));
    }

    private void okey() {

        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {

            boolean validate = validateActionAmount(mAmount);
            if (validate == false) {
                return;
            } else {
                mProduct.setAmountCore(mAmount);
            }
        } else {
            mProduct.setAmountCore(mAmount);
        }

        // CheckBox checkBox = (CheckBox) mView.findViewById(R.id.checkbox_d);
        CheckBox myCheck = (CheckBox) mView.findViewById(R.id.checkbox_d);

        TextView amountText = (TextView) mView.findViewById(R.id.amount_d);
        TextView itogoText = (TextView) mView.findViewById(R.id.itogo_d);
        amountText.setText(mProduct.getAmount());
        itogoText.setText(mProduct.getItogo());
        if (mAmount == 0) {
            // checkBox.setChecked(false);
            myCheck.setChecked(false);
            remove();
        } else {
            //  checkBox.setChecked(true);
            myCheck.setChecked(true);
            addSelect();
        }

        if (this.iActionE != null) {
            iActionE.action(mProduct);
        }
        dismiss();
    }

    private boolean validateActionAmount(double amount_core) {
        if (amount_core > mProduct.amount_action_max) {
            Toast.makeText(getActivity(), getString(R.string.sd23) + String.valueOf(mProduct.amount_action_max) + getString(R.string.ddfdflk), Toast.LENGTH_LONG).show();
            return false;
        }
        double amountCurrent = Utils.AmountCurrent(mAction.idu, mProduct.get_id()) + amount_core;
        boolean res = true;
        if (amountCurrent > mAction.currentDeripaskeJs.amount_max) {
            Toast.makeText(getActivity(), getString(R.string.askiud) + mAction.currentDeripaskeJs.amount_max + getString(R.string.ddfdflk), Toast.LENGTH_LONG).show();
            res = false;
        }
        return res;
    }

    private void addSelect() {
        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
            MProduct deleteP = null;
            for (MProduct product : mSettings.getVisit().selectProductForPresent) {
                if (product.rule_id.equals(mRuleId) && product.idu.equals(mProduct.idu)) {
                    deleteP = product;
                }
            }
            if (deleteP != null) {
                mSettings.getVisit().selectProductForPresent.remove(deleteP);
            }

            MProduct product = Configure.getSession().get(MProduct.class, mProduct.idu);
            product.rule_id = mRuleId;
            product.setAmountCore(mProduct.getAmount_core());
            product.price = mProduct.price;
            mSettings.getVisit().selectProductForPresent.add(product);
        }
        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {
            removeCore(mSettings.getVisit().selectProductForSale, (MProduct) mProduct);
            mSettings.getVisit().selectProductForSale.add((MProduct) mProduct);
        }
        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            removeCore(mSettings.getVisit().selectProductForBalance, (MProduct) mProduct);
            mSettings.getVisit().selectProductForBalance.add((MProduct) mProduct);
        }

        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
            removeActionProduct((MProduct) mProduct);

            mProduct.rule_id = SettingsStorageAction.core().mAction_new.get_id();
            mSettings.getVisit().selectProductForActions.add((MProduct) mProduct);
            mSettings.save();
        }

        Settings.save();
    }

    private void removeActionProduct(MProduct product) {
        MAction mAction = SettingsStorageAction.core().mAction_new;
        List<MProduct> deleteList = new ArrayList<>();
        List<MProduct> as = mSettings.getVisit().selectProductForActions;
        for (MProduct mActionProduct : as) {//&&mActionProduct.action_item_id.equals(mAction.getCurrentItem().idu)
            if (mActionProduct.idu.equals(product.idu) && mActionProduct.rule_id.equals(mAction.idu)) {
                deleteList.add(mActionProduct);
            }
        }
        mSettings.getVisit().selectProductForActions.removeAll(deleteList);
    }

    private void remove() {

        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
            if (mProduct.rule_id != null && mRuleId != null) {
                if (mProduct.rule_id.equals(mRuleId)) {
                    removeCorePresent(mSettings.getVisit().selectProductForPresent, (MProduct) mProduct);
                }
            }
        }

        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
            removeCore(mSettings.getVisit().selectProductForActions, (MProduct) mProduct);
        }

        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {
            removeCore(mSettings.getVisit().selectProductForSale, (MProduct) mProduct);
        }

        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            removeCore(mSettings.getVisit().selectProductForBalance, (MProduct) mProduct);
        }

        Settings.save();
    }

    private void removeCore(List<MProduct> list, MProduct mProduct) {
        List<MProduct> deleteList = new ArrayList<>();
        for (MProduct product : list) {
            if (product.idu.equals(mProduct.idu)) {
                deleteList.add(product);
            }
        }
        for (MProduct product : deleteList) {
            list.remove(product);
        }
    }

    private void removeCorePresent(List<MProduct> list, MProduct mProduct) {
        List<MProduct> deleteList = new ArrayList<>();
        for (MProduct product : list) {
            if (product.idu.equals(mProduct.idu)) {
                deleteList.add(product);
            }
        }
        for (MProduct product : deleteList) {
            list.remove(product);
        }
    }
}
