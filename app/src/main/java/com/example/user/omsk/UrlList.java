package com.example.user.omsk;

import com.example.user.omsk.setting.IListItem;
import com.example.user.omsk.setting.Item;

import java.util.ArrayList;
import java.util.List;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/



public class UrlList implements IListItem {
    @Override
    public List<Item> getList() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("Локальный тест сервер 80 port", "192.168.199.14:80"));
        items.add(new Item("Удаленный тест сервер SSL", "178.62.249.118"));
        items.add(new Item("Рабочий сервер", "bs000.net"));//178.62.242.187
        return items;
    }
}
