package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.orm2.Configure;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
class ListAdapterForSearchPoints extends ArrayAdapter<MPoint> implements IPainter {

    private final List<MVisitStory> mVisitStories;
    private final List<MRoute> mRoutes;
    private final Context context;
    private final int mResource;
    private boolean isShowDebt;
    private HashSet<String> mStringHashSet;
    private Settings mSettings;
    private List<MPoint> mPointList;
    private final String strSearch;
    private Set<String> mListIdDebt;

    ArrayMap<Integer, View> map = new ArrayMap<Integer, View>();


    ListAdapterForSearchPoints(Context context, int resource, List<MPoint> objects, boolean isShowowDebt, Settings mSettings, Set<String> listIdDebt,String strSearch) {
        super(context, resource, objects);
        this.context = context;
        this.mResource = resource;
        mPointList = objects;
        this.strSearch = strSearch;
        mVisitStories =Configure.getSession().getList(MVisitStory.class, null);
        mRoutes = Configure.getSession().getList(MRoute.class, null);
        this.isShowDebt = isShowowDebt;
        this.mStringHashSet = Utils.getHashSet();
        this.mSettings = mSettings;
        this.mListIdDebt = listIdDebt;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View mView = convertView;

        final MPoint p = getItem(position);
        if (p != null) {

            if (map.containsKey(position)) {
                mView = map.get(position);
            } else {
                mView = LayoutInflater.from(getContext()).inflate(mResource, null);

                if (mSettings.getVisit() != null && mSettings.getVisit().point != null && mSettings.getVisit().point.idu.equals(p.get_id())) {
                    mView.setBackgroundResource(R.color.colore97);
                }


                LinearLayout routePanel = (LinearLayout) mView.findViewById(R.id.route_panel);
                //ImageView imageView = (ImageView) mView.findViewById(R.id.imageView_route);
                ImageView imageAlert = (ImageView) mView.findViewById(R.id.image_alert);
                //ImageView imageTask = (ImageView) mView.findViewById(R.id.image_task);
                TextView textViewName = (TextView) mView.findViewById(R.id.point_name_route);

                if (p.getAlertPoint() != null) {
                    imageAlert.setVisibility(View.VISIBLE);
                } else {
                    imageAlert.setVisibility(View.INVISIBLE);
                }

                if (isShowDebt) {
                    String s1 = "[ " + Utils.getStringDecimal(p.debt) + getContext().getString(R.string.rub) + " ]";//.getDebitCore()
                    String s2 = s1 + "\n" + p.name;
                    Spannable spannable = new SpannableString(s2);
                    spannable.setSpan(new ForegroundColorSpan(Color.RED), 0, s1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    textViewName.setText(spannable, TextView.BufferType.SPANNABLE);
                } else {
                    textViewName.setText(p.name);
                    if (mStringHashSet.size() > 0) {
                        if (mStringHashSet.contains(p.idu)) {
                            textViewName.setTextColor(mSettings.colorTextPointVisit);
                        } else {
                            textViewName.setTextColor(mSettings.colorTextPointNoVisitNew);
                        }
                    } else {
                        textViewName.setTextColor(mSettings.colorTextPointNoVisitNew);
                    }
                }

                if(strSearch!=null&&strSearch.length()>0&&textViewName.getText().length()>0){
                    Utils.drawSearchText(textViewName,strSearch);
                }







                if (Linq.toStream(mRoutes).any(new com.example.user.omsk.linq2.Predicate<MRoute>() {
                    @Override
                    public boolean apply(MRoute t) {
                        return t.point_id.equals(p.idu);
                    }
                })) {
                    //imageView.setImageResource(R.drawable.red_point);
                    routePanel.setBackgroundResource(R.color.route_red);

                }

                MVisitStory story = Linq.toStream(mVisitStories).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MVisitStory>() {
                    @Override
                    public boolean apply(MVisitStory t) {
                        return t.point_id.equals(p.idu);
                    }
                });

                if (story != null) {
                    if (story.isNewObject()) {
                        // imageView.setImageResource(R.drawable.yellow_point);

                        routePanel.setBackgroundResource(R.color.route_yelo);
                    } else {
                        //imageView.setImageResource(R.drawable.green_point);
                        routePanel.setBackgroundResource(R.color.route_green);
                    }
                }
                map.put(position, mView);
            }


        } else {
            // значит показываем список долгов по точкам
            mView = LayoutInflater.from(getContext()).inflate(R.layout.item_debet_itogo, null);
            TextView textView = (TextView) mView.findViewById(R.id.itogo_debet);
            double itogo = 0;
            for (MPoint point : mPointList) {
                if (point == null) continue;
                itogo = itogo + point.debt;
            }
            textView.setText(getContext().getString(R.string.itogo) + Utils.getStringDecimal(itogo) + getContext().getString(R.string.rub));

        }
        if (p != null && mListIdDebt != null && mListIdDebt.contains(p.idu)) {
            mView.findViewById(R.id.image_debt).setVisibility(View.VISIBLE);
        }
        mView.setTag(p);
        return mView;
    }

    public void notifyAddin(int position) {


        for (View view : map.values()) {
            //view.setBackgroundColor(Color.TRANSPARENT);
            view.setBackgroundResource(R.color.colore3);
        }
        if (map.containsKey(position)) {
            map.get(position).setBackgroundResource(R.color.colore97);
        }
    }
}
