package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.speech.RecognizerIntent;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.settings.SettingsActivity;
import com.example.user.omsk.chat.DiscussArrayAdapter;
import com.example.user.omsk.kassa.AnnulCheck;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MChat;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MFreePresentTYpes;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPhoto;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MPriceType;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.models.MVideo;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.plan.MBasePlanPair;
import com.example.user.omsk.plan.MPlanPair;
import com.example.user.omsk.plan.MPlanPointProducts;
import com.example.user.omsk.plan.ModelMML;
import com.example.user.omsk.plan.ModelMMLOld;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.visitcontrol.VisitMarketing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.provider.Settings.Secure;
import static com.example.user.omsk.orm2.Configure.getSession;

//import com.example.user.omsk.models.MError;

public class Utils {
    public static org.apache.log4j.Logger log = Logger.getLogger(Utils.class);

    private static final String nodate = "Данные отсутствуют";
    public static final int IMAGE_WIDTH = 100;

    public static final int IMAGE_HEIGHT = 100;

    public static final int DELTA_DATE = 5;
    public static long MAX_FILE_SIZE = 12 * 1048576;
    public static final String HTTP = "https://";
    public static final String[] listABC = {"А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й",
            "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ы", "Э", "Ю", "Я"};
    public static final double MAX_DIS = 199d;
    public static final double LATITUDE = 56.819715;
    public static final double LONGITUDE = 60.640623;
    static final String BASE_NAME = "omsk_base.sqlite";
    final static boolean RELOAD_BASE = false;
    final static String templateVersion = "app-release_";
    //private final static boolean DEBUG = false;
    static LinearLayout layoutPanel;
    private static Map<String, Double> map = null;


    public static String simpleDateFormatE(Date date) {

        if (date == null) {
            return nodate;
        }
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return dateFormat.format(date);
    }


    public static String simpleDateFormatForCommentsE(Date date) {
        if (date == null) {
            return nodate;
        }
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }


    public static boolean isNetworkAvailable(Context context, Settings settings) {
        if (settings.checkNetForSender) {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } else {
            return true;
        }

    }

    public static void messageBox(final String title, final String message, final Activity activity, final IActionE iAction) {
        if (activity == null) return;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
                TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
                TextView tmessage = (TextView) v.findViewById(R.id.message_core);
             //  Button button1 = (Button) v.findViewById(R.id.button1);
                Button button2 = (Button) v.findViewById(R.id.button2);
                Button button3 = (Button) v.findViewById(R.id.button3);
                builder.setView(v);
                final AlertDialog alert = builder.create();

                titl.setText(title);
                tmessage.setText(message);
                if (iAction != null) {

                    button2.setVisibility(View.VISIBLE);
                    button2.setText("Закрыть");
                    button2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            alert.dismiss();
                        }
                    });

                }
                String but = "Ok";
                if (iAction == null) {
                    but = "Закрыть";
                }
                button3.setText(but);
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (iAction != null) {
                            iAction.action(null);
                        }
                        alert.dismiss();
                    }
                });


                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        });
    }

    private static void messageBoxDoubleAction(final String title, final String message, final Activity activity,
                                               final IActionE iAction) {
        if (activity == null) return;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
                TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
                TextView tmessage = (TextView) v.findViewById(R.id.message_core);
               // Button button1 = (Button) v.findViewById(R.id.button1);
              //  Button button2 = (Button) v.findViewById(R.id.button2);
                Button button3 = (Button) v.findViewById(R.id.button3);
                builder.setView(v);
                final AlertDialog alert = builder.create();

                titl.setText(title);
                tmessage.setText(message);

                button3.setText("Ok");
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (iAction != null) {
                            iAction.action(null);
                        }
                        alert.dismiss();
                    }
                });

                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        });
    }

    public static void messageBoxE(final Activity activity, final String title, final String message,
                                   final String buttonNegative,
                                   final String buttonPositive, final IActionE iActionNegative, final IActionE iActionPisitive, final boolean isOnDiasmis) {
        if (activity == null) return;
        final boolean[] run = {false};
        activity.runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
                TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
                TextView tmessage = (TextView) v.findViewById(R.id.message_core);
               // Button button1 = (Button) v.findViewById(R.id.button1);
                Button button2 = (Button) v.findViewById(R.id.button2);
                Button button3 = (Button) v.findViewById(R.id.button3);
                builder.setView(v);
                final AlertDialog alert = builder.create();

                titl.setText(title);
                tmessage.setText(message);

                button2.setVisibility(View.VISIBLE);
                button2.setText(buttonNegative);
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        run[0] = true;
                        if (iActionNegative != null) {
                            iActionNegative.action(null);

                        }
                    }
                });
                button3.setText(buttonPositive);
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        run[0] = true;
                        if (iActionPisitive != null) {
                            iActionPisitive.action(null);
                        }
                        alert.dismiss();
                    }
                });


                if (isOnDiasmis) {
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (!run[0]) {
                                iActionNegative.action(null);
                            }
                        }
                    });
                }

                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        });
    }

    static String getUUID(Context context) {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public static boolean isPointInsideToView(float x, float y, View view) {
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];
        return (x > viewX && x < (viewX + view.getWidth())) &&
                (y > viewY && y < (viewY + view.getHeight()));
    }

    private static Object cloneObject(Object obj) {
        try {
            Object clone = obj.getClass().newInstance();
            for (Field field : obj.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.get(obj) == null || Modifier.isFinal(field.getModifiers())) {
                    continue;
                }
                if (field.getType().isPrimitive() || field.getType().equals(String.class)
                        || field.getType().getSuperclass().equals(Number.class)
                        || field.getType().equals(Boolean.class)) {
                    field.set(clone, field.get(obj));
                } else {
                    Object childObj = field.get(obj);
                    if (childObj == obj) {
                        field.set(clone, clone);
                    } else {
                        field.set(clone, cloneObject(field.get(obj)));
                    }
                }
            }
            return clone;
        } catch (Exception e) {
            log.error(e);
            return null;
        }
    }

    public static void pricecometr(List<MProduct> mProducts, Settings settings) {
        List<MPriceType> types = getSession().getList(MPriceType.class, null);
        String idTypePrice = null;
        if (Settings.getStateSystem() == StateSystem.SHOW_STOCK_SIMPLE ||
                Settings.getStateSystem() == StateSystem.HOME ||
                Settings.getStateSystem() == StateSystem.SEARCH_POINT) {
            for (MPriceType type : types) {
                if (type.is_default) {
                    idTypePrice = type.idu;
                }
            }
        } else {

            idTypePrice = settings.getVisit().point.price_type_id;

        }

        for (MProduct mProduct : mProducts) {

            mProduct.price = mProduct.getPrice(idTypePrice);

        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static String getUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String streamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        return sb.toString();
    }

    static int getPositionSpinner(List<IUsingGuidId> list, String guid) {

        if (list == null || list.size() == 0) {
            return 0;
        }
        int res = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).get_id().equals(guid)) {
                res = i;
            }
        }
        return res;
    }

    synchronized static void addChatMessages(List<MChat> mChats, ISession ses, DiscussArrayAdapter mAdapter, Context context) {
        Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        for (MChat mChat : mChats) {
            MChat s = ses.get(MChat.class, mChat.get_id());
            if (s == null) {
                ses.insert(mChat);
                if (mAdapter != null) {
                    mAdapter.add(mChat);
                    vibe.vibrate(100);
                }
            }
        }
    }

    static boolean isExsistDataToServer() {


        try {

            String sds = Configure.getBaseName();
            ISession session = getSession();
            int total = 0;
            total = (int) session.executeScalar("select count(*) from point where state_object = 1 ");
            if (total > 0) return true;
            total = (int) session.executeScalar("select count(*) from visit_story where state_object = 1 ");
            if (total > 0) return true;


            total = (int) session.executeScalar("select count(*) from checks where is_sender = 0 ");
            if (total > 0) return true;
            total = (int) session.executeScalar("select count(*) from zreport");
            if (total > 0) return true;
            total = (int) session.executeScalar("select count(*) from auto where is_sender = 1");
            if (total > 0) return true;


            List<MPhoto> mPhotos = Utils.getPhotos(session);
            for (MPhoto s : mPhotos) {
                File f = new File(s.path);
                if (f.exists() && f.length() > 0) {
                    ++total;
                }
            }
            if (total > 0) return true;
            List<MVideo> mVideos = Utils.getVideo(session);
            for (MVideo s : mVideos) {
                File f = new File(s.path);
                if (f.exists() && f.length() > 0) {
                    ++total;
                }
            }
            return !(total == 0);


        } catch (Exception ex) {

            log.error(ex);
            return false;

        }

    }

    public static String getStringDecimal(double value) {
        return String.format("%.2f", value);
    }

    public static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static double getDistance(double latitude1, double longitude1, double latitude2, double longitude2) {

        Location location = new Location("");
        location.setLatitude(latitude1);
        location.setLongitude(longitude1);
        Location loc1 = new Location("");
        loc1.setLatitude(latitude2);
        loc1.setLongitude(longitude2);
        return (double) location.distanceTo(loc1);
    }

    static void confirmCommitVisit(Activity activity,  final IActionE cancelAction, final IActionE okIAction) {

        Settings settings= Settings.core();

        StringBuilder message = new StringBuilder();

        message.append(activity.getString(R.string.close_visit_titl)).
                append("\n").
                append(settings.getVisit().point.name);
        message.append("\n\n");
        double dis = getDistance(settings.getVisit().latitude, settings.getVisit().longitude, settings.getVisit().point.latitude, settings.getVisit().point.longitude);

        if (dis > Utils.MAX_DIS) {
            message.append("\n").
                    append("\n").
                    append(activity.getString(R.string.fhgfhgfhfgh)).
                    append("\n").
                    append(activity.getString(R.string.assddasd)).
                    append(String.valueOf(Utils.MAX_DIS)).
                    append(activity.getString(R.string.fggggggggdfg)).
                    append(Utils.getStringDecimal(dis)).append(" м.").
                    append("\n");
        }

        message.append("\n");

        if (settings.getVisit().selectProductForBalance.size() > 0) {//&&settings.getVisit().isSaveBalance
            message.append("\n").
                    append(activity.getString(R.string.balance_point)).
                    append("\n");
            Collections.sort(settings.getVisit().selectProductForBalance, new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            int i = 0;
            for (MProduct mProduct : settings.getVisit().selectProductForBalance) {
                if (mProduct.getAmount_core() == 0) continue;
                int del = 7 - String.valueOf(++i).length();
                StringBuilder dl = new StringBuilder();
                for (int f = 0; f < del; f++) {
                    dl.append(" ");
                }
                message.append("  ").append(i).append(".").append(dl).append(mProduct.name).append(" - ").append(mProduct.getAmount_core()).append(" ").append(mProduct.getUnit());
                message.append("\n");
            }
        }

        if (settings.getVisit().selectProductForSale.size() > 0) {
            message.append("\n").
                    append(activity.getString(R.string.saled_product)).
                    append("\n");
            int i = 0;
            double amount = 0;
            double itogo = 0;
            Collections.sort(settings.getVisit().selectProductForSale, new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            for (MProduct mProduct : settings.getVisit().selectProductForSale) {
                if (mProduct.price == 0 || mProduct.getAmount_core() == 0) continue;
                int del = 7 - String.valueOf(++i).length();
                String dl = "";
                for (int f = 0; f < del; f++) {
                    dl = dl + " ";
                }

                message.append("  ").append(i).append(".").append(dl).append(mProduct.name).append(" - ").append(mProduct.getAmount_core()).append(" ").append(mProduct.getUnit());

                message.append("\n");
                amount = amount + mProduct.getAmount_core();
                itogo = itogo + mProduct.getAmount_core() * mProduct.price;
            }
            message.append("\n").
                    append(activity.getString(R.string.itogo_amount)).append(Utils.getStringDecimal(amount)).
                    append("\n").
                    append(activity.getString(R.string.itogo_price2)).append(Utils.getStringDecimal(itogo)).append(activity.getString(R.string.rub)).
                    append("\n");

        }

        if (settings.getVisit().selectProductForPresent.size() > 0) {
            message.append("\n\n\n").append(activity.getString(R.string.add_present)).append("\n");

            message.append(activity.getString(R.string.sadasd)).
                    append(settings.getVisit().member_action).
                    append("\n");

            double itogo = 0;
            double amount = 0;
            int i = 0;

            for (MProduct mProduct : settings.getVisit().selectProductForPresent) {
                if (mProduct.price == 0 || mProduct.getAmount_core() == 0) continue;
                int del = 7 - String.valueOf(++i).length();
                String dl = "";
                for (int f = 0; f < del; f++) {
                    dl = dl + " ";
                }

                message.append("  ").
                        append(i).append(".").
                        append(dl).
                        append(mProduct.name).
                        append(" - ").
                        append(mProduct.getAmount_core()).
                        append(" ").append(mProduct.getUnit()).
                        append("\n").
                        append("     ").
                        append(dl).
                        append("Основание: ");
                MFreePresentTYpes present = getSession().get(MFreePresentTYpes.class, mProduct.rule_id);
                if (present == null) {
                    message.append(" не определена.");
                } else {
                    message.append(present.name);
                }
                message.append("\n");
                amount = amount + mProduct.getAmount_core();
                itogo = itogo + mProduct.getAmount_core() * mProduct.price;
            }

            message.append("\n").
                    append(activity.getString(R.string.itogo_amount_present)).
                    append(Utils.getStringDecimal(amount)).
                    append("\n").
                    append(activity.getString(R.string.itogo_price_present)).
                    append(Utils.getStringDecimal(itogo)).
                    append(activity.getString(R.string.rub)).
                    append("\n\n\n");

        }

        if (settings.getVisit().selectProductForActions.size() > 0) {
            message.append("\n").
                    append(activity.getString(R.string.dsgg)).
                    append("\n").
                    append(activity.getString(R.string.fddddhdfh)).
                    append(settings.getVisit().member_action).append("\n");

            Map<String, List<MProduct>> map = new HashMap<>();

            for (MProduct product : settings.getVisit().selectProductForActions) {

                if (map.containsKey(product.rule_id)) {

                    map.get(product.rule_id).add(product);
                } else {
                    List<MProduct> mProducts = new ArrayList<>();
                    mProducts.add(product);
                    map.put(product.rule_id, mProducts);
                }
            }


            for (Map.Entry<String, List<MProduct>> ss : map.entrySet()) {
                MAction action = getSession().get(MAction.class, ss.getKey());
                message.
                        append("По акции: ").
                        append(action.name).append("\n");
                int ii = 0;
                for (MProduct product : ss.getValue()) {
                    message.
                            append("  ").
                            append(++ii).
                            append(".      ").
                            append(product.name).
                            append(" - ").
                            append(product.getAmount_core()).
                            append(activity.getString(R.string.ddfdflk)).
                            append("\n");
                }
            }
        }
        if (settings.getVisit().selectPresents.size() > 0) {
            message.append("\n");
            message.append("Произведена выдача подарков:");
            int ii = 0;
            for (MVisitPresent vp : settings.getVisit().selectPresents) {
                message.
                        append("\n").
                        append("  ").
                        append(++ii).
                        append(".      ").
                        append(vp.getName()).
                        append("     - ").
                        append(String.valueOf(vp.amount));

            }
            message.append("\n");
        }
        Date d_start =Utils.intToDate(settings.getVisit().getStart());
        Date d_finish =Utils.intToDate(settings.getVisit().getStart());
        message.append("\n").
                append(activity.getString(R.string.start_time_visit)).append(Utils.simpleDateFormatE(d_start)).
                append("\n").
                append(activity.getString(R.string.finish_visit_time)).append(settings.getVisit().getFinish() == 0 || settings.getVisit().getFinish() == 0 ?
                Utils.simpleDateFormatE(Utils.curDate()) :
                Utils.simpleDateFormatE(d_finish)).
                append("\n").
                append(activity.getString(R.string.visit_result_titl)).append(settings.getVisit().getResultatVisit()).
                append("\n");
        if (settings.getVisit().selectProductForSale.size() > 0) {
            message.append(activity.getString(R.string.visit_order_type));
            MOrderType ot = settings.getVisit().getOrderTypeCore();
            if (ot == null) {
                message.append(activity.getString(R.string.none_type));
            } else {
                message.append(ot.name);
            }
            message.append("\n");
            if (ot.index == 1) {
                message.append(activity.getString(R.string.many_arm));
                message.append("0.0 ").append(activity.getString(R.string.rub));
            }
        }
        double debt = Utils.getDebt(settings.getVisit().debtList);
        message.append("\n").
                append(activity.getString(R.string.return_debt)).
                append(Utils.getStringDecimal(debt)).append(activity.getString(R.string.rub)).
                append("\n");
        if (settings.getVisit().concurent_actions != null && settings.getVisit().concurent_actions.trim().length() > 0) {
            message.append("\n").
                    append(activity.getString(R.string.hgfgh)).append("\n").
                    append(settings.getVisit().concurent_actions).
                    append("\n");
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
        TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
        TextView tmessage = (TextView) v.findViewById(R.id.message_core);
        Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        Button button3 = (Button) v.findViewById(R.id.button3);
        builder.setView(v);
        final AlertDialog alert = builder.create();
        titl.setText(activity.getString(R.string.confirm_visit));
        tmessage.setText(message.toString());
        button2.setVisibility(View.VISIBLE);
        button2.setText(activity.getString(R.string.cancel_visit));
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cancelAction != null) {
                    cancelAction.action(null);
                }
                alert.dismiss();
            }
        });

        button3.setText(activity.getString(R.string.close_visit));
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (okIAction != null) {
                    okIAction.action(null);
                }
                alert.dismiss();
            }
        });

        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    static void showNoteRealise(Activity activity, Settings settings, final IActionE cancelAction, final IActionE okIAction) {


        AssetManager am = activity.getAssets();
        InputStream is = null;
        try {
            is = am.open("note");
        } catch (IOException ignored) {
            return;
        }

        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }
            r.close();
        } catch (IOException ignored) {
            return;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
        TextView title = (TextView) v.findViewById(R.id.titl_dialog);
        TextView tmessage = (TextView) v.findViewById(R.id.message_core);
        //Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        Button button3 = (Button) v.findViewById(R.id.button3);
        builder.setView(v);
        final AlertDialog alert = builder.create();

        title.setText(activity.getString(R.string.note_new));
        tmessage.setText(total.toString());


        if (cancelAction != null) {
            button2.setVisibility(View.VISIBLE);
            button2.setText(activity.getString(R.string.note_cancel));
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cancelAction != null) {
                        cancelAction.action(null);
                    }
                    alert.dismiss();
                }
            });
        }


        button3.setText(activity.getString(R.string.close));
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (okIAction != null) {
                    okIAction.action(null);
                }
                alert.dismiss();
            }
        });

        alert.setCanceledOnTouchOutside(false);
        alert.show();
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        alert.getWindow().setLayout(width - 30, height - 30); //Controlling width and height.


    }


    public static String getPromotionDirectory() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/promotion";
    }

    public static List<MPhoto> getPhotos(ISession ses) {

        return ses.getList(MPhoto.class, null);
    }

    static void deleteDdirectoryPhotoPoint(String id) {

        File file = new File(VisitMarketing.getPhotoDirectory() + "/" + id);

        if (file.exists()) {
            file.delete();
        }
    }

    public static List<MVideo> getVideo(ISession session) {
        return session.getList(MVideo.class, null);
    }

    public static String getResultVisite(String id) {

        String res = "";
        MVisitResults sd = Configure.getSession().get(MVisitResults.class, id);

        if (sd != null) {
            res = sd.name;
        }
        return res;
    }


    public static String simpleDateFormatForRouteE(Date date) {

        if (date == null) {
            return nodate;
        }
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        return dateFormat.format(date);

    }


    public static String getTypePayment(String id) {

        String res = "";
        MOrderType mOrderType = Configure.getSession().get(MOrderType.class, id);

        if (mOrderType != null) {
            res = mOrderType.name;
        }
        return res;
    }

    public static MOrderType getMOrderType(String id) {
        return getSession().get(MOrderType.class, id);
    }

    static void checkAgent(Settings mSettings) {

        if (mSettings.getVisit() != null && mSettings.getVisit().point != null) {
            mSettings.getVisit().point.setAgent(null);
        }
    }

    public static int getWidthPlanMML(int count, Context context) {
        int _width = 0;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int yy = ((width) / 100) * 7;
        int w_name = context.getResources().getDimensionPixelSize(R.dimen.plan_name_widtch);
        int w_total = context.getResources().getDimensionPixelSize(R.dimen.plan_name_total);
        int w_total2 = context.getResources().getDimensionPixelSize(R.dimen.plan_name_total2);
        int w_abk = context.getResources().getDimensionPixelSize(R.dimen.plan_name_abk);
        int e = yy + w_name + w_total + w_total2 + w_abk - 15;

        if (count == 0) {

            _width = 1;

        } else {

            _width = (width - e) / count;

        }

        return _width;
    }

    public static boolean gruz200(InputStream stream) {
        int ch;
        StringBuilder sb = new StringBuilder();

        try {

            while ((ch = stream.read()) != -1) {
                sb.append((char) ch);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = sb.toString();

        int i = 0;
        try {

            i = Integer.parseInt(res.trim());

        } catch (Exception ignored) {

        }
        return i == 200;
    }


    public static String getErrorTrace(Exception e) {

        if (e == null || e.getStackTrace() == null) return "null ex_ion";

        StackTraceElement[] stackTraceElements = e.getStackTrace();
        StringBuilder builder = new StringBuilder();
        builder.append("Exception: ").append(e.getClass().getName()).append("\n")
                .append("Message: ").append(e.getMessage()).append("\nStacktrace:\n");

        for (StackTraceElement element : stackTraceElements) {
            builder.append("\t").append(element.toString()).append("\n");
        }

        return builder.toString();
    }

    static boolean deleteDirectorySettingsOmsk(String path) {

        File f = new File(path);

        if (f.isDirectory()) {
            File[] ff=f.listFiles();
            if(ff!=null){
                for (File c : ff)
                    c.delete();
            }

        }

        return f.delete();
    }

    public static void sendMessage(String msg, MyApplication application) {

        new SenderErrorPOST().send(application, msg);

    }

    public static ProgressDialog factoryDialog(Activity mActivity, String msg, final IActionE iAction) {

        ProgressDialog dialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
        msg = msg + mActivity.getString(R.string.weweee);
        dialog.setMessage(msg);
        dialog.setIndeterminate(true);

        if (iAction == null) {
            dialog.setCancelable(false);
        } else {
            dialog.setCancelable(true);
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    iAction.action(null);
                }
            });
        }


        return dialog;
    }

    public static void getIntent(Context context, Class<?> cls) {
//        Intent intent = new Intent(context, cls);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        context.startActivity(intent);
    }

    static HashSet<String> getHashSetProduct(MPoint point) {

        HashSet<String> res = new HashSet<>();
        List<MPlanPointProducts> list = getSession().getList(MPlanPointProducts.class, " point_id = ? ", point.idu);

        for (MPlanPointProducts mPlanPointProducts : list) {

            res.add(mPlanPointProducts.product_id);
        }

        List<MVisitStory> storyList = getSession().getList(MVisitStory.class, " point_id = ? ", point.idu);

        for (MVisitStory vs : storyList) {

            for (MProduct product : vs.getSalesProductList()) {
                res.add(product.idu);
            }
        }

        HashSet<String> resCore = new HashSet<>();

        for (String re : res) {
            MProduct product = getSession().get(MProduct.class, re);
            if (product != null) {
                resCore.add(product.sku);
            }
        }

        return resCore;
    }

    static void showCommonMessage(String message, MainActivity activity) {


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
        TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
        TextView tmessage = (TextView) v.findViewById(R.id.message_core);
      //  Button button1 = (Button) v.findViewById(R.id.button1);
      // Button button2 = (Button) v.findViewById(R.id.button2);
        Button button3 = (Button) v.findViewById(R.id.button3);
        builder.setView(v);
        final AlertDialog alert = builder.create();

        titl.setText(activity.getString(R.string.common_message));
        tmessage.setText(message);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        builder.setPositiveButton(activity.getString(R.string.close), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    public static void showHelpNote(FragmentActivity activity, String s) {
        AssetManager am = activity.getAssets();
        InputStream is = null;

        try {

            is = am.open(s);

        } catch (IOException ignored) {
            return;
        }

        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;

        try {
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }
            r.close();
        } catch (IOException ignored) {
            return;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
        TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
        TextView tmessage = (TextView) v.findViewById(R.id.message_core);
       // Button button1 = (Button) v.findViewById(R.id.button1);
       // Button button2 = (Button) v.findViewById(R.id.button2);
        Button button3 = (Button) v.findViewById(R.id.button3);
        builder.setView(v);
        final AlertDialog alert = builder.create();

        titl.setText(activity.getString(R.string.help));
        tmessage.setText(Html.fromHtml(total.toString()));


        button3.setText(activity.getString(R.string.close));
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.setCanceledOnTouchOutside(false);
        alert.show();
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        alert.getWindow().setLayout(width - 30, height - 30);

    }

    public static Date getStartOfDay(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();

    }


    static boolean isSpeechRecognitionActivityPresented(Activity ownerActivity) {

        try {

            PackageManager pm = ownerActivity.getPackageManager();
            List activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);

            if (activities.size() != 0) {
                return true;
            }

        } catch (Exception ignored) {
            log.error(ignored);
        }

        return false;
    }

    static void messageBoxAddressToCoordinate(String address, String latitude, String longitude, FragmentActivity activity,
                                              IActionE cancel, final IActionE ok) {

        StringBuilder stringBuilder = new StringBuilder();
        if (latitude == null || latitude.trim().length() == 0) {
            stringBuilder.append(activity.getString(R.string.wrrrrrewr)).
                    append(address)
                    .append("\n")
                    .append(activity.getString(R.string.dsf))
                    .append(activity.getString(R.string.xcvxcv)).append("\n")
                    .append(activity.getString(R.string.sdd)).append("\n");

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
            TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
            TextView tmessage = (TextView) v.findViewById(R.id.message_core);
          //  Button button1 = (Button) v.findViewById(R.id.button1);
            Button button2 = (Button) v.findViewById(R.id.button2);
            Button button3 = (Button) v.findViewById(R.id.button3);
            builder.setView(v);
            final AlertDialog alert = builder.create();

            titl.setText(activity.getString(R.string.address_jps));
            tmessage.setText(stringBuilder.toString());
            button2.setVisibility(View.VISIBLE);
            button2.setText(activity.getString(R.string.clearadres));
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ok != null) {
                        ok.action(null);
                    }
                    alert.dismiss();
                }
            });
            button3.setText(activity.getString(R.string.assa233));
            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });

            alert.setCanceledOnTouchOutside(false);
            alert.show();
        } else {
            stringBuilder.append(activity.getString(R.string.dfg)).
                    append(address)
                    .append("\n").append(activity.getString(R.string.dffff)).append(longitude)
                    .append("\n").append(activity.getString(R.string.ghfjghj)).append(latitude).append("\n")
                    .append(activity.getString(R.string.xcxcxcxcxcbxbc))
                    .append(activity.getString(R.string.vjghj)).append("\n")
                    .append(activity.getString(R.string.bvjghghghghghj)).append("\n");

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
            TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
            TextView tmessage = (TextView) v.findViewById(R.id.message_core);
           // Button button1 = (Button) v.findViewById(R.id.button1);
            Button button2 = (Button) v.findViewById(R.id.button2);
            Button button3 = (Button) v.findViewById(R.id.button3);
            builder.setView(v);
            final AlertDialog alert = builder.create();

            titl.setText(activity.getString(R.string.address_jps));
            tmessage.setText(stringBuilder.toString());
            button2.setVisibility(View.VISIBLE);
            button2.setText(activity.getString(R.string.assa133));
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });

            button3.setText(activity.getString(R.string.assa233));
            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ok != null) {
                        ok.action(null);
                    }
                    alert.dismiss();
                }
            });
            alert.setCanceledOnTouchOutside(false);
            alert.show();
        }
    }

    static void messageBoxCoordinateToAddress(String address, String latitude, String longitude, FragmentActivity activity,
                                              IActionE cancel, final IActionE ok) {

        StringBuilder stringBuilder = new StringBuilder();

        if (address == null || address.trim().length() == 0) {
            stringBuilder.append(activity.getString(R.string.fgdfgg))
                    .append("\n").append(" Широта: ").
                    append(latitude)
                    .append("\n")
                    .append(" Долгота: ")
                    .append(longitude)
                    .append("\n")
                    .append("\n")
                    .append(activity.getString(R.string.gdfgdfg))

                    .append(activity.getString(R.string.xcvxcvxcv)).append("\n")
                    .append(activity.getString(R.string.gfjfgj)).append("\n");

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
            TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
            TextView tmessage = (TextView) v.findViewById(R.id.message_core);
          //  Button button1 = (Button) v.findViewById(R.id.button1);
            Button button2 = (Button) v.findViewById(R.id.button2);
            Button button3 = (Button) v.findViewById(R.id.button3);
            builder.setView(v);
            final AlertDialog alert = builder.create();

            titl.setText(activity.getString(R.string.address_jps));
            tmessage.setText(stringBuilder.toString());
            button2.setVisibility(View.VISIBLE);
            button2.setText(activity.getString(R.string.clearadres));
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });

            button3.setText(activity.getString(R.string.assa133));
            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ok != null) {
                        ok.action(null);
                    }
                    alert.dismiss();
                }
            });

            alert.setCanceledOnTouchOutside(false);
            alert.show();

        } else {
            stringBuilder.append(activity.getString(R.string.sdfsdf))
                    .append("\n")
                    .append(" Широта: ")
                    .append(latitude)
                    .append("\n").append(" Долгота: ")
                    .append(longitude)
                    .append("\n")
                    .append("Ваш адрес: ")
                    .append(address)
                    .append("\n")
                    .append(activity.getString(R.string.assfafs))
                    .append(activity.getString(R.string.eqweeeeeeeqwe)).append("\n")
                    .append(activity.getString(R.string.qweeeeeeeqe)).append("\n");


            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
            TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
            TextView tmessage = (TextView) v.findViewById(R.id.message_core);
           // Button button1 = (Button) v.findViewById(R.id.button1);
            Button button2 = (Button) v.findViewById(R.id.button2);
            Button button3 = (Button) v.findViewById(R.id.button3);
            builder.setView(v);
            final AlertDialog alert = builder.create();

            titl.setText(activity.getString(R.string.address_jps));
            tmessage.setText(stringBuilder.toString());
            button2.setVisibility(View.VISIBLE);
            button2.setText(activity.getString(R.string.assa133));
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });

            button3.setText(activity.getString(R.string.assa233));
            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ok != null) {
                        ok.action(null);
                    }
                    alert.dismiss();
                }
            });

            alert.setCanceledOnTouchOutside(false);
            alert.show();
        }
    }

    // обнуление проданого продукта
    public static void setSalesProducts() {
        map = null;
    }

    public static synchronized Map<String, Double> getSalesProducts() {

        if (map == null) {

            map = new HashMap<>();
            ISession session = getSession();
            List<MVisitStory> list = session.getList(MVisitStory.class, null);

            for (MVisitStory mVisitStory : list) {

                for (MProduct product : mVisitStory.getSalesProductList()) {
                    if (map.containsKey(product.idu)) {
                        double d = map.get(product.idu) + product.getAmount_core();
                        map.put(product.idu, d);
                    } else {
                        map.put(product.idu, product.getAmount_core());
                    }
                }

                for (MProduct product : mVisitStory.getActionProductList()) {
                    if (map.containsKey(product.idu)) {
                        double d = map.get(product.idu) + product.getAmount_core();
                        map.put(product.idu, d);
                    } else {
                        map.put(product.idu, product.getAmount_core());
                    }
                }
            }
        }

        return map;
    }

    public static double AmountCurrent(String idu, String current_product_id) {

        double acum = 0;
        Settings mSettings = Settings.core();

        for (MProduct p : mSettings.getVisit().selectProductForActions) {
            if (p.idu.equals(current_product_id)) continue;

            if (p.rule_id.equals(idu)) {
                acum = acum + p.getAmount_core();
            }

        }

        return acum;
    }

    static void messageBoxCloseRoute(final String title, final Activity activity, final IActionE iAction) {

        activity.runOnUiThread(new Runnable() {
            public void run() {
                ISession session = getSession();
                List<MRoute> routeList = session.getList(MRoute.class, null);
                List<MTemplatePropety> startRoutes = session.getList(MTemplatePropety.class, null);

                MTemplatePropety mTemplatePropery = null;

                if (startRoutes.size() > 0) {
                    mTemplatePropery = startRoutes.get(startRoutes.size() - 1);
                }
                List<MVisitStory> visitList = session.getList(MVisitStory.class, null);
                if (visitList == null) return;
                StringBuilder message = new StringBuilder();

                if (mTemplatePropery != null) {
                    message.append(activity.getString(R.string.asasasasasasas)).append(Utils.simpleDateFormatE(mTemplatePropery.date)).append("\n");
                    message.append(activity.getString(R.string.dfff)).append(Utils.simpleDateFormatE(Utils.curDate())).append("\n");


                    double dff = (Utils.curDate().getTime() / 1000 - mTemplatePropery.date.getTime() / 1000) / 60;
                    if (dff > 1) {
                        message.append(activity.getString(R.string.cvxv)).append((int) dff).append(" мин.").append("\n");
                    }
                    message.append("\n");
                }
                message.append(activity.getString(R.string.ewewrwerr)).append(String.valueOf(routeList.size())).append("\n");
                message.append(activity.getString(R.string.fdgggggdfg)).append(String.valueOf(visitList.size())).append("\n\n");

                if (visitList.size() > 0) {
                    if (routeList.size() != visitList.size()) {
                        message.append(activity.getString(R.string.dfgdg)).append("\n");
                    }
                    double deltaVisit = 0;
                    for (MVisitStory visitStory : visitList) {
                        deltaVisit = deltaVisit + visitStory.finish - visitStory.start;
                    }
                    double ii = (deltaVisit / visitList.size()) / 60000;
                    message.append("\n\n");
                    message.append(activity.getString(R.string.asdddasd)).append(String.valueOf((int) ii)).append(" мин.").append("\n");

                    if (mTemplatePropery != null) {
                        double sd = (Utils.curDate().getTime() - mTemplatePropery.date.getTime() - deltaVisit) / (visitList.size() + 1);
                        int df = (int) (sd / 60000);
                        message.append(activity.getString(R.string.bvnvbn)).append(String.valueOf(df)).append(" мин.").append("\n");
                    }

                    if (visitList.size() > 0) {
                        int error = 0;

                        for (MVisitStory visitStory : visitList) {
                            Location location = new Location("");
                            location.setLatitude(visitStory.latitude);
                            location.setLongitude(visitStory.longitude);
                            MPoint point = getSession().get(MPoint.class, visitStory.point_id);
                            if (point == null) continue;
                            Location location1 = new Location("");
                            location1.setLatitude(point.latitude);
                            location1.setLongitude(point.longitude);
                            double dis = location.distanceTo(location1);
                            if (dis > Utils.MAX_DIS) {
                                error++;
                            }
                        }

                        if (error > 0) {
                            message.append("\n");
                            message.append(activity.getString(R.string.ghghjghjgj)).append(error).append(" точек.").append("\n");
                        }
                    }
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
                TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
                TextView tmessage = (TextView) v.findViewById(R.id.message_core);
                Button button1 = (Button) v.findViewById(R.id.button1);
                Button button2 = (Button) v.findViewById(R.id.button2);
                Button button3 = (Button) v.findViewById(R.id.button3);
                builder.setView(v);
                final AlertDialog alert = builder.create();

                titl.setText(title);
                tmessage.setText(message.toString());
                button2.setVisibility(View.VISIBLE);
                button2.setText(activity.getString(R.string.sdfsdfsdf));
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                button3.setText(R.string.dsfsdfsdf);
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (iAction != null) {
                            iAction.action(null);
                        }
                        alert.dismiss();
                    }
                });

                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        });

    }

    public static double getDebt(List<String> debtList) {

        if (debtList == null) {
            return 0;
        }

        if (debtList.size() == 0) {
            return 0;
        }
        double res = 0;
        ISession session = getSession();

        for (String s : debtList) {
            List<MDebt> mDebtList = session.getList(MDebt.class, " visit_id = ? ", s);
            for (MDebt mDebt : mDebtList) {
                res = res + mDebt.price * mDebt.amount;
            }
        }

        return res;
    }

    /**
     * словарь точек с задолжностью. ключь - гуид точки
     *
     * @return
     */
    static Map<String, MPoint> getMapDebtPoint() {
        ISession session = getSession();
        List<MOrderType> types = session.getList(MOrderType.class, " index1 = 1 ");
        List<MVisitStory> stories = session.getList(MVisitStory.class, null);
        List<MPoint> pointList = session.getList(MPoint.class, null);

        List<String> returns = new ArrayList<>();

        for (MVisitStory story : stories) {
            returns.addAll(story.debtList);
        }

        String ss = "ksksa";

        if (types.size() > 0) {
            ss = types.get(0).idu;
        }
        ////////////////////////////////////////////////////// addin
        List<MDebt> mDebtList = session.getList(MDebt.class, null);

        class titi {
            private double debt;
        }

        Map<String, titi> mapPointDebt = new HashMap<>();

        for (MDebt mDebt : mDebtList) {
            if (returns.contains(mDebt.order_id)) continue;
            if (mapPointDebt.containsKey(mDebt.point_id)) {
                mapPointDebt.get(mDebt.point_id).debt = mapPointDebt.get(mDebt.point_id).debt + mDebt.amount * mDebt.price;
            } else {
                titi t = new titi();
                t.debt = mDebt.amount * mDebt.price;
                mapPointDebt.put(mDebt.point_id, t);
            }
        }

        for (MVisitStory story : stories) {
            if (story.order_type_id != null && story.order_type_id.equals(ss)) {
                for (MProduct product : story.getSalesProductList()) {
                    if (mapPointDebt.containsKey(story.point_id)) {
                        mapPointDebt.get(story.point_id).debt = mapPointDebt.get(story.point_id).debt + product.getAmount_core() * product.price;
                    } else {
                        titi t = new titi();
                        t.debt = product.getAmount_core() * product.price;
                        mapPointDebt.put(story.point_id, t);
                    }
                }
            }
        }

        for (final Map.Entry<String, titi> sss : mapPointDebt.entrySet()) {
            MPoint point = Linq.toStream(pointList).firstOrDefault(new Predicate<MPoint>() {
                @Override
                public boolean apply(MPoint t) {
                    return t.idu.equals(sss.getKey());
                }
            });
            if (point == null) continue;
            point.debt = point.debt + sss.getValue().debt;
        }

        //////////////////////////////////////////////////////////////
        List<MPoint> lisrDebt = Linq.toStream(pointList).where(new Predicate<MPoint>() {
            @Override
            public boolean apply(MPoint t) {
                return t.debt > 0;
            }
        }).toList();

        List<MPoint> tempList = new ArrayList<>(lisrDebt);
        Map<String, MPoint> pointMap = new HashMap<>(tempList.size());

        for (MPoint point : tempList) {
            pointMap.put(point.get_id(), point);
        }

        return pointMap;
    }

    public static double getDebtPoint(MPoint pointa) {

        Map<String, MPoint> pointMap = Utils.getMapDebtPoint();

        if (pointMap.containsKey(pointa.idu)) {
            return pointMap.get(pointa.idu).debt;
        } else {
            return 0d;
        }
    }

    static HashSet<String> getHashSet() {

        ISession ses = getSession();
        List<MPlanPair> list = ses.getList(MPlanPair.class, null);
        HashSet<String> stringHashSet = new HashSet<>(list.size());

        for (MBasePlanPair mBasePlanPair : list) {
            stringHashSet.add(mBasePlanPair.point_id);
        }

        List<MVisitStory> stories = ses.getList(MVisitStory.class, null);
        List<MVisitResults> listE = getSession().getList(MVisitResults.class, " index1 = 1 ");

        if (listE.size() == 1) {
            MVisitResults r = listE.get(0);
            for (MVisitStory story : stories) {
                if (story.visit_result_id.equals(r.idu)) {
                    stringHashSet.add(story.point_id);
                }
            }
        }

        return stringHashSet;
    }

    public static void decorator(LinearLayout mLinearLayout, Settings mSettings, float scale) {

        if (scale > 1 || mSettings.isShowSku) {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mLinearLayout.getLayoutParams();
            int i = 50;// params.height ;
            i = i + 40;
            params.height = i;
            mLinearLayout.setLayoutParams(params);
        }
    }

    private static boolean areThereMockPermissionApps(Context context) {
        int count = 0;
        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages =
                pm.getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo applicationInfo : packages) {
            PackageInfo packageInfo = null;
            try {
                packageInfo = pm.getPackageInfo(applicationInfo.packageName,
                        PackageManager.GET_PERMISSIONS);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String[] requestedPermissions = packageInfo.requestedPermissions;
            if (requestedPermissions != null) {
                for (String requestedPermission : requestedPermissions) {
                    if (requestedPermission
                            .equals("android.permission.ACCESS_MOCK_LOCATION")
                            && !applicationInfo.packageName.equals(context.getPackageName())) {
                        count++;
                    }
                }
            }
        }

        return count > 0;

    }

    /**
     * задолжность по точке с учетом текущий хпродаж
     *
     * @param pointId id точки
     * @return
     */
    public static List<MDebt> getDebtForPoint(String pointId) {
        ISession session = Configure.getSession();
        List<MDebt> mDebtList = session.getList(MDebt.class, " point_id = ?", pointId);
        String s = session.getList(MOrderType.class, " index1 = 1").get(0).idu;
        List<MVisitStory> mVisitStories = session.getList(MVisitStory.class, " order_type = ? ", s); // получить все отсрочки

        for (MVisitStory mv : mVisitStories) {
            if (mv.point_id.equals(pointId)) { // еслт отсрочка по искомой точке, то конвертируем и добавляем в результат
                for (MProduct product : mv.getSalesProductList()) {
                    MDebt debt = new MDebt();
                    debt.amount = product.getAmount_core();
                    debt.price = product.price;
                    //debt.date = mv.date;
                    debt.point_id = mv.point_id;
                    debt.product_id = product.idu;
                    mDebtList.add(debt);
                }
            }
        }

        return mDebtList;
    }

    public static void showSettingKassa(Activity activity) {

        try {
            Fptr fptr = new Fptr();
            fptr.create(activity);
            fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa);
            Intent intent = new Intent(activity, SettingsActivity.class);
            intent.putExtra(SettingsActivity.DEVICE_SETTINGS, fptr.get_DeviceSettings());
            activity.startActivityForResult(intent, 1);
        } catch (Exception ex) {
            log.error(ex);
            Utils.messageBox("", ex.getMessage(), activity, null);
        }


    }


    public static boolean isDelay(MVisit mVisit) {

        String s = Configure.getSession().getList(MOrderType.class, " index1 = 1").get(0).idu;

        if (mVisit.order_type_id == null) {
            return true;
        }

        return mVisit.order_type_id.equals(s);
    }

    public static void validateProduct(List<MProductBase> product_list, ISession ses) {

        for (MProductBase product : product_list) {
            MProduct mProduct = ses.get(MProduct.class, product.idu);
            if (mProduct == null) {

                continue;
            }
            product.nameKKT = mProduct.nameKKT;
            product.name = mProduct.name;
            product.sku = mProduct.sku;
        }
    }

    public static double getMmlAmount(String s, boolean isOld) {
        double res = 0d;
        if (!isOld) {
            List<ModelMML> mmls = Configure.getSession().getList(ModelMML.class, "mml_name = ?", s.trim());
            if (mmls.size() > 0) {
                res = mmls.get(0).mmlPachek;
            }
        } else {
            List<ModelMMLOld> mmls = Configure.getSession().getList(ModelMMLOld.class, "mml_name = ?", s.trim());
            if (mmls.size() > 0) {
                res = mmls.get(0).mmlPachek;
            }
        }
        return res;
    }

    public static int getMmlCount(boolean isOld) {

        int res = 0;
        if (!isOld) {
            List<ModelMML> mmls = Configure.getSession().getList(ModelMML.class, null);
            res = mmls.size();
        } else {
            List<ModelMMLOld> mmls = Configure.getSession().getList(ModelMMLOld.class, null);
            res = mmls.size();
        }
        return res;

    }

    public static List<String> getStringMml(boolean isOld) {
        List<String> res = new ArrayList<>();
        if (!isOld) {
            List<ModelMML> mmls = Configure.getSession().getList(ModelMML.class, " 1 = 1 order by mml_name");
            for (ModelMML mml : mmls) {
                res.add(mml.mmlName);
            }

        } else {
            List<ModelMMLOld> mmls = Configure.getSession().getList(ModelMMLOld.class, " 1 = 1 order by mml_name");
            for (ModelMMLOld mml : mmls) {
                res.add(mml.mmlName);
            }
        }
        return res;
    }

    public static double getMmlPoint(String s, boolean isOld) {
        double res = 0d;
        if (!isOld) {
            List<ModelMML> mmls = Configure.getSession().getList(ModelMML.class, "mml_name = ?", s.trim());
            if (mmls.size() > 0) {
                res = mmls.get(0).mmlPoint;
            }
        } else {
            List<ModelMMLOld> mmls = Configure.getSession().getList(ModelMMLOld.class, "mml_name = ?", s.trim());
            if (mmls.size() > 0) {
                res = mmls.get(0).mmlPoint;
            }
        }
        return res;
    }


    public static boolean isLocationServiceEnabled(Activity activity) {
        LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void copyDirectoryOneLocationToAnotherLocation(File sourceLocation, File targetLocation)
            throws IOException {

        try (InputStream in = new FileInputStream(sourceLocation)) {
            try (OutputStream out = new FileOutputStream(targetLocation)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }

    }

    public static boolean getCheckSale(List<MChecksData> checksDataList) {
        for (final MChecksData data : checksDataList) {
            if (data.type_check != MChecksData.TYPE_CHECK_CACH) continue;

            final MChecksData dat = Linq.toStream(checksDataList).firstOrDefault(new Predicate<MChecksData>() {
                @Override
                public boolean apply(MChecksData t) {
                    return t.type_check == MChecksData.TYPE_CHECK_RETURN && t.checkUUIDDelete.equals(data.idu);
                }
            });
            if (dat == null) {
                return true;
            }

        }
        return false;
    }

    public static void messageBoxPartalCheck(final List<List<MProductBase>> lists, final Activity activity, final IActionE<List<List<MProductBase>>> iAction, final IActionE iActionCancel) {
        final StringBuilder ss = new StringBuilder("");
        double dd = 0;
        int f = 0;
        for (List<MProductBase> list : lists) {

            double itogo = 0;
            f++;

            ss.append("   ЧЕК №-").append(f);
            int df = 0;
            for (MProductBase product : list) {
                df++;
                itogo = itogo + product.getAmount_core() * product.price;
                ss.
                        append("\n").
                        append(df).
                        append(" - ").
                        append(product.name).
                        append("  ").
                        append(String.valueOf(product.getAmount_core())).
                        append(" пач. ").
                        append(String.valueOf(product.price)).
                        append(" руб.");

            }
            ss.
                    append("\n\n").
                    append("Итого чек:  ").
                    append(Utils.getStringDecimal(itogo)).
                    append(" руб.").
                    append("\n\n");
            dd = dd + itogo;


        }
        ss.append("ИТОГО:  ").append(Utils.getStringDecimal(dd)).append(" руб.").append("\n\n");
        ss.append(activity.getString(R.string.qweqwewee));


        activity.runOnUiThread(new Runnable() {


            public void run() {

                String sss = String.format("Внимание!\nПечать единого чека невозможна, " +
                        "из за ограничения итоговой суммы чека: %s руб.\nЧек будет разбит на %s чека:\n\n%s\n\n P.S. " +
                        "Или разделите визит на несколько визитов.", String.valueOf(Settings.core().maxSummPrice), String.valueOf(lists.size()), ss.toString());


                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
                TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
                TextView tmessage = (TextView) v.findViewById(R.id.message_core);
            //    Button button1 = (Button) v.findViewById(R.id.button1);
                Button button2 = (Button) v.findViewById(R.id.button2);
                Button button3 = (Button) v.findViewById(R.id.button3);
                builder.setView(v);
                final AlertDialog alert = builder.create();

                titl.setText("Разбиение чека на части");
                tmessage.setText(sss);
                button2.setVisibility(View.VISIBLE);
                button2.setText("Закрыть");
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iActionCancel.action(null);
                        alert.dismiss();
                    }
                });

                button3.setText("Печатать");
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (iAction != null) {
                            iAction.action(lists);
                        }
                        alert.dismiss();
                    }
                });

                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        });
    }


    public static void messageBoxDoubleCheck(final Activity activity, int type_check, final List<MProductBase> lists, final IActionE<Object> iActionOk) {
        final StringBuilder ss = new StringBuilder("");
        if (type_check == MChecksData.TYPE_CHECK_CACH) {
            ss.append("Тип чека - Текущая продажа\n");
        }
        if (type_check == MChecksData.TYPE_CHECK_DEBT) {
            ss.append("Тип чека - Возврат долга\n");
        }
        ss.append("СПИСОК ТОВАРА:\n");
        double itogo = 0;
        int df = 0;
        for (MProductBase product : lists) {
            df++;
            itogo = itogo + product.getAmount_core() * product.price;
            ss.
                    append("\n").
                    append(df).
                    append(" - ").
                    append(product.name).
                    append("  ").
                    append(String.valueOf(product.getAmount_core())).
                    append(" пач. ").
                    append(String.valueOf(product.price)).
                    append(" руб.");
        }
        ss.append("\nИТОГО:  ").append(Utils.getStringDecimal(itogo)).append(" руб.").append("\n\n");
        activity.runOnUiThread(new Runnable() {
            public void run() {
                String sss = "Внимание!\nЕсли Bы видете это предупреждение, " +
                        "есть подозрение задваивания чека.\nЧек напечатанный ранее и есть ваш желанный чек.\n" +
                        "В результате сбоя планшет  вероятно не смог зафиксировать печать чека.\n" +
                        "Что бы это проверить, сделайте повторную печать последнего документа.\n" +
                        "Если вы не разобрались в текущей ситуации - позвоните старшему.";


                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
                TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
                TextView tmessage = (TextView) v.findViewById(R.id.message_core);
               // Button button1 = (Button) v.findViewById(R.id.button1);
                Button button2 = (Button) v.findViewById(R.id.button2);
                Button button3 = (Button) v.findViewById(R.id.button3);
                builder.setView(v);
                final AlertDialog alert = builder.create();

                titl.setText("Признак задваивания чека");
                tmessage.setText(sss);
                button2.setVisibility(View.VISIBLE);
                button2.setText("Закрыть");
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        alert.dismiss();
                    }
                });

                button3.setText("Печатать");
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (iActionOk != null) {
                            iActionOk.action(null);
                        }
                        alert.dismiss();
                    }
                });

                alert.setCanceledOnTouchOutside(false);
                alert.show();
            }
        });
    }


    public static List<AnnulCheck.TempCheck> getTempCheck(String id_partial) {
        List<AnnulCheck.TempCheck> listRes = new ArrayList<>();
        List<MVisitStory> list = Configure.getSession().getList(MVisitStory.class, null);
        for (MVisitStory mVisitStory : list) {
            for (MChecksData data : mVisitStory.getCheckList()) {
                if (data.isReturn) continue;

                if (data.id_partial == null) continue;
                if (data.id_partial.equals(id_partial)) {
                    listRes.add(new AnnulCheck.TempCheck(mVisitStory, data));
                }
            }
        }
        return listRes;
    }

    public static Gson getGson() {


        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

                String ss = json.getAsJsonPrimitive().getAsString();
                if (ss.equals("null")) {
                    return null;
                }
              //  long dffd = json.getAsJsonPrimitive().getAsLong();
                String ssss = json.getAsJsonPrimitive().getAsString();
                try {
                    int ddd = Integer.parseInt(ssss);
                    long dd = (long) ddd * 1000;
                    return new Date(dd);
                } catch (Exception c) {
                    log.error(c);
                    long dd = json.getAsJsonPrimitive().getAsLong();
                    return new Date(dd);
                }
            }
        });

        builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
                if (date == null) {
                    return new JsonPrimitive("null");
                }
                int dd = (int) (date.getTime() / 1000);
                return new JsonPrimitive(dd);
            }
        });

        return builder.serializeNulls().create();
    }

    public static Gson getGsonSettings() {


        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

                String ss = json.getAsJsonPrimitive().getAsString();
                if (ss.equals("null")) {
                    return null;
                }
                try {
                    long dd = json.getAsJsonPrimitive().getAsLong();
                    return new Date(dd);
                }catch (Exception e){
                    return null;
                }


            }
        });

        builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
                if (date == null) {
                    return new JsonPrimitive("null");
                }
                return new JsonPrimitive(date.getTime());
            }
        });

        return builder.serializeNulls().create();
    }

    public static void leftAbs(LinearLayout ll, Activity activity) {
        if (ll == null) return;
        activity.findViewById(R.id.head_panel);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins((int) activity.getResources().getDimension(R.dimen.picabu), 0, 0, 0);
        ll.setLayoutParams(params);
    }

    static <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }

    public static void drawSearchText(TextView textViewName, String strSearch) {
        if (strSearch == null) return;
        String d = strSearch.trim();
        if (d.length() == 0) return;
        SpannableStringBuilder sb = new SpannableStringBuilder(textViewName.getText().toString());
        Pattern pa = Pattern.compile(strSearch, Pattern.CASE_INSENSITIVE);
        Matcher m = pa.matcher(textViewName.getText().toString());
        while (m.find()) {
            sb.setSpan(new ForegroundColorSpan(Color.rgb(255, 0, 0)), m.start(), m.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
        textViewName.setText(sb);
    }

    public static Date curDate() {
        return Calendar.getInstance().getTime();
    }

    public static List<String> getListError() throws Exception {
        List<String> list = new ArrayList<>();

        File file=new File(MyApplication.logfile);
        if(file.exists()==false){
            return list;
        }
        try{
            BufferedReader br = new BufferedReader(new FileReader(MyApplication.logfile));
            String line = null;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
            br.close();
        }catch (Exception ignored){

        }

        return list;
    }
    public static String getListErrorFromSender() throws Exception {
        StringBuilder  sb=new StringBuilder();
        File file=new File(Environment.getExternalStorageDirectory().toString() + File.separator + "log");
        if(file.exists()==false) return sb.toString();
        File[] files=file.listFiles();
        if(files==null){
            return sb.toString();
        }
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
            return   o1.getName().compareTo(o2.getName());
            }


        });
        for (File file1 : files) {
            BufferedReader br = new BufferedReader(new FileReader(file1));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("<br/>\n");
            }
            br.close();
        }
        System.out.print(sb.toString());
        return sb.toString();
    }
    ///////////////////////////
    public static int dateToInt(Date date) {
        return (int) (date.getTime() / 1000);
    }

    //bitnic.development@gmail.com
    public static Date intToDate(int i) {

        return new Date(((long) i * 1000));
    }

    public static double getNallCore() {
        List<MVisitStory> stories = Configure.getSession().getList(MVisitStory.class, null);
        MVisit visit = Settings.core().getVisit();
        double nal = 0;
        boolean exist = false;
        for (MVisitStory ss : stories) {
            if (visit == null) {
                for (MChecksData mChecksData : ss.getCheckList()) {
                    for (MProductBase pp : mChecksData.productList) {
                        double d = pp.getAmount_core() * pp.price;
                        nal = nal + d;
                    }
                }
            } else {
                if (visit.idu.equals(ss.get_id())) {
                    exist = true;
                    for (MChecksData mChecksData : visit.checksDataList) {
                        for (MProductBase pp : mChecksData.productList) {
                            double d = pp.getAmount_core() * pp.price;
                            nal = nal + d;
                        }
                    }
                } else {
                    for (MChecksData mChecksData : ss.getCheckList()) {
                        for (MProductBase pp : mChecksData.productList) {
                            double d = pp.getAmount_core() * pp.price;
                            nal = nal + d;
                        }
                    }
                }
            }

        }
        if (exist == false && visit != null) {
            for (MChecksData mChecksData : visit.checksDataList) {
                for (MProductBase pp : mChecksData.productList) {
                    double d = pp.getAmount_core() * pp.price;
                    nal = nal + d;
                }
            }
        }
        return Utils.round(nal,2);
    }

}

