package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;

import com.example.user.omsk.freesql.FreeSqlActivity;


public class StartFreeSqlActivity {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, FreeSqlActivity.class);
        activity.startActivity(intent);
    }
}

