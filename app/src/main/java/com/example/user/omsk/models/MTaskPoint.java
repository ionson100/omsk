package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

// задачи по точкам
@Table("task_point")
public class MTaskPoint {

    @PrimaryKey("id")
    public int id;

    @Column("task_id")
    public String task_id;

    @Column("point_id")
    public String point_id;

    @Column("result")
    public boolean result;

    @Column("message")
    public String message;
}
