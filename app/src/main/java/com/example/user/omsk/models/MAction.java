package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.IActionOrm;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

@Table("action")
public class MAction implements Serializable, IUsingGuidId {

    @PrimaryKey("id")
    public int _id;

    @SerializedName("id")
    @Column("idu")
    public String idu;

    @SerializedName("name")
    @Column("name")
    public String name;

    @SerializedName("description")
    @Column("description")
    public String description;

    @SerializedName("media")
    @Column("file_name")
    public String file_name;

    @SerializedName("start")
    @Column("start_action")
    public Date start_action_new_22 = null;

    @SerializedName("finish")
    @Column("finish_action")
    public Date finish_action_new_22 = null;

    @Column("is_active")
    @SerializedName("active")
    public boolean isActive;

    @SerializedName("js")
    @Column("items")
    public String javascript;

    public DeripaskeJs currentDeripaskeJs;

    public String get_id() {
        return this.idu;
    }

    public MAction getAction() {
        MAction action = Configure.getSession().get(MAction.class, idu);
        return action;
    }
}

@Table("user")
class User1 implements IActionOrm {
    @PrimaryKey("id")
    public int _id;
    @Column("uuid")
    public String uuid;
    @Column("name")
    public String name_user;

    @Override
    public void actionBeforeUpdate(Object o) {
    }

    @Override
    public void actionAfterUpdate(Object o) {
    }

    @Override
    public void actionBeforeInsert(Object o) {
    }

    @Override
    public void actionAfterInsert(Object o) {

    }

    @Override
    public void actionBeforeDelete(Object o) {
    }

    @Override
    public void actionAfterDelete(Object o) {
    }
}


