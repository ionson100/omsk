package com.example.user.omsk;

import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/



public class SettingsTest implements Serializable {
    int id = 0;

    public static SettingsTest core() {
        return (SettingsTest) Reanimator.get(SettingsTest.class);
    }

    public static void save() {
        Reanimator.save(SettingsTest.class);
    }
}
