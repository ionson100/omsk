package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
import android.util.Log;

public class LoggerE {
    private final static boolean isWrite = BuildConfig.DEBUG;

    public static void logE(String msg) {
        if (isWrite) {
            Log.e("________APP______", msg);
        }
    }

    public static void logI(String msg) {
        if (isWrite) {
            Log.i("________APP______", msg);
        }
    }
}
