package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;

import java.io.Serializable;

public abstract class MTripletPoductBase implements Serializable {

    @PrimaryKey("id")
    public int id;

    @Column("product_id")
    public String product_id;

    @Column("amount")
    public double amount;

    @Column("sale_item_id")
    public int sale_item_id;

    @Column("price")
    public double price;
}

