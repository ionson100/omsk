package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.plan.BasePlanMML;
import com.example.user.omsk.plan.FactoryPlan;
import com.example.user.omsk.plan.MBasePlanPair;
import com.example.user.omsk.plan.PlanE;
import com.example.user.omsk.plan.PlaneBase;
import com.example.user.omsk.plan.TempPlan;
import com.example.user.omsk.plan.UtilsPlan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class MakerPlanForVisitSale {

    static void make(final MPoint mPoint, Activity activity, View mView) {
        LinearLayout mPanelHeader = (LinearLayout) mView.findViewById(R.id.panel_header_plan_sale);
        LinearLayout panel = (LinearLayout) mView.findViewById(R.id.visit_sale_plan);
        List<TempPlan> tempPlanList = FactoryPlan.getTempPlanList(false);
        if (tempPlanList.size() == 0) {

            panel.setVisibility(View.GONE);
            return;
        }


        final TempPlan p = Linq.toStream(tempPlanList).firstOrDefault(new Predicate<TempPlan>() {
            @Override
            public boolean apply(TempPlan t) {
                return t.mPoint.idu.equals(mPoint.idu);
            }
        });

        if (p == null) {
            panel.setVisibility(View.GONE);
            return;
        }

        panel.setVisibility(View.VISIBLE);
        getViewHeaderSale(activity, mPanelHeader, false);
        final List<MBasePlanPair> mPlanPairList;
        mPlanPairList = UtilsPlan.getPlanPairList(false);//
       // PlaneBase mPlanE = null;
//        if (mPlanE == null) {
//            List<PlaneBase> planEs = UtilsPlan.getPlanE(false);
//            if (planEs.size() != 1) {
//                mPlanE = new PlanE();
//            } else {
//                mPlanE = planEs.get(0);
//            }
//        } else {
//
//        }


        LayoutInflater vi;
        vi = LayoutInflater.from(activity);
        View view = vi.inflate(R.layout.item_plan, null);
        TextView textViewName = (TextView) view.findViewById(R.id.plan_name);
        textViewName.setVisibility(View.GONE);
        TextView textViewAcb = (TextView) view.findViewById(R.id.plan_akb);
        TextView textViewTotal = (TextView) view.findViewById(R.id.plan_total);
        textViewTotal.setText(String.valueOf((int) p.mAllSalePachka) + "/" + "\n" + String.valueOf(p.mMedium_line));
        ///////////////////////////////////////////////////////
        TextView textViewTotalRub = (TextView) view.findViewById(R.id.plan_total_rub);
        double totalRub = 0;

        MBasePlanPair pair = Linq.toStream(mPlanPairList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MBasePlanPair>() {
            @Override
            public boolean apply(MBasePlanPair t) {
                return t.point_id.equals(p.mPoint.idu);
            }
        });

        if (pair != null) {
            totalRub = pair.price;
        }
        textViewTotalRub.setText(Utils.getStringDecimal(totalRub));
        textViewAcb.setBackgroundResource(p.getColorABK());
        textViewAcb.setText(String.valueOf(p.mPlanACB) + "/\n" + Utils.getStringDecimal(p.getPercentAKB()) + "%");
        List<String> df = new ArrayList<>(p.MMLMap.keySet());
        Collections.sort(df, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });

        int ww = getWidth(activity, df.size());

        for (String s : df) {

            double mml_amount = Utils.getMmlAmount(s, false);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ww, LinearLayout.LayoutParams.MATCH_PARENT);
            int value = (int) activity.getResources().getDimension(R.dimen.tablemagious);
            params.setMargins(value, 0, 0, 0);
            TextView textView = (TextView) vi.inflate(R.layout.item_text_plan, null);
            textView.setLayoutParams(params);
            if (p.getColorMML(false) == R.color.d5caed) {
                textView.setBackgroundResource(p.getColorMmlItems(s, mml_amount));//mPlanE.mml_amount
            } else {
                textView.setBackgroundResource(p.getColorMML(false));
            }
            textView.setGravity(Gravity.CENTER);
            double d = p.MMLMap.get(s);
            textView.setText(String.valueOf(mml_amount) + "/\n" + String.valueOf((int) d));//(int) mPlanE.mml_amount
            textView.setPadding(10, 0, 0, 0);
            ((LinearLayout) view).addView(textView);
        }
        mPanelHeader.setVisibility(View.VISIBLE);
        panel.addView(view);


    }

    private static View getViewHeaderSale(Activity activity, LinearLayout panel, boolean isOld) {

        List<BasePlanMML> planMMLList = UtilsPlan.getListPlanMML(isOld);
        if (planMMLList.size() == 0) {
            return new View(activity);
        }
        Set<String> stringSet = new HashSet<>();
        for (BasePlanMML planMML : planMMLList) {
            stringSet.add(planMML.mml);
        }
        int i = getWidth(activity, stringSet.size());
        List<String> strings = new ArrayList<>(stringSet);
        Collections.sort(strings, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });

        for (String string : strings) {
            LayoutInflater vi;
            vi = LayoutInflater.from(activity);
            TextView textView = (TextView) vi.inflate(R.layout.item_text_rotate, null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(i, LinearLayout.LayoutParams.MATCH_PARENT);
            textView.setLayoutParams(params);
            int value = (int) activity.getResources().getDimension(R.dimen.tablemagious);
            params.setMargins(value, 0, 0, 0);
            textView.setPadding(0, 5, 0, 0);
            textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            textView.setText(string);
            panel.addView(textView);
        }
        return new View(activity);
    }

    private static int getWidth(Activity activity, int sizes) {
        int _width = 0;
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int w_total = activity.getResources().getDimensionPixelSize(R.dimen.plan_name_total);
        int w_total2 = activity.getResources().getDimensionPixelSize(R.dimen.plan_name_total2);
        int w_abk = activity.getResources().getDimensionPixelSize(R.dimen.plan_name_abk);
        int e = w_total + w_total2 + w_abk + 50;
        if (sizes == 0) {
            _width = 1;
        } else {
            _width = (width - e) / sizes;
        }
        return _width;
    }
}
