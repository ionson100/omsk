package com.example.user.omsk.senders;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public class TempProductSender {

    public String product_id;

    public double amount;

    public double price;

    public String sku;

    String permit_id;
}
