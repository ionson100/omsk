package com.example.user.omsk.refill;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.List;


public class ListAdapterAddinStock extends ArrayAdapter<MProduct> {

    private final int mResource;

    public ListAdapterAddinStock(Context context, int resource, List<MProduct> objects) {
        super(context, resource, objects);
        this.mResource = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater vi = LayoutInflater.from(getContext());
        final View mView = vi.inflate(mResource, null);
        final MProduct p = getItem(position);
        TextView nameProduct = (TextView) mView.findViewById(R.id.lock_name_product);
        final TextView amountProduct = (TextView) mView.findViewById(R.id.lock_amount_product);
        nameProduct.setText(p.name);
        if (p.getAmount_core() > 0) {
            amountProduct.setText(Utils.getStringDecimal(p.getAmount_core()));
        } else {
            amountProduct.setText("");
        }
        mView.setTag(p);
        return mView;
    }
}
