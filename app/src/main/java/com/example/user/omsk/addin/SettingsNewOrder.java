package com.example.user.omsk.addin;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.setting.Reanimator;

import java.util.Date;

/**
 * ion100 on 20.09.2017.
 */

public class SettingsNewOrder {
    public Date startNew_22 = null;
    public Date finishNew_22 = null;

    public static SettingsNewOrder core() {
        return (SettingsNewOrder) Reanimator.get(SettingsNewOrder.class);
    }

    public static void save() {
        Reanimator.save(SettingsNewOrder.class);
    }
}
