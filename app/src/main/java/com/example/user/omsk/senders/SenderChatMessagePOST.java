package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.AsyncTask;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MChat;

import com.example.user.omsk.SettingsChat;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

public class SenderChatMessagePOST {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderChatMessagePOST.class);
    private String url;
    private MChat mChat;
    private List<MChat> mChats;
    private MyApplication application;
    private IActionE<Object> iAction;

    public void send(MyApplication application, String url, MChat mChat, List<MChat> mChats, IActionE<Object> iAction) {
        if (SettingsUser.core().isAutorise() == false) {
            return;
        }
        this.application = application;
        this.iAction = iAction;
        this.url = url;
        this.mChat = mChat;
        this.mChats = mChats;
        new SenderWorkerTack().executeOnExecutor(THREAD_POOL_EXECUTOR,mChat);//.execute(mChat);
    }


    private String getParams(chatez chatez) {
        Gson sd = Utils.getGson();
        return sd.toJson(chatez);
    }


    private String senderCore(String json) {

        String str = null;
        str = UtilsSender.postRequest(application, Utils.HTTP + url + "/message_client/", json, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {

                App(str);

            }
        });
        return str;
    }

    private void App(String res) {
        Gson sd = Utils.getGson();
        List<MChat> mChats1 = sd.fromJson(res, new TypeToken<List<MChat>>() {
        }.getType());
        for (MChat chat : mChats1) {

            mChats.add(chat);
        }
    }

    static class chatez {
        @SerializedName("text")
        private final String text;
        @SerializedName("last_date")
        private final int last_date;
        @SerializedName("uuid")
        private String uuid;

        public chatez(String uuid, String text, int last_date) {
            this.uuid = uuid;
            this.text = text;
            this.last_date = last_date;
        }
    }

    private class SenderWorkerTack extends AsyncTask<MChat, Void, String> {

        @Override
        protected void onPostExecute(final String str) {
            try{
                MainActivity.mainActivityInstance.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (str == null) {
                            SettingsChat.core().setLast_date_chat_new_22(Utils.curDate());
                            if (iAction != null) {
                                iAction.action(mChats);
                            }else {
                                log.error("chat-"+ str);
                            }
                        }
                    }
                });
            }catch (Exception ignored){

                log.error(ignored);
            }
        }

        @Override
        protected String doInBackground(MChat... params) {
            mChat = params[0];
            if(SettingsChat.core().getLast_date_chat_new_22()==null){
                SettingsChat.core().setLast_date_chat_new_22(Utils.curDate());
            }
            String sd = getParams(new chatez(SettingsUser.core().getUuidDevice(), mChat.text, (int) (SettingsChat.core().getLast_date_chat_new_22().getTime() - 10)));
            String res = senderCore(sd);
            return res;
        }
    }
}


