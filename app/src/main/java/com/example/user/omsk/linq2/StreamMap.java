package com.example.user.omsk.linq2;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

class StreamMap<TKey, TValue> implements SessionMap<TKey, TValue> {

    private final Map<TKey, TValue> mMap;

    public StreamMap(Map<TKey, TValue> map) {
        this.mMap = map;//pair
    }

    @Override
    public SessionMap<TKey, TValue> whereKey(Predicate<TKey> key) {
        Map<TKey, TValue> map = new HashMap<>();
        for (TKey tkey : mMap.keySet()) {
            if (key.apply(tkey)) {
                map.put(tkey, map.get(tkey));
            }
        }
        return new StreamMap<>(map);
    }

    @Override
    public SessionMap<TKey, TValue> whereValue(Predicate<TValue> value) {
        Map<TKey, TValue> map = new HashMap<>();

        for (Entry<TKey, TValue> ss : mMap.entrySet()) {
            TValue val = ss.getValue();
            if (value.apply(val)) {
                map.put(ss.getKey(), val);
            }
        }

        return new StreamMap<>(map);
    }

    @Override
    public Pair<TKey, TValue> firstOrDefault(Predicate<TValue> valuePredicate) {

        for (Entry<TKey, TValue> ss : mMap.entrySet()) {
            if (valuePredicate.apply(ss.getValue())) {
                return new Pair<>(ss.getKey(), ss.getValue());
            }
        }

        return null;
    }

    @Override
    public Number max(Function<TValue, Number> func) {
        Number acum = null;
        for (TValue d : mMap.values()) {
            acum = Utils.maxNumbers(acum, func.foo(d));
        }
        return acum;
    }

    @Override
    public Number min(Function<TValue, Number> func) {
        Number acum = null;
        for (TValue d : mMap.values()) {
            acum = Utils.minNumbers(acum, func.foo(d));
        }
        return acum;
    }

    @Override
    public Number sum(Function<TValue, Number> func) {
        Number acum = null;
        for (TValue d : mMap.values()) {
            Number sd = func.foo(d);
            acum = Utils.addNumbers(acum, sd);
        }
        return acum;
    }

    @Override
    public List<Pair<TKey, TValue>> toList() {
        List<Pair<TKey, TValue>> res = new ArrayList<>();
        for (Entry<TKey, TValue> ss : mMap.entrySet()) {
            res.add(new Pair<>(ss.getKey(), ss.getValue()));
        }

        return res;
    }

    @Override
    public boolean any(Predicate<TValue> predicate) {
        for (TValue d : mMap.values()) {
            if (predicate.apply(d)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean anyAll(Predicate<TValue> predicate) {
        boolean res = true;
        for (TValue d : mMap.values()) {
            if (!predicate.apply(d)) {
                res = false;
                break;
            }
        }
        return res;
    }

    @Override
    public Map<TKey, TValue> toMap() {
        return mMap;
    }

    @Override
    public int size() {
        return mMap.size();
    }

    @Override
    public boolean isEmpty() {
        return mMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return mMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return mMap.containsValue(value);
    }

    @Override
    public TValue get(Object key) {
        return mMap.get(key);
    }

    @Override
    public TValue put(TKey key, TValue value) {
        return mMap.put(key, value);
    }

    @Override
    public TValue remove(Object key) {
        return mMap.remove(key);
    }

    @Override
    public void putAll(@NonNull Map<? extends TKey, ? extends TValue> m) {
        mMap.putAll(m);
    }

    @Override
    public void clear() {
        mMap.clear();
    }

    @NonNull
    @Override
    public Set<TKey> keySet() {
        return mMap.keySet();
    }

    @NonNull
    @Override
    public Collection<TValue> values() {
        return mMap.values();
    }

    @NonNull
    @Override
    public Set<Entry<TKey, TValue>> entrySet() {
        return mMap.entrySet();
    }
}
