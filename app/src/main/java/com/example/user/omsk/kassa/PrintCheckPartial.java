package com.example.user.omsk.kassa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.senders.SenderErrorPOST;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CACH;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_DEBT;

/**
 * ion100 01.11.2017.
 */

public class PrintCheckPartial {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PrintCheckPartial.class);

    public static void pintCheckCore(final Activity activity, final Settings mSettings, final List<List<MProductBase>> mProductListBase, final int type_check,
                                     final IActionE iActionE) {


        new AsyncTask<Void, String, Void>() {

            String uuid = UUID.randomUUID().toString();
            List<MChecksData> dataList = new ArrayList<MChecksData>();

            private ProgressDialog dialog;
            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            private void openCheck(int type) throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }

                if (fptr.put_CheckType(type) < 0) {
                    checkError();
                }
                if (fptr.OpenCheck() < 0) {
                    checkError();
                }
            }


            private void closeCheck(int typeClose) throws Exception {
                if (fptr.put_TypeClose(typeClose) < 0) {
                    checkError();
                }
                if (fptr.CloseCheck() < 0) {
                    checkError();
                }
            }

            private void registrationFZ54(String name, double price, double quantity) throws Exception {

                if (fptr.put_TaxNumber(3) < 0) {
                    checkError();
                }
                if (fptr.put_Quantity(quantity) < 0) {
                    checkError();
                }
                if (fptr.put_Price(price) < 0) {
                    checkError();
                }
                if (fptr.put_PositionSum(quantity * price) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                    checkError();
                }
                if (fptr.put_Name(name) < 0) {
                    checkError();
                }
                if (fptr.Registration() < 0) {
                    checkError();
                }
            }

            private void reportZ() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REPORT_CLEAR) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError();
                }
                ZetReport mZetReprot = new ZetReportBuilder(fptr).build();
                MZReport m = new MZReport(mZetReprot);
                if (fptr.Report() < 0) {
                    checkError();
                }
                Configure.getSession().insert(m);
                ZetReportBuilder.clearReport();
            }

            private void printText(String text, int alignment, int wrap) throws Exception {
                if (fptr.put_Caption(text) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError();
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError();
                }
                if (fptr.PrintString() < 0) {
                    checkError();
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                for (List<MProductBase> mProductBases : mProductListBase) {
                    dataList.add(print(mProductBases, uuid));
                }

                return null;
            }

            private MChecksData print(List<MProductBase> mProductList, String partia_id) {
                MChecksData checksData = new MChecksData();
                checksData.id_partial = partia_id;
                fptr = new Fptr();


                try {
                    fptr.create(activity.getApplication());


                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    double max = 0;
                    for (MProductBase product : mProductList) {
                        max = max + product.price * product.getAmount_core();
                    }
                    if (max > mSettings.maxSummPrice) {
                        throw new Exception(activity.getString(R.string.maxprice) + "\n max price: (" + mSettings.maxSummPrice + " руб.) ");
                    }
                    // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                    // не являются ошибками, если мы хотим просто отменить чек
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }
                    // Открываем чек продажи, попутно обработав превышение смены
                    try {
                        openCheck(IFptr.CHEQUE_TYPE_SELL);
                    } catch (Exception e) {
                        // Проверка на превышение смены
                        if (fptr.get_ResultCode() == -3822) {
                            reportZ();
                            openCheck(IFptr.CHEQUE_TYPE_SELL);
                        } else {
                            throw e;
                        }
                    }
                    if (mSettings.getVisit() != null) {
                        ////////////////////////////////////////////// отправка данных емайл покупателю
                        if (mSettings.getVisit().point.email != null && mSettings.getVisit().notify == 2 && (type_check == TYPE_CHECK_CACH || type_check == TYPE_CHECK_DEBT)) {
                            if (fptr.put_FiscalPropertyNumber(1008) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyType(Fptr.FISCAL_PROPERTY_TYPE_STRING) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyValue(mSettings.getVisit().point.email) < 0) {
                                checkError();
                            }
                            if (fptr.WriteFiscalProperty() < 0) {
                                checkError();
                            }
                        }
                        if (mSettings.getVisit().point.sms != null && mSettings.getVisit().notify == 1 && (type_check == TYPE_CHECK_CACH || type_check == TYPE_CHECK_DEBT)) {
                            if (fptr.put_FiscalPropertyNumber(1008) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyType(Fptr.FISCAL_PROPERTY_TYPE_STRING) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyValue(mSettings.getVisit().point.sms) < 0) {
                                checkError();
                            }
                            if (fptr.WriteFiscalProperty() < 0) {
                                checkError();
                            }
                        }
                    }
                    Collections.sort(mProductList, new Comparator<MProductBase>() {
                        @Override
                        public int compare(MProductBase lhs, MProductBase rhs) {
                            return lhs.name.compareTo(rhs.name);
                        }
                    });

                    double totalAmount = 0;
                    double totalPrice = 0;
                    for (MProductBase product : mProductList) {
                        totalAmount = totalAmount + product.getAmount_core();
                        totalPrice = totalPrice + (product.getAmount_core() * product.price);
                        registrationFZ54(product.nameKKT, product.price, product.getAmount_core());
                        checksData.productList.add(product);
                    }
                    closeCheck(0);
                    if (mSettings.getVisit() != null) {
                        checksData.sms = mSettings.getVisit().point.sms;
                        checksData.email = mSettings.getVisit().point.email;
                        checksData.notify = mSettings.getVisit().notify;
                    }
                    checksData.type_check = type_check;
                    checksData.totalAmount = totalAmount;
                    checksData.totalPrice = totalPrice;
                    checksData.checkNumber = fptr.get_CheckNumber();
                    checksData.docNumber = fptr.get_DocNumber();
                    checksData.kmmNumber = fptr.get_SerialNumber();
                    mSettings.kkmNumber = fptr.get_SerialNumber();
                    Settings.save();
                    try {
                        checksData.date = Utils.curDate();//Utils.dateToInt(dd);
                    } catch (Exception ex) {
                        checksData.date = Utils.curDate();
                    }

                    ////////////////////////////////////////
                    if (fptr.put_RegisterNumber(10) < 0) {
                        checkError();
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError();
                    }

                    double aDouble = new Double(fptr.get_Summ());

                    PrintKkmSumma.summKKM(aDouble);


                } catch (Exception e) {
                    log.error(e);
                    fptr.CancelCheck();
                    checksData.errorText = e.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - PrintCheckPartial\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return checksData;
            }

            @Override
            protected void onPreExecute() {




                dialog = Utils.factoryDialog(activity, activity.getString(R.string.eqwe), null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                iActionE.action(dataList);
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
            }
        }.execute();
    }
}
