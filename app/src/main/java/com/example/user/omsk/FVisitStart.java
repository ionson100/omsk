package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MComments;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MSaleRows;
import com.example.user.omsk.models.MStockRows;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import org.apache.log4j.Logger;

import java.util.List;

public class FVisitStart extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FVisitStart.class);

    private Settings mSettings;
    private View mView;

    public FVisitStart() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_visit_start, container, false);
        mSettings = Settings.core();
        try {
            TextView labelName = (TextView) mView.findViewById(R.id.label_point_name);

            Pupper debitValue = (Pupper) mView.findViewById(R.id.label_debit_value);
            double dbt = Utils.getDebtPoint(mSettings.getVisit().point);
            debitValue.setPairString(R.string.debit_partial_satert_text, Utils.getStringDecimal(dbt) + getString(R.string.rub));
            labelName.setText(mSettings.getVisit().point.name);
            activateStockStory();
            activateLastVisitStory();
            activateLastComment();
            mView.findViewById(R.id.to_menu).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                }
            });
        } catch (Exception ex) {
            Utils.sendMessage("FVisitStart:" + Utils.getErrorTrace(ex), (MyApplication) getActivity().getApplication());
           log.error(ex);
            Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
        }
        return mView;
    }

    private void activateLastComment() {
        LinearLayout panelHead = (LinearLayout) mView.findViewById(R.id.panel_head_last_comments);
        List<MComments> lasrMComments = mSettings.getVisit().point.getmCommentsList();
        AddItemsComment.add(mView, R.id.panel_last_comments, getActivity(), lasrMComments);
        if (lasrMComments.size() == 0) {
            panelHead.setVisibility(View.GONE);
            mView.findViewById(R.id.panel_head_last_comments_label).setVisibility(View.GONE);
        }
    }

    private void activateLastVisitStory() {

        LinearLayout panel = (LinearLayout) mView.findViewById(R.id.panel_last_visit);
        Pupper dateLastVisit = (Pupper) mView.findViewById(R.id.label_last_date);
        dateLastVisit.setPairString(R.string.last_date, Utils.simpleDateFormatE(mSettings.getVisit().point.lastDateVisit));
        List<MSaleRows> rowsList = Configure.getSession().getList(MSaleRows.class, " point_id = ? ", mSettings.getVisit().point.get_id());
        List<MStockRows> stockRowsList = Configure.getSession().getList(MStockRows.class, " point_id = ? ", mSettings.getVisit().point.get_id());
        TextView label_date_last_sale = (TextView) mView.findViewById(R.id.label_date_last_sale);
        int countName = 0;
        double itogoAmount = 0;
        double itogoE = 0;
        for (MSaleRows mSaleRows : rowsList) {
            countName++;
            itogoAmount = itogoAmount + mSaleRows.amount;
            itogoE = itogoE + mSaleRows.amount * mSaleRows.price;

            LayoutInflater vi = LayoutInflater.from(getActivity());
            View row = vi.inflate(R.layout.item_product_for_start_page_visit2, null);
            TextView name = (TextView) row.findViewById(R.id.product_balance_name);
            TextView amount = (TextView) row.findViewById(R.id.product_balance_amount);
            TextView price = (TextView) row.findViewById(R.id.product_balance_price);
            TextView itogo = (TextView) row.findViewById(R.id.product_balance_itogo);

            MProduct p = mSaleRows.getProdict();
            if (p != null) {
                name.setText(p.name);
                amount.setText(Utils.getStringDecimal(mSaleRows.amount));
                price.setText(Utils.getStringDecimal(mSaleRows.price));
                itogo.setText(Utils.getStringDecimal(mSaleRows.price * mSaleRows.amount));
            }
            panel.addView(row);
        }
        if (rowsList.size() > 0) {
            label_date_last_sale.setText(label_date_last_sale.getText().toString() + ": " + Utils.simpleDateFormatE(rowsList.get(0).date));
        }
        if (stockRowsList.size() > 0) {
            TextView label_last_balance = (TextView) mView.findViewById(R.id.label_last_balance);
            label_last_balance.setText(label_last_balance.getText().toString() + ": " + Utils.simpleDateFormatE(stockRowsList.get(0).date));
        }

        LinearLayout panelItogo = (LinearLayout) mView.findViewById(R.id.start_visit_panel_balance);
        Pupper nameCount = (Pupper) panelItogo.findViewById(R.id.start_visit_amount_name);
        nameCount.setPairString(R.string.itogo_name, String.valueOf(countName));
        Pupper itigoCount = (Pupper) panelItogo.findViewById(R.id.start_visit_amount);
        itigoCount.setPairString(R.string.itogo_count_products1, Utils.getStringDecimal(itogoAmount));
        Pupper itogo = (Pupper) panelItogo.findViewById(R.id.start_visit_itogo);
        itogo.setPairString(R.string.itogo_price3, Utils.getStringDecimal(itogoE) + getString(R.string.rub));
        if (rowsList.size() == 0) {
            label_date_last_sale.setVisibility(View.GONE);
            panel.setVisibility(View.GONE);
            panelItogo.setVisibility(View.GONE);
            TextView none = (TextView) mView.findViewById(R.id.start_visit_no_last_sale);
            none.setText(getString(R.string.last_sale_none));
        }
    }

    private void activateStockStory() {
        LinearLayout panel = (LinearLayout) mView.findViewById(R.id.panel_balance_stock);
        List<MStockRows> list = Configure.getSession().getList(MStockRows.class, " point_id = ? ", mSettings.getVisit().point.get_id());
        for (MStockRows rows : list) {
            LayoutInflater vi = LayoutInflater.from(getActivity());
            View row = vi.inflate(R.layout.item_product_for_start_page_visit, null);
            TextView name = (TextView) row.findViewById(R.id.product_balance_name);
            TextView amount = (TextView) row.findViewById(R.id.product_balance_amount);
            MProduct p = rows.getSimpleProduct();
            if (p != null) {
                name.setText(p.name);
                amount.setText(Utils.getStringDecimal(rows.amount));
                panel.addView(row);
            }
        }
        if (list.size() == 0) {
            TextView none = (TextView) mView.findViewById(R.id.start_visit_balance_none);
            none.setText(R.string.last_balance_none);
            panel.setVisibility(View.GONE);
            mView.findViewById(R.id.label_last_balance).setVisibility(View.GONE);
        }
    }

}
