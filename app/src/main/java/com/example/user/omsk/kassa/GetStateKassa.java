package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

import java.util.Calendar;
import java.util.Date;


public class GetStateKassa {
    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(GetStateKassa.class);
    public static synchronized void get(final TextView log, final Activity activity, final Settings mSettings, final IActionE iActionE) {
        new AsyncTask<Void, String, Void>() {

            StateKKM state;
            private ProgressDialog dialog;
            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }


            @Override
            protected Void doInBackground(Void... params) {

                state = new StateKKM();
                fptr = new Fptr();
                try {
                    fptr.create(activity.getApplication());

                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }

                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");

                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }

                    //////////////////////////////////// не отправленные в офд
                    if (fptr.put_RegisterNumber(44) < 0) {
                        checkError();
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError();
                    }
                    state.notSenderOfd = fptr.get_Count();
                    ////////////////////////////////////////////// дата первого не отправленного
                    if (state.notSenderOfd > 0) {
                        if (fptr.put_RegisterNumber(45) < 0) {
                            checkError();
                        }
                        if (fptr.GetRegister() < 0) {
                            checkError();
                        }
                        state.notSenderFirstDate = StateKKM.getDate(fptr.get_Date(), fptr.get_Time());
                    }

                    state.operator = fptr.get_Operator();
                    state.fiscal = fptr.get_Fiscal();

/////////////////////
                    if (fptr.put_RegisterNumber(17) < 0) {
                        checkError();
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError();
                    }
                    state.date = StateKKM.getDate(fptr.get_Date(), fptr.get_Time());

///////////////////////


                    if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                        checkError();
                    }
                    if (fptr.SetMode() < 0) {
                        checkError();
                    }


                    if (fptr.put_CaptionPurpose(256) < 0) {
                        checkError();
                    }
                    if (fptr.GetCaption() < 0) {
                        checkError();
                    }
                    state.ofdUrl = fptr.get_Caption();
                    ////////////////////////////////////////////////////////////////                   //////////////////////////
                    if (fptr.put_ValuePurpose(301) < 0) {
                        checkError();
                    }
                    if (fptr.GetValue() < 0) {
                        checkError();
                    }
                    state.ofdPort = (int) fptr.get_Value();
/////////////////////////////////////////////////////////////////                    ///////////////////////////
                    state.serialNumer = fptr.get_SerialNumber();
                    state.inn = fptr.get_INN();
                    state.mode = fptr.get_Mode();
                    state.sessionOpen = fptr.OpenSession();
                    state.checkPaperPresent = fptr.get_CheckPaperPresent();
                    state.userPassword = fptr.get_UserPassword();
                    state.model = fptr.get_Model();
                    state.checkNumber = fptr.get_CheckNumber();
                    state.sessionOpened = fptr.get_SessionOpened();
                    state.session = fptr.get_Session();
                    state.checkState = fptr.get_CheckState();
                    state.batteryLow = fptr.get_BatteryLow();


                    state.fiscal = fptr.get_Fiscal();
                } catch (Exception ex) {
                    logE.error(ex);
                    state.errorMessage = ex.getMessage();
                    publishProgress(ex.getMessage());
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - GetStateKassa\n" + ex.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            protected void onPreExecute() {
                log.setText("");
                dialog = Utils.factoryDialog(activity, "Опрос состояния кассы", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                iActionE.action(state);
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }


        }.execute();
    }
}

class StateKKM {

    public int notSenderOfd;
    public Date notSenderFirstDate = null;

    public int operator;
    public boolean fiscal;
    public Date date = null;

    public String serialNumer;
    public String inn;
    public int mode;
    public int sessionOpen;
    public boolean checkPaperPresent;
    public String userPassword;
    public int model;
    public int checkNumber;
    public boolean sessionOpened;
    public int session;
    public int checkState;
    public boolean batteryLow;
    public String errorMessage;
    public String ofdUrl;
    public int ofdPort;

    public static Date getDate(Date date, Date time) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(time);
        int hour = cal1.get(Calendar.HOUR_OF_DAY);
        int min = cal1.get(Calendar.MINUTE);

        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.YEAR, year);
        cal2.set(Calendar.MONTH, month);
        cal2.set(Calendar.DAY_OF_MONTH, day);
        cal2.set(Calendar.HOUR_OF_DAY, hour);
        cal2.set(Calendar.MINUTE, min);
        Date datee = cal2.getTime();

        return datee;

    }
}
