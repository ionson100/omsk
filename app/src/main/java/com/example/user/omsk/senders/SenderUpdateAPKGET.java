package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.example.user.omsk.Certificate;
//import com.example.user.omsk.models.MError;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsStart;
import com.example.user.omsk.StateUpdate;
import com.example.user.omsk.Utils;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

// прием арк файла установки
public class SenderUpdateAPKGET {
    public static org.apache.log4j.Logger log = Logger.getLogger(SenderUpdateAPKGET.class);
    private final Activity mActivity;
    private final Settings mSettings;
    private final String pathApk = Environment.getExternalStorageDirectory().toString();

    public SenderUpdateAPKGET(Activity activity) {
        this.mActivity = activity;
        this.mSettings =Settings.core();
    }


    public void send() {
        new SenderWorkerTack().execute();
    }

    class SenderWorkerTack extends AsyncTask<Void, Void, Boolean> {
        String error = "";
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(mActivity, mSettings)) {
                cancel(true);
                Toast.makeText(mActivity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(mActivity, "Получение АРК с сервера", null);
            dialog.show();

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            if (isCancelled()) return;
            try {
                if (aBoolean) {
                    SettingsStart settingsStart = SettingsStart.core();
                    settingsStart.setIsFirstStartUpdate(StateUpdate.update);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri uri = Uri.fromFile(new File(pathApk + "/" + mSettings.serverVersionName));
                    intent.setDataAndType(uri, "application/vnd.android.package-archive");
                    mActivity.startActivityForResult(intent, 1000);
                } else {
                    Toast.makeText(mActivity, mActivity.getString(R.string.error_update4) + " " + error, Toast.LENGTH_LONG).show();
                }
            } catch (Exception ex) {
                log.error(ex);
                Toast.makeText(mActivity, mActivity.getString(R.string.error_update4), Toast.LENGTH_SHORT).show();
            } finally {
                if (dialog != null) {
                    dialog.cancel();
                }
            }
        }

        @Override
        protected Boolean doInBackground(Void... f_url) {

            if (isCancelled()) return false;
            int count;
            try {
                URL url = new URL(Utils.HTTP + mSettings.url + "/static/update/" + mSettings.serverVersionName);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                ////////////////////////////////////////////////////////////////// addin

                HttpsURLConnection.setDefaultHostnameVerifier(new UtilsSender.NullHostNameVerifier());
                conn.setSSLSocketFactory(UtilsSender.getSslSocketFactory(mActivity.getApplication()));
                //////////////////////////////////////////////////////////////////addin
                String cert = Certificate.getCertificateSHA1Fingerprint(mActivity);
                conn.setInstanceFollowRedirects(false);
                conn.setRequestProperty("Content-user", cert);
                conn.setReadTimeout(10000 /*milliseconds*/);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.connect();
               // int status = conn.getResponseCode();
                if (conn.getContentLength() == 0) {
                    error = "файл нулевой длины";
                    return false;
                }
                InputStream input = conn.getInputStream();
                OutputStream output = new FileOutputStream(pathApk + "/" + mSettings.serverVersionName);
                byte data[] = new byte[1024];
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                error = e.getLocalizedMessage();
                log.error(" download apk -" + e.getMessage());
                //Configure.getSession().insert(new MError());
                return false;
            }
            return true;
        }
    }
}
