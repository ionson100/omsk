package com.example.user.omsk.plan;

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;


@Table("mml")
public class ModelMML {
    @PrimaryKey("id")
    public int id;

    @Column("mml_name")
    public String mmlName;

    @Column("mml_pachek")
    public double mmlPachek;

    @Column("mml_point")
    public double mmlPoint;
}
