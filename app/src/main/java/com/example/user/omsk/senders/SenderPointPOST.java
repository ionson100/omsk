package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.AsyncTask;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.models.MAgent;
import com.example.user.omsk.models.MContact;
import com.example.user.omsk.models.MContactor;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class SenderPointPOST {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderPointPOST.class);
    int statusE = 0;
    String pointName;
    long date_pissemise;
    private final MyApplication application;
    private final Settings mSettings;

    public SenderPointPOST(MyApplication application) {
        this.application = application;
        this.mSettings = Settings.core();
    }

    private String getParamsPoint2(MPoint mPoint) {
        this.date_pissemise = mPoint.date_pessimise;
        Gson sd = Utils.getGson();
        return sd.toJson(mPoint);
    }

    public boolean send(MPoint point) {
        pointName = point.name;
        try {
            new SenderWorkerTack().execute(point).get();
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return !point.isNewObject();
    }


    private String sendCore(String msg, final MPoint point) {

        final String[] res = {null};
        res[0] = UtilsSender.postRequest(application, Utils.HTTP + mSettings.url + "/point_client/", msg, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {

                point.setNewObject(false);
                preparePointAnUpdate(point.id, str);
                log.error(Utils.simpleDateFormatE(Utils.curDate()) + ": Точка отправлена на сервер успешно: " + pointName);

            }
        });
        return res[0];
    }

    private MPoint preparePointAnUpdate(int id, String str) {

        Gson sd = Utils.getGson();
        MPoint point = sd.fromJson(str, MPoint.class);
        point.id = id;
        if (point.getContragent(Configure.getSession()) != null) {
            point.agent_id = point.getContragent(Configure.getSession()).idu;
        }
        ISession ses = Configure.getSession();
        ses.beginTransaction();

        if (point.getContragent(Configure.getSession()) != null) {
            MAgent a = ses.get(MAgent.class, point.getContragent(Configure.getSession()).idu);
            if (a == null) {
                //ses.insert(a);
            } else {
                point.getContragent(Configure.getSession()).id = a.id;
                ses.update(point.getContragent(Configure.getSession()));
            }
        }
        List<MContact> contactList = point.contacts;
        if (contactList != null && contactList.size() > 0) {
            List<MContactor> c = ses.getList(MContactor.class, " point_id = ? ", point.idu);
            for (MContactor mContactor : c) {
                ses.execSQL(" DELETE FROM contact where idu = ? ", mContactor.contact_id);
                ses.delete(mContactor);
            }
            for (MContact mContact : contactList) {
                ses.insert(mContact);
                ses.insert(new MContactor(point.idu, mContact.idu));
            }
        }
       // BigInteger dsd = new BigInteger("89022613151");
        int i = ses.updateWhere(point, " date_pessimise = " + this.date_pissemise);
        if (i == -1 || i == 0) {//  если точка обновилась за вермя передачи на сервер
            ses.endTransaction();
        } else {
            ses.commitTransaction();
            ses.endTransaction();
        }
        return point;
    }

    private class SenderWorkerTack extends AsyncTask<MPoint, Void, String> {
        @Override
        protected String doInBackground(MPoint... params) {
            String message = null;
            message = getParamsPoint2(params[0]);
            return sendCore(message, params[0]);
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (aBoolean != null) {


                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/point_client/:  " + aBoolean;
                log.error(msg);
                new SenderErrorPOST().send(application, msg);
            }
        }
    }
}





