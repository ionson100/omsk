package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.example.user.omsk.Utils;

import java.util.Iterator;


public class UsbDeviceConnector {
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private final Object lock = new Object();
    private int fd = -1;
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("com.android.example.USB_PERMISSION".equals(action)) {
                try {
                    UsbDevice e = intent.getParcelableExtra("device");
                    if (intent.getBooleanExtra("permission", false) && e != null) {
                        UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
                        UsbDeviceConnector.this.fd = manager.openDevice(e).getFileDescriptor();
                    }
                } catch (Exception var8) {
                    var8.printStackTrace();
                    UsbDeviceConnector.this.fd = -1;
                }
                synchronized (UsbDeviceConnector.this.lock) {
                    UsbDeviceConnector.this.lock.notify();
                }
                context.unregisterReceiver(UsbDeviceConnector.this.mUsbReceiver);
            }
        }
    };

    public UsbDeviceConnector() {
    }

    static String getFilePath(Context ctx) {
        String file = "";

        try {
            UsbManager ignored = (UsbManager) ctx.getSystemService(Context.USB_SERVICE);
            Iterator var5 = ignored.getDeviceList().values().iterator();

            while (var5.hasNext()) {
                UsbDevice d = (UsbDevice) var5.next();

                Utils.messageBox("sd", d.getDeviceName(), (Activity) ctx, null);
                return d.getDeviceName();

            }
        } catch (Exception var7) {
            ;
        }
        return file;
    }

    static boolean hasPermission(Context ctx, int vendorID, int productID) {
        try {
            UsbManager ignored = (UsbManager) ctx.getSystemService(Context.USB_SERVICE);
            Iterator var4 = ignored.getDeviceList().values().iterator();
            while (var4.hasNext()) {
                UsbDevice d = (UsbDevice) var4.next();
                if (d.getVendorId() == vendorID && d.getProductId() == productID) {
                    return ignored.hasPermission(d);
                }
            }
        } catch (Exception var6) {
            ;
        }

        return false;
    }

    int getFD(final Context ctx, int vendorID, int productID) {
        this.fd = -1;
        final UsbManager manager = (UsbManager) ctx.getSystemService(Context.USB_SERVICE);
        Iterator var6 = manager.getDeviceList().values().iterator();

        while (var6.hasNext()) {
            final UsbDevice d = (UsbDevice) var6.next();
            if (d.getVendorId() == vendorID && d.getProductId() == productID) {
                IntentFilter filter = new IntentFilter("com.android.example.USB_PERMISSION");
                ctx.registerReceiver(this.mUsbReceiver, filter);
                (new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(300L);
                        } catch (InterruptedException var2) {
                            var2.printStackTrace();
                        }

                        PendingIntent permissionIntent = PendingIntent.getBroadcast(ctx, 0, new Intent("com.android.example.USB_PERMISSION"), 0);
                        manager.requestPermission(d, permissionIntent);
                    }
                })).start();
                Object var9 = this.lock;
                synchronized (this.lock) {
                    try {
                        this.lock.wait();
                    } catch (InterruptedException var12) {
                        var12.printStackTrace();
                    }
                    break;
                }
            }
        }
        return this.fd;
    }
}

