package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.Table;

import java.util.Date;

import static com.example.user.omsk.R.id.date;

// визиты не приятые сервером
@Table("visit_story_ass")//visit_story_old
public class MVisitStoryAss extends MVisitStoryBase implements IObservable, IUsingGuidId, IPing {

    public MVisitStoryAss() {

        //date = Utils.dateToInt(new Date());
        idu = Utils.getUuid().replace("-", "");
    }

    @Override
    public boolean isNewObject() {

        if(stateObject==0){
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void setNewObject(boolean value) {
        if(value){
            stateObject=1;
        }else {
            stateObject = 0;
        }
    }

    @Override
    public Date getDate() {
        return Utils.intToDate(finish);
    }


    public MPoint getPoint() {
        return Configure.getSession().get(MPoint.class, point_id);
    }

    public MOrderType getOrederType() {
        return Configure.getSession().get(MOrderType.class, order_type_id);
    }

    public String get_id() {
        return idu;
    }
}

