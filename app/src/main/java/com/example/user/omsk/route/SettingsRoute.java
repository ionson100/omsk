package com.example.user.omsk.route;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.setting.Reanimator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SettingsRoute implements Serializable {


    public Date dateNew_22;
    public String guid;
    @SerializedName("approved")
    public boolean isVerification;
    @SerializedName("points")
    public List<String> pointList = new ArrayList<>();
    @Expose(serialize = false)
    public List<String> pointListCopy = new ArrayList<>();

    public SettingsRoute() {
        pointListCopy = new ArrayList<>();
    }

    public SettingsRoute clear() {
        dateNew_22 = null;
        pointList = new ArrayList<>();
        pointListCopy = new ArrayList<>();
        return this;
    }

    public static SettingsRoute core() {
        return (SettingsRoute) Reanimator.get(SettingsRoute.class);
    }

    public static void save() {
        Reanimator.save(SettingsRoute.class);
    }
}
