package com.example.user.omsk.senders;

import android.os.AsyncTask;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.Certificate;
import com.example.user.omsk.fotocontrol.MPointImage;

import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

public class SenderPhotoPointHTTPSCLIENT {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderPhotoPointHTTPSCLIENT.class);
    private boolean isdelete;
    private Settings settings;

    private MPointImage pointImage;

    private String visit_id;
    private MyApplication application;

    public void send( MPointImage pointImage, MyApplication application) {
        this.settings = Settings.core();
        this.pointImage = pointImage;
        this.application = application;
        new SenderWorkerTack().execute();
    }

    public void sendDelete( MPointImage pointImage, MyApplication application) {

        this.isdelete = true;
        this.settings = Settings.core();
        this.pointImage = pointImage;
        this.application = application;
        try {
            new SenderWorkerTack().execute().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {


            File file = new File(pointImage.name);
            if (file.exists() == false && isdelete == false) {
                return true;
            }


            String nameFile = new File(pointImage.name).getName();
            String ssd = nameFile.replace(".jpg", "");
            int d = ssd.indexOf("(");
            String point = ssd;
            if (d == -1) {

            } else {
                point = point.substring(0, d);
            }

            String url = Utils.HTTP + settings.url + "/update_point_photo/?point_id=" + point + "&" + "file=" + nameFile + "&" +
                    "guid=" + SettingsUser.core().getUuidDevice() + (isdelete == false ? "" : "&del=1");
            try {
                HttpClient httpclient = MySSLSocketFactory.getNewHttpClient(application);// new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                String cert = Certificate.getCertificateSHA1Fingerprint(application);
                httppost.addHeader("Content-user", cert);
                if (isdelete == false) {
                    InputStreamEntity reqEntity = new InputStreamEntity(new FileInputStream(pointImage.name), -1);
                    reqEntity.setContentType("binary/octet-stream");
                    reqEntity.setChunked(true); // send in multiple parts if needed
                    httppost.setEntity(reqEntity);
                }

                HttpResponse response = httpclient.execute(httppost);
                if (response.getStatusLine().getStatusCode() == 200) {
                    return true;
                } else {
                    log.error(" ответ сервера: " + response.getStatusLine().getStatusCode() +
                            ", фото точки удаления не ушло на сервер send photo - " +
                            response.getStatusLine().getStatusCode());
                    return false;
                }
            } catch (Exception e) {
                log.error("фото точки удаления не ушло на сервер - " + e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                Configure.getSession().delete(pointImage);

                if (isdelete) {

                    List<MPointImage> list = Configure.getSession().getList(MPointImage.class, "is_delet = 0 and name = ?", pointImage.name);
                    if (list.size() == 0) {
                        File f = new File(pointImage.name);
                        if (f.exists()) {
                            f.delete();
                        }
                    }
                }
            }
        }
    }

}
