package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// хранидлище точек
@Table("point")
public class MPoint implements Serializable, IUsingGuidId, IObservable {


    @PrimaryKey("id")
    public transient int id;


    @Column("fio")
    public String fio;


    @Column("telephon_fio")
    public String telephone_fio;

    @Column("inn")
    public String inn;

    @SerializedName("comments")
    public List<MComments> mComments;

    @SerializedName("email")
    @Column("email")
    public String email;

    @SerializedName("sms")
    @Column("sms")
    public String sms;

    @SerializedName("debt")
    @Column("debt")
    public double debt;

    @Expose
    @SerializedName("id")
    @Column("idu")

    public String idu;
    @Expose
    @SerializedName("balance")
    @Column("balance")
    public double balance;

    @SerializedName("last_visit")
    @Column("last_visit")
    public Date lastDateVisit = null;

    @Expose
    @SerializedName("price_type")
    @Column("price_type")
    public String price_type_id;

    @Expose
    @SerializedName("channel")
    @Column("channel")
    public String channel_id;

    @Expose
    @SerializedName("name")
    @Column("name")
    public String name = "";

    public transient boolean isSelectForRoute;
    @Expose
    @SerializedName("address")
    @Column("address")
    public String address = "";

    @Column("agent_id")
    public transient String agent_id;

    @Expose
    @SerializedName("point_type")
    @Column("point_type")
    public String point_type_id;

    @Expose
    @SerializedName("latitude")
    @Column("latitude")
    public double latitude;

    @Expose
    @SerializedName("longitude")
    @Column("longitude")
    public double longitude;

    @Expose
    @SerializedName("working_hours")
    @Column("work_time")
    public String work_time = "";

    @Expose
    @SerializedName("comment")
    @Column("commentary")
    public String commentary = "";
    /**
     * дата обновления или вставки точки на клиенте
     */
    @Column("date")
    public transient Date date = null;

//    @Column("isBeznal")
//    @Expose(deserialize = true, serialize = false)
//    public boolean isBeznal;// todo добавление что  точка безнальная

    @Expose(deserialize = false, serialize = false)
    public boolean isRecomendationBalance = true;

    @Expose(deserialize = false, serialize = false)
    public boolean isRecomendationSale = true;

    @Column("state_object")
    public transient boolean stateObject;

    @Expose
    @SerializedName("contacts")
    public List<MContact> contacts;

    @Expose
    @SerializedName("counterparty")
    public MAgent mAgent;

    @SerializedName("stock")
    public List<MStockRows> stock_rows;

    @SerializedName("last_sale")
    public MLastSale last_sale;

    @SerializedName("uuid")
    private String uuid;

    @Expose(deserialize = false, serialize = false)
    public List<MSaleRows> sale_rows;

    @Column("date_pessimise")
    public long date_pessimise;
    // тоговая точка не найдена в вашем регионе
    public transient boolean fuflo;
    public List<String> deletePhotos = new ArrayList<>();

    public MPoint() {
        this.idu = Utils.getUuid();
        this.point_type_id = "";
    }

    public List<MComments> getmCommentsList() {
        return Configure.getSession().getList(MComments.class, " point_id = ? ", idu);
    }

    public String getWorkTime() {
        return "Часы работы: ";
    }

    public String getPriceType() {

        MPriceType priceType = Configure.getSession().get(MPriceType.class, price_type_id);
        if (priceType == null) {
            return "Не установлен";
        }
        return priceType.name;
    }

    public String getNameChannelType() {
       // List<MDistributionchannel> df = Configure.getSession().getList(MDistributionchannel.class, null);
        MDistributionchannel distributionchannel = Configure.getSession().get(MDistributionchannel.class, channel_id);
        if (distributionchannel == null) {
            return "Не установлен.";
        } else {
            return distributionchannel.name;
        }
    }

    public String getCommentary() {
        if (commentary == null) {
            return "";
        } else {
            return commentary;
        }
    }

    public String getNamePointTYpe() {

        MPointType pointType = Configure.getSession().get(MPointType.class, point_type_id);
        if (pointType == null) {
            return "Не установлен";
        } else {
            return pointType.name;
        }
    }

    public String get_id() {
        return idu;
    }

    public List<MStockRows> getStockRows() {

        List<MStockRows> stock = Configure.getSession().getList(MStockRows.class, " point_id = ? ", idu);

        if (stock.size() == 0) {
            for (MSaleRows mSaleRows : getSaleRows()) {
                MStockRows ss = comtains(stock, mSaleRows);
                if (ss == null) {
                    MStockRows s = new MStockRows();
                    s.date = mSaleRows.date;
                    s.amount = mSaleRows.amount;
                    s.product_id = mSaleRows.product_id;
                    stock.add(s);
                } else {
                    ss.amount = ss.amount + mSaleRows.amount;
                }
            }
        } else {
            Date date = stock.get(0).date;
            for (MSaleRows mSaleRows : getSaleRows()) {
                if (mSaleRows.date.getTime() >= date.getTime()) {
                    MStockRows ss = comtains(stock, mSaleRows);
                    if (ss == null) {
                        MStockRows s = new MStockRows();
                        s.date = mSaleRows.date;
                        s.amount = mSaleRows.amount;
                        s.product_id = mSaleRows.product_id;
                        stock.add(s);
                    } else {
                        ss.amount = ss.amount + mSaleRows.amount;
                    }
                }

            }
        }
        return stock;
    }

    private MStockRows comtains(List<MStockRows> stock, MSaleRows mSaleRows) {
        MStockRows res = null;
        for (MStockRows mStockRows : stock) {
            if (mStockRows.product_id.equals(mSaleRows.product_id)) {
                res = mStockRows;
                break;
            }
        }
        return res;
    }

    public List<MSaleRows> getSaleRows() {
        sale_rows = Configure.getSession().getList(MSaleRows.class, " point_id = ? ", idu);
        return sale_rows;
    }

    public void setAgent(MAgent agent) {
        if (agent != null) {
            agent_id = agent.idu;
        } else {
            agent_id = null;
        }
        mAgent = agent;
    }


    public MAgent getContragent(ISession ses) {
        if (mAgent == null) {
            if (agent_id == null || agent_id.trim().length() == 0) {
                mAgent = null;
            } else {
                mAgent = ses.get(MAgent.class, agent_id);
            }
        }
        return mAgent;
    }


    public List<MContact> getListContact(ISession ses) {
        List<MContactor> mContactors = ses.getList(MContactor.class, " point_id = ? ", idu);
        if (mContactors.size() != 0) {
            StringBuilder sb = new StringBuilder();
            for (MContactor mContactor : mContactors) {
                sb.append("'");
                sb.append(mContactor.contact_id);
                sb.append("'");
                sb.append(",");
            }
            String s = sb.toString().substring(0, sb.toString().lastIndexOf(","));
            String sql = " idu in (" + s + ")";
            contacts = ses.getList(MContact.class, sql);
        } else {
            contacts = new ArrayList<>();
        }
        return contacts;
    }

    /**
     * Процедура инициализации точки для отправки на сервер
     *
     * @param
     */
    public void preparePoindForSender(ISession ses) {
        /*
      гуид устройства для отправки точки на сервер
     */

        uuid = SettingsUser.core().getUuidDevice();
        getListContact(ses);
        getContragent(ses);
    }

    @Override
    public boolean isNewObject() {
        return stateObject;
    }

    @Override
    public void setNewObject(boolean value) {
        stateObject = value;
        date = Utils.curDate();
    }

    @Override
    public Date getDate() {
        return date;
    }

    public MAlertPoint getAlertPoint() {
        List<MAlertPoint> alertPoints = Configure.getSession().getList(MAlertPoint.class, " point_id = ?", idu);
        if (alertPoints.size() == 0) {
            return null;
        } else {
            return alertPoints.get(0);
        }
    }

    public void saveOrUpdate(MAlertPoint mAlertPoint) {

        if (mAlertPoint.id == 0) {
            Configure.getSession().insert(mAlertPoint);

        } else {
            Configure.getSession().update(mAlertPoint);
        }
    }

    public void deleteAlert(MAlertPoint finalMAlertPoint) {
        Configure.getSession().delete(finalMAlertPoint);
    }

    public List<MTaskPoint> getTask() {
        return Configure.getSession().getList(MTaskPoint.class, " point_id = ?", idu);
    }
}

