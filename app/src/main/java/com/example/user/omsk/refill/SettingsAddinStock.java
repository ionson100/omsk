package com.example.user.omsk.refill;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SettingsAddinStock implements Serializable {
    public final List<MProduct> mLastListNew = new ArrayList<>();
    public Date mLastkDateNew_22 = null;
    public boolean mStatusSender;


    public static SettingsAddinStock core() {
        return (SettingsAddinStock) Reanimator.get(SettingsAddinStock.class);
    }

    public static void save() {
        Reanimator.save(SettingsAddinStock.class);
    }
}
