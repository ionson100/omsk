package com.example.user.omsk.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.kassa.KassaActivity;
import com.example.user.omsk.kassa.PrintKkmSumma;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.myTextView.MyTextView;
import com.example.user.omsk.orm2.Configure;

import org.osmdroid.contributor.util.Util;

import java.util.List;


public class DialogReportKKMMoney extends DialogFragment {

    public double nalKKM,nalCore;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_report_kkm, null);
        TextView textView = (TextView) v.findViewById(R.id.alert_message);
        String dd = "\n\nНаличность в ккм: %s \nНаличность за проданный товар по визитам: %s\nДанные должны совпадать\n\n";
        textView.setText(String.format(dd, String.valueOf(nalKKM), String.valueOf(Utils.round(nalCore,2))));
        MyTextView textView1 = (MyTextView) v.findViewById(R.id.title_dalog);
        textView1.setText("Наличность в ККМ");
        v.findViewById(R.id.print_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        builder.setView(v);
        return builder.create();
    }






}
