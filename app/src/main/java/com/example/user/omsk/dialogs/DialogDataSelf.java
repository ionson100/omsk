package com.example.user.omsk.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.List;

/**
 * ion100  on 12.12.2017.
 */

public class DialogDataSelf extends DialogFragment {

    public List<MProduct> mProducts = new ArrayList<>();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_data_self, null);
        TextView title = (TextView) v.findViewById(R.id.title_dalog);
        title.setText(R.string.hjhdsafasf);
        Pupper itogoName, itogoAmount, itogoPrice;
        itogoName = (Pupper) v.findViewById(R.id.itogo_name);
        itogoAmount = (Pupper) v.findViewById(R.id.itogo_count);
        itogoPrice = (Pupper) v.findViewById(R.id.itogo_price);
        double countName = 0, price = 0, count = 0;
        for (MProduct mProduct : mProducts) {
            double d = mProduct.amountSelfCore();
            if (d == 0) continue;
            countName = countName + 1;
            count = count + d;
            price = price + d * mProduct.price;
        }
        itogoName.setPairString(getString(R.string.fsdfff), String.valueOf(countName));
        itogoAmount.setPairString(getString(R.string.jkhjdfdf), String.valueOf(count));
        itogoPrice.setPairString(getString(R.string.sdfdsfdsf), Utils.getStringDecimal(price));
        builder.setView(v);
        return builder.create();
    }
}
