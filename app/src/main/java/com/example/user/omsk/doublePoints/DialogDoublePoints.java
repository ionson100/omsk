package com.example.user.omsk.doublePoints;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;

import java.util.List;
import java.util.Set;


public class DialogDoublePoints extends DialogFragment {

    //private IActionE mIAction;
    private Activity mActivity;
    private List<MPoint> mDoublePoints;
    private ListView mListView;
    private Set<String> mDoubleWord;
    private Settings mSettings;

    DialogDoublePoints setActivity(Activity activity, List<MPoint> mDoublePoints, Set<String> mDoubleWord, IActionE iAction, Settings mSettings) {
        this.mActivity = activity;
        this.mDoublePoints = mDoublePoints;
        this.mDoubleWord = mDoubleWord;
        //this.mIAction = iAction;
        this.mSettings = mSettings;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        vi = LayoutInflater.from(getActivity());
        View v = vi.inflate(R.layout.dialog_double_products, null);

        TextView tb_double_word = (TextView) v.findViewById(R.id.tb_double_word);
        StringBuilder builder1 = new StringBuilder();
        for (String str : mDoubleWord) {
            builder1.append(str).append(" ");
        }
        tb_double_word.setText(builder1.toString());


        builder.setView(v);
        mListView = (ListView) v.findViewById(R.id.list_double_points);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() instanceof MPoint) {
                    final MPoint point = (MPoint) view.getTag();
                    String str = getString(R.string.perehod2).replace("#1", point.name).replace("#2", point.address);

                    Utils.messageBox(getString(R.string.perehod), str, mActivity, new IActionE<View>() {
                        @Override
                        public void action(View v) {
                            mSettings.getVisit().point = point;
                            mSettings.showFragment(StateSystem.POINT, mActivity);
                        }
                    });
                }
            }
        });

        Button bt_cancel = (Button) v.findViewById(R.id.button_cancel);

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onResume() {
        initList();
        super.onResume();
    }

    private void initList() {
        ListAdapterDoublePoints points = new ListAdapterDoublePoints(mActivity, R.layout.item_double_points, mDoublePoints);
        mListView.setAdapter(points);
    }

}
