package com.example.user.omsk.history;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.models.MVisitStoryPointDay;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class History extends AppCompatActivity {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(History.class);
    // переменная служит для отображения последнего периода просмотра истории визитов за дату, эта дата и есть
    // эта переменная, она сбрасывется при синхронизации при открытии торгового дня

    public static Date mStartStoryVisit = null;
    private ListView mListView;
    private Settings mSettings;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_history);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mSettings = Settings.core();
            findViewById(R.id.bt_select_date).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogChoiceDatesHistory f = new DialogChoiceDatesHistory();
                    f.show(getSupportFragmentManager(), "sdcvsdd");
                }
            });
            mTextView = (TextView) findViewById(R.id.date_titul);
            mListView = (ListView) findViewById(R.id.list_history);
            String date1 = Utils.simpleDateFormatForCommentsE(mStartStoryVisit);
            String date2 = Utils.simpleDateFormatForCommentsE(Utils.curDate());


            deleterCurrentDataHistory(date1.equals(date2));


            if (mStartStoryVisit!=null) {


                deleterCurrentDataHistory(date1.equals(date2));
                SelectorDataHistoryDay.Select(
                        this,
                        mStartStoryVisit,
                        mListView, mSettings,
                        mTextView);


            }

        } catch (Exception e) {
            log.error(e);
            Utils.sendMessage("History:" + Utils.getErrorTrace(e), (MyApplication) this.getApplication());
            this.finish();
        }

    }

    private void deleterCurrentDataHistory(boolean delete) {
        if (!delete || mStartStoryVisit==null) return;
        ISession ses = Configure.getSession();
        final int outDate = (int) (mStartStoryVisit.getTime() + 24 * 60 * 60);
        final List<MVisitStoryBase> list = new ArrayList<MVisitStoryBase>(Configure.getSession().getList(MVisitStoryPointDay.class,
                " date BETWEEN " + String.valueOf(mStartStoryVisit) + " AND " + String.valueOf(outDate) + " "));
        for (MVisitStoryBase day : list) {
            ses.delete(day);
        }
    }

//    private void deleterCurrentDataHistorySale(boolean delete) {
//        if (!delete || mStartStoryVisit==null) return;
//      //  ISession ses = Configure.getSession();
//        final int outDate = (int) (mStartStoryVisit.getTime() + 24 * 60 * 60);
//    }

    public void reload(int yar, int month, int dey) {

        Calendar myCalendar = Calendar.getInstance();
        myCalendar.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        myCalendar.set(yar, month, dey, 0, 0, 0);
        Date myDate = myCalendar.getTime();
        String date1 = Utils.simpleDateFormatForCommentsE(myDate);
        String date2 = Utils.simpleDateFormatForCommentsE(Utils.curDate());


        deleterCurrentDataHistory(date1.equals(date2));
        SelectorDataHistoryDay.Select(
                this,
                myDate,
                mListView,
                mSettings,
                mTextView
        );

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
