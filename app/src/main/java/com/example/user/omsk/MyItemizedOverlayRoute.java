package com.example.user.omsk;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;

import com.example.user.omsk.dialogs.DialogMapRoute;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;

import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

/**
 * ion100 on 03.11.2017.
 */

class MyItemizedOverlayRoute extends ItemizedOverlay<OverlayItem> {

    //Map<String, Point> map = new HashMap<>();
    //private Paint paint;
  //  private Bitmap bitmap1, bitmap2;
    private final ArrayList<OverlayItem> mOverlayItemList = new ArrayList<>();
    private final Drawable pDefaultMarker;
    private final ResourceProxy pResourceProxy;
    private final Activity activity;

    public MyItemizedOverlayRoute(Drawable pDefaultMarker,
                                  ResourceProxy pResourceProxy, Activity activity) {
        super(pDefaultMarker, pResourceProxy);
        this.pDefaultMarker = pDefaultMarker;
        this.pResourceProxy = pResourceProxy;
        this.activity = activity;
      //  bitmap1 = BitmapFactory.decodeResource(activity.getResources(), R.drawable.gps_point1);
      //  bitmap2 = BitmapFactory.decodeResource(activity.getResources(), R.drawable.gps_point2);

    }


    public GeoPoint getGeoPoint() {
        if (mOverlayItemList.size() != 0) {
            return mOverlayItemList.get(0).getPoint();
        }

        return null;
    }


    @Override
    protected void onDrawItem(Canvas canvas, OverlayItem item, Point curScreenCoords, float aMapOrientation) {
        super.onDrawItem(canvas, item, curScreenCoords, aMapOrientation);
    }

    protected boolean onTap(int index) {
        OverlayItem item = mOverlayItemList.get(index);
        DialogMapRoute dialog = new DialogMapRoute();
        if (item.getSnippet().split(":")[0].equals("2")) {

            dialog.mVisitStory = Configure.getSession().get(MVisitStory.class, item.getTitle());
        } else {
            dialog.mPoint = Configure.getSession().get(MPoint.class, item.getTitle());
        }
        dialog.partial = item.getSnippet();

        dialog.show(((AppCompatActivity) activity).getSupportFragmentManager(), "dfdfdf");
        return true;
    }

    public void addItem(GeoPoint p, String title, String snippet) {

        OverlayItem newItem = new OverlayItem(title, snippet, p);
        mOverlayItemList.add(newItem);
        populate();
    }

    public void removeAll() {
        mOverlayItemList.clear();
        populate();
    }

    @Override
    public boolean onSnapToItem(int arg0, int arg1, Point arg2, IMapView arg3) {
        return false;
    }

    @Override
    protected OverlayItem createItem(int arg0) {
        return mOverlayItemList.get(arg0);
    }

    @Override
    public int size() {
        return mOverlayItemList.size();
    }
}
