package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogEditCountProduct;
import com.example.user.omsk.models.MProduct;

import java.util.List;

public class ListAdapterForSelectedBalanceProduct extends ArrayAdapter<MProduct> {

    private final IActionE mIAction;
    private final AppCompatActivity mContext;
    private final int mResource;

    public ListAdapterForSelectedBalanceProduct(AppCompatActivity context, int resource, List<MProduct> items, IActionE iAction) {
        super(context, resource, items);
        this.mContext = context;
        this.mResource = resource;
        this.mIAction = iAction;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(mResource, null);
        }
        final MProduct p = getItem(position);
        if (p != null) {
            final TextView tt1 = (TextView) v.findViewById(R.id.name_poduct);
            final TextView tt2 = (TextView) v.findViewById(R.id.balance_count_poduct);
            if (tt1 != null) {
                tt1.setText(p.name);
            }
            if (tt2 != null) {
                tt2.setText(Utils.getStringDecimal(p.getAmount_core()));
            }
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIAction != null) {
                        mIAction.action(v);
                    }

                    DialogEditCountProduct d = new DialogEditCountProduct();
                    d.setDismis(new IActionE<Object>() {
                        @Override
                        public void action(Object o) {


                        }
                    });


                    d.setProduct(p).show(mContext.getSupportFragmentManager(), "adsfyusdfsdf");


                }
            });
        }


        return v;
    }
}
