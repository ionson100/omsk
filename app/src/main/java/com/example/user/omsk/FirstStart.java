package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.user.omsk.models.ListerGroup;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.orm2.Table;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

//import com.example.user.omsk.models.MError;

public class FirstStart {
    public static org.apache.log4j.Logger log = Logger.getLogger(FirstStart.class);


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void workerBase(Settings mSettings, SettingsStart settingsStart, Context context) {


        if (settingsStart.isFirstStartUpdate() == StateUpdate.stabile) return;
        ISession ses = Configure.getSession();
        ses.beginTransaction();
        deleteTable(ses, context, mSettings);
        createTable(ses, context, mSettings);
        ses.commitTransaction();
        ses.endTransaction();
        settingsStart.setIsFirstStartUpdate(StateUpdate.stabile);
        mSettings.serverVersionName = null;
        Settings.save();

    }

    private static void deleteTable(ISession ses, Context context, Settings mSettings) {

        List<String> tables = new ArrayList<>(ListerGroup.classes.size());
        for (Class aClass : ListerGroup.classes) {
            if (aClass.isAnnotationPresent(Table.class)) {
                Table df = ((Table) aClass.getAnnotation(Table.class));
                tables.add(df.value());
            }
        }

        for (String table : tables) {

            String sql1 = " DROP TABLE IF EXISTS '" + table + "'; ";
            ses.execSQL(sql1);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static void createTable(ISession ses, Context context, Settings mSettings) {

        for (Class aClass : ListerGroup.classes) {
            if (aClass.isAnnotationPresent(Table.class)) {
                Configure.createTable(aClass);
            }
        }
        UserNamesSettings.core().idHideNoteShow = false;
        UserNamesSettings.save();
    }
}

