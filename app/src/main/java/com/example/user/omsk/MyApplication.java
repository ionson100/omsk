package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;

//import com.example.user.omsk.models.MError;
import com.example.user.omsk.models.ListerGroup;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.setting.Reanimator;

import org.apache.log4j.Logger;

import java.io.File;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import de.mindpipe.android.logging.log4j.LogConfigurator;

public class MyApplication extends android.app.Application {

    public static final String formatDateVisit="dd_MM_yyyy__HH_mm";
    public static final String csvfile = Environment.getExternalStorageDirectory().toString() + "/visits/file_%s.txt";
    public static final String logfile = Environment.getExternalStorageDirectory().toString() + File.separator + "log/file.log";
    public static org.apache.log4j.Logger log = Logger.getLogger(MyApplication.class);

    static {
        final LogConfigurator logConfigurator = new LogConfigurator();

        logConfigurator.setFileName(logfile);
        logConfigurator.setRootLevel(org.apache.log4j.Level.ALL);
        logConfigurator.setLevel("org.apache", org.apache.log4j.Level.ALL);
        logConfigurator.setUseFileAppender(true);
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(1024 * 1024 * 5);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.configure();
        disableSslVerification();
    }

    private static Typeface typeFace;

    public static Typeface getTypeface(Context contect) {
        if (typeFace == null) {
            typeFace = Typeface.createFromAsset(contect.getAssets(), "fonts/GloberRegular.otf");//GloberRegular.otf
        }
        return typeFace;
    }


    private Settings mSettings;
    //public static FVisit MVISIT;

    // процедура отсылки ошибки на сервер
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void handleUncaughtException(Thread thread, Throwable e) {


        StackTraceElement[] stackTraceElements = e.getStackTrace();
        StringBuilder builder = new StringBuilder();
        builder.append("Exception: ").append(e.getClass().getName()).append("\n")
                .append("Message: ").append(e.getMessage()).append("\nStacktrace:\n");

        for (StackTraceElement element : stackTraceElements) {
            builder.append("\t").append(element.toString()).append("\n");
        }
        log.trace("Hanler ",e);
        //Configure.getSession().insert(new MError(builder.toString()));
        new SenderErrorPOST().send(this, builder.toString());

        // проверям не убила ли система орм, если убила иницализируем по новой
        if (!Configure.isLive()) {
            new Configure(getApplicationInfo().dataDir + "/" + Utils.BASE_NAME, getApplicationContext(), ListerGroup.classes);
        }


        if (MainActivity.isStateVisitIsVisit()) {
            mSettings.setStateSystemForApplication(StateSystem.VISIT_HOME);
        } else {
            mSettings.setStateSystemForApplication(StateSystem.HOME);
        }
        // если возникает ошибка, то мы закрываем приложение и открываем по новой
        Intent mStartActivity = new Intent(getBaseContext(), MainActivity.class);
        int mPendingIntentId = 1234563434;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getBaseContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10, mPendingIntent);
        System.exit(0);
    }

    @Override
    public void onCreate() {

        super.onCreate();
        Reanimator.intContext(getApplicationContext());

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
    }


    @Override
    public void onTerminate() {
        log.info("onTerminate");

        super.onTerminate();
        Transceiver.cancelAll();
        Reanimator.close();
        Configure.close();
    }

    @Override
    public void onLowMemory() {
        log.info("onLowMemory");

        super.onLowMemory();
        Transceiver.cancelAll();
        Reanimator.close();
        Configure.close();
    }

    private static void disableSslVerification() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

}


