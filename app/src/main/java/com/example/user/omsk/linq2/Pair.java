package com.example.user.omsk.linq2;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public class Pair<TKey, TValue> {

    private final TKey key;
    private final TValue value;

    public Pair(TKey tKey, TValue tValue) {
        this.key = tKey;
        this.value = tValue;
    }

    public TValue getValue() {
        return value;
    }

    public TKey getKey() {
        return key;
    }
}
