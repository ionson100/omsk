package com.example.user.omsk.action;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisit;

import java.util.List;
import java.util.Map;

public class SummatorSku {
    private List<String> mKeys;
   // private MVisit mVisit;
    private Map<String, ActionValidate.TempProduct> mMap;

    public SummatorSku(List<String> keys, MVisit mVisit, Map<String, ActionValidate.TempProduct> saleProductDictionary) {
        this.mKeys = keys;
        //this.mVisit = mVisit;
        this.mMap = saleProductDictionary;
    }

    public double price() {
        double acum = 0;
        for (String key : mKeys) {
            if (mMap.containsKey(key)) {
                for (MProduct product : mMap.get(key).mProductList) {
                    acum = acum + product.getAmount_core() * product.price;
                }
            }
        }
        return acum;
    }

    public double product() {
        double acum = 0;
        for (String key : mKeys) {
            if (mMap.containsKey(key)) {
                for (MProduct product : mMap.get(key).mProductList) {
                    acum = acum + product.getAmount_core();
                }
            }
        }
        return acum;
    }

    public double amountAction(MAction mAction) {
        return 0;
    }
}
