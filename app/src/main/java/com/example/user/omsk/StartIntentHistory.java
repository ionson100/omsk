package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;

import com.example.user.omsk.history.History;

public class StartIntentHistory {
    public static void start(Activity activity) {
        Intent intent = new Intent(activity, History.class);
        activity.startActivity(intent);
    }
}
