package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;

// базовый класс
public abstract class BasePlanMML {
    @PrimaryKey("id")
    public int id;

    @Column("name")
    public String mml;

    @Column("product_id")
    public String product_id;


}
