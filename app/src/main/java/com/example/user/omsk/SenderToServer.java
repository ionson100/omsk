package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.AsyncTask;

import com.example.user.omsk.senders.ManualSender;

public class SenderToServer {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderToServer.class);
    private static MyApplication application;

    public static synchronized void sender(Settings mSettings, MyApplication application) {

        SenderToServer.application = application;
        // new SenderJobToServePhoto(mSettings).execute(); todo  фото приходили позже  чем точки
        new ManualSender(mSettings, application).send();
    }

    public static class SenderJobToServePhoto extends AsyncTask<Void, Void, Void> {

        private final Settings mSettings;

        public SenderJobToServePhoto(Settings mSettings) {
            this.mSettings = mSettings;
        }

        public void sendCore() {
            try {
                this.execute().get();
            } catch (Exception e) {
                log.error(e);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            WorkingFiles workingFiles = new WorkingFiles(mSettings);
            workingFiles.start(application);
            return null;
        }
    }
}
