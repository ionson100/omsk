package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;

import com.example.user.omsk.models.MPromotion;
import com.example.user.omsk.senders.SenderDownloadPOST;
import com.example.user.omsk.orm2.Configure;

import java.io.File;
import java.util.List;


public class WorkingPromotions {

    private final Settings settings;
    private Activity activity;

    public WorkingPromotions(Settings settings, Activity activity) {
        this.settings = settings;
        this.activity = activity;
    }

    public void start() {
        File fs = new File(Utils.getPromotionDirectory());
        if (!fs.exists()) {
            fs.mkdirs();
        }
        List<MPromotion> mPromotions = Configure.getSession().getList(MPromotion.class, null);
        for (MPromotion mPromotion : mPromotions) {
            File f = new File(Utils.getPromotionDirectory() + "/" + mPromotion.file_name);
            if (!f.exists()) {
                SenderDownloadPOST sd = new SenderDownloadPOST();
                sd.send(mPromotion,  activity);
            } else {
                BitmapCache.cache(mPromotion);
            }
        }
    }
}
