package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.orm2.Configure;

import java.util.List;
import java.util.Set;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class ListAdapterForRoute extends ArrayAdapter<MRoute> implements IPainter {

    private final List<MVisitStory> mVisitStories;
    private final int mResource;
    private Set<String> mSetPointIdDebt;
    private ArrayMap<Integer, View> map = new ArrayMap<Integer, View>();
    private Settings mSettings;

    public ListAdapterForRoute(Context context, int resource, List<MRoute> objects, Set<String> setPointIdDebt) {
        super(context, resource, objects);
        this.mResource = resource;
        mVisitStories =  Configure.getSession().getList(MVisitStory.class, null);
        this.mSetPointIdDebt = setPointIdDebt;
        mSettings = Settings.core();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;
        final MRoute p = getItem(position);
        if (p != null) {

            if (map.containsKey(position)) {
                mView = map.get(position);
            } else {
                mView = LayoutInflater.from(getContext()).inflate(mResource, null);


                if (mSettings.getVisit() != null && mSettings.getVisit().point != null && mSettings.getVisit().point.idu.equals(p.point_id)) {
                    mView.setBackgroundResource(R.color.colore97);
                }


                LinearLayout routePanel = (LinearLayout) mView.findViewById(R.id.route_panel);
                //ImageView imageView = (ImageView) mView.findViewById(R.id.imageView_route);
                ImageView imageAlert = (ImageView) mView.findViewById(R.id.image_alert);
                //ImageView imageTask = (ImageView) mView.findViewById(R.id.image_task);

                if (p.getPoint().getAlertPoint() != null) {
                    imageAlert.setVisibility(View.VISIBLE);
                } else {
                    imageAlert.setVisibility(View.INVISIBLE);
                }
//                if (p.getPoint().getTask().size() > 0) {
//                    imageTask.setVisibility(View.VISIBLE);
//                } else {
//                    imageTask.setVisibility(View.INVISIBLE);
//                }
                MVisitStory sd = Linq.toStream(mVisitStories).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MVisitStory>() {
                    @Override
                    public boolean apply(MVisitStory t) {
                        return t.point_id.equals(p.point_id);
                    }
                });
                if (sd == null) {
                    //imageView.setImageResource(R.drawable.red_point);
                    routePanel.setBackgroundResource(R.color.route_red);

                } else {
                    if (sd.isNewObject()) {
                        // imageView.setImageResource(R.drawable.yellow_point);
                        routePanel.setBackgroundResource(R.color.route_yelo);
                    } else {
                        //imageView.setImageResource(R.drawable.green_point);
                        routePanel.setBackgroundResource(R.color.route_green);
                    }
                }
                MPoint mPoint = p.getPoint();
                if (mPoint != null) {
                    TextView textView = (TextView) mView.findViewById(R.id.point_name_route);
                    textView.setText(mPoint.name);
                    mView.setTag(mPoint);
                }
                if (mPoint != null && mSetPointIdDebt.contains(mPoint.idu)) {
                    mView.findViewById(R.id.image_debt).setVisibility(View.VISIBLE);
                }
                mView.setTag(p.getPoint());
                map.put(position, mView);
            }


        }
        return mView;
    }

    @Override
    public void notifyAddin(int position) {
        for (View view : map.values()) {
            view.setBackgroundResource(R.color.colore3);
        }
        if (map.containsKey(position)) {
            map.get(position).setBackgroundResource(R.color.colore97);
        }
    }
}
