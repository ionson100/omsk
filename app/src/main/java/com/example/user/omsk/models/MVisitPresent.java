package com.example.user.omsk.models;

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

import java.io.Serializable;
import java.util.List;


@Table("visit_present")
public class MVisitPresent implements Serializable {

    @PrimaryKey("_id")
    public int _id;

    @Column("id")
    public String id;

    @Column("amount")
    public int amount;

    @Column("visit_id")
    public String visit_id;

    private transient String _name;

    public String getName() {

        if(_name==null){
            List<MPresent> mPresents= Configure.getSession().getList(MPresent.class," id = ? ",id);
            if(mPresents.size()>0){
                _name=mPresents.get(0).name;
            }else {
                _name="не найден";
            }
        }
        return _name;

    }

    @Column("visit_finish")
    public long visit_finish;

}
