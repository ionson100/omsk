package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

//import com.example.user.omsk.models.MError;

public class SenderGoogleAddressToGeoGET {
    public static org.apache.log4j.Logger log = Logger.getLogger(SenderGoogleAddressToGeoGET.class);
    private String formatAddress = "";
    private String latitude = "0.0000000";
    private String longitude = "0.0000000";
    private IActionAddressGeo iAction;
    private Activity activity;

    public void send(String address, IActionAddressGeo iAction, Activity activity) {
        this.activity = activity;
        this.iAction = iAction;
        new SenderWorkerTack().execute(address);
    }

    private HashMap<String, String> getParametr(String address) {
        HashMap<String, String> params = new HashMap<>();
        params.put("address", address.trim());
        params.put("language", "ru");
        return params;
    }

    private void senderCore(HashMap<String, String> params) {

        String sd = null;
        try {
            sd = Utils.getPostDataString(params);
        } catch (UnsupportedEncodingException e) {
            log.error(e);
           // Configure.getSession().insert(new MError("gps12" + e));
            return;
        }

        URL url;
        StringBuilder sb = new StringBuilder();
        try {

            url = new URL("http://maps.googleapis.com/maps/api/geocode/json?" + sd);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                Gson gson = Utils.getGson();

                SenderGoogleAddressToGeoGET.AddressGeo sdd = gson.fromJson(sb.toString(), SenderGoogleAddressToGeoGET.AddressGeo.class);
                if (sdd != null && sdd.results != null && sdd.results.size() > 0) {
                    SenderGoogleAddressToGeoGET.AddressE addressE = sdd.results.get(0);

                    formatAddress = SenderGoogleGeoToAddressGET.parseAddress(addressE.address_components);
                    latitude = String.valueOf(addressE.geometry.location.lat);
                    longitude = String.valueOf(addressE.geometry.location.lng);
                }

                if (sdd != null && sdd.results != null && sdd.results.size() > 0) {
                    SenderGoogleAddressToGeoGET.AddressE addressE = sdd.results.get(0);

                    formatAddress = SenderGoogleGeoToAddressGET.parseAddress(addressE.address_components);
                    longitude = String.valueOf(addressE.geometry.location.lat);
                    latitude = String.valueOf(addressE.geometry.location.lng);
                }
            } else {
            }
        } catch (Exception ignored) {
            log.error(ignored);
        }
    }

    public interface IActionAddressGeo {
        void action(String address, String latitude, String longitude);
    }

    private class SenderWorkerTack extends AsyncTask<String, Void, Void> {

        volatile boolean isStop;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(activity, Settings.core())) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, "Получение координат", new IActionE<View>() {
                @Override
                public void action(View v) {
                    isStop = true;
                }
            });
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (isCancelled()) return;
            if (!isStop) {
                iAction.action(formatAddress, latitude, longitude);
                if (dialog != null) {
                    dialog.cancel();
                }
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            if (isCancelled()) return null;
            HashMap<String, String> res = getParametr(params[0]);
            senderCore(res);
            return null;
        }
    }

    public class AddressGeo {
        public List<AddressE> results;
    }

    public class AddressE {
        public List<Pokemon> address_components;
        public String formatted_address;
        public Geometry geometry;
    }

    public class Geometry {

        public Location location;
    }

    public class Location {
        public double lat;
        public double lng;
    }

    public class Pokemon {

        public Object long_name;
        public Object short_name;
        public List<String> types;
    }
}
