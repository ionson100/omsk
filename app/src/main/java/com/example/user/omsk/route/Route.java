package com.example.user.omsk.route;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderGetRouterGET;
import com.example.user.omsk.senders.SenderSetRoutePOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class Route extends AppCompatActivity {

    private final List<MPoint> mRouteList = new ArrayList<>();
    private final List<MPoint> mSelectForRouter = new ArrayList<>();
    private final List<MPoint> mSelectForPoint = new ArrayList<>();
    private Settings mSettings;
    private SettingsRoute mRouteSettings;
    private List<MPoint> mPointList;
    private ListView mPointListView;
    private ListView mRouteListView;
    private TextView  mTextStatus;
    private EditText mEditText;
    private Button mCreateFrom;
    private Button btSenderToServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        final Activity activity = this;

        mPointList = Configure.getSession().getList(MPoint.class, null);
        mCreateFrom = (Button) findViewById(R.id.bt_route_date_copy);

        mTextStatus = (TextView) findViewById(R.id.text_status);
        mRouteListView = (ListView) findViewById(R.id.list_point_route);
        mPointListView = (ListView) findViewById(R.id.list_point_free);
        mRouteSettings = SettingsRoute.core();
        mSettings = Settings.core();

        HorizontalNumberPicker numberPicker = (HorizontalNumberPicker) findViewById(R.id.horizontal_number_picker);
        numberPicker.setAction(new HorizontalNumberPicker.IAction() {
            @Override
            public void Action(int value, Date startDate, Date finishDate) {
                getLastDate(startDate, finishDate);
            }


        });


        if (mRouteSettings.dateNew_22 ==null) {
            mCreateFrom.setVisibility(View.GONE);
            mTextStatus.setText(getString(R.string.wait_date_route));

        } else {
            mCreateFrom.setVisibility(View.VISIBLE);
            //printetator();

            for (final String s : mRouteSettings.pointList) {

                MPoint f = Linq.toStream(mPointList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MPoint>() {
                    @Override
                    public boolean apply(MPoint t) {
                        return t.idu.equals(s);
                    }
                });

                if (f != null) {
                    mRouteList.add(f);
                    mPointList.remove(f);
                }
            }
        }

        activateButtons(activity);
        activatePointList();
        activateRouteList();
        createListABC();
        ValidateE();
    }

    private void activateButtons(final Activity activity) {
        findViewById(R.id.bt_point_to_router).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRouteSettings.dateNew_22 ==null) {
                    Toast.makeText(activity, R.string.not_create_route, Toast.LENGTH_SHORT).show();
                } else {
                    mRouteSettings.isVerification = false;
                    addRoute(mSelectForRouter); //mRouteList.addAll(mSelectForRouter);
                    removePoints(mSelectForRouter);// mPointList.removeAll(mSelectForRouter);

                    activateRouteList();
                    for (MPoint mPoint : mSelectForRouter) {
                        mPoint.isSelectForRoute = false;
                    }
                    mSelectForRouter.clear();
                    saveRouteSettings();
                }
                ValidateE();
            }
        });

        findViewById(R.id.bt_route_to_point).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRouteSettings.dateNew_22 ==null) {
                    Toast.makeText(activity, R.string.not_create_route, Toast.LENGTH_SHORT).show();
                } else {
                    mRouteSettings.isVerification = false;
                    mRouteList.removeAll(mSelectForPoint);
                    mPointList.addAll(mSelectForPoint);

                    activatePointList();
                    activateRouteList();
                    for (MPoint mPoint : mSelectForPoint) {
                        mPoint.isSelectForRoute = false;
                    }
                    mSelectForPoint.clear();
                    saveRouteSettings();
                }
                ValidateE();
            }
        });

        findViewById(R.id.bt_close_route).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });


        findViewById(R.id.bt_route_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogChoiceDates df = new DialogChoiceDates();
                df.setmIsCreate(false);
                df.isSender(false);
                df.show(getSupportFragmentManager(), "sdsd");
                for (MPoint mPoint : mPointList) {
                    mPoint.isSelectForRoute = false;
                }
                activateRouteList();
                activatePointList();
            }
        });

        mCreateFrom.setOnClickListener(new View.OnClickListener() {//создать на основе
            @Override
            public void onClick(View v) {
                DialogChoiceDates df = new DialogChoiceDates();
                df.setmIsCreate(true);
                df.isSender(true);
                df.show(getSupportFragmentManager(), "weeewe");
            }
        });

        findViewById(R.id.bt_top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRouteSettings.dateNew_22 ==null) {
                    Toast.makeText(activity, R.string.not_create_route, Toast.LENGTH_SHORT).show();
                } else {
                    mRouteSettings.isVerification = false;
                    TopDownRoutePoints.top(mRouteList);
                    activateRouteList();
                    saveRouteSettings();
                }
                ValidateE();

            }
        });

        findViewById(R.id.bt_down).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRouteSettings.dateNew_22 ==null) {
                    Toast.makeText(activity, R.string.not_create_route, Toast.LENGTH_SHORT).show();
                } else {
                    mRouteSettings.isVerification = false;
                    TopDownRoutePoints.down(mRouteList);
                    activateRouteList();
                    saveRouteSettings();
                }
                btSenderToServer.setVisibility(View.GONE);
                ValidateE();
            }
        });

        findViewById(R.id.bt_route_show_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditText.setText("");
            }
        });

        mEditText = (EditText) findViewById(R.id.route_search_points_edit);
        mEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activateRouteListSearch(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    mEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {

                    mEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
                }
            }
        });

        btSenderToServer = (Button) findViewById(R.id.bt_route_to_server);
        btSenderToServer.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                if (!validateSenderToSever(activity)) return;
                mRouteSettings.guid = SettingsUser.core().getUuidDevice();
                mRouteSettings.isVerification = false;
                new SenderSetRoutePOST().send(activity, mSettings.url, mRouteSettings, new SenderGetRouterGET.IActionRoute() {
                    @Override
                    public void Action(SettingsRoute rs) {
                        Toast.makeText(activity, getString(R.string.route_send_to_server_goot), Toast.LENGTH_LONG).show();
                        mRouteSettings.guid = null;
                        mRouteSettings.dateNew_22 = null;
                        mRouteSettings.pointList.clear();
                        mRouteList.clear();
                        mCreateFrom.setVisibility(View.INVISIBLE);
                        mPointList = Configure.getSession().getList(MPoint.class, null);
                        activateRouteList();
                        activatePointList();
                        mTextStatus.setText(getString(R.string.wait_date_route));
                        saveRouteSettings();
                        btSenderToServer.setVisibility(View.GONE);
                        mRouteSettings.pointListCopy.clear();
                    }
                });
            }

        });
    }

    private void getLastDate(final Date startDate, final Date finishDate) {

        List<MPoint> mPoint = Linq.toStream(mPointList).where(new com.example.user.omsk.linq2.Predicate<MPoint>() {
            @Override
            public boolean apply(MPoint t) {
                return t.lastDateVisit.getTime() >= startDate.getTime() && t.lastDateVisit.getTime() <= finishDate.getTime();
            }
        }).toList();
        activatePointListABC(mPoint,null);

    }

    private boolean validateSenderToSever(Activity activity) {
        boolean res = true;
        if (mRouteSettings.dateNew_22 ==null) {
            Toast.makeText(activity, getString(R.string.not_create_route), Toast.LENGTH_SHORT).show();
            res = false;
        }
        if (mRouteSettings.pointList.size() == 0) {
            Toast.makeText(activity, getString(R.string.error_route_not_fill), Toast.LENGTH_LONG).show();
            res = false;
        }
        return res;
    }

    private void removePoints(List<MPoint> mPoints) {
        List<MPoint> delP = new ArrayList<>();
        for (MPoint mPoint : mPoints) {
            for (MPoint point : mPointList) {
                if (point.id == mPoint.id) {
                    delP.add(point);
                }
            }
        }
        mPointList.removeAll(delP);
    }


    private void addRoute(List<MPoint> mPoints) {
        for (final MPoint mPoint : mPoints) {
            MPoint point = Linq.toStream(mRouteList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MPoint>() {
                @Override
                public boolean apply(MPoint t) {
                    return t.id == mPoint.id;
                }
            });
            if (point == null) {
                mRouteList.add(mPoint);
            }
        }
    }

    private void activateRouteListSearch(String s) {
        if (s.equals("")) {
            activatePointList();
        } else {
            final List<MPoint> points = new ArrayList<>();
            for (MPoint point : mPointList) {
                if (point.name.toLowerCase(new Locale("ru", "RU")).contains(s.toString().toLowerCase(new Locale("ru", "RU")))) {
                    points.add(point);
                }
            }
//            this.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                  //  String string = String.valueOf(points.size()) + " из " + String.valueOf(mPointList.size());
//                }
//            });
            activatePointListABC(points,s);
        }
    }

    private void close() {
        finish();
        saveRouteSettings();
    }

    private void activateRouteList() {


        Parcelable index = null;
        try {
            index = mPointListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        ListAdapterRoutePoints points = new ListAdapterRoutePoints(null,this, R.layout.item_route_point_list, mRouteList, new IAddPointRoute() {
            @Override
            public void add(MPoint mPoint, boolean b) {
                if (b) {
                    mSelectForPoint.add(mPoint);
                } else {
                    mSelectForPoint.remove(mPoint);
                }
            }
        });

        mRouteListView.setAdapter(points);
        mRouteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
            }
        });
        if (index != null) {
            mRouteListView.onRestoreInstanceState(index);
        }
    }


    private void activatePointList() {

        Collections.sort(mPointList, new Comparator<MPoint>() {
            @Override
            public int compare(MPoint lhs, MPoint rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        Parcelable index = null;
        try {
            index = mPointListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        ListAdapterRoutePoints points = new ListAdapterRoutePoints(null,this, R.layout.item_route_point_list, mPointList, new IAddPointRoute() {
            @Override
            public void add(MPoint mPoint, boolean b) {
                if (b) {
                    mSelectForRouter.add(mPoint);
                } else {
                    mSelectForRouter.remove(mPoint);
                }
            }
        });

        mPointListView.setAdapter(points);
        mPointListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
            }
        });
        if (index != null) {
            mPointListView.onRestoreInstanceState(index);
        }


    }

    public void reload(int yar, int month, int dey) {
        Calendar myCalendar = Calendar.getInstance();
        myCalendar.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        myCalendar.set(yar, month, dey, 0, 0, 0);
        final Date myDate = myCalendar.getTime();

        final Context context = this;
        new SenderGetRouterGET().send(this, mSettings.url, myDate, SettingsUser.core().getUuidDevice(), new SenderGetRouterGET.IActionRoute() {
            @Override
            public void Action(SettingsRoute rs) {
                mRouteSettings.pointListCopy = new ArrayList<>();
                mCreateFrom.setVisibility(View.VISIBLE);
                mRouteSettings.dateNew_22 = myDate;
                mRouteSettings.pointList.clear();
                if (rs == null) {
                    Toast.makeText(context, getString(R.string.nod_data), Toast.LENGTH_SHORT).show();
                } else {
                    mRouteSettings.isVerification = rs.isVerification;
                    if (rs.isVerification) {
                        mRouteSettings.pointListCopy.addAll(rs.pointList);
                    }
                    mRouteSettings.pointList.addAll(rs.pointList);
                }

                mRouteList.clear();
                mPointList = Configure.getSession().getList(MPoint.class, null);
                for (final String s : mRouteSettings.pointList) {
                    MPoint f = Linq.toStream(mPointList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MPoint>() {
                        @Override
                        public boolean apply(MPoint t) {
                            return t.idu.equals(s);
                        }
                    });
                    if (f != null) {
                        mRouteList.add(f);
                        mPointList.remove(f);
                    }
                }
                ValidateE();
                activatePointList();
                activateRouteList();
                saveRouteSettings();
            }
        });
    }


    private void ValidateE() {
        btSenderToServer.setVisibility(View.GONE);
        mRouteSettings.isVerification = true;
        if (mTextStatus.getText().toString().equals(getString(R.string.wait_date_route))) {

        } else {
            if (mRouteSettings.dateNew_22 ==null) {
                mTextStatus.setText(getString(R.string.wait_date_route));
            } else {
                String string = getString(R.string.gfjgfj) + Utils.simpleDateFormatForRouteE(mRouteSettings.dateNew_22);
                mTextStatus.setText(string + getString(R.string.jjjjfjj));
            }

        }
        if (mRouteSettings.pointListCopy.size() == 0 || mRouteSettings.pointList.size() == 0) {
            btSenderToServer.setVisibility(View.VISIBLE);
            printeratorE();
            mRouteSettings.isVerification = true;
        } else if (mRouteSettings.pointListCopy.size() != mRouteSettings.pointList.size()) {
            btSenderToServer.setVisibility(View.VISIBLE);
            printeratorE();
            mRouteSettings.isVerification = true;
        } else {
            int ri = mRouteSettings.pointList.size();
            for (int i = 0; i < ri; i++) {
                String p1 = mRouteSettings.pointList.get(i);
                String p2 = mRouteSettings.pointListCopy.get(i);
                if (!p1.equals(p2)) {
                    btSenderToServer.setVisibility(View.VISIBLE);
                    printeratorE();
                    mRouteSettings.isVerification = true;
                    break;
                }
            }
        }
        SettingsRoute.save();
        if (mRouteSettings.pointList.size() == 0) {
            btSenderToServer.setVisibility(View.GONE);
        }
    }

    private void printeratorE() {

        if (mRouteSettings.dateNew_22 ==null) {
            mTextStatus.setText(getString(R.string.wait_date_route));
        } else {
            String string = getString(R.string.gfjgfj) + Utils.simpleDateFormatForRouteE(mRouteSettings.dateNew_22);
            mTextStatus.setText(string + getString(R.string.fggdfg));
        }
    }


    public void reloadSender(int yar, int month, int dey) {


        Calendar myCalendar = Calendar.getInstance();
        myCalendar.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        myCalendar.set(yar, month, dey, 0, 0, 0);
        Date myDate = myCalendar.getTime();

        final Context context = this;
        new SenderGetRouterGET().send(this, mSettings.url, myDate, SettingsUser.core().getUuidDevice(), new SenderGetRouterGET.IActionRoute() {
            @Override
            public void Action(SettingsRoute routeSettings) {
                if (routeSettings == null) {
                    Toast.makeText(context, R.string.nonedata, Toast.LENGTH_SHORT).show();
                } else {
                    mRouteList.clear();
                    mPointList = Configure.getSession().getList(MPoint.class, null);
                    for (final String s : routeSettings.pointList) {
                        MPoint f = Linq.toStream(mPointList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MPoint>() {
                            @Override
                            public boolean apply(MPoint t) {
                                return t.idu.equals(s);
                            }
                        });
                        if (f != null) {
                            mRouteList.add(f);
                            mPointList.remove(f);
                        }
                    }
                    activatePointList();
                    activateRouteList();
                    saveRouteSettings();
                    mRouteSettings.pointListCopy = new ArrayList<>();
                    ValidateE();
                }
            }
        });


        mTextStatus.setText(getString(R.string.routedate) + Utils.simpleDateFormatForRouteE(mRouteSettings.dateNew_22));
        saveRouteSettings();
    }

    private void createListABC() {
        MyListViewAbc listView = (MyListViewAbc) findViewById(R.id.list_abc_route);
        listView.setIAction(new IActionE<String>() {
            @Override
            public void action(String o) {
                showSearchFirstChar(o);
            }
        });
    }

    private void showSearchFirstChar(String s) {

        final List<MPoint> points = new ArrayList<>();
        for (MPoint point : mPointList) {
            if (point.name.trim().toLowerCase().startsWith(s.trim().toLowerCase())) {
                points.add(point);
            }
        }
        activatePointListABC(points,null);
    }

    private void activatePointListABC(List<MPoint> pointsL,String strSearch) {
        ListAdapterRoutePoints points = new ListAdapterRoutePoints(strSearch,this, R.layout.item_route_point_list, pointsL, new IAddPointRoute() {
            @Override
            public void add(MPoint mPoint, boolean b) {
                if (b) {
                    mSelectForRouter.add(mPoint);
                } else {
                    mSelectForRouter.remove(mPoint);
                }
            }
        });
        mPointListView.setAdapter(points);
    }


    private void saveRouteSettings() {
        mRouteSettings.pointList = new ArrayList<>();
        for (MPoint mPoint : mRouteList) {
            mRouteSettings.pointList.add(mPoint.idu);
        }
        SettingsRoute.save();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
