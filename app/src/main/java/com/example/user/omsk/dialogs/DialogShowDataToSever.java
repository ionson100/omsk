package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.BaseMedia;
import com.example.user.omsk.models.MPhoto;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVideo;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.auto.MAuto;
import com.example.user.omsk.kassa.MChecks;
import com.example.user.omsk.kassa.MZReport;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DialogShowDataToSever extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        List<String> list = new ArrayList<>();
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_show_data_to_server, null);
        ((TextView) view.findViewById(R.id.title_dalog)).setText(getString(R.string.вhsjdhsd));
        builder.setView(view);

        List<MPoint> pointList = Configure.getSession().getList(MPoint.class, " state_object = 1 ");
        for (MPoint mPoint : pointList) {
            list.add(getString(R.string.vdfsdf) + mPoint.name);
        }

        List<MVisitStory> visitStoryList = Configure.getSession().getList(MVisitStory.class, " state_object = 1 ");
        for (MVisitStory mVisitStory : visitStoryList) {
            list.add("Визит: " + mVisitStory.getPoint().name);
        }

        List<MChecks> mChecksList = Configure.getSession().getList(MChecks.class, " is_sender = 0");
        List<MZReport> mzReportList = Configure.getSession().getList(MZReport.class, null);

        for (MChecks mChecks : mChecksList) {
            list.add(getString(R.string.xcvbbb) + Utils.simpleDateFormatE(mChecks.date));
        }
        for (MZReport mzReport : mzReportList) {
            list.add(getString(R.string.xcbbbx) + Utils.simpleDateFormatE(mzReport.mZetReprot.firstDateOfd));
        }

        ISession session = Configure.getSession();
        List<MPhoto> mPhotoList = Utils.getPhotos(session);
        List<MVideo> mVideoList = Utils.getVideo(session);
        List<BaseMedia> baseMediaList = new ArrayList<>();
        baseMediaList.addAll(mPhotoList);
        for (MVideo mVideo : mVideoList) {
            File file = new File(mVideo.path);
            if (file.exists() && file.length() <= Utils.MAX_FILE_SIZE) {
                baseMediaList.add(mVideo);
            }
        }
        for (BaseMedia baseMedia : baseMediaList) {
            list.add(getString(R.string.bnggggn) + baseMedia.path);
        }

        List<MAuto> mAutos = Configure.getSession().getList(MAuto.class, "is_sender = 1");
        for (MAuto mAuto : mAutos) {
            list.add(getString(R.string.ggngngn) + Utils.simpleDateFormatE(mAuto.date1));
        }

        ListView listView = (ListView) view.findViewById(R.id.list_to_server);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);


        final AlertDialog dialog = builder.create();
        view.findViewById(R.id.btclose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        return dialog;
    }
}






