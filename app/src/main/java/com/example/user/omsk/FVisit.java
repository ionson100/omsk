package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.addin.NewOrderStockActivity;
import com.example.user.omsk.dialogs.DialogTaskVisit;
import com.example.user.omsk.kassa.UtilsKassa;
import com.example.user.omsk.models.MAlertPoint;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MTask;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.test.TestAct;
import com.example.user.omsk.visitcontrol.VisitMarketing;

import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;


public class FVisit extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FVisit.class);
    private VisitMarketing visitMarketing;
    public static final int TAKE_PHOTO_CODE = 11;
    public static final int ACTION_TAKE_VIDEO = 23;
    private View mView;

    private Settings mSettings;

    public FVisit() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_visit, container, false);
        try {
            mSettings = Settings.core();
            if (mSettings.visitFromError != null) {
                MVisit mVisit = mSettings.visitFromError;
                mSettings.visitFromError = null;
                mSettings.setVisit(mVisit);
            }
            if (mSettings.getVisit().order_type_id == null) {
                MOrderType mOrderType = Configure.getSession().getList(MOrderType.class, " index1 = 0").get(0);
                mSettings.getVisit().order_type_id = mOrderType.idu;
            }



            mSettings.getVisit().setStart((Utils.dateToInt(new Date())));
            Button btStart, btBalance, btRequest, btCommit;
            btStart = (Button) mView.findViewById(R.id.bt_action_start);
            btBalance = (Button) mView.findViewById(R.id.bt_action_balance);
            btRequest = (Button) mView.findViewById(R.id.bt_action_request);
            btCommit = (Button) mView.findViewById(R.id.bt_action_commit);

            mView.findViewById(R.id.bt_action_story_point).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_POINT_STORY_FOR, getActivity());
                }
            });

            btStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_START, getActivity());
                }
            });

            btBalance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_BALANCE, getActivity());
                }
            });

            btRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (UtilsKassa.getMCheckDataCache(mSettings.getVisit().checksDataList) != null) {
                        Settings.showFragment(StateSystem.VISIT_SALE, getActivity());
                    } else {
                        if (mSettings.getVisit().selectProductForActions.size() > 0) {

                            Utils.messageBoxE(getActivity(), getString(R.string.kasldkad), getString(R.string.error_bonus), "Отказаться", "Продолжить", new IActionE() {
                                @Override
                                public void action(Object v) {
                                }
                            }, new IActionE() {
                                @Override
                                public void action(Object v) {
                                    mSettings.getVisit().selectProductForActions.clear();
                                    Settings.showFragment(StateSystem.VISIT_SALE, getActivity());
                                }
                            }, true);
                        } else {

                            if(mSettings.getVisit().selectProductForSale.size()==0){
                                Settings.showFragment(StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE, getActivity());
                            }else {
                                Settings.showFragment(StateSystem.VISIT_SALE, getActivity());
                            }

                        }
                    }
                }
            });
            btCommit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mSettings.getVisit().selectProductForActions.size() > 0 || mSettings.getVisit().selectProductForPresent.size() > 0) {
                        if (mSettings.getVisit().member_action == null || mSettings.getVisit().member_action.trim().length() == 0) {
                            Utils.messageBox(getString(R.string.adad), getString(R.string.lksajdkajdsas), getActivity(), new IActionE() {
                                @Override
                                public void action(Object o) {
                                    return;
                                }
                            });
                        } else {
                            Settings.showFragment(StateSystem.VISIT_COMMIT, getActivity());
                        }
                    } else {
                        Settings.showFragment(StateSystem.VISIT_COMMIT, getActivity());
                    }
                }
            });

            if (Settings.getStateSystem() == StateSystem.VISIT_COMMIT) {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }

            if (mSettings.getVisit().latitude == 0 || mSettings.getVisit().longitude == 0) {

                ((MainActivity) getActivity()).STARTSERVICEGEO();

            }
            mView.findViewById(R.id.bt_refund).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_REFUND, getActivity());
                }
            });



            final MAlertPoint alertPoint = mSettings.getVisit().point.getAlertPoint();
            if (alertPoint != null) {
                final LinearLayout panelAlert = (LinearLayout) mView.findViewById(R.id.panel_alert);
                panelAlert.setVisibility(View.VISIBLE);
                TextView textView = (TextView) mView.findViewById(R.id.alert_message);
                textView.setText(alertPoint.message);
                mView.findViewById(R.id.delete_alert).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.messageBox(getString(R.string.delete), getString(R.string.delete2), getActivity(), new IActionE() {
                            @Override
                            public void action(Object e) {
                                Configure.getSession().delete(alertPoint);
                                panelAlert.setVisibility(View.GONE);
                            }
                        });
                    }
                });
            }

            visitMarketing = (VisitMarketing) mView.findViewById(R.id.visit_marceting);
            visitMarketing.mainActivity = (MainActivity) getActivity();


            ///////////////////////////////////////////////////// показывать кнопки задачи
            List<MTask> listTask = Configure.getSession().getList(MTask.class, null);
            if (listTask.size() > 0) {
                View vb = mView.findViewById(R.id.bt_task);
                vb.setVisibility(View.VISIBLE);
                vb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogTaskVisit taskVisit = new DialogTaskVisit();
                        taskVisit.setSettings(mSettings);
                        taskVisit.show(getActivity().getSupportFragmentManager(), "sdsfds");
                    }
                });
            } else {
                View vb = mView.findViewById(R.id.bt_task);
                vb.setVisibility(View.GONE);
            }

            mView.findViewById(R.id.bt_help).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.showHelpNote(getActivity(),  "note2.html");
                }
            });


            {
                View view = mView.findViewById(R.id.panel_free_product);
                Object o = Configure.getSession().executeScalar(" select count(*) from present_types where is_active=0 ");
                if (o.toString().equals("0")) {
                    view.setVisibility(View.GONE);
                } else {
                    view.setVisibility(View.VISIBLE);
                }
            }


            mView.findViewById(R.id.bt_new_order).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(getActivity(), NewOrderStockActivity.class));
                }
            });

            mView.findViewById(R.id.bt_test).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivity(new Intent(getActivity(), TestAct.class));
                }
            });

        } catch (Exception ex) {

            String msg = Utils.getErrorTrace(ex);
            Utils.sendMessage("FVisit:" + msg,  (MyApplication) getActivity().getApplication());
            log.error(ex);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();

        }
        return mView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //MyApplication.MVISIT = null;
    }

    public void mediaActivate() {
        visitMarketing.showPhoto();
    }


}
