package com.example.user.omsk.senders;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.user.omsk.Certificate;
import com.example.user.omsk.FEditAddPoint;
import com.example.user.omsk.FPoint;
import com.example.user.omsk.fotocontrol.MyViewFotoPoint;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class SenderDownloadImagePointPOST {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderDownloadImagePointPOST.class);
    private String filename;
    private Settings settings;

    private Activity activity;

    public void send(String point_id,  Activity activity) {

        this.activity = activity;

        this.filename = point_id;
        this.settings = Settings.core();
        new SenderWorkerTack().execute();
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            File f = new File(MyViewFotoPoint.getPhotoDirectoryPoint() + "/" + filename + ".jpg");
            if (f.exists()) {

                for (Fragment fragment : ((AppCompatActivity) activity).getSupportFragmentManager().getFragments()) {

                    if (fragment instanceof FPoint) {
                        ((FPoint) fragment).refrashImage();
                    }
                    if (fragment instanceof FEditAddPoint) {
                        ((FEditAddPoint) fragment).refrashImage();
                    }
                }
            }

        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpsURLConnection conection=null;
            int count; try {
                URL url = new URL(Utils.HTTP + settings.url + "/static/media/point_photos/" + URLEncoder.encode(filename + ".jpg", "UTF-8"));
                conection = (HttpsURLConnection) url.openConnection();
                HttpsURLConnection.setDefaultHostnameVerifier(new UtilsSender.NullHostNameVerifier());
                conection.setSSLSocketFactory(UtilsSender.getSslSocketFactory(activity));
                //////////////////////////////////////////////////////////////////addin
                String cert = Certificate.getCertificateSHA1Fingerprint(activity);
                conection.setInstanceFollowRedirects(false);
                conection.setRequestProperty("Content-user", cert);
                conection.setReadTimeout(15000 /*milliseconds*/);
                conection.setConnectTimeout(20000 /* milliseconds */);
                conection.setRequestMethod("GET");
                conection.connect();

                int lenghtOfFile = conection.getContentLength() + 100;
                InputStream input = null;
                OutputStream output = null;
                try{
                    input = conection.getInputStream();
                    output = new FileOutputStream(MyViewFotoPoint.getPhotoDirectoryPoint() + "/" + filename + ".jpg");
                    byte data[] = new byte[lenghtOfFile];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                }finally {
                    if(input!=null){
                        input.close();
                    }
                    if(output!=null){
                        output.close();
                    }
                }




            } catch (Exception e) {
                log.error(e);
                //Configure.getSession().insert(new MError(" download file point image:" + filename + " " + e.getMessage()));
            }finally {

            }

            return null;
        }
    }
}
