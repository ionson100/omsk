package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import org.osmdroid.views.MapView;

/**
 * тип для пересылки данных определения точки по карте
 */
public class Mapper {
    public Mapper(MyItemizedOverlay myItemizedOverlay, MapView mapView) {

        this.myItemizedOverlay = myItemizedOverlay;
        this.mapView = mapView;
    }

    public final MyItemizedOverlay myItemizedOverlay;
    public final MapView mapView;
}

