package com.example.user.omsk.visitcontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import java.util.List;

public class VisitItogo extends LinearLayout {

    private View view;
    private Pupper

            balance_count,
            sale_item,
            sale_count,
            sale_price,
            debt_price,
            kassa_nal;
    private Settings mSettings;

    public VisitItogo(Context context) {
        super(context);
        init();
    }

    public VisitItogo(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VisitItogo(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    void bulbul() {

        if (mSettings.getVisit().selectProductForBalance.size() == 0 && mSettings.getVisit().selectProductForSale.size() == 0 &&
                mSettings.getVisit().debtList.size() == 0) {
            this.setVisibility(GONE);
        } else {
            this.setVisibility(VISIBLE);
        }

        double item = 0, sale = 0;

        for (MProduct mProduct : mSettings.getVisit().selectProductForBalance) {
            item = item + mProduct.getAmount_core();
        }
        balance_count.setPairString(getContext().getString(R.string.xzcxzc), Utils.getStringDecimal(item));
        if (item == 0) {
            balance_count.setVisibility(GONE);
        } else {
            balance_count.setVisibility(VISIBLE);
        }


        item = 0;
        for (MProduct mProduct : mSettings.getVisit().selectProductForSale) {
            item = item + mProduct.getAmount_core();
        }
        sale_count.setPairString(getContext().getString(R.string.asdasddd), Utils.getStringDecimal(item));
        sale_item.setPairString(getContext().getString(R.string.asddddasd), "" + mSettings.getVisit().selectProductForSale.size());


        item = 0;
        for (MProduct mProduct : mSettings.getVisit().selectProductForSale) {
            item = item + mProduct.getAmount_core() * mProduct.price;
        }
        sale_price.setPairString(getContext().getString(R.string.sdddasd), Utils.getStringDecimal(item));
        if (item == 0) {
            sale_count.setVisibility(GONE);
            sale_item.setVisibility(GONE);
            sale_price.setVisibility(GONE);
        } else {
            sale_count.setVisibility(VISIBLE);
            sale_item.setVisibility(VISIBLE);
            sale_price.setVisibility(VISIBLE);
        }
        sale = sale + item;
        item = 0;

        List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, " point_id = ? order by date ", mSettings.getVisit().point.idu);


        for (MDebt mDebt : mDebtList) {
            if (mSettings.getVisit().debtList.contains(mDebt.order_id)) {
                item = item + mDebt.amount * mDebt.price;
            }
        }

        sale = sale + item;
        debt_price.setPairString(getContext().getString(R.string.sddddasdasd), Utils.getStringDecimal(item));

        kassa_nal.setPairString(getContext().getString(R.string.weeqwe), Utils.getStringDecimal(sale));
        if (item == 0) {
            debt_price.setVisibility(GONE);
        } else {
            debt_price.setVisibility(VISIBLE);
        }

    }

    private void init() {
        mSettings = Settings.core();


        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = vi.inflate(R.layout.visit_control_itogo, null);
        this.addView(view, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));


        balance_count = (Pupper) view.findViewById(R.id.c_itogo_balance_count);
        sale_item = (Pupper) view.findViewById(R.id.c_itogo_sale_item);
        sale_count = (Pupper) view.findViewById(R.id.c_itogo_sale_count);
        sale_price = (Pupper) view.findViewById(R.id.c_itogo_sale_price);
        debt_price = (Pupper) view.findViewById(R.id.c_itogo_debt_price);
        kassa_nal = (Pupper) view.findViewById(R.id.c_itogo_kassa_nal);

        bulbul();


    }

    public void refrash() {
        bulbul();
    }


}
