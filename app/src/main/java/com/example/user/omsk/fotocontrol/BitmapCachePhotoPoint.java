package com.example.user.omsk.fotocontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

import com.example.user.omsk.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * ion100 on 05.10.2017.
 */

public class BitmapCachePhotoPoint {

    public static void cache(String filename) {
        File file = new File(filename);
        Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
        if (bitmap != null) {
            getBytesFromBitmap(bitmap);
        }


    }

    private static byte[] getBytesFromBitmap(Bitmap image) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

}
