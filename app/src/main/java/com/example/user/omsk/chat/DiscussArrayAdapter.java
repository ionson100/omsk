package com.example.user.omsk.chat;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.models.MChat;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.ArrayList;
import java.util.List;

public class DiscussArrayAdapter extends ArrayAdapter<MChat> {

    private final List<MChat> mCountries = new ArrayList<>();

    public DiscussArrayAdapter(Context context) {
        super(context, R.layout.chat_item);
    }

    @Override
    public void add(MChat object) {
        mCountries.add(object);
        super.add(object);
    }

    public int getCount() {
        return this.mCountries.size();
    }

    public MChat getItem(int index) {
        return this.mCountries.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MChat comment = getItem(position);

        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (comment.common) {
            row = inflater.inflate(R.layout.chat_item_admin, parent, false);
        } else {
            row = inflater.inflate(R.layout.chat_item, parent, false);
        }


        LinearLayout wrapper = (LinearLayout) row.findViewById(R.id.wrapper);

        TableLayout tableChat = (TableLayout) row.findViewById(R.id.comment);
        TextView textView = (TextView) row.findViewById(R.id.text_chat1);
        TextView textView1 = (TextView) row.findViewById(R.id.user_chat1);
        textView1.setText(comment.user_name);
        TextView textView3 = (TextView) row.findViewById(R.id.timer_chat1);
        textView3.setText(Utils.simpleDateFormatE(comment.date));
        textView.setText(comment.text);
        TextView textView_function = (TextView) row.findViewById(R.id.text_function);


        textView.setTypeface(MyApplication.getTypeface(getContext()));
        textView1.setTypeface(MyApplication.getTypeface(getContext()));
        textView3.setTypeface(MyApplication.getTypeface(getContext()));
        textView_function.setTypeface(MyApplication.getTypeface(getContext()));


        textView_function.setText(comment.user_function);
        if (comment.common) {
            tableChat.setBackgroundResource(R.drawable.bubble_3);
            wrapper.setGravity(Gravity.LEFT);
        } else {
            if (comment.owner) {
                tableChat.setBackgroundResource(R.drawable.bubble_2);
                wrapper.setGravity(Gravity.RIGHT);
            } else {
                tableChat.setBackgroundResource(R.drawable.bubble_1);
                wrapper.setGravity(Gravity.LEFT);
            }
        }
        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}

