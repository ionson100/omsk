package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// хранилище типов точек ( магазин киоск)
@Table("distributionchannel")
public class MDistributionchannel implements Serializable, IUsingGuidId {

    @PrimaryKey("id")
    public transient int id;
    @SerializedName("id")
    @Column("idu")
    public String idu;
    @SerializedName("name")
    @Column("name")
    public String name;
    @SerializedName("description")
    @Column("description")
    public String description;

    public MDistributionchannel() {
        this.idu = Utils.getUuid();
    }

    public String get_id() {
        return idu;
    }
}
