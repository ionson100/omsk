package com.example.user.omsk.kassa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import com.atol.drivers.fptr.Fptr;
import com.example.user.omsk.dialogs.DialogReportKKMMoney;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MSummaKKM;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.senders.SenderErrorPOST;

import java.util.List;


public class PrintKkmSumma {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PrintKkmSumma.class);

    Activity activity;
    Settings mSettings;

    private ProgressDialog dialog;
    private String errorText;


    public void getSummKKM(final Activity activity, final Settings mSettings) throws Exception {

        this.activity = activity;
        this.mSettings = mSettings;

        new AsyncTask<Void, Void, Double>() {


            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }


            @Override
            protected Double doInBackground(Void... params) {

                Double aDouble=null;
                fptr = new Fptr();
                try {
                    fptr.create(activity.getApplication());
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }

                    if (fptr.put_RegisterNumber(10) < 0) {
                        checkError();
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError();
                    }

                    aDouble = new Double(fptr.get_Summ());

                    summKKM(aDouble);



                } catch (Exception e) {
                    log.error(e);
                    errorText = e.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка запрос наличности в ккм\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return aDouble;
            }


            @Override
            protected void onPreExecute() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog = Utils.factoryDialog(activity, "Запрос наличности ККМ.", null);
                        dialog.show();
                    }
                });
            }

            @Override
            protected void onPostExecute(Double aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                if (errorText != null) {
                    Utils.messageBox(activity.getString(R.string.error), errorText, activity, null);
                } else {
                    DialogReportKKMMoney money=new DialogReportKKMMoney();
                    money.nalKKM=aVoid.doubleValue();
                    money.nalCore=Utils.getNallCore();
                    money.show(((AppCompatActivity)activity).getSupportFragmentManager(), "asoia");
                }
            }
        }.execute();

    }

    public static void summKKM(double a){

        List<MSummaKKM> kkms= Configure.getSession().getList(MSummaKKM.class,null);
        if(kkms.size()==0){
            MSummaKKM summaKKM=new MSummaKKM();
            summaKKM.sumkkm=a;
            Configure.getSession().insert(summaKKM);
        }else {
            MSummaKKM summaKKM=kkms.get(0);
            summaKKM.sumkkm=a;
            Configure.getSession().update(summaKKM);
        }

    }




}
