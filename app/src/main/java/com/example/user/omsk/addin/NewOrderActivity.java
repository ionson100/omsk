package com.example.user.omsk.addin;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.kassa.DialogPrintDistinct;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.models.MNewOrder;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.example.user.omsk.models.MError;

public class NewOrderActivity extends AppCompatActivity {

    public static org.apache.log4j.Logger log = Logger.getLogger(NewOrderActivity.class);
    private List<MProduct> mProducts = new ArrayList<>();
    private List<MPoint> mPoints = new ArrayList<>();
    private List<MNewOrder> mNewOrders = new ArrayList<>();
    private ListView mListView;
    ListAdapterOrder listAdapterOrder;
    private Settings mSettings;
    List<MVisitStory> stories ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {

            mSettings = Settings.core();
            stories =  Configure.getSession().getList(MVisitStory.class, null);
            setContentView(R.layout.activity_new_order);
            mListView = (ListView) findViewById(R.id.list_point);
            mPoints = Configure.getSession().getList(MPoint.class, null);
            mProducts = Configure.getSession().getList(MProduct.class, null);

            showAll();
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    MPoint mPoint = listAdapterOrder.getItem(position);
                    if (mPoint == null) return;
                    DialogNewOrder dialogNewOrder = new DialogNewOrder();
                    dialogNewOrder.setActivate(mPoint, mNewOrders);
                    dialogNewOrder.show(getSupportFragmentManager(), "soidoasdi");
                }
            });

            findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            findViewById(R.id.bt_show_all).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAll();
                }
            });

            findViewById(R.id.bt_show_cur_day).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCurDay();
                }
            });

            findViewById(R.id.bt_period).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPeriod();
                }
            });

            findViewById(R.id.bt_month).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMonth();
                }
            });
        } catch (SQLException ex) {
            String msg = Utils.getErrorTrace(ex);
            Utils.sendMessage("dadata:" + msg,  (MyApplication) getApplication());
            log.error(ex);
            //Configure.getSession().insert(new MError(msg));
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }


    void showMonth() {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.DAY_OF_MONTH, 1);
        Date dd = date.getTime();
        long i = dd.getTime();
        mNewOrders = Configure.getSession().getList(MNewOrder.class, " date > ? order by date", i);
        addMyVisit(mNewOrders);
        Map<String, MPoint> map = new HashMap<>();
        for (final MNewOrder mNewOrder : mNewOrders) {
            if (map.containsKey(mNewOrder.point_id) == false) {
                MPoint mPoint = Linq.toStream(mPoints).firstOrDefault(new Predicate<MPoint>() {
                    @Override
                    public boolean apply(MPoint t) {
                        return t.idu.equals(mNewOrder.point_id);
                    }
                });
                if (mPoint != null) {
                    map.put(mPoint.idu, mPoint);
                } else {
                    MPoint point = new MPoint();
                    point.name = "Не найдена";
                    point.idu = mNewOrder.point_id;
                    map.put(point.idu, point);
                }
            }
        }
        List<MPoint> list = new ArrayList<>();

        for (Map.Entry<String, MPoint> ss : map.entrySet()) {
            list.add(ss.getValue());
        }

        Collections.sort(list, new Comparator<MPoint>() {
            @Override
            public int compare(MPoint lhs, MPoint rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        listAdapterOrder = new ListAdapterOrder(getApplicationContext(), 0, list);
        mListView.setAdapter(listAdapterOrder);

    }

    void showPeriod() {
        DialogPrintDistinct distinct = new DialogPrintDistinct();
        distinct.setmIActionE(new IActionE() {
            @Override
            public void action(Object o) {

                Date[] dd = (Date[]) o;
                Date d1 = dd[0];
                Date d2 = dd[1];
                long i1 = d1.getTime();
                long i2 = d2.getTime();

                mNewOrders = Configure.getSession().getList(MNewOrder.class, "date BETWEEN ? and ? order by date", i1, i2);



                List<MNewOrder> listz = new ArrayList<>();

                for (MVisitStory story : stories) {
                    if (story.getNewOrderProductList().size() > 0) {
                        MNewOrder order = new MNewOrder();
                        order.point_id = story.point_id;
                        order.date = story.finish;
                        order.products = story.getNewOrderProductList();
                        listz.add(order);
                    }
                }
                for (final MNewOrder order : listz) {
                    MNewOrder ff = Linq.toStream(mNewOrders).firstOrDefault(new Predicate<MNewOrder>() {
                        @Override
                        public boolean apply(MNewOrder t) {
                            return t.date == order.date;
                        }
                    });
                    if (ff == null) {
                        if (order.date >= i1 && order.date <= i2) {
                            mNewOrders.add(order);
                        }
                    }
                }

                Collections.sort(mNewOrders, new Comparator<MNewOrder>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public int compare(MNewOrder lhs, MNewOrder rhs) {
                        return Integer.compare(lhs.date,rhs.date);
                    }
                });


                Map<String, MPoint> map = new HashMap<>();
                for (final MNewOrder mNewOrder : mNewOrders) {
                    if (map.containsKey(mNewOrder.point_id) == false) {
                        MPoint mPoint = Linq.toStream(mPoints).firstOrDefault(new Predicate<MPoint>() {
                            @Override
                            public boolean apply(MPoint t) {
                                return t.idu.equals(mNewOrder.point_id);
                            }
                        });
                        if (mPoint != null) {
                            map.put(mPoint.idu, mPoint);
                        } else {
                            MPoint point = new MPoint();
                            point.name = "Не найдена";
                            point.idu = mNewOrder.point_id;
                            map.put(point.idu, point);
                        }
                    }
                }
                List<MPoint> list = new ArrayList<>();

                for (Map.Entry<String, MPoint> ss : map.entrySet()) {
                    list.add(ss.getValue());
                }

                Collections.sort(list, new Comparator<MPoint>() {
                    @Override
                    public int compare(MPoint lhs, MPoint rhs) {
                        return lhs.name.compareTo(rhs.name);
                    }
                });
                listAdapterOrder = new ListAdapterOrder(getApplicationContext(), 0, list);
                mListView.setAdapter(listAdapterOrder);


            }
        });
        distinct.show(NewOrderActivity.this.getSupportFragmentManager(), "adssdssd");
    }

    void showCurDay() {

        Date date = getStartOfDay(Utils.curDate());
        long i = date.getTime();
        mNewOrders = Configure.getSession().getList(MNewOrder.class, " date > ? order by date", i);
        addMyVisit(mNewOrders);

        Map<String, MPoint> map = new HashMap<>();
        for (final MNewOrder mNewOrder : mNewOrders) {
            if (map.containsKey(mNewOrder.point_id) == false) {
                MPoint mPoint = Linq.toStream(mPoints).firstOrDefault(new Predicate<MPoint>() {
                    @Override
                    public boolean apply(MPoint t) {
                        return t.idu.equals(mNewOrder.point_id);
                    }
                });
                if (mPoint != null) {
                    map.put(mPoint.idu, mPoint);
                } else {
                    MPoint point = new MPoint();
                    point.name = "Не найдена";
                    point.idu = mNewOrder.point_id;
                    map.put(point.idu, point);
                }
            }
        }
        List<MPoint> list = new ArrayList<>();

        for (Map.Entry<String, MPoint> ss : map.entrySet()) {
            list.add(ss.getValue());
        }

        Collections.sort(list, new Comparator<MPoint>() {
            @Override
            public int compare(MPoint lhs, MPoint rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        listAdapterOrder = new ListAdapterOrder(getApplicationContext(), 0, list);
        mListView.setAdapter(listAdapterOrder);
    }

    void showAll() {

        mNewOrders = Configure.getSession().getList(MNewOrder.class, "1=1 order by date");
        addMyVisit(mNewOrders);
        Map<String, MPoint> map = new HashMap<>();
        for (final MNewOrder mNewOrder : mNewOrders) {
            if (map.containsKey(mNewOrder.point_id) == false) {
                MPoint mPoint = Linq.toStream(mPoints).firstOrDefault(new Predicate<MPoint>() {
                    @Override
                    public boolean apply(MPoint t) {
                        return t.idu.equals(mNewOrder.point_id);
                    }
                });
                if (mPoint != null) {
                    map.put(mPoint.idu, mPoint);
                } else {
                    MPoint point = new MPoint();
                    point.name = getString(R.string.rtetret);
                    point.idu = mNewOrder.point_id;
                    map.put(point.idu, point);
                }
            }
        }
        List<MPoint> list = new ArrayList<>();

        for (Map.Entry<String, MPoint> ss : map.entrySet()) {
            list.add(ss.getValue());
        }

        Collections.sort(list, new Comparator<MPoint>() {
            @Override
            public int compare(MPoint lhs, MPoint rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        listAdapterOrder = new ListAdapterOrder(getApplicationContext(), 0, list);
        mListView.setAdapter(listAdapterOrder);
    }


    class ListAdapterOrder extends ArrayAdapter<MPoint> {

        @NonNull
        private final Context context;
        @NonNull
        private final List<MPoint> points;

        public ListAdapterOrder(@NonNull Context context, @LayoutRes int resource, @NonNull List<MPoint> points) {
            super(context, resource, points);
            this.context = context;
            this.points = points;
            this.points.add(null);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;

            final MPoint p = getItem(position);
            if (p != null) {


                try{
                    mView = LayoutInflater.from(getContext()).inflate(R.layout.item_list_point, null);
                    mView.findViewById(R.id.image_debt).setVisibility(View.INVISIBLE);
                    //   mView.findViewById(R.id.imageView_route).setVisibility(View.GONE);
                    mView.findViewById(R.id.image_alert).setVisibility(View.INVISIBLE);
                    //   mView.findViewById(R.id.image_task).setVisibility(View.GONE);
                    TextView textView = (TextView) mView.findViewById(R.id.point_name_route);
                    textView.setTextColor(Color.BLACK);
                    textView.setText(p.name);
                }catch (Exception ex){
                    log.error(ex);

                }

            } else {
                try {
                    mView = LayoutInflater.from(getContext()).inflate(R.layout.item_itogo_new_order, null);

                    Pupper pupper_order_itogo_point = (Pupper) mView.findViewById(R.id.order_itogo_point);
                    Pupper pupper_order_itogo_pachek = (Pupper) mView.findViewById(R.id.order_itogo_pachek);
                    Pupper pupper_order_itogo_pachek_name = (Pupper) mView.findViewById(R.id.order_itogo_pachek_name);

                    Map<String, MProduct> map = new HashMap<>();
                    for (MNewOrder mNewOrder : mNewOrders) {
                        for (MProduct product : mNewOrder.products) {
                            if (map.containsKey(product.idu)) {
                                MProduct pp = map.get(product.idu);
                                pp.setAmountCore(pp.getAmount_core() + product.getAmount_core());
                            } else {
                                map.put(product.idu, product);
                            }
                        }
                    }
                    List<MProduct> list = new ArrayList<>(map.values());


                    Collections.sort(list, new Comparator<MProduct>() {
                        @Override
                        public int compare(MProduct lhs, MProduct rhs) {
                            return lhs.name.compareTo(rhs.name);
                        }
                    });

                    pupper_order_itogo_point.setPairString(getString(R.string.sdsddd), String.valueOf(points.size() - 1));
                    pupper_order_itogo_pachek_name.setPairString(getString(R.string.jjkdsjf), String.valueOf(list.size()));

                    LinearLayout panel_order_products = (LinearLayout) mView.findViewById(R.id.panel_order_products);

                    double itogoE = 0;

                    for (MProduct mProduct : list) {
                        View viewOrder = LayoutInflater.from(getContext()).inflate(R.layout.item_product_story, null);
                        TextView name = (TextView) viewOrder.findViewById(R.id.name_s);
                        TextView amountE = (TextView) viewOrder.findViewById(R.id.amount_s);
                        TextView priceE = (TextView) viewOrder.findViewById(R.id.price_s);
                        priceE.setVisibility(View.GONE);
                        TextView itogo = (TextView) viewOrder.findViewById(R.id.itogo_s);
                        itogo.setVisibility(View.GONE);

                        name.setTextColor(Color.BLACK);
                        amountE.setTextColor(Color.BLACK);
                        name.setText(mProduct.name);

                        amountE.setText(String.valueOf(mProduct.getAmount_core()));
                        panel_order_products.addView(viewOrder);
                        itogoE = itogoE + mProduct.getAmount_core();
                    }
                    pupper_order_itogo_pachek.setPairString(getString(R.string.kjksdasd), String.valueOf(itogoE));
                }catch (Exception ex){
                    log.error(ex);

                }
            }
            return mView;
        }
    }

    public Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private void addMyVisit(List<MNewOrder> liste) {

        List<MNewOrder> list = new ArrayList<>();

        for (MVisitStory story : stories) {
            if (story.getNewOrderProductList().size() > 0) {
                MNewOrder order = new MNewOrder();
                order.point_id = story.point_id;
                order.date = story.finish;
                order.products = story.getNewOrderProductList();
                list.add(order);
            }
        }
        for (final MNewOrder order : list) {
            MNewOrder ff = Linq.toStream(liste).firstOrDefault(new Predicate<MNewOrder>() {
                @Override
                public boolean apply(MNewOrder t) {
                    return t.date == order.date;
                }
            });
            if (ff == null) {
                liste.add(order);
            }
        }

        Collections.sort(liste, new Comparator<MNewOrder>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MNewOrder lhs, MNewOrder rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });

    }
}

