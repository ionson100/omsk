package com.example.user.omsk.action;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.google.gson.annotations.SerializedName;

public class TempFreePresent {
    public String id;
    public String name;
    @SerializedName("active")
    public boolean archive;
}
