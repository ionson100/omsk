package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.senders.IActionResponse;
import com.example.user.omsk.senders.UtilsSender;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;

import org.apache.log4j.Logger;


public class SendReportToServer {

    public static org.apache.log4j.Logger log = Logger.getLogger(SendReportToServer.class);
    private android.app.Application mApplication;
    private Settings mSettings;
    private MZReport mzReport;
    private ZetReport mZetReprot;


    public void send(final android.app.Application application, final Settings mSettings, MZReport mzReport) {

        this.mApplication = application;
        this.mSettings = mSettings;
        this.mzReport = mzReport;
        this.mZetReprot = mzReport.mZetReprot;

        SenderWorkerReport senderWorkerReport = new SenderWorkerReport();
        try {
            senderWorkerReport.execute().get();
        } catch (Exception e) {
            log.error(e);
        }

    }

    private class SenderWorkerReport extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... params) {

            Gson sd = Utils.getGson();
            String json = sd.toJson(mZetReprot);
            String res = UtilsSender.postRequest(mApplication, Utils.HTTP + mSettings.url + "/create_zreport/", json, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {
                }
            });
            return res;
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            ISession session = Configure.getSession();
            try {

                session.beginTransaction();
                if (aBoolean == null) {
                    int i = (int) session.executeScalar(" select count(*) from zreport where id = " + mzReport.id);
                    if (i != 0) {
                        session.delete(mzReport);
                    }

                } else {
                    Toast.makeText(mApplication, aBoolean, Toast.LENGTH_SHORT).show();

                    String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + mSettings.url + "/create_zreport/:   " + aBoolean;
                    log.error(msg);                    //new SenderErrorPOST().send(mApplication,msg,mSettings);
                }
                session.commitTransaction();
            } finally {
                session.endTransaction();
            }

        }
    }
}
