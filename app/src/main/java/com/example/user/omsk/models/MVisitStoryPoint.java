package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.Where;

import java.io.Serializable;

// истоия визитов по точке ( по запросу)
@Table("story_visit_point")
@Where("ping = " + PINGATOR.story_visit_point)
public class MVisitStoryPoint extends MVisitStoryBase implements Serializable, IPing {//MStoryVisitPointBase
    @Column("ping")
    public int ping = PINGATOR.story_visit_point;
}

