package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.InnerDeterminerGPS;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderGoogleAddressToGeoGET;

public class DialogDetermineGeoAddress extends DialogFragment {

    private InnerDeterminerGPS determiner;
    private String mAddress = "";
    private String mLatitude = "0";
    private String mLongitude = "0";
    private TextView mTextView;
    private Activity mActivity;
    private SenderGoogleAddressToGeoGET.IActionAddressGeo mIAction;
    private LinearLayout mPanel_button;
    private TextView mTextViewLatitude;
    private TextView mTextViewLongitude;

   public   DialogDetermineGeoAddress activate(Activity activity, SenderGoogleAddressToGeoGET.IActionAddressGeo iAction) {
        this.mIAction = iAction;
        this.mActivity = activity;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        vi = LayoutInflater.from(getActivity());
        View v = vi.inflate(R.layout.dialog_determine_geo_address, null);
        ((TextView) v.findViewById(R.id.title_dalog)).setText(getString(R.string.kashdsjdhsd));
        builder.setView(v);
        mTextView = (TextView) v.findViewById(R.id.geo_address);
        mPanel_button = (LinearLayout) v.findViewById(R.id.panel_button);
        v.findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        v.findViewById(R.id.bt_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIAction != null) {
                    mIAction.action(mAddress, mLatitude, mLongitude);
                    dismiss();
                }
            }
        });
        mTextViewLatitude = (TextView) v.findViewById(R.id.geo_latitude);
        mTextViewLongitude = (TextView) v.findViewById(R.id.geo_longitude);

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        determiner = new InnerDeterminerGPS(mActivity, new SenderGoogleAddressToGeoGET.IActionAddressGeo() {
            @Override
            public void action(String address, String latitude, String longitude) {
                if (address == null || address.trim().length() == 0) {
                    mTextView.setText(R.string.errrrrer);
                } else {
                    mTextView.setText(address);
                }

                mTextViewLatitude.setText(String.valueOf(latitude));
                mTextViewLongitude.setText(String.valueOf(longitude));
                tempo(address, latitude, longitude);
                mPanel_button.setVisibility(View.VISIBLE);
            }
        });
    }

    private void tempo(String address, String latitude, String longitude) {
        this.mAddress = address;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    @Override
    public void onDestroy() {

        if (determiner != null) {
            determiner.destroy();
        }
        super.onDestroy();
    }
}
