package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.FactoryColor;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.PrimaryKey;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MProductBase {

    //  индентификатор правила, для отдачи продукта и для типы акции
    public String rule_id;

    // максимальное количество продукта для выдачи по акции
    public double amount_action_max;

    // только для отдачи презентов, и больше нигде
    // public String present_type_id;
    public transient boolean isSelect;

    public transient boolean isEdit;

    @PrimaryKey("id")
    public transient int id;

    @SerializedName("category_id")
    @Column("product_group")
    public String productGroup;

    @SerializedName("nameKKT")
    @Column("nameKKT")
    public String nameKKT;

    @SerializedName("id")
    @Column("idu")
    public String idu;

    @SerializedName("archived")
    @Column("archive")
    public boolean isArchive;///////////////////todo  добавление что товар в архиве

    @SerializedName("group_id")
    @Column("group_id")
    public String group_id;

    @SerializedName("name")
    @Column("name")
    public String name;

    private double amount_core;

    public double getAmount_core() {
        return amount_core;
    }

    public void setAmountCore(double v) {
        amount_core = v;
    }

    @SerializedName("cleared_name")
    @Column("cleared_name")
    public String sku;//sku


    //  количество продуктов в новом складе
    @SerializedName("amount2")
    @Column("amount_safe2")
    public double amountSelf2;

    //  количество продуктов в багажнике
    @SerializedName("amount")
    @Column("amount_safe")
    public double amountSelf;

    public double amountSelfCore() {

        double sales = 0;


        if (Utils.getSalesProducts().containsKey(this.idu)) {
            sales = Utils.getSalesProducts().get(this.idu);
        }
        return amountSelf - sales;

    }

    // используется только для засылки в js функцию акции
    @SerializedName("max_price")
    @Column("max_price")
    public double max_price;

    @SerializedName("unit")
    @Column("unit")
    public String unit;

    public double price;

    int color;

    private transient int padding;

    public double getPrice(String point_price_type) {
        List<MPrice> priceList = Configure.getSession().getList(MPrice.class, " product_id = ? ", this.idu);
        double res = 0;
        for (MPrice mPrice : priceList) {
            if (mPrice.price_type_id.equals(point_price_type)) {
                res = mPrice.value;
            }
        }
        return res;
    }

    public String get_id() {
        return idu;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public int getPadding() {
        return padding;
    }

    public void setPadding(int value) {
        padding = value;
    }

    public String getPrice() {
        return Utils.getStringDecimal(price);
    }

    public String getAmount() {
        if (amount_core == 0d) {
            return "";
        }
        return Utils.getStringDecimal(amount_core);
    }

    public void setAmount(String amount) {
        amount_core = Double.parseDouble(amount.replace(",", "."));
    }

    public String getItogo() {
        if (getTotalPrice() == 0) {
            return "";
        }
        return Utils.getStringDecimal(getTotalPrice());
    }

    public int getColor() {
        if (color == 0) {
            color = FactoryColor.getColor(idu);
        }
        return color;
    }

    public double getTotalPrice() {
        return Utils.round(amount_core * price, 2);
    }

    public MProduct cloneForKassa() {
        MProduct product = new MProduct();
        product.setAmountCore(this.getAmount_core());
        product.price = this.price;
        product.idu = this.idu;
        product.name = this.name;
        product.nameKKT = this.nameKKT;
        product.id = this.id;
        product.unit = this.unit;
        return product;
    }
}
