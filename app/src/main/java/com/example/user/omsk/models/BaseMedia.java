package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Ttesting;
import com.example.user.omsk.auto.MAuto;
import com.example.user.omsk.fotocontrol.MPointImage;
import com.example.user.omsk.kassa.MChecks;
import com.example.user.omsk.kassa.MZReport;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.plan.MPlanPair;
import com.example.user.omsk.plan.MPlanPairOld;
import com.example.user.omsk.plan.MPlanPointProducts;
import com.example.user.omsk.plan.MPlanPointProductsOld;
import com.example.user.omsk.plan.ModelMML;
import com.example.user.omsk.plan.ModelMMLOld;
import com.example.user.omsk.plan.PlanE;
import com.example.user.omsk.plan.PlanEOld;
import com.example.user.omsk.plan.PlanMML;
import com.example.user.omsk.plan.PlanMMLOld;
import com.example.user.omsk.plan.TempProduct;

import java.util.ArrayList;
import java.util.List;

public class BaseMedia {

    @PrimaryKey("id")
    public int id;


    @Column("visit_id")
    public String visit_id;


    @Column("point_id")
    public String point_id;

    @Column("path")
    public String path;
}

