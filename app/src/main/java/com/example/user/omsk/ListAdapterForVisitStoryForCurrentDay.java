package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListAdapterForVisitStoryForCurrentDay extends ArrayAdapter<MVisitStory> implements IPainter {

    Map<Integer, View> map = new HashMap<>();
    Map<Integer, View> mM = new HashMap<>();
    private List<MProduct> mProductList;
    private final Context mContext;
    private final int mResource;
    private final List<MVisitStory> mVisitStoryList;
    private final Activity activity;
    private List<MOrderType> mOrderTypes;
    private List<MVisitResults> mVisitResultses;
    private Settings mSettings;

    public ListAdapterForVisitStoryForCurrentDay(Context context, int resource, List<MVisitStory> objects, Activity activity) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.mVisitStoryList = objects;
        this.activity = activity;
        this.mProductList = Configure.getSession().getList(MProduct.class, null);
        mOrderTypes = Configure.getSession().getList(MOrderType.class, null);
        mVisitResultses = Configure.getSession().getList(MVisitResults.class, null);
        mSettings = Settings.core();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;
        MVisitStory p = getItem(position);

        if (p != null) {


            if (mM.containsKey(p.id)) {
                mView = mM.get(p.id);
            } else {
                if (mSettings.showOldViewForVisit == false) {
                    mView = UtilsViewBuilderNew.getItemStoryVisit(p, getContext(), mProductList, false, true, activity);//mOrderTypes,mVisitResultses,
                } else {
                    mView = UtilsViewBuilder.getItemStoryVisit(mOrderTypes, mVisitResultses, p, getContext(), mProductList, false, true, activity, false);//
                }

                if (mSettings.getVisit() != null && mSettings.getVisit().point != null && mSettings.getVisit().point.idu.equals(p.get_id())) {
                    mView.setBackgroundResource(R.color.colore97);
                }


                mM.put(p.id, mView);
            }

            if (mSettings.getVisit() != null && mSettings.getVisit().point != null && mSettings.getVisit().point.idu.equals(p.point_id)) {
                mView.setBackgroundResource(R.color.colore97);
            }

            if (map.containsKey(position) == false) {
                map.put(position, mView);
            }

        } else {
            if (mM.containsKey(-1)) {
                mView = mM.get(-1);
            } else {
                mView = UtilsViewBuilder.getItemTotal(getContext(), new ArrayList<MVisitStoryBase>(mVisitStoryList), Itogoficator.currentDay, false);
                mM.put(-1, mView);
            }
        }
        return mView;
    }

    @Override
    public void notifyAddin(int position) {
        for (View view : map.values()) {
            view.setBackgroundResource(R.color.colore3);
        }
        if (map.containsKey(position)) {
            map.get(position).setBackgroundResource(R.color.colore97);
        }

    }
}
