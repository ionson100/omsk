package com.example.user.omsk.reloadgps;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public interface IActionReloadGps {
    void action(double dis, double latitude, double longitude);
}
