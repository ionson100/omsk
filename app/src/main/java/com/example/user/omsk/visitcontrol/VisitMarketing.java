package com.example.user.omsk.visitcontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.omsk.FPresent;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.orientationimage.RotateImage;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StartIntentPlan;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;
import com.example.user.omsk.action.DialogAddDiscriptionConcurentActions;
import com.example.user.omsk.orm2.Configure;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.example.user.omsk.FVisit.ACTION_TAKE_VIDEO;
import static com.example.user.omsk.FVisit.TAKE_PHOTO_CODE;

/**
 * TODO: document your custom view class.
 */
public class VisitMarketing extends LinearLayout {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(VisitMarketing.class);
    public MainActivity mainActivity;
    private View panelActionProduct;
    private View noteAction;

    private String mOrder_debt;
    private Settings mSettings;

    private LinearLayout mPanelPhoto;


    public static String getPhotoDirectory() {

        String patch = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/tempvisit";
        File file = new File(patch);
        if (file.exists() == false) {
            file.mkdir();
        }

        return patch;

    }


    public VisitMarketing(Context context) {
        super(context);
        try {
            init();
        } catch (Exception ex) {


            log.error(ex);
        }

    }

    public VisitMarketing(Context context, AttributeSet attrs) {
        super(context, attrs);
        try {
            init();
        } catch (Exception ex) {

            log.error(ex);
        }

    }

    public VisitMarketing(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        try {
            init();
        } catch (Exception ex) {

            log.error(ex);
        }

    }

    private void init() {


        List<MOrderType> types = Configure.getSession().getList(MOrderType.class, " index1 = 1 ");
        if (types.size() == 1) {
            mOrder_debt = types.get(0).idu;
        }
        mSettings = Settings.core();
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = vi.inflate(R.layout.visit_control_marketig, null);
        /////////////////////////

        mPanelPhoto = (LinearLayout) mView.findViewById(R.id.panelPhoto);

        mView.findViewById(R.id.bt_action).setVisibility(View.VISIBLE);
        panelActionProduct = mView.findViewById(R.id.panel_action_product);
        noteAction = mView.findViewById(R.id.note_action);


        mView.findViewById(R.id.bt_present_core).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), FPresent.class);

                getContext().startActivity(intent);

            }
        });


        mView.findViewById(R.id.bt_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mSettings.getVisit().order_type_id == null) {
                    Toast.makeText(getContext(), "Отсутствие продаж!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mSettings.getVisit().order_type_id.equals(mOrder_debt) && mSettings.getVisit().debtList.size() > 0) {
                    Settings.showFragment(StateSystem.VISIT_ACTION, mainActivity);
                    Utils.messageBox(getContext().getString(R.string.warning), getContext().getString(R.string.sdsdsd), mainActivity, null);
                } else if (mSettings.getVisit().order_type_id.equals(mOrder_debt) && mSettings.getVisit().debtList.size() == 0) {
                    Utils.messageBox(getContext().getString(R.string.warning), getContext().getString(R.string.ddddsadads), mainActivity, null);
                } else {
                    Settings.showFragment(StateSystem.VISIT_ACTION, mainActivity);
                }
            }
        });

        Button t2 = (Button) mView.findViewById(R.id.dounload_plan);
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartIntentPlan.start(mainActivity, mSettings);
            }
        });
        SpannableString content = new SpannableString(getContext().getString(R.string.qweqwe));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        t2.setText(content);

        EditText editText = (EditText) mView.findViewById(R.id.member_name);
        editText.setText(mSettings.getVisit().member_action);

        if (mSettings.getVisit().selectProductForActions.size() > 0 || mSettings.getVisit().selectProductForPresent.size() > 0) {
            editText.setVisibility(View.VISIBLE);
        } else {
            editText.setVisibility(View.GONE);
        }
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().member_action = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mView.findViewById(R.id.bt_action_type_show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.VISIT_ACYIONS_TYPES_SHOW, mainActivity);
            }
        });

        mView.findViewById(R.id.bt_present).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.VISIT_PRESENT, mainActivity);
            }
        });

        mView.findViewById(R.id.bt_presentType_show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.VISIT_PRESENT_TYPES_SHOW, mainActivity);
            }
        });

        mView.findViewById(R.id.bt_action_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photo();
            }
        });
        mView.findViewById(R.id.bt_action_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                video();
            }
        });

        mView.findViewById(R.id.bt_action_concurents).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogAddDiscriptionConcurentActions().show(mainActivity.getSupportFragmentManager(), "we");
            }
        });


        showPhoto();


        this.addView(mView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
    }

    private void showmenPanelAction() {
        List<MTemplatePropety> mTemplatePropetyList = Configure.getSession().getList(MTemplatePropety.class, null);
        MTemplatePropety mTemplatePropety = new MTemplatePropety();
        if (mTemplatePropetyList.size() > 0) {
            mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
        }

        if (mTemplatePropety.isSendPlan == false) {
            panelActionProduct.setVisibility(View.GONE);
            noteAction.setVisibility(View.VISIBLE);
        } else {
            panelActionProduct.setVisibility(View.VISIBLE);
            noteAction.setVisibility(View.GONE);
        }
    }

    public void showPhoto() {


        showmenPanelAction();


        mPanelPhoto.removeAllViews();
        for (String s : mSettings.getVisit().listPhotoPath) {
            File file = new File(s);
            if (!file.exists()) continue;


            final ImageView view = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.image_view_present, null);
            // this.registerForContextMenu(view);
            Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
            Bitmap res = RotateImage.rotationImage(bitmap, file.getAbsolutePath());
            if (res == null) {
                res = bitmap;
            }
            view.setImageBitmap(res);
            view.setTag(s);

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str = (String) view.getTag();
                    File file = new File(str);
                    if (file.exists() == false) {

                        Toast.makeText(getContext(), "Файла не существует!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        String as = "file://" + "/" + str;
                        intent.setDataAndType(Uri.parse(as), "image/*");
                        getContext().startActivity(intent);
                    }

                }
            });


            view.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {

                                                    @Override
                                                    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                                                        final String str = (String) view.getTag();
                                                        menu.add(R.string.show).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                                            @Override
                                                            public boolean onMenuItemClick(MenuItem item) {
                                                                Intent intent = new Intent();
                                                                intent.setAction(Intent.ACTION_VIEW);
                                                                String as = "file://" + "/" + str;
                                                                intent.setDataAndType(Uri.parse(as), "image/*");
                                                                getContext().startActivity(intent);
                                                                return true;
                                                            }
                                                        });
                                                        menu.add(R.string.delete_photo).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                                            @Override
                                                            public boolean onMenuItemClick(MenuItem item) {
                                                                mSettings.getVisit().listVideoPath.remove(str);
                                                                mSettings.getVisit().listPhotoPath.remove(str);
                                                                showPhoto();

                                                                return true;
                                                            }
                                                        });
                                                    }
                                                }
            );


            mPanelPhoto.addView(view);
        }
        for (String s : mSettings.getVisit().listVideoPath) {
            File file = new File(s);
            if (!file.exists() || file.length() < 10) continue;

            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            final ImageView view = (ImageView) vi.inflate(R.layout.image_view_present, null);
            //  this.registerForContextMenu(view);
            Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
            Bitmap res = RotateImage.rotationImage(bitmap, file.getAbsolutePath());
            if (res == null) {
                res = bitmap;
            }
            view.setImageBitmap(res);
            view.setTag(s);

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str = (String) view.getTag();
                    File file = new File(str);
                    if (file.exists() == false) {

                        Toast.makeText(getContext(), "Файла не существует!", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            MediaPlayer mp = new MediaPlayer();
                            mp.setDataSource(str);
                            mp.prepare();
                            mp.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                }
            });

            view.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {

                                                    @Override
                                                    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                                                        final String str = (String) view.getTag();
                                                        menu.add(R.string.show).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                                            @Override
                                                            public boolean onMenuItemClick(MenuItem item) {

                                                                try {
                                                                    MediaPlayer mp = new MediaPlayer();
                                                                    mp.setDataSource(str);
                                                                    mp.prepare();
                                                                    mp.start();
                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }

                                                                return true;
                                                            }
                                                        });
                                                        menu.add(R.string.delete_photo).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                                            @Override
                                                            public boolean onMenuItemClick(MenuItem item) {
                                                                mSettings.getVisit().listVideoPath.remove(str);
                                                                mSettings.getVisit().listPhotoPath.remove(str);
                                                                showPhoto();
                                                                return true;
                                                            }
                                                        });
                                                    }
                                                }
            );


            long dd = file.length();
            if (file.length() > Utils.MAX_FILE_SIZE) {
                String msg = String.format("Видео файл слишком большой для отправки на сервер: %s байт.\n" +
                        "Максимальный размер  видео файла может быть не больше: %s байт.\n" +
                        "Удалите файл через контекстное меню и повторите попытку записи.\n" +
                        " ( %s )", String.valueOf(file.length()), String.valueOf(Utils.MAX_FILE_SIZE), file.getPath());
                Utils.messageBox(getContext().getString(R.string.warning), msg, mainActivity, null);
            }
            mPanelPhoto.addView(view);
        }
    }

    private void video() {//.mp4

        String dir = getPhotoDirectory();
        File newdir = new File(dir);
        if (!newdir.exists()) {
            newdir.mkdirs();
        }

        String file = dir + "/" + UUID.randomUUID().toString().replace("-", "") + ".mp4";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        } catch (IOException ignore) {
        }
        if (mSettings.getVisit().listVideoPath == null) {
            mSettings.getVisit().listVideoPath = new ArrayList<>();
        }
        mSettings.getVisit().listVideoPath.add(file);
        Settings.save();
        Uri outputFileUri = Uri.fromFile(newfile);
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 60 * 5);
        takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        mainActivity.startActivityForResult(takeVideoIntent, ACTION_TAKE_VIDEO);
    }

    private void photo() {

        String dir = getPhotoDirectory();
        File newdir = new File(dir);
        if (!newdir.exists()) {
            newdir.mkdirs();
        }
        String file = dir + "/" + UUID.randomUUID().toString().replace("-", "") + ".jpg";
        File newfile = new File(file);
        try {
            newfile.createNewFile();
        } catch (IOException ignore) {
        }
        mSettings.getVisit().listPhotoPath.add(file);
        Settings.save();
        Uri outputFileUri = Uri.fromFile(newfile);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra("photo", true);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        mainActivity.startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
    }
}

