package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.R;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MTask;
import com.example.user.omsk.models.MTaskPoint;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.List;

public class DialogTask extends DialogFragment {

    public final static int CREATE = 1;
    public final static int DELETE = 2;

    public interface IActionTaskDeleteCreate {
        void Action(MPoint mPoint, int action);
    }

    private MPoint mPoint;
    private IActionTaskDeleteCreate iActionTasktDeleteCreate;

    public DialogTask setPoint(MPoint mPoint) {

        this.mPoint = mPoint;
        return this;
    }

    public DialogTask setAction(IActionTaskDeleteCreate iActionTasktDeleteCreate) {

        this.iActionTasktDeleteCreate = iActionTasktDeleteCreate;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final List<MTaskPoint> taskList = mPoint.getTask();


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        vi = LayoutInflater.from(getActivity());
        View v = vi.inflate(R.layout.dialog_task_point, null);
        builder.setView(v);
        LinearLayout panel = (LinearLayout) v.findViewById(R.id.panel_task_base);
        List<MTask> mTasks = Configure.getSession().getList(MTask.class, null);
        for (final MTask mTask : mTasks) {
            View vs = LayoutInflater.from(getActivity()).inflate(R.layout.item_dialog_task_point, null);
            ((TextView) vs.findViewById(R.id.task_name)).setText(mTask.message);
            CheckBox checkBox = (CheckBox) vs.findViewById(R.id.task_checked);
            MTaskPoint taskPoint = Linq.toStream(taskList).firstOrDefault(new Predicate<MTaskPoint>() {
                @Override
                public boolean apply(MTaskPoint t) {
                    return t.task_id.equals(mTask.idu);
                }
            });
            if (taskPoint != null) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        MTaskPoint p = new MTaskPoint();
                        p.task_id = mTask.idu;
                        p.point_id = mPoint.idu;
                        taskList.add(p);
                    } else {
                        MTaskPoint tp = Linq.toStream(taskList).firstOrDefault(new Predicate<MTaskPoint>() {
                            @Override
                            public boolean apply(MTaskPoint t) {
                                return t.task_id.equals(mTask.idu);
                            }
                        });
                        if (tp != null) {
                            taskList.remove(tp);
                        }
                    }
                }
            });
            panel.addView(vs);
        }
        v.findViewById(R.id.bt_task_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        v.findViewById(R.id.bt_task_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (taskList.size() == 0) {
                    Toast.makeText(getActivity(), "Задачи не выбраны", Toast.LENGTH_SHORT).show();
                } else {
                    ISession ses = Configure.getSession();

                    ses.execSQL(" delete from task_point where point_id = '" + mPoint.idu + "' and  ");

                    for (MTaskPoint taskPoint : taskList) {
                        ses.insert(taskPoint);
                    }
                    iActionTasktDeleteCreate.Action(mPoint, DialogTask.CREATE);
                }
                dismiss();
            }
        });
        Button deleteBt = (Button) v.findViewById(R.id.bt_task_delete);
        if (taskList.size() > 0) {
            deleteBt.setVisibility(View.VISIBLE);
        } else {
            deleteBt.setVisibility(View.GONE);
        }
        deleteBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ISession ses = Configure.getSession();
                ses.execSQL(" delete from task_point where point_id = '" + mPoint.idu + "' ");
                iActionTasktDeleteCreate.Action(mPoint, DialogTask.DELETE);
                dismiss();
            }
        });
        return builder.create();
    }
}
