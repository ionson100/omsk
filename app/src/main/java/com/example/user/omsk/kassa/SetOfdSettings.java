package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.atol.drivers.fptr.Fptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

public class SetOfdSettings {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SetOfdSettings.class);
    public static void set(final Activity activity, final SettingsKassa w) {
        new AsyncTask<Void, String, Void>() {
            private String mErrorText;
            private ProgressDialog mDialog;
            private Fptr mFptr;

            private void checkError() throws Exception {
                int rc = mFptr.get_ResultCode();
                if (rc < 0) {
                    String rd = mFptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = mFptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    mFptr = new Fptr();
                    mFptr.create(activity);

                    if (mFptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) { // загрузка настроек
                        checkError();
                    }
                    if (mFptr.put_DeviceEnabled(true) < 0) {// устанока соединения
                        checkError();
                    }
                    if (mFptr.GetStatus() < 0) {
                        checkError();
                    }
                    if (mFptr.put_UserPassword("00000030") < 0) {
                        checkError();
                    }
                    if (mFptr.put_Mode(4) < 0) {
                        checkError();
                    }
                    if (mFptr.SetMode() < 0) {
                        checkError();
                    }
                } catch (Exception ex) {
                    log.error(ex);
                    mErrorText = ex.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), Settings.core().kkmNumber + "\n" +
                            "Ошибка ккм file  - SetOfdSettings\n" + ex.getMessage());
                } finally {
                    mFptr.ResetMode();
                    mFptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                mDialog = Utils.factoryDialog(activity, "Получение настроек wi-fi", null);
                mDialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (mDialog != null) {
                    mDialog.cancel();
                    mDialog = null;
                }
                if (mErrorText != null) {
                    Utils.messageBox(activity.getString(R.string.error), mErrorText, activity, null);
                } else {
                    Utils.messageBox(activity.getString(R.string.ok), activity.getString(R.string.sdfdfdf), activity, null);
                }
            }
        }.execute();
    }
}
