package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

//import com.example.user.omsk.models.MError;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVisit;

import org.apache.log4j.Logger;

public class FMapStory extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FMapStory.class);

    MapVisit mapVisit;

    public FMapStory() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_fmap_story, container, false);

        try {
            SettingsGpsMap map = SettingsGpsMap.core();

            MVisit mVisit = new MVisit();
            mVisit.latitude = map.fix_latitude;
            mVisit.longitude = map.fix_longitude;
            mVisit.point = new MPoint();
            mVisit.point.latitude = map.point_latitude;
            mVisit.point.longitude = map.point_longitude;
            mVisit.point.name = map.point_name;

            mapVisit = new MapVisit(mView, getActivity(), mVisit, true);

        } catch (Exception ex) {
            Utils.sendMessage("FMapStory:" + Utils.getErrorTrace(ex), (MyApplication) getActivity().getApplication());
          log.error(ex);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            Settings.showFragment(StateSystem.HOME, getActivity());
        }
        return mView;

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mapVisit != null) {
                mapVisit.destory();
            }
        } catch (Exception ignored) {
            log.error(ignored);
        }

    }


}
