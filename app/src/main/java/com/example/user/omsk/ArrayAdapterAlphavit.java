package com.example.user.omsk;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.log4j.Logger;

import java.util.List;

/**
 * ion100 23.11.2017.
 */

public class ArrayAdapterAlphavit extends ArrayAdapter<String> {
    public static org.apache.log4j.Logger log = Logger.getLogger(ArrayAdapterAlphavit.class);
    @NonNull
    private final Context context;

    @NonNull
    private final List<String> objects;

    public ArrayAdapterAlphavit(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;
        mView = LayoutInflater.from(getContext()).inflate(R.layout.alphavit_top, null);
        final String p = getItem(position);
        LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.wrap_abc);
        if (p.toUpperCase().equals("А")) {

            linearLayout.setBackgroundResource(R.drawable.button_alphavit_top);
        } else if (p.toUpperCase().equals("Я")) {

            linearLayout.setBackgroundResource(R.drawable.button_alphavit_bot);
        } else {
            linearLayout.setBackgroundResource(R.drawable.button_alphavit_normal);
        }

        TextView textView = (TextView) mView.findViewById(R.id.char_single);
        textView.setText(p);
        return mView;
    }
}
