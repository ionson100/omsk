package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.senders.IActionResponse;
import com.example.user.omsk.senders.TempCheckSender;
import com.example.user.omsk.senders.TempProductSender;
import com.example.user.omsk.senders.UtilsSender;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

public class SenderConstructor {
    public static org.apache.log4j.Logger log = Logger.getLogger(SenderConstructor.class);
    private android.app.Application mApplication;
    private Settings mSettings;
    private IActionE mIActionE;
    private MChecksData mChecksData;

    public void send(android.app.Application application, Settings mSettings, MChecks checksData, IActionE iActionE) {

        this.mIActionE = iActionE;
        this.mApplication = application;
        this.mSettings = mSettings;

        this.mChecksData = checksData.checkData;
        try {
            new SenderWorkerReport().execute().get();
        } catch (Exception e) {
            log.error(e);
        }
    }

    private class SenderWorkerReport extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... params) {
            TempCheckSender tempCheck = new TempCheckSender();
            tempCheck.uuid = SettingsUser.core().getUuidDevice();
            tempCheck.uuid_check_delete = mChecksData.checkUUIDDelete;
            tempCheck.idu = mChecksData.idu;
            tempCheck.is_returned = mChecksData.isReturn;
            tempCheck.number_check_delete = mChecksData.checkNumberDelete;
            tempCheck.type_check = mChecksData.type_check;
            tempCheck.date = mChecksData.date;
            tempCheck.number_check = mChecksData.checkNumber;
            tempCheck.number_doc = mChecksData.docNumber;
            tempCheck.order_id = mChecksData.order_debt_id;
            tempCheck.number_kkm = mChecksData.kmmNumber;
            tempCheck.price_check = mChecksData.totalPrice;
            tempCheck.amount_check = mChecksData.totalAmount;
            for (MProductBase product : mChecksData.productList) {
                TempProductSender tempProduct = new TempProductSender();
                tempProduct.sku = product.sku;
                tempProduct.price = product.price;
                tempProduct.amount = product.getAmount_core();
                tempProduct.product_id = product.idu;
                tempCheck.productList.add(tempProduct);
            }

            Gson sd = Utils.getGson();
            String json = sd.toJson(tempCheck);
            String res = UtilsSender.postRequest(mApplication, Utils.HTTP + mSettings.url + "/create_custom_check/", json, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {
                }
            });

            return res;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result == null || result.trim().length() == 0) {
                mIActionE.action(result);

                try {
                    MainActivity.mainActivityInstance.sendBroadcast(new Intent(MainActivity.FAB));
                } catch (Exception ignored) {

                }

            } else {
                Toast.makeText(mApplication, result, Toast.LENGTH_LONG).show();

                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + mSettings.url + "/create_custom_check/:   " + result;
                log.error(msg);                //new SenderErrorPOST().send(mApplication,msg,mSettings);
            }

        }
    }
}
