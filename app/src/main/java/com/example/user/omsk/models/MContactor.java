package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

import java.io.Serializable;

/**
 * Промежуточная таблица между точкой и контактами
 */
@Table("contactor")
public class MContactor implements Serializable {

    @PrimaryKey("id")
    public transient int id;

    @Column("point_id")
    public transient String point_id;

    @Column("contact_id")
    public transient String contact_id;

    public MContactor() {
    }

    public MContactor(String point_id, String contact_id) {
        this.contact_id = contact_id;
        this.point_id = point_id;
    }
}
