package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import java.io.Serializable;

// для отправки на сервер
public class TempComonProduct implements Serializable {

    public double price;

    public double amount;

    public String id;

    public String promo;
}
