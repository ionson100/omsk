package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DialogShowPointDebt extends DialogFragment {

    private List<MDebt> mDebts;
    private Map<String, MProduct> mProductMap;

    public DialogShowPointDebt() {
        List<MProduct> list = Configure.getSession().getList(MProduct.class, null);
        mProductMap = new HashMap<>(list.size());
        for (MProduct product : list) {
            mProductMap.put(product.idu, product);
        }
        //List<MiniVisit> mMiniVisits = new ArrayList<>();
    }

    public void setDebtsPoint(List<MDebt> mDebts, MPoint mPoint) {
        this.mDebts = mDebts;
        //MPoint mPoint1 = mPoint;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Map<Integer, MiniVisit> visitMap = new HashMap<>();
        for (MDebt mDebt : mDebts) {
            MProduct product = mProductMap.get(mDebt.product_id);
            if (product == null) {
                product = new MProduct();
                product.name = "Не найден";
            }
            product.setAmountCore(mDebt.amount);
            product.price = mDebt.price;
            if (visitMap.containsKey(mDebt.date)) {
                visitMap.get(mDebt.date).productList.add(product);
            } else {
                MiniVisit miniVisit = new MiniVisit();
                miniVisit.productList.add(product);
                miniVisit.date = mDebt.date;
                miniVisit.staus = mDebt.status;
                visitMap.put(mDebt.date, miniVisit);
            }
        }
        List<MiniVisit> miniVisits = new ArrayList<>(visitMap.values());
        Collections.sort(miniVisits, new Comparator<MiniVisit>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MiniVisit lhs, MiniVisit rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });
        miniVisits.add(null);// итоговый итем
        ListAdapterDebtE adapterDebtE = new ListAdapterDebtE(getContext(), R.layout.item_mini_visit, miniVisits);
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_point_debt, null);
        ((TextView) mView.findViewById(R.id.title_dalog)).setText(getString(R.string.saaadasd));
        ListView listView = (ListView) mView.findViewById(R.id.list_debt);
        mView.findViewById(R.id.show_dbt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        listView.setAdapter(adapterDebtE);
        builder.setView(mView);
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setLayout(600, 400);
        return alertDialog;
    }
}

class MiniVisit {
    public int date ;
    public List<MProduct> productList = new ArrayList<>();
    public boolean staus;
}

class ListAdapterDebtE extends ArrayAdapter<MiniVisit> {

    @NonNull
    private final Context context;
    private final int resource;
    @NonNull
    private final List<MiniVisit> objects;

    public ListAdapterDebtE(@NonNull Context context, @LayoutRes int resource, @NonNull List<MiniVisit> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;

        final MiniVisit p = getItem(position);
        if (p != null) {

            mView = LayoutInflater.from(getContext()).inflate(resource, null);
            LinearLayout panelBase = (LinearLayout) mView.findViewById(R.id.panel_all_saleProdict);
            Pupper date = (Pupper) mView.findViewById(R.id.debt_date);
            Pupper itogo = (Pupper) mView.findViewById(R.id.assa23);
            Pupper status = (Pupper) mView.findViewById(R.id.debt_status);
            double total = 0;
            if (p.productList != null && p.productList.size() > 0) {
                panelBase.setVisibility(View.VISIBLE);
                mView.findViewById(R.id.panel_all_saleProdict_label).setVisibility(View.VISIBLE);
                for (MProduct product : p.productList) {
                    View viewSale = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                    TextView name = (TextView) viewSale.findViewById(R.id.name_s);
                    TextView amountE = (TextView) viewSale.findViewById(R.id.amount_s);
                    TextView priceE = (TextView) viewSale.findViewById(R.id.price_s);
                    TextView itog = (TextView) viewSale.findViewById(R.id.itogo_s);
                    name.setText(product.name);
                    amountE.setText(Utils.getStringDecimal(product.getAmount_core()));
                    priceE.setText(Utils.getStringDecimal(product.price));
                    itog.setText(Utils.getStringDecimal(product.getAmount_core() * product.price));
                    panelBase.addView(viewSale);
                    total = total + product.getAmount_core() * product.price;
                }
            }

            Date d=Utils.intToDate(p.date);
            date.setPairString("Дата посещения:", Utils.simpleDateFormatE(d));
            itogo.setPairString("Итого на сумму (руб.):", Utils.getStringDecimal(total));
            if (p.staus) {
                status.setPairString("Оплачен", "");
            } else {
                status.setPairString("Не оплачен", "");
            }
        } else {
            mView = LayoutInflater.from(getContext()).inflate(R.layout.item_debet_itogo, null);
            TextView textView = (TextView) mView.findViewById(R.id.itogo_debet);
            double itogo = 0;

            for (MiniVisit object : objects) {
                if (object == null) continue;
                if (object.staus == true) continue;
                if(object.productList!=null){
                    for (MProduct product : object.productList) {
                        itogo = itogo + product.getAmount_core() * product.price;
                    }
                }

            }
            textView.setText(getContext().getString(R.string.itogo) + Utils.getStringDecimal(itogo) + getContext().getString(R.string.rub));
        }
        return mView;
    }
}

