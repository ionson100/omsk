package com.example.user.omsk.myTextView;

import android.content.Context;
import android.util.AttributeSet;

import com.example.user.omsk.MyApplication;

/**
 * ion100 on 04.12.2017.
 */

public class MyEditText extends android.support.v7.widget.AppCompatEditText {
    public MyEditText(Context context) {
        super(context);
        init();
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.setTypeface(MyApplication.getTypeface(getContext()));
    }
}
