package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.plan.Plan;
import com.example.user.omsk.plan.SenderPlanPointsProduct;

import java.util.List;

public class StartIntentPlan {
    public static void start(final Activity activity, Settings mSettings) {
        if (ValidateDay.validate(mSettings, activity)) {
            List<MTemplatePropety> mTemplatePropetyList = Configure.getSession().getList(MTemplatePropety.class, null);
            if (mTemplatePropetyList.size() == 0) {
                return;
            }
            MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
            if (mTemplatePropety.isSendPlan == false) {
                new SenderPlanPointsProduct().send(mSettings, activity, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        innreStart(activity);
                    }
                });
            } else {
                innreStart(activity);
            }
        }
    }

    private static void innreStart(Activity activity) {
        Intent intent = new Intent(activity, Plan.class);
        activity.startActivityForResult(intent, 10001);
    }


}
