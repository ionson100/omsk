package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitStoryPoint;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;

//import com.example.user.omsk.models.MError;

public class SenderPointStoryGET {

    public static org.apache.log4j.Logger log = Logger.getLogger(SenderPointStoryGET.class);
    private Settings mSettings;
    private Activity activity;
    private IActionE iAction;

    public void send( Activity activity, IActionE iAction) {
        this.mSettings = Settings.core();
        this.activity = activity;
        this.iAction = iAction;
        new SenderWorkerTack().execute();
    }

    private void App(String res) {

        Gson sd3 = Utils.getGson();
        Type listType = new TypeToken<ArrayList<TempVisitStory>>() {
        }.getType();
        ArrayList<TempVisitStory> df = sd3.fromJson(res, listType);
        ISession ses = Configure.getSession();
       // List<MVisitStoryPoint> visitPointList = new ArrayList<>();
        for (TempVisitStory t : df) {

            MVisitStoryPoint p = TempVisitStory.getMStoryVisitPoint(t, ses);
            p.point_id = mSettings.getVisit().point.get_id();
            ses.insert(p);
            if(t.date!=0){
                ses.execSQL(" delete from visit_present  where visit_finish = ?",t.date);
                for (MVisitPresent gift : t.gifts) {

                    gift.visit_finish=t.date;
                    ses.insert(gift);
                }
            }

        }
       // List<MVisitStoryPoint> sd = Configure.getSession().getList(MVisitStoryPoint.class, " point_id = ? ", mSettings.getVisit().point.get_id());
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(activity, mSettings)) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, "Обмен данными", null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (dialog != null) {
                dialog.cancel();
            }
            if (aBoolean == null) {

                iAction.action(null);
            } else {
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/visits_history_client/:  " + aBoolean;
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                log.error(msg);
               // Configure.getSession().insert(new MError(msg));
                // new SenderErrorPOST().send(activity, msg, Settings.core());
            }

        }


        @Override
        protected String doInBackground(Void... params) {

            if (isCancelled()) return null;

            final String[] result = {null};
            String str = "";
            try {
                str = Utils.HTTP + mSettings.url + "/visits_history_client/?guid=" +
                        URLEncoder.encode(SettingsUser.core().getUuidDevice(), "UTF-8") + "&point_id=" + mSettings.getVisit().point.get_id();
            } catch (UnsupportedEncodingException e) {
                return e.getMessage();
            }

            result[0] = UtilsSender.getRequest((MyApplication) activity.getApplication(), str, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {
                    if (str.toLowerCase().equals("ok")) {
                    } else {
                        App(str);
                    }
                }
            });
            return result[0];
        }
    }
}

