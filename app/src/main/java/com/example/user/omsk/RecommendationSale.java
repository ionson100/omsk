package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.widget.Toast;

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MSaleRows;
import com.example.user.omsk.models.MStockRows;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.List;

public class RecommendationSale {
    private final List<MStockRows> mStockRowsList;
    private final Settings mSettings;
    private final Activity activity;

    public RecommendationSale(Settings mSettings, Activity activity) {
        this.mStockRowsList = Configure.getSession().getList(MStockRows.class, " point_id = ? ", mSettings.getVisit().point.idu);
        this.mSettings = mSettings;
        this.activity = activity;
    }

    private void createListRecommendation(List<MProduct> saleCoreMProducts) {
        List<MSaleRows> mSaleRowses = Configure.getSession().getList(MSaleRows.class, " point_id = ? ", mSettings.getVisit().point.idu);
        if (mSaleRowses.size() == 0)
            return;
        if (mStockRowsList.size() == 0)
            return;
        if (mSaleRowses.get(0).date.getTime() == mStockRowsList.get(0).date.getTime()) {
            for (final MSaleRows sr : mSaleRowses) {
                MStockRows rows = Linq.toStream(mStockRowsList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MStockRows>() {
                    @Override
                    public boolean apply(MStockRows t) {
                        return t.product_id.equals(sr.product_id);
                    }
                });
                if (rows != null) {
                    rows.amount = rows.amount + sr.amount;
                } else {
                    MStockRows r = new MStockRows();
                    r.date = sr.date;
                    r.amount = sr.amount;
                    r.product_id = sr.product_id;
                    r.point_id = sr.point_id;
                    mStockRowsList.add(r);
                }
            }
        }

        ///////////////////////////////////////////////////////
        // вычисляем разницу дат
        int del = (int) (Utils.curDate().getTime() - mStockRowsList.get(0).date.getTime());
        // получаем  разницу в днях
        int sec = (60 * 60 * 24);
        int dey = del / sec;
        if (dey == 0) {
            Toast.makeText(activity, R.string.nothin_balance_data, Toast.LENGTH_SHORT).show();
            return; // если между визитами и дгя не было
        }


        for (final MStockRows stock : mStockRowsList) {

            MProduct product = Linq.toStream(mSettings.getVisit().selectProductForBalance).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                @Override
                public boolean apply(MProduct t) {
                    return t.idu.equals(stock.product_id);
                }
            });
            double s1 = stock.amount;
            double s2 = product == null ? 0 : product.getAmount_core();
            if (s2 >= s1) continue;// если текущий остаток больше предыдущего или равен ему
            int real = (int) ((int) (((s1 - s2) / dey) * 7) * 1.2);// основная формула это столько нужно что бы продержаться 7 дней
            if (s2 >= real) continue; // если остаток больше продержки
            int blok = (int) ((real - s2) / 10);// если нет считаем сколько добавить
            if (blok == 0) blok = 1;
            MProduct product1 = Configure.getSession().get(MProduct.class, stock.product_id);// достаем из базы текущий продукт
            if (product1 == null) continue;
            product1.setAmountCore(blok * 10);// переводим блоки в сигареты
            saleCoreMProducts.add(product1);// добавляем в аккумулятор
        }
        if (saleCoreMProducts.size() == 0) {
            Toast.makeText(activity, R.string.nothin_balance_core, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean compute() {

        List<MProduct> saleCoreMProducts = new ArrayList<>(); // промежуточный буфер
        createListRecommendation(saleCoreMProducts);
        if (saleCoreMProducts.size() == 0) {
            return false;
        }

        Utils.pricecometr(saleCoreMProducts, mSettings);// присвоение цены продукту

        for (final MProduct product : saleCoreMProducts) {

            if (product.getAmount_core() == 0 || product.isArchive) continue;
            // проверяем если уже такой продукт добавили для продажи
            MProduct p = Linq.toStream(mSettings.getVisit().selectProductForSale).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                @Override
                public boolean apply(MProduct t) {
                    return t.idu.equals(product.idu);
                }
            });
            if (p == null) {
                mSettings.getVisit().selectProductForSale.add(product); // если не добавили то рекомендуем
            } else {
                if (!p.isEdit) {
                    p.setAmountCore(product.getAmount_core());// усли добавили то меняем количество
                }
            }
        }
        Settings.save();
        return true;
    }

    public void deConpute() {
        List<MProduct> saleCoreMProducts = new ArrayList<>(); // промежуточный буфер
        createListRecommendation(saleCoreMProducts);
        for (final MProduct product : saleCoreMProducts) {
            MProduct p = Linq.toStream(mSettings.getVisit().selectProductForSale).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                @Override
                public boolean apply(MProduct t) {
                    return t.idu.equals(product.idu);
                }
            });

            if (p != null) {
                if (!p.isEdit) {
                    if (p.getAmount_core() <= product.getAmount_core()) {
                        mSettings.getVisit().selectProductForSale.remove(p);
                    } else {
                        p.setAmountCore(p.getAmount_core() - product.getAmount_core());
                    }
                }
            }
        }
    }
}
