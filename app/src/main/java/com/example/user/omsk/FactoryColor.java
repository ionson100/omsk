package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;

import com.example.user.omsk.models.MPriority;
import com.example.user.omsk.models.MPriorityProduct;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FactoryColor {
    private final static int C0 = Color.parseColor("#213a7a");
    private final static int C1 = Color.parseColor("#B03A07");
    private final static int C2 = Color.parseColor("#03691C");

    private FactoryColor() {
    }

    private static Map<MPriority, List<String>> mPriorityListMap;

    @TargetApi(Build.VERSION_CODES.M)
    public static int getColor(String product_id) {
        int color = C0;// MyApplication.appCompatActivity.getColor(R.color.black);
        if (mPriorityListMap == null) {
            mPriorityListMap = activateMap();
        }

        for (Map.Entry<MPriority, List<String>> ss : mPriorityListMap.entrySet()) {
            if (ss.getKey().value == 1 && ss.getValue().contains(product_id)) {
                return C1;
            }
            if (ss.getValue().contains(product_id)) {
                return C2;
            }
        }

        return color;
    }

    private static Map<MPriority, List<String>> activateMap() {
        Map<MPriority, List<String>> map = new HashMap<>();
        List<MPriority> priorities = Configure.getSession().getList(MPriority.class, null);
        Collections.sort(priorities, new Comparator<MPriority>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MPriority lhs, MPriority rhs) {
                return Integer.compare(lhs.value, rhs.value);
            }
        });
        for (MPriority priority : priorities) {
            List<String> strings = new ArrayList<>();
            List<MPriorityProduct> list = Configure.getSession().getList(MPriorityProduct.class, " priority_id = ? ", priority.get_id());
            for (MPriorityProduct mPriorityProduct : list) {
                strings.add(mPriorityProduct.product_id);
            }
            map.put(priority, strings);
        }
        return map;
    }

    public static void refreshData() {
        mPriorityListMap = null;
    }
}
