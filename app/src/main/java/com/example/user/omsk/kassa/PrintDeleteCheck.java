package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.Settings;


class PrintDeleteCheck {
    public static void delete(final Settings mSettings, final TextView dataTextView, final Activity activity, final AnnulCheck.TempCheck tempCheck, final IActionE iActionE) {

        UtilsKassa.messageBoxConfiпPrintCheck(activity, tempCheck.mChecksData.productList, tempCheck.mChecksData.type_check, new IActionE() {
            @Override
            public void action(Object o) {
                PrintDeleteCoreCheck.deleteReturn(mSettings, activity, dataTextView, tempCheck.mChecksData.productList, iActionE);
            }
        }, null);
    }
}
