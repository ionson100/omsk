package com.example.user.omsk.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.myTextView.MyTextView;

import java.util.concurrent.Semaphore;


public class DialogPrintDoubleCheck extends DialogFragment {


    public IActionE mIActionEOk;
    public IActionE mIActionENoOk;
    public IActionE mIActionEDismiss;
    public double sumKKT,sumReal;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_double_check, null);
        v.findViewById(R.id.print_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mIActionEOk.action(null);
            }
        });
        MyTextView textView= (MyTextView) v.findViewById(R.id.title_dalog);
        textView.setText(R.string.sdfsf);
        v.findViewById(R.id.print_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dismiss();
                mIActionENoOk.action(null);
            }
        });
        TextView textView1= (TextView) v.findViewById(R.id.alert_message);
        String ss=String.format("\nВНИМАНИЕ!!\nЕсть подозрение, что чек который Вы пытаетесь напечатать  в визите," +
                "уже существует в кассовом аппарете.\nЭто возможно произошло по причине:\n" +

                "1. Открывали маршрут более одного раза в контексте кассовой смены.\n" +
                "2. Сняли Z отчет ( маршрут не закрыт).\n" +
                "3. Произошла ошибка при распечатке предыдущего чека.\n" +
                "4. Окончание бумага при печати.\n" +
                "5. Отключение питания ККМ или планшета в момент печати\n" +
                "6. Разрыв связи планшета и ККМ в момент печати.\n" +
                "7. Закрытие программы на планшете в момент печати.\n" +
                "8. Сбой программы.\n" +
                "Что бы исключить эту возможность проверьте:\n" +
                "1 Произведите Х отчет.\n" +
                "2 Сравните наличие денег в кассе и в реале (оно должно совпадать)\n\n" +
                "Наличие денег в кассе ККМ : %s\nНаличие денег в результате визитов при текущем маршруте: %s",sumKKT,sumReal);

        String ss1=String.format("\nВНИМАНИЕ!!\nПодозрение на задваивание чеков\n" +


                "Наличие денег в кассе ККМ : %s руб.\n\n Не совпадает с\n\nНаличием денег в результате визитов при текущем маршруте: %s руб.\n\n",sumKKT,sumReal);
        textView1.setText(ss1);
        builder.setView(v);

        Dialog dialog=builder.create();

        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mIActionEDismiss.action(null);
    }
}
