package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// контакты по точкам
@Table("contact")
public class MContact implements Serializable, IUsingGuidId {

    @PrimaryKey("id")
    public transient int id;
    @SerializedName("id")
    @Column("idu")
    public String idu;
    @Column("point_id")
    public transient String point_id;
    @SerializedName("name")
    @Column("name")
    public String name;
    @SerializedName("email")
    @Column("email")
    public String email;
    @SerializedName("phone")
    @Column("phone")
    public String phone;
    @SerializedName("comment")
    @Column("comment")
    public String comment;

    public MContact() {
        this.idu = Utils.getUuid();
    }

    public String get_id() {
        return idu;
    }
}
