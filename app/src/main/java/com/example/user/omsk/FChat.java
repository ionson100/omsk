package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.example.user.omsk.chat.DiscussArrayAdapter;
import com.example.user.omsk.models.MChat;
import com.example.user.omsk.orm2.Configure;

import org.apache.log4j.Logger;

import java.util.List;

//import com.example.user.omsk.models.MError;

public class FChat extends Fragment implements IUpdateChat {

    public static org.apache.log4j.Logger log = Logger.getLogger(FChat.class);
    private EditText mEditText1;
    private DiscussArrayAdapter mAdapter;
    private ListView mListView;
    private Settings mSettings;

    public FChat() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_chat, container, false);
        mSettings = Settings.core();
        try {
            mEditText1 = (EditText) mView.findViewById(R.id.edittext);
            mListView = (ListView) mView.findViewById(R.id.chat);
            initChat();
            addItems();
        } catch (Exception e) {
            String msg = Utils.getErrorTrace(e);
            Utils.sendMessage(msg,  (MyApplication) getActivity().getApplication());
            log.error(e);
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;
    }

    private void initChat() {
        final MyApplication application = (MyApplication) getActivity().getApplication();
        mAdapter = new DiscussArrayAdapter(getActivity());
        mListView.setAdapter(mAdapter);
        mEditText1.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String str = mEditText1.getText().toString().trim();

                    if (str.length() > 0) {
                        FHome.sendMessageToSever(application, mSettings, mEditText1, mListView, mAdapter, getActivity());
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });
    }


    private void addItems() {

        List<MChat> list = Configure.getSession().getList(MChat.class, null);
        for (MChat mChat : list) {
            mAdapter.add(mChat);
        }
        int index = mAdapter.getCount();
        mListView.smoothScrollToPosition(index);

    }


    @Override
    public void updateChat() {
        int count = mAdapter.getCount();
        List<MChat> mChats = Configure.getSession().getList(MChat.class, null);
        for (int i = 0; i < mChats.size(); i++) {
            if (i < count) {
                continue;
            }
            mAdapter.add(mChats.get(i));
        }
        mListView.post(new Runnable() {
            @Override
            public void run() {
                int index = mAdapter.getCount();
                mListView.smoothScrollToPosition(index);
            }
        });

    }
}
