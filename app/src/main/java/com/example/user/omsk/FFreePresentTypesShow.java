package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.models.MFreePresentTYpes;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;

import java.util.List;


public class FFreePresentTypesShow extends Fragment {

    private View mView;
    private ListView mListView;

    public FFreePresentTypesShow() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_fvisit_present_types_show, container, false);
        mListView = (ListView) mView.findViewById(R.id.list_action);

        List<MFreePresentTYpes> mPresentTYpes = Configure.getSession().getList(MFreePresentTYpes.class, null);

        ListAdapterForPresentTypes presentTypes = new ListAdapterForPresentTypes(getActivity(), R.layout.item_list_present_type, mPresentTYpes);
        mListView.setAdapter(presentTypes);
        return mView;
    }
}

class ListAdapterForPresentTypes extends ArrayAdapter<MFreePresentTYpes> {

    private final List<MVisitStory> mVisitStories;
    private final int mResource;

    public ListAdapterForPresentTypes(Context context, int resource, List<MFreePresentTYpes> objects) {
        super(context, resource, objects);
        this.mResource = resource;
        mVisitStories = Configure.getSession().getList(MVisitStory.class, null);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;

        final MFreePresentTYpes p = getItem(position);
        if (p != null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            mView = vi.inflate(mResource, null);
            TextView name = (TextView) mView.findViewById(R.id.present_descroption_name);
            TextView desc = (TextView) mView.findViewById(R.id.present_descroption_text);
            LinearLayout panel = (LinearLayout) mView.findViewById(R.id.present_descroption_panel);
            name.setText(p.name);
            if (p.description == null || p.description.trim().length() == 0) {
                panel.setVisibility(View.GONE);
            } else {
                desc.setText(p.description);
            }
            View view = mView.findViewById(R.id.isarchive);
            if (p.isActive) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
        return mView;
    }
}

