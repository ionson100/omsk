package com.example.user.omsk;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.example.user.omsk.models.MPresent;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FPresent extends AppCompatActivity {

    private Settings mSettings=Settings.core();
    List<MPresent> mPresents=new ArrayList<>();
    ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fpresent);
        findViewById(R.id.bt_close_present).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FPresent.this.finish();
            }
        });
        mListView= (ListView) findViewById(R.id.list_table_present);
        mPresents= Configure.getSession().getList(MPresent.class,null);
        List<MPresent> mPresentList=new ArrayList<>();
        for (MPresent mPresent : mPresents) {
            long real=new Date().getTime();
            long start=mPresent.start.getTime();
            long finish=mPresent.finish.getTime();
            if(start<=real&&real<=finish){
                mPresentList.add(mPresent);
            }
        }
        mListView.setAdapter(new ListAdapterPresent(this,R.layout.item_present_visit,mPresentList,this));

    }
}
