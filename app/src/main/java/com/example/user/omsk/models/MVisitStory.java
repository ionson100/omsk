package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.Table;

import java.util.Date;

import static com.example.user.omsk.R.id.date;

// текущие визиты
@Table("visit_story")
public class MVisitStory extends MVisitStoryBase implements IObservable, IUsingGuidId, IPing {

    @Column("date_pessimise")
    public transient long date_pessimise;




    public MVisitStory() {
        idu = Utils.getUuid().replace("-", "");
    }

    @Override
    public boolean isNewObject() {

        return stateObject!=0;
    }

    @Override
    public void setNewObject(boolean value) {
        if(value){
            stateObject=1;
        }else {
            stateObject = 0;
        }

        //date =  Utils.dateToInt(new Date());
    }

    @Override
    public Date getDate() {
        return Utils.intToDate(finish);
    }

    public String get_id() {
        return idu;
    }


}
