package com.example.user.omsk;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public class RegsAddress {
    public static String getClearAddres(String address) {//\\s[А-Яа-я]*\\sобл.,\\s[А-Яа-я]*,\\s-----------------\\s[А-Яа-я\\s-]*,\\s
        String str = address.
                replaceAll("\\sРесп.\\s[А-Яа-я\\s-]*,", "").
                replaceAll("\\s[А-Яа-я\\s-]*\\sкрай,", "").
                replaceAll("\\s[А-Яа-я\\s]*\\sобл.,", "").
                replaceAll("[0-9]{6},", "").
                replace("Россия,", "").
                replace("Россия", "").
                replace("россия", "");
        return str.trim();
    }
}
