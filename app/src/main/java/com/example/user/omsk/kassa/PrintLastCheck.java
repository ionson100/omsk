package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

class PrintLastCheck {
    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(PrintLastCheck.class);
    public static void printLast(final Settings mSettings, final Activity activity, final TextView log) {

        new AsyncTask<Void, String, Void>() {
            String textAfter = log.getText().toString();
            private String errorText;
           // private MChecksData checksData = new MChecksData();
            private ProgressDialog dialog;
            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                fptr = new Fptr();
                try {
                    fptr.create(activity.getApplication());
                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                    // не являются ошибками, если мы хотим просто отменить чек
                    publishProgress("Отмена чека...");
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }
                    publishProgress("OK");
                    if (fptr.PrintLastCheckCopy() < 0) {
                        checkError();
                    }
                    publishProgress("Чек отпечатан");
                } catch (Exception e) {
                    logE.error(e);
                    publishProgress(e.getMessage());
                    errorText = e.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - PrintDoubleCheck\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                log.setText("");
                dialog = Utils.factoryDialog(activity, "Печать копии  чека.", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                if (errorText != null) {
                    Utils.messageBox(activity.getString(R.string.error), errorText, activity, null);
                } else {
                    log.setText(textAfter);
                }
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }
        }.execute();
    }
}
