package com.example.user.omsk.models;

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

import java.util.Date;



@Table("present")
public class MPresent {

    @PrimaryKey("_id")
    public int _id;

    @Column("id")
    public String id;

    @Column("start")
    public Date start;

    @Column("finish")
    public Date finish;

    @Column("name")
    public String name;

    @Column("description")
    public String description;
}
