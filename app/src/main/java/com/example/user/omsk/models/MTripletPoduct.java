package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.Where;

import java.io.Serializable;

//продкуты проданые из стационарного ларька в текущую смену
@Where("ping = " + PINGATOR.sale_item_product)
@Table("sale_item_product")
public class MTripletPoduct extends MTripletPoductBase implements Serializable, IPing {
    @Column("ping")
    public int ping = PINGATOR.sale_item_product;
}




