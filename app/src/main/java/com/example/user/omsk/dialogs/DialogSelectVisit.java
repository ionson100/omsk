package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MVisitStory;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class DialogSelectVisit extends DialogFragment {
    private ListView mListView;
    private List<MVisitStory> mVisitStories;
    private IActionSelectedVisit mIActionSelectedVisit;
    //private MVisitStory mVisitStory;

    public void initDialog(List<MVisitStory> mVisitStories, IActionSelectedVisit iActionSelectedVisit) {
        this.mVisitStories = mVisitStories;
        this.mIActionSelectedVisit = iActionSelectedVisit;
        Collections.sort(mVisitStories, new Comparator<MVisitStory>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MVisitStory lhs, MVisitStory rhs) {
                return Integer.compare(lhs.finish,rhs.finish);
            }
        });
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_select_visit, null);
        builder.setView(mView);
        mListView = (ListView) mView.findViewById(R.id.list_visit);
        MVisitStory s = new MVisitStory();
        s.point_id = mVisitStories.get(0).point_id;
        mVisitStories.add(s);
        ListAdapterVisit adapterVisit = new ListAdapterVisit(getActivity(), R.layout.item_dialog_select_visit, mVisitStories);
        mListView.setAdapter(adapterVisit);
        return builder.create();
    }

    public interface IActionSelectedVisit {
        void action(MVisitStory mVisitStory);
    }

    class ListAdapterVisit extends ArrayAdapter<MVisitStory> {

        @NonNull
        private final Context mContext;
        private final int mRresource;

        @NonNull
        private final List<MVisitStory> objects;

        public ListAdapterVisit(@NonNull Context context, @LayoutRes int resource, @NonNull List<MVisitStory> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mRresource = resource;
            this.objects = objects;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View mView = convertView;
            final MVisitStory p = getItem(position);
            if (p != null) {
                mView = LayoutInflater.from(getContext()).inflate(mRresource, null);
                TextView textView = (TextView) mView.findViewById(R.id.item_32);
                if (p.id > 0) {

                    Date d= Utils.intToDate(p.finish);
                    textView.setText(mContext.getString(R.string.wewwewe) + Utils.simpleDateFormatE(d));
                } else {
                    textView.setText(R.string.wewe);
                }
                final View finalMView = mView;
                mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalMView.setBackgroundResource(R.color.green);
                        mIActionSelectedVisit.action(p);
                    }
                });
            }
            return mView;
        }
    }
}
