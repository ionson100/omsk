package com.example.user.omsk.visitstoryworker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class FVisitActivity extends Activity {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FVisitActivity.class);

    private ListView mListView;
    private List<MVisitStory> stories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);

        Bundle b = getIntent().getExtras();


        try {
            if (b != null)
                stories = new WriterVisitStory().readWisitStory(b.getString("file"));
        } catch (IOException e) {
            log.error(e);
        }


        mListView = (ListView) findViewById(R.id.list_history_visit);
        findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mListView.setAdapter(new ListAdapterVisitCore(this, R.layout.item_visit_core, stories));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (Settings.core().isStart_dey()) {
                    Utils.messageBox("Выбор перегрузки визита", "Внимание, если Вы нажмете 'OK', прошлый выбранный визит запишется как сегодняшний визит, с текущей датой, он такой же равный, как и реальные визиты, сделанные в текущей открытой смене.\n" +
                            "Эта опция существует для переотправки на сервер потерянных визитов. Злоупотреблять ей не стоит", FVisitActivity.this, new IActionE() {

                        @Override
                        public void action(Object o) {

                            MVisitStory mVisitStory = stories.get(position);
                            mVisitStory.start = Utils.dateToInt(new Date());
                            mVisitStory.finish = Utils.dateToInt(new Date()) - 15 + 60 + 1000;
                            mVisitStory.setNewObject(true);
                            Configure.getSession().insert(mVisitStory);
                            sendBroadcast(new Intent(MainActivity.FAB));
                            finish();
                        }
                    });
                } else {
                    Utils.messageBox("Выбор перегрузки визита", "Вы не можете воспользоваться машиной времени, у Вас не открыта смена!", FVisitActivity.this, null);
                }

            }
        });
    }

   static class ListAdapterVisitCore extends ArrayAdapter<MVisitStory> {

        TextView visit_point, visit_start, visit_finish, visit_summ, visit_send;


        @NonNull
        private final Context context;
        private final int resource;
        @NonNull
        private final List<MVisitStory> objects;

        public ListAdapterVisitCore(@NonNull Context context, @LayoutRes int resource, @NonNull List<MVisitStory> objects) {
            super(context, resource, objects);

            this.context = context;
            this.resource = resource;
            this.objects = objects;
        }


        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;

            final MVisitStory p = getItem(position);
            if (p != null) {

                mView = LayoutInflater.from(getContext()).inflate(resource, null);
                visit_point = (TextView) mView.findViewById(R.id.visit_point);
                visit_start = (TextView) mView.findViewById(R.id.visit_start);
                visit_finish = (TextView) mView.findViewById(R.id.visit_finish);
                visit_summ = (TextView) mView.findViewById(R.id.visit_summ);
                visit_send = (TextView) mView.findViewById(R.id.visit_send);

                visit_point.setText("Визит в точку: " + p.getPoint().name);
                Date d_start=Utils.intToDate(p.start);
                visit_start.setText("Начало:      " + Utils.simpleDateFormatE(d_start));
                Date d_finish=Utils.intToDate(p.finish);
                visit_finish.setText("Окончание: " + Utils.simpleDateFormatE(d_finish));
                visit_send.setText("Статус отправки: " + (p.isNewObject() ? "отпрвлено" : "не отправлено"));

                LinearLayout layout = (LinearLayout) mView.findViewById(R.id.panel_product);

                double sum = 0;
                for (MProduct mProduct : p.getSalesProductList()) {
                    sum = sum + mProduct.amountSelfCore() * mProduct.price;
                    TextView textView = new TextView(getContext());
                    String str = mProduct.getName() + ".  (" + mProduct.amountSelfCore() + " x " + mProduct.price + ")";
                    textView.setText(str);
                    layout.addView(textView);
                }
                visit_summ.setText("Сумма продажи: " + String.valueOf(Utils.round(sum, 2)));

            }
            return mView;
        }
    }
}
