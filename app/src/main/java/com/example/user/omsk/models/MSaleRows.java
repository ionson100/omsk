package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

// истоиия по продаже что показывается в визите, приходит с синхронизацией для подсказки продажи.
@Table("sale_rows")
public class MSaleRows implements Serializable, IUsingGuidId {

    @PrimaryKey("id")
    public transient int id;

    @Expose(serialize = false, deserialize = true)
    @SerializedName("date")
    @Column("date")
    public Date date = null;

    @Column("idu")
    public transient String idu;

    @SerializedName("product_id")
    @Column("product_id")
    public String product_id;

    @Column("point_id")
    public transient String point_id;

    @SerializedName("amount")
    @Column("amount")
    public double amount;

    @SerializedName("price")
    @Column("price")
    public double price;

    public MSaleRows() {
        this.idu = Utils.getUuid();
    }

    public String get_id() {
        return idu;
    }

    public MProduct getProdict() {
        return Configure.getSession().get(MProduct.class, product_id);
    }
}


