package com.example.user.omsk.doublePoints;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.omsk.models.MAgent;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.orm2.Configure;

import java.util.List;


public class ListAdapterDoublePoints extends ArrayAdapter<MPoint> {

    private final int mRresource;

    public ListAdapterDoublePoints(Context context, int resource, List<MPoint> objects) {
        super(context, resource, objects);
      //  List<MPoint> mPoints = objects;

        this.mRresource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;

        final MPoint p = getItem(position);

        LayoutInflater vi;
        vi = LayoutInflater.from(getContext());
        mView = vi.inflate(mRresource, null);

        TextView pointName = (TextView) mView.findViewById(R.id.point_name);
        TextView pointAddress = (TextView) mView.findViewById(R.id.point_address);
        TextView pointContragent = (TextView) mView.findViewById(R.id.point_contragent);

        pointName.setText(p.name);
        pointAddress.setText(p.address);
        MAgent a = p.getContragent(Configure.getSession());
        if (a != null) {
            pointContragent.setText(a.name);
        }
        mView.setTag(p);

        return mView;
    }
}
