package com.example.user.omsk.auto;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.UserField;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**

 */

@Table("auto")
public class MAuto {


    public MAuto() {
        this.uid = UUID.randomUUID().toString();
    }

    @PrimaryKey("id")
    public int id;


    @Column("uid")
    public String uid;

    @Column("lastkm")
    public int lastKm;


    @Column("use_auto")
    public boolean useAuto;

    @UserField(IUserType = MyAutoField.class)
    @Column("auto_number")
    public List<AutoNummber> autoNummbers = new ArrayList<>();

    @Column("number")
    public String number;

    @Column("address1")
    public String address1;

    @Column("address2")
    public String address2;

    @Column("foto1")
    public String foto1;

    @Column("foto2")
    public String foto2;

    @Column("latiduda")
    public double latituda1;


    @Column("longituda")
    public double longituda1;

    @Column("latitude2")
    public double latituda2;
    @Column("longitude2")
    public double longituda2;

    @Column("km1")
    public int km1;


    @Column("km2")
    public int km2;

    @Column("date1")
    public Date date1 ;

    @Column("date2")
    public Date date2 ;

    @Column("is_sender")
    public boolean isSender;


}
