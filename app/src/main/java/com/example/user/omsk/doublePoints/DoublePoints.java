package com.example.user.omsk.doublePoints;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class DoublePoints {
    private static final int DID = 6;
    private final List<MPoint> mPoints;
    private final List<MPoint> mDoubleMPoints = new ArrayList<>();
    private final Activity activity;
    private final Settings mSettings;

    public DoublePoints(Activity activity, Settings mSettings) {
        this.activity = activity;
        mPoints = Configure.getSession().getList(MPoint.class, null);
        this.mSettings = mSettings;
    }

    public void check(MPoint point) {

        String address = "";
        if (point.address != null) {
            address = point.address;
        }
        String[] ss = (point.name + " " + address).replace(",", "").replace(".", "").replace("- ", "-").replace(" -", "-").toUpperCase().split("\\s+");
        Set<String> stringList = new HashSet<>();
        for (String s : ss) {
            if (s.equals(" ")) continue;
            if (s.equals("#")) continue;
            if (s.equals("УЛ")) continue;
            if (s.equals("УЛИЦА")) continue;
            if (s.equals("Г")) continue;
            if (s.equals("ПОСЁЛОК")) continue;
            if (s.equals("ОБЛ")) continue;
            if (s.equals("ОКРУГ")) continue;
            if (s.equals("РОССИЯ")) continue;
            if (s.equals("ПРОЕЗД")) continue;
            if (s.equals("Ш")) continue;
            if (s.equals("Д")) continue;
            if (s.equals("ШОССЕ")) continue;
            if (s.equals("П")) continue;
            if (s.equals("КОРПУС")) continue;
            if (s.equals("K")) continue;
            stringList.add(s);
        }


        Set<String> check1 = new HashSet<>();

        for (MPoint mPoint : mPoints) {
            Inter inter = innerCheck(mPoint, stringList);

            if (inter.result) {
                check1.addAll(inter.strings);
                mDoubleMPoints.add(mPoint);
            }
        }
        if (mDoubleMPoints.size() > 0) {

            DialogDoublePoints dialogDoublePoints = new DialogDoublePoints();
            dialogDoublePoints.setActivity(activity, mDoubleMPoints, check1, new IActionE<View>() {
                @Override
                public void action(View v) {

                }
            }, mSettings).show(((AppCompatActivity) activity).getSupportFragmentManager(), "ds34");

        } else {
            Toast.makeText(activity, activity.getString(R.string.not_double_points), Toast.LENGTH_SHORT).show();
        }


    }

    private Inter innerCheck(MPoint point, Set<String> setList) {

        Set<String> doubleWord = new HashSet<>();

        String address = "";
        if (point.address != null) {
            address = point.address;
        }
        String[] ss = (point.name + " " + address).replace(",", "").replace(".", "").replace("- ", "-").replace(" -", "-").toUpperCase().split("\\s+");
        Set<String> stringList = new HashSet<>();
        for (String s : ss) {
            if (s.equals(" ")) continue;
            if (s.equals("#")) continue;
            if (s.equals("УЛ")) continue;
            if (s.equals("УЛИЦА")) continue;
            if (s.equals("Г")) continue;
            if (s.equals("ПОСЁЛОК")) continue;
            if (s.equals("ОБЛ")) continue;
            if (s.equals("ОКРУГ")) continue;
            if (s.equals("РОССИЯ")) continue;
            if (s.equals("ПРОЕЗД")) continue;
            if (s.equals("Ш")) continue;
            if (s.equals("Д")) continue;
            if (s.equals("ШОССЕ")) continue;
            if (s.equals("П")) continue;
            if (s.equals("КОРПУС")) continue;
            if (s.equals("K")) continue;
            stringList.add(s);
        }

        for (String s : setList) {
            if (stringList.contains(s)) {
                doubleWord.add(s);
            }
        }
        Inter inter = new
                Inter();
        inter.strings = doubleWord;

        if (doubleWord.size() == setList.size()) {//||doubleWord.size()==setList.size()-1
            inter.result = true;
        }
        return inter;
    }
}

class Inter {
    public Set<String> strings;
    public boolean result;
}
