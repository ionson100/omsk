package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.refill.SettingsAddinStock;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SenderOrderPOST {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderOrderPOST.class);
    private SettingsAddinStock lackSettings;
    private String url;
    private Activity activity;
    View b1, b2;
    private String er205 = null;

    public void send(SettingsAddinStock lackSettings, String url, Activity activity, View b1, View b2) {
        this.lackSettings = lackSettings;
        this.b1 = b1;
        this.b2 = b2;
        this.url = url;
        this.activity = activity;
        new SenderWorkerTack().execute();
    }

    private String sederCore(String msg) {

        return UtilsSender.postRequest(activity.getApplication(), Utils.HTTP + url + "/stock_order_client/", msg, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {
            }
        });
    }

    private String getParams() {
        sender sender = new sender();
        sender.guid = SettingsUser.core().getUuidDevice();
        sender.products = new ArrayList<>();
        for (MProduct product : lackSettings.mLastListNew) {
            if (product.getAmount_core() == 0) continue;
            sender.products.add(new ssd(product.get_id(), product.getAmount_core()));
        }
        Gson sd = Utils.getGson();
        return sd.toJson(sender);
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {

        ProgressDialog dialog;


        @Override
        protected String doInBackground(Void... params) {
            if (isCancelled()) return null;
            String string = getParams();
            return sederCore(string);
        }

        @Override
        protected void onPreExecute() {

            if (!Utils.isNetworkAvailable(activity, Settings.core())) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }

            dialog = Utils.factoryDialog(activity, "Отправка данных", null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (dialog != null) {
                dialog.cancel();
            }
            if (aBoolean == null) {
                Toast.makeText(activity, activity.getString(R.string.send_to_server), Toast.LENGTH_SHORT).show();
                lackSettings.mStatusSender = true;
                SettingsAddinStock.save();
                b1.setVisibility(View.GONE);
                b2.setVisibility(View.GONE);
            } else {

                Toast.makeText(activity, activity.getString(R.string.error_send_to_server) + " " + er205, Toast.LENGTH_SHORT).show();
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/stock_order_client/:  " + aBoolean;
                log.error(msg);
                new SenderErrorPOST().send((MyApplication) activity.getApplication(), msg);
            }

        }
    }


   static class ssd implements Serializable {
        public final String id;
        public final double amount;

        public ssd(String id, double amount) {
            this.id = id;
            this.amount = amount;
        }
    }

static     class sender implements Serializable {
        public String guid;
        public List<ssd> products;
    }
}

