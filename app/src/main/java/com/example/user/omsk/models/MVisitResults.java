package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

// результст визита(сервер)
@Table("visit_results")
public class MVisitResults implements IUsingGuidId, Serializable {

    @PrimaryKey("id")
    public transient int id;

    @SerializedName("index")
    @Column("index1")
    public int index;

    @SerializedName("id")
    @Column("idu")
    public String idu;

    @SerializedName("name")
    @Column("name")
    public String name;

    @SerializedName("description")
    @Column("description")
    public String description;

    public String get_id() {
        return idu;
    }

    public static String getSale() {
        List<MVisitResults> list = Configure.getSession().getList(MVisitResults.class, null);
        MVisitResults results = Linq.toStream(list).firstOrDefault(new Predicate<MVisitResults>() {
            @Override
            public boolean apply(MVisitResults t) {
                return t.index == 1;
            }
        });
        return results.idu;
    }

    public static String getNotSale() {
        List<MVisitResults> list = Configure.getSession().getList(MVisitResults.class, null);
        MVisitResults results = Linq.toStream(list).firstOrDefault(new Predicate<MVisitResults>() {
            @Override
            public boolean apply(MVisitResults t) {
                return t.index == 0;
            }
        });
        return results.idu;
    }
}
