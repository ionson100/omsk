package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.Certificate;

import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;

import java.io.File;
import java.io.FileInputStream;

/**
 * Отправка фотографий или видео на сервер
 */
public class SenderPhotoHTTPSCLIENT {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderPhotoHTTPSCLIENT.class);
    private Settings settings;
    private String fileName;
    private String point_id;
    private String visit_id;
    private MyApplication application;

    public void send( String fileName, String point_id, String visit_id, MyApplication application) {
        this.settings = Settings.core();
        this.fileName = fileName;
        this.point_id = point_id;
        this.visit_id = visit_id;
        this.application = application;
        if (senderCore()) {
            Configure.getSession().execSQL(" delete from photo where path ='" + fileName + "\'");
            Configure.getSession().execSQL(" delete from video where path ='" + fileName + "\'");
            File f = new File(fileName);
            f.delete(); // если фото успешно отправленно то фото удаляем с планшета
        }
    }

    private boolean senderCore() {

        String url = Utils.HTTP + settings.url + "/media_client/?point_id=" + point_id + "&" +
                "guid=" + SettingsUser.core().getUuidDevice() + "&visit_id=" + visit_id + "&file=" + fileName.substring(fileName.lastIndexOf(".") + 1);
        try {
            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient(application);// new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            String cert = Certificate.getCertificateSHA1Fingerprint(application);
            httppost.addHeader("Content-user", cert);

            InputStreamEntity reqEntity = new InputStreamEntity(new FileInputStream(fileName), -1);
            reqEntity.setContentType("binary/octet-stream");
            reqEntity.setChunked(true); // send in multiple parts if needed
            httppost.setEntity(reqEntity);
            HttpResponse response = httpclient.execute(httppost);
            if (response.getStatusLine().getStatusCode() == 200) {
                return true;
            } else {
                log.error(" ответ сервера: " + response.getStatusLine().getStatusCode() +
                        ", медиа не ушло на сервер send photo - " + response.getStatusLine().getStatusCode());
                return false;
            }
        } catch (Exception e) {
            log.error("медиа не ушло на сервер send photo - send photo1 - " + e.getMessage());
            return false;
        }
    }
}

