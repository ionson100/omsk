package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.kassa.ZetReport;
import com.example.user.omsk.orm2.IUserType;
import com.google.gson.Gson;

public class MyReportField implements IUserType {
    @Override
    public Object getObject(String str) {
        Gson gson = Utils.getGson();
        ZetReport mZetReprot = gson.fromJson(str, ZetReport.class);
        return mZetReprot;
    }

    @Override
    public String getString(Object ojb) {
        String string = Utils.getGson().toJson(ojb);
        return string;
    }
}
