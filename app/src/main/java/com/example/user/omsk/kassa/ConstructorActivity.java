package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;


public class ConstructorActivity extends AppCompatActivity {

    private Settings mSettings;
    private LinearLayout mLinearLayout;
    private SettingsKassa mSettingsKassa;
    private TextView mLog;
    private Pupper p_name;
    private Pupper p_total_pach;
    private Pupper p_total_price;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constructor);

        mLog = (TextView) findViewById(R.id.log);

        p_name = (Pupper) findViewById(R.id.p_name);

        p_name.setPairString(getString(R.string.sDADD), "");


        p_total_pach = (Pupper) findViewById(R.id.p_total_pach);
        p_total_price = (Pupper) findViewById(R.id.p_total_price);


        mSettingsKassa = SettingsKassa.core();
        mSettings = Settings.core();
        mLinearLayout = (LinearLayout) findViewById(R.id.panel_base_prod);
        init();
        findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.bt_add_prod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConstructorActivity.this, AddProductActivity.class);
                ConstructorActivity.this.startActivityForResult(intent, 1);
            }
        });


        findViewById(R.id.print_ch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSettingsKassa.constructorList.size() == 0) {
                    Utils.messageBox(getString(R.string.error), getString(R.string.vbcvbbbvc), ConstructorActivity.this, null);
                    return;
                }

                for (MProductBase product : mSettingsKassa.constructorList) {
                    if (product.getAmount_core() == 0 || product.price == 0) {
                        Utils.messageBox(getString(R.string.error), "У товара: " + product.name + "\n" + getString(R.string.Las), ConstructorActivity.this, null);
                        return;
                    }
                }

                double summ = 0;
                for (MProductBase mProduct : mSettingsKassa.constructorList) {
                    summ = summ + mProduct.getAmount_core() * mProduct.price;
                }
                if (summ > mSettings.maxSummPrice) {
                    Utils.messageBox(getString(R.string.error),
                            "Сумма чека превышает предельно допустимую: " + mSettings.maxSummPrice + " руб.\n\nРазбейте чек на несколько чеков.", ConstructorActivity.this, null);
                } else {
                    UtilsKassa.messageBoxConfiпPrintCheck(ConstructorActivity.this, new ArrayList<MProductBase>(mSettingsKassa.constructorList), MChecksData.TYPE_CHECK_CONSTRUCTOR, new IActionE() {
                        @Override
                        public void action(Object o) {


                            PrintCheck.pintCheckCore(mLog, ConstructorActivity.this, mSettings, new ArrayList<MProductBase>(mSettingsKassa.constructorList), MChecksData.TYPE_CHECK_CONSTRUCTOR, new IActionE() {
                                @Override
                                public void action(Object o) {
                                    MChecksData checksData = (MChecksData) o;
                                    if (checksData.errorText != null) {
                                        Utils.messageBox(getString(R.string.error), checksData.errorText, ConstructorActivity.this, null);
                                    } else {

                                        MChecks mChecks = new MChecks();
                                        mChecks.checkData = checksData;
                                        mChecks.date = Utils.curDate();
                                        UtilsKassa.clearLast(mSettings);
                                        mChecks.checkData.isLast = true;
                                        Configure.getSession().insert(mChecks);
                                        sendBroadcast(new Intent(MainActivity.FAB));
                                    }
                                }
                            });
                        }
                    }, null);
                }


            }
        });
    }

    public void init() {

        for (final MProductBase product : mSettingsKassa.constructorList) {
            final View mView = LayoutInflater.from(this).inflate(R.layout.item_list_product_selected_for_sale_constructor, null);
            TextView nameTextView = (TextView) mView.findViewById(R.id.name_poduct);
            TextView priceTextView = (TextView) mView.findViewById(R.id.price_poduct);
            TextView amountTextView = (TextView) mView.findViewById(R.id.count_poduct);
            TextView totalPriceTextView = (TextView) mView.findViewById(R.id.total_price_poduct);
            nameTextView.setText(product.name);
            priceTextView.setText(Utils.getStringDecimal(product.price));
            amountTextView.setText(Utils.getStringDecimal(product.getAmount_core()));
            totalPriceTextView.setText(Utils.getStringDecimal(product.getAmount_core() * product.price));
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogConstructorProduct dialog = new DialogConstructorProduct();
                    dialog.setmProduct(product, mView, new IActionE() {
                        @Override
                        public void action(Object o) {
                            if (o.equals(1)) {
                                mLinearLayout.removeView(mView);
                            }
                            if (o.equals(2)) {
                                mLinearLayout.removeAllViews();
                            }
                            if (o.equals(3)) {
                                TextView price = (TextView) mView.findViewById(R.id.price_poduct);
                                price.setText(Utils.getStringDecimal(product.price));
                                TextView amountCore = (TextView) mView.findViewById(R.id.count_poduct);
                                amountCore.setText(Utils.getStringDecimal(product.getAmount_core()));
                                TextView itogo = (TextView) mView.findViewById(R.id.total_price_poduct);
                                itogo.setText(Utils.getStringDecimal(product.price * product.getAmount_core()));
                            }
                            pizdycatePuppers();

                        }
                    });
                    dialog.show(getSupportFragmentManager(), "sdasdii");
                }
            });
            mLinearLayout.addView(mView);


        }
        pizdycatePuppers();
    }

    void pizdycatePuppers() {
        double totalPach = 0;
        double totalPrice = 0;
        for (MProductBase product : mSettingsKassa.constructorList) {
            totalPach = totalPach + product.getAmount_core();
            totalPrice = totalPrice + (product.price * product.getAmount_core());
        }
        p_total_pach.setPairString(getString(R.string.lajsas), UtilsKassa.getStringDecimalE(totalPach));
        p_total_price.setPairString(getString(R.string.asaolsoais), UtilsKassa.getStringDecimalE(totalPrice));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLinearLayout.removeAllViews();
        init();
    }
}
