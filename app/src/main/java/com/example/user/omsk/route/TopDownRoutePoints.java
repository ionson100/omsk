package com.example.user.omsk.route;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MPoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class TopDownRoutePoints {

    public static List<MPoint> top(List<MPoint> mPoints) {

        List<Itator> itatorList = new ArrayList<>();
        for (int i = 0; i < mPoints.size(); i++) {
            if (mPoints.get(i).isSelectForRoute) {
                itatorList.add(new Itator(i, mPoints.get(i)));
            }
        }
        if (itatorList.size() == 0 || itatorList.get(0).index == 0) return mPoints;
        for (Itator itator : itatorList) {
            mPoints.remove(itator.getPoint());
            mPoints.add(itator.getIndex() - 1, itator.getPoint());
        }
        return mPoints;
    }

    public static List<MPoint> down(List<MPoint> mPoints) {
        Collections.reverse(mPoints);
        List<Itator> itatorList = new ArrayList<>();
        for (int i = 0; i < mPoints.size(); i++) {

            if (mPoints.get(i).isSelectForRoute) {
                itatorList.add(new Itator(i, mPoints.get(i)));
            }
        }
        if (itatorList.size() == 0 || itatorList.get(0).index == 0) {
            Collections.reverse(mPoints);
            return mPoints;
        }

        for (Itator itator : itatorList) {
            mPoints.remove(itator.getPoint());
            mPoints.add(itator.getIndex() - 1, itator.getPoint());
        }
        Collections.reverse(mPoints);
        return mPoints;
    }

    static class Itator {
        private final int index;
        private final MPoint mPoint;

        public Itator(int index, MPoint mPoint) {

            this.index = index;
            this.mPoint = mPoint;
        }

        public int getIndex() {
            return index;
        }

        public MPoint getPoint() {
            return mPoint;
        }
    }
}
