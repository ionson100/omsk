package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// хранится хозяин - владелец точки
@Table("agent")
public class MAgent implements Serializable, IUsingGuidId {//

    @PrimaryKey("id")
    public transient int id;
    @SerializedName("id")
    @Column("idu")
    public String idu;
    @SerializedName("address")
    public String address;
    @SerializedName("balance")
    public double balance;
    @SerializedName("name")
    @Column("name")
    public String name;
    @Column("phone")
    public transient String telephone;
    @Column("email")
    public transient String email;
    @Column("comment")
    public String commentary;
    @SerializedName("inn")
    @Column("inn")
    public String inn;

    public MAgent() {
        this.idu = Utils.getUuid();
    }

    public String get_id() {
        return idu;
    }
}
