package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.Table;

import java.io.Serializable;

// продукты
@Table("product")
public class MProduct extends MProductBase implements Serializable, IUsingGuidId {
    public MProduct() {
        this.idu = Utils.getUuid();

    }


}

