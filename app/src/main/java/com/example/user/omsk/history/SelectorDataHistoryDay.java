package com.example.user.omsk.history;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.ListAdapterForStoryVisits;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.models.MVisitStoryPointDay;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


class SelectorDataHistoryDay {

    static void Select(final Activity activity, Date dateUtc, final ListView mListView, Settings mSettings, final TextView titul) {

        titul.setText(Utils.simpleDateFormatForCommentsE(dateUtc));

        long aa = dateUtc.getTime() + 24 * 60 * 60 * 1000;
        Date outDate = new Date(aa);

        String sql = " finish BETWEEN " + String.valueOf(dateUtc.getTime() / 1000) + " AND " + String.valueOf(outDate.getTime() / 1000) + " ";
        List<MVisitStoryPointDay> days=Configure.getSession().getList(MVisitStoryPointDay.class, sql);
        final List<MVisitStoryBase> lis = new ArrayList<MVisitStoryBase>(days);

        if (lis.size() == 0) {
            new SenderHistoryDay().send(activity, dateUtc, outDate, mSettings, new SenderHistoryDay.ISenderHistoryDay() {
                @Override
                public void activate(List<MVisitStoryPointDay> list) {
                    if (list != null) {
                        List<MVisitStoryBase> pointList = new ArrayList<MVisitStoryBase>(list);
                        pritable(mListView, pointList, activity);
                    }
                }
            });
        } else {
            pritable(mListView, lis, activity);
        }
    }

    private static void pritable(ListView mListView, List<MVisitStoryBase> lis, Activity activity) {

        if (lis.size() > 0) {
            MVisitStoryBase p = lis.get(lis.size() - 1);
            if (p.id != 0) {
                lis.add(new MVisitStory());
            }
        } else {
            lis.add(new MVisitStory());
        }

        ListAdapterForStoryVisits storyVisits = new ListAdapterForStoryVisits(activity, R.layout.item_list_for_story_visits, lis, true, true);
        storyVisits.setHistoryCore(true);
        mListView.setAdapter(storyVisits);
    }
}
