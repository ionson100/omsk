package com.example.user.omsk.setting;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public class Item {

    public final Object key;
    public final Object value;

    public Item(Object key, Object value) {
        this.key = key;
        this.value = value;
    }
}
