package com.example.user.omsk.auto;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.InnerDeterminerGPS;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderGoogleAddressToGeoGET;

/**
 * ion100 on 19.10.2017.
 */

public class DialogDetermineGpsUser extends DialogFragment {


    public IActionE<String> iActionE;
    private InnerDeterminerGPS determiner;
    private ProgressBar progressBar2;
    private Button btSave;
    private TextView textView;
    String address;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_determine_gps, null);
        ((TextView) v.findViewById(R.id.title_dalog)).setText(getString(R.string.kidsdsd));

        builder.setView(v);
        v.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        textView = (TextView) v.findViewById(R.id.statis_label);


        btSave = (Button) v.findViewById(R.id.bt_save);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iActionE != null) {
                    iActionE.action(address);
                }
                dismiss();
            }
        });

        progressBar2 = (ProgressBar) v.findViewById(R.id.progressBar2);


        return builder.create();

    }

    @Override
    public void onResume() {
        super.onResume();
        determiner = new InnerDeterminerGPS(getActivity(), new SenderGoogleAddressToGeoGET.IActionAddressGeo() {
            @Override
            public void action(String address, String latitude, String longitude) {
                if (address == null || address.trim().length() == 0) {
                    textView.setText(R.string.errrrrer);
                } else {
                    DialogDetermineGpsUser.this.address = address;
                    textView.setText(address);
                }

                progressBar2.setVisibility(View.GONE);
                btSave.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onDestroy() {
        determiner.destroy();
        super.onDestroy();
    }
}


