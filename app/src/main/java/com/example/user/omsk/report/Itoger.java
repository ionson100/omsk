package com.example.user.omsk.report;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.DebtDuplet;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MTriplet;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class KeyProd {
    public String product_id;
    public double price;

    public KeyProd(String product_id, double price) {
        this.price = price;
        this.product_id = product_id;
    }

    //todo не используется
    @Override
    public int hashCode() {
        return product_id.hashCode() + (int) (price * 1000);
    }
}

public class Itoger {

    private double mTotalSale = 0d;
    private double mTotalReturnRefund = 0d;
    private String mKacca;
    private String mDolg;
    private final List<MVisitStoryBase> mVisitStoryList;
    //private final Itogoficator itogoficator;
//    private List<MVisitStory> visitStoryList;

    public Itoger(List<MVisitStoryBase> mVisitStoryBases) {
        this.mVisitStoryList = mVisitStoryBases;
        List<MOrderType> types = Configure.getSession().getList(MOrderType.class, null);
        mKacca = Linq.toStream(types).firstOrDefault(new Predicate<MOrderType>() {
            @Override
            public boolean apply(MOrderType t) {
                return t.index == 0;
            }
        }).idu;
        mDolg = Linq.toStream(types).firstOrDefault(new Predicate<MOrderType>() {
            @Override
            public boolean apply(MOrderType t) {
                return t.index == 1;
            }
        }).idu;
    }


    public int getPointCount() {
        return mVisitStoryList.size() - 1;
    }

    public List<MProduct> getMapItogoForFree() {
        Map<String, MProduct> map = new HashMap<>();

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null || mVisitStoryBase.getFreeProductList() == null) continue;
            for (MProduct product : mVisitStoryBase.getFreeProductList()) {
                if (map.containsKey(product.idu)) {
                    MProduct d = map.get(product.idu);
                    d.setAmountCore(d.getAmount_core() + product.getAmount_core());

                } else {
                    map.put(product.idu, product);
                }
            }
        }
        List<MProduct> list = new ArrayList<>(map.values());
        Collections.sort(list, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        return list;
    }

    public List<MProduct> getMapItogoForAction() {
        Map<String, MProduct> map = new HashMap<>();

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null || mVisitStoryBase.getActionProductList() == null) continue;
            for (MProduct product : mVisitStoryBase.getActionProductList()) {
                if (map.containsKey(product.idu)) {
                    MProduct d = map.get(product.idu);
                    d.setAmountCore(d.getAmount_core() + product.getAmount_core());

                } else {
                    MProduct product1 = Configure.getSession().get(MProduct.class, product.idu);
                    product1.setAmountCore(product.getAmount_core());
                    product1.price = product.price;
                    map.put(product.idu, product1);
                }
            }
        }
        List<MProduct> list = new ArrayList<>(map.values());
        Collections.sort(list, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        return list;
    }


    public List<MProduct> getMapItogoForReturnDebt() {
        Map<String, MProduct> map = new HashMap<>();

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {

            if (mVisitStoryBase == null) continue;
            if (mVisitStoryBase.debtList.size() == 0) continue;
            for (String s : mVisitStoryBase.debtList) {
                List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, " visit_id = ?", s);
                for (MDebt debt : mDebtList) {
                    MProduct product = Configure.getSession().get(MProduct.class, debt.product_id);
                    if (product == null) continue;
                    if (map.containsKey(product.idu) && map.get(product.idu).price == debt.price) {
                        MProduct d = map.get(product.idu);
                        d.setAmountCore(d.getAmount_core() + debt.amount);
                    } else {
                        product.setAmountCore(debt.amount);
                        product.price = debt.price;
                        map.put(product.idu, product);
                    }
                }
            }

        }

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null) continue;
            if (mVisitStoryBase.getDupletProductList().size() == 0) continue;
            for (DebtDuplet debtDuplet : mVisitStoryBase.getDupletProductList()) {
                for (MTriplet triplet : debtDuplet.tripletList) {
                    MProduct product = Configure.getSession().get(MProduct.class, triplet.product_id);
                    if (product == null) continue;
                    if (map.containsKey(product.idu) && map.get(product.idu).price == debtDuplet.price) {
                        MProduct d = map.get(product.idu);
                        d.setAmountCore(d.getAmount_core() + triplet.amount);
                    } else {
                        product.setAmountCore(triplet.amount);
                        product.price = triplet.price;
                        map.put(product.idu, product);
                    }
                }
            }
        }

        List<MProduct> list = new ArrayList<>(map.values());
        Collections.sort(list, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        for (MProduct product : list) {
            mTotalReturnRefund = mTotalReturnRefund + product.getAmount_core() * product.price;
        }
        return list;
    }


    public double getDebtMoneyForStory() {

        double rest = 0;
        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {

            if (mVisitStoryBase == null) continue;
            if (mVisitStoryBase.debtList.size() == 0) continue;
            for (String s : mVisitStoryBase.debtList) {
                List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, " visit_id = ?", s);
                for (MDebt debt : mDebtList) {
                    MProduct product = Configure.getSession().get(MProduct.class, debt.product_id);
                    if (product == null) continue;
                    rest = rest + debt.price * debt.amount;
                }
            }

        }

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null) continue;
            if (mVisitStoryBase.getDupletProductList().size() == 0) continue;
            for (DebtDuplet debtDuplet : mVisitStoryBase.getDupletProductList()) {
                for (MTriplet triplet : debtDuplet.tripletList) {
                    MProduct product = Configure.getSession().get(MProduct.class, triplet.product_id);
                    if (product == null) continue;
                    rest = rest + triplet.price * triplet.amount;
                }
            }
        }
        return rest;
    }


    public List<MProduct> getMapItogoForSale() {
        Map<KeyProd, MProduct> map = new HashMap<>();
        ISession session = Configure.getSession();

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null) continue;
            if (mVisitStoryBase.getSalesProductList() == null) continue;
            if (mVisitStoryBase.order_type_id == null) continue;
            if (mVisitStoryBase.order_type_id.equals(mDolg)) continue;
            for (MProduct product : mVisitStoryBase.getSalesProductList()) {


                KeyProd is = null;
                for (KeyProd prod : map.keySet()) {

                    double d1 = Utils.round(prod.price, 2);
                    double d2 = Utils.round(product.price, 2);
                    if (prod.product_id.equals(product.idu) && d1 == d2) {
                        is = prod;
                        break;
                    }
                }


                if (is != null) {
                    MProduct d = map.get(is);
                    d.setAmountCore(d.getAmount_core() + product.getAmount_core());

                } else {
                    KeyProd keyProd = new KeyProd(product.idu, product.price);
                    MProduct product1 = session.get(MProduct.class, product.idu);
                    product1.setAmountCore(product.getAmount_core());
                    product1.price = product.price;
                    map.put(keyProd, product1);
                }
            }
        }

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null) continue;
            if (mVisitStoryBase.debtList.size() == 0) continue;
            for (String s : mVisitStoryBase.debtList) {
                List<MDebt> mDebtList = session.getList(MDebt.class, " visit_id = ? ", s);
                for (MDebt debt : mDebtList) {
                    MProduct product = session.get(MProduct.class, debt.product_id);
                    if (product == null) continue;

                    KeyProd is = null;
                    for (KeyProd prod : map.keySet()) {
                        double d1 = Utils.round(prod.price, 2);
                        double d2 = Utils.round(debt.price, 2);
                        if (prod.product_id.equals(product.idu) && d1 == d2) {
                            is = prod;
                            break;
                        }
                    }


                    if (is != null) {
                        MProduct d = map.get(is);
                        double dd = d.getAmount_core() + debt.amount;
                        product.setAmountCore(dd);
                        product.price = d.price;
                        map.put(is, product);


                    } else {
                        KeyProd keyProd = new KeyProd(product.idu, debt.price);
                        product.setAmountCore(debt.amount);
                        product.price = debt.price;
                        map.put(keyProd, product);
                    }
                }
            }
        }

        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null) continue;

            for (DebtDuplet debtDuplet : mVisitStoryBase.getDupletProductList()) {
                for (MTriplet triplet : debtDuplet.tripletList) {
                    MProduct product = session.get(MProduct.class, triplet.product_id);
                    if (product == null) continue;

                    KeyProd is = null;
                    for (KeyProd prod : map.keySet()) {
                        double d1 = Utils.round(prod.price, 2);
                        double d2 = Utils.round(triplet.price, 2);
                        if (prod.product_id.equals(product.idu) && d1 == d2) {
                            is = prod;
                            break;
                        }
                    }


                    if (is != null) {
                        MProduct d = map.get(is);
                        double dd = d.getAmount_core() + triplet.amount;

                        product.setAmountCore(dd);
                        product.price = d.price;
                        map.put(is, product);

                    } else {
                        KeyProd keyProd = new KeyProd(product.idu, triplet.price);
                        product.setAmountCore(triplet.amount);
                        product.price = triplet.price;
                        map.put(keyProd, product);
                    }
                }
            }
        }


        List<MProduct> list = new ArrayList<>(map.values());
        Collections.sort(list, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        for (MProduct product : list) {
            mTotalSale = mTotalSale + product.getAmount_core() * product.price;
        }

        return list;
    }

    public List<MProduct> getMapItogoForDebt(boolean isHistoryCore) {

        Map<KeyProd, MProduct> map = new HashMap<>();

        ISession session = Configure.getSession();
        for (MVisitStoryBase mVisitStoryBase : mVisitStoryList) {
            if (mVisitStoryBase == null) continue;
            if (mVisitStoryBase.getSalesProductList().size() == 0) continue;
            if (isHistoryCore == false) {
                if (mVisitStoryBase.status == true) continue;
            }

            if (mVisitStoryBase.order_type_id == null) continue;
            if (mVisitStoryBase.order_type_id.equals(mKacca)) continue;
            // if(mVisitStoryBase.order_type_id.equals(dolg)!=false)continue;
            for (MProduct ww : mVisitStoryBase.getSalesProductList()) {
                MProduct product = session.get(MProduct.class, ww.idu);


                KeyProd is = null;
                for (KeyProd prod : map.keySet()) {
                    double d1 = Utils.round(prod.price, 2);
                    double d2 = Utils.round(ww.price, 2);
                    if (prod.product_id.equals(product.idu) && d1 == d2) {
                        is = prod;
                        break;
                    }
                }


                if (is != null) {
                    MProduct d = map.get(is);
                    double dd = d.getAmount_core() + ww.getAmount_core();

                    product.setAmountCore(dd);
                    product.price = d.price;
                    map.put(is, product);

                } else {
                    KeyProd keyProd = new KeyProd(product.idu, ww.price);
                    product.setAmountCore(ww.getAmount_core());
                    product.price = ww.price;
                    map.put(keyProd, product);
                }
            }
        }

        List<MProduct> list = new ArrayList<>(map.values());
        Collections.sort(list, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        return list;
    }

    public double getTotalMain() {
        return mTotalSale;//totalReturnRefund+
    }
}
