package com.example.user.omsk.setting.colorpicker.slider;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public interface OnValueChangedListener {
	void onValueChanged(float value);
}