package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.senders.IActionResponse;
import com.example.user.omsk.senders.UtilsSender;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SenderPlanPointsProduct {

    public static org.apache.log4j.Logger log = Logger.getLogger(SenderPlanPointsProduct.class);
    private Settings mSettings;
    private Activity mActivity;
    private IActionE mIAction;

    public void send(Settings mSettings, Activity activity, IActionE iAction) {
        this.mSettings = mSettings;
        this.mActivity = activity;
        this.mIAction = iAction;
        new SenderWorkerTack().execute();
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {


        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {

            if (!Utils.isNetworkAvailable(mActivity, mSettings)) {
                cancel(true);
                Toast.makeText(mActivity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(mActivity, mActivity.getString(R.string.get_data), null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (aBoolean == null) {
                mIAction.action(null);
            } else {


                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/plan_client_new_debts2/:  " + aBoolean;
                Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
                log.error(msg);
                //new SenderErrorPOST().send(mActivity, msg, Settings.core());

            }
            if (dialog != null) {
                dialog.cancel();
            }
            if (aBoolean == null) {
                ISession ses;
                ses = Configure.getSession();
                List<MTemplatePropety> mTemplatePropetyList = ses.getList(MTemplatePropety.class, null);
                if (mTemplatePropetyList.size() > 0) {
                    MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
                    mTemplatePropety.isSendPlan = true;
                    ses.update(mTemplatePropety);
                }
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            if (isCancelled()) return null;
            final String[] result = {null};
            String str = "";
            try {
                str = Utils.HTTP + mSettings.url + "/plan_client_new_debts2/?guid=" + URLEncoder.encode(SettingsUser.core().getUuidDevice(), "UTF-8");

            } catch (UnsupportedEncodingException e) {
                return e.getMessage();
            }

            result[0] = UtilsSender.getRequest((MyApplication) mActivity.getApplication(), str, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {


                    if (str.toLowerCase().equals("ok")) {
                    } else {
                        App(str);
                    }


                }
            });
            return result[0];
        }
    }

    private void App(String res) {

        ISession ses = Configure.getSession();
        try {
            ses.beginTransaction();
            ses.deleteTable("temp_plan");
            ses.deleteTable("plan_points");
            ses.deleteTable("plan_pair");
            ses.deleteTable("mml");

            Gson gson = Utils.getGson();
            ObjectSender sender = gson.fromJson(res, ObjectSender.class);
            List<TempProduct> actionList = sender.products;
//            List<TempProduct> actionList = gson.fromJson(res, new TypeToken<List<TempProduct>>() {
//            }.getType());
            Configure.bulk(TempProduct.class, actionList, ses);
            List<MPlanPointProducts> productsList = new ArrayList<>(actionList.size());
            for (TempProduct tempProduct : actionList) {
                if (tempProduct.permit_id != null) continue;
                MPlanPointProducts f = new MPlanPointProducts();
                f.point_id = tempProduct.point_id;
                f.product_id = tempProduct.product_id;
                f.amount = tempProduct.amount;
                productsList.add(f);
            }
            Configure.bulk(MPlanPointProducts.class, productsList, ses);
            Map<String, Double> map = new HashMap<>();
            for (TempProduct tp : actionList) {
                if (tp.permit_id != null) continue;
                if (map.containsKey(tp.point_id)) {
                    double ss = map.get(tp.point_id) + tp.amount * tp.price;
                    map.put(tp.point_id, ss);
                } else {
                    map.put(tp.point_id, tp.amount * tp.price);
                }
            }
            List<MPlanPair> pairList = new ArrayList<>();

            for (Map.Entry<String, Double> ss : map.entrySet()) {
                MPlanPair d = new MPlanPair();
                d.point_id = ss.getKey();
                d.price = ss.getValue();
                pairList.add(d);
            }

            Configure.bulk(MPlanPair.class, pairList, ses);

            Map<String, Double[]> maps = sender.map;
            for (String s : maps.keySet()) {
                ModelMML mml = new ModelMML();
                mml.mmlName = s.trim();
                mml.mmlPachek = maps.get(s)[0];
                mml.mmlPoint = maps.get(s)[1];
                ses.insert(mml);
            }


            ses.commitTransaction();
        } finally {
            ses.endTransaction();
        }
    }

    class TempPlanE {
        final Map<String, Map<String, Double>> products = new HashMap<>();
        final Map<String, Double> money = new HashMap<>();
    }
}
