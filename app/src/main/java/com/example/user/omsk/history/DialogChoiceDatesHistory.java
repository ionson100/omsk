package com.example.user.omsk.history;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.example.user.omsk.R;

public class DialogChoiceDatesHistory extends DialogFragment {

    private History mHistory;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

    //    Settings mSettings = Settings.core();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        vi = LayoutInflater.from(getActivity());
        View v = vi.inflate(R.layout.dialog_datepicker, null);
        final DatePicker datePicker = (DatePicker) v.findViewById(R.id.datePicker);
        builder.setView(v);
        builder.setNegativeButton(getString(R.string.route_not_confirm2), new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.confirm), new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mHistory.reload(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                dialog.cancel();
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHistory = (History) activity;
    }


}
