package com.example.user.omsk.refill;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.omsk.CalculateKeyBoard;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;

public class DialogEditCountProductForAddinStock extends DialogFragment {

    private IActionE mIActionDelete;
    private IActionE mIActionSave;
    private MProduct mProduct;
    private EditText mEditText;

    public DialogEditCountProductForAddinStock setActionDelete(IActionE iAction) {
        this.mIActionDelete = iAction;
        return this;
    }

    public DialogEditCountProductForAddinStock setActionSave(IActionE iAction) {
        this.mIActionSave = iAction;
        return this;
    }


    public DialogEditCountProductForAddinStock setProduct(MProduct product) {
        this.mProduct = product;
        return this;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        vi = LayoutInflater.from(getActivity());
        View v = vi.inflate(R.layout.dialog_count_product_for_lock, null);
        ((TextView) v.findViewById(R.id.title_dalog)).setText(getString(R.string.sdkisudd));
        builder.setView(v);
        mEditText = (EditText) v.findViewById(R.id.edit_count_lack);
        mEditText.setText(Utils.getStringDecimal(mProduct.getAmount_core()));
        TextView tv_name = (TextView) v.findViewById(R.id.name_product_core);
        tv_name.setText(mProduct.name);
        Button btOk = (Button) v.findViewById(R.id.btOk);
        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProduct.setAmountCore(getDouble());
                if (mIActionSave != null) {
                    mIActionSave.action(mProduct);
                }
                dismiss();
            }
        });

        mEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    mProduct.setAmountCore(getDouble());
                    if (mIActionSave != null) {
                        mIActionSave.action(mProduct);
                    }
                    dismiss();
                }
                return false;
            }
        });
        Button delete = (Button) v.findViewById(R.id.btdelete_row);
        Button cancel = (Button) v.findViewById(R.id.btCancel);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIActionDelete != null) {
                    mIActionDelete.action(mProduct);
                }
                dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        new CalculateKeyBoard(mEditText, v, Settings.core(), false, getActivity());

        return builder.create();
    }

    private double getDouble() {
        double res = 0d;
        try {
            res = Double.parseDouble(mEditText.getText().toString());
        } catch (Exception ignored) {
        }
        return res;
    }
}
