package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.speech.RecognizerIntent;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.omsk.addin.AddinEditPoint;
import com.example.user.omsk.auto.AutoCloseActivity;
import com.example.user.omsk.auto.AutoOpenActivity;
import com.example.user.omsk.dialogs.DialogShowDataToSever;
import com.example.user.omsk.kassa.StartIntentAnnulCheck;
import com.example.user.omsk.kassa.StartIntentConstructor;
import com.example.user.omsk.kassa.StartIntentKassa;
import com.example.user.omsk.models.ListerGroup;
import com.example.user.omsk.models.MChat;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.ping.MyServicePing;
import com.example.user.omsk.reloadgps.DialogReloadGps;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.senders.SenderHistoryNewOrders;
import com.example.user.omsk.senders.SenderSynchronizationPOST;
import com.example.user.omsk.setting.Reanimator;
import com.example.user.omsk.yandex.LbsInfo;
import com.example.user.omsk.yandex.LbsLocationListener;
import com.example.user.omsk.yandex.WifiAndCellCollector;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.osmdroid.http.HttpClientFactory;
import org.osmdroid.http.IHttpClientFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.Projection;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.widget.Toast.LENGTH_LONG;

//import com.example.user.omsk.models.MError;

public class MainActivity extends AppCompatActivity implements FactoryMenu.IMenuAction,
        NavigationView.OnNavigationItemSelectedListener, LbsLocationListener {

    static {

    }

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MainActivity.class);




    public static String FAB = "dsjfkdshfj";
    public static String CLOSE_APP = "kzxcjccccxzc";


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void action(int id) {

        switch (id) {
            case R.id.menu1: {
                if (isStateVisitIsVisit()) {
                    Toast.makeText(MainActivity.this, getString(R.string.alert_commit), Toast.LENGTH_SHORT).show();
                } else {
                    Settings.showFragment(StateSystem.HOME, MainActivity.this);
                }
                break;
            }
            case R.id.menu2: {

                startActivity(new Intent(MainActivity.this, MapRouteActivity.class));
                break;
            }
            case R.id.menu3: {
                StartIntentMessage.start(MainActivity.this);
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_ASS_POINT, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.ASS_POINT, MainActivity.this);
                }

                break;
            }
            case R.id.menu4: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_PRESENT_TYPES_SHOW, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.PRESENT_TYPES_SHOW, MainActivity.this);
                }
                break;
            }
            case R.id.menu5: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_ACYIONS_TYPES_SHOW, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.ACYIONS_TYPES_SHOW, MainActivity.this);
                }
                break;
            }
            case R.id.menu6: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_ASS_POINT, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.ASS_POINT, MainActivity.this);
                }
                break;
            }

            case R.id.menu7: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_SHOW_SETTINGS, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.SETTINGS, MainActivity.this);
                }
                break;
            }
            case R.id.menu8: {
                StartIntentAnnulCheck.start(mSettings, MainActivity.this);
                break;
            }
            case R.id.menu9: {
                StartIntentKassa.start(MainActivity.this, mSettings);
                break;
            }
            case R.id.menu10: {

                StartIntentConstructor.start(MainActivity.this, mSettings);
                break;
            }
            case R.id.menu11: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_SHOW_CHAT, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.CHAT, MainActivity.this);
                }
                break;
            }
            case R.id.menu12: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_SHOW_MAP, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.SIMPLE_MAP, MainActivity.this);
                }
                break;
            }
            case R.id.menu13: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_SHOW_POINT, MainActivity.this);
                } else {
                    StartSearchPointFragment.start(mSettings, MainActivity.this);
                }
                break;
            }
            case R.id.menu14: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_SHOW_STOCK, MainActivity.this);
                } else {
                    if (ValidateDay.validate(mSettings, MainActivity.this)) {
                        Settings.showFragment(StateSystem.SHOW_STOCK_SIMPLE, MainActivity.this);
                    }
                }
                break;
            }
            case R.id.menu15: {

                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_ADVERTISING, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.ADVERTISING, MainActivity.this);
                }
                break;
            }
            case R.id.menu16: {
                StartIntentRoute.start(mSettings, MainActivity.this);
                break;
            }
            case R.id.menu17: {
                StartIntentPlan.start(MainActivity.this, mSettings);
                break;
            }

            case R.id.menu18: {
                if (ValidateDay.validate(mSettings, MainActivity.this)) {
                    new SenderHistoryNewOrders().send(MainActivity.this);
                }

                break;
            }


            case R.id.menu19: {
                StartIntentHistory.start(MainActivity.this);
                break;
            }
            case R.id.menu20: {
                if (mSettings.isStart_dey()) {
                    Utils.messageBox(getString(R.string.error_label), getString(R.string.error_login), MainActivity.this, null);
                } else {
                    StartLoginActivity.start(MainActivity.this);
                }
                break;
            }
            case R.id.menu21: {
                if (isStateVisitIsVisit()) {
                    Settings.showFragment(StateSystem.VISIT_ERROR, MainActivity.this);
                } else {
                    Settings.showFragment(StateSystem.ERROR, MainActivity.this);
                }

                break;
            }
            case R.id.menu22: {
                // показ уведомления о релизе
                Utils.showNoteRealise(MainActivity.this, mSettings, null, null);
                break;
            }
            case R.id.menu23: {
                closeApp();
                break;
            }
            case R.id.part_2: {
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment instanceof FSearchPoint) {
                        ((FSearchPoint) fragment).showDebtPoints();
                    }
                }
                break;
            }


            default:
                break;
        }
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);


    }

    public class FabBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            FABCORE();
        }
    }


    private DrawerLayout drawer;
    public static MainActivity mainActivityInstance;
    private WifiAndCellCollector wifiAndCellCollector;
    private static final String titulUser = "Пользователь: %s.\nРегион: %s.";
    private FloatingActionButton fabupdate;
    private NavigationView navigationView;

    private FloatingActionButton fab;
    private FloatingActionButton fabPing;
    private MainReceiver mainReceiver;
    private MainReceiverPing mainReceiverPing;
    private MyDateChangeReceiver mDateChangeReceiver;
    private MainGeoReceiver mainGeoReceiver;
    private MainReceverCommonMessage mainReceverCommonMessage;
    private FabBroadcastReceiver fabBroadcastReceiver;
    private Settings mSettings;


    public String generateUUID() {
        UUID uuid = UUID.randomUUID();
        StringBuilder str = new StringBuilder(uuid.toString());
        int index = str.indexOf("-");
        while (index > 0) {
            str.deleteCharAt(index);
            index = str.indexOf("-");
        }
        return str.toString();
    }


    public MainActivity() {

        mainActivityInstance = this;

        try {

//            SharedPreferences plutoPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//            SharedPreferences.Editor plutoPrefEditor = plutoPreferences.edit();
//            plutoPrefEditor.putString("country", "Russia");
//            plutoPrefEditor.commit();
        }catch (Exception e){

        }
    }



    public NavigationView NAVIGATE_MENU() {
        return navigationView;
    }


    // активация юзер агента, это влияет на подгрузку фреймев опенстритмапа, агент устаревает, и
    // подгрузка не осуществляется. по этому есть возможность заслать агента с сервера при синхронизации,
    // ПОКА НЕ ИСПОЛЬЗУЕТСЯ ЗАГРУЗКА С СЕРВЕРА ПРИ СИНХРОНИЗАЦИИ
    private void initUserAgent() {
        final String userAgent = mSettings.user_agent;
        HttpClientFactory.setFactoryInstance(new IHttpClientFactory() {
            @Override
            public org.apache.http.client.HttpClient createHttpClient() {
                final DefaultHttpClient client = new DefaultHttpClient();
                client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, userAgent);
                return client;
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ////////////////////////////////////////
        try {


            
            //String sqlTable=Configure.getStringCreateAllTable(this);


            if (Build.VERSION.SDK_INT >= 23) {

                String[] INITIAL_PERMS = {
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.GET_ACCOUNTS,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.VIBRATE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                };//12

                requestPermissions(INITIAL_PERMS, 1327);
            }
            /////////////////////////////////////

            mSettings = Settings.core();
            new Configure(getApplicationInfo().dataDir + "/" + Utils.BASE_NAME, getApplicationContext(), ListerGroup.classes);

            SettingsStart mSettingsStart = SettingsStart.core();
            ///////////////////////////////// узнавание телефона
//            TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//            mSettings.setTeplephone(tMgr.getSimSerialNumber());


            Reanimator.setErrorrnic(new Reanimator.IErrornic() {
                @Override
                public void SaveError(String msg) {
                    new SenderErrorPOST().send((MyApplication) getApplicationContext(), msg);
                }
            });
            // проверка на первый старт системы, если первый старт - перестраиваем базу данных
            FirstStart.workerBase(mSettings, mSettingsStart, getBaseContext());
            // получение гуида устройства
            SettingsUser.core().setUuidDevice(Utils.getUUID(getBaseContext()));
            initUserAgent();

//            MyActivityLifecycleCallbacks callbacks=new MyActivityLifecycleCallbacks();
//
//            this.getApplication().registerActivityLifecycleCallbacks(callbacks);
            /////////////////////////////////////////


            setContentView(R.layout.activity_main);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mSettings = Settings.core();


            // new CrachTest().run();


            fabupdate = (FloatingActionButton) findViewById(R.id.fabUpadet);
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            assert navigationView != null;
            navigationView.setNavigationItemSelectedListener(this);
            fab = (FloatingActionButton) findViewById(R.id.fab);
            fabPing = (FloatingActionButton) findViewById(R.id.fabPing);
            fabPing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.messageBox(getString(R.string.Warning_label), getString(R.string.we34), MainActivity.this, null);
                }
            });


            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogShowDataToSever df = new DialogShowDataToSever();
                    df.show(getSupportFragmentManager(), "sdsdsd");
                }
            });

            FABCORE();

            //////////////////////////////
            Transceiver.subscribe(TransceiveStaticVariables.MAP, new Transceiver.ITransceiver() { // обработчик выбора точки на карте
                @Override
                public void action(Object o) {
                    if (o == null) {
                        mapper = null;
                    } else {
                        mapper = (Mapper) o;
                    }
                }
            });

            mDateChangeReceiver = new MyDateChangeReceiver();
            registerReceiver(mDateChangeReceiver, new IntentFilter(Intent.ACTION_TIME_CHANGED));

            mainGeoReceiver = new MainGeoReceiver();
            registerReceiver(mainGeoReceiver, new IntentFilter(MyServiceGeo.GEO));

            /////////////////////////////////////////
            IntentFilter filterCommonMessage = new IntentFilter();
            filterCommonMessage.addAction(MChat.BRODCAST_ACTION);
            mainReceverCommonMessage = new MainReceverCommonMessage();
            registerReceiver(mainReceverCommonMessage, filterCommonMessage);
            /////////////////////////////////////////

            fabBroadcastReceiver = new FabBroadcastReceiver();
            IntentFilter filter22 = new IntentFilter();
            filter22.addAction(FAB);
            registerReceiver(fabBroadcastReceiver, filter22);
            /////////////////////////////

            mainReceiver = new MainReceiver(); // регистация рессивера  следящего если данные для чата полученые из сервиса  при помощи таймера
            IntentFilter filter = new IntentFilter();
            filter.addAction("omsk_login");
            filter.addAction("omsk_chat");
            filter.addAction("ping");
            registerReceiver(mainReceiver, filter);
            /////////////////////////////////////////////
            mainReceiverPing = new MainReceiverPing();
            IntentFilter filterPing = new IntentFilter();
            filterPing.addAction("ping");
            registerReceiver(mainReceiverPing, filterPing);
            try {
                if (!SettingsUser.core().isAutorise()) {//  проверяем, если пользователь не авторизовался,  открываем форму авторизации, если да то идем дальше
                    StartLoginActivity.start(this);
                }
            } catch (Exception e) {
                StartLoginActivity.start(this);
            }
            Utils.layoutPanel = (LinearLayout) findViewById(R.id.head_panel);// панель для фрагментов
            //////////////////////////////////////////////////////// запуски серверов
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            mSettings.currentVersionName = Utils.templateVersion + (pInfo != null ? pInfo.versionName : "null") + ".apk";
            /////////////////////////////////////////////////
            visibleFloatButton(mSettings.currentVersionName, mSettings.serverVersionName);


            Settings.showFragment(mSettings.getStateSystem(), this); // открываем фрагмент на котором закрылись
            if (!isStateVisitIsVisit()) {
                CheckAssholes.check(this);// проврка на наличие отвергнутых записей.
            }
            // показ уведомления о релизе
            if (!UserNamesSettings.core().idHideNoteShow) {
                Utils.showNoteRealise(this, mSettings, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        UserNamesSettings.core().idHideNoteShow = true;
                        UserNamesSettings.save();
                    }
                }, null);
            }


            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            FactoryMenu factoryMenu = new FactoryMenu(navigationView, MainActivity.this);
            factoryMenu.setMenuAction(this);


        } catch (Exception e) {
            log.error(e);
            Utils.sendMessage("MaintActivity: " + Utils.getErrorTrace(e), (MyApplication) this.getApplication());
            log.error(e);
            //Configure.getSession().insert(new MError(Utils.getErrorTrace(e)));
        }
    }

    private void FABCORE() {
        final boolean res;//  обработчик события  для визуализации сигнала, есть ли данные для отправки на сервер
        res = Utils.isExsistDataToServer();
        fab.post(new Runnable() {
            @Override
            public void run() {
                if (res) {
                    fab.show();
                } else {
                    fab.hide();
                }
            }
        });
    }


    // проверка и зажигания кнопки сигнала что есть обновления.
    public void visibleFloatButton(String currentVersionName, String serverVersionName) {
    }

    public boolean validateUpdate() {
        boolean res = true;
        if (Settings.getStateSystem() != StateSystem.HOME) {
            Toast.makeText(this, getString(R.string.error_update1), LENGTH_LONG).show();
            res = false;
        }

        if (mSettings.isStart_dey()) {
            Toast.makeText(this, getString(R.string.error_update3), LENGTH_LONG).show();
            res = false;
        }
        if (Utils.isExsistDataToServer()) {
            Toast.makeText(this, getString(R.string.error_update2), LENGTH_LONG).show();
            res = false;
        }
        return res;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDestroy() {
        super.onDestroy();
        log.info("onDestroy");
        //Configure.getSession().insert(new MError());
        destoryCore();

        if (mSettings.useYandexGPS && wifiAndCellCollector != null && wifiAndCellCollector.isRun) { //отмена яндекса
            wifiAndCellCollector.stopCollect();
            wifiAndCellCollector = null;
        }

    }

    private void destoryCore() {
        Transceiver.cancelAll();
        stopService(new Intent(this, MyServiceGeo.class));
        stopService(new Intent(this, MyServicePing.class));
        stopService(new Intent(this, MyServiceChat.class));
        stopService(new Intent(this, MyServiceSender.class));
        unregisterReceiver(mainReceiver);
        unregisterReceiver(mainReceiverPing);
        unregisterReceiver(mDateChangeReceiver);
        unregisterReceiver(mainReceverCommonMessage);
        unregisterReceiver(mainGeoReceiver);
        unregisterReceiver(fabBroadcastReceiver);
        Settings.save();
        Reanimator.close();
        Configure.close();
    }

    // обработчик системной кнопки назад
    @Override
    public void onBackPressed() {

        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int state = Settings.getStateSystem();
            if (state == StateSystem.VISIT_STOCK_ACTION) {
                Settings.showFragment(StateSystem.VISIT_ACTION, this);
            } else if (state == StateSystem.MAP_ROUTE_STORY) {
                SettingsGpsMap.core().stateSystem = StateSystem.MAP_ROUTE_STORY;
                SettingsGpsMap.save();
                Settings.showFragment(StateSystem.SEARCH_POINT, this);
            } else if (state == StateSystem.MAP_ROUTE_STORY_ASS) {
                Settings.showFragment(StateSystem.ASS_POINT, this);
            } else if (state == StateSystem.VISIT_MAP_ROUTE_STORY_ASS) {
                Settings.showFragment(StateSystem.VISIT_ASS_POINT, this);
            } else if (state == StateSystem.VISIT_STOCK_PRESENT) {
                Settings.showFragment(StateSystem.VISIT_PRESENT, this);
            } else if ((state == StateSystem.POINT_ADD_SEARCH) && mSettings.getVisit() != null) {
                mSettings.getVisit().point = null;
                Settings.showFragment(StateSystem.SEARCH_POINT, this);
            } else if ((state == StateSystem.POINT_EDIT_SEARCH) && mSettings.getVisit() != null) {

                mSettings.getVisit().point = Configure.getSession().get(MPoint.class, mSettings.getVisit().point.get_id());
                Settings.showFragment(StateSystem.SEARCH_POINT, this);

            } else if (state == StateSystem.CHAT ||
                    state == StateSystem.MAP_ROUTE ||
                    state == StateSystem.SETTINGS ||
                    state == StateSystem.SHOW_STOCK_SIMPLE ||
                    state == StateSystem.SIMPLE_MAP ||
                    state == StateSystem.ADVERTISING ||
                    state == StateSystem.ERROR ||
                    state == StateSystem.ASS_POINT ||
                    state == StateSystem.PRESENT_TYPES_SHOW ||
                    state == StateSystem.ACYIONS_TYPES_SHOW ||
                    state == StateSystem.SEARCH_POINT) {
                Settings.showFragment(StateSystem.HOME, this);

            } else if (state == StateSystem.HOME) {
                if (FactoryFragment.out == 0) {
                    FactoryFragment.out++;
                    Toast.makeText(this, getString(R.string.push_to_out), Toast.LENGTH_SHORT).show();
                } else {
                    closeApp();
                }
            } else if (state == StateSystem.POINT_ADD ||
                    state == StateSystem.POINT_EDIT ||
                    state == StateSystem.CONTRAGENT_ADD ||
                    state == StateSystem.CONTRAGENT_EDIT ||
                    state == StateSystem.SHOW_MAP ||
                    state == StateSystem.CONTACT_ADD ||
                    state == StateSystem.CONTACT_EDIT) {

                if (state == StateSystem.POINT_ADD || state == StateSystem.POINT_EDIT) {
                    mSettings.setVisit(null);
                    Settings.showFragment(StateSystem.SEARCH_POINT, this);
                } else {
                    Settings.showFragment(StateSystem.POINT, this);
                }


            } else if (state == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE ||
                    state == StateSystem.SHOW_POINT_SELECT_POINT_OR_AGENT) {
                if (mSettings.useVisit2) {
                    Settings.showFragment(StateSystem.VISIT_HOME, this);
                } else {
                    Settings.showFragment(StateSystem.VISIT_SALE, this);
                }

            } else if (isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, this);
            } else if (state == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
                Settings.showFragment(StateSystem.VISIT_BALANCE, this);
            } else if (state == StateSystem.POINT ||
                    state == StateSystem.POINT_ADD_SEARCH ||
                    state == StateSystem.POINT_EDIT_SEARCH ||
                    state == StateSystem.CONTRAGENT_ADD_SEARCH ||
                    state == StateSystem.CONTRAGENT_EDIT_SEARCH) {

                if (state == StateSystem.POINT_ADD_SEARCH || state == StateSystem.POINT_EDIT_SEARCH) {
                    mSettings.setVisit(null);
                }
                Settings.showFragment(StateSystem.SEARCH_POINT, this);

            } else if (state == StateSystem.POINT_STORY) {
                Settings.showFragment(StateSystem.SEARCH_POINT, this);
            } else if (state == StateSystem.VISIT_POINT_STORY_FOR) {
                Settings.showFragment(StateSystem.VISIT_HOME, this);
            } else if (state == StateSystem.POINT_STORY_FOR_PREVIEW) {
                Settings.showFragment(StateSystem.POINT, this);
            } else if (state == StateSystem.VISIT_SHOW_MAP_FROM_POINT) {
                Settings.showFragment(StateSystem.VISIT_SHOW_POINT, this);
            } else if (state == StateSystem.VISIT_PRESENT_TYPES_SHOW) {
                Settings.showFragment(StateSystem.VISIT_HOME, this);
            } else if (state == StateSystem.VISIT_ACYIONS_TYPES_SHOW) {
                Settings.showFragment(StateSystem.VISIT_HOME, this);
            } else if (state == StateSystem.VISIT_ACTION) {
                Settings.showFragment(StateSystem.VISIT_HOME, this);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
       // int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void closeApp() {
        this.finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == AutoCloseActivity.RESULT && resultCode == Activity.RESULT_OK) {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment instanceof FHome) {
                    ((FHome) fragment).startDay();
                }
            }
        }


        if (requestCode == AutoOpenActivity.RESULT) {

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment instanceof FHome) {
                    ((FHome) fragment).PRINTABLE_DAY(false);
                }
            }

        }


        if (requestCode == 1961 && resultCode == Activity.RESULT_OK) {
            if (mSettings.getVisit() == null || mSettings.getVisit().point == null) return;
            String addres = data.getStringExtra("address");
            String lat = data.getStringExtra("latitude");
            String lon = data.getStringExtra("longitude");
            if (addres == null) return;
            mSettings.getVisit().point.address = addres;

            double lon1 = 0, lat1 = 0;
            try {
                lat1 = Double.parseDouble(lat);
                lon1 = Double.parseDouble(lon);
            } catch (Exception ex) {

            }
            if (lat1 == 0 || lon1 == 0) {

            } else {
                mSettings.getVisit().point.latitude = lat1;
                mSettings.getVisit().point.longitude = lon1;
            }
            Settings.save();
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {

                if (fragment instanceof FEditAddPoint) {
                    ((FEditAddPoint) fragment).refrashAddress();
                }
            }
        }

        if (requestCode == AddinEditPoint.CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            // Bitmap photo = (Bitmap) data.getExtras().get("data");
            if (mSettings.getVisit() == null) return;
            if (mSettings.getVisit().point == null) return;


            //  MyViewFotoPoint.storeTempImage(photo, mSettings.getVisit().point.idu, MainActivity.this);

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {

                if (fragment instanceof FEditAddPoint) {
                    ((FEditAddPoint) fragment).refrashImage();
                }
            }

        }


        if ((requestCode == SpeechSearchPoint.SPEECH && resultCode == RESULT_OK) || (requestCode == SpeechSearchProduct.SPEECH && resultCode == RESULT_OK)) {
            ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(matches != null){
                if ( matches.size() == 0) {
                    Toast.makeText(MainActivity.this, "Пусто, не получилось ((", LENGTH_LONG).show();
                } else {
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                        if (fragment instanceof IVoicSearching) {
                            ((IVoicSearching) fragment).voicSearching(matches.get(0).toString());
                        }
                    }
                    Toast.makeText(this, matches.get(0).toString(), Toast.LENGTH_SHORT).show();

                }
            }

            return;
        }


        final int ii = requestCode - resultCode;
        final int ii2 = resultCode;
        final SettingsStart settingsStart = SettingsStart.core();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (ii == 1000 && ii2 == 0) {
                    // возврат из обновления
                    settingsStart.setIsFirstStartUpdate(StateUpdate.stabile);

                } else if (ii == 1000 && ii2 != 0) {
                    finish();
                }
            }
        });


        if (Settings.getStateSystem() == StateSystem.VISIT_HOME) {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment instanceof FVisit) {


                    ((FVisit) fragment).mediaActivate();
                }

                if (fragment instanceof FVisit2) {
                    ((FVisit2) fragment).mediaActivate();
                }
            }
        }
        if (requestCode == 3) { // авториязация нового пользователя
            // text_titul.setText(String.format(titulUser, SettingsUser.core().getUserName(), SettingsUser.core().getRegion()));
            if (resultCode == RESULT_OK) {

                SenderSynchronizationPOST sd = new SenderSynchronizationPOST( this, true, true);
                sd.sendSynchronization(0, null);
                List<Fragment> df = getSupportFragmentManager().getFragments();
                if (df != null && df.size() > 0 && mSettings.isStart_dey()) {
                    for (Fragment fragment : df) {
                        if (fragment != null && fragment instanceof FHome) {
                            FHome fHome = (FHome) fragment;
                            fHome.refreshhButtonSynhrinize();
                        }
                    }
                }

                if (df != null && df.size() > 0) {
                    for (Fragment fragment : df) {
                        if (fragment != null && fragment instanceof FHome) {
                            FHome fHome = (FHome) fragment;
                            fHome.activateUser();
                        }
                    }
                }

            }
        }
    }


    // проверка, не находимся ли мы в визите
    public static boolean isStateVisitIsVisit() {

        int state = Settings.getStateSystem();
        boolean action = false;
        if (state == StateSystem.VISIT_HOME ||
                state == StateSystem.VISIT_STOCK_ACTION ||
                state == StateSystem.VISIT_COMMIT ||
                state == StateSystem.VISIT_REFUND ||
                state == StateSystem.VISIT_ADVERTISING ||
                state == StateSystem.VISIT_BALANCE ||
                state == StateSystem.VISIT_START ||
                state == StateSystem.VISIT_SALE ||
                state == StateSystem.VISIT_SHOW_STOCK ||
                state == StateSystem.VISIT_SHOW_CHAT ||
                state == StateSystem.VISIT_SHOW_MAP ||
                state == StateSystem.VISIT_SHOW_POINT ||
                state == StateSystem.VISIT_SHOW_SETTINGS ||
                state == StateSystem.VISIT_COMMIT ||
                state == StateSystem.VISIT_ERROR ||
                state == StateSystem.VISIT_PRESENT ||
                state == StateSystem.VISIT_STOCK_PRESENT ||
                state == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE ||
                state == StateSystem.VISIT_PRESENT_TYPES_SHOW ||
                state == StateSystem.VISIT_ACYIONS_TYPES_SHOW ||
                state == StateSystem.VISIT_ACTION ||
                state == StateSystem.VISIT_MAP_ROUTE ||
                state == StateSystem.VISIT_POINT_STORY_FOR ||
                state == StateSystem.VISIT_ASS_POINT ||
                state == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {
            action = true;
        }
        return action;
    }

    private Mapper mapper;
    private float x = -1;

    // обработчик касаня к экраны, для выбора точки на крте
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mapper != null && (Settings.getStateSystem() == StateSystem.EDIT_MAP || Settings.getStateSystem() == StateSystem.EDIT_MAP_SETTINGS)) {
            int actionType = ev.getAction();
            switch (actionType) {
                case MotionEvent.ACTION_DOWN: {// нажатие
                    x = ev.getX();
                    break;
                }
                case MotionEvent.ACTION_UP: {// отпускание
                    if ((x < (ev.getX() + 10) && x > (ev.getX() - 10)) && Utils.isPointInsideToView(ev.getX(), ev.getY(), mapper.mapView)) {
                        Toast.makeText(MainActivity.this, "Фиксация", Toast.LENGTH_SHORT).show();
                        Projection proj = mapper.mapView.getProjection();
                        GeoPoint loc = (GeoPoint) proj.fromPixels((int) ev.getX(), (int) ev.getY());//-100
                        mapper.myItemizedOverlay.removeAll();
                        GeoPoint myPoint1 = new GeoPoint(((double) loc.getLatitudeE6()) / 1000000, ((double) loc.getLongitudeE6()) / 1000000);
                        mapper.myItemizedOverlay.addItem(myPoint1, "myPoint1");
                        mapper.mapView.getController().animateTo(myPoint1);
                        mapper.mapView.invalidate();
                    }
                    break;
                }
                default:{
                    break;
                }
            }
        }

        return super.dispatchTouchEvent(ev);
    }


    // запуск сервисов
    @Override
    protected void onResume() {
        super.onResume();

        if (wifiAndCellCollector == null && mSettings.useYandexGPS) {
            SharedPreferences settings = getPreferences(Activity.MODE_PRIVATE);
            String uuid = settings.getString("UUID", null);
            if (uuid == null) {
                uuid = generateUUID();
                SharedPreferences.Editor edit = settings.edit();
                edit.putString("UUID", uuid);
                edit.apply();
            }
            wifiAndCellCollector = new WifiAndCellCollector(this, this, uuid);
            wifiAndCellCollector.startCollect();
            (new Thread() {
                @Override
                public void run() {

                    wifiAndCellCollector.requestMyLocation();
                }
            }).start();
        }

        if (wifiAndCellCollector != null && mSettings.useYandexGPS && wifiAndCellCollector.isRun == false) {
            wifiAndCellCollector.startCollect();
        }

        onResumeCoore();

    }


    void onResumeCoore() {


        if (!Utils.isMyServiceRunning(MyServiceChat.class, this)) {// сервис проверки чата
            startService(new Intent(this, MyServiceChat.class));
        }
        if (!Utils.isMyServiceRunning(MyServiceSender.class, this)) {// ссервис отправки  данных на сервис( точки визиты)
            startService(new Intent(this, MyServiceSender.class));
        }
        if (!Utils.isMyServiceRunning(MyServicePing.class, this)) {// ссервис пингования
            startService(new Intent(this, MyServicePing.class));
        }

        if (isStateVisitIsVisit()) { // запуск сервера геолокации есди находимся в визите
            if (mSettings.getVisit() != null &&
                    mSettings.getVisit().point != null &&
                    (mSettings.getVisit().latitude == 0d || mSettings.getVisit().longitude == 0d)) { // если нет координат

                STARTSERVICEGEO();

            }
        }

    }

    // остановка сервиса чата, что бы  он не посылал запросы когда приложение свернуто
    @Override
    protected void onPause() {

        stopService(new Intent(this, MyServiceChat.class));
        stopService(new Intent(this, MyServicePing.class));
        if (mSettings.useYandexGPS && wifiAndCellCollector != null) {
            wifiAndCellCollector.stopCollect();
        }

        Settings.save();
        super.onPause();
    }

    @Override
    public void onLocationChange(LbsInfo lbsInfo) {

        if (lbsInfo.isError) {

        } else {

            double latitude = 0, longitude = 0;
            try {
                latitude = Double.parseDouble(lbsInfo.lbsLatitude);
                longitude = Double.parseDouble(lbsInfo.lbsLongtitude);
            } catch (Exception ex) {

            }
            Location location = new Location("s");
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            {
                Intent intent = new Intent(MyServiceGeo.GEO);
                List<Parcelable> list = new ArrayList<>(1);
                list.add(location);
                intent.putParcelableArrayListExtra("assa", (ArrayList<? extends Parcelable>) list);
                getBaseContext().sendBroadcast(intent);
            }

            {
                Intent intent = new Intent(DialogReloadGps.MainReceiverGPS.INTENT);
                List<Parcelable> list = new ArrayList<>(1);
                list.add(location);
                intent.putParcelableArrayListExtra("assa", (ArrayList<? extends Parcelable>) list);
                getBaseContext().sendBroadcast(intent);
            }

        }
    }

    /**
     * приемник сигнала на обновление чата
     */
    class MainReceiver extends BroadcastReceiver {

        public static final String KEY = "key";
        public static final int LOGIN_VALUE = 0;
        public static final int CHAT_VALUE = 1;
        private static final int ID_ACTION_STOP = -1;

        @Override
        public void onReceive(Context context, Intent intent) {
            int type = intent.getIntExtra(KEY, ID_ACTION_STOP);
            switch (type) {
                case LOGIN_VALUE:
                    break;
                case CHAT_VALUE:
                    List<Fragment> df = getSupportFragmentManager().getFragments();
                    if (df != null && df.size() > 0) {
                        for (Fragment fragment : df) {
                            if (fragment instanceof IUpdateChat) {
                                ((IUpdateChat) fragment).updateChat();
                            }
                        }
                    }
                default:
                    break;
            }
        }
    }

    class MainReceiverPing extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            String res = intent.getStringExtra("1");
            if (res.equals("1")) {
                fabPing.setVisibility(View.GONE);
            } else {
                fabPing.setVisibility(View.VISIBLE);
            }
        }
    }

    public class MainReceverCommonMessage extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra(MChat.BRODCAST_MESSAGE);
            Utils.showCommonMessage(message, MainActivity.this);
        }
    }

    final Handler myHandler = new Handler(); // автоматически привязывается к текущему потоку.

    public class MainGeoReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, final Intent intent) {

            gpsaction(intent);


        }
    }

    private synchronized void gpsaction(final Intent intent) {


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                myHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Location location = (Location) intent.getParcelableArrayListExtra("assa").get(0);

                        MVisit mVisit = mSettings.getVisit();
                        if (mVisit != null && (mSettings.getVisit().longitude == 0 || mSettings.getVisit().longitude == 0)) {
                            mSettings.getVisit().latitude = location.getLatitude();
                            mSettings.getVisit().longitude = location.getLongitude();
                            Settings.save();
                            if (mSettings.useYandexGPS) {

                            } else {
                                stopService(new Intent(MainActivity.this, MyServiceGeo.class));
                            }

                            try {
                                Fragment sd = MainActivity.this.getSupportFragmentManager().findFragmentByTag("commitaction");
                                if (sd != null && sd instanceof FVisitCommit) {
                                    ((FVisitCommit) sd).actionGPS();
                                }
                            } catch (Exception ignored) {
                                log.error(ignored);
                            }

                        }
                    }
                });
            }
        });
        thread.start();
    }

    public class MyDateChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, Intent intent) {
            //MFiscalMemory.addMessage("Пользователь поменял настройки даты/времени:"+Utils.simpleDateFormat(Utils.dateToInt(new Date())));
        }
    }


    public void STARTSERVICEGEO() {
        if (mSettings.useYandexGPS && wifiAndCellCollector != null) {
            (new Thread() {
                @Override
                public void run() {

                    wifiAndCellCollector.requestMyLocation();
                }
            }).start();
        } else {
            if (!Utils.isMyServiceRunning(MyServiceGeo.class, MainActivity.this)) {
                startService(new Intent(this, MyServiceGeo.class));
            }

        }

    }

    public void STOPSERVICEGEO() {
        if (mSettings.useYandexGPS) {


        } else {
            stopService(new Intent(this, MyServiceGeo.class));
        }

    }
}


