package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;

public class InspectionVisit {

    public static boolean inspection(Settings mSettings, Activity activity) {
        if (mSettings.getStartDeyDate_22()==null || !mSettings.isStart_dey()) {
            return true;
        } else {
            String d = Utils.simpleDateFormatForCommentsE(Utils.curDate());//todo test-(24*12*60*60)/2)
            String d1 = Utils.simpleDateFormatForCommentsE(mSettings.getStartDeyDate_22());
            if (d.equals(d1)) {
                return true;
            } else {
                String message = activity.getString(R.string.error_start_dey).replace("dd1", d).replace("dd2", d1);
                Utils.messageBox(activity.getString(R.string.error_label), message, activity, null);
                return false;
            }
        }
    }
}
