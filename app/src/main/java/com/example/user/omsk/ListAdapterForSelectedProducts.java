package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogEditCountProduct;
import com.example.user.omsk.models.MFreePresentTYpes;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.kassa.UtilsKassa;
import com.example.user.omsk.orm2.Configure;

import java.util.List;

public class ListAdapterForSelectedProducts extends ArrayAdapter<MProduct> {

    private final IActionE mIAction;

    private final Settings mSettings;
    private final Context mContext;
    private final int mResource;

    public ListAdapterForSelectedProducts(Context context, int resource, List<MProduct> items, IActionE iAction) {
        super(context, resource, items);
        this.mContext = context;
        this.mResource = resource;
        this.mIAction = iAction;
        this.mSettings = Settings.core();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(mResource, null);
            v.setBackgroundResource(R.color.white);
        }
        final MProduct p = getItem(position);
        if (p != null) {
            final TextView tt1 = (TextView) v.findViewById(R.id.name_poduct);
            final TextView tt2 = (TextView) v.findViewById(R.id.price_poduct);
            final TextView tt3 = (TextView) v.findViewById(R.id.count_poduct);
            final TextView tt4 = (TextView) v.findViewById(R.id.total_price_poduct);
            if (mSettings.getVisit() != null) {
                if (p.amountSelfCore() - p.getAmount_core() < 0) {
                    tt1.setBackgroundColor(mSettings.colorTextStock);
                    tt2.setBackgroundColor(mSettings.colorTextStock);
                    tt3.setBackgroundColor(mSettings.colorTextStock);
                    tt4.setBackgroundColor(mSettings.colorTextStock);
                }
            }
            if (tt1 != null) {
                String name = p.name;
                if (p.rule_id != null) {
                    MFreePresentTYpes d = Configure.getSession().get(MFreePresentTYpes.class, p.rule_id);
                    if (d == null) {
                        d=new MFreePresentTYpes();
                        d.name = "не найдена";
                    }
                    name = name + "\n" + d.name;
                }

                tt1.setText(name);
            }
            if (tt2 != null) {
                tt2.setText(p.getPrice());
            }
            if (tt3 != null) {
                tt3.setText(Utils.getStringDecimal(p.getAmount_core()));
            }
            if (tt4 != null) {
                tt4.setText(Utils.getStringDecimal(p.getTotalPrice()));
            }


            if (UtilsKassa.getMCheckDataCache(mSettings.getVisit().checksDataList) == null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mIAction != null) {
                            mIAction.action(v);
                        }
                        DialogEditCountProduct d = new DialogEditCountProduct();

                        d.setDismis(new IActionE<Object>() {
                            @Override
                            public void action(Object o) {
                                notifyDataSetChanged();

                            }
                        });
                        d.setProduct(p).show(((AppCompatActivity) mContext).getSupportFragmentManager(), "edsfsdfsdf");
                    }
                });
            }


        }


        return v;
    }
}



