package com.example.user.omsk.setting;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public interface iListenerСhanges {
    void OnCallListen(Class aClass, String fieldName, Object oldValue, Object newValue);
}
