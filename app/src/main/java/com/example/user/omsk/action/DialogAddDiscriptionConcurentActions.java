package com.example.user.omsk.action;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Settings;


public class DialogAddDiscriptionConcurentActions extends DialogFragment {
    private Settings mSettings;
    EditText editText;

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        mSettings = Settings.core();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_concurent_actions, null);
        ((TextView) v.findViewById(R.id.title_dalog)).setText(getString(R.string.peeewe));
        builder.setView(v);
        String string = "";
        if (mSettings.getVisit() != null && mSettings.getVisit().concurent_actions != null) {
            string = mSettings.getVisit().concurent_actions;
        }
        editText = (EditText) v.findViewById(R.id.edit_text_concurent);
        editText.setText(string);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().concurent_actions = s.toString();
            }
        });


        v.findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        v.findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        return builder.create();
    }

    public void save() {

        if (mSettings.getVisit() == null) return;
        String string = editText.getText().toString().trim().replace("\n", "");
        if (string.length() == 0) return;
        mSettings.getVisit().concurent_actions = string;
        Settings.save();
        dismiss();
    }

}
