package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.kassa.UtilsKassa;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FVisitRefund extends android.support.v4.app.Fragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FVisitRefund.class);
    private TextView debtString;
    private View mView;
    private Settings mSettings;

    public FVisitRefund() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_refund, container, false);
        try {

            mView.findViewById(R.id.iamage_help).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.showHelpNote(getActivity(),  "note2.html");
                }
            });

            mSettings = Settings.core();
            debtString = (TextView) mView.findViewById(R.id.itogo_debt_string);
            double res = Utils.getDebt(mSettings.getVisit().debtList);
            debtString.setText(getString(R.string.itogo_debt) + Utils.getStringDecimal(res) + getString(R.string.rub));
            EditText editText = (EditText) mView.findViewById(R.id.debit_value);
            editText.setText(Double.toString(mSettings.getVisit().refund));
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    double res = 0;
                    try {
                        res = Double.parseDouble(s.toString());
                    } catch (Exception ignored) {

                    }
                    mSettings.getVisit().refund = res;
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            mView.findViewById(R.id.bt_to_meny_visit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                }
            });
            TextView refund = (TextView) mView.findViewById(R.id.visit_refunt_value);
            double debres = Utils.getDebtPoint(mSettings.getVisit().point);
            refund.setText(getString(R.string.lasdkjsd) + Utils.getStringDecimal(debres) + getString(R.string.rub));
            activatePanel();
        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("Freful:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;
    }

    private void activatePanel() {
        ListView debt_list = (ListView) mView.findViewById(R.id.debt_list);
        List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, " point_id = ? order by date ", mSettings.getVisit().point.idu);
        List<MVisitStory> visitStoryList =
         Configure.getSession().getList(MVisitStory.class, " point_id = ? and idu <> ?",mSettings.getVisit().point.idu, mSettings.getVisit().idu);

        List<MDebt> delete = new ArrayList<>();
        for (MVisitStory mVisitStory : visitStoryList) {
            for (MDebt debt : mDebtList) {
                if (mVisitStory.debtList.contains(debt.order_id)) {
                    delete.add(debt);
                }
            }
        }
        for (MDebt debt : delete) {
            mDebtList.remove(debt);
        }
        Map<String, List<MDebt>> map = new HashMap<>();
        for (MDebt mDebt : mDebtList) {
            if (map.containsKey(mDebt.order_id)) {
                map.get(mDebt.order_id).add(mDebt);
            } else {
                List<MDebt> list = new ArrayList<>();
                list.add(mDebt);
                map.put(mDebt.order_id, list);
            }
        }
        List<TempList> tempLists = new ArrayList<>();

        for (Map.Entry<String, List<MDebt>> ss : map.entrySet()) {
            TempList list = new TempList();
            list.string = ss.getKey();
            list.mDebts = ss.getValue();
            list.date = ss.getValue().get(0).date;
            tempLists.add(list);
        }

        Collections.sort(tempLists, new Comparator<TempList>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(TempList lhs, TempList rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });
        ListAdapterDebt adapterDebt = new ListAdapterDebt(getActivity(), R.layout.item_debt, tempLists);
        adapterDebt.setActionDebt(new ListAdapterDebt.IActionDebt() {
            @Override
            public void action(String visit_id, double itogo, boolean checked) {
                if (checked) {
                    if (mSettings.getVisit().debtList.contains(visit_id)) {
                    } else {
                        mSettings.getVisit().debtList.add(visit_id);
                    }
                } else {
                    if (mSettings.getVisit().debtList.contains(visit_id)) {
                        mSettings.getVisit().debtList.remove(visit_id);
                    }
                }
                Settings.save();
                double res = Utils.getDebt(mSettings.getVisit().debtList);
                debtString.setText(getString(R.string.itogo_debt) + Utils.getStringDecimal(res) + getString(R.string.rub));
            }
        });
        debt_list.setAdapter(adapterDebt);
    }

    static class ListAdapterDebt extends ArrayAdapter<TempList> {

        public interface IActionDebt {
            void action(String visit_id, double itogo, boolean checked);
        }

        @NonNull
        private final Context context;
        private final int resource;
        private IActionDebt iActionDebt;
        private Settings mSettings;

        void setActionDebt(IActionDebt actionDebt) {
            this.iActionDebt = actionDebt;
        }

        @NonNull
        private final List<TempList> objects;

        public ListAdapterDebt(@NonNull Context context, @LayoutRes int resource, @NonNull List<TempList> objects) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            this.objects = objects;
            this.mSettings = Settings.core();
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;

            final TempList p = getItem(position);
            if (p != null) {
                mView = LayoutInflater.from(getContext()).inflate(resource, null);
                Pupper date = (Pupper) mView.findViewById(R.id.visit_date);
                Pupper itogo = (Pupper) mView.findViewById(R.id.visit_itogo_price);
                final CheckBox action = (CheckBox) mView.findViewById(R.id.check_action);


                double itogoE = 0;

                LinearLayout debit_item = (LinearLayout) mView.findViewById(R.id.debit_item);

                String order_id = "";
                for (MDebt mDebt : p.mDebts) {
                    itogoE = itogoE + mDebt.amount * mDebt.price;
                    View v = LayoutInflater.from(getContext()).inflate(R.layout.item_product_story, null);
                    MProduct product = mDebt.getProduct();
                    TextView name = (TextView) v.findViewById(R.id.name_s);
                    TextView amount = (TextView) v.findViewById(R.id.amount_s);
                    TextView price = (TextView) v.findViewById(R.id.price_s);
                    TextView itogoES = (TextView) v.findViewById(R.id.itogo_s);
                    name.setText(product.name);
                    amount.setText(Utils.getStringDecimal(mDebt.amount));
                    price.setText(Utils.getStringDecimal(mDebt.price));
                    itogoES.setText(Utils.getStringDecimal(mDebt.amount * mDebt.price));
                    order_id = mDebt.order_id;

                    debit_item.addView(v);
                }
                Date d=Utils.intToDate(p.mDebts.get(0).date);
                date.setPairString(context.getString(R.string.dfdf), Utils.simpleDateFormatE(d));
                itogo.setPairString(context.getString(R.string.sddddsd), Utils.getStringDecimal(itogoE));


                final double finalItogoE = itogoE;

                final String finalOrder_id = order_id;
                MChecksData data = UtilsKassa.getMChecksDataDebt(mSettings.getVisit().checksDataList, finalOrder_id);


                if (data != null) {
                    mView.findViewById(R.id.text_check_ok).setVisibility(View.VISIBLE);
                    action.setVisibility(View.GONE);
                } else {
                    if (mSettings.getVisit().debtList.contains(p.string)) {
                        action.setChecked(true);
                    }

                    final boolean[] rum = {false};
                    action.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                            if (!rum[0]) {
                                if (mSettings.getVisit().selectProductForActions.size() != 0) {
                                    Utils.messageBoxE((Activity) context, context.getString(R.string.warning), context.getString(R.string.gdfgdfsg), "Отказаться", "продолжить",
                                            new IActionE<View>() {
                                                @Override
                                                public void action(View v) {
                                                    rum[0] = true;
                                                    action.setChecked(!isChecked);
                                                }
                                            }, new IActionE<View>() {
                                                @Override
                                                public void action(View v) {
                                                    iActionDebt.action(p.string, finalItogoE, isChecked);
                                                    mSettings.getVisit().selectProductForActions.clear();
                                                }
                                            }, true);
                                } else {
                                    iActionDebt.action(p.string, finalItogoE, isChecked);
                                }
                            } else {
                                rum[0] = false;
                            }
                        }
                    });
                }


            }
            return mView;
        }
    }

    public static class TempList {
        public String string;
        public List<MDebt> mDebts;
        public int date ;
    }
}


