package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.view.View;

import com.example.user.omsk.models.MVisitStoryAss;
import com.example.user.omsk.orm2.Configure;

import java.util.List;


public class CheckAssholes {

    public static void check(final Activity activity) {
        List<MVisitStoryAss> list = Configure.getSession().getList(MVisitStoryAss.class, null);
        if (list != null) {
            if (list.size() > 0) {
                Utils.messageBox(activity.getString(R.string.Warning_label), activity.getString(R.string.ass), activity, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        if(MainActivity.isStateVisitIsVisit()){
                            Settings.showFragment(StateSystem.VISIT_ASS_POINT, activity);
                        }else {
                            Settings.showFragment(StateSystem.ASS_POINT, activity);
                        }

                    }
                });
            }
        }

    }
}
