package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.addin.SettingsNewOrder;

import java.util.Calendar;
import java.util.Date;


public class DialogPrintDistinct extends DialogFragment {

    private IActionE mIActionE;
    private Date mDateStart = SettingsNewOrder.core().startNew_22;
    private Date mDateFinich = SettingsNewOrder.core().finishNew_22;

    public void setmIActionE(IActionE mIActionE) {
        this.mIActionE = mIActionE;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_print_distinct, null);
        builder.setView(mView);
        if (mDateStart!=null) {
            Button button = (Button) mView.findViewById(R.id.bt_start);
            button.setText(Utils.simpleDateFormatE(mDateStart));
        }

        if (mDateFinich!=null) {
            Button button = (Button) mView.findViewById(R.id.bt_finich);
            button.setText(Utils.simpleDateFormatE(mDateFinich));
        }


        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mView.findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIActionE != null) {
                    Date[] d = new Date[2];
                    d[0] = mDateStart;
                    d[1] = mDateFinich;
                    SettingsNewOrder.core().startNew_22 = mDateStart;
                    SettingsNewOrder.core().finishNew_22 = mDateFinich;
                    SettingsNewOrder.save();
                    mIActionE.action(d);
                    dismiss();
                }
            }
        });

        mView.findViewById(R.id.bt_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                DialogSelectDatesReport dialog = new DialogSelectDatesReport();

                dialog.setActionAndDate(new IActionE() {
                    @Override
                    public void action(Object o) {
                        Calendar c = Calendar.getInstance();
                        c.setTime((Date) o);
                        c.set(Calendar.HOUR_OF_DAY, 0);
                        c.set(Calendar.MINUTE, 0);
                        c.set(Calendar.SECOND, 1);
                        c.set(Calendar.MILLISECOND, 0);

                        mDateStart = c.getTime();

                        ((Button) v).setText(Utils.simpleDateFormatE(mDateStart));
                    }
                }, mDateStart==null ? Utils.curDate() : mDateStart);

                dialog.show(getActivity().getSupportFragmentManager(), "dshd");
            }
        });

        mView.findViewById(R.id.bt_finich).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                DialogSelectDatesReport dialog = new DialogSelectDatesReport();
                dialog.setActionAndDate(new IActionE() {
                    @Override
                    public void action(Object o) {

                        Calendar c = Calendar.getInstance();
                        c.setTime((Date) o);
                        c.set(Calendar.HOUR_OF_DAY, 23);
                        c.set(Calendar.MINUTE, 59);
                        c.set(Calendar.SECOND, 59);
                        c.set(Calendar.MILLISECOND, 0);
                        mDateFinich = c.getTime();


                        ((Button) v).setText(Utils.simpleDateFormatE(mDateFinich));
                    }
                }, mDateFinich==null ? Utils.curDate() : mDateFinich);

                dialog.show(getActivity().getSupportFragmentManager(), "dshd");
            }
        });

        return builder.create();
    }
}
