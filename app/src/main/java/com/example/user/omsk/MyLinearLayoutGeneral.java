package com.example.user.omsk;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyLinearLayoutGeneral extends LinearLayout {
    public MyLinearLayoutGeneral(Context context) {
        super(context);
    }

    public MyLinearLayoutGeneral(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyLinearLayoutGeneral(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
        for (int i = 0; i < this.getChildCount(); i++) {
            View v = this.getChildAt(i);
            if(v instanceof TextView){
                TextView t = (TextView) v;
                if (pressed) {
                    t.setBackgroundResource(R.color.colore97);
                } else {
                    t.setBackgroundResource(R.color.colore3);
                }
            }

        }
    }

}



