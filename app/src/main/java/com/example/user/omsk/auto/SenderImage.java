package com.example.user.omsk.auto;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.AsyncTask;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.Certificate;
//import com.example.user.omsk.models.MError;
import com.example.user.omsk.senders.MySSLSocketFactory;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;

/**
 * ion100 on 23.10.2017.
 */

public class SenderImage {

    private static final org.apache.log4j.Logger log = Logger.getLogger(SenderImage.class);
    private boolean isdelete;
    private Settings settings;


    private String visit_id;
    private MyApplication application;

    public void send(Settings settings, MyApplication application) {
        this.settings = settings;

        this.application = application;
        File file = new File(AutoOpenActivity.getPhotoDirectoryAuto());

        File[] ss = file.listFiles();
        if (ss == null || ss.length == 0) return;
        for (File s : ss) {

            new SenderWorkerTack().execute(s.getAbsolutePath());

        }


    }

    public void sendFile(Settings settings, MyApplication activity, String s) {
        this.settings = settings;
        this.application = activity;
        File file = new File(s);
        if (file.exists() == false) return;
        new SenderWorkerTack().execute(s);
    }


    private class SenderWorkerTack extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {


            String nameFile = params[0];
            String ssd = nameFile.replace(".jpg", "");
            int d = ssd.indexOf("(");
            String point = ssd;
            if (d == -1) {

            } else {
                point = point.substring(0, d);
            }
            point = point.substring(point.lastIndexOf("/") + 1);
            String fil = params[0].substring(params[0].lastIndexOf("/") + 1);

            String url = Utils.HTTP + settings.url + "/client_way_photo/?guid=" + SettingsUser.core().getUuidDevice() + "&" + "waybill_id=" + point + "&" +
                    "file=" + fil;
            try {
                HttpClient httpclient = MySSLSocketFactory.getNewHttpClient(application);// new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                String cert = Certificate.getCertificateSHA1Fingerprint(application);
                httppost.addHeader("Content-user", cert);
                if (isdelete == false) {
                    InputStreamEntity reqEntity = new InputStreamEntity(new FileInputStream(nameFile), -1);
                    reqEntity.setContentType("binary/octet-stream");
                    reqEntity.setChunked(true); // Send in multiple parts if needed
                    httppost.setEntity(reqEntity);
                }

                HttpResponse response = httpclient.execute(httppost);
                if (response.getStatusLine().getStatusCode() == 200) {
                    File file = new File(nameFile);
                    if (file.exists()) {
                        if (file.delete()) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                } else {
                    log.error(" ответ сервера: " + response.getStatusLine().getStatusCode() + ", фото путевого листа не ушло на сервер send photo - " + response.getStatusLine().getStatusCode());
                    return false;
                }
            } catch (Exception e) {
                log.error("фото путевого листа не ушло на сервер - " + Utils.getErrorTrace(e));
                return false;
            }
            return false;
        }


    }

}
