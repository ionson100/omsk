package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

class IconSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final List<String> mValues;// пункты меню
    private final Context mContext;
    private final int resId;

    public IconSpinnerAdapter(Context context, List<String> values, @DrawableRes int resId) {
        this.mValues = values;
        this.mContext = context;
        this.resId = resId;

    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public Object getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(mContext, R.layout.spinner_header, null);
        TextView t = (TextView) v.findViewById(R.id.text_spinner_header);
        t.setText(mValues.get(position));
        ImageView imageView = (ImageView) v.findViewById(R.id.image_spinner_header);
        imageView.setImageResource(resId);

        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(mContext, R.layout.spinner_item_simple, null);
        textView.setText(mValues.get(position));

        return textView;
    }
}
