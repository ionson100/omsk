package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.AsyncTask;

import com.example.user.omsk.MainActivity;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

//import com.example.user.omsk.models.MError;

public class SenderVisitPOST {

    public static final String STR200 = "200";


    private static final org.apache.log4j.Logger log = Logger.getLogger(SenderVisitPOST.class);

    private MVisitStory visitStory;

    private MVisitStory mVisitStory;
    private final MyApplication application;
    private final Settings mSettings;
    private long permision;

    public SenderVisitPOST(MyApplication application) {
        this.application = application;
        this.mSettings =Settings.core();
    }

    private String getParamsVisit(MVisitStory visitStory) {


        Gson sd = Utils.getGson();
        TempVisit tempVisit = TempVisit.getTempVisit(visitStory);
        if (tempVisit.gifts != null) {
            //logger.error(" КОЛИЧЕСТВО ПОДАРКОВ - " + tempVisit.gifts.size());
            for (MVisitPresent gift : tempVisit.gifts) {
                log.error("id - " + gift.id);
            }
        }
        return sd.toJson(tempVisit);
    }


    public void send(MVisitStory visitStory) {

        MVisitStory temp = Configure.getSession().get(MVisitStory.class, visitStory.idu);
        permision = temp.date_pessimise;

        try {
            visitStory.gifts = Configure.getSession().getList(MVisitPresent.class, " visit_id = ? ", visitStory.idu);

        } catch (Exception ex) {

        }


        this.visitStory = visitStory;
        if (visitStory.getSalesProductList().size() > 0 && visitStory.order_type_id == null) {
            return;
        }

        if (visitStory.order_type_id != null && visitStory.order_type_id.equals(MOrderType.get1C())) {
            return ;
        }

        if (visitStory.order_type_id != null && visitStory.order_type_id.equals(MOrderType.getОтсрочка()) && visitStory.refund == -1) {
            return ;
        }

        if (visitStory.getSalesProductList().size() > 0) {
            visitStory.visit_result_id = MVisitResults.getSale();
        } else {
            visitStory.visit_result_id = MVisitResults.getNotSale();
        }

        mVisitStory = visitStory;
        new SenderWorkerTack().execute(visitStory);
    }

    private String sendCore(String json, final MVisitStory param) {


        final String[] res = {null};
        final ISession session = Configure.getSession();
        res[0] = UtilsSender.postRequest(application, Utils.HTTP + mSettings.url + "/visit_client/", json, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {


                if (str == null) {
                    throw new RuntimeException(" ответ сервера на отправку визита - NULL ");
                } else {
                    if (str.equals("202")) {// визит просрочен положен как напоминание о просрочку

                        Shifter.shift(mVisitStory);
                        param.setNewObject(false);
                        session.update(param);
                        application.sendBroadcast(new Intent(MainActivity.FAB));
                        log.error(Utils.curDate() + ": Визит отправлен но не принят сервером: " + mVisitStory.point_name);

                        //убираем фото если было с визитом
//                            List<MPhoto> mPhotos = Utils.getPhotos(session);
//                            for (MPhoto mPhoto : mPhotos) {
//                                if (mPhoto.visit_id.equals(mVisitStory.get_id())) {
//                                    Configure.getSession().delete(mPhoto);
//                                }
//                            }

                    } else {

                        if (str.trim().equals(STR200)) {
                            log.error(Utils.curDate() + ": Визит отправлен на сервер: " + mVisitStory.idu + " ответ: " + str.trim());
                            param.setNewObject(false);
                        } else {
                            throw new RuntimeException("визит 200 ответ - " + str);
                        }


                    }
                }


            }
        });
        return res[0];
    }

    private class SenderWorkerTack extends AsyncTask<MVisitStory, Void, String> {
        @Override
        protected void onPostExecute(String aBoolean) {
            if (aBoolean == null) {
                ISession session = Configure.getSession();
                try{
                    session.beginTransaction();
                    MVisitStory temp = session.get(MVisitStory.class, visitStory.idu);
                    if(temp.date_pessimise==permision){

                        visitStory.setNewObject(false);
                        session.update(visitStory);
                        session.commitTransaction();

                        application.sendBroadcast(new Intent(MainActivity.FAB));

                      //  MVisitStory temp2 = Configure.getSession().get(MVisitStory.class, visitStory.idu);

                        try {
                            String msg="Успешная отправка посещения в точку: " + mVisitStory.getPoint().name;
                            log.error(msg);
                            new SenderErrorPOST().send(application, msg);
                        } catch (Exception ignored) {

                        }
                    }else {
                        String msg="не совпадение замка блокировки во время передачи визит был изменен ";
                        log.error(msg);
                        new SenderErrorPOST().send(application, msg);
                    }
                }finally {
                    session.endTransaction();
                }


            } else {
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/visit_client/: " + aBoolean;
                log.error(msg);
                //Configure.getSession().insert(new MError(msg));
                new SenderErrorPOST().send(application, msg);
            }
        }

        @Override
        protected String doInBackground(MVisitStory... params) {
            String msg = getParamsVisit(params[0]);
            return sendCore(msg, params[0]);
        }
    }
}


