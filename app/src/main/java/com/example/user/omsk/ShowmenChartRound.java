package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import java.util.ArrayList;
import java.util.List;

public class ShowmenChartRound {
    public static Intent show(List<TempSale> tempSaleList, Context context) {

        List<Integer> integers = new ArrayList<>();
        List<String> strings = new ArrayList<>();

        for (TempSale tempSale : tempSaleList) {
            integers.add((int) tempSale.amount);
            strings.add(tempSale.name);
        }

        int[] values = new int[integers.size()];     // шаг 2

        for (int i = 0; i < integers.size(); i++) values[i] = integers.get(i);

        String[] bars = new String[strings.size()];
        for (int i = 0; i < strings.size(); i++) bars[i] = strings.get(i);

        int[] colors = new int[]{Color.BLUE, Color.GREEN, Color.MAGENTA,

                Color.YELLOW, Color.CYAN, Color.BLUE, Color.GREEN, Color.MAGENTA, Color.YELLOW, Color.CYAN, Color.BLACK,
                Color.DKGRAY, Color.LTGRAY, Color.WHITE, Color.rgb(121, 22, 330), Color.rgb(121, 122, 330), Color.rgb(344, 444, 555), Color.CYAN, Color.BLACK,
                Color.YELLOW, Color.CYAN, Color.BLUE, Color.GREEN, Color.MAGENTA, Color.YELLOW, Color.CYAN, Color.BLACK};

        CategorySeries series = new CategorySeries("Pie Chart");  // шаг 3

        DefaultRenderer dr = new DefaultRenderer();   // шаг 4

        for (int v = 0; v < integers.size(); v++) {    // шаг 5

            series.add(bars[v], values[v]);

            SimpleSeriesRenderer r = new SimpleSeriesRenderer();

            r.setColor(colors[v]);

            dr.addSeriesRenderer(r);

        }

        dr.setChartTitle(context.getString(R.string.charround));
        dr.setChartTitleTextSize(30);
        dr.setLabelsTextSize(25);
        dr.setLabelsColor(Color.BLACK);
        dr.setLegendTextSize(40);
        dr.setZoomEnabled(true);
        return ChartFactory.getPieChartIntent(context, series, dr, "Pie of bars");    // шаг 6
    }
}
