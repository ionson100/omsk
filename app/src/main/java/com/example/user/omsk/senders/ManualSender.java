package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.fotocontrol.MPointImage;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.SenderToServer;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.auto.MAuto;
import com.example.user.omsk.auto.SenderAuto;
import com.example.user.omsk.auto.SenderImage;

import com.example.user.omsk.kassa.MChecks;
import com.example.user.omsk.kassa.MZReport;
import com.example.user.omsk.kassa.SendReportToServer;
import com.example.user.omsk.kassa.SenderConstructor;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ManualSender {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ManualSender.class);
    private static final long MIN_CLICK_INTERVAL_TO_SERVER = 30000;
    private static long lastClickTime;
    private final Settings mSettings;
    private final MyApplication application;

    public ManualSender(Settings settings, MyApplication application) {
        mSettings = settings;

        this.application = application;

    }

    public void send() {
        long currentTime = SystemClock.elapsedRealtime();
        if (currentTime - lastClickTime > MIN_CLICK_INTERVAL_TO_SERVER) {
            lastClickTime = currentTime;
            innerSend();
        }
    }

    private synchronized void innerSend() {

        try {

            if (SettingsUser.core().isAutorise() == false) {
                return;
            }


            List<MPoint> pointList = null;
            List<MVisitStory> visitStoryList = null;
            Configure.getSession();

            try {
                pointList = Configure.getSession().getList(MPoint.class, " state_object = 1 ");//" state_object = 1 "" state_object = 1 "
                for (MPoint point : pointList) {
                    point.preparePoindForSender(Configure.getSession());
                }
                visitStoryList =  Configure.getSession().getList(MVisitStory.class, " state_object = 1 ");
            } catch (Exception ignored) {
                log.error(ignored);
            }


            Collections.sort(pointList, new Comparator<MPoint>() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @Override
                public int compare(MPoint lhs, MPoint rhs) {
                    return lhs.getDate().compareTo(rhs.getDate());


                }
            });
            Collections.reverse(pointList);

            Collections.sort(visitStoryList, new Comparator<MVisitStory>() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @Override
                public int compare(MVisitStory lhs, MVisitStory rhs) {
                    return lhs.getDate().compareTo(rhs.getDate());


                }
            });
            Collections.reverse(visitStoryList);

            for (MPoint mPoint : pointList) {
                boolean res = new SenderPointPOST(application).send(mPoint);
                if (res && !mPoint.isNewObject()) {
                }
            }


            for (MVisitStory mVisitStory : visitStoryList) {
                new SenderVisitPOST(application).send(mVisitStory);
            }

            List<MChecks> mChecksList = Configure.getSession().getList(MChecks.class, " is_sender = 0");
            if (mChecksList != null) {
                Collections.sort(mChecksList, new Comparator<MChecks>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public int compare(MChecks lhs, MChecks rhs) {
                        return lhs.date.compareTo(rhs.date);
                    }
                });

                for (final MChecks mChecks : mChecksList) {
                    new SenderConstructor().send(application, mSettings, mChecks, new IActionE() {
                        @Override
                        public void action(Object o) {
                            boolean b = o == null;
                            if (b) {
                                mChecks.isSender = true;
                                Configure.getSession().update(mChecks);
                            }
                        }
                    });
                }
            }


            List<MZReport> mzReportList = Configure.getSession().getList(MZReport.class, null);

            if (mzReportList != null) {
                Collections.sort(mzReportList, new Comparator<MZReport>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public int compare(MZReport lhs, MZReport rhs) {
                        return lhs.mZetReprot.date.compareTo(rhs.mZetReprot.date);
                    }
                });
                for (MZReport mzReport : mzReportList) {
                    new SendReportToServer().send(application, mSettings, mzReport);
                }
            }

            new SenderToServer.SenderJobToServePhoto(mSettings).sendCore(); // todo:  фото должны приходить позже чем точки
            {
                List<MPointImage> list = Configure.getSession().getList(MPointImage.class, "is_delet <> 0");
                if (list != null) {
                    for (MPointImage pointImage : list) {
                        new SenderPhotoPointHTTPSCLIENT().sendDelete( pointImage, application);
                    }
                }
            }

            {
                List<MPointImage> list = Configure.getSession().getList(MPointImage.class, "is_delet = 0");
                if (list != null) {
                    for (MPointImage pointImage : list) {
                        new SenderPhotoPointHTTPSCLIENT().send( pointImage, application);
                    }
                }
            }

            List<MAuto> mAutos = Configure.getSession().getList(MAuto.class, " is_sender = 1");
            for (MAuto mAuto : mAutos) {
                new SenderAuto().send(mAuto, application, mSettings);
            }

            new SenderImage().send(mSettings, application);

        } finally {
            application.sendBroadcast(new Intent(MainActivity.FAB));
        }
    }
}
