package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;


class PrintCloseCheck {
    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(PrintCloseCheck.class);
    public static void print(final TextView log, final Activity activity, final Settings mSettings, final IActionE iActionE) {

        new AsyncTask<Void, String, Void>() {
            private Fptr fptr;
            private ProgressDialog dialog;
            private String errorString;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            private void printText(String text, int alignment, int wrap) throws Exception {
                if (fptr.put_Caption(text) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError();
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError();
                }
                if (fptr.PrintString() < 0) {
                    checkError();
                }
            }

//            private void printText(String text) throws Exception {
//                printText(text, IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
//            }

            private void openCheck(int type) throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_CheckType(type) < 0) {
                    checkError();
                }
                if (fptr.OpenCheck() < 0) {
                    checkError();
                }
            }

            private void closeCheck(int typeClose) throws Exception {
                if (fptr.put_TypeClose(typeClose) < 0) {
                    checkError();
                }
                if (fptr.CloseCheck() < 0) {
                    checkError();
                }
            }

            @Override
            protected void onPreExecute() {
                dialog = Utils.factoryDialog(activity, "Печать тест чека", null);
                dialog.show();
                log.setText("");
            }

            protected void onPostExecute(Void aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                iActionE.action(errorString);
            }

            @Override
            protected Void doInBackground(Void... params) {

                fptr = new Fptr();
                try {
                    fptr.create(activity.getApplication());

                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }

                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");

                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    publishProgress("OK");

                    // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                    // не являются ошибками, если мы хотим просто отменить чек
                    publishProgress("Отмена чека...");
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }
                    publishProgress("OK");

                    if (true) {
                        if (fptr.put_UserPassword("00000030") < 0) {
                            checkError();
                        }

                        publishProgress("Чек закрыт.");

                        printText("Чек закрыт.", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                        printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    }
                } catch (Exception e) {
                    logE.error(e);
                    fptr.CancelCheck();
                    publishProgress(e.toString());
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - PrintCloseCheck\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }

        }.execute();
    }
}
