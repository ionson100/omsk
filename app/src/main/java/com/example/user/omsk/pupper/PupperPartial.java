package com.example.user.omsk.pupper;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.R;


public class PupperPartial extends LinearLayout {

    private TextView mTextViewValue;

    public PupperPartial(Context context) {
        super(context);
        init();
    }

    public PupperPartial(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PupperPartial(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    public TextView getValue() {
        return mTextViewValue;
    }

    public void setPairString(int titul, int value) {

        String sourceString = "<b>" + getContext().getString(titul) + "</b> " + getContext().getString(value);
        mTextViewValue.setText(Html.fromHtml(sourceString));

        mTextViewValue.setText(value);
    }

    public void setPairString(String titul, int value) {

        String sourceString = "<b>" + titul + "</b> " + getContext().getString(value);
        mTextViewValue.setText(Html.fromHtml(sourceString));
    }

    public void setPairString(int titul, String value) {

        String sourceString = "<b>" + getContext().getString(titul) + "</b> " + value;
        mTextViewValue.setText(Html.fromHtml(sourceString));
    }

    public void setPairString(String titul, String value) {

        String sourceString = "<b>" + titul + "</b> " + value;
        mTextViewValue.setText(Html.fromHtml(sourceString));
    }

    private void init() {
        LayoutInflater li = LayoutInflater.from(getContext());
        View view = li.inflate(R.layout.pupper_partial, null, false);

        mTextViewValue = (TextView) view.findViewById(R.id.pupper_value);
        this.addView(view, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
    }
}
