package com.example.user.omsk.myListViewAbc;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * ion100 on 29.11.2017.
 */

public class MyListViewAbc extends LinearLayout {
    private IActionE<String> iActionE;
    private ListView listView;

    public MyListViewAbc(Context context) {
        super(context);
        init();
    }

    public MyListViewAbc(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyListViewAbc(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        List<String> list = new ArrayList<>(Utils.listABC.length);
        for (String s : Utils.listABC) {
            list.add(s);
        }
        listView = (ListView) LayoutInflater.from(getContext()).inflate(R.layout.my_list_abc, null, false);
        ArrayAdapterAlphavit alphavit = new ArrayAdapterAlphavit(getContext(), 0, list);
        listView.setAdapter(alphavit);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = Utils.listABC[position];
                if (iActionE != null) {
                    iActionE.action(s);
                }
            }
        });
        this.addView(listView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
    }

    public void setIAction(IActionE<String> iAction) {
        this.iActionE = iAction;
    }

    static class ArrayAdapterAlphavit extends ArrayAdapter<String> {
        @NonNull
        private final Context context;

        @NonNull
        private final List<String> objects;

        public  ArrayAdapterAlphavit(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> objects) {
            super(context, resource, objects);
            this.context = context;
            this.objects = objects;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;
            mView = LayoutInflater.from(getContext()).inflate(R.layout.alphavit_top, null);
            final String p = getItem(position);
            LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.wrap_abc);
            if (p.toUpperCase().equals("А")) {

                linearLayout.setBackgroundResource(R.drawable.button_alphavit_top);
            } else if (p.toUpperCase().equals("Я")) {

                linearLayout.setBackgroundResource(R.drawable.button_alphavit_bot);
            } else {
                linearLayout.setBackgroundResource(R.drawable.button_alphavit_normal);
            }

            TextView textView = (TextView) mView.findViewById(R.id.char_single);
            textView.setText(p);
            return mView;
        }
    }

}
