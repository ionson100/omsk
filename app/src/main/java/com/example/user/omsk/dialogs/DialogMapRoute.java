package com.example.user.omsk.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.pupper.PupperPartial;

import java.util.Date;

/**
 * ion100 on 03.11.2017.
 */

public class DialogMapRoute extends android.support.v4.app.DialogFragment {

    public MVisitStory mVisitStory;
    public MPoint mPoint;
    public String partial;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_map_point, null);
        builder.setView(mView);
        if (mPoint != null) {
            PupperPartial name, address, fio, telephone, item_number;
            mView.findViewById(R.id.panelqw1).setVisibility(View.VISIBLE);
            address = (PupperPartial) mView.findViewById(R.id.address_point);
            fio = (PupperPartial) mView.findViewById(R.id.fio_point);
            telephone = (PupperPartial) mView.findViewById(R.id.telephone_point);
            name = (PupperPartial) mView.findViewById(R.id.name_point);
            item_number = (PupperPartial) mView.findViewById(R.id.item_number1);
            item_number.setPairString("Очередность визита:", "" + partial.split(":")[1]);
            name.setPairString("Название точки:", mPoint.name);
            address.setPairString("Адрес точки:", mPoint.address);
            fio.setPairString("Ф.И.О.:", mPoint.fio);
            telephone.setPairString("Телефон:", mPoint.telephone_fio);
        }
        if (mVisitStory != null) {
            mView.findViewById(R.id.panelqw2).setVisibility(View.VISIBLE);
            PupperPartial name_point_v, address_point_v, visit_daate_finish, item_number;
            name_point_v = (PupperPartial) mView.findViewById(R.id.name_point_v);
            address_point_v = (PupperPartial) mView.findViewById(R.id.address_point_v);
            visit_daate_finish = (PupperPartial) mView.findViewById(R.id.visit_daate_finish);
            item_number = (PupperPartial) mView.findViewById(R.id.item_number);

            name_point_v.setPairString("Название точки:", mVisitStory.getPoint().name);
            address_point_v.setPairString("Адрес точки:", mVisitStory.getPoint().address);
            Date date= Utils.intToDate(mVisitStory.finish);
            visit_daate_finish.setPairString("Дата визита:", Utils.simpleDateFormatE(date));
            item_number.setPairString("Очередность визита:", "" + partial.split(":")[1]);

        }

        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return builder.create();
    }


}
