package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;

public class GetOFDData {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(GetOFDData.class);
    private Fptr mFptr;

    private void checkError() throws Exception {
        int rc = mFptr.get_ResultCode();
        if (rc < 0) {
            String rd = mFptr.get_ResultDescription(), bpd = null;
            if (rc == -6) {
                bpd = mFptr.get_BadParamDescription();
            }
            if (bpd != null) {
                throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
            } else {
                throw new Exception(String.format("[%d] %s", rc, rd));
            }
        }
    }

    public SettingsKassa get(Activity activity) {

        SettingsKassa ss = new SettingsKassa();

        Fptr fptr = new Fptr();
        try {
            fptr.create(activity.getApplication());
            if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                checkError();
            }
            if (fptr.put_DeviceEnabled(true) < 0) {
                checkError();
            }

            if (fptr.GetStatus() < 0) {
                checkError();
            }
            if (fptr.put_UserPassword("00000030") < 0) {
                checkError();
            }
            if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                checkError();
            }
            if (fptr.SetMode() < 0) {
                checkError();
            }
            ///////////////////////////////////// url
            if (fptr.put_CaptionPurpose(256) < 0) {
                checkError();
            }
            if (fptr.GetCaption() < 0) {
                checkError();
            }
            ss.ofd_url = fptr.get_Caption();
            ////////////////////////////////////////// dns

            if (fptr.put_CaptionPurpose(257) < 0) {
                checkError();
            }
            if (fptr.GetCaption() < 0) {
                checkError();
            }
            ss.ofd_dns = fptr.get_Caption();
            ////////////////////////////////////////// ПОРТ

            if (fptr.put_ValuePurpose(301) < 0) {
                checkError();
            }
            if (fptr.GetValue() < 0) {
                checkError();
            }
            ss.ofd_port = (int) fptr.get_Value();
            //////////////////////////////////////////
        } catch (Exception ex) {

            log.error(ex);
            ss.ofd_url = "Ошибка чтения данных из кассы: " + ex.toString();
            new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), Settings.core().kkmNumber + "\n" +
                    "Ошибка ккм file  - GetOFDData\n" + ex.getMessage());

        } finally {
            fptr.ResetMode();
            fptr.destroy();
        }

        return ss;
    }
}
