package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;

import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Date;

public class SenderMessagePOST {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderMessagePOST.class);

    private ProgressDialog dialog;
    private MyApplication application;
    private Settings mSettings;
    private String msg;
    private Activity activity;


    public void send(String message, MyApplication application,  Activity activity) {
        this.application = application;
        this.mSettings = Settings.core();
        this.msg = message;
        this.activity = activity;
        new SenderWorkerTack().execute();
    }

    private String sendCore(String json) {

        ErrorS errorS = new ErrorS();
        errorS.tel = "0000";
        errorS.date = Utils.curDate();
        errorS.message = "Сообщение: " + json;
        errorS.guid = SettingsUser.core().getUuidDevice();
        Gson sd = Utils.getGson();
        json = sd.toJson(errorS);

        return UtilsSender.postRequest(application, Utils.HTTP + mSettings.url + "/error/", json, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {
            }
        });
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            if (!Utils.isNetworkAvailable(activity, mSettings)) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, "Отпрвка  сообщения", null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (dialog != null) {
                dialog.cancel();
                dialog.dismiss();
            }
            if (aBoolean == null) {
                Toast.makeText(activity, "Сообщение отправлено.", Toast.LENGTH_SHORT).show();
                activity.finish();
            } else {
                Toast.makeText(activity, "Ошибка передачи.", Toast.LENGTH_SHORT).show();

                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/error_message/:  " + aBoolean;
                log.equals(msg);
                //new SenderErrorPOST().send(application, msg, Settings.core());
            }

        }

        @Override
        protected String doInBackground(Void... params) {
            if (isCancelled()) return null;
            return sendCore(msg);
        }
    }

    static class  ErrorS implements Serializable {
        public String guid;
        public String message;
        public String tel;
        Date date = null;
    }
}
