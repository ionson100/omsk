package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.example.user.omsk.senders.SenderMessagePOST;

public class MessageActivity extends Activity {

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        editText = (EditText) findViewById(R.id.message_body);
        findViewById(R.id.message_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.message_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });

        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return send();
                }
                return false;
            }
        });
    }

    boolean send() {
        String msq = editText.getText().toString();
        if (msq.trim().equalsIgnoreCase("")) {
            editText.setError(getString(R.string.empty_string));
            return true;
        } else {
            new SenderMessagePOST().send(editText.getText().toString().trim(), (MyApplication) getApplication(), this);
        }
        return false;
    }
}
