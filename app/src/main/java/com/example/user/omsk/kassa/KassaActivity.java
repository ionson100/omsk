package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.settings.SettingsActivity;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;


public class KassaActivity extends AppCompatActivity {

    private static final boolean PRINT_FISCAL_CHECK = true;
    private static final boolean PRINT_NONFISCAL_CHECK = true;
    private TextView mLog;
    private Settings mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kassa);

        mSettings = Settings.core();
        // boolean isEmulator = "goldfish".equals(Build.HARDWARE);

        mLog = (TextView) findViewById(R.id.log);

        findViewById(R.id.kassa_settings_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showSettingKassa(KassaActivity.this);
            }

        });

        findViewById(R.id.kassa_settings_button_default).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.messageBox(getString(R.string.sdddsdd), getString(R.string.sddsdsd), KassaActivity.this, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        Fptr fptr = new Fptr();
                        try {
                            fptr.create(KassaActivity.this);
                            SettingsKassa.core().settingsKassa = fptr.get_DeviceSettings();
                            SettingsKassa.save();
                        } finally {
                            fptr.destroy();
                        }
                    }
                });
            }
        });

        findViewById(R.id.check_kassir_user_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetKassirName.check(mLog, KassaActivity.this, mSettings, new IActionE() {
                    @Override
                    public void action(final Object o) {
                        KassaActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.messageBox(getString(R.string.xzvvvvvvvvv), o.toString(), KassaActivity.this, null);
                            }
                        });
                    }
                });
            }
        });

        findViewById(R.id.kassa_settings_button_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.messageBox(getString(R.string.warning), getString(R.string.assssssssssssww), KassaActivity.this, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        SetSettingsKKP.set(mLog, KassaActivity.this, mSettings, new IActionE() {
                            @Override
                            public void action(Object o) {
                                Utils.messageBox("", o.toString(), KassaActivity.this, null);
                            }
                        });
                    }
                });
            }
        });

        findViewById(R.id.kassa_test).setOnClickListener(new View.OnClickListener() {
            final ProgressDialog[] dialog = new ProgressDialog[1];


            @Override
            public void onClick(View v) {
                CheckConnectDevise.check(mLog, KassaActivity.this, mSettings, new IActionE() {
                    @Override
                    public void action(Object o) {
                        Utils.messageBox("", o.toString(), KassaActivity.this, null);
                    }
                });
            }
        });

        findViewById(R.id.kassa_test_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintTestCheck.print(mLog, KassaActivity.this, mSettings, new IActionE() {
                    @Override
                    public void action(Object o) {
                        if (o != null) {
                            Utils.messageBox(getString(R.string.error), o.toString(), KassaActivity.this, null);
                        }
                        mLog.setText("");
                        sendBroadcast(new Intent(MainActivity.FAB));
                    }
                });
            }
        });
        findViewById(R.id.check_status_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetStateKassa.get(mLog, KassaActivity.this, mSettings, new IActionE() {
                    @Override
                    public void action(Object o) {
                        final StateKKM state = (StateKKM) o;
                        if (state.errorMessage == null) {

                            UtilsKassa.messageBoxStateKassa(state, KassaActivity.this);
                        } else {
                            KassaActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.messageBox(getString(R.string.error), state.errorMessage.toString(), KassaActivity.this, null);
                                }
                            });
                        }
                        mLog.setText("");
                    }
                });
            }
        });

        findViewById(R.id.z_report).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.messageBox(getString(R.string.zsdsd), getString(R.string.sdffffdf), KassaActivity.this, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        PrintZReport.get(mLog, KassaActivity.this, mSettings, new IActionE() {
                            @Override
                            public void action(Object o) {
                                Zreport zreport = (Zreport) o;
                                if (zreport.error != null) {
                                    Utils.messageBox(getString(R.string.error), zreport.error, KassaActivity.this, null);
                                } else {
                                }
                                mLog.setText("");
                            }
                        });
                    }
                });
            }
        });

        findViewById(R.id.x_report).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintXReport.get(mLog, KassaActivity.this, mSettings, new IActionE() {
                    @Override
                    public void action(Object o) {
                        Zreport zreport = (Zreport) o;
                        if (zreport.error != null) {
                            Utils.messageBox(getString(R.string.error), zreport.error, KassaActivity.this, null);
                        }
                        mLog.setText("");
                    }
                });
            }
        });

        findViewById(R.id.kassa_settings_usb_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final UsbManager manager = (UsbManager) KassaActivity.this.getSystemService(Context.USB_SERVICE);
                    if (manager.getDeviceList().size() == 0) {
                        Utils.messageBox("USB", getString(R.string.laks), KassaActivity.this, null);
                    }
                    for (String s : manager.getDeviceList().keySet()) {
                        Utils.messageBox("USB", "USB устройство - " + s + " определено.", KassaActivity.this, null);
                    }
                } catch (Exception ignored) {
                }
            }
        });


        findViewById(R.id.kassa_settings_button_default2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintLastCheck.printLast(mSettings, KassaActivity.this, mLog);
            }
        });

        findViewById(R.id.kassa_wifi_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetWiFiSettings.get(KassaActivity.this, new IActionE() {
                    @Override
                    public void action(Object o) {
                        GetWiFiSettings.WifiSettings wifiSettings = (GetWiFiSettings.WifiSettings) o;
                        DialogWiFi wiFi = new DialogWiFi();
                        wiFi.setWifiSet(wifiSettings);
                        wiFi.setmIActionE(new IActionE() {
                            @Override
                            public void action(Object o) {
                                GetWiFiSettings.WifiSettings w = (GetWiFiSettings.WifiSettings) o;
                                SetWiFiSettings.set(KassaActivity.this, w);
                            }
                        });
                        wiFi.show(KassaActivity.this.getSupportFragmentManager(), "zzf");
                    }
                });

            }
        });

        findViewById(R.id.kassa_relationship).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetOfdSettings.get(KassaActivity.this, new IActionE() {
                    @Override
                    public void action(Object o) {
                        SettingsKassa kassa = (SettingsKassa) o;
                        DialogRelationOFD relationOFD = new DialogRelationOFD();
                        relationOFD.setSettongsKassa(kassa);
                        relationOFD.setmIActionE(new IActionE() {
                            @Override
                            public void action(Object o) {

                                SettingsKassa set = (SettingsKassa) o;
                                SetOfdSettings.set(KassaActivity.this, set);
                            }
                        });
                        relationOFD.show(KassaActivity.this.getSupportFragmentManager(), "ksajdfaisd");
                    }
                });
            }
        });

        findViewById(R.id.kassa_constructor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartIntentConstructor.start(KassaActivity.this, mSettings);
            }
        });

        findViewById(R.id.kassa_works_checks).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartIntentAnnulCheck.start(mSettings, KassaActivity.this);
            }
        });

        if (mSettings.isStart_dey() == false || SettingsUser.core().isAutorise() == false) {
            findViewById(R.id.kassa_constructor).setVisibility(View.GONE);
            findViewById(R.id.kassa_works_checks).setVisibility(View.GONE);
        }

        findViewById(R.id.x_report_distinct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintXReportDistinct.print(KassaActivity.this);
            }
        });

        findViewById(R.id.check_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintCloseCheck.print(mLog, KassaActivity.this, mSettings, new IActionE() {
                    @Override
                    public void action(Object o) {
                        String s = (String) o;
                        if (s != null) {
                            Utils.messageBox(getString(R.string.error), s, KassaActivity.this, null);
                        } else {
                            mLog.setText("");
                        }
                    }
                });
            }
        });

        findViewById(R.id.check_string).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.messageBox(getString(R.string.warning), getString(R.string.dasdasd), KassaActivity.this, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        Intent intent = new Intent(KassaActivity.this, TableKassaActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });

        findViewById(R.id.open_session).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenSession.Open(KassaActivity.this);
            }
        });

        findViewById(R.id.kassa_test_ofd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintConnectOfd.pint(KassaActivity.this);
            }
        });

        findViewById(R.id.kassa_001).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogLLCommandRequest request = new DialogLLCommandRequest();
                request.show(getSupportFragmentManager(), "sadiuds");
            }
        });

        findViewById(R.id.kkm_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintInfoKKM.pint(KassaActivity.this);
            }
        });

        findViewById(R.id.return_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.messageBox(getString(R.string.warning), getString(R.string.return_aass), KassaActivity.this, new IActionE() {
                    @Override
                    public void action(Object o) {
                        startActivity(new Intent(KassaActivity.this, ReturnCheck.class));
                    }
                });
            }
        });
        findViewById(R.id.show_nal_kkm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Double d = new Double(0);
                try {
                    new PrintKkmSumma().getSummKKM(KassaActivity.this, Settings.core());
                } catch (Exception e) {
                    Toast.makeText(KassaActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
//                DialogReportKKMMoney money = new DialogReportKKMMoney();
//                money.show(KassaActivity.this.getSupportFragmentManager(), "asoia");
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (data != null && data.getExtras() != null) {
                String settings = data.getExtras().getString(SettingsActivity.DEVICE_SETTINGS);
                SettingsKassa.core().settingsKassa = settings;
                SettingsKassa.save();
                Utils.messageBox(getString(R.string.kjzxczxc), settings, this, null);
            }
        }
    }

    private void logAdd(String string) {
        String value = (String) mLog.getText();
        if (value.length() > 2096) {
            value = value.substring(0, 2096);
        }
        mLog.setText(string + "\n" + value);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

