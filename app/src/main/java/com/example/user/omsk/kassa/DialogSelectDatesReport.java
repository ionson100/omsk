package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;

import java.util.Calendar;
import java.util.Date;


public class DialogSelectDatesReport extends DialogFragment {

    IActionE mIActionE;
    private Date mDate = null;


    public void setActionAndDate(IActionE action, Date date) {
        this.mIActionE = action;
        this.mDate = date;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

     //   Settings mSettings = Settings.core();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_datepicker, null);
        final DatePicker datePicker = (DatePicker) v.findViewById(R.id.datePicker);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mDate);
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        builder.setView(v);
        builder.setNegativeButton(getString(R.string.route_not_confirm2), new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }

        });
        builder.setPositiveButton(getString(R.string.confirm), new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year = datePicker.getYear();

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                mIActionE.action(calendar.getTime());
                dismiss();
            }

        });
        return builder.create();
    }
}



