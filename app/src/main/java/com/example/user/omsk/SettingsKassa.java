package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MProductConstructor;
import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SettingsKassa implements Serializable {

    public List<MProductConstructor> constructorList = new ArrayList<>();
    public String settingsKassa;

    public static SettingsKassa core() {
        return (SettingsKassa)
                Reanimator.get(SettingsKassa.class);
    }

    public static void save() {
        Reanimator.save(SettingsKassa.class);
    }

    public String ofd_url;
    public String ofd_dns;
    public int ofd_port;

}
