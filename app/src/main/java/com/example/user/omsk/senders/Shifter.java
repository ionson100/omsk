package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

//import com.example.user.omsk.models.MError;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.models.MVisitStoryAss;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import org.apache.log4j.Logger;

import java.util.ArrayList;

// перемещение визита который не приняли на сервере в старый
public class Shifter {
    public static org.apache.log4j.Logger log = Logger.getLogger(Shifter.class);
    public static void shift(MVisitStory visitStory) {

        MVisitStoryAss storyOld = new MVisitStoryAss();
        storyOld.idu = visitStory.idu;
        storyOld.refund = visitStory.refund;
        storyOld.uuid = visitStory.uuid;
        storyOld.payment = visitStory.payment;
        storyOld.point_id = visitStory.point_id;
        storyOld.connterpirty_id = visitStory.connterpirty_id;
        storyOld.start = visitStory.start;
        storyOld.finish = visitStory.finish;
        storyOld.latitude = visitStory.latitude;
        storyOld.longitude = visitStory.longitude;
        storyOld.visit_result_id = visitStory.visit_result_id;
        storyOld.comment = visitStory.comment;
        storyOld.order_type_id = visitStory.order_type_id;
        storyOld.isRecomendationBalance = visitStory.isRecomendationBalance;
        storyOld.isRecomendationSale = visitStory.isRecomendationSale;
        storyOld.stateObject = visitStory.stateObject;
        //storyOld.date = visitStory.date;
        storyOld.setSalecProductCore(new ArrayList<MProduct>());
        storyOld.setSalecProductCore(visitStory.getSalesProductList());
        storyOld.setBalanceProductCore(visitStory.getBalanceProductList());
        storyOld.setFreeProductCore(visitStory.getFreeProductList());
        storyOld.setActionProductCore(visitStory.getActionProductList());
        storyOld.member_action = visitStory.member_action;
        storyOld.concurent_actions = visitStory.concurent_actions;
        ISession ses = Configure.getSession();
        try {
            MVisitStoryAss ass = ses.get(MVisitStoryAss.class, storyOld.idu);
            if (ass == null) {
                ses.insert(storyOld);
            }
        } catch (Exception ex) {

            log.error(ex);
            //Configure.getSession().insert(new MError(ex.getMessage()));
        }
    }
}
