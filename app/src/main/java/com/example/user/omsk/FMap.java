package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.PathOverlay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FMap extends Fragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FMap.class);
    private Mapper mapper;
    private Settings mSettings;
    private MapView mMapView;
    private MyItemizedOverlay myItemizedOverlay = null;
    private GeoPoint startPointRoute;

    public FMap() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mSettings = Settings.core();
        View view = null;
        try {
            if (Settings.getStateSystem() == StateSystem.EDIT_MAP || Settings.getStateSystem() == StateSystem.EDIT_MAP_SETTINGS) {
                view = inflater.inflate(R.layout.fmap_save, container, false);

                Button btSave = (Button) view.findViewById(R.id.button_sabe_point);
                btSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Settings.getStateSystem() == StateSystem.EDIT_MAP) {
                            if (mSettings.getVisit().point.id == 0) {
                                Settings.showFragment(StateSystem.POINT_ADD, getActivity());
                            } else {
                                Settings.showFragment(StateSystem.POINT_EDIT, getActivity());
                            }
                            if (myItemizedOverlay.getGeoPoint() != null) {
                                mSettings.getVisit().point.latitude = myItemizedOverlay.getGeoPoint().getLatitude();
                                mSettings.getVisit().point.longitude = myItemizedOverlay.getGeoPoint().getLongitude();
                            }
                        } else if (Settings.getStateSystem() == StateSystem.EDIT_MAP_SETTINGS) {

                            mSettings.setStartLatitude(myItemizedOverlay.getGeoPoint().getLatitude());
                            mSettings.setStartLongitude(myItemizedOverlay.getGeoPoint().getLongitude());
                            Settings.showFragment(StateSystem.SETTINGS, getActivity());
                        }
                        new FactoryFragment().ActionMap(mSettings, (AppCompatActivity) getActivity(), mMapView);
                    }
                });

                Button cancelBt = (Button) view.findViewById(R.id.button_cancel_point);
                cancelBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Settings.getStateSystem() == StateSystem.EDIT_MAP) {
                            if (mSettings.getVisit().point.id == 0) {
                                Settings.showFragment(StateSystem.POINT_ADD, getActivity());
                            } else {
                                Settings.showFragment(StateSystem.POINT_EDIT, getActivity());
                            }
                        } else {
                            Settings.showFragment(StateSystem.SETTINGS, getActivity());
                        }
                    }
                });
            } else {

                view = inflater.inflate(R.layout.fragment_map, container, false);
                if (Settings.getStateSystem() == StateSystem.SIMPLE_MAP) {

                } else {
                    if (mSettings.getVisit().point.latitude == 0d || mSettings.getVisit().point.longitude == 0d) {
                        view.findViewById(R.id.text_not_geo).setVisibility(View.VISIBLE);
                    }
                }


            }
            mMapView = (MapView) view.findViewById(R.id.mapview);
            mMapView.setBuiltInZoomControls(true);
            mMapView.setClickable(true);

            if (Settings.getStateSystem() == StateSystem.EDIT_MAP || Settings.getStateSystem() == StateSystem.EDIT_MAP_SETTINGS) {
                mMapView.setBuiltInZoomControls(false);
            } else {
                mMapView.setBuiltInZoomControls(true);
            }

            mMapView.setMultiTouchControls(true);
            mMapView.setClickable(true);
            mMapView.setTileSource(TileSourceFactory.MAPNIK);
            mMapView.getController().setZoom(16);
            Drawable marker = getResources().getDrawable(android.R.drawable.star_big_on);
            assert marker != null;
            int markerWidth = marker.getIntrinsicWidth();
            int markerHeight = marker.getIntrinsicHeight();
            marker.setBounds(0, markerHeight, markerWidth, 0);
            ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getActivity().getApplicationContext());
            myItemizedOverlay = new MyItemizedOverlay(marker, resourceProxy);
            mMapView.getOverlays().add(myItemizedOverlay);
            if (Settings.getStateSystem() == StateSystem.MAP_ROUTE || Settings.getStateSystem() == StateSystem.VISIT_MAP_ROUTE) {

                mMapView.getController().setZoom(13);
                List<MRoute> routeList = Configure.getSession().getList(MRoute.class, null);
                List<MPoint> mPoints = new ArrayList<>();
                for (MRoute mRoute : routeList) {
                    MPoint point = mRoute.getPoint();
                    if (point.id == 0) continue;
                    mPoints.add(point);
                }
                List<MVisitStory> visitStoryList = Configure.getSession().getList(MVisitStory.class, null);
                Collections.reverse(visitStoryList);
                for (MVisitStory visitStory : visitStoryList) {
                    MPoint point = visitStory.getPoint();
                    if (point.id == 0) continue;
                    mPoints.add(0, point);
                }
                for (MPoint point : mPoints) {
                    GeoPoint geoPoint = new GeoPoint(point.latitude, point.longitude);
                    if (startPointRoute == null) {
                        startPointRoute = geoPoint;
                    }
                    myItemizedOverlay.addItem(geoPoint, "myPoint1");

                }
                mMapView.getController().animateTo(startPointRoute);

                if (visitStoryList != null && visitStoryList.size() > 0) {
                    PathOverlay myPath = new PathOverlay(Color.RED, getActivity());
                    List<GeoPoint> pointList = new ArrayList<>(visitStoryList.size());
                    for (MVisitStory visit : visitStoryList) {
                        pointList.add(new GeoPoint(visit.latitude, visit.longitude));
                    }
                    for (GeoPoint point : pointList) {
                        myPath.addPoint(point);
                    }
                    mMapView.getOverlays().add(myPath);
                }
                mMapView.invalidate();
            } else {
                GeoPoint myPoint1 = new GeoPoint(56.819715, 60.640623);
                if (mSettings.getVisit() != null && mSettings.getVisit().point != null && mSettings.getVisit().point.latitude != 0 && mSettings.getVisit().point.longitude != 0) {
                    myPoint1 = new GeoPoint(mSettings.getVisit().point.latitude, mSettings.getVisit().point.longitude);
                } else {
                    if (mSettings.getStartLatitude() != 0 && mSettings.getStartLongitude() != 0) {
                        myPoint1 = new GeoPoint(mSettings.getStartLatitude(), mSettings.getStartLongitude());
                    }
                }
                myItemizedOverlay.addItem(myPoint1, "myPoint1");
                mMapView.getController().animateTo(myPoint1);
                mMapView.invalidate();
            }


            if (Settings.getStateSystem() == StateSystem.EDIT_MAP || Settings.getStateSystem() == StateSystem.EDIT_MAP_SETTINGS) {
                mapper = new Mapper(myItemizedOverlay, mMapView);
                Transceiver.send(TransceiveStaticVariables.MAP, mapper);
            }
/////////////////////////////////////////////////////////////центрирование в центре экрана
            final boolean[] run = {true};
            mMapView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    run[0] = false;
                    return false;
                }
            });
            ViewTreeObserver vto = view.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (run[0]) {
                        GeoPoint myPoint1 = new GeoPoint(56.819715, 60.640623);
                        if (mSettings.getVisit() != null && mSettings.getVisit().point != null && mSettings.getVisit().point.latitude != 0 && mSettings.getVisit().point.longitude != 0) {

                            myPoint1 = new GeoPoint(mSettings.getVisit().point.latitude, mSettings.getVisit().point.longitude);

                        } else {
                            if (mSettings.getStartLatitude() != 0 && mSettings.getStartLongitude() != 0) {
                                myPoint1 = new GeoPoint(mSettings.getStartLatitude(), mSettings.getStartLongitude());
                            }
                        }
                        if (mMapView != null)
                            if (startPointRoute != null) {
                                mMapView.getController().setCenter(startPointRoute);
                            } else {
                                mMapView.getController().setCenter(myPoint1);
                            }
                    }
                }
            });
        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FMap:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.getOverlays().remove(myItemizedOverlay);
        mMapView.getTileProvider().clearTileCache();
        myItemizedOverlay = null;
        mMapView = null;
        System.gc();
    }
}
