package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.models.MVisitStoryPoint;
import com.example.user.omsk.senders.SenderPointStoryGET;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class FPointStory extends Fragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FPointStory.class);
    private ListView mListView;
    private List<MVisitStoryBase> mStoryVisitPointList;
    private Settings mSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mSettings = Settings.core();

        View mView = inflater.inflate(R.layout.fragment_point_story, container, false);
        try {
            mListView = (ListView) mView.findViewById(R.id.list_story_visits);
            TextView point_name = (TextView) mView.findViewById(R.id.point_name);
            point_name.setText(mSettings.getVisit().point.name);
            mView.findViewById(R.id.bt_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Settings.getStateSystem() == StateSystem.POINT_STORY) {
                        Settings.showFragment(StateSystem.SEARCH_POINT, getActivity());
                    } else if (Settings.getStateSystem() == StateSystem.VISIT_POINT_STORY_FOR) {
                        Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                    } else if (Settings.getStateSystem() == StateSystem.POINT_STORY_FOR_PREVIEW) {
                        Settings.showFragment(StateSystem.POINT, getActivity());
                    }
                }
            });
            mView.findViewById(R.id.bt_refresh).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mStoryVisitPointList.clear();
                    ISession ses = Configure.getSession();
                    List<MVisitStoryPoint> list = Configure.getSession().getList(MVisitStoryPoint.class, " point_id = ? ", mSettings.getVisit().point.idu);
                    for (MVisitStoryPoint mVisitStoryPoint : list) {
                        ses.delete(mVisitStoryPoint);
                    }
                    ListAdapterForStoryVisits storyVisits = new ListAdapterForStoryVisits(getActivity(), R.layout.item_list_for_story_visits, mStoryVisitPointList, false, false);
                    mListView.setAdapter(storyVisits);
                    refresh();
                }
            });
            refresh();


        } catch (Exception e) {
            log.error(e);
            Utils.sendMessage("FPointStory:" + Utils.getErrorTrace(e),  (MyApplication) getActivity().getApplication());
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;
    }

    List<MVisitStoryPoint> getListMStoryVisitPoints() {
        List<MVisitStoryPoint> list = Configure.getSession().getList(MVisitStoryPoint.class, " point_id = ? ", mSettings.getVisit().point.get_id());

        if (mSettings.distanceStoryPoint) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH, 1);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 1);
           // String df = Utils.simpleDateFormatE(c.getTime());
            Date s=c.getTime();
            final long res = s.getTime();
            List<MVisitStoryPoint> list1 = new ArrayList<>();
            for (MVisitStoryPoint pp : list) {
                if (pp.finish >res) {
                    list1.add(pp);
                }
            }
            list = list1;
        }
        return list;
    }

    private void refresh() {
        List<MVisitStoryPoint> list = getListMStoryVisitPoints();
        mStoryVisitPointList = new ArrayList<MVisitStoryBase>(list);
        if (mStoryVisitPointList.size() == 0) {
            new SenderPointStoryGET().send( getActivity(), new IActionE<View>() {
                @Override
                public void action(View v) {
                    if (mSettings.getVisit() == null || mSettings.getVisit().point == null) {
                        Toast.makeText(getActivity(), "Ошибка передачи данных", Toast.LENGTH_SHORT).show();
                    } else {
                        List<MVisitStoryPoint> tempList = getListMStoryVisitPoints();
                        mStoryVisitPointList = new ArrayList<MVisitStoryBase>(tempList);
                        actionList();
                    }
                }
            });
        } else {
            actionList();
        }
    }

    private void actionList() {
        mStoryVisitPointList.add(new MVisitStoryPoint());
        ListAdapterForStoryVisits storyVisits = new ListAdapterForStoryVisits(getActivity(), 0, mStoryVisitPointList, false, false);
        mListView.setAdapter(storyVisits);
    }
}
