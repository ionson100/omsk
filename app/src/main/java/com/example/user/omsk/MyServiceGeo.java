package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;

import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.List;

public class MyServiceGeo extends Service {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(MyServiceGeo.class);
    public MyServiceGeo() {
    }

    protected LocationManager locationManager;
    MyLocationListener listener;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        locationManager = (LocationManager) getSystemService(getBaseContext().LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
        }
        listener = new MyLocationListener();
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
        }

        if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {

        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
            }
            if (listener != null && locationManager != null) {
                locationManager.removeUpdates(listener);
            }
        } catch (Exception ex) {
            log.error(ex);
            String msg = "onDestroy geo servis" + ex.getMessage();
            Utils.sendMessage(msg,  (MyApplication) getApplication());
            Configure.getSession().insert(msg);
        }
        super.onDestroy();
    }


    public static final String GEO = "geo123";

    class MyLocationListener implements LocationListener {

        @Override
        public synchronized void onLocationChanged(Location location) {

            Intent intent = new Intent(GEO);
            List<Parcelable> list = new ArrayList<>(1);
            list.add(location);
            intent.putParcelableArrayListExtra("assa", (ArrayList<? extends Parcelable>) list);
            getBaseContext().sendBroadcast(intent);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    }
}
