package com.example.user.omsk.reloadgps;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.omsk.MainActivity;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;

public class DialogReloadGps extends DialogFragment implements LocationListener {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DialogFragment.class);

    private MainReceiverGPS mainReceiverGPS;
    private Location myLocation = null;
    private LocationManager mLocationManager;
    private double dis;
    private TextView mTextIteration;
    private Activity mActivity;
    private Settings mSettings;
    private View mView;
    private TextView mReloladTextView;
    private int iter = 0;
    private Button mButtonCancel;
    private Button mButtonOk;
    private IActionReloadGps mIActionReloadGps;

    public void setmIActionReloadGps(IActionReloadGps mIActionReloadGps) {
        this.mIActionReloadGps = mIActionReloadGps;
    }

    public void setmActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mSettings = Settings.core();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_reload_gps, null);
        ((TextView) mView.findViewById(R.id.title_dalog)).setText(getString(R.string.asdasi));
        mReloladTextView = (TextView) mView.findViewById(R.id.reload_geo);
        mButtonCancel = (Button) mView.findViewById(R.id.button_reload_geo);
        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destroy();
                dismiss();
            }
        });
        mButtonOk = (Button) mView.findViewById(R.id.button_reload_geo_save);
        mButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myLocation == null) return;
                if (mIActionReloadGps != null) {
                    mIActionReloadGps.action(dis, myLocation.getLatitude(), myLocation.getLongitude());
                }
                int ii = mSettings.getVisit().pressure;
                mSettings.getVisit().pressure = ii + 1000;
                destroy();
                dismiss();


            }
        });
        mTextIteration = (TextView) mView.findViewById(R.id.iteration_geo);

        builder.setView(mView);


        mainReceiverGPS = new MainReceiverGPS();
        getActivity().registerReceiver(mainReceiverGPS, new IntentFilter(MainReceiverGPS.INTENT));


        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSettings.useYandexGPS == false) {
            mLocationManager = (LocationManager) mActivity.getSystemService(mActivity.getBaseContext().LOCATION_SERVICE);
            if (mLocationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
                if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }
        } else {
            ((MainActivity) getActivity()).STARTSERVICEGEO();
        }

    }

    //boolean runDialog = true;

    @Override
    public void onLocationChanged(Location location) {
        activate(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }


    //int dd = 0;


    synchronized void activate(Location location) {

        try {
            // if(++dd<25) return;
            mView.findViewById(R.id.progressBar4).setVisibility(View.INVISIBLE);
            mView.findViewById(R.id.button_reload_geo_save2).setVisibility(View.VISIBLE);

            iter++;
            myLocation = location;
            dis = Utils.getDistance(location.getLatitude(), location.getLongitude(), mSettings.getVisit().point.latitude, mSettings.getVisit().point.longitude);
            mTextIteration.setText(String.valueOf(iter));
            mSettings.getVisit().distance = (int) dis;
            int i = mSettings.getVisit().pressure / 1000;
            mSettings.getVisit().pressure = 1000 * i;
            mSettings.getVisit().pressure = mSettings.getVisit().pressure + iter;
            String msg = "";

            if (dis > Utils.MAX_DIS) {
                msg = getString(R.string.ssddddd) + Utils.getStringDecimal(dis) + " м.";
            } else {

                msg = getString(R.string.sddsdd) + Utils.getStringDecimal(dis) + " м.";

                if (mIActionReloadGps != null) {
                    mIActionReloadGps.action(dis, location.getLatitude(), location.getLongitude());
                }

                int ii = mSettings.getVisit().pressure;
                mSettings.getVisit().pressure = ii + 1000;
                destroy();
                dismiss();
            }
            mReloladTextView.setText(msg);

        } catch (Exception ignored) {

            log.error(ignored);
        }

    }

    void destroy() {
        if (mLocationManager != null) {
            mLocationManager.removeUpdates(this);
            mLocationManager = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroy();
        System.gc();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // todo can not perform this action after onsaveinstancestate checkstateloss
    }

    public class MainReceiverGPS extends BroadcastReceiver {

        public static final String KEY = "key";
        public static final String INTENT = "dslfdsjfkfdsf";


        @Override
        public void onReceive(Context context, Intent intent) {

            Location location = (Location) intent.getParcelableArrayListExtra("assa").get(0);
            activate(location);

        }
    }
}
