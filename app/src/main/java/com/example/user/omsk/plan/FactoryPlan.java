package com.example.user.omsk.plan;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.example.user.omsk.orm2.Configure.getSession;


public class FactoryPlan {

    Map<String, MProduct> mProductMap;

    public FactoryPlan() {
        ISession session = Configure.getSession();
        List<MProduct> productList = session.getList(MProduct.class, null);
        mProductMap = new HashMap<>();
        for (MProduct product : productList) {
            mProductMap.put(product.idu, product);
        }
    }

    public static List<TempPlan> getTempPlanList(boolean isOld) {

        List<MProduct> mProducts = getSession().getList(MProduct.class, null);
        List<BasePlanMML> planMMLList = UtilsPlan.getListPlanMML(isOld);
        List<TempPlan> res = new ArrayList<>();
        List<MBasePlanPair> mPlanPairList = UtilsPlan.getPlanPairList(isOld);
        List<PlaneBase> planEs = UtilsPlan.getPlanE(isOld);
        if (planEs.size() != 1) {
            return res;
        }
        if (planMMLList.size() == 0) {
            return res;
        }
        PlaneBase planE = planEs.get(0);
        Map<String, List<String>> listMap = UtilsPlan.getMMLNameProducts(planMMLList);
        Set<String> stringSet = new HashSet<>();
        for (BasePlanMML planMML : planMMLList) {
            stringSet.add(planMML.mml);
        }

        Map<String, HashSet<String>> hashSetMap = new HashMap<>();
        List<MBasePlanPointProducts> mPlanPointProductses = UtilsPlan.getListMPlanPointProducts(isOld);
        Map<String, Map<String, Double>> stringMapMap = new HashMap<>();
        //////////////////////////////////////////////////////////////////////// добавление чужих
        for (final MBasePlanPointProducts mPlanPointProductse : mPlanPointProductses) {

            MProduct product = Linq.toStream(mProducts).firstOrDefault(new Predicate<MProduct>() {
                @Override
                public boolean apply(MProduct t) {
                    return t.idu.equals(mPlanPointProductse.product_id);
                }
            });
            if (product != null) {
                if (hashSetMap.containsKey(mPlanPointProductse.point_id)) {
                    hashSetMap.get(mPlanPointProductse.point_id).add(product.sku);
                } else {
                    HashSet<String> set = new HashSet<>();
                    set.add(product.sku);
                    hashSetMap.put(mPlanPointProductse.point_id, set);
                }
            }
            if (stringMapMap.containsKey(mPlanPointProductse.point_id)) {
                Map<String, Double> map = stringMapMap.get(mPlanPointProductse.point_id);
                if (map.containsKey(mPlanPointProductse.product_id)) {
                    double d = map.get(mPlanPointProductse.product_id);
                    d = d + mPlanPointProductse.amount;
                    map.put(mPlanPointProductse.product_id, d);
                } else {
                    map.put(mPlanPointProductse.product_id, mPlanPointProductse.amount);
                }
            } else {
                Map<String, Double> map = new HashMap<>();
                map.put(mPlanPointProductse.product_id, mPlanPointProductse.amount);
                stringMapMap.put(mPlanPointProductse.point_id, map);

            }
        }
        //////////////////////////////////////////////// добавления своих
        List<MOrderType> aa = getSession().getList(MOrderType.class, " index1 = 1 ");
        if (!isOld && aa.size() == 1) {
            String debt_id = aa.get(0).idu;
            ISession session = getSession();
            List<MVisitStory> stories = session.getList(MVisitStory.class, null);
            addSelfCore(mProducts, hashSetMap, stringMapMap, debt_id, stories);
            ////// добавление товаров по которым отдали долги
            stories.clear();
            List<MVisitStory> stories1 = session.getList(MVisitStory.class, null);
            for (MVisitStory visitStory : stories1) {
                for (String s : visitStory.debtList) {
                    List<MDebt> mDebtList = session.getList(MDebt.class, " visit_id = ? ", s);
                    MVisitStory d = new MVisitStory();
                    d.order_type_id = "huY";
                    d.setSalecProductCore(new ArrayList<MProduct>());
                    for (MDebt mDebt : mDebtList) {
                        if (d.point_id == null) {
                            d.point_id = mDebt.point_id;
                        }
                        MProduct product = session.get(MProduct.class, mDebt.product_id);
                        if (product == null) continue;
                        product.setAmountCore(mDebt.amount);
                        product.price = mDebt.price;
                        d.getSalesProductList().add(product);
                    }
                    stories.add(d);
                }
            }
        }

        for (Map.Entry<String, Map<String, Double>> ss : stringMapMap.entrySet()) {
            MPoint point = getSession().get(MPoint.class, ss.getKey());

            Map<String, Double> map = ss.getValue();
            if (point == null) {
                point = new MPoint();
                point.idu = ss.getKey();
                point.name = "не найдена  или делегирована";
            }

            double totalRub = 0;

            final MPoint finalPoint = point;
            MBasePlanPair pair = Linq.toStream(mPlanPairList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MBasePlanPair>() {
                @Override
                public boolean apply(MBasePlanPair t) {
                    return t.point_id.equals(finalPoint.idu);
                }
            });

            if (pair != null) {
                totalRub = pair.price;
            }
            ///////////////////////////////////////////////
            TempPlan tempPlan = new TempPlan();
            tempPlan.mTotalRub = totalRub;
            tempPlan.mPlanACB = planE.akb;
            //tempPlan.mPlanMML = mml_amount;//planE.mml_amount
            tempPlan.mPoint = point;
            tempPlan.mMml_group = planE.mml_group;
            res.add(tempPlan);

            if (hashSetMap.get(ss.getKey()) != null) {
                tempPlan.mMedium_line = hashSetMap.get(ss.getKey()).size();
            }


            for (String mmlName : stringSet) {
                List<String> prodictIdList = listMap.get(mmlName);
                double d = 0;
                double total = 0;
                for (String s1 : map.keySet()) {
                    total = total + map.get(s1);
                    if (prodictIdList.contains(s1)) {
                        d = d + map.get(s1);
                    }
                }
                tempPlan.mAllSalePachka = total;
                tempPlan.MMLMap.put(mmlName, d);
            }
        }

        Collections.sort(res, new Comparator<TempPlan>() {
            @Override
            public int compare(TempPlan lhs, TempPlan rhs) {
                return lhs.mPoint.name.compareTo(rhs.mPoint.name);
            }
        });
        return res;
    }

    private static void addSelfCore(List<MProduct> mProducts, Map<String, HashSet<String>> hashSetMap,
                                    Map<String, Map<String, Double>> stringMapMap, String debt_id, List<MVisitStory> stories) {
        for (MVisitStory story : stories) {
            if (story.debtList.size() > 0) {
                List<MProduct> list = new ArrayList<>();
                ISession session = getSession();
                for (String s : story.debtList) {
                    List<MDebt> products = session.getList(MDebt.class, " visit_id = ?", s);
                    for (MDebt pp : products) {
                        MProduct product = session.get(MProduct.class, pp.product_id);
                        if (product == null) continue;
                        product.setAmountCore(pp.amount);
                        product.price = pp.price;
                        list.add(product);
                    }
                }

                for (final MProduct sr : list) {
                    MProduct product = Linq.toStream(mProducts).firstOrDefault(new Predicate<MProduct>() {
                        @Override
                        public boolean apply(MProduct t) {
                            return t.idu.equals(sr.idu);
                        }
                    });

                    if (hashSetMap.containsKey(story.point_id)) {
                        if (product != null) {
                            hashSetMap.get(story.point_id).add(product.sku);
                        }
                    } else {
                        if (product != null) {
                            HashSet<String> set = new HashSet<>();
                            hashSetMap.put(story.point_id, set);
                        }
                    }
                }

                if (stringMapMap.containsKey(story.point_id)) {
                    Map<String, Double> map = stringMapMap.get(story.point_id);

                    for (MProduct sr : list) {
                        if (map.containsKey(sr.idu)) {
                            double d = map.get(sr.idu);
                            d = d + sr.getAmount_core();
                            map.put(sr.idu, d);
                        } else {
                            map.put(sr.idu, sr.getAmount_core());
                        }
                    }

                } else {
                    Map<String, Double> map1 = new HashMap<>();
                    stringMapMap.put(story.point_id, map1);
                    Map<String, Double> map = stringMapMap.get(story.point_id);

                    for (MProduct sr : list) {
                        if (map.containsKey(sr.idu)) {
                            double d = map.get(sr.idu);
                            d = d + sr.getAmount_core();
                            map.put(sr.idu, d);
                        } else {
                            map.put(sr.idu, sr.getAmount_core());
                        }
                    }
                }
            }

            String ss = getSession().getList(MOrderType.class, " index1 = 0 ").get(0).idu;

            if (story.order_type_id == null) continue;
            if (!story.order_type_id.equals(ss)) continue;
            if (story.getSalesProductList().size() == 0) continue;

            for (final MProduct sr : story.getSalesProductList()) {
                MProduct product = Linq.toStream(mProducts).firstOrDefault(new Predicate<MProduct>() {
                    @Override
                    public boolean apply(MProduct t) {
                        return t.idu.equals(sr.idu);
                    }
                });

                if (hashSetMap.containsKey(story.point_id)) {
                    if (product != null) {
                        hashSetMap.get(story.point_id).add(product.sku);
                    }
                } else {
                    if (product != null) {
                        HashSet<String> set = new HashSet<>();
                        set.add(product.sku);
                        hashSetMap.put(story.point_id, set);
                    }
                }
            }


            if (stringMapMap.containsKey(story.point_id)) {
                Map<String, Double> map = stringMapMap.get(story.point_id);

                for (MProduct sr : story.getSalesProductList()) {
                    if (map.containsKey(sr.idu)) {
                        double d = map.get(sr.idu);
                        d = d + sr.getAmount_core();
                        map.put(sr.idu, d);
                    } else {
                        map.put(sr.idu, sr.getAmount_core());
                    }
                }

            } else {
                Map<String, Double> map1 = new HashMap<>();
                stringMapMap.put(story.point_id, map1);
                Map<String, Double> map = stringMapMap.get(story.point_id);

                for (MProduct sr : story.getSalesProductList()) {
                    if (map.containsKey(sr.idu)) {
                        double d = map.get(sr.idu);
                        d = d + sr.getAmount_core();
                        map.put(sr.idu, d);
                    } else {
                        map.put(sr.idu, sr.getAmount_core());
                    }
                }
            }
        }
    }

    public static View getViewHeader(Context context, LinearLayout panel, boolean isOld) {

        List<BasePlanMML> planMMLList = UtilsPlan.getListPlanMML(isOld);
        if (planMMLList.size() == 0) {
            return new View(context);
        }
        Set<String> stringSet = new HashSet<>();
        for (BasePlanMML planMML : planMMLList) {
            stringSet.add(planMML.mml);
        }
        int i = Utils.getWidthPlanMML(stringSet.size(), context);
        List<String> strings = new ArrayList<>(stringSet);
        Collections.sort(strings, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareTo(rhs);
            }
        });


        if (panel.getChildCount() > 4) {
            List<View> views = new ArrayList<>();
            for (int i1 = 0; i1 < panel.getChildCount(); i1++) {
                if (i1 < 4) continue;

                views.add(panel.getChildAt(i1));
            }
            for (View view : views) {
                panel.removeView(view);
            }
        }


        for (String string : strings) {
            LayoutInflater vi;
            vi = LayoutInflater.from(context);
            TextView textView = (TextView) vi.inflate(R.layout.item_text_rotate, null);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(i, LinearLayout.LayoutParams.MATCH_PARENT);
            textView.setLayoutParams(params);
            int value = (int) context.getResources().getDimension(R.dimen.tablemagious);

            params.setMargins(value, 0, 0, 0);
            textView.setPadding(0, 5, 0, 0);
            textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            textView.setText(string);
            panel.addView(textView);
        }
        return new View(context);
    }

    public double getGroupProductAmount(boolean isOld, String group_id) {

        double res = 0d;
        List<MBasePlanPointProducts> list = UtilsPlan.getListMPlanPointProducts(isOld);
        for (MBasePlanPointProducts ss : list) {
            MProduct product = mProductMap.get(ss.product_id);
            if (product == null || product.productGroup == null) continue;
            if (product.productGroup.equals(group_id)) {
                res = res + ss.amount;
            }
        }
        //todo добавление за текуший день, проданых и отданых долгов
        if (isOld == false) {
            ISession session = Configure.getSession();
            String f = session.getList(MOrderType.class, " index1 = 1 ").get(0).idu;
            List<MVisitStory> visitStoryList =  session.getList(MVisitStory.class, null);

            for (MVisitStory visitStory : visitStoryList) {
                if (visitStory.order_type_id == null) continue;
                if (visitStory.order_type_id.equals(f)) continue;
                for (MProduct product : visitStory.getSalesProductList()) {
                    if (product.productGroup == null) continue;
                    if (product.productGroup.equals(group_id)) {
                        res = res + product.getAmount_core();
                    }
                }
            }
            for (MVisitStory visitStory : visitStoryList) {

                for (String s : visitStory.debtList) {
                    List<MDebt> mDebtList = session.getList(MDebt.class, " visit_id = ? ", s);
                    for (MDebt mDebt : mDebtList) {
                        MProduct product = mProductMap.get(mDebt.product_id);
                        if (product == null || product.productGroup == null) continue;
                        if (product.productGroup.equals(group_id)) {
                            res = res + mDebt.amount;
                        }
                    }
                }
            }
        }
        return res;
    }
}
