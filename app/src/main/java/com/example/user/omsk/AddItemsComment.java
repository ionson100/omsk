package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MComments;

import org.apache.log4j.Logger;

import java.util.List;

/**
 * Добавление комментариев на страницу просмотра характеристик точки
 */
public class AddItemsComment {
    public static org.apache.log4j.Logger log = Logger.getLogger(AddItemsComment.class);
    public static void add(View parentView, int idViewChildren, Settings settings, Activity activity) {
        LinearLayout panel = (LinearLayout) parentView.findViewById(idViewChildren);
        panel.removeAllViews();
        List<MComments> lastMComments = settings.getVisit().point.getmCommentsList();
        for (MComments lasrMComment : lastMComments) {
            LayoutInflater vi = LayoutInflater.from(activity);
            View row = vi.inflate(R.layout.item_last_comments, null);
            TextView date = (TextView) row.findViewById(R.id.last_comments_date);
            TextView message = (TextView) row.findViewById(R.id.last_comments_comment);
            date.setText(Utils.simpleDateFormatForCommentsE(lasrMComment.date));
            message.setText(lasrMComment.comment);
            panel.addView(row);
        }
    }

    public static void add(View parentView, int idViewChildren, Activity activity, List<MComments> mCommenter) {
        LinearLayout panel = (LinearLayout) parentView.findViewById(idViewChildren);
        for (MComments lastMComment : mCommenter) {
            LayoutInflater vi = LayoutInflater.from(activity);
            View row = vi.inflate(R.layout.item_last_comments, null);
            TextView date = (TextView) row.findViewById(R.id.last_comments_date);
            TextView message = (TextView) row.findViewById(R.id.last_comments_comment);
            date.setText(Utils.simpleDateFormatForCommentsE(lastMComment.date));
            message.setText(lastMComment.comment);
            panel.addView(row);
        }
    }
}
