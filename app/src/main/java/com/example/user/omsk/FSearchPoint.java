package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.dialogs.DialogAlertPoint;
import com.example.user.omsk.dialogs.DialogContextPointMenu;
import com.example.user.omsk.dialogs.DialogSelectVisit;
import com.example.user.omsk.dialogs.DialogShowPointDebt;
import com.example.user.omsk.fotocontrol.MyViewFotoPoint;
import com.example.user.omsk.models.MAgent;
import com.example.user.omsk.models.MAlertPoint;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class FSearchPoint extends Fragment implements IVoicSearching {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FSearchPoint.class);
    ListAdapterForSearchPoints adapter;
    List<MRoute> adapterRoute;
    private TextView mTextView;
    private List<MPoint> mPointList;
    private ListView mListContragent;
    private View mView;
    private Settings mSettings;
    private Button mButtonUpdateRoute;
    private EditText mTextSearch;
    private LinearLayout mButtonLayout;
    private boolean isShowVisit;
    private MVisit currentVisit;
    Set<String> pointIdDebt = Collections.EMPTY_SET;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_point, container, false);
        mSettings = Settings.core();
        pointIdDebt = new HashSet<>();
        if (mSettings.showDebtPoints) {
            Map<String, MPoint> pointMap = Utils.getMapDebtPoint();
            for (MPoint point : pointMap.values()) {
                pointIdDebt.add(point.idu);
            }
        }
        if (mSettings.getVisit() == null) {
            mSettings.setVisit(new MVisit());
        }
        try {
            ////////////////////////////////////  проверка на контрагента
            Utils.checkAgent(mSettings);
            mPointList = Configure.getSession().getList(MPoint.class, " 1 = 1 order by name");

            ((TextView) mView.findViewById(R.id.point_count)).setText("Итого: " + String.valueOf(mPointList.size()));

            mListContragent = (ListView) mView.findViewById(R.id.list_contragent);
            mButtonLayout = (LinearLayout) mView.findViewById(R.id.panel_buttons);
            mTextView = (TextView) mView.findViewById(R.id.tb_select_contragent);

            mTextSearch = (EditText) mView.findViewById(R.id.text_search);
            mTextSearch.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);

            mButtonUpdateRoute = (Button) mView.findViewById(R.id.button_update_route);
            mButtonUpdateRoute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new UpdateRouteFromServer().update( getActivity(), new IActionE<View>() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void action(View v) {
                            showRoute();
                        }
                    });
                }
            });


            mView.findViewById(R.id.button_stock_colaps).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {

                    showAll();
                    mButtonUpdateRoute.setVisibility(View.GONE);
                    mTextSearch.setVisibility(View.VISIBLE);
                }
            });

            mView.findViewById(R.id.button_marshrut_core).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {

                    showRoute();
                    mTextSearch.setVisibility(View.GONE);
                    mButtonUpdateRoute.setVisibility(View.VISIBLE);
                }
            });

            mView.findViewById(R.id.buton_select_point).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPoint();
                }
            });

            mView.findViewById(R.id.button_point_story).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSettings.getVisit().point == null) {
                        Toast.makeText(getActivity(), getString(R.string.error_not_select_point), Toast.LENGTH_SHORT).show();
                    } else {
                        mButtonUpdateRoute.setVisibility(View.GONE);
                        mTextSearch.setVisibility(View.VISIBLE);
                        Settings.showFragment(StateSystem.POINT_STORY, getActivity());
                    }
                }
            });


            mView.findViewById(R.id.buton_start_visit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_POINT) return;
                    if (!InspectionVisit.inspection(mSettings, getActivity())) {
                        return;
                    }
                    goVisit();
                }
            });
            mTextSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (mSettings.getStateSearchPoint() == StateSearchPoint.name) {
                        showSearchName(s.toString());
                    } else if (mSettings.getStateSearchPoint() == StateSearchPoint.address) {
                        showSearchAddress(s.toString());
                    } else if (mSettings.getStateSearchPoint() == StateSearchPoint.conragent) {
                        showSearchContragent(s.toString());
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() > 0) {
                        mTextSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    } else {
                        //Assign your image again to the view, otherwise it will always be gone even if the text is 0 again.
                        mTextSearch.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
                    }
                }
            });
            createListABC();
            createListPoints();


            if (mSettings.getVisit().point != null) {
                mButtonLayout.setVisibility(View.VISIBLE);
                mTextView.setText(mSettings.getVisit().point.name);
                currentVisit = mSettings.getVisit();
            }

            mView.findViewById(R.id.button_marshrut).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activateShowVisites();
                }
            });
            mView.findViewById(R.id.bt_context_menu).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogContextPointMenu d = new DialogContextPointMenu();
                    d.show(getActivity().getSupportFragmentManager(), "sdsdsfsdfs");
                }
            });

            mListContragent.setOnCreateContextMenuListener(this);
            NavigationView mNavigationView = ((MainActivity) getActivity()).NAVIGATE_MENU();


            new SpeechSearchPoint(mView, getActivity()).activate();

            MyViewFotoPoint.deleteTempFiles();

            FactoryMenu factoryMenu = new FactoryMenu(mNavigationView, getActivity());
            factoryMenu.activatePoint();
            factoryMenu.setMenuAction((FactoryMenu.IMenuAction) getActivity());


        } catch (Exception e) {
            log.error(e);
            Utils.sendMessage("FSearchPoints:" + Utils.getErrorTrace(e), (MyApplication) getActivity().getApplication());
            Settings.showFragment(StateSystem.HOME, getActivity());
        }
        return mView;
    }

    void activateShowVisites() {

        isShowVisit = true;

        showMyVisits();
        mButtonUpdateRoute.setVisibility(View.GONE);
        mTextSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SettingsGpsMap.core().stateSystem == StateSystem.MAP_ROUTE_STORY) {
            SettingsGpsMap.core().stateSystem = StateSystem.HOME;
            SettingsGpsMap.save();
            activateShowVisites();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void showDebtPoints() {
        isShowVisit = false;
        Map<String, MPoint> pointMap = Utils.getMapDebtPoint();

        List<MPoint> tt = new ArrayList<>(pointMap.values());
        Collections.sort(tt, new Comparator<MPoint>() {
            @Override
            public int compare(MPoint lhs, MPoint rhs) {
                return Double.compare(lhs.debt, rhs.debt);
            }
        });
        if (tt.size() > 0) {
            tt.add(null);
        }
        activateList(tt, true, null);
    }


    public boolean isShowVisit() {

        return isShowVisit;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showSearchContragent(String s) {
        isShowVisit = false;
        if (s.equals("")) {
            showAll();

        } else {
            final List<MPoint> points = new ArrayList<>();
            for (MPoint point : mPointList) {
                MAgent mAgent = point.getContragent(Configure.getSession());
                if (mAgent == null || mAgent.name == null) continue;
                if (point.getContragent(Configure.getSession()).name.toLowerCase(new Locale("ru", "RU")).contains(s.toString().toLowerCase(new Locale("ru", "RU")))) {
                    points.add(point);
                }
            }

            activateList(points, false, null);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showSearchAddress(String s) {
        isShowVisit = false;
        if (s.equals("")) {
            showAll();

        } else {
            final List<MPoint> points = new ArrayList<>();
            for (MPoint point : mPointList) {
                if (point.address == null) continue;
                if (point.address.toLowerCase(new Locale("ru", "RU")).contains(s.toString().toLowerCase(new Locale("ru", "RU")))) {
                    points.add(point);
                }
            }

            activateList(points, false, null);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showRoute() {
        isShowVisit = false;
        try {
            adapter = null;
            adapterRoute = Configure.getSession().getList(MRoute.class, null);
            ListAdapterForRoute routes = new ListAdapterForRoute(getActivity(), R.layout.item_list_point, adapterRoute, pointIdDebt);
            mListContragent.setAdapter(routes);
        } catch (Exception e) {
            log.error(e);
            Toast.makeText(getActivity(), "Ошибка приложения.", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showSearchName(String s) {
        isShowVisit = false;
        if (s.equals("")) {
            showAll();

        } else {
            final List<MPoint> points = new ArrayList<>();
            for (MPoint point : mPointList) {
                if (point.name == null) continue;
                if (point.name.toLowerCase(new Locale("ru", "RU")).contains(s.toString().toLowerCase(new Locale("ru", "RU")))) {
                    points.add(point);
                }
            }
            activateList(points, false,s);
        }
    }

    private void showMyVisits() {
        isShowVisit = true;
        adapterRoute = null;
        adapter = null;// отключаем контекстное меню для показа если маршруты
        List<MVisitStory> stories = Configure.getSession().getList(MVisitStory.class, null);
        Collections.sort(stories, new Comparator<MVisitStory>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MVisitStory lhs, MVisitStory rhs) {
                return Integer.compare(lhs.start,rhs.start);
            }
        });
        stories.add(null);
        ListAdapterForVisitStoryForCurrentDay strings = new ListAdapterForVisitStoryForCurrentDay(getActivity(), R.layout.item_list_visit_story, stories, getActivity());
        mListContragent.setAdapter(strings);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showSearchFirstChar(String s) {
        isShowVisit = false;
        final List<MPoint> points = new ArrayList<>();
        for (MPoint point : mPointList) {
            if (point.name.trim().toLowerCase().startsWith(s.trim().toLowerCase())) {
                points.add(point);
            }
        }

        activateList(points, false, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createListPoints() {

        showAll();
        mListContragent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null) {
                    mSettings.getVisit().point = (MPoint) view.getTag();
                    mTextView.setText(mSettings.getVisit().point.name);

                    mButtonLayout.setVisibility(View.VISIBLE);
                    Settings.save();

                  //  Object ss = mListContragent.getAdapter();
                    if (mListContragent.getAdapter() instanceof IPainter) {
                        ((IPainter) mListContragent.getAdapter()).notifyAddin(position);
                    }

                }
            }
        });
    }

    private void createListABC() {

        MyListViewAbc myListViewAbc = (MyListViewAbc) mView.findViewById(R.id.listView_alphavit);
        myListViewAbc.setIAction(new IActionE<String>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void action(String o) {
                showSearchFirstChar(o);
                mButtonUpdateRoute.setVisibility(View.GONE);
                mTextSearch.setVisibility(View.VISIBLE);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void activateList(List<MPoint> mPoints, boolean isDebt, String s) {
        adapterRoute = null;
        adapter = new ListAdapterForSearchPoints(getActivity(), R.layout.item_list_point, mPoints, isDebt, mSettings, this.pointIdDebt,s);
        mListContragent.setAdapter(adapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showAll() {
        isShowVisit = false;
        activateList(mPointList, false,null);
    }


    ///////////////////////////////
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        View vs = getActivity().getLayoutInflater().inflate(R.layout.menu_view_title, null);
        menu.setHeaderView(vs);
        if (false) {


            if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_POINT) return;
            if (mSettings.getVisit().point == null) {
                menu.add(1, FPoint.MENU_ADD_POINT, 0, getString(R.string.create_point_menu));
                return;
            }
            if (mSettings.getVisit().point.fuflo) {
                return;
            }
            if (mSettings.getVisit().point.getAlertPoint() == null) {
                menu.add(1, FPoint.MENU_ADD_ALERT, 0, getString(R.string.alert_create));
            } else {
                menu.add(1, FPoint.MENU_EDIT_ALERT, 0, getString(R.string.alert_edit));
            }
            menu.add(1, FPoint.MENU_ADD_POINT, 0, getString(R.string.create_point_menu));
            if (mSettings.getVisit().point != null) {

                menu.add(1, FPoint.MENU_EDIT_POINT, 0, getString(R.string.edit_point_menu));

            }
        } else {
            AdapterView.AdapterContextMenuInfo aMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;

            final int position = aMenuInfo.position;
            MPoint point = null;

            if (adapter != null && adapterRoute == null) {
                point = adapter.getItem(position);
            } else if (adapter == null && adapterRoute != null) {
                point = adapterRoute.get(position).getPoint();
            } else {
                return;
            }
            if (point != null) {
                mSettings.getVisit().point = point;
                mTextView.setText(mSettings.getVisit().point.name);
                Settings.save();
                Object ss = mListContragent.getAdapter();
                if (mListContragent.getAdapter() instanceof IPainter) {
                    ((IPainter) mListContragent.getAdapter()).notifyAddin(position);
                }


            } else {
                return;
            }
            if (!mSettings.getVisit().point.fuflo) {
                MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(R.menu.point_context_menu, menu);
                MAlertPoint alertPoint = point.getAlertPoint();
                if (alertPoint == null) {
                    menu.findItem(R.id.con_edit_alert).setVisible(false);
                } else {
                    menu.findItem(R.id.con_add_alert).setVisible(false);
                }
            }
        }
    }


    void painterAlert(MPoint mPoint, int action) {
        for (int i = 0; i < mListContragent.getChildCount(); i++) {
            MPoint p = (MPoint) mListContragent.getChildAt(i).getTag();
            if (p != null && p.idu.equals(mPoint.idu)) {
                View v = mListContragent.getChildAt(i);
                ImageView imageView = (ImageView) v.findViewById(R.id.image_alert);
                if (action == DialogAlertPoint.CREATE) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo cmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        View ss = cmi.targetView;

        final View info = ss.findViewById(R.id.image_alert);

        switch (item.getItemId()) {
            case FPoint.MENU_ADD_POINT: {
                mSettings.getVisit().point = new MPoint();
                Settings.showFragment(StateSystem.POINT_ADD_SEARCH, getActivity());
                return true;
            }
            case FPoint.MENU_EDIT_POINT: {
                Settings.showFragment(StateSystem.POINT_EDIT_SEARCH, getActivity());
                return true;
            }
            case FPoint.MENU_ADD_AGENT: {
                if (mSettings.getVisit().point.agent_id == null) {
                    Settings.showFragment(StateSystem.CONTRAGENT_ADD_SEARCH, getActivity());
                    mSettings.getVisit().point.setAgent(new MAgent());
                }
                return true;
            }
            case FPoint.MENU_EDIT_AGENT: {
                if (mSettings.getVisit().point.agent_id != null) {
                    Settings.showFragment(StateSystem.CONTRAGENT_EDIT_SEARCH, getActivity());
                }
                return true;
            }
            case FPoint.MENU_ADD_ALERT: {

                DialogAlertPoint dialogAlertPoint = new DialogAlertPoint();
                dialogAlertPoint.setPoint(mSettings.getVisit().point).setAction(new DialogAlertPoint.IActionAlertDeleteCreate() {
                    @Override
                    public void Action(MPoint mPoint, int action) {


                    }
                });
                dialogAlertPoint.show(getActivity().getSupportFragmentManager(), "3443");
                return true;
            }
            case FPoint.MENU_EDIT_ALERT: {
                DialogAlertPoint dialogAlertPoint = new DialogAlertPoint();
                dialogAlertPoint.setPoint(mSettings.getVisit().point).setAction(new DialogAlertPoint.IActionAlertDeleteCreate() {
                    @Override
                    public void Action(MPoint mPoint, int action) {
                        try {
                            if (action == DialogAlertPoint.CREATE) {
                                info.setVisibility(View.VISIBLE);
                            } else {
                                info.setVisibility(View.INVISIBLE);
                            }
                        } catch (Exception ignored) {

                            log.error(ignored);
                        }
                    }
                });
                dialogAlertPoint.show(getActivity().getSupportFragmentManager(), "3434");
                return true;
            }
            case R.id.con_add_alert: {
                DialogAlertPoint dialogAlertPoint = new DialogAlertPoint();
                dialogAlertPoint.setPoint(mSettings.getVisit().point).setAction(new DialogAlertPoint.IActionAlertDeleteCreate() {
                    @Override
                    public void Action(MPoint mPoint, int action) {
                        try {
                            if (action == DialogAlertPoint.CREATE) {
                                info.setVisibility(View.VISIBLE);
                            } else {
                                info.setVisibility(View.GONE);
                            }
                        } catch (Exception ignored) {

                            log.error(ignored);
                        }
                    }
                });
                dialogAlertPoint.show(getActivity().getSupportFragmentManager(), "3443");
                return true;

            }
            case R.id.con_edit_alert: {
                DialogAlertPoint dialogAlertPoint = new DialogAlertPoint();
                dialogAlertPoint.setPoint(mSettings.getVisit().point).setAction(new DialogAlertPoint.IActionAlertDeleteCreate() {
                    @Override
                    public void Action(MPoint mPoint, int action) {
                        try {
                            if (action == DialogAlertPoint.CREATE) {
                                info.setVisibility(View.VISIBLE);
                            } else {
                                info.setVisibility(View.GONE);
                            }
                        } catch (Exception ignored) {

                            log.error(ignored);
                        }
                    }
                });
                dialogAlertPoint.show(getActivity().getSupportFragmentManager(), "3434");
                return true;

            }
            case R.id.con_add_point: {
                mSettings.getVisit().point = new MPoint();
                Settings.showFragment(StateSystem.POINT_ADD_SEARCH, getActivity());
                return true;
            }
            case R.id.con_edit_point: {
                Settings.showFragment(StateSystem.POINT_EDIT_SEARCH, getActivity());
                return true;
            }
            case R.id.con_visit: {
                if (InspectionVisit.inspection(mSettings, getActivity())) {
                    goVisit();
                }

                return true;
            }
            case R.id.con_show_point: {
                showPoint();
                return true;
            }
            case R.id.con_show_point_debt: {
                showPointDebt();
                return true;
            }
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    // отработка нажати контекстноо меню - история точки по задолжности
    private void showPointDebt() {
        if (mSettings.getVisit().point == null) {
            Toast.makeText(getActivity(), getString(R.string.error_not_select_point), Toast.LENGTH_SHORT).show();
        } else {
            List<MDebt> mDebtList = Utils.getDebtForPoint(mSettings.getVisit().point.idu);
            if (mDebtList.size() == 0) {
                Toast.makeText(getActivity(), R.string.qweqew, Toast.LENGTH_LONG).show();
                return;
            }
            try {
                DialogShowPointDebt pointDebt = new DialogShowPointDebt();
                pointDebt.setDebtsPoint(mDebtList, mSettings.getVisit().point);
                pointDebt.show(getActivity().getSupportFragmentManager(), "sdjdkjj");
            } catch (Exception ignored) {
                log.error(ignored);
            }
        }
    }


    void showPoint() {
        if (mSettings.getVisit().point == null) {
            Toast.makeText(getActivity(), getString(R.string.error_not_select_point), Toast.LENGTH_SHORT).show();
        } else if (mSettings.getVisit().point.fuflo) {
            Toast.makeText(getActivity(), getString(R.string.point_fiflo), Toast.LENGTH_SHORT).show();
        } else {
            Settings.showFragment(StateSystem.POINT, getActivity());
        }
    }


    void goVisit() {

        if (mSettings.getVisit().point == null) {
            Toast.makeText(getActivity(), getString(R.string.error_not_select_point), Toast.LENGTH_SHORT).show();
        } else if (mSettings.getVisit().point.fuflo) {
            Toast.makeText(getActivity(), getString(R.string.point_fiflo), Toast.LENGTH_SHORT).show();
        } else {
            List<MVisitStory> stories = Configure.getSession().getList(MVisitStory.class, " point_id = '" + mSettings.getVisit().point.idu + "\'");
            if (stories.size() >= 1) {
                DialogSelectVisit dialogSelectVisit = new DialogSelectVisit();
                dialogSelectVisit.initDialog(stories, new DialogSelectVisit.IActionSelectedVisit() {
                    @Override
                    public void action(MVisitStory mVisitStory) {
                        MVisit vs = mVisitStory.getMVisit(getActivity());
                        if(vs.getStart()==0){
                            vs.setStart(Utils.dateToInt(new Date()));
                        }else {
                            vs.secondary = true;
                        }
                        mSettings.setVisit(vs);
                        Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                    }
                });
                dialogSelectVisit.show(getActivity().getSupportFragmentManager(), "sdkjjdddsd");
                return;
            } else {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void voicSearching(String o) {
        String s = o.trim().toUpperCase();
        List<MPoint> mPoints = new ArrayList<>();
        for (MPoint point : mPointList) {
            if (point.name.trim().toUpperCase().contains(s)) {
                mPoints.add(point);
            }
        }
        activateList(mPoints, false, s);
    }
}
