package com.example.user.omsk.route;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MPoint;

public interface IAddPointRoute {
    void add(MPoint mPoint, boolean b);
}
