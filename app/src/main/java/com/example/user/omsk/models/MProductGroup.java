package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// группы продуктов
@Table("product_group")
public class MProductGroup implements IUsingGuidId, Serializable {

    @PrimaryKey("id")
    public transient int id;

    @SerializedName("id")
    @Column("idu")
    public String idu;

    public boolean isSelect;

    @SerializedName("parent_id")
    @Column("parent_id")
    public String parent_id;

    @SerializedName("name")
    @Column("name")
    public String name;

    private transient int padding = 0;

    public MProductGroup() {
        this.idu = Utils.getUuid();
    }

    public String get_id() {
        return idu;
    }

    public String getName() {
        return name;
    }

    public int getPadding() {

        return padding;
    }

    public void setPadding(int value) {
        padding = value;
    }
}
