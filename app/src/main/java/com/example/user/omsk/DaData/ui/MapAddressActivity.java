package com.example.user.omsk.DaData.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.Toast;

import com.example.user.omsk.Mapper;
import com.example.user.omsk.MyItemizedOverlay;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;

public class MapAddressActivity extends AppCompatActivity {

    private double lat1, lon1;
    private Mapper mapper;
    private Settings mSettings;
    private MapView mMapView;
    private MyItemizedOverlay myItemizedOverlay = null;
    private GeoPoint startPointRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_addres);

        Intent intent = getIntent();
        String lat = intent.getStringExtra("latitude");
        String lon = intent.getStringExtra("longitude");
        if (lat == null || lon == null) finish();


        if(lat!=null){
            lat1 = Double.parseDouble(lat);
        }
        if(lon!=null){
            lon1 = Double.parseDouble(lon);
        }



        mSettings = Settings.core();

        Button btSave = (Button) findViewById(R.id.button_sabe_point);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (myItemizedOverlay.getGeoPoint() != null) {
                    Intent intent = new Intent();
                    intent.putExtra("latitude", myItemizedOverlay.getGeoPoint().getLatitude());
                    intent.putExtra("longitude", myItemizedOverlay.getGeoPoint().getLongitude());
                    setResult(RESULT_OK, intent);
                    finish();
                }


            }
        });

        Button cancelBt = (Button) findViewById(R.id.button_cancel_point);
        cancelBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });


        mMapView = (MapView) findViewById(R.id.mapview);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setClickable(true);


        mMapView.setBuiltInZoomControls(false);

        mMapView.setMultiTouchControls(true);
        mMapView.setClickable(true);
        mMapView.setTileSource(TileSourceFactory.MAPNIK);
        mMapView.getController().setZoom(16);
        Drawable marker = getResources().getDrawable(android.R.drawable.star_big_on);
        assert marker != null;
        int markerWidth = marker.getIntrinsicWidth();
        int markerHeight = marker.getIntrinsicHeight();
        marker.setBounds(0, markerHeight, markerWidth, 0);
        ResourceProxy resourceProxy = new DefaultResourceProxyImpl(MapAddressActivity.this.getApplicationContext());
        myItemizedOverlay = new MyItemizedOverlay(marker, resourceProxy);
        mMapView.getOverlays().add(myItemizedOverlay);

        GeoPoint myPoint1 = new GeoPoint(lat1, lon1);

        myItemizedOverlay.addItem(myPoint1, "myPoint1");
        mMapView.getController().animateTo(myPoint1);
        mMapView.invalidate();


        mapper = new Mapper(myItemizedOverlay, mMapView);
        //Transceiver.send(TransceiveStaticVariables.MAP, mapper);

/////////////////////////////////////////////////////////////центрирование в центре экрана
        final boolean[] run = {true};
        mMapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                run[0] = false;
                return false;
            }
        });
        ViewTreeObserver vto = mMapView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (run[0]) {
                    GeoPoint myPoint2 = new GeoPoint(lat1, lon1);

                    if (mMapView != null)
                        if (startPointRoute != null) {
                            mMapView.getController().setCenter(startPointRoute);
                        } else {
                            mMapView.getController().setCenter(myPoint2);
                        }
                }
            }
        });


    }


    private float x = -1;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        int actionType = ev.getAction();
        switch (actionType) {
            case MotionEvent.ACTION_DOWN: {// нажатие
                x = ev.getX();
                break;
            }
            case MotionEvent.ACTION_UP: {// отпускание
                if ((x < (ev.getX() + 10) && x > (ev.getX() - 10)) && Utils.isPointInsideToView(ev.getX(), ev.getY(), mapper.mapView)) {
                    Toast.makeText(MapAddressActivity.this, "Фиксация", Toast.LENGTH_SHORT).show();
                    Projection proj = mapper.mapView.getProjection();
                    GeoPoint loc = (GeoPoint) proj.fromPixels((int) ev.getX(), (int) ev.getY());//-100
                    mapper.myItemizedOverlay.removeAll();
                    GeoPoint myPoint1 = new GeoPoint(((double) loc.getLatitudeE6()) / 1000000, ((double) loc.getLongitudeE6()) / 1000000);
                    mapper.myItemizedOverlay.addItem(myPoint1, "myPoint1");
                    mapper.mapView.getController().animateTo(myPoint1);
                    mapper.mapView.invalidate();
                }
                break;
            }
            default:{
                break;
            }
        }
        return super.dispatchTouchEvent(ev);
    }
}
