package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.dialogs.DialogDataSelf;
import com.example.user.omsk.dialogs.DialogProductForSale2;
import com.example.user.omsk.models.MFreePresentTYpes;
import com.example.user.omsk.models.MPriority;
import com.example.user.omsk.models.MPriorityProduct;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MProductGroup;
import com.example.user.omsk.action.SettingsStorageAction;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.plan.BasePlanMML;
import com.example.user.omsk.plan.GroupDouble;
import com.example.user.omsk.plan.PlanE;
import com.example.user.omsk.plan.UtilsPlan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static com.example.user.omsk.Settings.getStateSystem;
import static com.example.user.omsk.StateSystem.SHOW_STOCK_SIMPLE;
import static com.example.user.omsk.linq2.Linq.toStream;
import static com.example.user.omsk.orm2.Configure.getSession;

//import static com.example.user.omsk.StateSystem.STATIONARY;

public class FStock extends Fragment implements ListAdapterStock.OnRowDataClick, ListAdapterStock.OnRowNavigateClick, IVoicSearching {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FStock.class);
    List<TempIonMenu> tempIOnMenus = new ArrayList<>();
    private EditText mEditText;
    private ListAdapterStock listAdapterStock;
    private List<Object> curObj = new ArrayList<>();
    private List<MFreePresentTYpes> mFreePresentTYpes;
    private Spinner mSpinner;
    private NavigationView mNavigationView;
    private List<MProduct> mProductsPresent = new ArrayList<>();
    private Settings mSettings;
    private View mView;

    ListView mListView;
    private List<MProduct> mGlobalProducts;
    private List<MProductGroup> mGlobalProductGroups;
    private final List<CheckBox> mPrioryCheckBoxList = new ArrayList<>();

    public FStock() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mSettings = Settings.core();
        mView = inflater.inflate(R.layout.fragment_stock2, container, false);

        try {
            mNavigationView = ((MainActivity) getActivity()).NAVIGATE_MENU();
            mFreePresentTYpes = Configure.getSession().getList(MFreePresentTYpes.class, " is_active = 0 ");

            mListView = (ListView) mView.findViewById(R.id.list_stock);


            final List<MProduct> list = getSession().getList(MProduct.class, null);
            mGlobalProducts = toStream(list).where(new Predicate<MProduct>() {
                @Override
                public boolean apply(MProduct t) {
                    return !t.isArchive;
                }
            }).toList();

            Utils.pricecometr(mGlobalProducts, mSettings);
            ///////////////////////////////////////////////////////////////////////////////////////удалаление продуктов с нулевой ценой
            List<MProduct> mProductsForDelete = new ArrayList<>();
            for (MProduct gp : mGlobalProducts) {
                if (gp.price == 0) {
                    mProductsForDelete.add(gp);
                }
            }
            mGlobalProducts.removeAll(mProductsForDelete);
            if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {

            }

            ((TextView) mView.findViewById(R.id.point_count)).setText("Итого: " + String.valueOf(mGlobalProducts.size()));
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            mGlobalProductGroups = getSession().getList(MProductGroup.class, null);
            createListABC();
            createMenu();
            createSearchPanel();
            if (mSettings.methodShowStock) {
                showStockSelf();
            } else {
                showAll();
            }
            activateButton();
            new SpeechSearchProduct(mView, getActivity()).activate();

            if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
                mEditText.setVisibility(View.GONE);
                mView.findViewById(R.id.image_microphone_product).setVisibility(View.GONE);
                mView.findViewById(R.id.button_stock_colaps).setVisibility(View.GONE);
                mView.findViewById(R.id.listView_alphavit).setVisibility(View.GONE);

               // List<MProduct> productList = new ArrayList<>();

                mGlobalProducts = new ArrayList<>();

                for (String product : SettingsStorageAction.core().mAction_new.currentDeripaskeJs.products) {
                    MProduct p = Configure.getSession().get(MProduct.class, product);
                    if (p != null) {
                        for (MProduct prodo : mSettings.getVisit().selectProductForActions) {
                            if (prodo.idu.equals(p.idu)) {
                                p.amount_action_max = prodo.getAmount_core();
                            }
                        }
                        if (SettingsStorageAction.core().mAction_new.currentDeripaskeJs.map.containsKey(p.idu)) {
                            p.amount_action_max = SettingsStorageAction.core().mAction_new.currentDeripaskeJs.map.get(p.idu);//p.amount_action_max+
                        }
                        mGlobalProducts.add(p);
                    }
                }
                // TODO: 17.11.2016   получить продкуты для отображения
                Utils.pricecometr(mGlobalProducts, mSettings);
                List<Object> objectList = new ArrayList<Object>(mGlobalProducts);
                activateList(objectList, false);
                TextView textView = (TextView) mView.findViewById(R.id.max_amount_action);
                textView.setVisibility(View.VISIBLE);
                String ss = SettingsStorageAction.core().mAction_new.idu;
                double max = SettingsStorageAction.core().mAction_new.currentDeripaskeJs.amount_max;
                String ss1q = String.valueOf(max);
                textView.setText(getString(R.string.max_bonus) + ss1q + getString(R.string.sht));
            }
           // int sdd = Settings.getStateSystem();

            if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
                View dd = mView.findViewById(R.id.panel_spinner);
                dd.setVisibility(View.VISIBLE);
                mSpinner = (Spinner) mView.findViewById(R.id.spiner_present_type);
                List<MFreePresentTYpes> d = new ArrayList<>(mFreePresentTYpes);
                MFreePresentTYpes tYpes = new MFreePresentTYpes();
                tYpes.name = getString(R.string.select_actions);
                d.add(0, tYpes);
                ListAdapterForSnipperPresentType adapter = new ListAdapterForSnipperPresentType(getContext(), R.layout.simple_spinner_item, d);
                mSpinner.setAdapter(adapter);
                mSpinner.setSelection(0);


                mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        String rule = "";
                        List<MFreePresentTYpes> list1 = new ArrayList<>(mFreePresentTYpes);
                        list1.add(0, new MFreePresentTYpes());
                        rule = list1.get(position).idu;
                        mProductsPresent = new ArrayList<>();

                        for (MProduct product : mSettings.getVisit().selectProductForPresent) {
                            if (product.rule_id.equals(rule)) {
                                product.rule_id = rule;
                                mProductsPresent.add(product);
                            }
                        }
                        for (MProduct p1 : mGlobalProducts) {
                            p1.isSelect = false;
                            p1.setAmountCore(0);
                            p1.rule_id = null;
                            for (MProduct p2 : mProductsPresent) {
                                if (p1.idu.equals(p2.idu)) {
                                    p1.rule_id = rule;
                                    p1.isSelect = true;
                                    p1.setAmountCore(p2.getAmount_core());
                                }
                            }
                        }
                        activateList(curObj, true);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                    }
                });


            }

        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FStock:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;
    }

    public void voicSearching(String o) {

        String s = o.trim().toUpperCase();
        List<MProduct> list1 = new ArrayList<>();
        for (MProduct mGlobalProduct : mGlobalProducts) {
            if (mGlobalProduct.name.trim().toUpperCase().contains(s)) {
                list1.add(mGlobalProduct);
            }
        }
        activateList(new ArrayList<Object>(list1), false);
    }


    private void bustilat() {
        if (Settings.getStateSystem() != StateSystem.VISIT_STOCK_PRESENT) return;
        for (int i = 0; i < mListView.getChildCount(); i++) {
            View view = mListView.getChildAt(i);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox_d);
            TextView amount = (TextView) view.findViewById(R.id.amount_d);
            TextView itogo = (TextView) view.findViewById(R.id.itogo_d);
            Object o = view.getTag();
            if (o instanceof MProduct) {
                // checkBox.setChecked(false);
                checkBox.setChecked(false);
                amount.setText("");
                itogo.setText("");
                MProduct p = (MProduct) o;
                for (MProduct product : mProductsPresent) {
                    if (p.idu.equals(product.idu)) {
                        // checkBox.setChecked(true);
                        checkBox.setChecked(true);
                        p.setAmountCore(product.getAmount_core());
                        p.price = product.price;
                        amount.setText(Utils.getStringDecimal(p.getAmount_core()));
                        itogo.setText(Utils.getStringDecimal(p.price * p.getAmount_core()));
                    }
                }
            }
        }
    }

    public void menu_p3() {
        List<MProduct> list = getSession().getList(MProduct.class, null);
        List<MProduct> list2 = toStream(list).where(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.isArchive;
            }
        }).toList();
        Utils.pricecometr(list2, mSettings);
        activateList(new ArrayList<Object>(list2), true);
    }

    public void menu_p2() {
        List<MProduct> list = getSession().getList(MProduct.class, null);
        Utils.pricecometr(list, mSettings);
        List<MProduct> list2 = toStream(list).where(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.price == 0;
            }
        }).toList();
        activateList(new ArrayList<Object>(list2), true);
    }

    private void createMenu() {

        FactoryMenu factoryMenu = new FactoryMenu(mNavigationView, getActivity());
        factoryMenu.activateProduct();
        factoryMenu.setMenuAction((FactoryMenu.IMenuAction) getActivity());


        List<MPriority> priorityList = getSession().getList(MPriority.class, null);
        Collections.sort(priorityList, new Comparator<MPriority>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MPriority lhs, MPriority rhs) {
                return Integer.compare(lhs.value, rhs.value);
            }
        });

        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {

            mView.findViewById(R.id.table_price2).setVisibility(View.GONE);
            return;
        }

        int order = 100;
        mNavigationView.getMenu().clear();// очищение меню, возможно задваивание
        factoryMenu.getProdictPanelAddin().removeAllViews();
        for (final MPriority mPriority : priorityList) {
            if (mPriority.value == 2) {
                List<BasePlanMML> planMMLList = UtilsPlan.getListPlanMML(false);
                Map<String, List<String>> listMap = UtilsPlan.getMMLNameProducts(planMMLList);
                if (listMap.size() == 0) {

                    TempIonMenu m = new TempIonMenu();
                    m.name = mPriority.name;
                    m.image = R.drawable.menu_preority;
                    m.iActionE = new IActionE<Object>() {
                        @Override
                        public void action(Object o) {
                            List<MPriorityProduct> list = getSession().getList(MPriorityProduct.class, " priority_id = ? ", mPriority.get_id());
                            List<MProduct> listP = FactoryProduct.getListPrioryProduct(mGlobalProducts, list);
                            Collections.sort(listP, new Comparator<MProduct>() {
                                @Override
                                public int compare(MProduct lhs, MProduct rhs) {
                                    return lhs.name.compareTo(rhs.name);
                                }
                            });
                            activateList(new ArrayList<Object>(listP), true);
                            в_стойло();
                        }
                    };
                    tempIOnMenus.add(m);


                } else {
                    List<String> sd = new ArrayList<>(listMap.keySet());
                    Collections.sort(sd, new Comparator<String>() {
                        @Override
                        public int compare(String lhs, String rhs) {
                            return lhs.compareTo(rhs);
                        }
                    });

                    for (final String s : sd) {
                        TempIonMenu m = new TempIonMenu();
                        m.name = "ММЛ:" + s;
                        m.image = R.drawable.menu_mml;
                        m.iActionE = new IActionE<Object>() {
                            @Override
                            public void action(Object o) {
                                List<BasePlanMML> planMMLList = UtilsPlan.getListPlanMML(false);
                                Map<String, List<String>> listMap = UtilsPlan.getMMLNameProducts(planMMLList);
                                List<String> productIdList = listMap.get(s);
                                List<MProduct> listP = FactoryProduct.getListMMLGroup(mGlobalProducts, productIdList);
                                Collections.sort(listP, new Comparator<MProduct>() {
                                    @Override
                                    public int compare(MProduct lhs, MProduct rhs) {
                                        return lhs.name.compareTo(rhs.name);
                                    }
                                });
                                activateList(new ArrayList<Object>(listP), true);
                                в_стойло();
                            }
                        };
                        tempIOnMenus.add(m);
                    }
                }
            } else {


                TempIonMenu m = new TempIonMenu();
                m.name = mPriority.name;
                m.image = R.drawable.menu_preority;
                m.iActionE = new IActionE<Object>() {
                    @Override
                    public void action(Object o) {
                        List<MPriorityProduct> list = getSession().getList(MPriorityProduct.class, " priority_id = ? ", mPriority.get_id());
                        List<MProduct> listP = FactoryProduct.getListPrioryProduct(mGlobalProducts, list);
                        Collections.sort(listP, new Comparator<MProduct>() {
                            @Override
                            public int compare(MProduct lhs, MProduct rhs) {
                                return lhs.name.compareTo(rhs.name);
                            }
                        });
                        activateList(new ArrayList<Object>(listP), true);
                        в_стойло();
                    }
                };
                tempIOnMenus.add(m);
            }
        }


        ////////////////////////////////////////////////////////////////////// добавление группы товаров
        List<PlanE> planEs = Configure.getSession().getList(PlanE.class, null);
        if (planEs.size() > 0) {
            PlanE planE = planEs.get(0);
            Map<String, GroupDouble> plansMap = planE.plansMap;
            List<GroupDouble> sdd = new ArrayList<>();

            for (Map.Entry<String, GroupDouble> ss : plansMap.entrySet()) {
                GroupDouble groupDouble = ss.getValue();
                groupDouble.grup_id = ss.getKey();
                sdd.add(groupDouble);
            }

            Collections.sort(sdd, new Comparator<GroupDouble>() {
                @Override
                public int compare(GroupDouble lhs, GroupDouble rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });

            for (final GroupDouble groupDouble : sdd) {
                TempIonMenu m = new TempIonMenu();
                m.name = groupDouble.name;
                m.image = R.drawable.menu_group;
                m.iActionE = new IActionE<Object>() {
                    @Override
                    public void action(Object o) {
                        List<MProduct> lp = getSession().getList(MProduct.class, null);
                        Utils.pricecometr(lp, mSettings);
                        List<MProduct> sd = new ArrayList<>();
                        for (MProduct product : lp) {
                            if (product.productGroup == null) continue;
                            if (product.isArchive) continue;
                            if (!product.productGroup.equals(groupDouble)) continue;
                            if (product.price <= 0) continue;
                            sd.add(product);
                        }
                        Collections.sort(sd, new Comparator<MProduct>() {
                            @Override
                            public int compare(MProduct lhs, MProduct rhs) {
                                return lhs.name.compareTo(rhs.name);
                            }
                        });

                        activateList(new ArrayList<Object>(sd), true);
                        в_стойло();
                    }
                };
                tempIOnMenus.add(m);
            }


        }
        {

            TempIonMenu m = new TempIonMenu();
            m.name = getString(R.string.show_self);
            m.image = R.drawable.menu_board;
            m.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {

                    DialogDataSelf self = new DialogDataSelf();
                    self.show(getActivity().getSupportFragmentManager(), "fsdf");
                    self.mProducts = mGlobalProducts;
                    в_стойло();
                }
            };
            tempIOnMenus.add(m);
        }
        if (getStateSystem() != StateSystem.VISIT_SHOW_STOCK && getStateSystem() != SHOW_STOCK_SIMPLE) {
            {

                TempIonMenu m = new TempIonMenu();
                m.name = getString(R.string.show_select_only);
                m.image = R.drawable.menu_selected;
                m.iActionE = new IActionE<Object>() {
                    @Override
                    public void action(Object o) {
                        if (getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
                            activateList(new ArrayList<Object>(mSettings.getVisit().selectProductForPresent), false);
                        }
                        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {
                            activateList(new ArrayList<Object>(mSettings.getVisit().selectProductForSale), false);
                        }
                        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
                            activateList(new ArrayList<Object>(mSettings.getVisit().selectProductForBalance), false);
                        }
                        в_стойло();
                    }
                };
                tempIOnMenus.add(m);

            }
            {

                TempIonMenu m = new TempIonMenu();
                m.name = getString(R.string.claer_all);
                m.image = R.drawable.menu_reset;
                m.iActionE = new IActionE<Object>() {
                    @Override
                    public void action(Object o) {
                        if (getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
                            mSettings.getVisit().selectProductForPresent = new ArrayList<>();
                        }
                        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {
                            mSettings.getVisit().selectProductForSale = new ArrayList<>();
                        }
                        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
                            mSettings.getVisit().selectProductForBalance = new ArrayList<>();
                        }

                        for (MProduct product : mGlobalProducts) {
                            product.setAmountCore(0);// = 0;
                        }
                        showAll();
                        в_стойло();
                    }
                };
                tempIOnMenus.add(m);
            }


        }

        /////////////
        {
            TempIonMenu m = new TempIonMenu();
            m.name = getString(R.string.menu_jhjasas);
            m.image = R.drawable.menu_board;
            m.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    showStockSelf();
                    в_стойло();
                }
            };
            tempIOnMenus.add(m);
        }

        {
            TempIonMenu m = new TempIonMenu();
            m.name = getString(R.string.menu_shjhasd);
            m.image = R.drawable.menu_zero_price;
            m.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    menu_p2();
                    в_стойло();
                }
            };
            tempIOnMenus.add(m);
        }

        {
            TempIonMenu m = new TempIonMenu();
            m.name = getString(R.string.menu_shadad);
            m.image = R.drawable.menu_archive;
            m.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    menu_p3();
                    в_стойло();
                }
            };
            tempIOnMenus.add(m);
        }

        {
            TempIonMenu m = new TempIonMenu();
            m.name = getString(R.string.menu_sadasddf);
            m.image = R.drawable.menu_informations;
            m.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    Utils.showNoteRealise(getActivity(), mSettings, null, null);
                    в_стойло();
                }
            };
            tempIOnMenus.add(m);
        }

        {
            TempIonMenu m = new TempIonMenu();
            m.name = getString(R.string.menu_sadasdfdf);
            m.image = R.drawable.menu_visit;
            m.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    MainActivity.mainActivityInstance.finish();
                }
            };
            tempIOnMenus.add(m);
        }

        /////////////////


        ShowmenMenu.show(tempIOnMenus, factoryMenu, getActivity());


        int sta = getStateSystem();

        if (getStateSystem() == StateSystem.SHOW_STOCK_SIMPLE) {
            mView.findViewById(R.id.stock_amount).setVisibility(View.GONE);
            mView.findViewById(R.id.stock_total).setVisibility(View.GONE);
            mView.findViewById(R.id.table_price1).setVisibility(View.GONE);

        } else if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            mView.findViewById(R.id.stock_amount).setVisibility(View.GONE);
            mView.findViewById(R.id.stock_total).setVisibility(View.GONE);
            mView.findViewById(R.id.table_price1).setVisibility(View.GONE);
        } else if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
            // mView.findViewById(R.id.stock_amount).setVisibility(View.GONE);
            mView.findViewById(R.id.stock_total).setVisibility(View.GONE);
            mView.findViewById(R.id.stock_bort).setVisibility(View.GONE);
            mView.findViewById(R.id.table_price1).setVisibility(View.GONE);
            mView.findViewById(R.id.table_price2).setVisibility(View.GONE);
        } else {
            mView.findViewById(R.id.table_price2).setVisibility(View.GONE);
        }


    }

    private void в_стойло() {
        DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
    }

    private void createSearchPanel() {
        mEditText = (EditText) mView.findViewById(R.id.text_search);
        mEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try{
                    if (s.toString().trim().length()==0) {
                        showAll();
                    } else {
                        activateList(new ArrayList<Object>(FactoryProduct.getSearchResult(mGlobalProducts, s.toString().trim())), false,s.toString());
                    }
                }catch (Exception ex){
                    log.error(ex);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() > 0) {
                    mEditText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    mEditText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
                }
            }
        });
    }


    private void createListABC() {

        MyListViewAbc mListViewE = (MyListViewAbc) mView.findViewById(R.id.listView_alphavit);
        mListViewE.setIAction(new IActionE<String>() {
            @Override
            public void action(String o) {
                activateList(new ArrayList<Object>(FactoryProduct.getListAbs(mGlobalProducts, o)), false);
            }
        });

    }

    private void activateButton() {
        mView.findViewById(R.id.button_stock_colaps).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAll();
            }
        });

        mView.findViewById(R.id.button_stock).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int ff = Settings.getStateSystem();
                if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
                    Settings.showFragment(StateSystem.VISIT_ACTION, getActivity());
                } else if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {

                    if (mSettings.useVisit2) {
                        Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                    } else {
                        if(mSettings.getVisit().selectProductForSale.size()==0){
                            Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                        }else {
                            Settings.showFragment(StateSystem.VISIT_SALE, getActivity());
                        }

                    }

                } else if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
                    if (mSettings.useVisit2) {
                        Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                    } else {
                        Settings.showFragment(StateSystem.VISIT_BALANCE, getActivity());
                    }

                } else if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK) {
                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                } else if (getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
                    Settings.showFragment(StateSystem.VISIT_PRESENT, getActivity());
                } else {
                    Settings.showFragment(StateSystem.HOME, getActivity());
                }
            }
        });
    }

    public void showStockSelf() {
        List<MProduct> list = new ArrayList<>();
        for (MProduct mProduct : mGlobalProducts) {
            if (mProduct.amountSelf != 0) {
                list.add(mProduct);
            }
        }
        activateList(new ArrayList<Object>(list), false);
    }

    private void showAll() {
        List<Object> objectList = new ArrayList<Object>(FactoryProduct.getProductGroupsFree(mGlobalProductGroups));
        activateList(objectList, false);
    }

    @Override
    public void onRowDataClick(final MProductBase mProduct, final View view, List<Object> objects) {

        if (getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
            DialogProductForSale2 dialogProductForSale2 = new DialogProductForSale2();
            dialogProductForSale2.setParametr(mProduct, view, mSettings);
            dialogProductForSale2.setiActionE(new IActionE<MProductBase>() {
                @Override
                public void action(MProductBase o) {
                    //painterItemProduct(view, mProduct);
                }
            });
            dialogProductForSale2.show(getActivity().getSupportFragmentManager(), "sd");
        }
        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            DialogProductForSale2 dialogProductForSale2 = new DialogProductForSale2();
            dialogProductForSale2.setParametr(mProduct, view, mSettings);
            dialogProductForSale2.setiActionE(new IActionE<MProductBase>() {
                @Override
                public void action(MProductBase o) {
                    //painterItemProduct(view, mProduct);
                }
            });
            dialogProductForSale2.show(getActivity().getSupportFragmentManager(), "sd");
        }

        if ((getStateSystem() == StateSystem.VISIT_STOCK_PRESENT && (!mProduct.isArchive && mProduct.price != 0))) {

            if (mSpinner.getSelectedItemPosition() == 0) {
                Toast.makeText(getActivity(), R.string.no_select_type, Toast.LENGTH_SHORT).show();
            } else {

                DialogProductForSale2 dialogProductForSale2 = new DialogProductForSale2();
                dialogProductForSale2.setParametr(mProduct, view, mSettings);
                List<MFreePresentTYpes> d = new ArrayList<>(mFreePresentTYpes);
                d.add(0, new MFreePresentTYpes());
                dialogProductForSale2.setmRuleId(d.get(mSpinner.getSelectedItemPosition()).idu);
                dialogProductForSale2.setiActionE(new IActionE<MProductBase>() {
                    @Override
                    public void action(MProductBase o) {
                        //painterItemProduct(view, mProduct);
                    }
                });
                dialogProductForSale2.show(getActivity().getSupportFragmentManager(), "wsd");
            }
        }

        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE && (!mProduct.isArchive && mProduct.price != 0)) {// показать форму выбора количества продуктов
            DialogProductForSale2 dialogProductForSale2 = new DialogProductForSale2();
            dialogProductForSale2.setParametr(mProduct, view, mSettings);
            dialogProductForSale2.setiActionE(new IActionE<MProductBase>() {
                @Override
                public void action(MProductBase o) {
                    //painterItemProduct(view, mProduct);
                }
            });
            dialogProductForSale2.show(getActivity().getSupportFragmentManager(), "wsd");
        }


    }

    private void painterItemProduct(View view, MProductBase mProduct) {
        View v1 = view.findViewById(R.id.panel_decorator);
        View v2 = view.findViewById(R.id.amount_safe_d);
        View v3 = view.findViewById(R.id.amount_safe_d2);
        View v4 = view.findViewById(R.id.price_d);
        View v5 = view.findViewById(R.id.amount_d);
        View v6 = view.findViewById(R.id.itogo_d);
        if (mProduct.getAmount_core() > 0) {

            v1.setBackgroundResource(R.color.colore97);
            v2.setBackgroundResource(R.color.colore97);
            v3.setBackgroundResource(R.color.colore97);
            v4.setBackgroundResource(R.color.colore97);
            v5.setBackgroundResource(R.color.colore97);
            v6.setBackgroundResource(R.color.colore97);
        } else {
            v1.setBackgroundResource(R.color.colore3);
            v2.setBackgroundResource(R.color.colore3);
            v3.setBackgroundResource(R.color.colore3);
            v4.setBackgroundResource(R.color.colore3);
            v5.setBackgroundResource(R.color.colore3);
            v6.setBackgroundResource(R.color.colore3);
        }
    }

    @Override
    public void onRowNavigateClick(MProductGroup mProductGroup, List<Object> objects) {
        mProductGroup.isSelect = !mProductGroup.isSelect;
        List<Object> objectList;
        if (mProductGroup.isSelect) {
            objectList = AddinData(objects, mProductGroup);
        } else {
            List<Object> objectListDelete = new ArrayList<>();
            objectList = RemoveData(objects, mProductGroup, objectListDelete);
            for (Object o : objectListDelete) {
                objects.remove(o);
            }
        }
        activateList(objectList, false);
    }

    private List<Object> RemoveData(List<Object> objects, MProductGroup mProductGroup, List<Object> objectListDelete) {

        for (Object object : objects) {
            if (object instanceof MProductGroup) {
                if (((MProductGroup) object).parent_id != null && ((MProductGroup) object).parent_id.equals(mProductGroup.idu)) {
                    objectListDelete.add(object);
                    RemoveData(objects, (MProductGroup) object, objectListDelete);
                }
            } else {
                if (((MProduct) object).group_id.equals(mProductGroup.idu)) {
                    objectListDelete.add(object);
                }
            }
        }
        return objects;
    }

    private List<Object> AddinData(List<Object> objects, final MProductGroup mProductGroup) {

        List<MProductGroup> groupList = Configure.getSession().getList(MProductGroup.class, null);
        List<MProductGroup> dd = new ArrayList<>();
        for (MProductGroup pg : groupList) {
            if (pg.parent_id != null && pg.parent_id.equals(mProductGroup.idu)) {
                dd.add(pg);
            }
        }
        Collections.sort(dd, new Comparator<MProductGroup>() {
            @Override
            public int compare(MProductGroup lhs, MProductGroup rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        List<MProduct> mProductList = FactoryProduct.getListForMenu(mGlobalProducts, mProductGroup.idu);
        int index = 0;
        for (Object object : objects) {
            index++;
            if (object instanceof MProductGroup) {
                MProductGroup sd = (MProductGroup) object;
                if (sd.idu.equals(mProductGroup.idu)) {
                    break;
                }
            }
        }

        for (MProductGroup productGroup : dd) {
            productGroup.setPadding(mProductGroup.getPadding() + 20);
        }
        for (MProduct product : mProductList) {
            product.setPadding(mProductGroup.getPadding() + 20);
        }
        objects.addAll(index, new ArrayList<Object>(dd));
        objects.addAll(index + dd.size(), new ArrayList<Object>(mProductList));
        return objects;
    }

    private List<MProduct> getSelectList() {

        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_ACTION) {
            List<MProduct> mProducts = new ArrayList<>();
            for (MProduct mActionProduct : mSettings.getVisit().selectProductForActions) {
                if (mActionProduct != null) {
                    mProducts.add(mActionProduct);
                }
            }
            return mProducts;
        }


        if (getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
            return new ArrayList<>();
        }

        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {
            return mSettings.getVisit().selectProductForSale;
        }

        if (getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            return mSettings.getVisit().selectProductForBalance;
        }
        return new ArrayList<>();
    }


    private void activateList(List<Object> objects, boolean isCheckAction) {

        curObj = objects;
        if (!isCheckAction) {
            for (CheckBox checkBox : mPrioryCheckBoxList) {
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                }
            }
        }
        Parcelable index = null;
        try {
            index = mListView.onSaveInstanceState();
        } catch (Exception ignored) {

        }
        listAdapterStock = new ListAdapterStock(getActivity(), R.layout.myrow_date_sale_product_fix_2, objects, new ArrayList<MProductBase>(getSelectList()), false);
        //listAdapterStock.mProducts=mGlobalProducts;
        listAdapterStock.setOnRowDataClick(this).setOnRowNavigateClick(this);
        if (mSettings.getVisit() != null && mSettings.getVisit().point != null) {
            listAdapterStock.setmPoint(mSettings.getVisit().point);
        }
        mListView.setAdapter(listAdapterStock);
        if (index != null) {
            mListView.onRestoreInstanceState(index);
        }
    }

    private synchronized void activateList(List<Object> objects, boolean isCheckAction,String strSearch){
        curObj = objects;
        if (!isCheckAction) {
            for (CheckBox checkBox : mPrioryCheckBoxList) {
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                }
            }
        }
        Parcelable index = null;
        try {
            index = mListView.onSaveInstanceState();
        } catch (Exception ignored) {

        }
        listAdapterStock = new ListAdapterStock(getActivity(), R.layout.myrow_date_sale_product_fix_2, objects, new ArrayList<MProductBase>(getSelectList()), false,strSearch);
        //listAdapterStock.mProducts=mGlobalProducts;
        listAdapterStock.setOnRowDataClick(this).setOnRowNavigateClick(this);
        if (mSettings.getVisit() != null && mSettings.getVisit().point != null) {
            listAdapterStock.setmPoint(mSettings.getVisit().point);
        }
        mListView.setAdapter(listAdapterStock);
        if (index != null) {
            mListView.onRestoreInstanceState(index);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

    }
}
