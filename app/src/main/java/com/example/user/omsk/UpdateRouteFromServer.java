package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;

import com.example.user.omsk.senders.SenderUpdateRouteFromServer;

public class UpdateRouteFromServer {
    public void update( Activity activity, IActionE iAction) {
        new SenderUpdateRouteFromServer().send( activity, iAction);
    }
}
