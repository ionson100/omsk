package com.example.user.omsk.visitcontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.FVisitRefund;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.kassa.UtilsKassa;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VisitDebt extends LinearLayout {


    public IActionE<Object> iActionE;
    private TextView debtString;
    private View mView;
    private Settings mSettings;
    public MainActivity mainActivity;


    public VisitDebt(Context context) {
        super(context);
        init();
    }

    public VisitDebt(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VisitDebt(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = vi.inflate(R.layout.visit_control_debt, null);
        this.addView(mView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));


        mSettings = Settings.core();
        debtString = (TextView) mView.findViewById(R.id.itogo_debt_string);
        double res = Utils.getDebt(mSettings.getVisit().debtList);
        debtString.setText(getContext().getString(R.string.itogo_debt) + Utils.getStringDecimal(res) + getContext().getString(R.string.rub));


        TextView refund = (TextView) mView.findViewById(R.id.visit_refunt_value);
        double debres = Utils.getDebtPoint(mSettings.getVisit().point);
        refund.setText(getContext().getString(R.string.lasdkjsd) + Utils.getStringDecimal(debres) + getContext().getString(R.string.rub));
        activatePanel();
    }

    private void activatePanel() {
        LinearLayout debt_list = (LinearLayout) mView.findViewById(R.id.debt_list);
        debt_list.removeAllViews();
        List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, " point_id = ? order by date ", mSettings.getVisit().point.idu);
        List<MVisitStory> visitStoryList =Configure.getSession().getList(MVisitStory.class, " point_id = ? and idu <> ?",
                mSettings.getVisit().point.idu, mSettings.getVisit().idu);
        List<MDebt> delete = new ArrayList<>();
        for (MVisitStory mVisitStory : visitStoryList) {
            for (MDebt debt : mDebtList) {
                if (mVisitStory.debtList.contains(debt.order_id)) {
                    delete.add(debt);
                }
            }
        }
        for (MDebt debt : delete) {
            mDebtList.remove(debt);
        }
        Map<String, List<MDebt>> map = new HashMap<>();
        for (MDebt mDebt : mDebtList) {
            if (map.containsKey(mDebt.order_id)) {
                map.get(mDebt.order_id).add(mDebt);
            } else {
                List<MDebt> list = new ArrayList<>();
                list.add(mDebt);
                map.put(mDebt.order_id, list);
            }
        }
        List<FVisitRefund.TempList> tempLists = new ArrayList<>();
        for (Map.Entry<String, List<MDebt>> ss : map.entrySet()) {
            FVisitRefund.TempList list = new FVisitRefund.TempList();
            list.string = ss.getKey();
            list.mDebts = ss.getValue();
            list.date = ss.getValue().get(0).date;
            tempLists.add(list);
        }

        Collections.sort(tempLists, new Comparator<FVisitRefund.TempList>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(FVisitRefund.TempList lhs, FVisitRefund.TempList rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });

        if (tempLists.size() == 0) {
            this.setVisibility(GONE);
            return;
        }


        for (final FVisitRefund.TempList tempList : tempLists) {


            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_debt, null);

            Pupper date = (Pupper) view.findViewById(R.id.visit_date);
            Pupper itogo = (Pupper) view.findViewById(R.id.visit_itogo_price);
            final CheckBox action = (CheckBox) view.findViewById(R.id.check_action);

            double itogoE = 0;

            LinearLayout debit_item = (LinearLayout) view.findViewById(R.id.debit_item);

            String order_id = "";
            for (MDebt mDebt : tempList.mDebts) {
                itogoE = itogoE + mDebt.amount * mDebt.price;
                View v = LayoutInflater.from(getContext()).inflate(R.layout.item_product_story, null);
                MProduct product = mDebt.getProduct();
                TextView name = (TextView) v.findViewById(R.id.name_s);
                TextView amount = (TextView) v.findViewById(R.id.amount_s);
                TextView price = (TextView) v.findViewById(R.id.price_s);
                TextView itogoES = (TextView) v.findViewById(R.id.itogo_s);
                name.setText(product.name);
                amount.setText(Utils.getStringDecimal(mDebt.amount));
                price.setText(Utils.getStringDecimal(mDebt.price));
                itogoES.setText(Utils.getStringDecimal(mDebt.amount * mDebt.price));
                order_id = mDebt.order_id;

                debit_item.addView(v);
            }
            Date d=Utils.intToDate(tempList.mDebts.get(0).date);
            date.setPairString(getContext().getString(R.string.dfdf), Utils.simpleDateFormatE(d));
            itogo.setPairString(getContext().getString(R.string.sddddsd), Utils.getStringDecimal(itogoE));


            final double finalItogoE = itogoE;

            final String finalOrder_id = order_id;
            MChecksData data = UtilsKassa.getMChecksDataDebt(mSettings.getVisit().checksDataList, finalOrder_id);


            if (data != null) {
                view.findViewById(R.id.text_check_ok).setVisibility(View.VISIBLE);
                action.setVisibility(View.GONE);
            } else {
                if (mSettings.getVisit().debtList.contains(tempList.string)) {
                    action.setChecked(true);
                }

                final boolean[] rum = {false};

                action.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                        if (!rum[0]) {
                            if (mSettings.getVisit().selectProductForActions.size() != 0) {
                                Utils.messageBoxE(mainActivity, getContext().getString(R.string.warning),
                                        getContext().getString(R.string.gdfgdfsg), "Отказаться", "продолжить",
                                        new IActionE<View>() {
                                            @Override
                                            public void action(View v) {
                                                rum[0] = true;
                                                action.setChecked(!isChecked);
                                            }
                                        }, new IActionE<View>() {
                                            @Override
                                            public void action(View v) {
                                                VisitDebt.this.action(tempList.string, finalItogoE, isChecked);
                                                mSettings.getVisit().selectProductForActions.clear();
                                            }
                                        }, true);
                            } else {
                                VisitDebt.this.action(tempList.string, finalItogoE, isChecked);
                            }
                        } else {
                            rum[0] = false;
                        }
                        if (iActionE != null) {
                            iActionE.action(null);
                        }
                    }
                });

            }
            debt_list.addView(view);
        }

    }

    void action(String visit_id, double itogo, boolean checked) {
        if (checked) {
            if (mSettings.getVisit().debtList.contains(visit_id)) {
            } else {
                mSettings.getVisit().debtList.add(visit_id);
            }
        } else {
            if (mSettings.getVisit().debtList.contains(visit_id)) {
                mSettings.getVisit().debtList.remove(visit_id);
            }
        }
        Settings.save();
        double res = Utils.getDebt(mSettings.getVisit().debtList);
        debtString.setText(getContext().getString(R.string.itogo_debt) + Utils.getStringDecimal(res) + getContext().getString(R.string.rub));
    }

}
