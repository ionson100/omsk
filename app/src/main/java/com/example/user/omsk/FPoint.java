package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.omsk.dialogs.DialogAlertPoint;
import com.example.user.omsk.dialogs.DialogSelectVisit;
import com.example.user.omsk.fotocontrol.MyViewFotoPoint;
import com.example.user.omsk.models.MAgent;
import com.example.user.omsk.models.MAlertPoint;
import com.example.user.omsk.models.MContact;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.PupperPartial;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FPoint extends android.support.v4.app.Fragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FPoint.class);
    public static final int MENU_ADD_ALERT = 8;
    public static final int MENU_EDIT_ALERT = 9;
    public static final int MENU_ADD_POINT = 1;
    public static final int MENU_EDIT_POINT = 2;
    public static final int MENU_ADD_AGENT = 3;
    public static final int MENU_EDIT_AGENT = 4;
    private static final int MENU_ADD_CONTACT = 5;
    private static final int MENU_EDIT_CONTACT = 6;
    private LinearLayout mContactPanel;
    private MyViewFotoPoint myViewFotoPoint;
    private View mView;
    private List<MPoint> mData;
    private Settings mSettings;

    public FPoint() {
        // Required empty public constructor
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        View vs = getActivity().getLayoutInflater().inflate(R.layout.menu_view_title, null);
        menu.setHeaderView(vs);
        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_POINT) return;
        if (mSettings.getVisit().point == null) return;
        //  menu.setHeaderIcon(R.mipmap.ic_launcher);
        switch (v.getId()) {
            case R.id.point_texture: {
                MAlertPoint alertPoint = mSettings.getVisit().point.getAlertPoint();
                if (alertPoint == null) {
                    menu.add(1, MENU_ADD_ALERT, 0, getString(R.string.alert_create));
                } else {
                    menu.add(1, MENU_EDIT_ALERT, 0, getString(R.string.alert_edit));
                }
                menu.add(1, MENU_ADD_POINT, 0, getActivity().getString(R.string.create_point_menu));
                if (mSettings.getVisit().point != null) {
                    menu.add(1, MENU_EDIT_POINT, 0, getString(R.string.edit_point_menu));
                }

                if (mSettings.getEditContact() != null) {
                    menu.add(2, MENU_EDIT_CONTACT, 0, getString(R.string.edit_contact_menu));
                }
                if (mSettings.getVisit().point != null) {
                    menu.add(2, MENU_ADD_CONTACT, 0, getString(R.string.add_contact_menu));
                }
                break;
            }
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case MENU_ADD_POINT: {
                mSettings.getVisit().point = null;
                Settings.showFragment(StateSystem.POINT_ADD, getActivity());
                return true;
            }
            case MENU_EDIT_POINT: {
                Settings.showFragment(StateSystem.POINT_EDIT, getActivity());
                return true;
            }
            case MENU_ADD_AGENT: {
                if (mSettings.getVisit().point.agent_id == null) {
                    Settings.showFragment(StateSystem.CONTRAGENT_ADD, getActivity());
                    mSettings.getVisit().point.setAgent(new MAgent());
                }
                return true;
            }
            case MENU_EDIT_AGENT: {
                if (mSettings.getVisit().point.agent_id != null) {
                    Settings.showFragment(StateSystem.CONTRAGENT_EDIT, getActivity());
                }
                return true;
            }
            case MENU_ADD_CONTACT: {
                Settings.showFragment(StateSystem.CONTACT_ADD, getActivity());
                return true;
            }
            case MENU_EDIT_CONTACT: {
                // mSettings.setEditContact(mSettings.getVisit().point.getListContact().set(viewPager.getCurrentItem()));
                Settings.showFragment(StateSystem.CONTACT_EDIT, getActivity());
                return true;
            }
            case MENU_ADD_ALERT: {
                DialogAlertPoint dialogAlertPoint = new DialogAlertPoint();
                dialogAlertPoint.setPoint(mSettings.getVisit().point).setAction(new DialogAlertPoint.IActionAlertDeleteCreate() {
                    @Override
                    public void Action(MPoint mPoint, int action) {
                    }
                });
                dialogAlertPoint.show(getActivity().getSupportFragmentManager(), "3443");
                return true;
            }
            case MENU_EDIT_ALERT: {
                DialogAlertPoint dialogAlertPoint = new DialogAlertPoint();
                dialogAlertPoint.setPoint(mSettings.getVisit().point).setAction(new DialogAlertPoint.IActionAlertDeleteCreate() {
                    @Override
                    public void Action(MPoint mPoint, int action) {
                    }
                });
                dialogAlertPoint.show(getActivity().getSupportFragmentManager(), "3434");
                return true;
            }
        }
        return super.onContextItemSelected(item);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_point, container, false);


        try {

            mSettings = Settings.core();

            if (mSettings.getVisit().point == null && mSettings.getVisit().point.idu != null) {
                MPoint p = Configure.getSession().get(MPoint.class, mSettings.getVisit().point.idu);
                if (p == null) {

                    Settings.showFragment(StateSystem.HOME, getActivity());
                    return mView;
                }
            }

            MyViewFotoPoint.deleteTempFiles();

            mContactPanel = (LinearLayout) mView.findViewById(R.id.contact_panel);
            Button btStory = (Button) mView.findViewById(R.id.bt_point_story);
            if (Settings.getStateSystem() == StateSystem.POINT) {
                btStory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Settings.showFragment(StateSystem.POINT_STORY_FOR_PREVIEW, getActivity());
                    }
                });
            } else {
                btStory.setVisibility(View.GONE);
            }
            Button btShowMap = (Button) mView.findViewById(R.id.btShowMap);
            btShowMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_POINT) {
                        Settings.showFragment(StateSystem.VISIT_SHOW_MAP_FROM_POINT, getActivity());
                    } else {
                        Settings.showFragment(StateSystem.SHOW_MAP, getActivity());
                    }
                }
            });
            Button btRequest = (Button) mView.findViewById(R.id.button_create_doc);
            btRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_POINT) {
                        Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                        return;
                    }
                    if (!InspectionVisit.inspection(mSettings, getActivity())) {
                        return;
                    }
                    if (mSettings.getVisit().point == null) {
                        Toast.makeText(getActivity(), getString(R.string.errorEntryAction), Toast.LENGTH_SHORT).show();
                    } else {
                        List<MVisitStory> stories =  Configure.getSession().getList(MVisitStory.class, " point_id = '" + mSettings.getVisit().point.idu + "\'");
                        if (stories.size() >= 1) {
                            DialogSelectVisit dialogSelectVisit = new DialogSelectVisit();
                            dialogSelectVisit.initDialog(stories, new DialogSelectVisit.IActionSelectedVisit() {
                                @Override
                                public void action(MVisitStory mVisitStory) {
                                    MVisit vs = mVisitStory.getMVisit(getActivity());
                                    vs.secondary = true;
                                    mSettings.setVisit(vs);
                                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                                }
                            });
                            dialogSelectVisit.show(getActivity().getSupportFragmentManager(), "sdkjjdddsd");
                        } else {
                            Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                        }
                    }
                }
            });


            spinnerCreator(mView);
            contextMenuCreator();
            activateComments();

            bind(mView);
            List<MContact> listContact = mSettings.getVisit().point.getListContact(Configure.getSession());
            mContactPanel.removeAllViews();
            for (final MContact mContact : listContact) {
                LayoutInflater vi = LayoutInflater.from(getContext());

                View view1 = vi.inflate(R.layout.iten_list_new_contact, null);
                PupperPartial name = (PupperPartial) view1.findViewById(R.id.contact_name);
                PupperPartial email = (PupperPartial) view1.findViewById(R.id.contact_email);
                PupperPartial phone = (PupperPartial) view1.findViewById(R.id.contact_phone);
                PupperPartial comment = (PupperPartial) view1.findViewById(R.id.contact_comment);
                view1.findViewById(R.id.image_contact_editor).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSettings.setEditContact(mContact);
                        Settings.showFragment(StateSystem.CONTACT_EDIT, getActivity());
                    }
                });
                view1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSettings.setEditContact(mContact);
                    }
                });
                name.setPairString(mContact.name == null ? "" : mContact.name, "");
                email.setPairString("Email: ", (mContact.email == null ? "" : mContact.email));
                phone.setPairString("Телефон: ", (mContact.phone == null ? "" : mContact.phone));
                comment.setPairString("Комментарии: ", (mContact.comment == null ? "" : mContact.comment));
                mContactPanel.addView(view1);
            }


            //////////////////////////////////addin
            PupperPartial fio = (PupperPartial) mView.findViewById(R.id.fio_point);
            PupperPartial fioTel = (PupperPartial) mView.findViewById(R.id.fio_tel_point);
            PupperPartial inn = (PupperPartial) mView.findViewById(R.id.inn_point);

            fio.setPairString(getString(R.string.kjdksdsd), mSettings.getVisit().point.fio);
            fioTel.setPairString("Телефон: ", mSettings.getVisit().point.telephone_fio);
            inn.setPairString("ИНН: ", mSettings.getVisit().point.inn);

            myViewFotoPoint = (MyViewFotoPoint) mView.findViewById(R.id.my_control_foto);
            myViewFotoPoint.activate(mSettings.getVisit().point, true, getActivity());


        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FPoint:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;

    }

    private void activateComments() {
        AddItemsComment.add(mView, R.id.panel_last_comments, mSettings, getActivity());
    }

    private void contextMenuCreator() {

        final View d = mView.findViewById(R.id.point_texture);
        registerForContextMenu(d);
        mView.findViewById(R.id.bt_context_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().openContextMenu(d);
            }
        });
    }


    private void spinnerCreator(final View v) {

        if (mSettings.getVisit() == null) return;
        mData = Configure.getSession().getList(MPoint.class, null);

        Collections.sort(mData, new Comparator<MPoint>() {
            @Override
            public int compare(MPoint lhs, MPoint rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        //List<String> strings = new ArrayList<>();
//        for (MPoint mContragent : mData) {
//            strings.add(mContragent.name);
//        }
    //    final IconSpinnerAdapter mSpinerArrayAdapter = new IconSpinnerAdapter(getActivity().getBaseContext(), strings, R.drawable.ic_point_home);
//        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
//        spinner.setAdapter(mSpinerArrayAdapter);
//        if (mSettings.getVisit().point == null) {
//            spinner.setSelection(0);
//        } else {
//            spinner.setSelection(Utils.getPositionSpinner(new ArrayList<IUsingGuidId>(mData), mSettings.getVisit().point.idu));
//        }
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//
//                mSettings.getVisit().point = mData.get(position);
//                //  Toast.makeText(getActivity(), mSettings.getVisit().point.work_time, Toast.LENGTH_LONG).show();
//
//
//                activateComments();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_POINT) {
//            spinner.setEnabled(false);
//        }
    }

    private void bind(View v) {

        double dbt = Utils.getDebtPoint(mSettings.getVisit().point);
        ((PupperPartial) mView.findViewById(R.id.label_debit_value)).setPairString(getString(R.string.label_debit_value), Utils.getStringDecimal(dbt) + getString(R.string.rub));


        MPoint point = mSettings.getVisit().point;
        ((PupperPartial) mView.findViewById(R.id.p_name)).setPairString("Название точки: ", point.name);
        ((PupperPartial) mView.findViewById(R.id.p_address)).setPairString("Адрес: ", point.address);

        ((PupperPartial) mView.findViewById(R.id.price_type)).setPairString("Тип цены: ", point.getPriceType());

        ((PupperPartial) mView.findViewById(R.id.commentary)).setPairString("Комментарий: ", point.getCommentary());
        ((PupperPartial) mView.findViewById(R.id.point_type)).setPairString("Тип точки: ", point.getNamePointTYpe());
        ((PupperPartial) mView.findViewById(R.id.channel_type)).setPairString("Канал сбыта: ", point.getNameChannelType());


        if (mSettings.getVisit().point.getContragent(Configure.getSession()) != null) {
            MAgent mAgent = mSettings.getVisit().point.getContragent(Configure.getSession());

        }

        ////////////////////////////////
        PupperPartial textViewSms = ((PupperPartial) mView.findViewById(R.id.point_sms_check));
        PupperPartial textViewEmail = ((PupperPartial) mView.findViewById(R.id.point_email_check));
        if (point.sms == null) {
            textViewSms.setPairString("Чек, отправка смс: ", "Нет данных");
        } else {
            textViewSms.setPairString("Чек, отправка смс: ", String.valueOf(point.sms));


        }
        if (point.email == null) {
            textViewEmail.setPairString("Чек, отправка email уведомления: ", "Нет данных");
        } else {
            textViewEmail.setPairString("Чек, отправка email уведомления: ", point.email);
        }
    }

    public void refrashImage() {

        myViewFotoPoint.showImages();

    }
}

