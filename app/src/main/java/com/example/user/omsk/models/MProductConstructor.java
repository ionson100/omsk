package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.Table;

import java.io.Serializable;

@Table("product_constructor")
public class MProductConstructor extends MProductBase implements Serializable, IUsingGuidId {
    public MProductConstructor() {
        this.idu = Utils.getUuid();
    }

    public transient boolean isSelect;
    public transient boolean isEdit;
}
