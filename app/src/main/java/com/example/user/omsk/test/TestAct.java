package com.example.user.omsk.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.TempProductSender;
import com.example.user.omsk.senders.TempVisit;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.google.gson.Gson;

public class TestAct extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        final EditText editText = (EditText) findViewById(R.id.text_rename);
        findViewById(R.id.bt_rename).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = editText.getText().toString();
                Gson d = Utils.getGson();
                TempVisit ss = d.fromJson(s, TempVisit.class);
                MVisit visit = new MVisit();
                visit.id = 0;
                visit.idu = ss.idu;
                for (TempProductSender p : ss.action_product_list) {

                    MProduct ds = Configure.getSession().get(MProduct.class, p.product_id);
                    ds.setAmountCore(p.amount);
                    ds.price = p.price;
                    visit.selectProductForActions.add(ds);
                }

                for (TempProductSender p : ss.free_product_list) {

                    MProduct ds = Configure.getSession().get(MProduct.class, p.product_id);
                    ds.setAmountCore(p.amount);
                    ds.price = p.price;
                    visit.selectProductForPresent.add(ds);
                }
                for (TempProductSender p : ss.newOrder) {

                    MProduct ds = Configure.getSession().get(MProduct.class, p.product_id);
                    ds.setAmountCore(p.amount);
                    ds.price = p.price;
                    visit.newOrderList.add(ds);
                }
                for (TempProductSender p : ss.sales_product_list) {
                    MProduct ds = Configure.getSession().get(MProduct.class, p.product_id);
                    if(ds==null) continue;
                    ds.setAmountCore(p.amount);
                    ds.price = p.price;
                    visit.selectProductForSale.add(ds);
                }
                for (TempProductSender p : ss.stock_product_list) {

                    MProduct ds = Configure.getSession().get(MProduct.class, p.product_id);
                    if(ds==null) continue;
                    ds.setAmountCore(p.amount);
                    ds.price = p.price;
                    visit.selectProductForBalance.add(ds);
                }


                visit.pressure = ss.pressure;
                visit.distance = ss.distance;
                visit.stateLocation = ss.locations;
                visit.member_action = ss.member_action;
                visit.concurent_actions = ss.concurent_actions;
                visit.order_type_id = ss.order_type_id;
                visit.setFinish(ss.finish);
                visit.setStart(ss.start);
                visit.point = Settings.core().getVisit().point;
                visit.latitude = ss.latitude;
                visit.longitude = ss.longitude;
                visit.visit_result_id = ss.visit_result_id;
                visit.comment = ss.comment;
                visit.payment = ss.payment;
                visit.refund = ss.refund;
                Settings.core().setVisit(visit);
                Settings.save();


            }
        });


    }

}
