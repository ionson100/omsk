package com.example.user.omsk.route;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.List;


public class ListAdapterRoutePoints extends ArrayAdapter<MPoint> {

    private final String strSearch;
    private final int mResource;
    private final IAddPointRoute mIAddPointRoute;

    public ListAdapterRoutePoints(String strSearch,Context context, int resource, List<MPoint> mPoints, IAddPointRoute iAddPointRoute) {
        super(context, resource, mPoints);
        this.strSearch = strSearch;
        this.mResource = resource;
        this.mIAddPointRoute = iAddPointRoute;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;
        final MPoint p = getItem(position);
        if (p != null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            mView = vi.inflate(mResource, null);
            final CheckBox checkBox = (CheckBox) mView.findViewById(R.id.checkbox_point_route);
            checkBox.setChecked(p.isSelectForRoute);
            TextView textViewName = (TextView) mView.findViewById(R.id.text_point_route);
            textViewName.setText(p.name);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.setChecked(!checkBox.isChecked());
                    p.isSelectForRoute = checkBox.isChecked();
                    if (checkBox.isChecked()) {
                        mIAddPointRoute.add(p, true);
                    } else {
                        mIAddPointRoute.add(p, false);
                    }
                }
            });
            if(strSearch!=null&&strSearch.length()>0&&textViewName.getText().length()>0){
                Utils.drawSearchText(textViewName,strSearch);
            }
        }
        mView.setTag(p);
        return mView;
    }
}
