package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.user.omsk.models.MContact;
import com.example.user.omsk.models.MContactor;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FEditAddContact extends Fragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FEditAddContact.class);
    private List<MContact> mContacts;
    private View mView;
    private Settings mSettings;

    public FEditAddContact() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mSettings = Settings.core();
        try {
            mView = inflater.inflate(R.layout.fragment_edit_add_contact, container, false);
            if (mSettings.getVisit() == null) {
                Settings.showFragment(StateSystem.HOME, getActivity());
                return mView;
            }
            mContacts = Configure.getSession().getList(MContact.class, null);
            Collections.sort(mContacts, new Comparator<MContact>() {
                @Override
                public int compare(MContact lhs, MContact rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            if (Settings.getStateSystem() == StateSystem.CONTACT_ADD) {
                mSettings.setEditContact(new MContact());
                mSettings.getEditContact().name = getString(R.string.new_contragent);
                mContacts.add(0, mSettings.getEditContact());
            }
            Button btSave = (Button) mView.findViewById(R.id.to_save_contact);
            btSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mSettings.getEditContact().phone == null || mSettings.getEditContact().phone.trim().length() == 0) {
                        ((EditText) mView.findViewById(R.id.contact_phone)).setError(getString(R.string.validte2));
                        mView.findViewById(R.id.contact_phone).requestFocus();
                        return;
                    }
                    if (mSettings.getEditContact().name == null || mSettings.getEditContact().name.trim().length() == 0) {
                        ((EditText) mView.findViewById(R.id.contact_name)).setError(getString(R.string.validtel));
                        mView.findViewById(R.id.contact_name).requestFocus();
                        return;
                    }
                    ///////////////////////////////add
                    mSettings.getEditContact().name = mSettings.getEditContact().name.trim();
                    /////////////////////////////////////////////
                    ISession ses = Configure.getSession();
                    ses.beginTransaction();
                    try {
                        if (Settings.getStateSystem() == StateSystem.CONTACT_ADD) {
                            if (mSettings.getEditContact().id == 0) {
                                ses.insert(mSettings.getEditContact());
                            } else {
                                ses.update(mSettings.getEditContact());
                            }
                            ses.insert(new MContactor(mSettings.getVisit().point.idu, mSettings.getEditContact().idu));
                        } else {
                            ses.update(mSettings.getEditContact());
                        }
                        mSettings.getVisit().point.setNewObject(true);
                        ses.update(mSettings.getVisit().point);
                        ses.commitTransaction();
                    } finally {
                        ses.endTransaction();
                    }
                    Settings.showFragment(StateSystem.POINT, getActivity());
                    getActivity().sendBroadcast(new Intent(MainActivity.FAB));
                }
            });
            Bind();
            activateSpinner();

        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FEditAddContact:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            Settings.showFragment(StateSystem.HOME, getActivity());
        }
        return mView;
    }

    private void activateSpinner() {

        Spinner spinner = (Spinner) mView.findViewById(R.id.spiner_contacts);
        List<String> strings = new ArrayList<>();

        for (MContact mContact : mContacts) {
            strings.add(mContact.name);
        }

        IconSpinnerAdapter contact = new IconSpinnerAdapter(getActivity(), strings, R.drawable.ic_contact);
        spinner.setAdapter(contact);
        spinner.setSelection(getPosition());
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSettings.setEditContact(mContacts.get(position));
                Bind();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void Bind() {

        EditText et1 = (EditText) mView.findViewById(R.id.contact_name);
        EditText et2 = (EditText) mView.findViewById(R.id.contact_phone);
        final EditText et3 = (EditText) mView.findViewById(R.id.contact_email);
        EditText et4 = (EditText) mView.findViewById(R.id.contact_comment);
        et1.setText(mSettings.getEditContact().name);
        et2.setText(mSettings.getEditContact().phone);
        et3.setText(mSettings.getEditContact().email);
        et4.setText(mSettings.getEditContact().comment);
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getEditContact().name = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getEditContact().phone = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && !s.toString().equals("")) {
                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(s).matches()) {
                        et3.setError(getString(R.string.validteEmail));
                        et3.requestFocus();
                    } else {
                        mSettings.getEditContact().email = s.toString();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getEditContact().comment = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });
    }

    private int getPosition() {
        int res = 0;
        for (int i = 0; i < mContacts.size(); i++) {
            if (mContacts.get(i).id == mSettings.getEditContact().id) {
                res = i;
                break;
            }
        }
        return res;
    }
}

