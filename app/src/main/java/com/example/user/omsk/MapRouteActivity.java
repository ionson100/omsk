package com.example.user.omsk;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;

import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.PathOverlay;

import java.util.List;


public class MapRouteActivity extends AppCompatActivity {

    private PathOverlay myPathRed;
    private PathOverlay myPathBlue;

    private Mapper mapper;
    private Settings mSettings;
    private MapView mMapView;
    private MyItemizedOverlayRoute myItemizedOverlay = null;
    private GeoPoint startPointRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_route);

        mSettings = Settings.core();
        mMapView = (MapView) findViewById(R.id.mapview);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setClickable(true);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        mMapView.setClickable(true);
        mMapView.setTileSource(TileSourceFactory.MAPNIK);
        mMapView.getController().setZoom(14);
        myPathRed = new PathOverlay(Color.RED, this);
        myPathBlue = new PathOverlay(Color.BLUE, this);

        {

            myItemizedOverlay = new MyItemizedOverlayRoute(getResources().getDrawable(R.drawable.gps_point1),
                    new DefaultResourceProxyImpl(this.getApplicationContext()), MapRouteActivity.this);
            mMapView.getOverlays().add(myItemizedOverlay);
            int i = 0;
            List<MRoute> routeList = Configure.getSession().getList(MRoute.class, null);

            for (MRoute mRoute : routeList) {
                i++;

                if (mRoute.getPoint().latitude == 0 || mRoute.getPoint().longitude == 0) continue;
                GeoPoint geoPoint = new GeoPoint(mRoute.getPoint().latitude, mRoute.getPoint().longitude);
                myItemizedOverlay.addItem(geoPoint, mRoute.point_id, "1:" + String.valueOf(i));
                if (startPointRoute == null) {
                    startPointRoute = geoPoint;
                }

                if (mSettings.usePaintRoute) {
                    myPathRed.addPoint(new GeoPoint(mRoute.getPoint().latitude, mRoute.getPoint().longitude));
                }


            }
            myItemizedOverlay = new MyItemizedOverlayRoute(getResources().getDrawable(R.drawable.gps_point2),
                    new DefaultResourceProxyImpl(this.getApplicationContext()), MapRouteActivity.this);
            mMapView.getOverlays().add(myItemizedOverlay);

            if (mSettings.usePaintRoute) {
                mMapView.getOverlays().add(myPathRed);
            }
            List<MVisitStory> visitStoryList =  Configure.getSession().getList(MVisitStory.class, null);
            i = 0;
            for (MVisitStory story : visitStoryList) {
                i++;
                if (story.latitude == 0 || story.longitude == 0) continue;
                GeoPoint geoPoint = new GeoPoint(story.latitude, story.longitude);
                if (startPointRoute == null) {
                    startPointRoute = geoPoint;
                }
                myItemizedOverlay.addItem(geoPoint, story.idu, "2:" + String.valueOf(i));

                if (mSettings.usePaintRoute) {
                    myPathBlue.addPoint(new GeoPoint(story.latitude, story.longitude));
                }
            }

            if (mSettings.usePaintRoute) {
                mMapView.getOverlays().add(myPathBlue);
            }
        }

        mMapView.invalidate();

        final boolean[] run = {true};
        mMapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                run[0] = false;
                return false;
            }
        });
        ViewTreeObserver vto = mMapView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (run[0]) {
                    if (mMapView != null)
                        if (startPointRoute != null) {
                            mMapView.getController().setCenter(startPointRoute);
                        }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mMapView.getOverlays().remove(myItemizedOverlay);
        if (mSettings.usePaintRoute) {
            try {
                mMapView.getOverlays().remove(myPathRed);
                mMapView.getOverlays().remove(myPathBlue);
            } catch (Exception ignored) {
            }
        }

        mMapView.getTileProvider().clearTileCache();
        myPathRed = null;
        myPathBlue = null;
        myItemizedOverlay = null;
        mMapView = null;
        System.gc();
    }


}
