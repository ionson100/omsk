package com.example.user.omsk.auto;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.DaData.ui.AutoCompleteActivity;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
//import com.example.user.omsk.models.MError;
import com.example.user.omsk.orientationimage.RotateImage;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.PupperPartial;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

import static com.example.user.omsk.auto.AutoOpenActivity.getPhotoDirectoryTempAuto;

public class AutoCloseActivity extends AppCompatActivity {

    public static org.apache.log4j.Logger log = Logger.getLogger(AutoCloseActivity.class);
    EditText editTextKM, editTextAddress;
    public static final int RESULT = 2302;
    private static final int CAMERA_REQUEST_CLOSE_AUTO = 9898;
    private MAuto mAuto;
    private Settings mSettings;
    private LinearLayout panelPhoto;
    private Button buttonFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_auto_close);


            mSettings = Settings.core();
            panelPhoto = (LinearLayout) findViewById(R.id.panel_photo);


            List<MAuto> mAutos = Configure.getSession().getList(MAuto.class, " uid = ?", mSettings.uid_auto);
            if (mAutos.size() == 0) {
                setResult(Activity.RESULT_CANCELED);
                Toast.makeText(this, R.string.sdsdjhsd, Toast.LENGTH_SHORT).show();
                finish();
            }
            mAuto = mAutos.get(0);

            if (mAuto.km1 == 0) {
                mAuto.km1 = mAuto.lastKm;
            }

            PupperPartial kmOpen = (PupperPartial) findViewById(R.id.km_open_label);
            kmOpen.setPairString(getString(R.string.dsuiui), String.valueOf(mAuto.km1));

            editTextKM = (EditText) findViewById(R.id.cur_km);
            editTextAddress = (EditText) findViewById(R.id.address);

            findViewById(R.id.bt_address_gpg).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (!Utils.isLocationServiceEnabled(AutoCloseActivity.this)) {
                        Utils.messageBox(getString(R.string.error), getString(R.string.kahjadas), AutoCloseActivity.this, new IActionE() {
                            @Override
                            public void action(Object o) {
                                Intent viewIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                AutoCloseActivity.this.startActivity(viewIntent);
                            }
                        });
                        return;
                    }


                    DialogDetermineGpsUser gpsUser = new DialogDetermineGpsUser();
                    gpsUser.iActionE = new IActionE<String>() {
                        @Override
                        public void action(String o) {
                            editTextAddress.setText(o);
                        }
                    };
                    gpsUser.show(getSupportFragmentManager(), "afskdjuaisdu");
                }
            });


            buttonFoto = (Button) findViewById(R.id.bt_photo);
            buttonFoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String patch = getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(2).jpg";
                    Uri outputFileUri = Uri.fromFile(new File(patch));
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.putExtra("photo", true);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST_CLOSE_AUTO);

                }
            });


            findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            });

            findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {
                    save();

                }
            });

            findViewById(R.id.bt_address_kladr).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivityForResult(new Intent(AutoCloseActivity.this, AutoCompleteActivity.class), 1962);
                }
            });

            editTextKM.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    try {
                        mAuto.km2 = Integer.parseInt(s.toString().trim());
                    } catch (Exception ex) {
                        log.error(ex);
                        editTextKM.setError(ex.getMessage());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            showPhoto();
        } catch (Exception ex) {
            String msg = Utils.getErrorTrace(ex);
            Utils.sendMessage("close open:" + msg,  (MyApplication) getApplication());

            log.error(ex);
            //Configure.getSession().insert(new MError(msg));
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CLOSE_AUTO && resultCode == Activity.RESULT_OK) {

            showPhoto();
        }
        if (requestCode == 1962 && resultCode == Activity.RESULT_OK) {

            String s = data.getStringExtra("address");
            editTextAddress.setText(s);
        }
    }

    void showPhoto() {
        panelPhoto.removeAllViews();
//        String patch = getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(2).jpg";
        final File f = new File(getPhotoDirectoryTempAuto());
        File[] files = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().indexOf(mAuto.uid + "(2).jpg") != -1;
            }
        });
        if(files==null){
            return;
        }
        for (final File file : files) {
            if (!file.exists()) continue;

            final ImageView view = (ImageView) LayoutInflater.from(this).inflate(R.layout.image_view_present, null);

            Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
            Bitmap res = RotateImage.rotationImage(bitmap, file.getAbsolutePath());
            if (res == null) {
                res = bitmap;
            }
            view.setImageBitmap(res);
            view.setTag(file.getPath());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str = (String) view.getTag();
                    File file = new File(str);
                    if (file.exists() == false) {

                        Toast.makeText(AutoCloseActivity.this, "Файла не существует", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        String as = "file://" + "/" + str;
                        intent.setDataAndType(Uri.parse(as), "image/*");
                        AutoCloseActivity.this.startActivity(intent);
                    }
                }
            });


            view.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                    menu.add(R.string.delete_photo).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            final String str = (String) view.getTag();
                            final File file = new File(str);
                            if (file.exists()) {

                                if (file.delete()) {
                                    showPhoto();
                                }
                            }
                            return true;
                        }
                    });

                }
            });

            panelPhoto.addView(view);
        }
        if (panelPhoto.getChildCount() == 0) {
            buttonFoto.setVisibility(View.VISIBLE);
        } else {
            buttonFoto.setVisibility(View.GONE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void save() {


        String zz = editTextKM.getText().toString().trim();
        if (zz.length() == 0) {
            editTextKM.setError(getString(R.string.saasfasf));
            Utils.messageBox(getString(R.string.error), getString(R.string.saasfasf), AutoCloseActivity.this, null);
            return;

        } else {
            try {
                mAuto.km2 = Integer.parseInt(editTextKM.getText().toString().trim());
            } catch (Exception ex) {
                log.error(ex);
                editTextKM.setError(ex.getMessage());
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                mAuto.km2 = 0;
                return;
            }

        }

        zz = editTextAddress.getText().toString().trim();
        if (zz.length() == 0) {
            editTextAddress.setError(getString(R.string.dasdasddsd));
            Utils.messageBox(getString(R.string.error), getString(R.string.dasdasddsd), AutoCloseActivity.this, null);
            return;
        } else {
            mAuto.address2 = editTextAddress.getText().toString().trim();
        }


        if (panelPhoto.getChildCount() != 1) {
            Utils.messageBox(getString(R.string.error), getString(R.string.wqeqweqwe), this, null);
            return;
        }

        if (mAuto.km2 < mAuto.km1) {
            Utils.messageBox(getString(R.string.error), getString(R.string.auuisdsd), this, null);
            return;
        }

        if ((mAuto.km2 - mAuto.km1) > mSettings.max_km) {
            Utils.messageBox(getString(R.string.error), "Вы действительно проехали " + String.valueOf(mAuto.km2 - mAuto.km1) + " км?", this, new IActionE() {
                @Override
                public void action(Object o) {
                    commitList();
                }
            });
            return;
        }

        commitList();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void commitList() {
        mAuto.foto2 = getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(2).jpg";

        mAuto.date2 = Utils.curDate();


        mAuto.isSender = true;
        try {

            Utils.copyDirectoryOneLocationToAnotherLocation(
                    new File(AutoOpenActivity.getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(2).jpg"),
                    new File(AutoOpenActivity.getPhotoDirectoryAuto() + "/" + mAuto.uid + "(2).jpg"));
            Configure.getSession().update(mAuto);

            Configure.getSession().update(mAuto);

            Settings.save();
            sendBroadcast(new Intent(MainActivity.FAB));
            setResult(Activity.RESULT_OK);
            finish();
        } catch (Exception ex) {

            log.error(ex);
        }
    }
}
