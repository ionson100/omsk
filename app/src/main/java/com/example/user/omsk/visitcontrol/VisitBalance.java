package com.example.user.omsk.visitcontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogEditCountProduct;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.R;
import com.example.user.omsk.RecommendationBalance;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;

import java.util.Collections;
import java.util.Comparator;

public class VisitBalance extends LinearLayout {


    public IActionE<Object> iActionE;
    private Settings mSettings;
    private LinearLayout mListView;
    private Activity activity;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public VisitBalance(Context context) {
        super(context);
        init();
    }

    public VisitBalance(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VisitBalance(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = vi.inflate(R.layout.visit_control_balance, null);

        mView.findViewById(R.id.bt_context_menu).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.openContextMenu(VisitBalance.this);
                // VisitSale.this.showContextMenu();
            }
        });


        this.addView(mView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        this.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {


                                                @Override
                                                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {



                                                    String name = "";
                                                    if (mSettings.getVisit().point.isRecomendationBalance) {
                                                        name = getContext().getString(R.string.recommendations);
                                                    } else {
                                                        name = getContext().getString(R.string.recommendations_delete2);
                                                    }


                                                    menu.add(name).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

                                                        @Override
                                                        public boolean onMenuItemClick(MenuItem item) {
                                                            if (mSettings.getVisit().point.isRecomendationBalance) {

                                                                boolean d = new RecommendationBalance(mSettings).conpute();
                                                                if (d) {
                                                                    mSettings.getVisit().point.isRecomendationBalance = false;
                                                                }
                                                            } else {
                                                                new RecommendationBalance(mSettings).deCompute();
                                                                mSettings.getVisit().point.isRecomendationBalance = true;
                                                            }
                                                            activateList();
                                                            Settings.save();
                                                            return true;
                                                        }
                                                    });
                                                    menu.add(R.string.add_product).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                                        @Override
                                                        public boolean onMenuItemClick(MenuItem item) {
                                                            Settings.showFragment(StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE, activity);
                                                            return false;
                                                        }
                                                    });
                                                }
                                            }

        );
        mSettings = Settings.core();

        if (mSettings.getVisit() == null) {
            Settings.showFragment(StateSystem.HOME, activity);

        }

        mListView = (LinearLayout) mView.findViewById(R.id.listView_balance_stock);


        activateList();
    }

    private void deleteProductAll() {
        mSettings.getVisit().selectProductForBalance.clear();
        Settings.save();
    }


    private void printableButtonText(Button v) {
        if (mSettings.getVisit().point.isRecomendationBalance) {
            v.setText(R.string.recommendations);
        } else {
            v.setText(R.string.recommendations_delete);
        }
    }

    private void activateList() {//

        mListView.removeAllViews();
        Collections.sort(mSettings.getVisit().selectProductForBalance, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        for (final MProduct mProduct : mSettings.getVisit().selectProductForBalance) {


            View v = LayoutInflater.from(getContext()).inflate(R.layout.item_list_product_selected_for_balanse_new_page, null);
            final TextView tt1 = (TextView) v.findViewById(R.id.name_poduct);
            final TextView tt2 = (TextView) v.findViewById(R.id.balance_count_poduct);
            if (tt1 != null) {
                tt1.setText(mProduct.name);
            }
            if (tt2 != null) {
                tt2.setText(Utils.getStringDecimal(mProduct.getAmount_core()));
            }
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tt1.setBackgroundResource(R.color.colore97);
                    tt2.setBackgroundResource(R.color.colore97);
                    DialogEditCountProduct product = new DialogEditCountProduct();
                    product.iActionEForDebt = new IActionE<Object>() {
                        @Override
                        public void action(Object o) {
                            activateList();
                        }
                    };
                    product.setDismis(new IActionE<Object>() {
                        @Override
                        public void action(Object o) {
                            tt1.setBackgroundResource(R.color.colore3);
                            tt2.setBackgroundResource(R.color.colore3);
                        }
                    });
                    product.setProduct(mProduct);
                    product.show(((MainActivity) activity).getSupportFragmentManager(), "adsfyusdfsdf");
                }
            });
            mListView.addView(v);
        }
        if (iActionE != null) {
            iActionE.action(null);
        }
    }

    public void refreshList() {

    }


    private void deleteProduct(String id) {
        MProduct del = null;
        for (MProduct product : mSettings.getVisit().selectProductForBalance) {
            if (product.idu.equals(id)) {
                del = product;
            }
        }
        if (del != null) {
            mSettings.getVisit().selectProductForBalance.remove(del);
            Settings.save();
        }
    }

}
