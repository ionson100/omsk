package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;

import com.example.user.omsk.refill.RefillStock;

public class StartIntentLack {
    public static void start(Settings mSettings, Activity activity) {
        if (ValidateDay.validate(mSettings, activity)) {
            Intent intent = new Intent(activity, RefillStock.class);
            activity.startActivity(intent);
        }
    }
}
