package com.example.user.omsk.senders;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitStory;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TempVisit {

   static TempVisit getTempVisit(MVisitStory d) {
       TempVisit t = new TempVisit();
       t.gifts=d.gifts;
       t.id = d.id;
       t.locations = d.stateLocation;
       t.distance = d.distance;
       t.pressure = d.pressure;

       t.idu = d.idu;
       t.refund = d.refund;
       t.uuid = d.uuid;
       t.payment = d.payment;
       t.point_id = d.point_id;
       t.connterpirty_id = d.connterpirty_id;
       t.finish = d.finish;
       t.start = d.start;
       t.latitude = d.latitude;
       t.longitude = d.longitude;
       t.visit_result_id = d.visit_result_id;
       t.comment = d.comment;
       t.member_action = d.member_action;
       t.concurent_actions = d.concurent_actions;
       t.order_type_id = d.order_type_id;
       t.index = 200;
       t.visit_id_list = d.debtList;
       for (MProduct product : d.getSalesProductList()) {
           TempProductSender p = new TempProductSender();
           p.sku = product.sku;
           p.product_id = product.idu;
           p.amount = product.getAmount_core();
           p.price = product.price;
           t.sales_product_list.add(p);
       }
       for (MProduct product : d.getBalanceProductList()) {
           TempProductSender p = new TempProductSender();
           p.sku = product.sku;
           p.product_id = product.idu;
           p.amount = product.getAmount_core();
           p.price = product.price;
           t.stock_product_list.add(p);
       }
       for (MProduct product : d.getFreeProductList()) {
           TempProductSender p = new TempProductSender();
           p.sku = product.sku;
           p.product_id = product.idu;
           p.amount = product.getAmount_core();
           p.price = product.price;
           p.permit_id = product.rule_id;
           t.free_product_list.add(p);
       }
       for (MProduct product : d.getActionProductList()) {
           TempProductSender p = new TempProductSender();
           p.sku = product.sku;
           p.product_id = product.idu;
           p.amount = product.getAmount_core();
           p.price = product.price;
           p.permit_id = product.rule_id;
           t.action_product_list.add(p);
       }

       for (MProduct product : d.getNewOrderProductList()) {
           TempProductSender p = new TempProductSender();
           p.sku = product.sku;
           p.product_id = product.idu;
           p.amount = product.getAmount_core();
           p.price = product.price;
           p.permit_id = product.rule_id;
           t.newOrder.add(p);
       }


       Collections.sort(d.getCheckList(), new Comparator<MChecksData>() {
           @RequiresApi(api = Build.VERSION_CODES.KITKAT)
           @Override
           public int compare(MChecksData lhs, MChecksData rhs) {
               return Integer.compare(lhs.checkNumber, rhs.checkNumber);
           }
       });
       for (MChecksData checksData : d.getCheckList()) {
           TempCheckSender tempCheck = new TempCheckSender();
           tempCheck.email = checksData.email;
           tempCheck.notify = checksData.notify;
           if (checksData.notify == 1) {
               tempCheck.sms = checksData.sms;
               tempCheck.email = null;
           }
           if (checksData.notify == 2) {
               tempCheck.sms = null;
               tempCheck.email = checksData.email;
           }
           tempCheck.uuid_check_delete = checksData.checkUUIDDelete;
           tempCheck.idu = checksData.idu;
           tempCheck.is_returned = checksData.isReturn;
           tempCheck.number_check_delete = checksData.checkNumberDelete;
           tempCheck.type_check = checksData.type_check;
           tempCheck.date = checksData.date;
           tempCheck.number_check = checksData.checkNumber;
           tempCheck.number_doc = checksData.docNumber;
           tempCheck.order_id = checksData.order_debt_id;
           tempCheck.number_kkm = checksData.kmmNumber;
           tempCheck.price_check = checksData.totalPrice;
           tempCheck.amount_check = checksData.totalAmount;
           for (MProductBase product : checksData.productList) {
               TempProductSender tempProduct = new TempProductSender();
               tempProduct.sku = product.sku;
               tempProduct.price = product.price;
               tempProduct.amount = product.getAmount_core();
               tempProduct.product_id = product.idu;
               tempProduct.permit_id = product.rule_id;
               tempCheck.productList.add(tempProduct);
           }
           t.tempCheckList.add(tempCheck);
       }
       return t;
   }

   @SerializedName("locations")
   public int locations;

   @SerializedName("distance")
   public int distance;

   @SerializedName("refunds")
   public List<String> visit_id_list = new ArrayList<>();

   public int index = 200;



   @SerializedName("member_action")
   public String member_action;


   @SerializedName("concurent_actions")
   public String concurent_actions;

   @SerializedName("pressure")
   public int pressure;

   @SerializedName("route_index")
   public int id;

   @SerializedName("id")
   public String idu;

   @SerializedName("refund")
   public double refund;

   @SerializedName("uuid")
   public String uuid;

   @SerializedName("payment")
   public double payment = -1;

   @SerializedName("point_id")
   public String point_id;

   @SerializedName("counterparty_id")
   public String connterpirty_id;

   @SerializedName("start")
   public int start;

   @SerializedName("finish")
   public int finish;

   @SerializedName("latitude")
   public double latitude;

   @SerializedName("longitude")
   public double longitude;

   @SerializedName("visit_result_id")
   public String visit_result_id;

   @SerializedName("comment")
   public String comment;

   @SerializedName("order_type")
   public String order_type_id;

   @SerializedName("sale")
   public List<TempProductSender> sales_product_list = new ArrayList<>();//MSaleRowsStoryBase

   @SerializedName("stock")
   public List<TempProductSender> stock_product_list = new ArrayList<>();// история склада//MStockRowsStoryBase

   @SerializedName("presents")
   public List<TempProductSender> free_product_list = new ArrayList<>();//MSaleRowsStoryPresentBase

   @SerializedName("promo")
   public List<TempProductSender> action_product_list = new ArrayList<>();//MActionProduct

   public boolean gps_status;

   @SerializedName("check_list")
   public List<TempCheckSender> tempCheckList = new ArrayList<>();

   @SerializedName("new_order")
   public List<TempProductSender> newOrder = new ArrayList<>();
    @SerializedName("gifts")
    public List<MVisitPresent> gifts =new ArrayList<>();
}
