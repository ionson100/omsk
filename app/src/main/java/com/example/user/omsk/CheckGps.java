package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;

class CheckGps {
    public static boolean check(FVisitCommit fVisit) {
        final LocationManager manager = (LocationManager) fVisit.getActivity().getSystemService(Context.LOCATION_SERVICE);

        boolean res = false;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {//
            showGPSDisabledAlertToUser(fVisit.getActivity());
        } else {
            res = true;
        }
        return res;
    }

    private static void showGPSDisabledAlertToUser(final Activity activity) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setMessage(activity.getString(R.string.kjaksdsd))
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.siduiuddsd),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Отказаться",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}
