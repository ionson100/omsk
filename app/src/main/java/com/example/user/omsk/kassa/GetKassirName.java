package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

public class GetKassirName {

    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(GetKassirName.class);
    public static void check(final TextView log, final Activity activity, final Settings mSettings, final IActionE iActionE) {

        new AsyncTask<Void, String, Void>() {
            private String errorText = "";
            private ProgressDialog dialog;

            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    fptr = new Fptr();
                    fptr.create(activity.getApplication());

                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError();
                    }
                    if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                        checkError();
                    }
                    if (fptr.SetMode() < 0) {
                        checkError();
                    }
                    if (fptr.put_CaptionPurpose(118) < 0) {
                        checkError();
                    }
                    if (fptr.GetCaption() < 0) {
                        checkError();
                    }
                    errorText = fptr.get_Caption();

                    publishProgress("OK");
                } catch (Exception ex) {

                    logE.error(ex);
                    publishProgress(ex.getMessage());
                    errorText = ex.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber +
                            "\n" + "Ошибка ккм file  - GetKassirName\n" + ex.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                log.setText("");
                dialog = Utils.factoryDialog(activity, "Провека имени кассира", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                iActionE.action(errorText);
                log.setText("");
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }


        }.execute();
    }
}

