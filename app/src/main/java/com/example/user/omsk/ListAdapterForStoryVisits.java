package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListAdapterForStoryVisits extends ArrayAdapter<MVisitStoryBase> {

    Map<Integer, View> map = new HashMap<>();
    View itogo;
    private final List<MVisitStoryBase> mStoryVisitPoints;
    private List<MProduct> mProducts;
    private boolean isStoryDey;
    private boolean historyCore;
    private List<MOrderType> mOrderTypes;
    private List<MVisitResults> mVisitResults;
    private Settings mSettings;

    public ListAdapterForStoryVisits(Context context, int resource, List<MVisitStoryBase> objects, boolean isShowAllVisit, boolean isStoryDey) {
        super(context, resource, objects);
        this.mStoryVisitPoints = objects;
        this.isStoryDey = isStoryDey;
        ISession session = Configure.getSession();
        this.mProducts = session.getList(MProduct.class, null);
        mOrderTypes = session.getList(MOrderType.class, null);
        mVisitResults = session.getList(MVisitResults.class, null);
        mSettings = Settings.core();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;

        final MVisitStoryBase p = getItem(position);
        if (p != null) {
            if (p.id != 0) {
                if (map.containsKey(p.id)) {
                    mView = map.get(p.id);
                } else {
                    mView = createView(p);
                    map.put(p.id, mView);
                }

            } else {
                if (itogo == null) {
                    if (isStoryDey) {
                        itogo = mView = UtilsViewBuilder.getItemTotal(getContext(), mStoryVisitPoints, Itogoficator.storyDey, historyCore);
                    } else {
                        itogo = mView = UtilsViewBuilder.getItemTotal(getContext(), mStoryVisitPoints, Itogoficator.storyPoint, historyCore);
                    }
                }
                return itogo;
            }
        }
        return mView;
    }

    private View createView(MVisitStoryBase point) {
        if (mSettings.showOldViewForVisit == false) {
            View s = UtilsViewBuilderNew.getItemStoryVisit(point, getContext(), mProducts, historyCore, false, (Activity) getContext());
            return s;
        } else {
            View s = UtilsViewBuilder.getItemStoryVisit(mOrderTypes, mVisitResults, point, getContext(), mProducts, historyCore, false, null, false);
            return s;
        }
    }

    public void setHistoryCore(boolean historyCore) {
        this.historyCore = historyCore;
    }
}
