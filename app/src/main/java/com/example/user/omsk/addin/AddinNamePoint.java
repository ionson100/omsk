package com.example.user.omsk.addin;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.RegsAddress;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;


public class AddinNamePoint {

    private static final String divide = "  ";
    public static final String divide2 = " # ";

    private String[] arraySpinner;
    private Spinner spinner_ooo;
    EditText namePoint, filial, namePointCore;
    private View mView;
    private Activity mActivity;
    private MPoint mPoint;

    public void activate(final View mView, Activity mActivity, final MPoint mPoint) {
        this.mView = mView;
        this.mActivity = mActivity;
        this.mPoint = mPoint;

        this.arraySpinner = new String[]{
                "ИП", "ООО", "ЗАО", "ПАО"
        };
        spinner_ooo = (Spinner) mView.findViewById(R.id.name_point_ooo_type);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, arraySpinner);
        spinner_ooo.setAdapter(adapter);
        namePoint = (EditText) mView.findViewById(R.id.name_point_ooo_name);
        namePointCore = (EditText) mView.findViewById(R.id.e_name);
        filial = (EditText) mView.findViewById(R.id.name_point_ooo_filial);

        final boolean[] run = {false};
        spinner_ooo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if (!run[0]) {
                    run[0] = true;
                    return;
                }
                String n1 = namePoint.getText().toString().trim();
                if (n1.length() > 0) {

                    n1 = n1.replace("\"", "").replace("'", "").replace("`", "");
                    n1 = n1.substring(0, 1).toUpperCase() + n1.substring(1);

                    String n2 = arraySpinner[spinner_ooo.getSelectedItemPosition()];
                    String n3 = filial.getText().toString();
                    String address = RegsAddress.getClearAddres(mPoint.address);
                    String ss = n1 + divide + n2 + divide + n3 + divide2 + address;
                    namePointCore.setText(ss);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        pareslable(mPoint.name);


        namePoint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().indexOf("  ") != -1) {
                    namePoint.setText(s.toString().replace("  ", ""));
                }
             //   String sd = namePointCore.getText().toString();

                String n1 = namePoint.getText().toString().trim();
                n1 = n1.replace("\"", "").replace("'", "").replace("`", "");
                if (n1.length() > 1) {
                    n1 = n1.substring(0, 1).toUpperCase() + n1.substring(1);
                }


                String n2 = arraySpinner[spinner_ooo.getSelectedItemPosition()];
                String n3 = filial.getText().toString();
                String address = RegsAddress.getClearAddres(mPoint.address);
                namePointCore.setText(n1 + divide + n2 + divide + n3 + divide2 + address);


            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }

    public void pareslable(String df) {

        if (df == null) {
            filial.setText(Settings.core().city);
            return;
        }

        int dd = df.indexOf(divide2);
        if (dd == -1) {
            filial.setText(Settings.core().city);
            return;
        }


        String[] d = df.split(divide2);


        if (d.length == 1 || d.length == 2) {
            String[] sFirst = d[0].split(divide);
            if (sFirst.length != 3) {
                filial.setText(Settings.core().city);
                return;
            }

            namePoint.setText(sFirst[0]);
            if (sFirst[1].toUpperCase().equals("ИП")) {
                spinner_ooo.setSelection(0);
            }
            if (sFirst[1].toUpperCase().equals("ООО")) {
                spinner_ooo.setSelection(1);
            }
            if (sFirst[1].toUpperCase().equals("ЗАО")) {
                spinner_ooo.setSelection(2);
            }
            if (sFirst[1].toUpperCase().equals("ПАО")) {
                spinner_ooo.setSelection(3);
            }
            filial.setText(sFirst[2]);

        } else {
            filial.setText(Settings.core().city);
        }

    }


    public boolean validate() {
        if (namePoint.getText().toString().trim().length() == 0) {
            Utils.messageBox(mActivity.getString(R.string.error), mActivity.getString(R.string.assasssas), mActivity, null);
            return false;
        }
        if (mPoint.address.length() == 0) {
            Utils.messageBox(mActivity.getString(R.string.error), mActivity.getString(R.string.sadasdd), mActivity, null);
            return false;
        }

        return true;
    }

    public boolean validateNamePoint(String name) {
        if (name == null) {
            return false;
        }
        if (name.indexOf(divide2) == -1) {
            return false;
        }
        String[] s = name.split(divide2);
        if (s.length != 2) {
            return false;
        }
        if (s[0] == null || s[0].trim().length() == 0) {
            return false;
        }
        if (s[1] == null || s[1].trim().length() == 0) {
            return false;
        }
        String[] s1 = s[0].split(divide);
        if (s1.length != 3) {
            return false;
        }
        if (s1[0] == null || s1[0].trim().length() == 0) {
            return false;
        }
        if (s1[1] == null || s1[1].trim().length() == 0) {
            return false;
        }
        if (s1[2] == null || s1[2].trim().length() == 0) {
            return false;
        }
        return true;
    }
}
