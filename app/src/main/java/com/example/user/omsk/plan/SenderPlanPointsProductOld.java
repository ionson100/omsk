package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.IActionResponse;
import com.example.user.omsk.senders.UtilsSender;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SenderPlanPointsProductOld {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderPlanPointsProductOld.class);
    private Settings mSettings;
    private Activity mActivity;
    private IActionE mIAction;

    public void send(Settings mSettings, Activity activity, IActionE iAction) {
        this.mSettings = mSettings;
        this.mActivity = activity;
        this.mIAction = iAction;
        new SenderWorkerTack().execute();
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {



        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(mActivity, mSettings)) {
                cancel(true);
                Toast.makeText(mActivity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(mActivity, mActivity.getString(R.string.get_data), null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (aBoolean == null) {
                mIAction.action(null);
            } else {
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/plan_history_client/:  " + aBoolean;
                Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
                log.error(msg);
                //new SenderErrorPOST().send(mActivity, msg, Settings.core());

            }
            if (dialog != null) {
                dialog.cancel();
            }
            if (aBoolean == null) {
                ISession ses;
                ses = Configure.getSession();
                List<MTemplatePropety> mTemplatePropetyList = ses.getList(MTemplatePropety.class, null);
                if (mTemplatePropetyList.size() > 0) {
                    MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
                    mTemplatePropety.isSendPlanOld = true;
                    ses.update(mTemplatePropety);
                }
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            if (isCancelled()) return null;
            final String[] result = {null};
            String str = "";
            try {
                str = Utils.HTTP + mSettings.url + "/plan_history_client/?guid=" +
                        URLEncoder.encode(SettingsUser.core().getUuidDevice(), "UTF-8");
            } catch (UnsupportedEncodingException e) {

                return e.getMessage();
            }

            result[0] = UtilsSender.getRequest((MyApplication) mActivity.getApplication(), str, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {

                    if (str.toLowerCase().equals("ok")) {
                    } else {
                        App(str);
                    }

                }
            });
            return result[0];
        }

        private void App(String res) {
            Gson sd3 = Utils.getGson();
            TempHeader df = sd3.fromJson(res, TempHeader.class);
            ISession ses = Configure.getSession();

            try {
                ses.beginTransaction();
                ses.execSQL(" DELETE FROM old_plan_points ");
                ses.execSQL(" DELETE FROM old_plan_pair ");
                ses.execSQL(" DELETE FROM mml_old ");
                List<MPlanPointProductsOld> list1 = new ArrayList<>();
                for (String s : df.products.keySet()) {

                    for (String s1 : df.products.get(s).keySet()) {
                        MPlanPointProductsOld f = new MPlanPointProductsOld();
                        f.point_id = s;
                        f.product_id = s1;
                        f.amount = df.products.get(s).get(s1);
                        list1.add(f);
                    }
                }

                Configure.bulk(MPlanPointProductsOld.class, list1, ses);

                for (String s : df.money.keySet()) {
                    MPlanPairOld d = new MPlanPairOld();
                    d.point_id = s;
                    d.price = df.money.get(s);
                    ses.insert(d);
                }
                if (df.plan != null) {
                    ses.insert(df.plan);
                    df.plan.insertMML(ses);
                }


                Map<String, Double[]> maps = df.map;

                for (Map.Entry<String, Double[]> ss : maps.entrySet()) {
                    ModelMMLOld mml = new ModelMMLOld();
                    mml.mmlName = ss.getKey().trim();
                    mml.mmlPachek = ss.getValue()[0];
                    mml.mmlPoint = ss.getValue()[1];
                    ses.insert(mml);
                }

                ses.commitTransaction();
            } finally {
                ses.endTransaction();
            }
        }
    }

    class TempHeader {
        public final Map<String, Double> money = new HashMap<>();
        public final Map<String, Map<String, Double>> products = new HashMap<>();
        public PlanEOld plan;
        public Map<String, Double[]> map = new HashMap<>();
    }
}
