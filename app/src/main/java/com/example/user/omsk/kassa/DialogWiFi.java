package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;


public class DialogWiFi extends DialogFragment {

    private RadioButton work1RadioButton;
    private RadioButton work2RadioButton;
    private RadioButton cropto0RadioButton;
    private RadioButton cropto1RadioButton;
    private RadioButton cropto2RadioButton;
    private RadioButton cropto3RadioButton;
    private RadioButton cropto4RadioButton;

    private IActionE mIActionE;
    private GetWiFiSettings.WifiSettings wifiSet;

    public void setmIActionE(IActionE mIActionE) {
        this.mIActionE = mIActionE;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_wi_fi, null);
        final EditText userEditText = (EditText) mView.findViewById(R.id.wifi_user);
        mView.findViewById(R.id.availablewifi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogWiFiAvialable avialable = new DialogWiFiAvialable();
                avialable.iActionE = new IActionE<String>() {
                    @Override
                    public void action(String o) {
                        userEditText.setText(o);
                    }
                };
                avialable.show(getActivity().getSupportFragmentManager(), "asddasd");

            }
        });


        mView.findViewById(R.id.settingswifi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });


        ((TextView) mView.findViewById(R.id.title_dalog)).setText(R.string.aasdasddsdsdsd);
        builder.setView(mView);
        final EditText editTextChanel = (EditText) mView.findViewById(R.id.wifi_number_chanel);

        final EditText passwordEditText = (EditText) mView.findViewById(R.id.wifi_password);
        work1RadioButton = (RadioButton) mView.findViewById(R.id.wifi_work1);
        work2RadioButton = (RadioButton) mView.findViewById(R.id.wifi_work2);
        cropto0RadioButton = (RadioButton) mView.findViewById(R.id.wifi_cripto1);
        cropto1RadioButton = (RadioButton) mView.findViewById(R.id.wifi_cripto2);
        cropto2RadioButton = (RadioButton) mView.findViewById(R.id.wifi_cripto3);
        cropto3RadioButton = (RadioButton) mView.findViewById(R.id.wifi_cripto4);
        cropto4RadioButton = (RadioButton) mView.findViewById(R.id.wifi_cripto5);

        work1RadioButton.setChecked(false);
        work2RadioButton.setChecked(false);
        cropto0RadioButton.setChecked(false);
        cropto1RadioButton.setChecked(false);
        cropto2RadioButton.setChecked(false);
        cropto3RadioButton.setChecked(false);
        cropto4RadioButton.setChecked(false);
        userEditText.setText(wifiSet.user);
        passwordEditText.setText((wifiSet.password));
        editTextChanel.setText(String.valueOf(wifiSet.chanel));
        if (wifiSet.point == 1) {
            work1RadioButton.setChecked(true);
        }
        if (wifiSet.point == 2) {
            work2RadioButton.setChecked(true);
        }

        if (wifiSet.cripto == 0) {
            cropto0RadioButton.setChecked(true);
        }
        if (wifiSet.cripto == 1) {
            cropto1RadioButton.setChecked(true);
        }
        if (wifiSet.cripto == 2) {
            cropto2RadioButton.setChecked(true);
        }
        if (wifiSet.cripto == 3) {
            cropto3RadioButton.setChecked(true);
        }
        if (wifiSet.cripto == 4) {
            cropto4RadioButton.setChecked(true);
        }
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mView.findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wifiSet.user = userEditText.getText().toString();
                wifiSet.password = passwordEditText.getText().toString();

                int chanel = 0;
                try {
                    chanel = Integer.parseInt(editTextChanel.getText().toString());
                } catch (Exception ignored) {
                }
                if (chanel > 14) {
                    Utils.messageBox(getString(R.string.error), getString(R.string.bcvbbbb), getActivity(), null);
                    return;
                }
                wifiSet.chanel = chanel;

                if (work1RadioButton.isChecked()) {
                    wifiSet.point = 1;
                }
                if (work2RadioButton.isChecked()) {
                    wifiSet.point = 2;
                }
                //////////
                if (cropto0RadioButton.isChecked()) {
                    wifiSet.cripto = 0;
                }
                if (cropto1RadioButton.isChecked()) {
                    wifiSet.cripto = 1;
                }
                if (cropto2RadioButton.isChecked()) {
                    wifiSet.cripto = 2;
                }
                if (cropto3RadioButton.isChecked()) {
                    wifiSet.cripto = 3;
                }
                if (cropto4RadioButton.isChecked()) {
                    wifiSet.cripto = 4;
                }
                if (mIActionE != null) {
                    mIActionE.action(wifiSet);
                }
                dismiss();
            }
        });
        return builder.create();
    }

    public void setWifiSet(GetWiFiSettings.WifiSettings wifiSet) {
        this.wifiSet = wifiSet;
    }
}

