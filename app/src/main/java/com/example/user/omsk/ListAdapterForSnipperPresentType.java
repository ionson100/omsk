package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.omsk.models.MFreePresentTYpes;

import java.util.List;

public class ListAdapterForSnipperPresentType extends ArrayAdapter<MFreePresentTYpes> {

    private final int mResource;

    public ListAdapterForSnipperPresentType(Context context, int resource, List<MFreePresentTYpes> objects) {
        super(context, resource, objects);
        this.mResource = resource;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;
        final MFreePresentTYpes p = getItem(position);
        if (p != null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            mView = vi.inflate(mResource, null);
            TextView ee = (TextView) mView.findViewById(R.id.qwqw);
            ee.setText(p.name);

        }
        return mView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mView = convertView;
        final MFreePresentTYpes p = getItem(position);
        if (p != null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            mView = vi.inflate(R.layout.simple_spinner_item_present, null);
            TextView ee = (TextView) mView.findViewById(R.id.qwqw);
            ee.setText(p.name);
        }
        return mView;
    }
}
