package com.example.user.omsk.setting.colorpicker.builder;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.setting.colorpicker.ColorPickerView;
import com.example.user.omsk.setting.colorpicker.renderer.ColorWheelRenderer;
import com.example.user.omsk.setting.colorpicker.renderer.FlowerColorWheelRenderer;
import com.example.user.omsk.setting.colorpicker.renderer.SimpleColorWheelRenderer;

public class ColorWheelRendererBuilder {
	public static ColorWheelRenderer getRenderer(ColorPickerView.WHEEL_TYPE wheelType) {
		switch (wheelType) {
			case CIRCLE:
				return new SimpleColorWheelRenderer();
			case FLOWER:
				return new FlowerColorWheelRenderer();
		}
		throw new IllegalArgumentException("wrong WHEEL_TYPE");
	}
}