package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.models.MAgent;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MStockRows;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.kassa.UtilsKassa;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.PupperPartial;

import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class FVisitSale extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FVisitSale.class);

    ListAdapterForSelectedProducts adapter;
    private List<MStockRows> mStockRowsList;
    private PupperPartial mTotalProduct;
    private PupperPartial mTotalAmount;
    private PupperPartial mTotalPrice;
    private ListView mListView;
    private View mView;
    private MPoint mPoint;
    private MAgent mAgent;
    private Settings mSettings;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mSettings = Settings.core();
        mView = inflater.inflate(R.layout.fragment_visit_sale, container, false);
        try {
            /////////////////////////////////////////////////////////////////////////////////////////
            Button recomendation = (Button) mView.findViewById(R.id.button_recommendations);
            printableButtonText(recomendation);

            mStockRowsList = Configure.getSession().getList(MStockRows.class, " point_id = ? ", mSettings.getVisit().point.idu);
            recomendation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSettings.getVisit().point.isRecomendationSale) {
                        if (validateRecomendation()) {

                            boolean d = new RecommendationSale(mSettings, getActivity()).compute();

                            if (d) {
                                mSettings.getVisit().point.isRecomendationSale = false;
                            }
                        }
                    } else {
                        new RecommendationSale(mSettings, getActivity()).deConpute();
                        mSettings.getVisit().point.isRecomendationSale = true;
                    }
                    Settings.save();
                    printableButtonText((Button) v);
                    activateListProduct();
                }

                private boolean validateRecomendation() {
                    if (mStockRowsList.size() == 0) {
                        Utils.messageBox(getString(R.string.recommendations), getString(R.string.error_recomendation1), getActivity(), null);
                        return false;
                    } else {
                        if (mStockRowsList.get(0).date != mSettings.getVisit().point.lastDateVisit) {
                            Utils.messageBox(getString(R.string.recommendations), getString(R.string.error_recomendation1), getActivity(), null);
                            return false;
                        }
                    }
                    return true;
                }
            });
            //////////////////////////////////////////////////////////////////////////////////////

            if (mSettings.getVisit() == null) {
                Settings.showFragment(StateSystem.HOME, getActivity());
                return mView;
            }
            LinearLayout panelPoint = (LinearLayout) mView.findViewById(R.id.panelPointrequest);
            registerForContextMenu(panelPoint);
            mTotalProduct = (PupperPartial) mView.findViewById(R.id.reqiest_total_product);
            mTotalAmount = (PupperPartial) mView.findViewById(R.id.reqiest_total_count);
            mTotalPrice = (PupperPartial) mView.findViewById(R.id.reqiest_total_price_text);
            mPoint = mSettings.getVisit().point;
            mAgent = mSettings.getVisit().point.getContragent(Configure.getSession());
            Button btsave = (Button) mView.findViewById(R.id.button_regiest_ok);
            btsave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                }
            });

            Button btSelectProduct = (Button) mView.findViewById(R.id.button_regiest_add_sale_product);
            btSelectProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE, getActivity());
                }
            });
            Bind();
            activateListProduct();
            Transceiver.subscribe(TransceiveStaticVariables.F2323, new Transceiver.ITransceiver() {
                @Override
                public void action(Object o) {
                    if (o != null) {
                        MProduct prod = null;
                        for (MProduct product : mSettings.getVisit().selectProductForSale) {
                            if (product.get_id().equals(o.toString())) {
                                prod = product;
                            }
                        }
                        if (prod != null) {
                            mSettings.getVisit().selectProductForSale.remove(prod);
                        }
                    } else {
                        mSettings.getVisit().selectProductForSale.clear();
                    }
                    Settings.save();
                    activateListProduct();
                }
            });
            /////////////////////////////////////////////////////////////////добавлен фрагмент плана
            try {
                MakerPlanForVisitSale.make(mPoint, getActivity(), mView);
            } catch (Exception ignore) {
                log.error(ignore);
                new SenderErrorPOST().send((MyApplication) getActivity().getApplication(), "plan_sel:" + ignore.getMessage());
            }
            /////////////////////////////////////////////////////////////////////////////////////////////
            if (UtilsKassa.getMCheckDataCache(mSettings.getVisit().checksDataList) != null) {
                mView.findViewById(R.id.button_recommendations).setVisibility(View.GONE);
                mView.findViewById(R.id.button_regiest_add_sale_product).setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            Utils.sendMessage("FVisitSale:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            log.error(ex);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
        }
        return mView;
    }

    private void printableButtonText(Button v) {
        if (mSettings.getVisit().point.isRecomendationSale) {
            v.setText(R.string.recommendations);
        } else {
            v.setText(R.string.recommendations_delete);
        }
    }


    @SuppressLint("SetTextI18n")
    private void activateListProduct() {
        mListView = (ListView) mView.findViewById(R.id.list_selectted_product);
        if (mSettings.getVisit().selectProductForSale != null) {
            Collections.sort(mSettings.getVisit().selectProductForSale, new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            adapter = new ListAdapterForSelectedProducts(getActivity(),
                    R.layout.item_list_product_selected_for_sale_1, mSettings.getVisit().selectProductForSale, new IActionE<View>() {
                @Override
                public void action(View v) {

                    // adapter.notifyDataSetChanged();

                }
            });
            mListView.setAdapter(adapter);
            mListView.setSelection(mSettings.getVisit().scroll_sale_list);
            mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    mSettings.getVisit().scroll_sale_list = firstVisibleItem;
                }
            });


            mTotalProduct.setPairString(getString(R.string.itogo_poduct_naimenovany), String.valueOf(mSettings.getVisit().selectProductForSale.size()));
            double count = 0;
            double price = 0;
            for (MProduct mProduct : mSettings.getVisit().selectProductForSale) {
                count = count + mProduct.getAmount_core();
                price = price + mProduct.getTotalPrice();
            }
            mTotalAmount.setPairString(getString(R.string.itogo_count_products1), Utils.getStringDecimal(count));
            mTotalPrice.setPairString(getString(R.string.itogo_price3), Utils.getStringDecimal(price) + getString(R.string.rub));
        }
    }


    private void Bind() {
        if (mPoint != null) {
            ((TextView) mView.findViewById(R.id.request_point_name)).setText(mPoint.name);
        }
        if (mAgent != null) {
            ((PupperPartial) mView.findViewById(R.id.request_agent_name)).setPairString("Агент: ", mAgent.name);
        }
    }

    @Override
    public void onDestroy() {
        Transceiver.cancelSubscribe(TransceiveStaticVariables.F2323);
        super.onDestroy();
    }
}
