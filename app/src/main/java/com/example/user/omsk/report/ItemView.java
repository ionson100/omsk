package com.example.user.omsk.report;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.TempSale;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.models.DebtDuplet;
import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MFreePresentTYpes;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MTriplet;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ItemView {

    public static void addDupletDebt(MVisitStoryBase storyBase, Context context, View mView) {
        if (storyBase.getDupletProductList().size() > 0) {
            mView.findViewById(R.id.debt_titul).setVisibility(View.VISIBLE);
            LinearLayout debtPanel = (LinearLayout) mView.findViewById(R.id.debt_panel);
            debtPanel.setVisibility(View.VISIBLE);
            debtPanel.removeAllViews();

            ISession session = Configure.getSession();

            for (DebtDuplet duplet : storyBase.getDupletProductList()) {
                double itogoDt0 = 0;
                View debtItem = LayoutInflater.from(context).inflate(R.layout.item_debet_story, null);
                Pupper pointName = (Pupper) debtItem.findViewById(R.id.debt_point);
                pointName.setVisibility(View.GONE);
                Pupper visitDate = (Pupper) debtItem.findViewById(R.id.debt_date);
                Pupper visitItogo = (Pupper) debtItem.findViewById(R.id.debt_itogo);
                LinearLayout panelBase = (LinearLayout) debtItem.findViewById(R.id.panelBase);

                for (MTriplet trip : duplet.tripletList) {
                    View itemProduct = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                    MProduct product = session.get(MProduct.class, trip.product_id);
                    if (product == null) continue;
                    ((TextView) itemProduct.findViewById(R.id.name_s)).setText(product.name);
                    ((TextView) itemProduct.findViewById(R.id.amount_s)).setText(Utils.getStringDecimal(trip.amount));
                    ((TextView) itemProduct.findViewById(R.id.price_s)).setText(Utils.getStringDecimal(trip.price));
                    ((TextView) itemProduct.findViewById(R.id.itogo_s)).setText(Utils.getStringDecimal(trip.price * trip.amount));
                    itogoDt0 = itogoDt0 + trip.price * trip.amount;
                    panelBase.addView(itemProduct);
                }
                visitDate.setPairString(context.getString(R.string.dsdddd), Utils.simpleDateFormatE(duplet.date));
                visitItogo.setPairString(context.getString(R.string.dfdfff), Utils.getStringDecimal(itogoDt0));
                debtPanel.addView(debtItem);
            }
        }
    }


    public static void addDebtList(MVisitStoryBase storyBase, Context context, View mView) {
        if (storyBase.debtList.size() > 0) {

            mView.findViewById(R.id.debt_titul).setVisibility(View.VISIBLE);
            LinearLayout debtPanel = (LinearLayout) mView.findViewById(R.id.debt_panel);
            debtPanel.setVisibility(View.VISIBLE);
            debtPanel.removeAllViews();

            ISession session = Configure.getSession();

            for (String s : storyBase.debtList) {
                double itogoDt0 = 0;
                List<MDebt> mDebtList = session.getList(MDebt.class, " visit_id = ? ", s);
                View debtItem = LayoutInflater.from(context).inflate(R.layout.item_debet_story, null);
                Pupper pointName = (Pupper) debtItem.findViewById(R.id.debt_point);
                pointName.setVisibility(View.GONE);
                Pupper visitDate = (Pupper) debtItem.findViewById(R.id.debt_date);
                Pupper visitItogo = (Pupper) debtItem.findViewById(R.id.debt_itogo);
                LinearLayout panelBase = (LinearLayout) debtItem.findViewById(R.id.panelBase);

                for (MDebt mDebt : mDebtList) {
                    View itemProduct = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                    MProduct product = session.get(MProduct.class, mDebt.product_id);
                    if (product == null) continue;
                    ((TextView) itemProduct.findViewById(R.id.name_s)).setText(product.name);
                    ((TextView) itemProduct.findViewById(R.id.amount_s)).setText(Utils.getStringDecimal(mDebt.amount));
                    ((TextView) itemProduct.findViewById(R.id.price_s)).setText(Utils.getStringDecimal(mDebt.price));
                    ((TextView) itemProduct.findViewById(R.id.itogo_s)).setText(Utils.getStringDecimal(mDebt.price * mDebt.amount));
                    itogoDt0 = itogoDt0 + mDebt.price * mDebt.amount;
                    panelBase.addView(itemProduct);

                }
                Date d=Utils.intToDate(mDebtList.get(0).date);
                visitDate.setPairString(context.getString(R.string.dsdddd), Utils.simpleDateFormatE(d));
                visitItogo.setPairString(context.getString(R.string.dfdfff), Utils.getStringDecimal(itogoDt0));

                debtPanel.addView(debtItem);
            }
        }
    }

    public static void addSaleProduct(MVisitStoryBase storyBase, Context context, View mView) {
        LinearLayout panelProducts = (LinearLayout) mView.findViewById(R.id.panel_saled_products);
        //LinearLayout panelProductsCore = (LinearLayout) mView.findViewById(R.id.panel_saled_products_core);
        if (storyBase.getSalesProductList() != null && storyBase.getSalesProductList().size() > 0) {
            panelProducts.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.panel_saled_products_label).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.panel_saled_products_label).setVisibility(View.VISIBLE);
        }
        Collections.sort(storyBase.getSalesProductList(), new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        for (MProduct product : storyBase.getSalesProductList()) {
            View vv = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
            TextView name = (TextView) vv.findViewById(R.id.name_s);
            TextView amount = (TextView) vv.findViewById(R.id.amount_s);
            TextView price = (TextView) vv.findViewById(R.id.price_s);
            TextView itogo = (TextView) vv.findViewById(R.id.itogo_s);
            name.setText("  " + product.name);
            amount.setText(Utils.getStringDecimal(product.getAmount_core()));
            price.setText(Utils.getStringDecimal(product.price));
            itogo.setText(Utils.getStringDecimal((product.price * product.getAmount_core())));
            panelProducts.addView(vv);
        }
    }

    public static void addBalanceProduct(MVisitStoryBase storyBase, Context context, List<MProduct> mProductList, View mView) {
        if (storyBase.getBalanceProductList().size() > 0 && Settings.core().isShowBalance) {
            LinearLayout panelProducts_balance = (LinearLayout) mView.findViewById(R.id.panel_saled_balance);
            panelProducts_balance.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.panel_saled_balance_label).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.panel_saled_balance_label).setVisibility(View.VISIBLE);
            List<TempSale> tempSalesBalance = new ArrayList<>();
            for (final MProduct sp : storyBase.getBalanceProductList()) {
                MProduct product = Linq.toStream(mProductList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                    @Override
                    public boolean apply(MProduct t) {
                        return t.idu.equals(sp.idu);
                    }
                });
                if (product == null) continue;
                TempSale tempSale = new TempSale(product.idu, product.name, sp.getAmount_core(), 0, product.amountSelfCore());//// TODO: 17.11.2016
                tempSalesBalance.add(tempSale);

            }
            for (TempSale tempSale : tempSalesBalance) {
                View vv = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) vv.findViewById(R.id.name_s);
                TextView amount = (TextView) vv.findViewById(R.id.amount_s);
                TextView price = (TextView) vv.findViewById(R.id.price_s);
                TextView itogo = (TextView) vv.findViewById(R.id.itogo_s);
                name.setText("  " + tempSale.name);
                amount.setText(Utils.getStringDecimal(tempSale.amount));
                price.setText(Utils.getStringDecimal(0d));
                itogo.setText(Utils.getStringDecimal((0d)));
                panelProducts_balance.addView(vv);

            }
        }
    }


    public static void addFreeProduct(MVisitStoryBase storyBase, Context context, View mView) {
        if (storyBase.getFreeProductList().size() > 0) {

            LinearLayout panelProducts_present = (LinearLayout) mView.findViewById(R.id.panel_saled_products_present);
            panelProducts_present.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.panel_saled_products_present_label).setVisibility(View.VISIBLE);

            mView.findViewById(R.id.panel_saled_products_present_label).setVisibility(View.VISIBLE);


            Collections.sort(storyBase.getFreeProductList(), new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            double present_count = 0;
            for (MProduct product : storyBase.getFreeProductList()) {
                View vv = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) vv.findViewById(R.id.name_s);
                TextView amount = (TextView) vv.findViewById(R.id.amount_s);
                TextView price = (TextView) vv.findViewById(R.id.price_s);
                TextView itogo = (TextView) vv.findViewById(R.id.itogo_s);
                String nameType = context.getString(R.string.xcxcxc);
                MFreePresentTYpes type = Configure.getSession().get(MFreePresentTYpes.class, product.rule_id);
                if (type != null) {
                    nameType = type.name;
                }
                name.setText("  " + product.name + "\n" + nameType);
                amount.setText(Utils.getStringDecimal(product.getAmount_core()));
                price.setText(Utils.getStringDecimal(0d));
                itogo.setText(Utils.getStringDecimal((0d)));
                panelProducts_present.addView(vv);
                if (product.amountSelf < 0) {
                    name.setBackgroundColor(Settings.core().colorTextStock);
                    amount.setBackgroundColor(Settings.core().colorTextStock);
                    price.setBackgroundColor(Settings.core().colorTextStock);
                    itogo.setBackgroundColor(Settings.core().colorTextStock);
                }
                present_count = present_count + product.getAmount_core();
            }
            /////////////////////////////////////////////////////////////////////////////////////// present
            if (storyBase.getFreeProductList().size() > 0) {
                Pupper amountProduct_present = (Pupper) mView.findViewById(R.id.visit_story_amount_poduct_present);
                amountProduct_present.setVisibility(View.VISIBLE);
                amountProduct_present.setPairString(R.string.itogo_present, Utils.getStringDecimal(present_count));
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////
        }
    }


    public static void addActionProduct(MVisitStoryBase storyBase, Context context, View mView) {
        if (storyBase.getActionProductList().size() > 0) {

            LinearLayout panelProducts_action = (LinearLayout) mView.findViewById(R.id.panel_promo_products);
            mView.findViewById(R.id.panel_promo_products_label).setVisibility(View.VISIBLE);
            panelProducts_action.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.panel_promo_products_label).setVisibility(View.VISIBLE);

            Collections.sort(storyBase.getActionProductList(), new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.rule_id.compareTo(rhs.rule_id);
                }
            });
            double totalAmountProductActions = 0;
            for (MProduct product : storyBase.getActionProductList()) {
                totalAmountProductActions = totalAmountProductActions + product.getAmount_core();
                View view = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) view.findViewById(R.id.name_s);
                TextView amount = (TextView) view.findViewById(R.id.amount_s);
                TextView price = (TextView) view.findViewById(R.id.price_s);
                TextView itogo = (TextView) view.findViewById(R.id.itogo_s);
                String nameType = context.getString(R.string.xcxcxc);
                MAction type = Configure.getSession().get(MAction.class, product.rule_id);
                if (type != null) {
                    nameType = type.name;
                }
                name.setText(product.name + "\n" + nameType);
                amount.setText(Utils.getStringDecimal(product.getAmount_core()));
                price.setText(Utils.getStringDecimal(0d));//product.price
                itogo.setText(Utils.getStringDecimal(0d));//product.price* product.amount_core
                panelProducts_action.addView(view);
            }

            Pupper amount_actions = (Pupper) mView.findViewById(R.id.visit_story_amount_actions);
            amount_actions.setVisibility(View.VISIBLE);
            amount_actions.setPairString(context.getString(R.string.extract_actions), Utils.getStringDecimal(totalAmountProductActions));
        }
    }

    public static void addNewOrder(MVisitStoryBase storyBase, Context context, View mView) {
        if (storyBase.getNewOrderProductList().size() == 0) return;
        LinearLayout panelBase = (LinearLayout) mView.findViewById(R.id.panel_new_order);
        panelBase.setVisibility(View.VISIBLE);
        mView.findViewById(R.id.panel_new_order_label).setVisibility(View.VISIBLE);

        Collections.sort(storyBase.getNewOrderProductList(), new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        for (MProduct mProduct : storyBase.getNewOrderProductList()) {

            View view = LayoutInflater.from(context).inflate(R.layout.item_product_story, null);
            TextView name = (TextView) view.findViewById(R.id.name_s);
            TextView amount = (TextView) view.findViewById(R.id.amount_s);
            TextView price = (TextView) view.findViewById(R.id.price_s);
            price.setVisibility(View.GONE);
            TextView itogo = (TextView) view.findViewById(R.id.itogo_s);
            itogo.setVisibility(View.GONE);
            name.setText(mProduct.name);
            amount.setText(Utils.getStringDecimal(mProduct.getAmount_core()));
            panelBase.addView(view);
        }
    }

    public static void addPresent(MVisitStoryBase storyBase, Context context, View mView) {
        List<MVisitPresent>  list=storyBase.getPresnt();
        if(list.size()>0){
            mView.findViewById(R.id.panel_present).setVisibility(View.VISIBLE);
            LinearLayout layoutPresent= (LinearLayout) mView.findViewById(R.id.panel_present_core);
            for (MVisitPresent present : list) {
                View view = LayoutInflater.from(context).inflate(R.layout.item_present_story, null);
                TextView name= (TextView) view.findViewById(R.id.name_present);
                TextView amount= (TextView) view.findViewById(R.id.amount_present);
                name.setText(present.getName());
                amount.setText(String.valueOf(present.amount));
                layoutPresent.addView(view);
            }
        }
    }
}
