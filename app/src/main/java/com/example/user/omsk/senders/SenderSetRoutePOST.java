package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

//import com.example.user.omsk.models.MError;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.route.SettingsRoute;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class SettingsRouteE implements Serializable {

    public Date date = null;
    public String guid;
    @SerializedName("approved")
    public boolean isVerification;
    @SerializedName("points")
    public List<String> pointList = new ArrayList<>();
    @Expose(serialize = false)
    public List<String> pointListCopy = new ArrayList<>();

}

public class SenderSetRoutePOST {

    public static org.apache.log4j.Logger log = Logger.getLogger(SenderSetRoutePOST.class);
    private Activity activity;
    private String urlCore;
    private SenderGetRouterGET.IActionRoute iActionRoute;

    public void send(Activity activity, String url, SettingsRoute routeSettings, SenderGetRouterGET.IActionRoute iActionRoute) {
        this.activity = activity;
        this.urlCore = url;
        this.iActionRoute = iActionRoute;
        Gson sd = Utils.getGson();

        SettingsRouteE d = new SettingsRouteE();
        d.date = routeSettings.dateNew_22;
        d.guid = routeSettings.guid;
        d.isVerification = routeSettings.isVerification;
        d.pointList = routeSettings.pointList;
        d.pointListCopy = routeSettings.pointListCopy;


        String str = sd.toJson(d);
        new SenderWorkerTack().execute(str);
    }

    private class SenderWorkerTack extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(activity, Settings.core())) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, "Отправка данных", null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (aBoolean == null && iActionRoute != null) {
                iActionRoute.Action(null);
            } else {

                Toast.makeText(activity, aBoolean, Toast.LENGTH_LONG).show();

                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/client/track/android/:  " + aBoolean;

                log.error(msg);
                //Configure.getSession().insert(new MError(msg));
                //new SenderErrorPOST().send(activity, msg, Settings.core());

            }
            if (dialog != null) {
                dialog.cancel();
            }
        }


        @Override
        protected String doInBackground(String... params) {

            if (isCancelled()) return null;
            return UtilsSender.postRequest(activity.getApplication(), Utils.HTTP + urlCore + "/client/track/android/", params[0], new IActionResponse() {
                @Override
                public void invoke(String str, int status) {
                }
            });
        }

    }
}
