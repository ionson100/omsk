package com.example.user.omsk.freesql;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.user.omsk.R;

import java.util.List;


public class FreeTables extends LinearLayout {
    public FreeTables(Context context) {
        super(context);
    }

    public FreeTables(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FreeTables(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FreeTables(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private TableLayout tableLayoutCore;
    private ScrollView scrollView;
    ;

    public void activate(List<String> fieldNames, List<List<String>> datasList) {
        this.removeAllViews();
        this.setOrientation(VERTICAL);
        TableLayout table = new TableLayout(getContext());
        tableLayoutCore = new TableLayout(getContext());
        TableRow row = new TableRow(getContext());
        /////////////////////////////
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int delta = width / fieldNames.size();
        for (String fieldName : fieldNames) {
            TextView t = new TextView(getContext());


            android.widget.TableRow.LayoutParams llp = new android.widget.TableRow.LayoutParams(delta, LayoutParams.MATCH_PARENT);
            int value = (int) getContext().getResources().getDimension(R.dimen.tablemagious);
            llp.setMargins(value, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
            t.setLayoutParams(llp);

            t.setText(fieldName);
            t.setTextColor(Color.WHITE);
            t.setBackgroundColor(Color.BLACK);
            t.setGravity(Gravity.CENTER_HORIZONTAL);
            t.setWidth(delta);
            row.addView(t);
        }
        table.addView(row);
        this.addView(table);
        scrollView = new ScrollView(getContext());

        scrollView.addView(tableLayoutCore);
        this.addView(scrollView);

        for (List<String> strings : datasList) {
            TableRow row1 = new TableRow(getContext());
            for (String str : strings) {
                TextView t = new TextView(getContext());
                t.setTextColor(Color.BLACK);
                t.setText(str);
                t.setGravity(Gravity.CENTER_HORIZONTAL);
                t.setWidth(delta);
                row1.addView(t);
                t.setPadding(10, 20, 10, 20);
                t.setBackgroundResource(R.color.freesql);
                android.widget.TableRow.LayoutParams llp = new android.widget.TableRow.LayoutParams(delta, LayoutParams.MATCH_PARENT);
                int value = (int) getContext().getResources().getDimension(R.dimen.tablemagious);
                llp.setMargins(value, 2, 0, 0); // llp.setMargins(left, top, right, bottom);
                t.setLayoutParams(llp);
            }
            tableLayoutCore.addView(row1);
        }

    }
}
