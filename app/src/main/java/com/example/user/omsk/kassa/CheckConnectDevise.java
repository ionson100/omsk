package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;


public class CheckConnectDevise {
    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(CheckConnectDevise.class);
    public static void check(final TextView log, final Activity activity, final Settings mSettings, final IActionE iActionE) {


        new AsyncTask<Void, String, Void>() {


            String errorStrig = "OK";
            private IFptr fptr;
            private ProgressDialog dialog;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected void onPreExecute() {
                log.setText("");
                dialog = Utils.factoryDialog(activity, "Проверка связи.", null);
                dialog.setCancelable(false);
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface d, int which) {
                        d.dismiss();
                        cancel(true);
                    }
                });
                dialog.show();
            }


            protected void onPostExecute(Void aVoid) {

                if (isCancelled() == false) {
                    if (dialog != null) {
                        dialog.cancel();
                        dialog = null;
                    }
                    iActionE.action(errorStrig);
                    log.setText("");
                }


            }

            @Override
            protected Void doInBackground(Void... params) {
                fptr = new Fptr();
                try {
                    fptr.create(activity.getApplication());
                    publishProgress("Загрузка настроек");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    if (isCancelled() == false) {
                        if (fptr.GetStatus() < 0) {
                            checkError();
                        }
                        publishProgress("OK");
                    }
                    publishProgress("Проверка связи...");

                    if (fptr.put_RegisterNumber(10) < 0) {
                        checkError();
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError();
                    }

                    double aDouble = new Double(fptr.get_Summ());

                    PrintKkmSumma.summKKM(aDouble);

                } catch (Exception e) {
                    logE.error(e);
                    errorStrig = e.getMessage();
                    publishProgress(e.toString());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }

        }.execute();

    }
}
