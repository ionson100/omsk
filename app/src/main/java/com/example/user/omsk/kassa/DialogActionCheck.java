package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.PartialerCheckDebit;
import com.example.user.omsk.PartialerCheckSale;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CACH;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_DEBT;


public class DialogActionCheck extends DialogFragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DialogActionCheck.class);
    List<View> mViewList = new ArrayList<>();
    private IActionE mIActionE;
    private IActionE mIActionEDismis;
    private Settings mSettings;

    public void setmSettings(Settings dd) {
        this.mSettings = dd;
    }

    public void setActionE(IActionE actionE) {
        this.mIActionE = actionE;
    }

    public void setActionEDismis(IActionE actionE) {
        this.mIActionEDismis = actionE;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


      //  List<MProduct> mProductList = Configure.getSession().getList(MProduct.class, null);


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_action_check, null);
        ((TextView) mView.findViewById(R.id.title_dalog)).setText(getString(R.string.khjkasd));
        LinearLayout panelBase = (LinearLayout) mView.findViewById(R.id.panel_checks);

        builder.setView(mView);
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        if (mSettings.getVisit().selectProductForSale.size() > 0) {

            PrintCache(panelBase);
        }

        List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, null);

        List<MDebt> debtListCore = new ArrayList<>();

        for (final String s : mSettings.getVisit().debtList) {
            MDebt debt = Linq.toStream(mDebtList).firstOrDefault(new Predicate<MDebt>() {
                @Override
                public boolean apply(MDebt t) {
                    return t.order_id.equals(s);
                }
            });
            if (debt != null) {
                debtListCore.add(debt);
            }
        }
        Collections.sort(debtListCore, new Comparator<MDebt>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MDebt lhs, MDebt rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });

        for (final String debt : mSettings.getVisit().debtList) {
            PrintDebt(panelBase, debt);
        }

        mView.findViewById(R.id.bt_sms_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogSmsEmail smsEmail = new DialogSmsEmail();
                smsEmail.show(getActivity().getSupportFragmentManager(), "jdkashj");
            }
        });


        return builder.create();
    }

    private void PrintDebt(LinearLayout panelBase, final String debt) {


        final List<MProductBase> list = new ArrayList<>();
        final MChecksData checksData = UtilsKassa.getMChecksDataDebt(mSettings.getVisit().checksDataList, debt);

        int dateE=0 ;
        List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, " visit_id = ?", debt);
        if (mDebtList.size() == 0) return;
        double count = 0;
        double itogo = 0;

        final ISession ses = Configure.getSession();
        for (MDebt mDebt : mDebtList) {
            MProduct product = ses.get(MProduct.class, mDebt.product_id);
            if (product == null) {
                product = new MProduct();
                product.unit = "пач";
                product.nameKKT = product.name = "Товар не найден";
            }
            count = count + mDebt.amount;
            itogo = itogo + (mDebt.amount * mDebt.price);
            product.price = mDebt.price;
            product.setAmountCore(mDebt.amount);
            dateE = mDebt.date;
            list.add(product.cloneForKassa());
        }


//        for (MChecksData mChecksData : mSettings.getVisit().checksDataList) {
//            if (mChecksData.type_check == TYPE_CHECK_DEBT) {
//                Long dq = mChecksData.debt_data.getTime();
//                long dq1 = dateE.getTime();
//                if (dq == dq1) {
//                    Utils.messageBox("Error", "Чек уже выбит на  такое время долга!\r" +
//                            " Date1: " + Utils.simpleDateFormatE(mChecksData.debt_data) + "\r" +
//                            " Date2: " + Utils.simpleDateFormatE(dateE), getActivity(), null);
//                    return;
//                }
//            }
//        }


        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_dialog_action_check, null);
        TextView typeTextView = (TextView) view.findViewById(R.id.type_check);
        final TextView dataTextView = (TextView) view.findViewById(R.id.check_data);
        TextView zetTextView = (TextView) view.findViewById(R.id.zreport_text);
        final Button printCheckButton = (Button) view.findViewById(R.id.bt_print_check);
        final Button printDoubleCheckButton = (Button) view.findViewById(R.id.bt_print_duoble_check);
        Date d=Utils.intToDate(dateE);
        typeTextView.setText(getString(R.string.adasd) + Utils.simpleDateFormatE(d));

        dataTextView.setText(getString(R.string.lajsas) + Utils.getStringDecimal(count) + "\n" + getString(R.string.sassas) + Utils.getStringDecimal(itogo) + " руб.\n");

        final double finalCount = count;
        final double finalItogo = itogo;
        final int finalDateE = dateE;
        final boolean[] run = {true};
        final int finish = Utils.dateToInt(new Date());



        printCheckButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public synchronized void onClick(View v) {


                //                /////////////////////// проверка на задваивание

                for (final MChecksData mChecksData : mSettings.getVisit().checksDataList) {
                    if (mChecksData.type_check == MChecksData.TYPE_CHECK_DEBT) {
                        if (mChecksData.order_debt_id.equals(debt)) {

                            MChecksData scc = Linq.toStream(mSettings.getVisit().checksDataList).firstOrDefault(new Predicate<MChecksData>() {
                                @Override
                                public boolean apply(MChecksData t) {
                                    return t.type_check == MChecksData.TYPE_CHECK_RETURN && t.checkUUIDDelete.equals(mChecksData.idu);
                                }
                            });
                            if (scc == null) {
                                Utils.messageBox(getString(R.string.error), getString(R.string.jsakjdsd), getActivity(), null);
                                return;
                            }

                        }
                    }
                }


                if (!run[0]) return;
                run[0] = false;


                final List<List<MProductBase>> lists = PartialerCheckDebit.divider(list, mSettings);
                if (lists.size() == 0) {
                    Utils.messageBox(getString(R.string.error), getString(R.string.asddd), getActivity(), null);
                } else if (lists.size() == 1) {

                    UtilsKassa.messageBoxConfiпPrintCheck(getActivity(), new ArrayList<MProductBase>(list), TYPE_CHECK_DEBT, new IActionE() {
                        @Override
                        public void action(Object o) {
                            printCheckButton.setVisibility(View.GONE);
                            print(lists);
                        }
                    }, new IActionE() {
                        @Override
                        public void action(Object o) {
                            run[0] = true;
                        }
                    });
                } else {

                    Utils.messageBoxPartalCheck(lists, getActivity(), new IActionE<List<List<MProductBase>>>() {
                        @Override
                        public void action(List<List<MProductBase>> o) {
                            printCheckButton.setVisibility(View.GONE);
                            print(lists);
                        }


                    }, new IActionE() {
                        @Override
                        public void action(Object o) {
                            run[0] = true;
                        }
                    });

                }


            }

            private void print(List<List<MProductBase>> lists1) {
                PrintCheckPartial.pintCheckCore(getActivity(), mSettings, lists1, TYPE_CHECK_DEBT, new IActionE() {
                    @Override
                    public void action(Object o) {
                        final List<MChecksData> datas = (List<MChecksData>) o;
                        for (final MChecksData data : datas) {
                            if (data.errorText != null) {
                                Utils.messageBox(getString(R.string.error), data.errorText, getActivity(), null);
                            } else {
                                ISession session = Configure.getSession();
                                try {

                                    dataTextView.setText(getString(R.string.lajsas) + Utils.getStringDecimal(finalCount) + "\n" + getString(R.string.sassas) + Utils.getStringDecimal(finalItogo) + " руб.\n");
                                    data.visit_id = mSettings.getVisit().idu;
                                    data.type_check = TYPE_CHECK_DEBT;
                                    data.order_debt_id = debt;
                                    data.debt_data = finalDateE;
                                    UtilsKassa.clearLast(mSettings);
                                    mSettings.getVisit().checksDataList.add(data);
                                    Settings.save();
                                    printCheckButton.setVisibility(View.GONE);
                                    printDoubleCheckButton.setVisibility(View.VISIBLE);
                                    mSettings.getVisit().setFinish(Utils.dateToInt(new Date()));
                                    MVisitStory visitStory = mSettings.getVisit().getVisitStory();
                                    visitStory.date_pessimise=new Date().getTime();

                                    visitStory.setNewObject(true);
                                    UtilsKassa.clearLast(mSettings);
                                    data.isLast = true;
                                    session.beginTransaction();
                                    if (visitStory.id == 0) {
                                        session.insert(visitStory);
                                        mSettings.getVisit().id = visitStory.id;
                                    } else {

                                        session.update(visitStory);
                                    }
                                    mIActionE.action(data);
                                    getActivity().sendBroadcast(new Intent(MainActivity.FAB));
                                    run[0] = true;
                                    printDoubleCheckButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            printDoubleCheck(data, dataTextView);
                                        }
                                    });
                                    session.commitTransaction();
                                } catch (Exception ex) {
                                    log.error(ex);
                                    Utils.messageBox(getString(R.string.error), ex.getMessage(), getActivity(), null);
                                } finally {
                                    session.endTransaction();
                                }
                            }
                        }
                        run[0] = true;
                    }
                });
            }
        });


        if (checksData == null) {
            printCheckButton.setVisibility(View.VISIBLE);
            printDoubleCheckButton.setVisibility(View.GONE);
        } else if (checksData.is_zreport) {
            zetTextView.setVisibility(View.VISIBLE);
            printCheckButton.setVisibility(View.GONE);
            printDoubleCheckButton.setVisibility(View.GONE);
        } else {
            printDoubleCheckButton.setVisibility(View.VISIBLE);
        }
        printDoubleCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printDoubleCheck(checksData, dataTextView);
            }
        });
        mViewList.add(view);
        panelBase.addView(view);
    }

    void printDoubleCheck(MChecksData checksData, TextView dataTextView) {
        MVisitStory m = mSettings.getVisit().getVisitStory();
        AnnulCheck.TempCheck tempCheck = new AnnulCheck.TempCheck(m, checksData);
        PrintDoubeCheck.print(mSettings, dataTextView, getActivity(), tempCheck);
    }


    private void PrintCache(LinearLayout panelBase) {


        if (Utils.isDelay(mSettings.getVisit())) return;
        final MChecksData checksData = UtilsKassa.getMCheckDataCache(mSettings.getVisit().checksDataList);

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_dialog_action_check, null);
        TextView typeTextView = (TextView) view.findViewById(R.id.type_check);
        final TextView dataTextView = (TextView) view.findViewById(R.id.check_data);
        final TextView zetTextView = (TextView) view.findViewById(R.id.zreport_text);
        final Button printCheckButton = (Button) view.findViewById(R.id.bt_print_check);
        final Button printDoubleCheckButton = (Button) view.findViewById(R.id.bt_print_duoble_check);
        typeTextView.setText(R.string.seaer);
        double count = 0;
        double itogo = 0;
        for (MProduct product : mSettings.getVisit().selectProductForSale) {
            count = count + product.getAmount_core();
            itogo = itogo + product.getAmount_core() * product.price;
        }
        dataTextView.setText(getString(R.string.lajsas) + Utils.getStringDecimal(count) + "\n" + getString(R.string.sassas) + Utils.getStringDecimal(itogo) + " руб.\n");
        final double finalCount = count;
        final double finalItogo = itogo;
        final boolean[] run = {true};
       // final int finish = Utils.dateToInt(new Date());


        printCheckButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public synchronized void onClick(View v) {


                ////////////////////////////////////////////проверка на дубликат

                for (final MChecksData mChecksData : mSettings.getVisit().checksDataList) {
                    if (mChecksData.type_check == MChecksData.TYPE_CHECK_CACH) {

                        MChecksData сss = Linq.toStream(mSettings.getVisit().checksDataList).firstOrDefault(new Predicate<MChecksData>() {
                            @Override
                            public boolean apply(MChecksData t) {
                                return t.type_check == MChecksData.TYPE_CHECK_RETURN && t.checkUUIDDelete.equals(mChecksData.idu);
                            }
                        });
                        if (сss == null) {
                            Utils.messageBox(getString(R.string.error), getString(R.string.dsfaf), getActivity(), null);
                            return;
                        }

                    }
                }

                if (!run[0]) return;
                run[0] = false;


                double summ = 0;
                for (MProduct mProduct : mSettings.getVisit().selectProductForSale) {
                    summ = summ + mProduct.getAmount_core() * mProduct.price;
                }

                final List<List<MProductBase>> lists = PartialerCheckSale.divider(new ArrayList<MProductBase>(mSettings.getVisit().selectProductForSale), mSettings);
                if (lists.size() == 0) {
                    Utils.messageBox(getString(R.string.error), getString(R.string.asddd), getActivity(), null);
                } else if (lists.size() == 1) {

                    UtilsKassa.messageBoxConfiпPrintCheck(getActivity(), new ArrayList<MProductBase>(mSettings.getVisit().selectProductForSale), TYPE_CHECK_CACH, new IActionE() {
                        @Override
                        public void action(Object o) {
                            printCheckButton.setVisibility(View.GONE);
                            print(lists);
                        }
                    }, new IActionE() {
                        @Override
                        public void action(Object o) {
                            run[0] = true;
                        }
                    });
                } else {

                    Utils.messageBoxPartalCheck(lists, getActivity(), new IActionE<List<List<MProductBase>>>() {
                        @Override
                        public void action(List<List<MProductBase>> o) {
                            printCheckButton.setVisibility(View.GONE);
                            print(lists);
                        }


                    }, new IActionE() {
                        @Override
                        public void action(Object o) {
                            run[0] = true;
                        }
                    });

                }


            }

            private void print(List<List<MProductBase>> lists1) {
                PrintCheckPartial.pintCheckCore(getActivity(), mSettings, lists1, TYPE_CHECK_CACH, new IActionE() {
                    @Override
                    public void action(Object o) {
                        final List<MChecksData> datas = (List<MChecksData>) o;
                        for (final MChecksData data : datas) {
                            if (data.errorText != null) {
                                Utils.messageBox(getString(R.string.error), data.errorText, getActivity(), null);
                            } else {
                                ISession session = Configure.getSession();
                                try {
                                    dataTextView.setText(getString(R.string.lajsas) + Utils.getStringDecimal(finalCount) + "\n" + getString(R.string.sassas) + Utils.getStringDecimal(finalItogo) + " руб.\n");
                                    data.visit_id = mSettings.getVisit().idu;
                                    data.type_check = TYPE_CHECK_CACH;
                                    UtilsKassa.clearLast(mSettings);
                                    mSettings.getVisit().checksDataList.add(data);
                                    Settings.save();
                                    printCheckButton.setVisibility(View.GONE);
                                    printDoubleCheckButton.setVisibility(View.VISIBLE);
                                    mSettings.getVisit().setFinish(Utils.dateToInt(new Date()));
                                    MVisitStory visitStory = mSettings.getVisit().getVisitStory();
                                    visitStory.setNewObject(true);
                                    UtilsKassa.clearLast(mSettings);
                                    data.isLast = true;

                                    visitStory.date_pessimise=new Date().getTime();
                                    session.beginTransaction();
                                    if (visitStory.id == 0) {
                                        session.insert(visitStory);
                                        mSettings.getVisit().id = visitStory.id;
                                    } else {
                                        session.update(visitStory);
                                    }
                                    getActivity().sendBroadcast(new Intent(MainActivity.FAB));
                                    mIActionE.action(data);
                                    run[0] = true;
                                    printDoubleCheckButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            printDoubleCheck(data, dataTextView);
                                        }
                                    });
                                    session.commitTransaction();
                                } catch (Exception ex) {

                                    log.error(ex);
                                    Utils.messageBox(getString(R.string.error), ex.getMessage(), getActivity(), null);
                                } finally {
                                    session.endTransaction();
                                }
                            }
                        }
                        run[0] = true;


                    }
                });
            }


        });


        if (checksData == null) {
            printCheckButton.setVisibility(View.VISIBLE);
            printDoubleCheckButton.setVisibility(View.GONE);
        } else if (checksData.is_zreport) {
            zetTextView.setVisibility(View.VISIBLE);
        } else {
            printDoubleCheckButton.setVisibility(View.VISIBLE);
        }
        mViewList.add(view);
        panelBase.addView(view);

        printDoubleCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printDoubleCheck(checksData, dataTextView);
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mIActionEDismis.action(null);
    }


}
