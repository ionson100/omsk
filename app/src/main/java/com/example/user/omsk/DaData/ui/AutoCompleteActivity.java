package com.example.user.omsk.DaData.ui;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

//import com.example.user.omsk.models.MError;


public class AutoCompleteActivity extends AppCompatActivity {

    public static org.apache.log4j.Logger log = Logger.getLogger(AutoCompleteActivity.class);
    private Settings mSettings;
    private TextView textViewCore;
    private static final List<String> EMPTY = new ArrayList<>();

    private AddressCompleteTextView textView;
    LinearLayout linearLayoutPanelGeo;
    EditText editTextLat, editTextLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        try {

            mSettings = Settings.core();
            setContentView(R.layout.activity_auto_complete);
            textView = (AddressCompleteTextView) findViewById(R.id.aauto_complete);
            findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (textViewCore.getText().toString().trim().length() > 0) {
                        String ss = "";
                        if (Settings.core().getVisit() != null && Settings.core().getVisit().point != null) {
                            ss = "Изменить у точки адрес?\nСуществующий адрес: " + Settings.core().getVisit().point.address + "\n\n" +
                                    "Изменить на: " + textViewCore.getText().toString();
                            if (editTextLat.getText().toString().equals("0") == false && editTextLon.getText().toString().equals("0") == false) {
                                ss = ss + "\nТак же изменится широта и долгота точки";
                            }
                        } else {
                            ss = "Адрес фиксации путевого листа: " + textViewCore.getText().toString().trim().replace("null,", "");
                        }

                        Utils.messageBox(getString(R.string.bvbcvb), ss, AutoCompleteActivity.this, new IActionE() {
                            @Override
                            public void action(Object o) {
                                Intent intent = new Intent();
                                intent.putExtra("address", textViewCore.getText().toString().trim().replace("null,", ""));
                                intent.putExtra("latitude", editTextLat.getText().toString());
                                intent.putExtra("longitude", editTextLon.getText().toString());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });
                    }


                }
            });
            findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            ///////////////////////////////////
            final AddressCompleteTextView textView = (AddressCompleteTextView) findViewById(R.id.aauto_complete);
            textView.setAdapter(new AddressAdapter(this));
            textView.setLoadingIndicator((ProgressBar) findViewById(R.id.progress_bar));
            linearLayoutPanelGeo = (LinearLayout) findViewById(R.id.panel_geo);
            textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    AddressData address = (AddressData) adapterView.getItemAtPosition(position);
                    if (address == null) return;
                    textView.setText(address.value);
                    textView.setSelection(address.value.length());
                    if (address.data == null) return;
                    String ss = address.data.postal_code + ", " + address.data.country + ", " + address.value.replace("обл, ", "обл., ").replace(" ул ", " улица ").replace(" Респ ", " Респ. ");

                    if (address.data.geo_lat != null && address.data.geo_lon != null) {

                        editTextLat.setText(address.data.geo_lat);
                        editTextLon.setText(address.data.geo_lon);
                        linearLayoutPanelGeo.setVisibility(View.VISIBLE);
                    } else {
                        linearLayoutPanelGeo.setVisibility(View.GONE);
                        editTextLat.setText("0");
                        editTextLon.setText("0");
                    }
                    textViewCore.setText(ss);
                }
            });
            textViewCore = (TextView) findViewById(R.id.address_core);
            findViewById(R.id.bt_geo_addres).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(AutoCompleteActivity.this, MapAddressActivity.class);
                    intent.putExtra("latitude", editTextLat.getText().toString());
                    intent.putExtra("longitude", editTextLon.getText().toString());
                    startActivityForResult(intent, 1962);

                }
            });

            editTextLat = (EditText) findViewById(R.id.e_latitude);
            editTextLon = (EditText) findViewById(R.id.e_longitude);
        } catch (Exception ex) {
            String msg = Utils.getErrorTrace(ex);
            Utils.sendMessage("dadata:" + msg,  (MyApplication) getApplication());
            log.error(msg);
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    boolean edit = false;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1962 && resultCode == RESULT_OK) {

            edit = true;
            double la = data.getDoubleExtra("latitude", 0);
            double lo = data.getDoubleExtra("longitude", 0);
            if (la == 0 || lo == 0) return;
            editTextLon.setText("" + lo);
            editTextLat.setText("" + la);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (edit == false) {
            textView.setText(Settings.core().city.replace("(", "").replace(")", "") + " ");
            textView.setSelection(textView.getText().toString().length());
        }

    }
}

class SenderAutoComplete {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderAutoComplete.class);
    String error = null;

    private List<AddressData> stringList;
    private String query;


    public void send(List<AddressData> stringList, String query) {


        this.stringList = stringList;
        this.query = query;


        try {
            new SenderWorker().execute().get();
        } catch (Exception e) {
           log.error(e);
        }
    }

    private class SenderWorker extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {


            HttpsURLConnection conn = null;

            try {


                /////////////////////////////////
                HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                SSLContext context = SSLContext.getInstance("TLS");
                context.init(null, new X509TrustManager[]{new X509TrustManager() {
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                }}, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                /////////////////////////////////


                URL url = new URL("https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address");

                conn = (HttpsURLConnection) url.openConnection();

                conn.setReadTimeout(15000 /*milliseconds*/);
                conn.setConnectTimeout(20000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Authorization", "Token " + Settings.core().dadata);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write("{ \"query\": \"" + query + "\", \"count\": 10 }");
                wr.flush();
                wr.close();
                conn.connect();
                final int status = conn.getResponseCode();
                int ch;
                StringBuilder sb = new StringBuilder();
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                    while ((ch = reader.read()) != -1) {
                        sb.append((char) ch);
                    }
                    reader.close();
                } catch (IOException ignored) {

                }

                if (status == 200) {

                    stringList.clear();
                    Gson gson = Utils.getGson();

                    Iosia iosia = gson.fromJson(sb.toString(), Iosia.class);
                    for (AddressData suggestion : iosia.suggestions) {
                        stringList.add(suggestion);
                    }
                } else {

                    switch (status) {
                        case 400: {
                            throw new Exception("Некорректный запрос: " + query);
                        }
                        case 401: {
                            throw new Exception("В запросе отсутствует API-ключ или секретный ключ.");
                        }
                        case 402: {
                            throw new Exception("Недостаточно средств для обработки запроса, пополните баланс.");
                        }
                        case 403: {
                            throw new Exception("В запросе указан несуществующий ключ.");
                        }
                        case 405: {
                            throw new Exception("Запрос сделан с методом, отличным от POST");
                        }
                        case 413: {
                            throw new Exception("Запрос содержит более 10 записей.");
                        }
                        default: {
                            throw new Exception("Произошла внутренняя ошибка сервиса во время обработки.\nОтвет сервера: " + status);
                        }
                    }
                }

            } catch (Exception e) {
                log.error(e);
                error = e.getMessage();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }


            return null;
        }
    }


    class Iosia {
        List<AddressData> suggestions = new ArrayList<>();
    }

    // для верификации носта ssl с которым держим связь
    public static class NullHostNameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

}
