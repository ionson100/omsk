package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
// comment for points

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

// хранилище сообщений пользователей, которые он хочет отправить на сервер ( рекомендации пожелания)
@Table("comments")
public class MComments implements Serializable {

    @PrimaryKey("id")
    public transient int id;

    @Column("point_id")
    public transient String point_id;

    @SerializedName("date")
    @Column("date")
    public Date date = null;

    @SerializedName("comment")
    @Column("comment")
    public String comment;
}
