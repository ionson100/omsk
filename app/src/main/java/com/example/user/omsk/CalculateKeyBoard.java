package com.example.user.omsk;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.view.View;
import android.widget.EditText;

import org.apache.log4j.Logger;

/**
 * ion100 on 03.11.2017.
 */

public class CalculateKeyBoard {
    public static org.apache.log4j.Logger log = Logger.getLogger(CalculateKeyBoard.class);
    AudioManager am;
    private final EditText editText;
    private final View view;
    private Settings settings;


    public CalculateKeyBoard(EditText editText, View view, Settings settings, boolean isShowSeparatorButton, Activity activity) {


        am = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        this.editText = editText;
        this.view = view;
        this.settings = settings;
        if (settings.useCalculator) {
            editText.setFocusable(false);
        } else {

            View sss = view.findViewById(R.id.panel_calculator);
            sss.setVisibility(View.GONE);
            return;
        }

        view.findViewById(R.id.b1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        view.findViewById(R.id.b01).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
        if (!isShowSeparatorButton) {
            view.findViewById(R.id.b01).setVisibility(View.INVISIBLE);
        }
        view.findViewById(R.id.bt_basket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonBasketClick(v);
            }
        });
        view.findViewById(R.id.b1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick(v);
            }
        });
    }

    private void buttonBasketClick(View v) {
        editText.setText("0");
        am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE, 0.5f);
    }

    void buttonClick(View view) {


        am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD, 0.5f);

        String s = editText.getText().toString();
        String addin = (String) view.getTag();
        if (s.equals("0") && !addin.equals(",")) {
            s = "";
        }
        if (s.indexOf(",") != -1 && addin.equals(",")) {

        } else {
            s = s + addin;
            editText.setText(s);
        }


    }
}
