package com.example.user.omsk.plan;

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

/**
 * ion100 on 12.09.2017.
 */
@Table("mml_old")
public class ModelMMLOld {

    @PrimaryKey("id")
    public int id;

    @Column("mml_name")
    public String mmlName;

    @Column("mml_pachek")
    public double mmlPachek;

    @Column("mml_point")
    public double mmlPoint;
}
