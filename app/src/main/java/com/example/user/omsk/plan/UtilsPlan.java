package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UtilsPlan {

    public static List<MBasePlanPair> getPlanPairList(boolean isOld) {

        List<MBasePlanPair> list = null;
        if (!isOld) {
            list = new ArrayList<MBasePlanPair>(Configure.getSession().getList(MPlanPair.class, null));
        } else {
            list = new ArrayList<MBasePlanPair>(Configure.getSession().getList(MPlanPairOld.class, null));
        }

        if (!isOld) {
            List<MVisitStory> stories = Configure.getSession().getList(MVisitStory.class, null);
            for (final MVisitStory story : stories) {
                MBasePlanPair pair = Linq.toStream(list).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MBasePlanPair>() {
                    @Override
                    public boolean apply(MBasePlanPair t) {
                        return t.point_id.equals(story.point_id);
                    }
                });
                double price = 0;
                if (story.debtList.size() > 0) {
                    price = price + Utils.getDebt(story.debtList);
                }
                if (story.order_type_id != null) {
                    if (story.order_type_id.equals(Configure.getSession().getList(MOrderType.class, " index1 = 0").get(0).idu)) {
                        for (MProduct ms : story.getSalesProductList()) {
                            price = price + (ms.getAmount_core() * ms.price);
                        }
                    }
                }
                if (pair != null) {
                    pair.price = pair.price + price;
                } else {
                    MPlanPair p = new MPlanPair();
                    p.point_id = story.point_id;
                    p.price = price;
                    list.add(p);
                }
            }
        }
        return list;
    }

    public static List<BasePlanMML> getListPlanMML(boolean isOld) {

        List<BasePlanMML> res = null;
        if (!isOld) {
            res = new ArrayList<BasePlanMML>(Configure.getSession().getList(PlanMML.class, null));
        } else {
            res = new ArrayList<BasePlanMML>(Configure.getSession().getList(PlanMMLOld.class, null));
        }
        return res;
    }

    public static List<PlaneBase> getPlanE(boolean isOld) {

        List<PlaneBase> list = null;
        if (!isOld) {
            List<PlanE> as = Configure.getSession().getList(PlanE.class, null);
            list = new ArrayList<PlaneBase>(as);
        } else {
            list = new ArrayList<PlaneBase>(Configure.getSession().getList(PlanEOld.class, null));
        }
        return list;
    }

    public static Map<String, List<String>> getMMLNameProducts(List<BasePlanMML> planMMLList) {
        Map<String, List<String>> listMap = new HashMap<>();
        for (BasePlanMML mml : planMMLList) {
            if (listMap.containsKey(mml.mml)) {
                List<String> strings = listMap.get(mml.mml);
                if (strings.contains(mml.product_id)) {
                } else {
                    strings.add(mml.product_id);
                    listMap.put(mml.mml, strings);
                }
            } else {
                List<String> strings = new ArrayList<>();
                strings.add(mml.product_id);
                listMap.put(mml.mml, strings);
            }
        }
        return listMap;
    }

    public static List<MBasePlanPointProducts> getListMPlanPointProducts(boolean isOld) {

        List<MBasePlanPointProducts> list = null;
        if (!isOld) {
            list = new ArrayList<MBasePlanPointProducts>(Configure.getSession().getList(MPlanPointProducts.class, null));

        } else {
            list = new ArrayList<MBasePlanPointProducts>(Configure.getSession().getList(MPlanPointProductsOld.class, null));
        }
        return list;
    }
}


