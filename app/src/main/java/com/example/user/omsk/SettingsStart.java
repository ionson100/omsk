package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;


public class SettingsStart implements Serializable {

    public static SettingsStart core() {
        return (SettingsStart) Reanimator.get(SettingsStart.class);
    }

    public static void save() {
        Reanimator.save(SettingsStart.class);
    }

    private StateUpdate isFirstStartUpdate = StateUpdate.defaultInsert;

    public StateUpdate isFirstStartUpdate() {
        return isFirstStartUpdate;
    }

    public void setIsFirstStartUpdate(StateUpdate isFirstStartUpdate) {
        this.isFirstStartUpdate = isFirstStartUpdate;
        save();
    }
}
