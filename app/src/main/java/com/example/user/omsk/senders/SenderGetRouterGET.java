package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;


import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.route.SettingsRoute;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

public class SenderGetRouterGET {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderGetRouterGET.class);

    int statusE = 0;
    private Activity activity;
    private String urlCore;
    private Date date = null;
    private String guid;
    private IActionRoute iActionRoute;
    private SettingsRoute routeSettings;

    public void send(Activity activity, String url, Date date, String guid, IActionRoute iActionRoute) {
        this.activity = activity;
        this.urlCore = url;
        this.date = date;
        this.guid = guid;
        this.iActionRoute = iActionRoute;
        new SenderWorkerTack().execute();
    }


    public interface IActionRoute {
        void Action(SettingsRoute routeSettings);
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(activity, Settings.core())) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, "Получение данных", null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String string) {
            if (isCancelled()) return;
            if (string == null && iActionRoute != null && iActionRoute != null) {
                iActionRoute.Action(routeSettings);
            } else {
                if (statusE == 205) {
                    Toast.makeText(activity, string, Toast.LENGTH_SHORT).show();
                }
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/client/track/:  " + string;
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                log.error(msg);
                //new SenderErrorPOST().send(activity, msg, Settings.core());
            }
            if (dialog != null) {
                dialog.cancel();
            }
        }


        @Override
        protected String doInBackground(Void... params) {

            if (isCancelled()) return null;
            String str = null;
            final String[] result = {null};

            try {
                str = Utils.HTTP + urlCore + "/client/track/?guid=" + URLEncoder.encode(guid, "UTF-8") + "&date=" + URLEncoder.encode(String.valueOf((int) (date.getTime() / 1000)), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return e.getMessage();
            }

            result[0] = UtilsSender.getRequest((MyApplication) activity.getApplication(), str, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {


                    statusE = status;
                    if (str.toLowerCase().equals("ok")) {
                    } else {
                        App(str);
                    }


                }
            });
            return result[0];
        }

        private void App(String res) {
            Gson sd3 = Utils.getGson();
            routeSettings = sd3.fromJson(res, SettingsRoute.class);
            routeSettings.dateNew_22 = date;
        }
    }
}
