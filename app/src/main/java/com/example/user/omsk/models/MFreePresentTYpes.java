package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

import java.io.Serializable;

/**
 * тип акции по которой призентуем сигареты
 */
@Table("present_types")
public class MFreePresentTYpes implements IUsingGuidId, Serializable {

    public MFreePresentTYpes() {
    }

    public MFreePresentTYpes(String idu, String name, String description, boolean isActive) {

        this.description = description;
        this.idu = idu;
        this.name = name;
        this.isActive = isActive;
    }

    @PrimaryKey("id")
    public int id;

    @Column("idu")
    public String idu;

    @Column("name")
    public String name;

    @Column("description")
    public String description;

    @Column("is_active")
    public boolean isActive;

    public String get_id() {
        return idu;
    }
}
