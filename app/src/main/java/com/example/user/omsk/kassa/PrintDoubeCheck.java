package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.widget.TextView;

import com.example.user.omsk.Settings;

class PrintDoubeCheck {
    public static void print(Settings mSettings, TextView log, Activity activity, AnnulCheck.TempCheck tempCheck) {

        if (tempCheck.mChecksData == null || tempCheck.mChecksData.isLast) {
            PrintLastCheck.printLast(mSettings, activity, log);
        } else {
            PrintMyLastCheck.printLast(mSettings, activity, log, tempCheck);
        }
    }
}
