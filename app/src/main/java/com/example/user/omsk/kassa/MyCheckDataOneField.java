package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MProductConstructor;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.orm2.IUserType;
import com.google.gson.Gson;

public class MyCheckDataOneField implements IUserType {
    @Override
    public Object getObject(String str) {
        Gson gson = Utils.getGson();
        MChecksData checksData = gson.fromJson(str, MChecksData.class);
        ISession session = Configure.getSession();
        for (MProductBase mProductBase : checksData.productList) {
            MProductBase productBase = session.get(MProductConstructor.class, mProductBase.idu);
            if (productBase == null) continue;
            mProductBase.name = productBase.name;
            mProductBase.sku = productBase.sku;
            mProductBase.nameKKT = productBase.nameKKT;
        }
        return checksData;
    }

    @Override
    public String getString(Object ojb) {
        String string = Utils.getGson().toJson(ojb);
        return string;
    }
}
