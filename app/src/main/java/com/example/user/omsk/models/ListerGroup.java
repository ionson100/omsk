package com.example.user.omsk.models;

import com.example.user.omsk.Ttesting;
import com.example.user.omsk.auto.MAuto;
import com.example.user.omsk.fotocontrol.MPointImage;
import com.example.user.omsk.kassa.MChecks;
import com.example.user.omsk.kassa.MZReport;
import com.example.user.omsk.plan.MPlanPair;
import com.example.user.omsk.plan.MPlanPairOld;
import com.example.user.omsk.plan.MPlanPointProducts;
import com.example.user.omsk.plan.MPlanPointProductsOld;
import com.example.user.omsk.plan.ModelMML;
import com.example.user.omsk.plan.ModelMMLOld;
import com.example.user.omsk.plan.PlanE;
import com.example.user.omsk.plan.PlanEOld;
import com.example.user.omsk.plan.PlanMML;
import com.example.user.omsk.plan.PlanMMLOld;
import com.example.user.omsk.plan.TempProduct;

import java.util.ArrayList;
import java.util.List;

public class ListerGroup {
    public static final List<Class> classes=new ArrayList<>();
    static {
        classes.add(MAction.class);
        classes.add(MAgent.class);
        classes.add(MAlertPoint.class);
        classes.add(MAuto.class);
        classes.add(MChat.class);
        classes.add(MChecks.class);
        classes.add(MComments.class);
        classes.add(MContact.class);
        classes.add(MContactor.class);
        classes.add(MDebt.class);

        classes.add(MDistributionchannel.class);
        classes.add(MFreePresentTYpes.class);
        classes.add(MNewOrder.class);
        classes.add(ModelMML.class);
        classes.add(ModelMMLOld.class);
        classes.add(MOrderType.class);
        classes.add(MPhoto.class);
        classes.add(MPlanPair.class);
        classes.add(MPlanPairOld.class);
        classes.add(MPlanPointProducts.class);

        classes.add(MPlanPointProductsOld.class);
        classes.add(MProduct.class);
        classes.add(MPoint.class);
        classes.add(MPointImage.class);
        classes.add(MPointType.class);
        classes.add(MPresent.class);
        classes.add(MPrice.class);
        classes.add(MPriceType.class);
        classes.add(MPriority.class);
        classes.add(MPriorityProduct.class);
        classes.add(MProduct.class);

        classes.add(MProductConstructor.class);
        classes.add(MProductGroup.class);
        classes.add(MPromotion.class);
        classes.add(MRoute.class);
        classes.add(MSaleRows.class);
        classes.add(MStockRows.class);
        classes.add(MStoryTaskPoint.class);
        classes.add(MTask.class);
        classes.add(MTaskPoint.class);
        classes.add(MTemplatePropety.class);
        classes.add(MTripletPoduct.class);

        classes.add(MTripletPoductStory.class);
        classes.add(MVideo.class);
        classes.add(MVisitPresent.class);
       // classes.add(MVisitPresentAss.class);
        classes.add(MVisitResults.class);
        classes.add(MVisitStory.class);
        classes.add(MVisitStoryAss.class);
        classes.add(MVisitStoryPoint.class);
        classes.add(MVisitStoryPointDay.class);
        classes.add(MZReport.class);
        classes.add(PlanE.class);

        classes.add(PlanEOld.class);
        classes.add(PlanMML.class);
        classes.add(PlanMMLOld.class);
        classes.add(TempProduct.class);
        classes.add(Ttesting.class);
        classes.add(User1.class);

        classes.add(MSummaKKM.class);



    }
}
