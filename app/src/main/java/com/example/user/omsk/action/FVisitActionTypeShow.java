package com.example.user.omsk.action;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class FVisitActionTypeShow extends Fragment {

    public FVisitActionTypeShow() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_visit_action_type_show, container, false);

        ListView mListView = (ListView) mView.findViewById(R.id.list_action);

        List<MAction> mActionList = Configure.getSession().getList(MAction.class, null);

        Collections.sort(mActionList, new Comparator<MAction>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MAction lhs, MAction rhs) {
                return lhs.finish_action_new_22.compareTo(rhs.finish_action_new_22);
            }
        });
        Collections.reverse(mActionList);

        ListAdapterForActionTypes presentTypes = new ListAdapterForActionTypes(getActivity(), R.layout.item_list_action_type, mActionList);
        mListView.setAdapter(presentTypes);
        return mView;
    }

    private class ListAdapterForActionTypes extends ArrayAdapter<MAction> {

        private final List<MVisitStory> mVisitStories;
        private final int resource;
        private Date curdate =null;

        ListAdapterForActionTypes(Context context, int resource, List<MAction> objects) {
            super(context, resource, objects);
            this.resource = resource;
            mVisitStories =  Configure.getSession().getList(MVisitStory.class, null);
            curdate = Utils.curDate();
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View mView = convertView;

            final MAction p = getItem(position);
            if (p != null) {

                mView = LayoutInflater.from(getContext()).inflate(resource, null);

                TextView actionArhive = (TextView) mView.findViewById(R.id.action_arhive);
                TextView name = (TextView) mView.findViewById(R.id.action_name);
                TextView desc = (TextView) mView.findViewById(R.id.action_description);
                TextView start = (TextView) mView.findViewById(R.id.action_start);
                TextView finish = (TextView) mView.findViewById(R.id.action_finish);
                ImageView imageView = (ImageView) mView.findViewById(R.id.image_action);

                if (p.file_name == null || p.file_name.trim().length() == 0) {
                    imageView.setVisibility(View.GONE);
                } else {
                    File imgFile = new File(Utils.getPromotionDirectory() + "/" + p.file_name);
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        imageView.setImageBitmap(myBitmap);
                    } else {
                        imageView.setVisibility(View.GONE);
                    }
                }

                name.setText(p.name);
                desc.setText(p.description);
                if (p.isActive) {
                    actionArhive.setVisibility(View.VISIBLE);
                }
                start.setText(getString(R.string.start_action1) + " " + Utils.simpleDateFormatForCommentsE(p.start_action_new_22));
                finish.setText(getString(R.string.finish_action) + " " + Utils.simpleDateFormatE(p.finish_action_new_22));
                if (p.finish_action_new_22.getTime() < curdate.getTime()) {
                    mView.setBackgroundResource(R.color.d5caed);
                } else {
                    mView.setBackgroundResource(R.color.caede7);
                }
            }
            return mView;
        }
    }
}
