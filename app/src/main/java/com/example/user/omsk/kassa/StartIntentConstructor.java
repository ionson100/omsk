package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;

import com.example.user.omsk.Settings;

public class StartIntentConstructor {
    public static void start(Activity mainActivity, Settings mSettings) {
        Intent intent = new Intent(mainActivity, ConstructorActivity.class);
        mainActivity.startActivity(intent);
    }
}
