package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;

import com.example.user.omsk.models.MContact;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.setting.Reanimator;
import com.example.user.omsk.setting.SettingField;
import com.example.user.omsk.setting.TypeField;

import java.io.Serializable;
import java.util.Date;

public class Settings implements Serializable {

    public boolean useAuto;

    public String ofd_url = "ofdp.platformaofd.ru";
    public int ofd_port = 21101;


    public String settingsKassa;

    public double maxSummPrice = 100000d;

    public String jsonGroup;

    public MVisit visitFromError;

    // ЮЗЕР АГЕНТЕ для имитации браузера при запросе фрагметов карт с опенстритмап
    public String user_agent;
    // поле где лежит  тип цены для ларька
    public String typePriceStationaryShop;

    public long timeFinished;


    public String ipPing = "104.219.251.211";
    // город для ареса точки
    public String city;
    public String kkmNumber = "";
    public String password;
    public String dadata = "";
    public String uid_auto = "";

    public static synchronized Settings core() {
        return (Settings)
                Reanimator.get(Settings.class);
    }

    public static void save() {
        Reanimator.save(Settings.class);
    }

    void activateStart() {

        start_dey = false;

        save();
    }

    /**
     * урл по умолчанию, как правило боевой сервер
     */
    @SettingField(
            image = R.drawable.server,
            descriptions = R.string.url_description,
            index = 1,
            title = R.string.url_titl,
            IListItem = UrlList.class,
            typeField = TypeField.list)
    public String  url = "bs000.net";//178.62.242.187

    @SettingField(
            descriptions = R.string.show_stock_deck,
            index = 2,
            title = R.string.stock,
            typeField = TypeField.BooleanSwitch)
    public boolean methodShowStock = true;

    @SettingField(
            descriptions = R.string.show_balance,
            index = 3,
            title = R.string.show_balance_titl,
            typeField = TypeField.BooleanSwitch)
    public boolean isShowBalance = true;

    @SettingField(
            descriptions = R.string.show_sell_point_dis,
            index = 4,
            title = R.string.show_sell_point_titl,
            typeField = TypeField.BooleanSwitch)
    //показывать продукты проданные в этом месяце по точке
    public boolean isShowSellProduct = true;

    @SettingField(
            descriptions = R.string.story_point_des,
            index = 6,
            title = R.string.story_point,
            typeField = TypeField.BooleanSwitch)
    public boolean distanceStoryPoint;

    @SettingField(
            descriptions = R.string.show_visit_type,
            index = 7,
            title = R.string.show_visit,
            typeField = TypeField.BooleanSwitch)
    public boolean showOldViewForVisit;


    @SettingField(

            index = 8,
            title = R.string.sku,
            typeField = TypeField.BooleanSwitch)
    public boolean isShowSku = false;

    @SettingField(

            index = 9,
            title = R.string.show_debt_point,
            typeField = TypeField.BooleanSwitch)
    public boolean showDebtPoints;


    ///////////////////
    @SettingField(

            index = 10,
            title = R.string.useVisit2,
            typeField = TypeField.BooleanSwitch)
    public boolean useVisit2;


    //////////////////////////////////////////////
    @SettingField(

            descriptions = R.string.yandex_desc,
            index = 11,
            title = R.string.yandex,

            typeField = TypeField.BooleanSwitch)
    public boolean useYandexGPS;

    @SettingField(

            descriptions = R.string.calculator_desc,
            index = 12,
            title = R.string.calculator,

            typeField = TypeField.BooleanSwitch)
    public boolean useCalculator;

    @SettingField(
            descriptions = R.string.usePaintRoute_desc,
            index = 13,
            title = R.string.usePaintRoute,

            typeField = TypeField.BooleanSwitch)
    public boolean usePaintRoute;


    @SettingField(
            descriptions = R.string.checkNetForSender_desc,
            index = 13,
            title = R.string.checkNetForSender,

            typeField = TypeField.BooleanSwitch)
    public boolean checkNetForSender = true;


    ////////////////////
    @SettingField(
            image = R.drawable.color,
            descriptions = R.string.km_prob_2,
            index = 14,
            title = R.string.km_prob,
            typeField = TypeField.Integer)
    public int max_km = 1500;


    //////////////////////////////////////////////////
    @SettingField(
            image = R.drawable.color,
            defaultColor = Color.BLACK,
            descriptions = R.string.color_des,
            index = 15,
            title = R.string.no_visit,
            typeField = TypeField.Color)
    public int colorTextPointNoVisitNew = Color.parseColor("#213a7a");
    ;

    @SettingField(
            image = R.drawable.color,
            index = 16,
            defaultColor = Color.BLUE,
            title = R.string.color_visit,
            typeField = TypeField.Color)
    public int colorTextPointVisit = Color.BLUE;

    @SettingField(
            image = R.drawable.color,
            descriptions = R.string.color_des2,
            index = 17,
            defaultColor = Color.RED,
            title = R.string.color_stock,
            typeField = TypeField.Color)
    public int colorTextStock = Color.RED;
    /**
     * стартовые позиции при показе карты
     */
    private double start_latitude = Utils.LATITUDE;

    /**
     * стартовые позиции при показе карты
     */
    private double start_longitude = Utils.LONGITUDE;
    /**
     * Объект посещения
     */
    private MVisit visit;

    /**
     * сотояние всей системы//volatile
     */
    private int stateSystem = StateSystem.HOME;
    /**
     * время последней синхронизации
     */
    private int lastSynchronization;
    /**
     * промежуточное состояние для редактирования контакта
     */
    private transient MContact editContact;

    public String currentVersionName;

    public Date current_time_22 = null;

    public String serverVersionName;

    public int getLastSynchronization() {
        return lastSynchronization;
    }

    /**
     * время последней синхронизации
     *
     * @param lastSynchronization
     */
    public void setLastSynchronization(int lastSynchronization) {
        this.lastSynchronization = lastSynchronization;
        save();
    }

    /**
     * редактируемый контакт
     *
     * @return
     */
    public MContact getEditContact() {
        return editContact;
    }

    public void setEditContact(MContact editContact) {
        this.editContact = editContact;
        save();
    }

    /**
     * Визит, если открыт
     *
     * @return
     */
    public MVisit getVisit() {
        return visit;
    }

    public void setVisit(MVisit visit) {
        this.visit = visit;
        save();
    }

    public static int getStateSystem() {
        return Settings.core().stateSystem;
    }

    public static void showFragment(int stateSystem, Activity activity) {
        Settings.core().stateSystem = stateSystem;
        save();
        new FactoryFragment().Action(Settings.core(), (AppCompatActivity) activity);
    }

    /**
     * проброс страницы старта при системной не отловленной ошибкой
     *
     * @param stateSystem
     */
    public void setStateSystemForApplication(int stateSystem) {
        this.stateSystem = stateSystem;
        save();
    }


    //private String teplephone="";

    public double getStartLatitude() {
        return start_latitude;
    }

    public void setStartLatitude(double start_latitude) {
        this.start_latitude = start_latitude;
        save();
    }

    public double getStartLongitude() {
        return start_longitude;
    }

    public void setStartLongitude(double start_longitude) {
        this.start_longitude = start_longitude;
        save();
    }

    private boolean start_dey;

    /**
     * Сотояния торгового дня
     *
     * @return
     */
    public boolean isStart_dey() {
        return start_dey;
    }

    private Date startDeyDate_22;


    /**
     * время старта торгового дня
     *
     * @return
     */
    public Date getStartDeyDate_22() {
        return startDeyDate_22;
    }

    public void setStart_dey(boolean start_dey) {
        this.start_dey = start_dey;
        if (start_dey) {
            startDeyDate_22 = Utils.curDate();
        } else {
            startDeyDate_22 = null;
        }
        save();
    }


//    /**
//     * номер телефона на планшете
//     *
//     * @return
//     */
//    public String getTeplephone() {
//        return teplephone;
//    }
//
//    /**
//     * номер телефона на планшете
//     *
//     * @param teplephone
//     */
//    public void setTeplephone(String teplephone) {
//        this.teplephone = "";
//        if (teplephone != null && teplephone.length() > 10) {
//            this.teplephone = teplephone.substring(0, 11);
//        }
//        save();
//    }

    private int stateSearchPoint;

    /**
     * атрибуты поиска точки (имя адрес хозяин)
     *
     * @return
     */
    public int getStateSearchPoint() {
        return stateSearchPoint;
    }

    public void setStateSearchPoint(int stateSearchPoint) {
        this.stateSearchPoint = stateSearchPoint;
        save();
    }


}
