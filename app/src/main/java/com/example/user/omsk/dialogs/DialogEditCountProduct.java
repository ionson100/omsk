package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.CalculateKeyBoard;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.TransceiveStaticVariables;
import com.example.user.omsk.Transceiver;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.pupper.PupperPartial;


public class DialogEditCountProduct extends DialogFragment {


    private IActionE<Object> iActionEDismis;

    private EditText tv;

    public void setDismis(IActionE<Object> dismis) {
        this.iActionEDismis = dismis;
    }

    public IActionE<Object> iActionEForDebt;
    public IActionE<Object> iActionEForSale;
    private MProduct mProduct;
    private View.OnClickListener onClickListener;
    private MAction mAction;

    public DialogEditCountProduct setProduct(MProduct product) {
        this.mProduct = product;
        return this;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_count_product, null);
        ((TextView) v.findViewById(R.id.title_dalog)).setText(getString(R.string.sdkisudd));
        if (Settings.getStateSystem() == StateSystem.VISIT_ACTION) {
            PupperPartial textView1 = (PupperPartial) v.findViewById(R.id.max_bonus);

            textView1.setPairString(getString(R.string.max_bonus), String.valueOf(mAction.currentDeripaskeJs.amount_max));
            textView1.setVisibility(View.VISIBLE);

        }
        PupperPartial price = (PupperPartial) v.findViewById(R.id.price_product);
        price.setPairString("Цена руб./пач.: ", String.valueOf(mProduct.price));
        builder.setView(v);
        tv = (EditText) v.findViewById(R.id.edit_count);
        tv.setText(Utils.getStringDecimal(mProduct.getAmount_core()));

        PupperPartial tv_name = (PupperPartial) v.findViewById(R.id.name_product_core);
        tv_name.setPairString("Продукт:", mProduct.name);
        Button btOk = (Button) v.findViewById(R.id.btOk);
        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okProduct(tv);
            }
        });
        tv.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    okProduct(tv);
                }
                return false;
            }
        });

        final TextView textViewItogo = (TextView) v.findViewById(R.id.label_itogo);
        textViewItogo.setText("Итого: " + Utils.getStringDecimal(mProduct.getAmount_core() * mProduct.price) + " руб.");
        tv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                double dd = 0;
                try {
                    dd = Double.parseDouble(s.toString());
                } catch (Exception ignored) {
                }
                textViewItogo.setText("Итого: " + Utils.getStringDecimal(dd * mProduct.price) + " руб.");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Button delete = (Button) v.findViewById(R.id.btdelete_row);
        Button delete_all = (Button) v.findViewById(R.id.btdelete_all_row);
        Button cancel = (Button) v.findViewById(R.id.btCancel);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onClickListener == null) {
                    Transceiver.send(TransceiveStaticVariables.F2323, mProduct.get_id());
                } else {
                    onClickListener.onClick(v);
                }

                Settings.save();
                if (iActionEForDebt != null) {
                    MProduct ss = null;
                    for (MProduct s : Settings.core().getVisit().selectProductForBalance) {
                        if (s.idu.equals(mProduct.get_id())) {
                            ss = s;
                        }
                    }
                    if (ss != null) {
                        Settings.core().getVisit().selectProductForBalance.remove(ss);
                        Settings.save();
                        iActionEForDebt.action(null);
                    }

                }
                if (iActionEForSale != null) {
                    MProduct ss = null;
                    for (MProduct s : Settings.core().getVisit().selectProductForSale) {
                        if (s.idu.equals(mProduct.get_id())) {
                            ss = s;
                        }
                    }
                    if (ss != null) {
                        Settings.core().getVisit().selectProductForSale.remove(ss);
                        Settings.save();
                        iActionEForSale.action(null);
                    }
                }
                dismiss();
            }
        });

        delete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onClickListener == null) {
                    Transceiver.send(TransceiveStaticVariables.F2323, null);
                } else {
                    onClickListener.onClick(null);
                }


                if (iActionEForDebt != null) {
                    Settings.core().getVisit().selectProductForBalance.clear();
                    iActionEForDebt.action(null);
                }

                if (iActionEForSale != null) {
                    Settings.core().getVisit().selectProductForSale.clear();
                    iActionEForSale.action(null);
                }

                Settings.save();
                dismiss();
            }
        });

        new CalculateKeyBoard(tv, v, Settings.core(), false, getActivity());

        if (Settings.core().useCalculator) {
            tv.setFocusable(false);
        } else {

            View sss = v.findViewById(R.id.panel_calculator);
            sss.setVisibility(View.GONE);
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return builder.create();
    }

    private void okProduct(TextView tv) {
        double res = 0;
        try {
            res = Double.parseDouble(tv.getText().toString().replace(",", "."));
        } catch (Exception ignored) {

        }


        if (mAction != null) {
            boolean validate = validateAmount(res);
            if (validate == false) {
                return;
            }
        }


        mProduct.setAmountCore(res);
        mProduct.isEdit = true;
        Settings.save();
        if (iActionEForDebt != null) {
            iActionEForDebt.action(null);
        }
        if (iActionEForSale != null) {
            iActionEForSale.action(null);
        }
        if (mProduct.getAmount_core() == 0) {
            Transceiver.send(TransceiveStaticVariables.F2323, mProduct.get_id());
        } else {
            Transceiver.send(TransceiveStaticVariables.F2323, "ds");
        }


        dismiss();
    }

    private boolean validateAmount(double res) {

        if (res > mProduct.amount_action_max) {
            Toast.makeText(getActivity(), getString(R.string.sd23) + String.valueOf(mProduct.amount_action_max) + getString(R.string.rt), Toast.LENGTH_LONG).show();
            return false;
        }

        double amountCurrent = Utils.AmountCurrent(mAction.idu, mProduct.get_id()) + res;

        if (amountCurrent > mAction.currentDeripaskeJs.amount_max) {

            Toast.makeText(getActivity(), getString(R.string.sdasddads) + String.valueOf(mAction.currentDeripaskeJs.amount_max) + getString(R.string.rt), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void setActionId(MAction action_id) {

        this.mAction = action_id;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (iActionEDismis != null) {
            iActionEDismis.action(null);
        }
    }
}

