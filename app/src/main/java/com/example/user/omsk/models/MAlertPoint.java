package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

// хранилище  пользовательких сообщений для точки, которые пользователь видит посещая точку
@Table("alert_point")
public class MAlertPoint {

    @PrimaryKey("id")
    public int id;

    @Column("point_id")
    public String point_id;

    @Column("message")
    public String message;
}
