package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

// хранилище текущего маршрута
@Table("route")
public class MRoute {

    @PrimaryKey("id")
    public int id;

    @Column("point_id")
    public String point_id;

    public MPoint getPoint() {
        MPoint point = Configure.getSession().get(MPoint.class, point_id);
        if (point == null) {
            point = new MPoint();
            point.name = "Торговая точка не найдена в Вашем регионе, попробуйте обновить данные или обратитесь к администратору";
            point.fuflo = true;
        }
        return point;
    }
}
