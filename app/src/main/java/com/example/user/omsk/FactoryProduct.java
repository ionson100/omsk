package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MPriorityProduct;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductGroup;
import com.example.user.omsk.linq2.Linq;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class FactoryProduct {

    public static List getProductsFree(List<MProduct> list) {
        return Linq.toStream(list).where(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct o) {
                return (o.group_id == null || o.group_id.length() == 0);
            }
        }).toList();
    }

    public static List<MProduct> getListForMenu(List<MProduct> list, final String group_id) {
        return Linq.toStream(list).where(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.group_id != null && t.group_id.equals(group_id);
            }
        }).toList();
    }

    public static List<MProduct> getListPrioryProduct(List<MProduct> list, List<MPriorityProduct> mPriorityProducts) {
        List<MProduct> list1 = new ArrayList<>();
        for (MPriorityProduct mPriorityProduct : mPriorityProducts) {
            for (MProduct product : list) {
                if (product.idu.equals(mPriorityProduct.product_id)) {
                    list1.add(product);
                }
            }
        }
        return list1;
    }

    public static List<MProduct> getListAmountSafe(List<MProduct> list) {
        List<MProduct> list1 = new ArrayList<>();
        for (MProduct product : list) {
            if (product.amountSelf != 0) {
                list1.add(product);
            }
        }
        return list1;
    }

    public static List<MProductGroup> getProductGroupsFree(List<MProductGroup> list) {
        List<MProductGroup> list1 = new ArrayList<>();
        for (MProductGroup mProductGroup : list) {
            if (mProductGroup.parent_id == null || mProductGroup.parent_id.length() == 0) {
                mProductGroup.isSelect = false;
                list1.add(mProductGroup);
            }
        }
        return list1;
    }


    public static List<MProduct> getListAbs(List<MProduct> list, final String s) {

        return Linq.toStream(list).where(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.name.toLowerCase().startsWith(s.toLowerCase());
            }
        }).toList();


    }

    public static List<MProduct> getSearchResult(List<MProduct> list, final String text) {

        return Linq.toStream(list).where(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct o) {
                return o.name.toLowerCase(new Locale("ru", "RU")).contains(text.toLowerCase(new Locale("ru", "RU")));
            }
        }).toList();

    }

    public static List<MProduct> getListMMLGroup(List<MProduct> mGlobalProducts, List<String> productIdList) {
        List<MProduct> list = new ArrayList<>();
        for (MProduct mProduct : mGlobalProducts) {
            if (productIdList.contains(mProduct.get_id())) {
                list.add(mProduct);
            }
        }
        return list;
    }
}
