package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.IUserType;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Map;

public class MyPlanJsonField implements IUserType {
    @Override
    public Object getObject(String str) {
        Gson gson = Utils.getGson();
        Map<String, GroupDouble> actionList = gson.fromJson(str, new TypeToken<Map<String, GroupDouble>>() {
        }.getType());
        return actionList;
    }

    @Override
    public String getString(Object ojb) {
        String string = Utils.getGson().toJson(ojb);
        return string;
    }
}
