package com.example.user.omsk.pupper;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.R;

public class Pupper extends LinearLayout {
    private TextView mTextViewTitul;
    private TextView mTextViewValue;

    public Pupper(Context context) {
        super(context);
        init();
    }

    public Pupper(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Pupper(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public TextView getTitul() {
        return mTextViewTitul;
    }

    public TextView getValue() {
        return mTextViewValue;
    }

    public void setPairString(int titul, int value) {
        mTextViewTitul.setText(titul);
        mTextViewValue.setText(value);
    }

    public void setPairString(String titul, int value) {
        mTextViewTitul.setText(titul);
        mTextViewValue.setText(value);
    }

    public void setPairString(int titul, String value) {
        mTextViewTitul.setText(titul);
        mTextViewValue.setText(value);
    }

    public void setPairString(String titul, String value) {
        mTextViewTitul.setText(titul);
        mTextViewValue.setText(value);
    }

    private void init() {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.pupper, null, false);
        mTextViewTitul = (TextView) view.findViewById(R.id.pupper_titul);
        mTextViewValue = (TextView) view.findViewById(R.id.pupper_value);
        this.addView(view, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
    }
}
