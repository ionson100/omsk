package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.omsk.models.MVisit;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IMapView;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.TileSystem;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.UUID;

class MapVisit implements ViewTreeObserver.OnGlobalLayoutListener {

    private final boolean isHorizon;
    private ViewTreeObserver vto2;
    private GeoPoint myPointShop;
    private MapView mMapView;
    private MapItemizedOverlay myOverlaShop;
    private MapItemizedOverlay myOverlaFix;
    private boolean[] runer = {false};
    private MVisit mVisit;
    private boolean[] run = {true};
    private vtoTemp vtoTemp;

    MapVisit(View mView, Context context, final MVisit mVisit, final boolean isHorizon) {

        this.mVisit = mVisit;
        this.isHorizon = isHorizon;
     //   Settings mSettings = Settings.core();
        if (mVisit.latitude == 0 || mVisit.longitude == 0) {
            return;
        }
        mMapView = (MapView) mView.findViewById(R.id.mapview_visit);
        mMapView.setVisibility(View.VISIBLE);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setClickable(true);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        mMapView.setClickable(true);
        mMapView.setTileSource(TileSourceFactory.MAPNIK);
        mMapView.getController().setZoom(15);
        myOverlaShop = new MapItemizedOverlay(context.getResources().getDrawable(R.drawable.shop), context);
        myOverlaFix = new MapItemizedOverlay(context.getResources().getDrawable(R.drawable.fix), context);
        vto2 = mMapView.getViewTreeObserver();
        vto2.addOnGlobalLayoutListener(this);
        ///////////////////////////
        if (mVisit.point.latitude != 0 && mVisit.point.longitude != 0) {
            myPointShop = new GeoPoint(mVisit.point.latitude, mVisit.point.longitude);
            String name = mVisit.point.name;
            OverlayItem overlayitem2 = new OverlayItem(UUID.randomUUID().toString(), "Торговая точка", name, myPointShop);
            myOverlaShop.addOverlay(overlayitem2);
        } else {
            View view = mView.findViewById(R.id.error_message);
            if (view != null) {
                view.setVisibility(View.VISIBLE);
            }
            myPointShop = new GeoPoint(0, 0);//mSettings.getStartLatitude()mSettings.getStartLongitude()
            String name = context.getString(R.string.dasd);
            OverlayItem overlayitem2 = new OverlayItem(UUID.randomUUID().toString(), "Торговая точка", name, myPointShop);
            myOverlaShop.addOverlay(overlayitem2);
        }
        ///////////////////////////
        if (mVisit.latitude != 0 && mVisit.longitude != 0) {
            final GeoPoint myPoint2 = new GeoPoint(mVisit.latitude, mVisit.longitude);
            double dif = Utils.getDistance(mVisit.latitude, mVisit.longitude, mVisit.point.latitude, mVisit.point.longitude);
            String str2 = context.getString(R.string.bvn) + Utils.getStringDecimal(dif) + " м.";
            OverlayItem overlayitem3 = new OverlayItem(UUID.randomUUID().toString(), context.getString(R.string.gfg), str2, myPoint2);
            myOverlaFix.addOverlay(overlayitem3);
        }
        //////////////////////////////////
        mMapView.getOverlays().add(myOverlaShop);
        mMapView.getOverlays().add(myOverlaFix);
        mMapView.getController().setCenter(myPointShop);
        mMapView.getController().animateTo(myPointShop);
        mMapView.invalidate();
        //////////////////////////////////
        mMapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                run[0] = false;
                return false;
            }
        });
        vtoTemp = new vtoTemp();
        vto2.addOnGlobalLayoutListener(vtoTemp);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    void destory() {
        mMapView.getOverlays().remove(myOverlaShop);
        mMapView.getOverlays().remove(myOverlaFix);
        mMapView.getTileProvider().clearTileCache();
        myOverlaShop = null;
        if (vto2 != null && vto2.isAlive()) {
            vto2.removeOnGlobalLayoutListener(this);
            if (vtoTemp != null) {
                vto2.removeOnGlobalLayoutListener(vtoTemp);
            }
        }
        vto2 = null;
        mMapView = null;
        System.gc();
    }

    @Override
    public void onGlobalLayout() {
        if (!runer[0]) {
            if (mMapView == null) return;
            int width = mMapView.getMeasuredWidth();
            int height = mMapView.getMeasuredHeight();
            double dis = Utils.getDistance(mVisit.latitude, mVisit.longitude, mVisit.point.latitude, mVisit.point.longitude);
            double del = 0;
            if (isHorizon) {
                double mm = (((double)height / 2) / 100) * 15;
                double real = ((double)height / 2) - mm;
                del = dis / real;
            } else {
                double mm = (((double)width / 2) / 100) * 15;
                double real = ((double)width / 2) - mm;

                del = dis / real;
            }
            int zoom = -1;
            if (del >= TileSystem.GroundResolution(mVisit.latitude, 2)) {
                zoom = 2;
            }
            for (int i = 3; i < 20; i++) {
                double dff = TileSystem.GroundResolution(mVisit.latitude, i);
                if (del >= dff) {
                    zoom = i - 1;
                    break;
                }
            }
            if (zoom == -1 && del < 1) {
                zoom = 19;
            }
            mMapView.getController().setZoom(zoom);
        }
        runer[0] = true;
    }

    private class vtoTemp implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            if (run[0]) {
                if (mMapView != null && mMapView.getController() != null) {
                    mMapView.getController().setCenter(myPointShop);
                }
            }
        }
    }
}

class MapItemizedOverlay2 extends ItemizedOverlay<OverlayItem> {
    private ArrayList<OverlayItem> mOverlays = new ArrayList<>();
    private Context mContext;

    public MapItemizedOverlay2(Drawable defaultMarker, Context context) {
        super(defaultMarker, new DefaultResourceProxyImpl(context));
        mContext = context;
    }

    public void addOverlay(OverlayItem overlay) {
        mOverlays.add(overlay);
        populate();
    }

    @Override
    protected OverlayItem createItem(int i) {
        return mOverlays.get(i);
    }

    @Override
    public int size() {
        return mOverlays.size();
    }

    protected boolean onTap(int index) {
        OverlayItem item = mOverlays.get(index);
        Dialog dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(true);
        TextView map_popup_header = (TextView) dialog.findViewById(R.id.map_popup_header);
        map_popup_header.setText(item.getTitle());
        TextView map_popup_body = (TextView) dialog.findViewById(R.id.map_popup_body);
        map_popup_body.setText(item.getSnippet());
        ImageView imgMoreInfo = (ImageView) dialog.findViewById(R.id.map_more_info_imageView);
        imgMoreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        dialog.show();
        return true;
    }


    @Override
    public boolean onSnapToItem(int arg0, int arg1, Point arg2, IMapView arg3) {
        return false;
    }
}
