package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

public class TestKassa {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TestKassa.class);
    Fptr fptr;

    public void test(Activity activity) {
        try {
            fptr = new Fptr();
            fptr.create(activity.getApplication());
            if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                checkError();
            }
            if (fptr.put_DeviceEnabled(true) < 0) {
                checkError();
            }
            if (fptr.GetStatus() < 0) {
                checkError();
            }
            if (fptr.put_UserPassword("00000030") < 0) {
                checkError();
            }
            if (fptr.put_Mode(IFptr.MODE_REPORT_NO_CLEAR) < 0) {
                checkError();
            }
            if (fptr.SetMode() < 0) {
                checkError();
            }
            if (fptr.put_ReportType(19) < 0) {
                checkError();
            }
            if (fptr.put_CheckNumber(280) < 0) {
                checkError();
            }
            String dff = "";
            int dd = fptr.BeginReport();
            while (dd == -1) {
                dff = fptr.get_Caption();
                dd = fptr.BeginReport();
            }
            fptr.EndReport();

        } catch (Exception ex) {

            log.error(ex);
            Utils.messageBox(activity.getString(R.string.error), ex.getMessage(), activity, null);
            new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), Settings.core().kkmNumber + "\n" +
                    "Ошибка ккм file  - TestKassa\n" + ex.getMessage());
        } finally {
            fptr.ResetMode();
            fptr.destroy();
        }
    }

    private void checkError() throws Exception {
        int rc = fptr.get_ResultCode();
        if (rc < 0) {
            String rd = fptr.get_ResultDescription(), bpd = null;
            if (rc == -6) {
                bpd = fptr.get_BadParamDescription();
            }
            if (bpd != null) {
                throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
            } else {
                throw new Exception(String.format("[%d] %s", rc, rd));
            }
        }
    }
}
