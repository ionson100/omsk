package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.UserField;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class PlaneBase {
    // словарь ммл название ььд и какая номенклатура принадлежит ему
    public final Map<String, List<String>> mml = new HashMap<>();
    @PrimaryKey("id")
    public int id;
    // сколько зеленых надо зажечь для плана по акб
    @Column("akb_points")
    public int akb_points;
    // сколько зеленых надо зажечь для плана по ььд
    @Column("mml_points")
    public int mml_points;
    // план продажи по всем ммл один на все
//    @Column("mml_amount")
//    public double mml_amount;
    // сколько товара надо продать за отчетный период в пачках
    @Column("amount")
    public double amount;
    // план по акб по точкам для каждой точки одно и тоже значение
    @Column("akb")
    public double akb;
    // минимальное количество млл для точки ( для выполнения плана)
    @Column("mml_group")
    public int mml_group;
    // название группы процент исполнения.
    @UserField(IUserType = MyPlanJsonField.class)
    @SerializedName("groups")
    @Column("plans")
    public Map<String, GroupDouble> plansMap = new HashMap<>();
}

