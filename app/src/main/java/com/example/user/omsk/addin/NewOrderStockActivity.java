package com.example.user.omsk.addin;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.database.SQLException;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.FactoryProduct;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.ListAdapterStock;
//import com.example.user.omsk.models.MError;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MProductGroup;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Action;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.refill.DialogEditCountProductForAddinStock;
import com.example.user.omsk.refill.ListAdapterAddinStock;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NewOrderStockActivity extends AppCompatActivity implements ListAdapterStock.OnRowDataClick {

    private static final org.apache.log4j.Logger log = Logger.getLogger(NewOrderStockActivity.class);
    private ListAdapterStock listAdapterStock;

    private List<MProduct> mProductList;
    private ListView lackListView;
    private ListView productListView;
    private Settings mSettings;
    private Button buttonDelete;


    private void init() {
     //   final Activity activity = this;

        Utils.pricecometr(mProductList, mSettings);
//        List<MProduct> products = new ArrayList<>();
//        for (MProduct product : mProductList) {
//            if (product.price == 0 || product.isArchive) {
//                products.add(product);
//            }
//        }

        buttonDelete = (Button) findViewById(R.id.bt_lack_to_server);

        lackListView = (ListView) findViewById(R.id.list_lack);
        productListView = (ListView) findViewById(R.id.list_product_lack);


        lackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {


                DialogEditCountProductForAddinStock dialog = new DialogEditCountProductForAddinStock();
                dialog.setProduct((MProduct) view.getTag()).setActionDelete(new IActionE() {
                    @Override
                    public void action(Object o) {
                        MProduct mProduct = (MProduct) o;
                        mSettings.getVisit().newOrderList.remove(mProduct);
                        addToListProduct(mProduct);
                        Collections.sort(mProductList, new Comparator<MProduct>() {
                            @Override
                            public int compare(MProduct lhs, MProduct rhs) {
                                return lhs.name.compareTo(rhs.name);
                            }
                        });
                        activateLack();
                        if (mSettings.getVisit().newOrderList.size() == 0) {
                            buttonDelete.setVisibility(View.GONE);
                        } else {
                            buttonDelete.setVisibility(View.VISIBLE);
                        }


                    }
                }).setActionSave(new IActionE() {
                    @Override
                    public void action(Object o) {
                        activateLack();

                    }
                }).show(getSupportFragmentManager(), "sdsd");

            }
        });
        lackListView.setOnCreateContextMenuListener(this);

        final EditText lack_search_product_edit = (EditText) findViewById(R.id.lack_search_product_edit);
        lack_search_product_edit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
        lack_search_product_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    showAll();
                } else {
                    showSearch(s.toString());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() > 0) {
                    lack_search_product_edit.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    //Assign your image again to the view, otherwise it will always be gone even if the text is 0 again.
                    lack_search_product_edit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
                }
            }
        });

        if (mSettings.getVisit().newOrderList.size() == 0) {
            buttonDelete.setVisibility(View.GONE);
        } else {
            buttonDelete.setVisibility(View.VISIBLE);
        }
    }

    private void showSearch(String str) {
        Linq.toStream(mProductList).forEach(new Action<MProduct>() {
            @Override
            public void action(MProduct mProduct) {
                mProduct.isSelect = false;
            }
        });
        List<Object> objects = new ArrayList<Object>(FactoryProduct.getSearchResult(mProductList, str));
        activateListProduct(objects,str);
    }

    private void showAll() {

        Linq.toStream(mProductList).forEach(new Action<MProduct>() {
            @Override
            public void action(MProduct mProduct) {
                mProduct.isSelect = false;
            }
        });

        List<Object> objectsnew = new ArrayList<Object>(mProductList);
        activateListProduct(objectsnew);
    }


    private void activateLack() {

        ListAdapterAddinStock adapterLock = new ListAdapterAddinStock(this, R.layout.item_lock, new ArrayList<>(mSettings.getVisit().newOrderList));
        Parcelable index = null;
        try {
            index = lackListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        lackListView.setAdapter(adapterLock);
        if (index != null) {
            lackListView.onRestoreInstanceState(index);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_new_order_stock);

            mProductList = Configure.getSession().getList(MProduct.class, "amount_safe2 <> 0");
            mSettings = Settings.core();
            activateABC();
            init();
            showAll();
            activateLack();


            ImageButton btMoveLeft = (ImageButton) findViewById(R.id.bt_lack_left);
            btMoveLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<MProduct> mProducts = new ArrayList<>();

                    for (int i = 0; i < listAdapterStock.getCount(); i++) {
                        MProduct mProduct = (MProduct) listAdapterStock.getItem(i);
                        if (mProduct.isSelect) {
                            mProducts.add(mProduct);
                        }

                    }


                    for (MProduct mProduct : mProducts) {
                        addToLack(mProduct);
                    }
                    activateLack();


                    for (int i = 0; i < listAdapterStock.getCount(); i++) {
                        MProduct mProduct = (MProduct) listAdapterStock.getItem(i);
                        mProduct.isSelect = false;
                        listAdapterStock.notifyDataSetChanged();
                    }
                    buttonDelete.setVisibility(View.VISIBLE);

                }
            });

            findViewById(R.id.bt_close_lack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.save();
                    finish();
                }
            });

            buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mSettings.getVisit().newOrderList.size() > 0) {
                        Utils.messageBox(getString(R.string.warning), getString(R.string.sasdh), NewOrderStockActivity.this, new IActionE() {
                            @Override
                            public void action(Object o) {
                                deleteOrder();

                            }
                        });
                    } else {
                        deleteOrder();
                    }


                }
            });
        } catch (SQLException ex) {
            String msg = Utils.getErrorTrace(ex);
            Utils.sendMessage("newOrder:" + msg,  (MyApplication) getApplication());
           log.error(ex);
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    private void deleteOrder() {
        for (MProduct P : mProductList) {
            P.isSelect = false;
        }
        for (MProduct mProduct : mSettings.getVisit().newOrderList) {
            mProduct.isSelect = false;
        }
        mSettings.getVisit().newOrderList.clear();
        Settings.save();
        activateLack();
        buttonDelete.setVisibility(View.GONE);
    }


    private void activateABC() {

        MyListViewAbc mListViewE = (MyListViewAbc) findViewById(R.id.list_abc_lack);
        mListViewE.setIAction(new IActionE<String>() {
            @Override
            public void action(String o) {
                showProducABC(o);
            }
        });
    }

    private void showProducABC(String str) {
        Linq.toStream(mProductList).forEach(new Action<MProduct>() {
            @Override
            public void action(MProduct mProduct) {
                mProduct.isSelect = false;
            }
        });
        List<Object> objects = new ArrayList<Object>(FactoryProduct.getListAbs(mProductList, str));
        activateListProduct(objects);

    }

    private void activateListProduct(List<Object> objects) {


        Parcelable index = null;
        try {
            index = productListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        listAdapterStock = new ListAdapterStock(this, R.layout.myrow_date_sale_product_fix, objects,
                new ArrayList<MProductBase>(mSettings.getVisit().newOrderList), true);//mSettings,
        listAdapterStock.showDataSelf2Stock = true;
        listAdapterStock.setShowBort(true);
        listAdapterStock.isNewOrder = true;
        listAdapterStock.setOnRowDataClick(NewOrderStockActivity.this);
        productListView.setAdapter(listAdapterStock);
        if (index != null) {
            productListView.onRestoreInstanceState(index);
        }
    }
    private void  activateListProduct(List<Object> objects,String strSearch){
        Parcelable index = null;
        try {
            index = productListView.onSaveInstanceState();
        } catch (Exception ignored) {
        }

        listAdapterStock = new ListAdapterStock(this, R.layout.myrow_date_sale_product_fix, objects,
                new ArrayList<MProductBase>(mSettings.getVisit().newOrderList), true,strSearch);//mSettings,
        listAdapterStock.showDataSelf2Stock = true;
        listAdapterStock.setShowBort(true);
        listAdapterStock.isNewOrder = true;
        listAdapterStock.setOnRowDataClick(NewOrderStockActivity.this);
        productListView.setAdapter(listAdapterStock);
        if (index != null) {
            productListView.onRestoreInstanceState(index);
        }
    }

    @Override
    public void onRowDataClick(MProductBase mProduct, View view, List<Object> objects) {

        mProduct.isSelect = !mProduct.isSelect;
        //CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox_d);
        CheckBox myCheck = (CheckBox) view.findViewById(R.id.checkbox_d);
        //checkBox.setChecked(mProduct.isSelect);
        myCheck.setChecked(mProduct.isSelect);
        //listAdapterStock.notifyAddin(mProduct.idu);

    }


    private List<Object> AddinData(List<Object> objects, MProductGroup mProductGroup) {

        List<MProduct> mProductList1 = FactoryProduct.getListForMenu(mProductList, mProductGroup.idu);
        int index = 0;
        for (Object object : objects) {
            index++;
            if (object instanceof MProductGroup) {
                MProductGroup sd = (MProductGroup) object;
                if (sd.idu.equals(mProductGroup.idu)) {
                    break;
                }
            }
        }
        objects.addAll(index, new ArrayList<Object>(mProductList1));
        return objects;
    }

    private void addToLack(final MProduct mProduct) {
        mProduct.isSelect = false;
        for (MProduct pp : mSettings.getVisit().newOrderList) {
            if (pp.idu.equals(mProduct.get_id())) {
                return;
            }
        }

        MProduct product = Configure.getSession().get(MProduct.class, mProduct.idu);





        if (product != null) {
            product.setAmountCore(10.0d);
            mSettings.getVisit().newOrderList.add(product);
            Settings.save();
        }
    }

    private void addToListProduct(final MProduct mProduct) {
        mProduct.isSelect = false;
        mProduct.setAmountCore(0);// = 0;

        MProduct product = Linq.toStream(mProductList).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
            @Override
            public boolean apply(MProduct t) {
                return t.idu.equals(mProduct.idu);
            }
        });

        if (product == null) {
            mProductList.add(mProduct);
        }
    }
}
