package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

//import com.example.user.omsk.models.MError;
import com.example.user.omsk.models.MPromotion;
import com.example.user.omsk.orm2.Configure;

import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;

// сохранние файлов рекламы на диск
public class BitmapCache {
    public static org.apache.log4j.Logger log = Logger.getLogger(BitmapCache.class);
    public static void cache(MPromotion mPromotion) {
        try {
            if (mPromotion.images == null) {
                File file = new File(Utils.getPromotionDirectory() + "/" + mPromotion.file_name);
                Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
                if (bitmap != null) {
                    mPromotion.images = getBytesFromBitmap(bitmap);
                    Configure.getSession().update(mPromotion);
                }
            }
        } catch (Exception ex) {
            log.error(ex);
           // Configure.getSession().insert(new MError());
        }
    }

    private static byte[] getBytesFromBitmap(Bitmap image) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }
}

