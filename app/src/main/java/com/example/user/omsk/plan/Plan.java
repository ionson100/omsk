package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Function;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import org.apache.log4j.Logger;

import java.util.List;


public class Plan extends AppCompatActivity {

    public static org.apache.log4j.Logger log = Logger.getLogger(Plan.class);
    public static final int RESULT_OK_PLAN = 34445;
    private LinearLayout mPanelHeader;
    private ListView mListView;
    private Settings mSettings;
    private List<TempPlan> mTempPlanList;
    private AppCompatActivity mActivity;
    private boolean mIsOld;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_plan);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mListView = (ListView) findViewById(R.id.list_plan);
            mSettings = Settings.core();
            mPanelHeader = (LinearLayout) findViewById(R.id.panel_header_plan);
            mActivity = this;
            activateButton();
            createListABC();
            init();
        } catch (Exception e) {
            log.error(e);
            String msg = "Plan:" + Utils.getErrorTrace(e);
            log.error(msg);
            Utils.sendMessage(msg,  (MyApplication) this.getApplication());
            this.finish();
        }

    }

    private void init() {
        if (mTempPlanList == null) {
            mTempPlanList = FactoryPlan.getTempPlanList(mIsOld);
        }
        FactoryPlan.getViewHeader(this, mPanelHeader, mIsOld);
        actionList(mTempPlanList);
    }


    private void activateButton() {
        findViewById(R.id.bt_plan_show_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionList(mTempPlanList);
            }
        });
        findViewById(R.id.bt_acb_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<TempPlan> list = Linq.toStream(mTempPlanList).where(new com.example.user.omsk.linq2.Predicate<TempPlan>() {
                    @Override
                    public boolean apply(TempPlan t) {
                        return t.getColorABK() == R.color.caede7;
                    }
                }).toList();

                actionList(list);
            }
        });
        findViewById(R.id.bt_acb_open).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<TempPlan> list = Linq.toStream(mTempPlanList).where(new com.example.user.omsk.linq2.Predicate<TempPlan>() {
                    @Override
                    public boolean apply(TempPlan t) {
                        return t.getColorABK() == R.color.d5caed;
                    }
                }).orderBy(new Function<TempPlan, Double>() {
                    public Double foo(TempPlan t) {
                        return t.mTotalRub;
                    }
                }).reverce().toList();

                actionList(list);

            }
        });

        findViewById(R.id.bt_mml_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                List<TempPlan> list = Linq.toStream(mTempPlanList).where(new com.example.user.omsk.linq2.Predicate<TempPlan>() {
                    @Override
                    public boolean apply(TempPlan t) {
                        return t.getGreenItem(mIsOld) > 0;
                    }
                }).toList();


                actionList(list);
            }
        });
        findViewById(R.id.bt_mml_open).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                List<TempPlan> list = Linq.toStream(mTempPlanList).where(new com.example.user.omsk.linq2.Predicate<TempPlan>() {
                    @Override
                    public boolean apply(TempPlan t) {
                        return t.getGreenItem(mIsOld) <= 0;
                    }
                }).toList();
                actionList(list);
            }
        });

        findViewById(R.id.bt_plan_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mIsOld = !mIsOld;
                ISession ses;
                ses = Configure.getSession();
                List<MTemplatePropety> mTemplatePropetyList = ses.getList(MTemplatePropety.class, null);
                if (mTemplatePropetyList.size() == 0) {
                    return;
                }
                MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
                int i = 0;
                if (mIsOld) {
                    if (mTemplatePropety.isSendPlanOld) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                } else {
                    if (mTemplatePropety.isSendPlan) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                }
                if (i == 0) {
                    if (mIsOld) {
                        new SenderPlanPointsProductOld().send(mSettings, mActivity, new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                mTempPlanList = null;
                                init();
                            }
                        });
                    } else {
                        new SenderPlanPointsProduct().send(mSettings, mActivity, new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                mTempPlanList = null;
                                init();
                            }
                        });
                    }

                } else {
                    mTempPlanList = null;
                    init();
                }
                txtureButton((Button) v);
            }
        });
    }

    private void txtureButton(Button v) {
        if (!mIsOld) {
            v.setText(R.string.old_plan);
        } else {
            v.setText(R.string.cur_plan);
        }
    }

    private void actionList(List<TempPlan> tempPlans) {
        TempPlan tempPlan = Linq.toStream(tempPlans).firstOrDefault(new com.example.user.omsk.linq2.Predicate<TempPlan>() {
            @Override
            public boolean apply(TempPlan t) {
                return t.mPoint == null;
            }
        });
        if (tempPlan == null) {
            tempPlans.add(new TempPlan());
        }
        ListAdapterForPlan listAdapterForPlan = new ListAdapterForPlan(this, R.layout.item_plan, tempPlans, mIsOld, this);
        mListView.setAdapter(listAdapterForPlan);
    }

    private void createListABC() {


        MyListViewAbc listView = (MyListViewAbc) findViewById(R.id.list_abc_plan);
        listView.setIAction(new IActionE<String>() {
            @Override
            public void action(final String o) {
                List<TempPlan> tempPlans = Linq.toStream(mTempPlanList).where(new com.example.user.omsk.linq2.Predicate<TempPlan>() {
                    @Override
                    public boolean apply(TempPlan t) {
                        return t.mPoint != null && t.mPoint.name.startsWith(o);
                    }
                }).toList();
                actionList(tempPlans);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        if (MyApplication.MVISIT != null && Settings.core().getVisit() != null) {
//            MyApplication.MVISIT.showButtonAction();
//        }
        this.finish();
    }

    public void resume() {
        onResume();
    }
}

