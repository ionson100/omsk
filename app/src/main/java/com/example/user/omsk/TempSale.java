package com.example.user.omsk;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public class TempSale {

    public String order_free;
    public final String product_id;
    public final double amountSafe;
    public final String name;
    public double amount;
    public final double price;

    public TempSale(String product_id, String name, double amount, double price, double amountSafe) {
        this.product_id = product_id;
        this.amountSafe = amountSafe;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }
}
