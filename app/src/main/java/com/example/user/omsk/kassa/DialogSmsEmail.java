package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.orm2.Configure;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DialogSmsEmail extends DialogFragment {

    private String mEmail;
    private String mSms;
    private Settings mSettings;
    private int mNotific;
    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private RadioButton radioButton3;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

      //  List<MProduct> mProductList = Configure.getSession().getList(MProduct.class, null);
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_sms_email_check, null);
        ((TextView) mView.findViewById(R.id.title_dalog)).setText(getString(R.string.jksdsdvgg));
        final EditText editTextSms = (EditText) mView.findViewById(R.id.edit_sms_check);


        editTextSms.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {

            private boolean backspacingFlag = false;
            private boolean editedFlag = false;
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursorComplement = s.length() - editTextSms.getSelectionStart();
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");

                if (!editedFlag) {

                    if (phone.length() >= 6 && !backspacingFlag) {

                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
                        editTextSms.setText(ans);
                        editTextSms.setSelection(editTextSms.getText().length() - cursorComplement);
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3);
                        editTextSms.setText(ans);
                        editTextSms.setSelection(editTextSms.getText().length() - cursorComplement);
                    }
                } else {
                    editedFlag = false;
                }
            }
        });
        mSettings = Settings.core();
        mEmail = mSettings.getVisit().point.email;
        mSms = mSettings.getVisit().point.sms;
        mNotific = mSettings.getVisit().notify;

        if (mSettings.getVisit().point.sms != null && mSettings.getVisit().point.sms.length() == 11) {

            String m = mSettings.getVisit().point.sms.substring(1);
            String ans = "(" + m.substring(0, 3) + ") " + m.substring(3, 6) + "-" + m.substring(6);
            editTextSms.setText(ans);
        }

        editTextSms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String dd = s.toString().replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
                mSettings.getVisit().point.sms = "8" + dd;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        final EditText editTextEmail = (EditText) mView.findViewById(R.id.edit_email_check);

        if (mSettings.getVisit().point.email != null) {
            editTextEmail.setText(mSettings.getVisit().point.email);
        }
        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mSettings.getVisit().point.email = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        radioButton1 = (RadioButton) mView.findViewById(R.id.rb1);
        radioButton2 = (RadioButton) mView.findViewById(R.id.rb2);
        radioButton3 = (RadioButton) mView.findViewById(R.id.rb3);


        switch (mSettings.getVisit().notify) {
            case 0: {
                radioButton1.setChecked(true);
                break;
            }
            case 1: {
                radioButton2.setChecked(true);
                break;
            }
            case 2: {
                radioButton3.setChecked(true);
                break;
            }
            default:{
                break;
            }
        }
        radioButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    mSettings.getVisit().notify = 0;
            }
        });
        radioButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                mSettings.getVisit().notify = 1;


            }
        });
        radioButton3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                mSettings.getVisit().notify = 2;


            }
        });


        mView.findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSettings.getVisit().notify == 1 && !validateNumber()) {
                    Toast.makeText(getActivity(), "Не верный номер телефона", Toast.LENGTH_SHORT).show();
                    radioButton1.setChecked(true);
                    radioButton2.setChecked(false);
                    radioButton3.setChecked(false);
                    return;
                }
                if (mSettings.getVisit().notify == 2 && !validateEmail()) {
                    Toast.makeText(getActivity(), "Не верный Email адрес", Toast.LENGTH_SHORT).show();
                    radioButton1.setChecked(true);
                    radioButton2.setChecked(false);
                    radioButton3.setChecked(false);
                    return;
                }

                boolean isPointUpdate = false;
                if (mSms == null && mSettings.getVisit().point.sms != null) {
                    isPointUpdate = true;
                }
                if (mSms != null && (!mSms.equals(mSettings.getVisit().point.sms))) {
                    isPointUpdate = true;
                }

                if (mEmail == null && mSettings.getVisit().point.email != null) {
                    isPointUpdate = true;
                }
                if (mEmail != null && (!mEmail.equals(mSettings.getVisit().point.email))) {
                    isPointUpdate = true;
                }
                if (isPointUpdate) {
                    if (mSettings.getVisit().point.id == 0) {
                        MPoint point = Configure.getSession().get(MPoint.class, mSettings.getVisit().point.idu);
                        if (point != null) {
                            mSettings.getVisit().point.id = point.id;
                        }
                    }
                    mSettings.getVisit().point.setNewObject(true);
                    Configure.getSession().update(mSettings.getVisit().point);
                    getActivity().sendBroadcast(new Intent(MainActivity.FAB));
                }

                Settings.save();
                dismiss();

            }
        });
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSettings.getVisit().point.email = mEmail;
                mSettings.getVisit().point.sms = mSms;
                mSettings.getVisit().notify = mNotific;
                Settings.save();
                dismiss();
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(mView);
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return builder.create();
    }

    boolean validateNumber() {
        return mSettings.getVisit().point.sms != null && mSettings.getVisit().point.sms.matches("\\d{11}");
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public boolean validateEmail() {
        if (mSettings.getVisit().point.email == null) {
            return false;
        }
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(mSettings.getVisit().point.email);
        return matcher.find();
    }
}
