package com.example.user.omsk;


import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.models.MAlertPoint;

import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.visitcontrol.VisitBalance;
import com.example.user.omsk.visitcontrol.VisitDebt;
import com.example.user.omsk.visitcontrol.VisitItogo;
import com.example.user.omsk.visitcontrol.VisitMarketing;
import com.example.user.omsk.visitcontrol.VisitSale;
import com.example.user.omsk.orm2.Configure;

import org.apache.log4j.Logger;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class FVisit2 extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FVisit2.class);
    private VisitMarketing visitMarketing;
    private VisitBalance visitBalance;
    private VisitSale visitSale;
    private VisitDebt visitDebt;
    private VisitItogo visitItogo;
    private Settings mSettings;

    public FVisit2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_fvisit2, container, false);
        try {

            mSettings = Settings.core();

            if (mSettings.visitFromError != null) {
                MVisit mVisit = mSettings.visitFromError;
                mSettings.visitFromError = null;
                mSettings.setVisit(mVisit);
            }
            if (mSettings.getVisit().order_type_id == null) {
                MOrderType mOrderType = Configure.getSession().getList(MOrderType.class, " index1 = 0").get(0);
                mSettings.getVisit().order_type_id = mOrderType.idu;
            }


            mSettings.getVisit().setStart(Utils.dateToInt(new Date()));

            if (mSettings.getVisit().latitude == 0 || mSettings.getVisit().longitude == 0) {

                ((MainActivity) getActivity()).STARTSERVICEGEO();

            }


            visitMarketing = (VisitMarketing) mView.findViewById(R.id.vist_marceting);
            visitMarketing.mainActivity = (MainActivity) getActivity();
            visitBalance = (VisitBalance) mView.findViewById(R.id.visit_balance);
            visitBalance.setActivity(getActivity());
            visitBalance.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    visitItogo.refrash();
                }
            };
            visitSale = (VisitSale) mView.findViewById(R.id.visit_sale);
            visitSale.mainActivity = (MainActivity) getActivity();
            visitSale.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    visitItogo.refrash();
                }
            };
            visitDebt = (VisitDebt) mView.findViewById(R.id.visit_debt);
            visitDebt.mainActivity = (MainActivity) getActivity();
            visitDebt.iActionE = new IActionE<Object>() {
                @Override
                public void action(Object o) {
                    visitItogo.refrash();
                }
            };
            visitItogo = (VisitItogo) mView.findViewById(R.id.visit_itogo);

            mView.findViewById(R.id.bt_action_commit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Settings mSettings = Settings.core();
                    if (mSettings.getVisit().selectProductForActions.size() > 0 || mSettings.getVisit().selectProductForPresent.size() > 0) {
                        if (mSettings.getVisit().member_action == null || mSettings.getVisit().member_action.trim().length() == 0) {
                            Utils.messageBox(getContext().getString(R.string.adad), getContext().getString(R.string.lksajdkajdsas), getActivity(), new IActionE() {
                                @Override
                                public void action(Object o) {
                                    return;
                                }
                            });
                        } else {
                            Settings.showFragment(StateSystem.VISIT_COMMIT, getActivity());
                        }
                    } else {
                        Settings.showFragment(StateSystem.VISIT_COMMIT, getActivity());
                    }
                }
            });


            final MAlertPoint alertPoint = mSettings.getVisit().point.getAlertPoint();
            if (alertPoint != null) {
                final LinearLayout panelAlert = (LinearLayout) mView.findViewById(R.id.panel_alert);
                panelAlert.setVisibility(View.VISIBLE);
                TextView textView = (TextView) mView.findViewById(R.id.alert_message);
                textView.setText(alertPoint.message);
                mView.findViewById(R.id.delete_alert).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.messageBox(getString(R.string.delete), getString(R.string.delete2), getActivity(), new IActionE() {
                            @Override
                            public void action(Object e) {
                                Configure.getSession().delete(alertPoint);
                                panelAlert.setVisibility(View.GONE);
                            }
                        });
                    }
                });
            }


            MakerPlanForVisitSale.make(mSettings.getVisit().point, getActivity(), mView);
//            String ss="Начала визита:    "+Utils.simpleDateFormatE(mSettings.getVisit().getStart())+"\n";
//            if(mSettings.getVisit().getFinish()!=null){
//                ss=ss+"Окончание визита: "+Utils.simpleDateFormatE(mSettings.getVisit().getFinish());
//            }
//            Toast.makeText(getActivity(), ss, Toast.LENGTH_SHORT).show();


        } catch (SQLException ex) {
            String msg = Utils.getErrorTrace(ex);
            Utils.sendMessage("FVisit2:" + msg,  (MyApplication) getActivity().getApplication());
           log.error(ex);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
        return mView;
    }

    public void mediaActivate() {
        visitMarketing.showPhoto();
    }
}
