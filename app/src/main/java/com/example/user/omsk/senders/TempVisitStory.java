package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.DebtDuplet;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MTriplet;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitStoryPoint;
import com.example.user.omsk.models.TempComonProduct;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TempVisitStory {

    public boolean status;
    public double refund;
    public String comment;
    public String result;
    public int date;
    public double payment;
    public String member_action;
    public String payment_type;
    public String trader;
    public List<MVisitPresent> gifts=new ArrayList<>();
    public List<TempComonProduct> products = new ArrayList<>();
    public List<TempComonProduct> free_products = new ArrayList<>();
    public List<TempComonProduct> promo_products = new ArrayList<>();
    public ArrayList<TempRefunds> refunds = new ArrayList<>();

    public static MVisitStoryPoint getMStoryVisitPoint(TempVisitStory d, ISession session) {
        MVisitStoryPoint f = new MVisitStoryPoint();
        f.status = d.status;
        f.refund = d.refund;
        f.comment = d.comment;
        f.visit_result_id = d.result;
        f.finish = d.date;
       // f.start = d.date;
        f.payment = d.payment;
        f.member_action = d.member_action;
        f.order_type_id = d.payment_type;
        f.trader = d.trader;
        for (TempComonProduct product : d.products) {
            MProduct p = session.get(MProduct.class, product.id);
            if (p == null) {
                p = new MProduct();
                p.name = "Не найден";
            }
            p.setAmountCore(product.amount);
            p.price = product.price;
            f.getSalesProductList().add(p);
        }
        for (TempComonProduct product : d.free_products) {
            MProduct p = Configure.getSession().get(MProduct.class, product.id);
            if (p == null) {
                p = new MProduct();
                p.name = "Не найден";
            }
            p.setAmountCore(product.amount);
            p.price = product.price;
            p.rule_id = product.promo;
            f.getFreeProductList().add(p);
        }
        for (TempComonProduct product : d.promo_products) {
            MProduct p = Configure.getSession().get(MProduct.class, product.id);
            if (p == null) {
                p = new MProduct();
                p.name = "Не найден";
            }
            p.setAmountCore(product.amount);
            p.price = product.price;
            p.rule_id = product.promo;
            f.getActionProductList().add(p);
        }
        f.setDupletCore(getDuplet(d.refunds));
        return f;
    }

    public static List<DebtDuplet> getDuplet(List<TempRefunds> list) {
        List<DebtDuplet> res = new ArrayList<>();

        Map<Date, List<MTriplet>> map = new HashMap<>();
        for (TempRefunds tempRefunds : list) {
            if (map.containsKey(tempRefunds.date)) {
                map.get(tempRefunds.date).add(new MTriplet(tempRefunds.product_id, tempRefunds.amount, tempRefunds.price));
            } else {
                List<MTriplet> d = new ArrayList<>();
                d.add(new MTriplet(tempRefunds.product_id, tempRefunds.amount, tempRefunds.price));
                map.put(tempRefunds.date, d);
            }
        }
        for (Map.Entry<Date, List<MTriplet>> ss : map.entrySet()) {
            List<MTriplet> d = ss.getValue();
            double amount = 0d;
            for (MTriplet triplet : d) {
                amount = amount + triplet.amount * triplet.price;
            }
            DebtDuplet debtDuplet = new DebtDuplet();
            debtDuplet.amount = amount;
            debtDuplet.date = ss.getKey();
            debtDuplet.tripletList = d;
            res.add(debtDuplet);
        }

        return res;
    }

    public static class TempRefunds {
        public Date date = null;
        public double amount;
        public double price;
        public String product_id;
    }
}
