package com.example.user.omsk.visitstoryworker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DialogVisitStoryAll extends DialogFragment {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DialogVisitStoryAll.class);

    private File[] files;
    private ListView mListView;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_visit_story, null);
        TextView textView = (TextView) v.findViewById(R.id.title_dalog);
        v.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mListView= (ListView) v.findViewById(R.id.list_visit);
        textView.setText("История торговых сессий");
        File file=new File(MyApplication.csvfile);
        if(file.getParentFile().exists()){
            files=file.getParentFile().listFiles();
            mListView.setAdapter(new ListAdapterVisit(getContext(),R.layout.item_visit_file,files));
        }


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getContext(),FVisitActivity.class);
                Bundle b = new Bundle();
                b.putString("file", files[position].getPath());
                intent.putExtras(b);
                getActivity().startActivity(intent);
                dismiss();
            }
        });
        builder.setView(v);
        return builder.create();
    }
}
 class ListAdapterVisit extends ArrayAdapter<File>{

     @NonNull
     private final Context context;
     private final int resource;
     @NonNull
     private final File[] objects;

     public ListAdapterVisit(@NonNull Context context, @LayoutRes int resource, @NonNull File[] objects) {
         super(context, resource, objects);
         this.context = context;
         this.resource = resource;
         this.objects = objects;
     }
     @NonNull
     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
         View mView = convertView;

         final File p = getItem(position);
         if (p != null) {

             mView = LayoutInflater.from(getContext()).inflate(resource, null);
             TextView textView= (TextView) mView.findViewById(R.id.session_date);
             String name=p.getName().replace("file_","").replace(".txt","");
             SimpleDateFormat formatter = new SimpleDateFormat(MyApplication.formatDateVisit);
             try {
                 Date date= formatter.parse(name);
                 textView.setText(Utils.simpleDateFormatE(date));
             } catch (ParseException e) {
                 e.printStackTrace();
             }
         }
         return mView;
     }
 }
