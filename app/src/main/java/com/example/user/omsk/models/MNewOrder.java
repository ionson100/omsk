package com.example.user.omsk.models;

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.UserField;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table("story_new_order")
public class MNewOrder {

    @PrimaryKey("id")
    public int id;


    @Column("point_id")
    public String point_id;

    @Column("date")
    public int date ;

    @UserField(IUserType = MyProductJsonField.class)
    @Column("ptoducts")
    public List<MProduct> products = new ArrayList<>();


}

