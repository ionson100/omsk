package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

import java.util.Date;

@Table("temp_plan")
public class TempProduct {

    @PrimaryKey("id")
    public int id;
    @Column("product_id")
    public String product_id;
    @Column("permit_id")
    public String permit_id;
    @Column("visit_id")
    public String visit_id;
    @Column("amount")
    public double amount;
    @Column("price")
    public double price;
    @Column("visit_date")
    public int visit_date ;

    @Column("point_id")
    public String point_id;
}

