package com.example.user.omsk.action;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.models.MAction;
import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;


public class SettingsStorageAction implements Serializable {

    public MAction mAction_new;

    public static SettingsStorageAction core() {
        return (SettingsStorageAction) Reanimator.get(SettingsStorageAction.class);
    }

    public static void save() {
        Reanimator.save(SettingsStorageAction.class);
    }
}
