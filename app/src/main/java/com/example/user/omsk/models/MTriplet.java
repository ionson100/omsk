package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import java.io.Serializable;

public class MTriplet implements Serializable {
    public MTriplet(String product_id, double amount, double price) {
        this.amount = amount;
        this.price = price;
        this.product_id = product_id;
    }

    public String product_id;
    public double amount;
    public double price;
}
