package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MProductGroup;

import java.util.HashSet;
import java.util.List;


public class ListAdapterStock extends ArrayAdapter<Object> {


    private  String strSearch;
    private boolean isConstructor;

    public String selectIdProduct;

    private Settings mSettings;
    private MPoint mPoint;
    private HashSet<String> mHashSetProduct;
    private boolean mIsUseKassa;
    private boolean isShowBort;

    public boolean showDataSelf2Stock;
    public boolean showDataSelfStock;

    public boolean isNewOrder;

    public void setShowBort(boolean show) {
        isShowBort = show;
    }

    public void setmIsUseKassa(boolean kassa) {
        this.mIsUseKassa = kassa;
    }


    public interface OnRowNavigateClick {
        void onRowNavigateClick(MProductGroup mProductGroup, List<Object> objects);
    }

    public interface OnRowDataClick {
        void onRowDataClick(MProductBase mProduct, View view, List<Object> objects);
    }

    private OnRowNavigateClick onRowNavigateClick;
    private OnRowDataClick onRowDataClick;

    public ListAdapterStock setOnRowNavigateClick(OnRowNavigateClick onRowNavigateClick) {
        this.onRowNavigateClick = onRowNavigateClick;
        return this;
    }

    public ListAdapterStock setOnRowDataClick(OnRowDataClick onRowDataClick) {
        this.onRowDataClick = onRowDataClick;
        return this;
    }

    public ListAdapterStock setmPoint(MPoint mPoint) {
        this.mPoint = mPoint;
        this.mHashSetProduct = Utils.getHashSetProduct(mPoint);
        return this;
    }


    private final int resource;
    private boolean isOrder = false;
    private final List<Object> objects;
    // private final Settings mSettings;
    private final List<MProductBase> selectList;


//    public List<MProduct> mProducts=new ArrayList<>();

    public ListAdapterStock(Context context, int resource, List<Object> objects, List<MProductBase> selectList, boolean isOrder) {// Settings settings,
        super(context, resource, objects);
        this.resource = resource;

        this.isOrder = isOrder;
        this.objects = objects;
        this.selectList = selectList;
        this.mSettings = Settings.core();
    }

    public ListAdapterStock(Context context, int resource, List<Object> objects, List<MProductBase> selectList, boolean isOrder,String strSearch) {// Settings settings,
        super(context, resource, objects);

        this.resource = resource;
        this.isOrder = isOrder;
        this.objects = objects;
        this.selectList = selectList;
        this.strSearch = strSearch;
        this.mSettings = Settings.core();
    }

    public ListAdapterStock(Context context, int resource, List<Object> objects, List<MProductBase> selectList) {
        super(context, resource, objects);
        this.resource = resource;

        this.isConstructor = true;
        this.objects = objects;


        this.selectList = selectList;
        this.mSettings = Settings.core();
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        Object p = getItem(position);

        if (mIsUseKassa) {
            convertView = activateData((MProductBase) p);

        } else {
            if (p instanceof MProductGroup) {
                convertView = activateMeny((MProductGroup) p);
            } else {
                convertView = activateData((MProduct) p);
            }
        }
        convertView.setTag(p);
        return convertView;
    }

//    private View activateSelfAmount(){
//        View mView = LayoutInflater.from(getContext()).inflate(R.layout.item_itogo_stock, null);
//        Pupper itogoName,itogoAmount,itogoPrice;
//        itogoName= (Pupper) mView.findViewById(R.id.itogo_name);
//        itogoAmount= (Pupper) mView.findViewById(R.id.itogo_count);
//        itogoPrice= (Pupper) mView.findViewById(R.id.itogo_price);
//        double countName =0,price=0,count=0;
//        for (MProduct mProduct : mProducts) {
//            countName = countName +1;
//            double d=mProduct.amountSelfCore();
//            count=count+d;
//            price=price+d*mProduct.price;
//        }
//        itogoName.setPairString("Итого названий:",String.valueOf(countName));
//        itogoAmount.setPairString("Итого пачек:",String.valueOf(count));
//        itogoPrice.setPairString("Итого на сумму:(руб.)",Utils.getStringDecimal(price));
//        return mView;
//
//    }

    private View activateData(final MProductBase p) {


        if (mIsUseKassa) {
            for (MProductBase product : selectList) {
                if (product.idu.equals(p.idu)) {
                    p.isSelect = true;
                    p.price = product.price;
                    p.setAmountCore(product.getAmount_core());
                }
            }
        }


        final View mView = LayoutInflater.from(getContext()).inflate(resource, null);


        float scale = getContext().getResources().getConfiguration().fontScale;
        LinearLayout mLinearLayout = (LinearLayout) mView.findViewById(R.id.panel_decorator);
        Utils.decorator(mLinearLayout, mSettings, scale);

        if (mIsUseKassa == false) {
            getSelectProduct(p);
        }


        ImageView imageView = (ImageView) mView.findViewById(R.id.image_312);

        TextView name = (TextView) mView.findViewById(R.id.name_d);
        TextView nameSku = (TextView) mView.findViewById(R.id.name_sku);

        if (mSettings.isShowSku) {
            nameSku.setText(p.sku);
            nameSku.setVisibility(View.VISIBLE);
        } else {
            nameSku.setVisibility(View.GONE);

        }


        name.setText(p.name);
        name.setTextColor(p.getColor());
        TextView amountSafe = (TextView) mView.findViewById(R.id.amount_safe_d);
        TextView amountSafe2 = (TextView) mView.findViewById(R.id.amount_safe_d2);


        if (mIsUseKassa) {
            amountSafe.setVisibility(View.GONE);
        } else {


            double dsafe = p.amountSelfCore();
            if (dsafe == 0d) {
                amountSafe.setText("");
            } else {
                amountSafe.setText(Utils.getStringDecimal(dsafe));//// TODO: 17.11.2016
            }
            amountSafe2.setText(Utils.getStringDecimal(p.amountSelf2));
        }

        if (isShowBort) {

        } else {
            amountSafe.setVisibility(View.VISIBLE);
            amountSafe2.setVisibility(View.GONE);
        }

        if (isNewOrder) {
            amountSafe.setVisibility(View.GONE);
        }


        TextView price = (TextView) mView.findViewById(R.id.price_d);
        price.setText(p.getPrice());
        TextView amountCore = (TextView) mView.findViewById(R.id.amount_d);


        amountCore.setText(p.getAmount());
        TextView itogo = (TextView) mView.findViewById(R.id.itogo_d);
        itogo.setText(p.getItogo());

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRowDataClick.onRowDataClick(p, mView, objects);
            }
        });


        CheckBox myCheck = (CheckBox) mView.findViewById(R.id.checkbox_d);


        int ss = mSettings.getStateSystem();
        if (ss == StateSystem.VISIT_SHOW_STOCK || ss == StateSystem.SHOW_STOCK_SIMPLE) {
            myCheck.setVisibility(View.GONE);
        }


        if (isNewOrder) {

            myCheck.setChecked(p.isSelect);
            myCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    p.isSelect = isChecked;
                }
            });

        } else {
            if (!p.isSelect) {
                if (p.getAmount_core() != 0) {
                    myCheck.setChecked(true);
                }
            } else {

                myCheck.setChecked(true);
            }
        }


        if (isOrder) {
            price.setPadding(0, 0, 10, 0);
            amountCore.setVisibility(View.GONE);
            itogo.setVisibility(View.GONE);
        }
        if (mSettings.isShowSellProduct && this.mPoint != null && this.mHashSetProduct.contains(p.sku)) {
            imageView.setVisibility(View.VISIBLE);
        }

        mView.findViewById(R.id.wrapper_product).setPadding(p.getPadding(), 0, 0, 0);


        if (Settings.getStateSystem() == StateSystem.SHOW_STOCK_SIMPLE) {
            name.setVisibility(View.VISIBLE);
            amountSafe.setVisibility(View.VISIBLE);
            amountSafe2.setVisibility(View.GONE);
            amountCore.setVisibility(View.GONE);
            itogo.setVisibility(View.GONE);
        }
        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE) {
            name.setVisibility(View.VISIBLE);
            amountSafe.setVisibility(View.VISIBLE);
            amountSafe2.setVisibility(View.GONE);
            amountCore.setVisibility(View.VISIBLE);
            itogo.setVisibility(View.VISIBLE);
        }
        if (Settings.getStateSystem() == StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE) {
            name.setVisibility(View.VISIBLE);
            amountSafe.setVisibility(View.VISIBLE);
            amountSafe2.setVisibility(View.GONE);
            amountCore.setVisibility(View.GONE);
            itogo.setVisibility(View.GONE);
        }
        if (Settings.getStateSystem() == StateSystem.VISIT_STOCK_PRESENT) {
            name.setVisibility(View.VISIBLE);
            amountSafe.setVisibility(View.GONE);
            amountSafe2.setVisibility(View.GONE);
            amountCore.setVisibility(View.GONE);
            itogo.setVisibility(View.GONE);
        }

        //int ddd = amountSafe2.getVisibility();
        //int dded = amountSafe.getVisibility();

        if (isConstructor) {
            amountSafe.setVisibility(View.GONE);
        }
        if(strSearch!=null&&strSearch.length()>0&&name.getText().length()>0){
            Utils.drawSearchText(name,strSearch);
        }
        return mView;
    }


    private void getSelectProduct(MProductBase product) {
        for (MProductBase mProduct : selectList) {
            if (mProduct.idu.equals(product.idu)) {
                product.setAmountCore(mProduct.getAmount_core());
            }
        }
    }


    private View activateMeny(final MProductGroup group) {

        View mView = LayoutInflater.from(getContext()).inflate(R.layout.myrow_navigate, null);

        LinearLayout zx = (LinearLayout) mView.findViewById(R.id.wrapper_product_croups);
        zx.setPadding(group.getPadding(), 0, 0, 0);

        ImageButton imageButton = (ImageButton) mView.findViewById(R.id.image_b);
        imageButton.setPadding(0, 0, 0, 0);

        if (!group.isSelect) {
            imageButton.setImageResource(R.drawable.ic_laucher_close);//close
        } else {
            imageButton.setImageResource(R.drawable.ic_launcher_open);//open
        }
        TextView textView = (TextView) mView.findViewById(R.id.name);
        textView.setPadding(20, 0, 0, 0);
        textView.setText(group.name);

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRowNavigateClick.onRowNavigateClick(group, objects);
            }
        });
        return mView;

    }
}
