package com.example.user.omsk.kassa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.atol.drivers.fptr.Fptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;


public class PrintConnectOfd {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PrintConnectOfd.class);
    public static void pint(final Activity activity) {



        final String[] eror = {null};
        new AsyncTask<Void, String, Void>() {
            private ProgressDialog dialog;
            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                //String str = "";
                fptr = new Fptr();
                try {

                    fptr.create(activity.getApplication());

                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    if (fptr.put_DeviceEnabled(true) < 0) {//Установка соединения...
                        checkError();
                    }

                    if (fptr.GetStatus() < 0) {//Проверка связи
                        checkError();
                    }
                    if (fptr.put_CommandBuffer("82 01 06 00") < 0) {
                        checkError();
                    }
                    if (fptr.put_NeedResultFlag(false) < 0) {
                        checkError();
                    }
                    if (fptr.put_TimeoutACK(500) < 0) {
                        checkError();
                    }
                    if (fptr.put_TimeoutENQ(5000) < 0) {
                        checkError();
                    }
                    if (fptr.RunCommand() < 0) {
                        checkError();
                    }
                    //str = fptr.get_AnswerBuffer();
                } catch (Exception e) {

                    log.error(e);
                    eror[0] = e.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), Settings.core().kkmNumber + "\n" +
                            "Ошибка ккм file  - PrintConnectOfd\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {

                dialog = Utils.factoryDialog(activity, "Печать теста ОФД", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                if (eror[0] != null) {
                    Utils.messageBox(activity.getString(R.string.error), eror[0], activity, null);
                }
            }
        }.execute();
    }
}
