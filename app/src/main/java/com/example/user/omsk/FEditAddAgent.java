package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.user.omsk.models.MAgent;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FEditAddAgent extends Fragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FEditAddAgent.class);
    private List<MAgent> mAgents;
    private Settings mSettings;
    private View mView;

    public FEditAddAgent() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_edit_add_agent, container, false);
        mSettings = Settings.core();
        try {
            if (mSettings.getVisit() == null) {
                Settings.showFragment(StateSystem.HOME, getActivity());
                return mView;
            }
            if (mSettings.getVisit().point.getContragent(Configure.getSession()) == null) {
                mSettings.getVisit().point.setAgent(new MAgent());
            }

            Button ok = (Button) mView.findViewById(R.id.btOk_a);

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSettings.getVisit().point.getContragent(Configure.getSession()).name == null || mSettings.getVisit().point.getContragent(Configure.getSession()).name.trim().length() == 0) {
                        ((EditText) mView.findViewById(R.id.ea_name)).setError(getString(R.string.field_name_null));
                        return;
                    }
                    if (!ValidateINN.isValidINN(mSettings.getVisit().point.getContragent(Configure.getSession()).inn)) {
                        ((EditText) mView.findViewById(R.id.ea_inn)).setError(getString(R.string.inn_not_valid));
                        mView.findViewById(R.id.ea_inn).requestFocus();
                        return;
                    }
                    ISession ses = Configure.getSession();
                    ses.beginTransaction();
                    try {
                        mSettings.getVisit().point.setNewObject(true);
                        if (Settings.getStateSystem() == StateSystem.CONTRAGENT_ADD || Settings.getStateSystem() == StateSystem.CONTRAGENT_ADD_SEARCH) {
                            mSettings.getVisit().point.agent_id = mSettings.getVisit().point.getContragent(Configure.getSession()).idu;
                            ///////////////////////////////aad
                            mSettings.getVisit().point.getContragent(Configure.getSession()).name = mSettings.getVisit().point.getContragent(Configure.getSession()).name.trim();
                            /////////////////////////////////////
                            if (mSettings.getVisit().point.getContragent(Configure.getSession()).id == 0) {
                                ses.insert(mSettings.getVisit().point.getContragent(Configure.getSession()));
                            } else {
                                ses.update(mSettings.getVisit().point.getContragent(Configure.getSession()));
                            }

                        } else {
                            mSettings.getVisit().point.agent_id = mSettings.getVisit().point.getContragent(Configure.getSession()).idu;
                            ses.update(mSettings.getVisit().point.getContragent(Configure.getSession()));
                            ses.update(mSettings.getVisit().point);
                        }

                        ses.update(mSettings.getVisit().point);
                        ses.commitTransaction();
                    } finally {
                        ses.endTransaction();
                    }

                    Settings.save();
                    if (Settings.getStateSystem() == StateSystem.CONTRAGENT_ADD || Settings.getStateSystem() == StateSystem.CONTRAGENT_EDIT) {
                        Settings.showFragment(StateSystem.POINT, getActivity());
                    } else {
                        Settings.showFragment(StateSystem.SEARCH_POINT, getActivity());
                    }
                    getActivity().sendBroadcast(new Intent(MainActivity.FAB));

                }
            });

            activateSpinner();
            bind();


        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FEditAddAgent:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            Settings.showFragment(StateSystem.HOME, getActivity());
        }
        return mView;
    }

    private void activateSpinner() {
        mAgents = Configure.getSession().getList(MAgent.class, null);

        Collections.sort(mAgents, new Comparator<MAgent>() {
            @Override
            public int compare(MAgent lhs, MAgent rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        if (mSettings.getVisit().point.getContragent(Configure.getSession()).id == 0) {
            mSettings.getVisit().point.getContragent(Configure.getSession()).name = getString(R.string.new_contragent);
            mAgents.add(0, mSettings.getVisit().point.getContragent(Configure.getSession()));
        }
        List<String> strings = new ArrayList<>();
        for (MAgent mContragent : mAgents) {
            strings.add(mContragent.name);
        }
        IconSpinnerAdapter mSpinerArrayAdapter = new IconSpinnerAdapter(getActivity().getBaseContext(), strings, R.drawable.ic_contragent);
        Spinner spinner = (Spinner) mView.findViewById(R.id.spinner_agent);
        spinner.setAdapter(mSpinerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                MAgent sd = mAgents.get(position);
                mSettings.getVisit().point.setAgent(sd);
                bind();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        if (mSettings.getVisit().point.getContragent(Configure.getSession()).id != 0) {
            spinner.setSelection(getPositionSinner());
        }
    }

    private int getPositionSinner() {
        int res = 0;
        for (int i = 0; i < mAgents.size(); i++) {
            MAgent ma = mAgents.get(i);
            String idu1 = ma.idu;
            String idu2 = mSettings.getVisit().point.getContragent(Configure.getSession()).idu;
            if (idu1.equals(idu2)) {
                res = i;
                break;
            }
        }
        return res;
    }


    private void bind() {

        TextView tv1 = (TextView) mView.findViewById(R.id.ea_name);
        TextView tv2 = (TextView) mView.findViewById(R.id.ea_comentary);
        TextView tv3 = (TextView) mView.findViewById(R.id.ea_email);
        TextView tv4 = (TextView) mView.findViewById(R.id.ea_telephone);
        TextView tv5 = (TextView) mView.findViewById(R.id.ea_inn);

        tv1.setText(mSettings.getVisit().point.getContragent(Configure.getSession()).name);
        tv2.setText(mSettings.getVisit().point.getContragent(Configure.getSession()).commentary);
        tv3.setText(mSettings.getVisit().point.getContragent(Configure.getSession()).email);
        tv4.setText(mSettings.getVisit().point.getContragent(Configure.getSession()).telephone);
        tv5.setText(mSettings.getVisit().point.getContragent(Configure.getSession()).inn);

        tv1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().point.getContragent(Configure.getSession()).name = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tv2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().point.getContragent(Configure.getSession()).commentary = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        tv3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().point.getContragent(Configure.getSession()).email = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tv4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().point.getContragent(Configure.getSession()).telephone = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tv5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (ValidateINN.isValidINN(s.toString())) {
                    mSettings.getVisit().point.getContragent(Configure.getSession()).inn = s.toString();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}
