package com.example.user.omsk.yandex;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

/**
 * ion100 on 14.09.2017.
 */

public interface LbsLocationListener {
    public void onLocationChange(LbsInfo lbsInfo);

}

