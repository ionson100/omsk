package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.UserField;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// базовый класс для хранения данных по  визиту
public abstract class MVisitStoryBase {


    @Column("check_data")
    public String data11;
    ///////////////////////////////////////
    private List<MChecksData> checks;

    public List<MChecksData> getCheckList() {
        if (checks == null) {
            if (data11 == null) {
                checks = new ArrayList<>();
            } else {
                Gson gson = Utils.getGson();
                checks = gson.fromJson(data11, new TypeToken<List<MChecksData>>() {
                }.getType());
            }
        }
        return checks;
    }

    public void setRefrachCheck() {
        data11 = Utils.getGson().toJson(this.getCheckList());
    }

    public void setCheck(List<MChecksData> checks) {
        this.checks = checks;
    }

    /////////////////////////////////////


//    @UserField(IUserType = MyCheckListField.class)


    @Column("notific")
    public int notify;

    @Column("stateLocation")
    public int stateLocation;

    @Column("distance")
    public int distance;

    @Column("pressure")
    public int pressure;

    @Column("status")
    public boolean status;


    // только для истории
    public transient String trader;
    // только для истории
    public transient String point_name;

    @Column("member_action")
    public String member_action;

    @Column("concurent_actions")
    public String concurent_actions;

    @PrimaryKey("id")
    public int id;

    @Column("idu")
    public String idu;

    @Column("refund")
    public double refund;

    @Column("uuid")
    public String uuid;

    @Column("payment")
    public double payment = -1;

    @Column("point_id")
    public String point_id;

    @Column("connterpirty_id")
    public String connterpirty_id;

    @Column("start")
    public int start ;

    @Column("finish")
    public int finish;

    @Column("latitude")
    public double latitude;

    @Column("longitude")
    public double longitude;

    @Column("visit_result_id")
    public String visit_result_id;

    @Column("comment")
    public String comment;

    @Column("order_type")
    public String order_type_id;

    @Column("isRecomendationBalance")
    public boolean isRecomendationBalance;

    @Column("isRecomendationSale")
    public boolean isRecomendationSale;

    @Column("state_object")
    public transient int stateObject = 0;

    //@Column("date")

    //public transient int date ;


    @Column("sales_product")
    public String ddd;


    //////////////////////////////////////////////////////////


    private List<MProduct> sale, stock, free, action, neworder;
    private List<DebtDuplet> duplet;

    public List<MProduct> getSalesProductList() {
        if (sale == null) {
            if (ddd == null) {
                sale = new ArrayList<>();
            } else {
                Gson gson = Utils.getGson();
                sale = gson.fromJson(ddd, new TypeToken<List<MProduct>>() {
                }.getType());
            }
        }
        return sale;
    }

    public void setSalecProductCore(List<MProduct> list) {
        sale = list;
    }

    public void setSalecProduct() {
        ddd = Utils.getGson().toJson(this.getSalesProductList());
    }

    //////////////////////////////////

    @Column("stock_product")
    public String ddd1;

    public List<MProduct> getBalanceProductList() {
        if (stock == null) {
            if (ddd1 == null) {
                stock = new ArrayList<>();
            } else {
                Gson gson = Utils.getGson();
                stock = gson.fromJson(ddd1, new TypeToken<List<MProduct>>() {
                }.getType());
            }
        }
        return stock;
    }

    public void setBalanceProductCore(List<MProduct> list) {
        stock = list;
    }

    public void setBalanceProduct() {
        ddd1 = Utils.getGson().toJson(this.getBalanceProductList());
    }
    //////////////////////////////////////

    //////////////////////////////////

    @Column("free_product")
    public String ddd2;

    public List<MProduct> getFreeProductList() {
        if (free == null) {
            if (ddd2 == null) {
                free = new ArrayList<>();
            } else {
                Gson gson = Utils.getGson();
                free = gson.fromJson(ddd2, new TypeToken<List<MProduct>>() {
                }.getType());
            }
        }
        return free;
    }

    public void setFreeProductCore(List<MProduct> list) {
        free = list;
    }

    public void setFreeProduct() {
        ddd2 = Utils.getGson().toJson(this.getFreeProductList());
    }
    //////////////////////////////////////

    //////////////////////////////////

    @Column("debt_duplet")
    public String ddd3;

    public List<DebtDuplet> getDupletProductList() {
        if (duplet == null) {
            if (ddd3 == null) {
                duplet = new ArrayList<>();
            } else {
                Gson gson = Utils.getGson();
                duplet = gson.fromJson(ddd3, new TypeToken<List<DebtDuplet>>() {
                }.getType());
            }
        }
        return duplet;
    }

    public void setDupletCore(List<DebtDuplet> list) {
        duplet = list;
    }

    public void setDuplet() {
        ddd3 = Utils.getGson().toJson(this.getDupletProductList());
    }
    //////////////////////////////////////


    @UserField(IUserType = MyStringJsonField.class)
    @Column("debt_list")
    public List<String> debtList = new ArrayList<>();


    //////////////////////////////////
    @Column("action_product")
    public String ddd4;

    public List<MProduct> getActionProductList() {
        if (action == null) {
            if (ddd4 == null) {
                action = new ArrayList<>();
            } else {
                Gson gson = Utils.getGson();
                action = gson.fromJson(ddd4, new TypeToken<List<MProduct>>() {
                }.getType());
            }
        }
        return action;
    }

    public void setActionProductCore(List<MProduct> list) {
        action = list;
    }

    public void setActionProduct() {
        ddd4 = Utils.getGson().toJson(this.getActionProductList());
    }

    //////////////////////////////////////


    //////////////////////////////////
    @Column("new_oreder")
    public String ddd5;

    public List<MProduct> getNewOrderProductList() {
        if (neworder == null) {
            if (ddd5 == null) {
                neworder = new ArrayList<>();
            } else {
                Gson gson = Utils.getGson();
                neworder = gson.fromJson(ddd5, new TypeToken<List<MProduct>>() {
                }.getType());
            }
        }
        return neworder;
    }

    public void setNewOrderProductCore(List<MProduct> list) {
        neworder = list;
    }

    public void setNewOrderProduct() {
        ddd5 = Utils.getGson().toJson(this.getNewOrderProductList());
    }

    //////////////////////////////////////


    //  @SerializedName("tasks")
    public List<MStoryTaskPoint> task_round = new ArrayList<>();

    public MPoint getPoint() {
        MPoint p = Configure.getSession().get(MPoint.class, point_id);
        if (p == null) {
            p = new MPoint();
            p.name = "точка не найдена на планшете";
        }
        return p;
    }

    public MOrderType getOrederType() {
        return Configure.getSession().get(MOrderType.class, order_type_id);
    }

    public MVisit getMVisit(Context context) {

        MVisit res = new MVisit();
        res.checksDataList = this.getCheckList();
        res.point = Configure.getSession().get(MPoint.class, point_id);
        if (res.point == null) {
            res.point = new MPoint();
            res.point.name = "Точка не найдена";
        }

        res.selectPresents = Configure.getSession().getList(MVisitPresent.class, " visit_id = ? ", this.idu);

        if(this.gifts!=null){
            res.selectPresents = this.gifts;
        }



        res.notify = this.notify;
        res.isFirstVisit = this.id == 0;
        res.stateLocation = this.stateLocation;
        res.member_action = this.member_action;
        res.concurent_actions = this.concurent_actions;
        res.point.isRecomendationSale = this.isRecomendationSale;
        res.point.isRecomendationBalance = this.isRecomendationBalance;
        res.point.preparePoindForSender(Configure.getSession());
        res.comment = this.comment;
        res.latitude = this.latitude;
        res.longitude = this.longitude;
        res.payment = this.payment;
        res.order_type_id = this.order_type_id;
        res.member_action = this.member_action;
        res.selectProductForBalance = this.getBalanceProductList();
        res.selectProductForSale = this.getSalesProductList();
        res.selectProductForPresent = this.getFreeProductList();
        res.selectProductForActions = this.getActionProductList();
        res.newOrderList = this.getNewOrderProductList();
        res.debtList = this.debtList;
        res.distance = this.distance;
        for (MStoryTaskPoint storyTaskPoint : task_round) {
            MStoryTaskPoint taskPoint = new MStoryTaskPoint();
            taskPoint.id = storyTaskPoint.id;
            taskPoint.message = storyTaskPoint.message;
            taskPoint.result = storyTaskPoint.result;
            taskPoint.point_id = storyTaskPoint.point_id;
            taskPoint.task_id = storyTaskPoint.task_id;
            taskPoint.visit_id = storyTaskPoint.visit_id;
            res.storyTaskPoints.add(taskPoint);
        }

        res.setStart(this.start);
        res.visit_result_id = this.visit_result_id;
        res.id = this.id;

        res.idu = this.idu;
        res.pressure = this.pressure;
        res.refund = this.refund;
        res.setFinish(this.finish);
        //////////////////////////////////
        List<MPhoto> mPhotos = Configure.getSession().getList(MPhoto.class, " visit_id = ? ", res.idu);
        res.listPhotoPath = new ArrayList<>();
        for (MPhoto mPhoto : mPhotos) {
            File f = new File(mPhoto.path);
            if (f.exists()) {
                res.listPhotoPath.add(mPhoto.path);
            }
        }
        ///////////////////////////
        List<MVideo> mVideoList = Configure.getSession().getList(MVideo.class, " visit_id = ? ", res.idu);
        res.listVideoPath = new ArrayList<>();
        for (MVideo mVideo : mVideoList) {
            File f = new File(mVideo.path);
            if (f.exists()) {
                res.listVideoPath.add(mVideo.path);
            }
        }
        return res;
    }

    public List<MVisitPresent> gifts;// =new ArrayList<>();

    public List<MVisitPresent> getPresnt() {
        if (gifts == null) {
            if (this.finish == 0) {
                gifts = Configure.getSession().getList(MVisitPresent.class, " visit_id = ? ", this.idu);

            } else {
                gifts = Configure.getSession().getList(MVisitPresent.class, " visit_id = ? or  visit_finish = ? ", this.idu, this.finish);

            }

        }
        return gifts;
    }
}
