package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.action.FVisitAction;
import com.example.user.omsk.action.FVisitActionTypeShow;

import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.List;

//  тут происходит магия смены фрагментов страницы, и мена или добавления левого выдижного меню.
public class FactoryFragment implements CompoundButton.OnCheckedChangeListener {

    private Settings mSettings;
    static int out = 0;
    private android.support.v4.app.FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    static void visibyliteMenu(int id_item, NavigationView navigationView) {
        MenuItem item = navigationView.getMenu().findItem(id_item);
        if (item != null) {
            item.setVisible(false);
        }
    }

    void ActionMap(Settings settings, AppCompatActivity context, MapView mapView) {
        if (mapView != null) {

            Transceiver.send(TransceiveStaticVariables.MAP, null);
            mapView.getTileProvider().clearTileCache();
            mapView = null;
            System.gc();
        }
        Action(settings, context);
    }


    public void Action(final Settings settings, final AppCompatActivity activity) {
        mSettings = Settings.core();
        out = 0;
        if (Settings.getStateSystem() == StateSystem.HOME) {
            settings.setVisit(null);

        }

        Utils.layoutPanel.removeAllViews();// удаление всех предыдущих фрагментов
        mFragmentManager = activity.getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        removeFragment();

        NavigationView navigationView = ((MainActivity) activity).NAVIGATE_MENU();

        FactoryMenu d = new FactoryMenu(navigationView, activity);
        d.activateHome();
        d.setMenuAction((FactoryMenu.IMenuAction) activity);



        ///MyApplication.MVISIT = null;

        switch (Settings.getStateSystem()) {

            case StateSystem.SHOW_POINT_SELECT_POINT_OR_AGENT: {
                showPoint(mFragmentTransaction);
                break;
            }
            case StateSystem.HOME: {

                ((MainActivity) activity).STOPSERVICEGEO();
                showHome(mFragmentTransaction);

                break;
            }
            case StateSystem.SETTINGS: {

                showSettings(mFragmentTransaction);
                break;
            }
            case StateSystem.CHAT: {
                showChat(mFragmentTransaction);
                break;
            }
            case StateSystem.POINT: {
                showPoint(mFragmentTransaction);
                break;
            }
            case StateSystem.SIMPLE_MAP: {

                showMap(mFragmentTransaction);

                break;
            }
            case StateSystem.POINT_ADD: {

                showEditPoint(mFragmentTransaction);

                break;
            }
            case StateSystem.POINT_EDIT: {
                showEditPoint(mFragmentTransaction);
                break;
            }
            case StateSystem.CONTRAGENT_ADD: {
                showEditContrAgent(mFragmentTransaction);

                break;
            }
            case StateSystem.CONTRAGENT_EDIT: {
                showEditContrAgent(mFragmentTransaction);
                break;
            }
            case StateSystem.EDIT_MAP: {
                showMap(mFragmentTransaction);
                break;
            }
            case StateSystem.SHOW_MAP: {
                showMap(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_SHOW_MAP_FROM_POINT: {
                showMap(mFragmentTransaction);
                break;
            }

            case StateSystem.EDIT_MAP_SETTINGS: {
                showMap(mFragmentTransaction);
                break;
            }
            case StateSystem.SHOW_STOCK_SIMPLE: {
                showStock(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE: {
                showStock(mFragmentTransaction);
                break;
            }

            case StateSystem.VISIT_SALE: {
                showRequest(mFragmentTransaction);
                break;
            }
            case StateSystem.ERROR: {
                showError(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_ERROR: {
                showError(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_HOME: {
                showAction(mFragmentTransaction, activity);
                break;
            }
            case StateSystem.VISIT_START: {
                showStartAction(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_BALANCE: {
                showBalanceAction(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_COMMIT: {
                showCommitAction(mFragmentTransaction);
                break;
            }
            case StateSystem.CONTACT_ADD: {
                showContact(mFragmentTransaction);
                break;
            }
            case StateSystem.CONTACT_EDIT: {
                showContact(mFragmentTransaction);
                break;
            }

            case StateSystem.VISIT_SHOW_CHAT: {
                showChat(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_SHOW_MAP: {
                showMap(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_SHOW_SETTINGS: {
                showSettings(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_SHOW_POINT: {
                showPoint(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_SHOW_STOCK: {
                showStock(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_STOCK_ACTION: {
                showStock(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE: {
                showStock(mFragmentTransaction);
                break;
            }
            case StateSystem.SEARCH_POINT: {
                showSearchContrAgent(mFragmentTransaction);
                break;
            }
            case StateSystem.POINT_EDIT_SEARCH: {
                showEditPoint(mFragmentTransaction);
                break;
            }
            case StateSystem.POINT_ADD_SEARCH: {
                showEditPoint(mFragmentTransaction);
                break;
            }
            case StateSystem.CONTRAGENT_EDIT_SEARCH: {
                showEditContrAgent(mFragmentTransaction);
                break;
            }
            case StateSystem.CONTRAGENT_ADD_SEARCH: {
                showEditContrAgent(mFragmentTransaction);
                break;
            }
            case StateSystem.ADVERTISING: {
                showAdvertising(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_ADVERTISING: {
                showAdvertising(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_REFUND: {
                showRefund(mFragmentTransaction);
                break;
            }
            case StateSystem.POINT_STORY: {
                showPointStory(mFragmentTransaction);
                break;
            }
            case StateSystem.POINT_STORY_FOR_PREVIEW: {
                showPointStory(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_POINT_STORY_FOR: {
                showPointStory(mFragmentTransaction);
                break;
            }
            case StateSystem.ASS_POINT: {
                showAssPint(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_ASS_POINT: {
                showAssPint(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_PRESENT: {
                showPresent(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_STOCK_PRESENT: {
                showStock(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_PRESENT_TYPES_SHOW: {
                showPresentTypesShow(mFragmentTransaction);
                break;
            }
            case StateSystem.PRESENT_TYPES_SHOW: {
                showPresentTypesShow(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_ACYIONS_TYPES_SHOW: {
                showTypeAction(mFragmentTransaction);
                break;
            }
            case StateSystem.ACYIONS_TYPES_SHOW: {
                showTypeAction(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_ACTION: {
                showActionCore(mFragmentTransaction);
                break;
            }
            case StateSystem.MAP_ROUTE: {
                showMap(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_MAP_ROUTE: {
                showMap(mFragmentTransaction);
                break;
            }

            case StateSystem.MAP_ROUTE_STORY: {
                showMapStory(mFragmentTransaction);
                break;
            }

            case StateSystem.MAP_ROUTE_STORY_ASS: {
                showMapStory(mFragmentTransaction);
                break;
            }
            case StateSystem.VISIT_MAP_ROUTE_STORY_ASS: {
                showMapStory(mFragmentTransaction);
                break;
            }
            default: {
                Toast.makeText(activity, activity.getString(R.string.error_orientation_state) + Settings.getStateSystem(), Toast.LENGTH_LONG).show();
                Settings.showFragment(StateSystem.HOME, activity);
            }
        }
    }

    private void showMapStory(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FMapStory(), "acddtidoncore");
        mFragmentTransaction.commit();
    }

    private void showActionCore(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FVisitAction(), "actionddcore");
        mFragmentTransaction.commit();
    }

    private void showTypeAction(FragmentTransaction mFragmentTransaction) {

        mFragmentTransaction.add(R.id.head_panel, new FVisitActionTypeShow(), "actiontypeshow");
        mFragmentTransaction.commit();
    }

    private void showPresentTypesShow(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FFreePresentTypesShow(), "present");
        mFragmentTransaction.commit();
    }

    private void showPresent(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FVisitPresent(), "present");
        mFragmentTransaction.commit();
    }

    private void showAssPint(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FAss(), "pointAss");
        mFragmentTransaction.commit();
    }

    private void showPointStory(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FPointStory(), "pointStory");
        mFragmentTransaction.commit();
    }

    private void showRefund(FragmentTransaction mFragmentTransaction) {

        mFragmentTransaction.add(R.id.head_panel, new FVisitRefund(), "refund");
        mFragmentTransaction.commit();
    }


    private void showAdvertising(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FAdvertising(), "advertising");
        mFragmentTransaction.commit();
    }

    private void showSearchContrAgent(FragmentTransaction mFragmentTransaction) {
        mFragmentTransaction.add(R.id.head_panel, new FSearchPoint(), "searchcontragent");
        mFragmentTransaction.commit();
    }


    private void showContact(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FEditAddContact(), "contact");
        fragmentTransaction.commit();
    }

    private void showCommitAction(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FVisitCommit(), "commitaction");
        fragmentTransaction.commit();
    }

    private void showBalanceAction(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FVisitBalance(), "balanceaction");
        fragmentTransaction.commit();
    }

    private void showStartAction(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FVisitStart(), "");
        fragmentTransaction.commit();
    }

    private void showAction(FragmentTransaction fragmentTransaction, Activity activity) {
        if (mSettings.getVisit() != null && mSettings.getVisit().point != null) {

            if (mSettings.useVisit2) {
                fragmentTransaction.add(R.id.head_panel, new FVisit2(), "visit2");
            } else {
                fragmentTransaction.add(R.id.head_panel, new FVisit(), "visit");
            }
            fragmentTransaction.commit();

        } else {
            new SenderErrorPOST().send((MyApplication) activity.getApplication(), activity.getString(R.string.kajsasas));
            Settings.showFragment(StateSystem.HOME, activity);
        }
    }


    private void showError(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FError(), "story");
        fragmentTransaction.commit();
    }

    private void showRequest(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FVisitSale(), "request");
        fragmentTransaction.commit();
    }

    private void showEditContrAgent(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FEditAddAgent(), "editagent");
        fragmentTransaction.commit();
    }

    private void showEditPoint(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FEditAddPoint(), "editpoint");
        fragmentTransaction.commit();
    }

    private void showStock(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FStock(), "stock");
        fragmentTransaction.commit();
    }

    private void showMap(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FMap(), "map");
        fragmentTransaction.commit();
    }

    private void showChat(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FChat(), "comchat");
        fragmentTransaction.commit();
    }

    private void showHome(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FHome(), "home");
        fragmentTransaction.commit();
    }

    private void showSettings(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FSetting(), "settings");
        fragmentTransaction.commit();
    }


    private void removeFragment() {

        List<android.support.v4.app.Fragment> df = mFragmentManager.getFragments();
        if (df != null && df.size() > 0) {
            List<android.support.v4.app.Fragment> deleteFr = new ArrayList<>();
            for (android.support.v4.app.Fragment fragment : df) {
                if (fragment != null) {
                    deleteFr.add(fragment);
                }
            }
            for (Fragment fragment : deleteFr) {
                mFragmentManager.beginTransaction().remove(fragment).commit();
            }
        }
    }

    private void showPoint(FragmentTransaction fragmentTransaction) {
        fragmentTransaction.add(R.id.head_panel, new FPoint(), "point");
        fragmentTransaction.commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//        if (!isChecked) return;
//        if (buttonView.getId() == R.id.nav_search_point_name) {
//            mSettings.setStateSearchPoint(StateSearchPoint.name);
//        }
//        if (buttonView.getId() == R.id.nav_search_point_addres) {
//            mSettings.setStateSearchPoint(StateSearchPoint.address);
//        }
//        if (buttonView.getId() == R.id.nav_search_point_contagent) {
//            mSettings.setStateSearchPoint(StateSearchPoint.conragent);
//        }
//        List<CheckBox> checkBoxes = (List<CheckBox>) buttonView.getTag();
//        checkBoxes.get(0).setChecked(mSettings.getStateSearchPoint() == StateSearchPoint.name);
//        checkBoxes.get(1).setChecked(mSettings.getStateSearchPoint() == StateSearchPoint.address);
//        checkBoxes.get(2).setChecked(mSettings.getStateSearchPoint() == StateSearchPoint.conragent);
    }
}
