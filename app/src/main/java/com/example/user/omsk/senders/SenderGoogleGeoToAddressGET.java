package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.example.user.omsk.IActionE;

import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class SenderGoogleGeoToAddressGET {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderGoogleGeoToAddressGET.class);
    private String formatAddress = "";
    private double latitude;
    private double longitude;
    private SenderGoogleAddressToGeoGET.IActionAddressGeo iAction;
    private Activity activity;

    public static String parseAddress(List<SenderGoogleAddressToGeoGET.Pokemon> address_components) {
        StringBuilder sb = new StringBuilder();
        SenderGoogleAddressToGeoGET.Pokemon picemon = null;
        picemon = getRealPicemon(address_components, "postal_code");
        if (picemon != null) {
            sb.append(picemon.short_name);
            sb.append(", ");
        }
        picemon = getRealPicemon(address_components, "country");
        if (picemon != null) {
            sb.append(picemon.long_name);
            sb.append(", ");
        }
        picemon = getRealPicemon(address_components, "administrative_area_level_1");
        if (picemon != null) {
            sb.append(picemon.short_name);
            sb.append(", ");
        }
        picemon = getRealPicemon(address_components, "locality");
        if (picemon != null) {
            sb.append(picemon.short_name);
            sb.append(", ");
        }
        picemon = getRealPicemon(address_components, "sublocality_level_1");
        if (picemon != null) {
            sb.append(picemon.short_name);
            sb.append(", ");
        }
        picemon = getRealPicemon(address_components, "route");
        if (picemon != null) {
            sb.append(picemon.long_name);
            sb.append(", ");
        }
        picemon = getRealPicemon(address_components, "street_number");
        if (picemon != null) {
            sb.append(picemon.long_name);
            sb.append(", ");
        }
        picemon = getRealPicemon(address_components, "premise");
        if (picemon != null) {
            sb.append(picemon.long_name);
            sb.append(", ");
        }
        return sb.toString().substring(0, sb.lastIndexOf(","));
    }

    private static SenderGoogleAddressToGeoGET.Pokemon getRealPicemon(List<SenderGoogleAddressToGeoGET.Pokemon> list, String type) {
        SenderGoogleAddressToGeoGET.Pokemon p = null;
        for (SenderGoogleAddressToGeoGET.Pokemon picemon : list) {
            if (picemon.types.contains(type)) {
                p = picemon;
            }
        }
        return p;
    }

    public void send(double latitude, double longitude, SenderGoogleAddressToGeoGET.IActionAddressGeo iAction, Activity activity) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.iAction = iAction;
        this.activity = activity;
        new SenderWorkerTack().execute(latitude, longitude);
    }

    private HashMap<String, String> getParametr(double latitude, double longitude) {
        HashMap<String, String> params = new HashMap<>();
        params.put("latlng", String.valueOf(latitude) + "," + String.valueOf(longitude));
        params.put("language", "ru");
        return params;
    }

    private void senderCore(HashMap<String, String> params) {
        String sd = null;
        try {
            sd = Utils.getPostDataString(params);
        } catch (UnsupportedEncodingException e) {
            log.error("gps1" + e);
            return;
        }
        URL url;
        StringBuilder sb = new StringBuilder();
        try {
            url = new URL("http://maps.googleapis.com/maps/api/geocode/json?" + sd);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                Gson gson = Utils.getGson();
                SenderGoogleAddressToGeoGET.AddressGeo sdd = gson.fromJson(sb.toString(), SenderGoogleAddressToGeoGET.AddressGeo.class);
                if (sdd != null && sdd.results != null && sdd.results.size() > 0) {
                    SenderGoogleAddressToGeoGET.AddressE addressE = sdd.results.get(0);
                    formatAddress = parseAddress(addressE.address_components);
                    latitude = addressE.geometry.location.lat;
                    longitude = addressE.geometry.location.lng;
                }
            } else {
            }
        } catch (Exception ignore) {
            log.error("gps:" + ignore.getMessage());
        }
    }

    private class SenderWorkerTack extends AsyncTask<Double, Void, Void> {

        volatile boolean isStop;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(activity, Settings.core())) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, "Получение адреса", new IActionE<View>() {
                @Override
                public void action(View v) {
                    isStop = true;
                }
            });
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if (isCancelled()) return;
            if (!isStop) {

                if (dialog != null) {
                    dialog.cancel();
                }
                iAction.action(formatAddress, String.valueOf(latitude), String.valueOf(longitude));
            }
        }

        @Override
        protected Void doInBackground(Double... params) {

            if (isCancelled()) return null;
            HashMap<String, String> param = getParametr(params[0], params[1]);
            senderCore(param);
            return null;
        }
    }
}
