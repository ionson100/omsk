package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.Where;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// при запросе истории по дням
@Table("story_visit_point")//story_visit_point_day
@Where("ping= " + PINGATOR.story_visit_point_day)
public class MVisitStoryPointDay extends MVisitStoryBase implements Serializable, IPing {//
    @Column("ping")
    public int ping = PINGATOR.story_visit_point_day;


}
