package com.example.user.omsk.senders;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;

import com.example.user.omsk.models.MNewOrder;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.addin.NewOrderActivity;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class SenderHistoryNewOrders {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderHistoryNewOrders.class);
    private Activity mActivity;
    private Settings mSettings;

    public void send(Activity activity) {


        int i = (int) Configure.getSession().executeScalar("select count(*) from story_new_order");
        //List<MNewOrder> mNewOrders=Configure.getSession().getList(MNewOrder.class,null);
        if (i > 0) {
            activity.startActivity(new Intent(activity, NewOrderActivity.class));
        } else {
            this.mActivity = activity;
            this.mSettings = Settings.core();
            new SenderHistoryNewOrders.SenderJob().execute();
        }


    }


    private class SenderJob extends AsyncTask<Void, Void, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(mActivity, mSettings)) {
                cancel(true);
                Toast.makeText(mActivity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(mActivity, mActivity.getString(R.string.get_data), new IActionE<View>() {
                @Override
                public void action(View v) {
                }
            });
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {

            if (isCancelled()) return;

            if (dialog != null) {
                dialog.cancel();
            }
            if (aBoolean != null) {
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/route_history_client/:  " + aBoolean;
                Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
                log.error(msg);
                // new SenderErrorPOST().send(mActivity, msg, Settings.core());
            } else {
                mActivity.startActivity(new Intent(mActivity, NewOrderActivity.class));
            }
        }


        @Override
        protected String doInBackground(Void... params) {
            String str = null;
            if (isCancelled()) {
                return null;
            }
            final String[] result = {null};
            try {
                str = Utils.HTTP + mSettings.url + "/request_history/?guid=" + URLEncoder.encode(SettingsUser.core().getUuidDevice(),
                        "UTF-8");

            } catch (UnsupportedEncodingException e) {

                return e.getMessage();
            }

            result[0] = UtilsSender.getRequest((MyApplication) mActivity.getApplication(), str, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {
                    App(str);
                }
            });

            return result[0];

        }

        private void App(String res) {


            Gson sd3 = Utils.getGson();
            Type listType = new TypeToken<ArrayList<MNewOrder>>() {
            }.getType();

            List<MNewOrder> list = sd3.fromJson(res, listType);

            ISession session = Configure.getSession();
            for (MNewOrder mNewOrder : list) {

                for (MProduct product : mNewOrder.products) {
                    product.setAmountCore(product.amountSelf);
                    product.amountSelf = 0;
                }

                session.insert(mNewOrder);
            }
        }
    }

}
