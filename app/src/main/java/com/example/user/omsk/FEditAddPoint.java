package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.DaData.ui.AutoCompleteActivity;
import com.example.user.omsk.addin.AddinEditPoint;
import com.example.user.omsk.addin.AddinNamePoint;
import com.example.user.omsk.dialogs.DialogDetermineGeoAddress;
import com.example.user.omsk.doublePoints.DoublePoints;
import com.example.user.omsk.fotocontrol.MyViewFotoPoint;
import com.example.user.omsk.models.MDistributionchannel;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MPointType;
import com.example.user.omsk.models.MPriceType;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.senders.SenderGoogleAddressToGeoGET;
import com.example.user.omsk.senders.SenderGoogleGeoToAddressGET;

import org.apache.log4j.Logger;

import java.util.List;

//import com.example.user.omsk.models.MError;


public class FEditAddPoint extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FEditAddPoint.class);
    private View mView;
    private List<MPointType> mPointTypeList;
    private List<MDistributionchannel> mChannelList;
    private List<MPriceType> typeList;
    private Settings mSettings;
    private AddinNamePoint addinNamePoint;
    private MyViewFotoPoint myViewFotoPoint;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try {
            mSettings = Settings.core();
            mView = inflater.inflate(R.layout.fragment_edit_add_point, container, false);

            if (mSettings.getVisit() == null) {
                Settings.showFragment(StateSystem.HOME, getActivity());
                return mView;
            }

            if (mSettings.getVisit().point == null) {
                mSettings.getVisit().point = new MPoint();
                mSettings.getVisit().point.setNewObject(true);
            }

            typeList = Configure.getSession().getList(MPriceType.class, null);

            mChannelList = Configure.getSession().getList(MDistributionchannel.class, null);
            mPointTypeList = Configure.getSession().getList(MPointType.class, null);

            mView.findViewById(R.id.bt_auto_complete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().startActivityForResult(new Intent(getActivity(), AutoCompleteActivity.class), 1961);
                }
            });

            AddinEditPoint.addin(getActivity(), mView, mSettings.getVisit().point, this);

            addinNamePoint = new AddinNamePoint();
            addinNamePoint.activate(mView, getActivity(), mSettings.getVisit().point);
            ((TextView) mView.findViewById(R.id.point_name_core)).setText(mSettings.getVisit().point.name);

            binding();
            activateButton();
            myViewFotoPoint = (MyViewFotoPoint) mView.findViewById(R.id.my_control_foto);
            myViewFotoPoint.activate(mSettings.getVisit().point, false, getActivity());

        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FEditAddPoint:" + Utils.getErrorTrace(ex), (MyApplication) getActivity().getApplication());
            Settings.showFragment(StateSystem.HOME, getActivity());
        }
        return mView;
    }


    private void activateButton() {
        Button okBt = (Button) mView.findViewById(R.id.ebtOk);
        Button btMap = (Button) mView.findViewById(R.id.btMap);
        mView.findViewById(R.id.bt_add_address).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSettings.getVisit().point.address != null && mSettings.getVisit().point.address.trim().length() > 5) {
                    String address = RegsAddress.getClearAddres(mSettings.getVisit().point.address);
                    TextView textView = (TextView) mView.findViewById(R.id.e_name);
                    String s = textView.getText().toString();
                    if (s.indexOf(AddinNamePoint.divide2) != -1) {
                        s = s.substring(0, s.indexOf(AddinNamePoint.divide2));
                    }
                    textView.setText(s + AddinNamePoint.divide2 + address);
                }
            }
        });

        if (mSettings.getVisit().point.id==0) {
            mView.findViewById(R.id.bt_double_points).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSettings.getVisit().point.name.trim().length() == 0) {
                        Toast.makeText(getActivity(), getString(R.string.check_double), Toast.LENGTH_SHORT).show();
                    } else {
                        new DoublePoints(getActivity(), mSettings).check(mSettings.getVisit().point);
                    }
                }
            });
        } else {
            mView.findViewById(R.id.bt_double_points).setVisibility(View.GONE);
        }


        mView.findViewById(R.id.bt_geo_addres2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!Utils.isLocationServiceEnabled(getActivity())) {
                    Utils.messageBox(getString(R.string.error), getString(R.string.kahjadas), getActivity(), new IActionE() {
                        @Override
                        public void action(Object o) {
                            Intent viewIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            getActivity().startActivity(viewIntent);
                        }
                    });
                    return;
                }


                new DialogDetermineGeoAddress().activate(getActivity(), new SenderGoogleAddressToGeoGET.IActionAddressGeo() {
                    @Override
                    public void action(String address, String latitude, String longitude) {
                        if (address == null || address.trim().length() == 0) {
                        } else {
                            ((TextView) mView.findViewById(R.id.e_addres)).setText(address);
                        }

                        ((TextView) mView.findViewById(R.id.e_latitude)).setText(latitude);
                        ((TextView) mView.findViewById(R.id.e_longitude)).setText(longitude);
                    }
                }).show(getActivity().getSupportFragmentManager(), "dsfyfdfusdfsdf");
            }
        });


        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.EDIT_MAP, getActivity());
            }
        });

        mView.findViewById(R.id.bt_address_geo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mSettings.getVisit().point.address == null || mSettings.getVisit().point.address.trim().length() == 0) {
                    ((EditText) mView.findViewById(R.id.e_addres)).setError(getString(R.string.field_addres_null));
                    mView.findViewById(R.id.e_addres).requestFocus();
                    return;
                }

                new SenderGoogleAddressToGeoGET().send(mSettings.getVisit().point.address, new SenderGoogleAddressToGeoGET.IActionAddressGeo() {
                    @Override
                    public void action(final String address, final String latitude, final String longitude) {

                        double lat = 0, lon = 0;
                        try {
                            lat = Double.parseDouble(latitude);
                            lon = Double.parseDouble(longitude);
                        } catch (Exception ignored) {

                        }
                        if (lat == 0 || lon == 0) {
                            Utils.messageBox(getString(R.string.asasas), getString(R.string.oisodidsd), getActivity(), null);
                            return;
                        }


                        Utils.messageBoxAddressToCoordinate(address, latitude, longitude, getActivity(), new IActionE<View>() {
                            @Override
                            public void action(View v) {

                            }
                        }, new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                ((EditText) mView.findViewById(R.id.e_addres)).setText(address);
                                ((EditText) mView.findViewById(R.id.e_latitude)).setText(longitude);
                                ((EditText) mView.findViewById(R.id.e_longitude)).setText(latitude);
                            }
                        });


                    }
                }, getActivity());

            }
        });

        mView.findViewById(R.id.bt_geo_addres).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSettings.getVisit().point.latitude == 0 && mSettings.getVisit().point.longitude == 0) {
                    ((EditText) mView.findViewById(R.id.e_latitude)).setError(getString(R.string.geo_error));
                    mView.findViewById(R.id.e_latitude).requestFocus();
                    ((EditText) mView.findViewById(R.id.e_longitude)).setError(getString(R.string.geo_error));
                    mView.findViewById(R.id.e_longitude).requestFocus();
                    return;
                }


                new SenderGoogleGeoToAddressGET().send(mSettings.getVisit().point.latitude, mSettings.getVisit().point.longitude, new SenderGoogleAddressToGeoGET.IActionAddressGeo() {
                    @Override
                    public void action(final String address, String latitude, String longitude) {
                        Utils.messageBoxCoordinateToAddress(address, latitude, longitude, getActivity(), new IActionE<View>() {
                            @Override
                            public void action(View v) {

                            }
                        }, new IActionE<View>() {
                            @Override
                            public void action(View v) {
                                ((EditText) mView.findViewById(R.id.e_addres)).setText(address);
                            }
                        });

                    }
                }, getActivity());

            }
        });

        okBt.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                buttonOkClick();
            }
        });

        mView.findViewById(R.id.bt_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showHelpNote(getActivity(),  "note1.html");
            }
        });

        mView.findViewById(R.id.bt_help_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showHelpNote(getActivity(), "note_name");
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void buttonOkClick() {


        if (((EditText) mView.findViewById(R.id.name_point_ooo_name)).getText().toString().trim().length() == 0) {

            Utils.messageBox(getString(R.string.error), getString(R.string.kjkjsafdasff), getActivity(), null);
            ((EditText) mView.findViewById(R.id.name_point_ooo_name)).setError(getString(R.string.kisudsd));
            return;
        }
        if (!addinNamePoint.validateNamePoint(mSettings.getVisit().point.name)) {
            Utils.messageBox(getString(R.string.error), getString(R.string.jks23ff), getActivity(), null);
            return;
        }

        if (mSettings.getVisit().point.fio == null || mSettings.getVisit().point.fio.trim().length() == 0) {

            Utils.messageBox(getString(R.string.error), getString(R.string.ladsd), getActivity(), null);
            ((EditText) mView.findViewById(R.id.e_fio)).setError(getString(R.string.kisudsd));
            return;
        }
        if (mSettings.getVisit().point.telephone_fio == null || mSettings.getVisit().point.telephone_fio.trim().length() != 11) {

            Utils.messageBox(getString(R.string.error), getString(R.string.jksdffff), getActivity(), null);
            ((EditText) mView.findViewById(R.id.e_fio_tel)).setError(getString(R.string.jksdffff2));
            return;
        }


        EditText ddd = (EditText) mView.findViewById(R.id.e_inn);
        if (!com.example.user.omsk.addin.ValidateINN.isValidINN(ddd.getText().toString().trim())) {
            Utils.messageBox(getString(R.string.error), getString(R.string.jskadjfasd), getActivity(), null);
            ((EditText) mView.findViewById(R.id.e_inn)).setError(getString(R.string.jksdffff2));
            return;
        }



        if (myViewFotoPoint.isValidateCount() == false) {
            Utils.messageBox(getString(R.string.error), getString(R.string.dsofusdfsdf), getActivity(), null);
            return;
        }

        if (mSettings.getVisit().point.name == null || mSettings.getVisit().point.name.trim().length() == 0) {
            ((EditText) mView.findViewById(R.id.e_name)).setError(getString(R.string.field_name_null));
            mView.findViewById(R.id.e_name).requestFocus();
            return;
        }

        if (mSettings.getVisit().point.address == null || mSettings.getVisit().point.address.trim().length() == 0) {
            ((EditText) mView.findViewById(R.id.e_addres)).setError(getString(R.string.field_addres_null));
            mView.findViewById(R.id.e_addres).requestFocus();
            return;
        }

        if (!CheckNamePoint.check(mSettings.getVisit().point, getActivity())) {
            return;
        }

            mSettings.getVisit().point.price_type_id = typeList.get(0).get_id();

            mSettings.getVisit().point.point_type_id = mPointTypeList.get(0).idu;

        /////////////////////////////////add
        mSettings.getVisit().point.name = mSettings.getVisit().point.name.trim();
        mSettings.getVisit().point.address = mSettings.getVisit().point.address.trim();
        /////////////////////////////////////

        mSettings.getVisit().point.setNewObject(true);
        mSettings.getVisit().point.date_pessimise = Utils.curDate().getTime();

        ISession ses = Configure.getSession();
        ses.beginTransaction();
        try {

            if (mSettings.getVisit().point.id == 0) {
                ses.insert(mSettings.getVisit().point);
            } else {
                ses.update(mSettings.getVisit().point);
            }
            /////////////////////////////////////////////// фотографии точки
            myViewFotoPoint.save(mSettings.getVisit().point.deletePhotos, ses);

            ses.commitTransaction();

        } catch (Exception ex) {
            String msg = getString(R.string.nopointsave) + ex.getMessage();
            log.error(msg);
           // Configure.getSession().insert(new MError(msg));
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
            Settings.showFragment(StateSystem.HOME, getActivity());
        }finally {
            ses.endTransaction();
            Settings.showFragment(StateSystem.SEARCH_POINT, getActivity());
            getActivity().sendBroadcast(new Intent(MainActivity.FAB));
        }
    }

    private void binding() {

        EditText nameE = (EditText) mView.findViewById(R.id.e_name);

        String aas = ((EditText) mView.findViewById(R.id.name_point_ooo_name)).getText().toString().trim();

        if (aas.length() != 0) {
            nameE.setText(mSettings.getVisit().point.name);
        }
        nameE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String sd = s.toString();
                mSettings.getVisit().point.name = sd;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        EditText addressE = (EditText) mView.findViewById(R.id.e_addres);
        addressE.setText(mSettings.getVisit().point.address);
        addressE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().point.address = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        EditText commentaryE = (EditText) mView.findViewById(R.id.e_commentary);
        commentaryE.setText(mSettings.getVisit().point.commentary);
        commentaryE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSettings.getVisit().point.commentary = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        EditText laqtitudeE = (EditText) mView.findViewById(R.id.e_latitude);
        laqtitudeE.setText(String.valueOf(mSettings.getVisit().point.latitude));
        laqtitudeE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    mSettings.getVisit().point.latitude = Double.parseDouble(s.toString());
                } catch (Exception e) {
                    mSettings.getVisit().point.latitude = 0d;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        EditText longitudeE = (EditText) mView.findViewById(R.id.e_longitude);
        longitudeE.setText(String.valueOf(mSettings.getVisit().point.longitude));
        longitudeE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    mSettings.getVisit().point.longitude = Double.parseDouble(s.toString());
                } catch (Exception e) {
                    mSettings.getVisit().point.longitude = 0d;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }



    public void refrashImage() {
        myViewFotoPoint.showImages();
    }


    public void refrashAddress() {
        ((TextView) mView.findViewById(R.id.e_addres)).setText(mSettings.getVisit().point.address);
        ((TextView) mView.findViewById(R.id.e_latitude)).setText("" + mSettings.getVisit().point.latitude);
        ((TextView) mView.findViewById(R.id.e_longitude)).setText("" + mSettings.getVisit().point.longitude);
    }
}
