package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.example.user.omsk.models.MPromotion;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * рекламные материалы
 */
public class FAdvertising extends Fragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FAdvertising.class);
    private ListView listView;
    private EditText editText;
    private List<MPromotion> mPromotions;
    // private Settings mSettings;
    private View mView;

    public FAdvertising() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_advertising, container, false);
        try {
            mPromotions = Configure.getSession().getList(MPromotion.class, null);

            Collections.sort(mPromotions, new Comparator<MPromotion>() {
                @Override
                public int compare(MPromotion lhs, MPromotion rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            List<MPromotion> delete = new ArrayList<>();
            for (MPromotion mPromotion : mPromotions) {
                File f = new File(Utils.getPromotionDirectory() + "/" + mPromotion.file_name);
                if (!f.exists()) {
                    delete.add(mPromotion);
                } else {

                }
            }
            for (MPromotion mPromotion : delete) {
                mPromotions.remove(mPromotion);
            }
            activateListAdvertising();
            activateListAbs();
            activateButton();

        } catch (Exception ex) {
            log.error(ex);
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void activateButton() {

        mView.findViewById(R.id.bt_to_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Settings.getStateSystem() == StateSystem.ADVERTISING) {
                    Settings.showFragment(StateSystem.HOME, getActivity());
                } else {
                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                }

            }
        });

        mView.findViewById(R.id.bt_show_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });

        editText = (EditText) mView.findViewById(R.id.text_search);
        editText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().equals("")) {
                    ListAdapterAdvertising advertising = new ListAdapterAdvertising(getActivity(), R.layout.item_list_promotion, mPromotions,null);
                    listView = (ListView) mView.findViewById(R.id.list_advertising);
                    listView.setAdapter(advertising);
                } else {
                    List<MPromotion> mPromotionList = new ArrayList<>();
                    for (MPromotion mPromotion : mPromotions) {
                        if (mPromotion.name.toLowerCase(new Locale("ru", "RU")).contains(s.toString().toLowerCase(new Locale("ru", "RU"))) ||
                                mPromotion.description.toLowerCase(new Locale("ru", "RU")).contains(s.toString().toLowerCase(new Locale("ru", "RU")))) {
                            mPromotionList.add(mPromotion);
                        }
                    }
                    ListAdapterAdvertising advertisings = new ListAdapterAdvertising(getActivity(), R.layout.item_list_promotion, mPromotionList, s.toString());
                    listView = (ListView) mView.findViewById(R.id.list_advertising);
                    listView.setAdapter(advertisings);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0) {
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {

                    editText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search, 0, 0, 0);
                }
            }
        });

    }

    private void activateListAdvertising() {
        ListAdapterAdvertising advertisings = new ListAdapterAdvertising(getActivity(), R.layout.item_list_promotion, mPromotions,null);
        listView = (ListView) mView.findViewById(R.id.list_advertising);
        listView.setAdapter(advertisings);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String file = mPromotions.get(position).file_name;
                    File targetFile = new File(Utils.getPromotionDirectory() + "/" + file);
                    Uri targetUri = Uri.fromFile(targetFile);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String extension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(targetFile).toString());
                    String mimetype = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                    intent.setDataAndType(targetUri, mimetype);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    startActivity(intent);
                } catch (Exception ignored) {
                    log.error(ignored);
                }

            }
        });
    }


    private void activateListAbs() {
        MyListViewAbc listView = (MyListViewAbc) mView.findViewById(R.id.list_abc);
        listView.setIAction(new IActionE<String>() {
            @Override
            public void action(String o) {
                editText.setText("");

                showSearchFirstChar(o);
            }
        });
    }

    private void showSearchFirstChar(String s) {

        List<MPromotion> mPromotionList = new ArrayList<>();

        for (MPromotion mPromotion : mPromotions) {
            if (mPromotion.name.toLowerCase().startsWith(s.toLowerCase())) {
                mPromotionList.add(mPromotion);
            }
        }
        ListAdapterAdvertising advertisings = new ListAdapterAdvertising(getActivity(), R.layout.item_list_promotion, mPromotionList,null);
        listView = (ListView) mView.findViewById(R.id.list_advertising);
        listView.setAdapter(advertisings);
    }
}
