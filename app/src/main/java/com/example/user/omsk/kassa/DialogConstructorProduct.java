package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MProductConstructor;
import com.example.user.omsk.R;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.pupper.Pupper;

import java.util.List;

public class DialogConstructorProduct extends DialogFragment {


    private MProductBase mProduct;
    //private View view;
    private IActionE mIActionE;
    private SettingsKassa mSettingsKassa;
    private EditText mAmountEditText;
    private EditText mPriceEditText;
    private EditText mTotalEditText;
    private Pupper mNamePupper;
    private Pupper mTotalPupper;

    public void setmProduct(MProductBase mProd, View view, IActionE iActionE) {
        this.mProduct = mProd;
        //this.view = view;
        this.mIActionE = iActionE;
    }


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_choice_products_item, null);
        ((TextView) mView.findViewById(R.id.title_dalog)).setText(getString(R.string.hsjhdsd));
        builder.setView(mView);
        mSettingsKassa = SettingsKassa.core();

        boolean add = true;
        for (MProductBase product : mSettingsKassa.constructorList) {
            if (product.idu.equals(mProduct.idu)) {
                add = false;
            }
        }
        if (add) {
            mSettingsKassa.constructorList.add((MProductConstructor) mProduct);
            mSettingsKassa.save();
        }


        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {//удалить продукт
            @Override
            public void onClick(View v) {
                MProductBase sd = Linq.toStream(mSettingsKassa.constructorList).firstOrDefault(new Predicate<MProductConstructor>() {
                    @Override
                    public boolean apply(MProductConstructor t) {
                        return t.idu.equals(mProduct.idu);
                    }
                });
                if (sd != null) {
                    mSettingsKassa.constructorList.remove(sd);
                }

                dismiss();
                mProduct.setAmountCore(0d);
                mProduct.price = 0d;
                mProduct.isSelect = false;
                mIActionE.action(1);

            }
        });
        mView.findViewById(R.id.bt_delete_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSettingsKassa.constructorList.clear();
                dismiss();
                mProduct.price = 0d;
                mProduct.isSelect = false;
                mIActionE.action(2);
            }
        });

        mView.findViewById(R.id.bt_add_prod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mProduct.isSelect = true;
                dismiss();
                mIActionE.action(3);
                List<MProductConstructor> zdd = SettingsKassa.core().constructorList;
                for (MProductConstructor mProductConstructor : zdd) {
                    if (mProductConstructor.idu.equals(mProduct.idu)) {
                        mProductConstructor.price = mProduct.price;
                        mProductConstructor.setAmountCore(mProduct.getAmount_core());
                        SettingsKassa.save();
                        break;
                    }
                }
            }
        });

        mNamePupper = (Pupper) mView.findViewById(R.id.p_name);

        mNamePupper.setPairString(mProduct.name, "");

        mTotalPupper = (Pupper) mView.findViewById(R.id.p_total);

        mAmountEditText = (EditText) mView.findViewById(R.id.pi_amount);
        mPriceEditText = (EditText) mView.findViewById(R.id.pi_price);
        mTotalEditText = (EditText) mView.findViewById(R.id.pi_total);

        mAmountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                double dd = 0;
                try {
                    dd = Double.parseDouble(s.toString().replace(',', '.'));
                } catch (Exception ignored) {
                }
                mProduct.setAmountCore(dd);
                mTotalEditText.setText(Utils.getStringDecimal(mProduct.getAmount_core() * mProduct.price));
                mTotalPupper.setPairString(getString(R.string.asaolsoais), Utils.getStringDecimal(totalevich()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mPriceEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double dd = 0;
                try {
                    dd = Double.parseDouble(s.toString().replace(',', '.'));
                } catch (Exception ignored) {
                }
                mProduct.price = dd;
                mTotalEditText.setText(Utils.getStringDecimal(mProduct.getAmount_core() * mProduct.price).replace(',', '.'));
                mTotalPupper.setPairString(getString(R.string.asaolsoais), Utils.getStringDecimal(totalevich()).replace(',', '.'));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mAmountEditText.setText(Utils.getStringDecimal(mProduct.getAmount_core()).replace(',', '.'));
        mPriceEditText.setText(Utils.getStringDecimal(mProduct.price).replace(',', '.'));


        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    public double totalevich() {
        double dd = 0;
        for (MProductBase product : mSettingsKassa.constructorList) {
            dd = dd + product.getAmount_core() * product.price;
        }
        return dd;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SettingsKassa.save();

    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mIActionE.action(3);
    }
}
