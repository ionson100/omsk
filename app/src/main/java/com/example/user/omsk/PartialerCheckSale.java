package com.example.user.omsk;

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.List;

/**
 * ion100 on 01.11.2017.
 */

public class PartialerCheckSale {

    public static List<List<MProductBase>> divider(List<MProductBase> list, Settings mSettings) {

        List<List<MProductBase>> resultList = new ArrayList<>();

        double summ = 0;

        double max_item = 0;


        for (MProduct mProduct : mSettings.getVisit().selectProductForSale) {
            summ = summ + mProduct.getAmount_core() * mProduct.price;
            if (max_item < mProduct.price) {
                max_item = mProduct.price;
            }
        }
        if (max_item > mSettings.maxSummPrice) {
            return resultList;
        }
        if (summ <= mSettings.maxSummPrice) {
            resultList.add(new ArrayList<MProductBase>(list));
            return resultList;
        } else {
            List<MProduct> coreList = new ArrayList<>();
            for (MProductBase mProduct : list) {
                for (int i = 0; i < mProduct.getAmount_core(); i++) {
                    MProduct product = new MProduct();
                    product.idu = mProduct.idu;
                    product.setAmountCore(1);
                    product.price = mProduct.price;
                    coreList.add(product);
                }
            }

            List<MProduct> products = new ArrayList<>();
            while (coreList.size() > 0) {


                MProduct p = coreList.get(0);
                if ((getTotal(products) + p.getAmount_core() * p.price) < mSettings.maxSummPrice) {
                    products.add(p);
                    coreList.remove(p);
                } else {
                    resultList.add(new ArrayList<MProductBase>(products));
                    products = new ArrayList<>();
                }
            }

            resultList.add(new ArrayList<MProductBase>(products));

            List<List<MProductBase>> resultListCore = new ArrayList<>();

            for (List<MProductBase> mProducts : resultList) {

                List<MProductBase> list1 = new ArrayList<>();
                for (final MProductBase mProduct : mProducts) {

                    boolean es = false;
                    for (MProductBase product : list1) {
                        if (product.idu.equals(mProduct.idu)) {
                            es = true;
                            product.setAmountCore(product.getAmount_core() + mProduct.getAmount_core());
                        }
                    }
                    if (es == false) {
                        MProduct s = Configure.getSession().get(MProduct.class, mProduct.idu);
                        s.setAmountCore(mProduct.getAmount_core());
                        s.price = mProduct.price;
                        list1.add(s);
                    }

                }
                resultListCore.add(list1);

            }
            return resultListCore;
        }


    }

    public static double getTotal(List<MProduct> list) {
        double res = 0;
        for (MProduct mProduct : list) {
            res = res + mProduct.getAmount_core() * mProduct.price;
        }
        return res;
    }
}

