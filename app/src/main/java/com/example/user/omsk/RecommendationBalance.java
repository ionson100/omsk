package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MStockRows;
import com.example.user.omsk.linq2.Linq;

import java.util.ArrayList;
import java.util.List;

public class RecommendationBalance {

    private final List<MProduct> mProductsRecomendation = new ArrayList<>();
    private final Settings mSettings;

    public RecommendationBalance(Settings mSettings) {
        this.mSettings = mSettings;
    }

    public boolean conpute() {

        UpdateRecommendationsList();

        for (final MProduct mProduct : mProductsRecomendation) {

            MProduct p = Linq.toStream(mSettings.getVisit().selectProductForBalance).firstOrDefault(new com.example.user.omsk.linq2.Predicate<MProduct>() {
                @Override
                public boolean apply(MProduct t) {
                    return t.idu.equals(mProduct.idu);
                }
            });

            if (p == null) {
                mSettings.getVisit().selectProductForBalance.add(mProduct);
            } else {
                if (!p.isEdit) {
                    p.setAmountCore(p.getAmount_core() + mProduct.getAmount_core());
                }
            }
        }
        return mProductsRecomendation.size() > 0;
    }

    public void deCompute() {
        UpdateRecommendationsList();
        for (final MProduct product : mProductsRecomendation) {

            MProduct p = Linq.toStream(mSettings.getVisit().selectProductForBalance).firstOrDefault(new
                                                                                                            com.example.user.omsk.linq2.Predicate<MProduct>() {
                                                                                                                @Override
                                                                                                                public boolean apply(MProduct t) {
                                                                                                                    return t.idu.equals(product.idu);
                                                                                                                }
                                                                                                            });
            if (p != null) {
                if (!p.isEdit) {
                    if (p.getAmount_core() <= product.getAmount_core()) {
                        mSettings.getVisit().selectProductForBalance.remove(p);
                    } else {
                        p.setAmountCore(p.getAmount_core() - product.getAmount_core());
                    }
                }

            }
        }
    }

    private void UpdateRecommendationsList() {
        if (mProductsRecomendation.size() == 0) {
            List<MStockRows> list = mSettings.getVisit().point.getStockRows();
            for (MStockRows rows : list) {
                MProduct p = rows.getProductForShowBalance();
                if (p != null) {
                    mProductsRecomendation.add(p);
                }
            }
        }
    }
}
