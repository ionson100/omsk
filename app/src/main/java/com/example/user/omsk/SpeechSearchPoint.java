package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.ImageButton;

import static com.example.user.omsk.Utils.isSpeechRecognitionActivityPresented;


class SpeechSearchPoint {

    public static final int SPEECH = 23;
    private final Activity activity;
    private ImageButton imageButton;

    SpeechSearchPoint(View mView, final Activity activity) {
        this.activity = activity;
        imageButton = (ImageButton) mView.findViewById(R.id.bt_microphone);
    }

    public void activate() {
        if (!isSpeechRecognitionActivityPresented(activity)) {
            imageButton.setVisibility(View.GONE);
            return;
        }
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Голосовой поиск");    // текстовая подсказка пользователю
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM); // модель распознавания оптимальная для распознавания коротких фраз-поисковых запросов
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);    // количество результатов, которое мы хотим получить, в данном случае хотим только первый - самый релевантный
                activity.startActivityForResult(intent, SPEECH);
            }
        });
    }
}

class SpeechSearchProduct {

    public static final int SPEECH = 24;
    private final Activity activity;
    private ImageButton imageButton;

    SpeechSearchProduct(View mView, final Activity activity) {
        this.activity = activity;
        imageButton = (ImageButton) mView.findViewById(R.id.image_microphone_product);
    }

    public void activate() {
        if (!isSpeechRecognitionActivityPresented(activity)) {
            imageButton.setVisibility(View.GONE);
            return;
        }
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Голосовой поиск");    // текстовая подсказка пользователю
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM); // модель распознавания оптимальная для распознавания коротких фраз-поисковых запросов
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);    // количество результатов, которое мы хотим получить, в данном случае хотим только первый - самый релевантный
                activity.startActivityForResult(intent, SPEECH);
            }
        });
    }
}
