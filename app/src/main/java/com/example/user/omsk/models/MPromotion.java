package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// рекламная рассылка ( сервер)
@Table("romotion")
public class MPromotion implements IUsingGuidId, Serializable {

    @PrimaryKey("id")
    public transient int id;

    @SerializedName("id")
    @Column("idu")
    public String idu;

    @SerializedName("name")
    @Column("name")
    public String name;

    @SerializedName("file_name")
    @Column("file_name")
    public String file_name;

    @SerializedName("description")
    @Column("description")
    public String description;

    @Column("images")
    public transient byte[] images;

    public MPromotion() {
        this.idu = Utils.getUuid();
    }

    public MPromotion(String idu, String name, String file_name) {
        this.idu = idu;
        this.name = name;
        this.file_name = file_name;
    }

    public String get_id() {
        return idu;
    }
}
