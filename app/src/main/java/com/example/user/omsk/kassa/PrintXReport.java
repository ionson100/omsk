package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;


public class PrintXReport {
    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(PrintXReport.class);
    public static void get(final TextView log, final Activity activity, final Settings mSettings, final IActionE iActionE) {
        new AsyncTask<Void, String, Void>() {
            private ProgressDialog dialog;
            private Fptr fptr;
            private Zreport zreport;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                zreport = new Zreport();
                try {

                    fptr = new Fptr();
                    fptr.create(activity.getApplication());

                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }

                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");

                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError();
                    }

                    if (fptr.put_Mode(IFptr.MODE_REPORT_NO_CLEAR) < 0) {
                        checkError();
                    }
                    if (fptr.SetMode() < 0) {
                        checkError();
                    }
                    if (fptr.put_ReportType(IFptr.REPORT_X) < 0) {
                        checkError();
                    }
                    if (fptr.Report() < 0) {
                        checkError();
                    }


                } catch (Exception ex) {
                    logE.error(ex);
                    zreport.error = ex.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - PrintXReport\n" + ex.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            protected void onPreExecute() {
                log.setText("");
                dialog = Utils.factoryDialog(activity, "X - Отчет", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                iActionE.action(zreport);
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }
        }.execute();
    }
}
