package com.example.user.omsk;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * ion100 USER on 07.12.2017.
 */

class ShowmenMenu {
    public static void show(List<TempIonMenu> tempIOnMenus, FactoryMenu factoryMenu, Activity activity) {


        List<List<TempIonMenu>> lists = Utils.chopped(tempIOnMenus, 3);

        for (final List<TempIonMenu> list : lists) {
            View v = LayoutInflater.from(activity).inflate(R.layout.item_menu, null);
            View menu1 = v.findViewById(R.id.menu1);
            TextView text1 = (TextView) v.findViewById(R.id.text1);
            ImageView image1 = (ImageView) v.findViewById(R.id.image1);

            View menu2 = v.findViewById(R.id.menu2);
            TextView text2 = (TextView) v.findViewById(R.id.text2);
            ImageView image2 = (ImageView) v.findViewById(R.id.image2);

            View menu3 = v.findViewById(R.id.menu3);
            TextView text3 = (TextView) v.findViewById(R.id.text3);
            ImageView image3 = (ImageView) v.findViewById(R.id.image3);

            if (list.size() == 1) {
                text1.setText(list.get(0).name);
                image1.setImageResource(list.get(0).image);
                menu1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(0).iActionE.action(null);
                    }
                });
                menu1.setVisibility(View.VISIBLE);
            }
            if (list.size() == 2) {

                text1.setText(list.get(0).name);
                image1.setImageResource(list.get(0).image);
                menu1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(0).iActionE.action(null);
                    }
                });
                menu1.setVisibility(View.VISIBLE);


                text2.setText(list.get(1).name);
                image2.setImageResource(list.get(1).image);
                menu2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(1).iActionE.action(null);
                    }
                });
                menu2.setVisibility(View.VISIBLE);
            }
            if (list.size() == 3) {
                text1.setText(list.get(0).name);
                image1.setImageResource(list.get(0).image);
                menu1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(0).iActionE.action(null);
                    }
                });
                menu1.setVisibility(View.VISIBLE);


                text2.setText(list.get(1).name);
                image2.setImageResource(list.get(1).image);
                menu2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(1).iActionE.action(null);
                    }
                });
                menu2.setVisibility(View.VISIBLE);

                text3.setText(list.get(2).name);
                image3.setImageResource(list.get(2).image);
                menu3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        list.get(2).iActionE.action(null);
                    }
                });
                menu3.setVisibility(View.VISIBLE);
            }

            factoryMenu.getProdictPanelAddin().addView(v);


        }


    }
}
