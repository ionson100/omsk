package com.example.user.omsk.models;

import java.util.Date;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public interface IObservable {


    /** true -новый объект, false -старый
     * @return
     */
    boolean isNewObject();

    /**true -новый объект, false -старый
     * @param value
     */
    void setNewObject(boolean value);

    Date getDate();

}

