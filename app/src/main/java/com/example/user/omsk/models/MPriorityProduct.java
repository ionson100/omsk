package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

// получаем с сервера)
@Table("priority_product")
public class MPriorityProduct {

    @PrimaryKey("id")
    public transient int id;

    @SerializedName("product")
    @Column("product_id")
    public String product_id;

    @SerializedName("priority")
    @Column("priority_id")
    public String priority_id;
}
