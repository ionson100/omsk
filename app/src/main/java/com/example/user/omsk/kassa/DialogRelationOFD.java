package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.SettingsKassa;


public class DialogRelationOFD extends DialogFragment {


    // private Fptr mFptr;
    private IActionE mIActionE;
    private SettingsKassa mSes;

    public void setSettongsKassa(SettingsKassa ses) {
        this.mSes = ses;
    }

    public void setmIActionE(IActionE mIActionE) {
        this.mIActionE = mIActionE;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

      //  final SettingsKassa settingsKassa = SettingsKassa.core();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_chanel_ofd, null);
        builder.setView(mView);

        //final EditText ofd_url, ofd_dns, ofd_port;
       // ofd_url = (EditText) mView.findViewById(R.id.ofd_addresss);
       // ofd_dns = (EditText) mView.findViewById(R.id.ofd_dns);
       // ofd_port = (EditText) mView.findViewById(R.id.ofd_port);
        final RadioButton enthernetRadioButton = (RadioButton) mView.findViewById(R.id.rb_Ethernet);
        final RadioButton wifiRadioButton = (RadioButton) mView.findViewById(R.id.rb_Wi_Fi);
        final RadioButton gpsRadioButton = (RadioButton) mView.findViewById(R.id.rb_GSM);
        enthernetRadioButton.setChecked(false);
        wifiRadioButton.setChecked(false);
        gpsRadioButton.setChecked(false);
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mView.findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dismiss();

                if (mIActionE != null) {
                    mIActionE.action(mSes);
                }
            }
        });


        return builder.create();
    }
//    private void checkError() throws Exception {
//        int rc = mFptr.get_ResultCode();
//        if (rc < 0) {
//            String rd = mFptr.get_ResultDescription(), bpd = null;
//            if (rc == -6) {
//                bpd = mFptr.get_BadParamDescription();
//            }
//            if (bpd != null) {
//                throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
//            } else {
//                throw new Exception(String.format("[%d] %s", rc, rd));
//            }
//        }
//    }


}
