package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.example.user.omsk.orm2.UserField;

import java.util.Date;

/**
 * тут храним чеки конструкторв и их отмену
 */

@Table("checks")
public class MChecks {
    @PrimaryKey("id")
    public int id;

    @Column("date")
    public Date date = null;

    @UserField(IUserType = MyCheckDataOneField.class)
    @Column("check_data")
    public MChecksData checkData;

    @Column("is_sender")
    public boolean isSender;
}
