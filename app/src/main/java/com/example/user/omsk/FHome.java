package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.atol.drivers.fptr.settings.SettingsActivity;
import com.example.user.omsk.models.MChat;
import com.example.user.omsk.models.MPhoto;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.models.MVideo;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.senders.SenderChatMessagePOST;
import com.example.user.omsk.senders.SenderCheckUpdateGet;
import com.example.user.omsk.senders.SenderCommitDey;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.senders.SenderGetRouterGET;
import com.example.user.omsk.senders.SenderHistoryNewOrders;
import com.example.user.omsk.senders.SenderSynchronizationPOST;
import com.example.user.omsk.senders.SenderUpdateAPKGET;
import com.example.user.omsk.senders.UtilsSender;
import com.example.user.omsk.auto.AutoCloseActivity;
import com.example.user.omsk.auto.AutoOpenActivity;
import com.example.user.omsk.auto.MAuto;
import com.example.user.omsk.chat.DiscussArrayAdapter;
import com.example.user.omsk.kassa.MChecks;
import com.example.user.omsk.kassa.MZReport;
import com.example.user.omsk.kassa.StartIntentKassa;
import com.example.user.omsk.kassa.TestKassa;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.route.SettingsRoute;
import com.example.user.omsk.setting.Reanimator;
import com.example.user.omsk.visitstoryworker.DialogVisitStoryAll;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FHome extends Fragment implements IUpdateChat {


    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FHome.class);
    private List<MVisitStory> visitStoryList;
    private long lastClickTime = 0;
    private static final long MIN_CLICK_INTERVAL = 3000;
    private EditText mEditText1;
    private DiscussArrayAdapter mAdapter;
    private ListView mListView;
    private Button btStartDey;
    private Button btSynchronize;
    private Button btToServer;
    private Button btCheckUpdate;
    private View mView;
    private Settings mSettings;
    private Button buttonUpdate, btPlan, btStock, btPoints;


    public FHome() {
        mSettings = Settings.core();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        mView = inflater.inflate(R.layout.fragment_home, container, false);
        visitStoryList = Configure.getSession().getList(MVisitStory.class, null);
        ISession ses;
        ses = Configure.getSession();
        List<MTemplatePropety> mTemplatePropetyList = ses.getList(MTemplatePropety.class, null);
        if (mTemplatePropetyList.size() > 0) {
            MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
            if (mTemplatePropety.successSynchronize == false) {
                Utils.messageBox(getString(R.string.warning), getString(R.string.jzkddew), getActivity(), null);
            }
        }


        btCheckUpdate = (Button) mView.findViewById(R.id.button_check_update);
        buttonUpdate = (Button) mView.findViewById(R.id.button_update_app);

        mEditText1 = (EditText) mView.findViewById(R.id.edittetxt);
        mListView = (ListView) mView.findViewById(R.id.chat);
        Button btSetting = (Button) mView.findViewById(R.id.button_2);
        Button btChat = (Button) mView.findViewById(R.id.button_6);
        btPoints = (Button) mView.findViewById(R.id.button_7);
        btStock = (Button) mView.findViewById(R.id.button_4);
        Button btStory = (Button) mView.findViewById(R.id.button_8);
        btSynchronize = (Button) mView.findViewById(R.id.button_9);
        btToServer = (Button) mView.findViewById(R.id.button_10);

        btStartDey = (Button) mView.findViewById(R.id.button_00);

        visibleButtonUpdate(mSettings.currentVersionName, mSettings.serverVersionName);

        btCheckUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SenderCheckUpdateGet().send(getActivity(),  new IActionE<String>() {
                    @Override
                    public void action(String o) {
                        visibleButtonUpdate(mSettings.currentVersionName, mSettings.serverVersionName);
                    }
                });
            }
        });


        btPlan = (Button) mView.findViewById(R.id.button_plan);
        Button btMap = (Button) mView.findViewById(R.id.button_5);


        mView.findViewById(R.id.button_story).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateShowHistory()) {
                    StartIntentHistory.start(getActivity());
                }

            }
        });

        mView.findViewById(R.id.bt_kassa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartIntentKassa.start(getActivity(), mSettings);
            }
        });


        btPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartIntentPlan.start(getActivity(), mSettings);
            }
        });

        mView.findViewById(R.id.button_102).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartIntentLack.start(mSettings, getActivity());
            }
        });

        mView.findViewById(R.id.button_101).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartIntentRoute.start(mSettings, getActivity());
            }
        });

        mView.findViewById(R.id.button_100).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenBrowser.open(getActivity(), mSettings);
            }
        });


        btStartDey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;
                    //   startDay();
                    if (!SettingsUser.core().isAutorise()) {
                        StartLoginActivity.start(getActivity());
                        return;
                    }
                    if (Utils.isExsistDataToServer()) {
                        Utils.messageBox(getString(R.string.error_label), getString(R.string.error_out_work_state), getActivity(), null);
                        return;
                    }
                    if (mSettings.useAuto && mSettings.uid_auto != null && mSettings.uid_auto.length() > 0) {


                        List<MAuto> mAutos = Configure.getSession().getList(MAuto.class, " uid = ? and date1 = 0", mSettings.uid_auto);
                        if (mAutos.size() > 0) {
                            if (mAutos.get(0).date1==null && mAutos.get(0).uid.equals(mSettings.uid_auto)) {
                                getActivity().startActivityForResult(new Intent(getActivity(), AutoOpenActivity.class), AutoOpenActivity.RESULT);
                                return;
                            }
                        }


                        List<MAuto> sd = Configure.getSession().getList(MAuto.class, " uid = ? and date2 = 0", mSettings.uid_auto);
                        if (sd != null && sd.size() > 0) {
                            getActivity().startActivityForResult(new Intent(getActivity(), AutoCloseActivity.class), AutoCloseActivity.RESULT);
                        } else {
                            startDay();
                        }
                    } else {
                        startDay();
                    }
                }
            }


        });

        btToServer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!SettingsUser.core().isAutorise()) {
                    if (Utils.isExsistDataToServer()) {
                        Utils.messageBox(getString(R.string.error), getString(R.string.error_send_to_server1), getActivity(), null);
                    } else {
                        StartLoginActivity.start(getActivity());
                    }
                } else {
                    senderToServer();
                }


            }
        });


        btSynchronize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                synchonise();
            }

            private void synchonise() {
                if (!SettingsUser.core().isAutorise()) {
                    StartLoginActivity.start(getActivity());
                } else {
                    if (Utils.isExsistDataToServer()) {
                        Utils.messageBox(getString(R.string.error_label), getString(R.string.error_send1), getActivity(), null);
                    } else {
                        SenderSynchronizationPOST sd = new SenderSynchronizationPOST( getActivity(), false, false);
                        sd.sendSynchronization(0, null);
                    }
                }
            }
        });


        btStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.ERROR, getActivity());

            }
        });

        btStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ValidateDay.validate(mSettings, getActivity())) {
                    Settings.showFragment(StateSystem.SHOW_STOCK_SIMPLE, getActivity());
                }
            }
        });

        btSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.SETTINGS, getActivity());
            }
        });

        btPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartSearchPointFragment.start(mSettings, getActivity());
            }
        });


        btChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.CHAT, getActivity());
            }
        });

        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.SIMPLE_MAP, getActivity());
            }
        });

        mView.findViewById(R.id.button_20).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.ADVERTISING, getActivity());
            }
        });

        initChat();
        addItems();
        visiblerButton();

        mView.findViewById(R.id.bt_test_kassa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TestKassa testKassa = new TestKassa();
                testKassa.test(getActivity());
            }
        });
        pinterStartDayButton(btStartDey);

        mView.findViewById(R.id.bt_new_order).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ValidateDay.validate(mSettings, getActivity())) {
                    new SenderHistoryNewOrders().send(getActivity());
                }

            }
        });


        activateUser();

        mView.findViewById(R.id.titul_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSettings.isStart_dey()) {

                    Utils.messageBox(getString(R.string.error_label), getString(R.string.error_login), getActivity(), null);


                } else {
                    StartLoginActivity.start(getActivity());
                }
            }
        });
        return mView;
    }


    public void activateUser() {
        try {
            TextView textView1 = (TextView) mView.findViewById(R.id.title_user);
            TextView textView2 = (TextView) mView.findViewById(R.id.title_region);
            textView1.setText(SettingsUser.core().getUserName());
            textView2.setText(SettingsUser.core().getRegion());
        } catch (Exception ignored) {

            log.error(ignored);
        }
    }

    public void startDay() {


        if (!Utils.isNetworkAvailable(getActivity(), mSettings) && !mSettings.isStart_dey()) {
            Utils.messageBox(getString(R.string.error), getString(R.string.error_syinch), getActivity(), null);
        } else {
            if (!mSettings.isStart_dey()) {
                mSettings.setStart_dey(!mSettings.isStart_dey());
                pinterStartDayButton(btStartDey);
                SenderSynchronizationPOST sd = new SenderSynchronizationPOST( getActivity(), true, false);
                sd.sendSynchronization(0, new SenderGetRouterGET.IActionRoute() {
                    @Override
                    public void Action(SettingsRoute routeSettings) {
                        boolean d = !mSettings.isStart_dey();
                        mSettings.setStart_dey(d);
                        pinterStartDayButton(btStartDey);
                    }
                });
            } else {

                if (mSettings.useAuto) {
                    closeRoute();
                } else {
                    Utils.messageBoxCloseRoute(getString(R.string.alert_route), getActivity(), new IActionE<View>() {
                        @Override
                        public void action(View v) {
                            closeRoute();
                        }
                    });
                }
            }
        }
    }

    void closeRoute() {
        new SenderCommitDey().send(getActivity(),  new IActionE<Boolean>() {
            @Override
            public void action(Boolean o) {

                boolean d = !mSettings.isStart_dey();
                mSettings.setStart_dey(d);

                pinterStartDayButton(btStartDey);
                mSettings.setVisit(null);
                Settings.save();

            }
        });
    }

    void visiblerButton() {
        if (!mSettings.isStart_dey()) {
            mView.findViewById(R.id.bt_new_order).setVisibility(View.GONE);
            btStock.setVisibility(View.GONE);
            btCheckUpdate.setVisibility(View.VISIBLE);//visible
            btPoints.setVisibility(View.GONE);
            btPlan.setVisibility(View.GONE);
            btSynchronize.setVisibility(View.GONE);
            mView.findViewById(R.id.button_101).setVisibility(View.GONE);
            mView.findViewById(R.id.button_102).setVisibility(View.GONE);
        } else {
            btCheckUpdate.setVisibility(View.GONE);
            btStock.setVisibility(View.VISIBLE);
            btPoints.setVisibility(View.VISIBLE);
            btPlan.setVisibility(View.VISIBLE);
            btSynchronize.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.bt_new_order).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.button_101).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.button_102).setVisibility(View.VISIBLE);
        }
    }


    public void visibleButtonUpdate(String currentVersionName, String serverVersionName) {
        if (currentVersionName == null || serverVersionName == null) {
            buttonUpdate.setVisibility(View.GONE);
            return;
        } else {
            if (currentVersionName.equals(serverVersionName) || serverVersionName == null || serverVersionName.length() == 0) {
                buttonUpdate.setVisibility(View.GONE);
            } else {
                buttonUpdate.setVisibility(View.VISIBLE);
            }
        }

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateUpdate()) {
                    Utils.messageBox(getString(R.string.update_app), getString(R.string.update_app_message), getActivity(), new IActionE<View>() {
                        @Override
                        public void action(View v) {
                            new SenderUpdateAPKGET(getActivity()).send();
                        }
                    });
                }
            }
        });
        if (buttonUpdate.getVisibility() == View.VISIBLE) {
            btStartDey.setBackgroundResource(R.drawable.button_new);
        } else {
            btStartDey.setBackgroundResource(R.drawable.button_top_new);
        }
    }

    public boolean validateUpdate() {
        boolean res = true;
        if (Settings.getStateSystem() != StateSystem.HOME) {
            Toast.makeText(getActivity(), getString(R.string.error_update1), Toast.LENGTH_LONG).show();
            res = false;
        }

        if (mSettings.isStart_dey()) {
            Toast.makeText(getActivity(), getString(R.string.error_update3), Toast.LENGTH_LONG).show();
            res = false;
        }
        if (Utils.isExsistDataToServer()) {
            Toast.makeText(getActivity(), getString(R.string.error_update2), Toast.LENGTH_LONG).show();
            res = false;
        }
        return res;
    }


    private boolean validateShowHistory() {
        boolean res = false;
        int i = (int) Configure.getSession().executeScalar(" select count(*) from product ");
        if (i > 0) {
            res = true;
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_validate_show_history), Toast.LENGTH_LONG).show();
        }
        return res;
    }

    private synchronized void senderToServer() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SenderToServer.sender(mSettings, (MyApplication) getActivity().getApplication());
                    }
                }).start();
            }
        });
    }

    private void pinterStartDayButton(Button button) {
        if (button == null) return;
        if (!mSettings.isStart_dey()) {
            button.setText(getActivity().getString(R.string.start_dey));
        } else {
            button.setText(getActivity().getString(R.string.finish_dey));
        }
        visiblerButton();
    }

    private void initChat() {
        mAdapter = new DiscussArrayAdapter(getActivity());
        mListView.setAdapter(mAdapter);
        mEditText1.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String str = mEditText1.getText().toString().trim();
                    if (str.length() > 0) {

                        switch (str) {
                            case "100312873911": {//удаление планов
                                ISession ses = Configure.getSession();
                                ses.execSQL(" DELETE FROM plan_points ");
                                ses.execSQL(" DELETE FROM plan_pair ");
                                mEditText1.setText("");
                                List<MTemplatePropety> mTemplatePropetyList = ses.getList(MTemplatePropety.class, null);
                                if (mTemplatePropetyList.size() > 0) {
                                    MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
                                    mTemplatePropety.isSendPlan = false;
                                    ses.update(mTemplatePropety);
                                }
                                break;
                            }
                            case "911100312873": //удаление программы
                                String path = Reanimator.getHostPath();
                                if (Utils.deleteDirectorySettingsOmsk(path)) {
                                    getActivity().finish();
                                    System.exit(0);
                                }
                                break;
                            case "bibicon": //свободные sql запросы
                                StartFreeSqlActivity.start(getActivity());
                                break;

                          

                            case "911312873": //отсылка настроек на сервер
                                UtilsSender.sendSettings(mSettings, (MyApplication) getActivity().getApplication());
                                mEditText1.setText("");

                                break;

                            case "00000":
                                try {
                                    Configure.getSession().deleteTable("auto");
                                    getActivity().sendBroadcast(new Intent(MainActivity.FAB));
                                } catch (Exception ex) {
                                    log.error(ex);
                                    Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                                break;
                            case "100312873": {// удаление историй по визитам, и результатов редактирования точек
                                ISession ses = Configure.getSession();
                                List<MPoint> pointList = ses.getList(MPoint.class, " state_object = 1 ");
                                for (MPoint point : pointList) {
                                    point.stateObject = false;
                                    ses.update(point);
                                }
                                List<MVisitStory> list =ses.getList(MVisitStory.class, " state_object = 1 ");
                                for (MVisitStory mVisitStory : list) {
                                    mVisitStory.setNewObject(false);
                                    ses.update(mVisitStory);
                                }

                                List<MChecks> mChecksList = ses.getList(MChecks.class, " is_sender = 0");
                                for (MChecks mChecks : mChecksList) {
                                    mChecks.isSender = true;
                                    ses.update(mChecks);
                                }

                                List<MZReport> mzReportList = ses.getList(MZReport.class, null);
                                for (MZReport mzReport : mzReportList) {
                                    ses.delete(mzReport);
                                }

                                List<MVideo> mVideoList = ses.getList(MVideo.class, null);
                                for (MVideo mVideo : mVideoList) {
                                    ses.delete(mVideo);
                                    File f = new File(mVideo.path);
                                    if (f.exists() && f.length() > 0) {
                                        f.delete();
                                    }
                                }

                                List<MPhoto> mPhotoList = ses.getList(MPhoto.class, null);
                                for (MPhoto mPhoto : mPhotoList) {
                                    ses.delete(mPhoto);
                                    File f = new File(mPhoto.path);
                                    if (f.exists() && f.length() > 0) {
                                        f.delete();
                                    }
                                }

                                ses.deleteTable("auto", "is_sender = 1");
                                getActivity().sendBroadcast(new Intent(MainActivity.FAB));
                                mEditText1.setText("");
                                String ss="Юзер ввел 100312873!!!!!";
                                log.error(ss);
                                new SenderErrorPOST().send((MyApplication) FHome.this.getActivity().getApplication(),ss);

                                break;
                            }
                            case "000111222": {
                                DialogVisitStoryAll storyAll=new DialogVisitStoryAll();
                                storyAll.show(getActivity().getSupportFragmentManager(),"asas");
                                break;
                            }

                            default:

                                if (mEditText1.getText().toString().trim().length() != 0) {
                                    sendMessageToSever((MyApplication) getActivity().getApplication(), mSettings, mEditText1, mListView, mAdapter, getActivity());
                                }

                                break;
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });
    }

    public synchronized static void sendMessageToSever(MyApplication application, Settings mSettings, final EditText mEditText1, final ListView mListView, final DiscussArrayAdapter mAdapter, final Activity activity) {

        String str = mEditText1.getText().toString();
        MChat d = new MChat();
        d.date = Utils.curDate();
        d.text = str;
        final List<MChat> mChats = new ArrayList<>();
        mEditText1.setText("");
        new SenderChatMessagePOST().send(application, mSettings.url, d, mChats, new IActionE<Object>() {
            @Override
            public void action(Object v) {
                ISession ses = Configure.getSession();
                Utils.addChatMessages(mChats, ses, mAdapter, activity);
                int index = mAdapter.getCount();
                mListView.smoothScrollToPosition(index);

            }
        });

    }

    private void addItems() {

        List<MChat> list = Configure.getSession().getList(MChat.class, null);
        for (MChat mChat : list) {
            mAdapter.add(mChat);
        }
        int index = mAdapter.getCount();
        mListView.smoothScrollToPosition(index);

    }

    @Override
    public void onResume() {
        super.onResume();
        mView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null && data.getExtras() != null) {

                String settings = data.getExtras().getString(SettingsActivity.DEVICE_SETTINGS);
                SettingsKassa.core().settingsKassa = settings;
                SettingsKassa.save();
                Toast.makeText(getContext(), settings, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void updateChat() {
        //TODO завернуть в ловлю ошибок
        int count = mAdapter.getCount();
        List<MChat> mChats = Configure.getSession().getList(MChat.class, null);
        for (int i = 0; i < mChats.size(); i++) {
            if (i < count) {
                continue;
            }
            mAdapter.add(mChats.get(i));
        }
        mListView.post(new Runnable() {
            @Override
            public void run() {
                int index = mAdapter.getCount();
                mListView.smoothScrollToPosition(index);
            }
        });
    }

    public void refreshhButtonSynhrinize() {
        mSettings.setStart_dey(!mSettings.isStart_dey());
        pinterStartDayButton(btStartDey);

    }

    public void PRINTABLE_DAY(boolean b) {
        pinterStartDayButton(btStartDey);
    }
}
