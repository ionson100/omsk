package com.example.user.omsk.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;

import java.util.List;

/**
 * ion100 on 11.12.2017.
 */

public class DialogContextPointMenu extends DialogFragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DialogContextPointMenu.class);
    Settings mSettings;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mSettings = Settings.core();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.menu_context_point, null);


        v.findViewById(R.id.menu_new_point).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSettings.getVisit().point = new MPoint();
                Settings.showFragment(StateSystem.POINT_ADD_SEARCH, getActivity());
                dismiss();
            }
        });

        v.findViewById(R.id.menu_show_point).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.POINT, getActivity());
                dismiss();
            }
        });
        v.findViewById(R.id.menu_visit_point).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mSettings.getVisit().point == null) {
                    Toast.makeText(getActivity(), getString(R.string.error_not_select_point), Toast.LENGTH_SHORT).show();
                } else if (mSettings.getVisit().point.fuflo) {
                    Toast.makeText(getActivity(), getString(R.string.point_fiflo), Toast.LENGTH_SHORT).show();
                } else {
                    List<MVisitStory> stories = Configure.getSession().getList(MVisitStory.class, " point_id = '" + mSettings.getVisit().point.idu + "\'");
                    if (stories.size() >= 1) {
                        DialogSelectVisit dialogSelectVisit = new DialogSelectVisit();
                        dialogSelectVisit.initDialog(stories, new DialogSelectVisit.IActionSelectedVisit() {
                            @Override
                            public void action(MVisitStory mVisitStory) {
                                MVisit vs = mVisitStory.getMVisit(getActivity());
                                vs.secondary = true;
                                mSettings.setVisit(vs);
                                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                            }
                        });
                        dialogSelectVisit.show(getActivity().getSupportFragmentManager(), "sdkjjdddsd");
                        return;
                    } else {
                        Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                    }
                }
                dismiss();
            }
        });
        v.findViewById(R.id.menu_edit_point).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.showFragment(StateSystem.POINT_EDIT_SEARCH, getActivity());
                dismiss();
            }
        });

        v.findViewById(R.id.menu_alert_point).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        v.findViewById(R.id.menu_debt_point).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSettings.getVisit().point == null) {
                    Toast.makeText(getActivity(), getString(R.string.error_not_select_point), Toast.LENGTH_SHORT).show();
                } else {
                    List<MDebt> mDebtList = Utils.getDebtForPoint(mSettings.getVisit().point.idu);
                    if (mDebtList.size() == 0) {
                        Toast.makeText(getActivity(), R.string.qweqew, Toast.LENGTH_LONG).show();
                        dismiss();
                        return;
                    }
                    try {
                        DialogShowPointDebt pointDebt = new DialogShowPointDebt();
                        pointDebt.setDebtsPoint(mDebtList, mSettings.getVisit().point);
                        pointDebt.show(getActivity().getSupportFragmentManager(), "sdjdkjj");
                    } catch (Exception ignored) {

                        log.error(ignored);
                    }
                }
                dismiss();
            }
        });

        if (mSettings.getVisit() == null || mSettings.getVisit().point == null) {
            v.findViewById(R.id.point_panel).setVisibility(View.GONE);
        }


        builder.setView(v);


        return builder.create();
    }


}
