package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.example.user.omsk.senders.SenderGoogleAddressToGeoGET;
import com.example.user.omsk.senders.SenderGoogleGeoToAddressGET;

public class InnerDeterminerGPS implements LocationListener {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(InnerDeterminerGPS.class);
    private final SenderGoogleAddressToGeoGET.IActionAddressGeo myIAction;

    private LocationManager locationManager;
    private final Activity activity;

    public InnerDeterminerGPS(Activity activity, SenderGoogleAddressToGeoGET.IActionAddressGeo iAction) {
        this.myIAction = iAction;
        this.activity = activity;
        activate();
    }

    private void activate() {
        locationManager = (LocationManager) activity.getSystemService(activity.getBaseContext().LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
        }
        if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    public void destroy() {
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
            }
            locationManager.removeUpdates(this);
        }
    }

    private static final int COUNT = 5;
    private boolean isRun = true;
    private int i = COUNT;

    @Override
    public void onLocationChanged(Location location) {
        try {
            i++;
            if (isRun && i > COUNT) {
                isRun = false;
                //.......................
                new SenderGoogleGeoToAddressGET().send(location.getLatitude(), location.getLongitude(), new SenderGoogleAddressToGeoGET.IActionAddressGeo() {
                    @Override
                    public void action(String address, String latitude, String longitude) {
                        myIAction.action(address, latitude, longitude);

                    }
                }, activity);
            }

        } catch (Exception e) {
            log.error(e);

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
