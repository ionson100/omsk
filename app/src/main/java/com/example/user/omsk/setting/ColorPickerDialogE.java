package com.example.user.omsk.setting;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.content.DialogInterface;

import com.example.user.omsk.setting.colorpicker.ColorPickerView;
import com.example.user.omsk.setting.colorpicker.OnColorSelectedListener;
import com.example.user.omsk.setting.colorpicker.builder.ColorPickerClickListener;
import com.example.user.omsk.setting.colorpicker.builder.ColorPickerDialogBuilder;

class ColorPickerDialogE {

    private Context context;
    private IAction iAction;

    ColorPickerDialogE(Context context, IAction iAction){
        this.context = context;

        this.iAction = iAction;
    }

    public interface IAction{
        void Action(int color);
    }
    public void show(int color,String name) {

        ColorPickerDialogBuilder
                .with(context)
                .setTitle(name)
                .initialColor(color)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {}
                })
                .setPositiveButton("Сохранить", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        if(iAction!=null){
                            iAction.Action(selectedColor);
                        }
                    }
                })
                .setNegativeButton("Отказаться", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }
}
