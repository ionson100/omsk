package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.user.omsk.models.MProduct;

import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;

public class FVisitBalance extends Fragment {
    public static org.apache.log4j.Logger log = Logger.getLogger(FVisitBalance.class);
    private Settings mSettings;
    private ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_visit_balance, container, false);
        mSettings = Settings.core();
        try {
            if (mSettings.getVisit() == null) {
                Settings.showFragment(StateSystem.HOME, getActivity());
                return mView;
            }

            mListView = (ListView) mView.findViewById(R.id.listView_balance_stock);

            Button btAddProduct = (Button) mView.findViewById(R.id.bt_add_balance_product);
            btAddProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_BAlANCE, getActivity());
                }
            });
            Button btMenu = (Button) mView.findViewById(R.id.bt_to_visit_menu);
            btMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
                }
            });

            Transceiver.subscribe(TransceiveStaticVariables.F2323, new Transceiver.ITransceiver() {
                @Override
                public void action(Object o) {
                    if (o != null) {
                        deleteProduct(o.toString());
                    } else {
                        deleteProductAll();
                    }
                    activateList();
                }
            });
            mView.findViewById(R.id.bt_to_visit_balance_save).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSettings.getVisit().point.isRecomendationBalance) {

                        boolean d = new RecommendationBalance(mSettings).conpute();
                        if (d) {
                            mSettings.getVisit().point.isRecomendationBalance = false;
                        }
                    } else {
                        new RecommendationBalance(mSettings).deCompute();
                        mSettings.getVisit().point.isRecomendationBalance = true;
                    }
                    printableButtonText((Button) v);
                    activateList();
                    Settings.save();
                }
            });

            printableButtonText((Button) mView.findViewById(R.id.bt_to_visit_balance_save));

            activateList();
        } catch (Exception ex) {
            Utils.sendMessage("FVisitBalance:" + Utils.getErrorTrace(ex), (MyApplication) getActivity().getApplication());
            log.error(ex);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
            Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
        }
        return mView;
    }

    private void deleteProductAll() {
        mSettings.getVisit().selectProductForBalance.clear();
        Settings.save();
    }


    private void printableButtonText(Button v) {
        if (mSettings.getVisit().point.isRecomendationBalance) {
            v.setText(R.string.recommendations);
        } else {
            v.setText(R.string.recommendations_delete);
        }
    }

    private void activateList() {//

        Collections.sort(mSettings.getVisit().selectProductForBalance, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });

        ListAdapterForSelectedBalanceProduct mSelectedBalanceProduct =
                new ListAdapterForSelectedBalanceProduct((AppCompatActivity) getActivity(),

                        R.layout.item_list_product_selected_for_balanse, mSettings.getVisit().selectProductForBalance, new IActionE<View>() {
                    @Override
                    public void action(View v) {
                        for (int i = 0; i < mListView.getChildCount(); i++) {
                            LinearLayout ve = (LinearLayout) mListView.getChildAt(i);
                            TextView t1 = (TextView) ve.getChildAt(0);
                            TextView t2 = (TextView) ve.getChildAt(1);

                        }
                        //LinearLayout ve = (LinearLayout) v;
                      //  TextView t1 = (TextView) ve.getChildAt(0);
                      //  TextView t2 = (TextView) ve.getChildAt(1);

                    }
                });
        mListView.setAdapter(mSelectedBalanceProduct);
        mListView.setSelection(mSettings.getVisit().scroll_balance_list);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                mSettings.getVisit().scroll_balance_list = firstVisibleItem;
            }
        });
    }


    private void deleteProduct(String id) {
        MProduct del = null;
        for (MProduct product : mSettings.getVisit().selectProductForBalance) {
            if (product.idu.equals(id)) {
                del = product;
            }
        }
        if (del != null) {
            mSettings.getVisit().selectProductForBalance.remove(del);
            Settings.save();
        }
    }

    @Override
    public void onDestroy() {
        Transceiver.cancelSubscribe(TransceiveStaticVariables.F2323);
        super.onDestroy();
    }
}
