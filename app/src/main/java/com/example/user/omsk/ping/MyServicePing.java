package com.example.user.omsk.ping;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.net.InetAddress;
import java.util.Timer;
import java.util.TimerTask;

public class MyServicePing extends Service {
    private Timer mTimer;
    private Intent inte;
    private String ipping;

    public MyServicePing() {
        //ipping=Settings.core().ipPing;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        mTimer.cancel();
        super.onDestroy();
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {
        mTimer = new Timer();
        MyTimerTask mMyTimerTask = new MyTimerTask();
        mTimer.schedule(mMyTimerTask, 500, 30000);
        inte = new Intent("ping");
        return Service.START_NOT_STICKY;
    }

    private synchronized void updatePing() {
        try {

            if (ipping == null) try {
                InetAddress in = InetAddress.getByName("bs000.net");
                ipping = in.getHostAddress();
            } catch (Exception ex) {

            }
            if (ipping != null) {
                boolean d = PingE.ping(ipping);
                if (d) {
                    inte.putExtra("1", "1");
                } else {
                    inte.putExtra("1", "0");
                }
                sendBroadcast(inte);
            }


        } catch (Exception ignored) {
        }
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            updatePing();
        }
    }
}

