package com.example.user.omsk.route;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.Calendar;
import java.util.Date;

public class HorizontalNumberPicker extends LinearLayout {

    private final TextView mTextView;
    private final View mView;
    private final EditText mEditText;
    private IAction iAction;

    public HorizontalNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.horizontal_number_picker, this);
        mTextView = (TextView) mView.findViewById(R.id.route_visit_value);
        mEditText = (EditText) mView.findViewById(R.id.edit_text);
        mView.findViewById(R.id.btn_plus).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = getInt(mEditText.getText().toString());
                i = i + 1;
                mEditText.setText(String.valueOf(i));
                if (iAction != null) {
                    iAction.Action(i, getStartDate(i), getFinishDate(i));
                }
            }
        });
        mView.findViewById(R.id.btn_minus).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = getInt(mEditText.getText().toString());
                i = i - 1;
                if (i < 0) {
                    i = 0;
                }
                mEditText.setText(String.valueOf(i));
                if (iAction != null) {
                    iAction.Action(i, getStartDate(i), getFinishDate(i));
                }
            }
        });
    }

    public void setAction(IAction action) {
        this.iAction = action;
    }

    private int getInt(String str) {
        int res = 0;
        try {
            res = Integer.parseInt(str);
        } catch (Exception ignored) {

        }
        return res;
    }

    private Date getStartDate(int value) {

        Date date = Utils.curDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, value * -1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        mTextView.setText(Utils.simpleDateFormatForRouteE(cal.getTime()));
        return cal.getTime();
    }

    private Date getFinishDate(int value) {
        Date date = Utils.curDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, value * -1);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public interface IAction {
        void Action(int value, Date startDate, Date finishDate);
    }
}
