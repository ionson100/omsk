package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MPhoto;
import com.example.user.omsk.models.MVideo;
import com.example.user.omsk.senders.SenderPhotoHTTPSCLIENT;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.io.File;
import java.util.List;

public class WorkingFiles {

    private final Settings settings;

    public WorkingFiles(Settings settings) {
        this.settings = settings;
    }

    public void start(MyApplication application) {
        ISession session = Configure.getSession();
        List<MPhoto> mPhotos = Utils.getPhotos(session);
        for (MPhoto s : mPhotos) {
            File f = new File(s.path);
            if (f.exists() && f.length() > 0 && f.length() <= Utils.MAX_FILE_SIZE) {
                new SenderPhotoHTTPSCLIENT().send( s.path, s.point_id, s.visit_id, application);
            } else {
            }
        }
        List<MVideo> mVideos = Utils.getVideo(session);
        for (MVideo s : mVideos) {
            File f = new File(s.path);
            if (f.exists() && f.length() > 0 && f.length() <= Utils.MAX_FILE_SIZE) {
                new SenderPhotoHTTPSCLIENT().send( s.path, s.point_id, s.visit_id, application);
            } else {
            }
        }
    }
}
