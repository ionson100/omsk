package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.user.omsk.models.MChat;
import com.example.user.omsk.senders.SenderChatMessagePOST;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MyServiceChat extends Service {

    private Timer mTimer;
    String url;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int  onStartCommand(Intent intent, int flags, int startId) {
//
        url = Settings.core().url;
        mTimer = new Timer();
        MyTimerTask mMyTimerTask = new MyTimerTask();
        mTimer.schedule(mMyTimerTask, 1000, 60000);
        return Service.START_STICKY;
    }

    private synchronized void updateChat() {

        MChat d = new MChat();
        d.date = Utils.curDate();
        d.text = null;
        final List<MChat> mChats = new ArrayList<>();

        new SenderChatMessagePOST().send((MyApplication) getApplication(), url, d, mChats, new IActionE<Object>() {
            @Override
            public void action(Object v) {
                ISession ses = Configure.getSession();
                Utils.addChatMessages(mChats, ses, null, getBaseContext());
                Intent intent = new Intent("omsk_login");
                intent.putExtra(MainActivity.MainReceiver.KEY, MainActivity.MainReceiver.CHAT_VALUE);
                sendBroadcast(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        mTimer.cancel();
        mTimer = null;
        super.onDestroy();
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            updateChat();
        }
    }
}
