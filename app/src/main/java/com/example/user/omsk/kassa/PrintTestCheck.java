package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

public class PrintTestCheck {

    public static synchronized void print(final TextView log, final Activity activity, final Settings mSettings, final IActionE iActionE) {

        new AsyncTask<Void, String, Void>() {
            private Fptr fptr;
            private ProgressDialog dialog;
            private String errorString;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            private void printText(String text, int alignment, int wrap) throws Exception {
                if (fptr.put_Caption(text) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError();
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError();
                }
                if (fptr.PrintString() < 0) {
                    checkError();
                }
            }

            private void printText(String text) throws Exception {
                printText(text, IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
            }

            private void openCheck(int type) throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_CheckType(type) < 0) {
                    checkError();
                }
                if (fptr.OpenCheck() < 0) {
                    checkError();
                }
            }

            private void closeCheck(int typeClose) throws Exception {
                if (fptr.put_TypeClose(typeClose) < 0) {
                    checkError();
                }
                if (fptr.CloseCheck() < 0) {
                    checkError();
                }
            }

            private void registration(String name, double price, double quantity) throws Exception {
                if (fptr.put_Quantity(quantity) < 0) {
                    checkError();
                }
                if (fptr.put_Price(price) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                    checkError();
                }
                if (fptr.put_Name(name) < 0) {
                    checkError();
                }
                if (fptr.Registration() < 0) {
                    checkError();
                }
            }

            private void registrationFZ54(String name, double price, double quantity, int discountType,
                                          double discount, int taxNumber) throws Exception {

                if (fptr.put_TaxNumber(3) < 0) {
                    checkError();
                }
                if (fptr.put_Quantity(quantity) < 0) {
                    checkError();
                }
                if (fptr.put_Price(price) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                    checkError();
                }
                if (fptr.put_Name(name) < 0) {
                    checkError();
                }
                if (fptr.put_PositionSum(quantity * price) < 0) {
                    checkError();
                }
                if (fptr.Registration() < 0) {
                    checkError();
                }
            }

            private void payment(double sum, int type) throws Exception {
                if (fptr.put_Summ(sum) < 0) {
                    checkError();
                }
                if (fptr.put_TypeClose(type) < 0) {
                    checkError();
                }
                if (fptr.Payment() < 0) {
                    checkError();
                }
                publishProgress(String.format("Remainder: %.2f, Change: %.2f", fptr.get_Remainder(), fptr.get_Change()));
            }

            private void reportZ() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REPORT_CLEAR) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError();
                }
                ZetReport mZetReprot = new ZetReportBuilder(fptr).build();
                MZReport m = new MZReport(mZetReprot);
                if (fptr.Report() < 0) {
                    checkError();
                }
                Configure.getSession().insert(m);
                ZetReportBuilder.clearReport();
            }

            private void printFooter() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REPORT_NO_CLEAR) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.PrintFooter() < 0) {
                    checkError();
                }
            }

            private void printBarcode(int type, String barcode, double scale) throws Exception {
                if (fptr.put_Alignment(IFptr.ALIGNMENT_CENTER) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodeType(type) < 0) {
                    checkError();
                }
                if (fptr.put_Barcode(barcode) < 0) {
                    checkError();
                }
                if (type == IFptr.BARCODE_TYPE_QRCODE) {
                    if (fptr.put_Height(0) < 0) {
                        checkError();
                    }
                } else {
                    if (fptr.put_Height(50) < 0) {
                        checkError();
                    }
                }
                if (fptr.put_BarcodePrintType(IFptr.BARCODE_PRINTTYPE_AUTO) < 0) {
                    checkError();
                }
                if (fptr.put_PrintBarcodeText(false) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodeControlCode(true) < 0) {
                    checkError();
                }
                if (fptr.put_Scale(scale) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodeCorrection(0) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodeColumns(3) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodeRows(1) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodeProportions(50) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodeUseProportions(true) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodePackingMode(IFptr.BARCODE_PDF417_PACKING_MODE_DEFAULT) < 0) {
                    checkError();
                }
                if (fptr.put_BarcodePixelProportions(300) < 0) {
                    checkError();
                }
                if (fptr.PrintBarcode() < 0) {
                    checkError();
                }
            }

            private void discount(double sum, int type, int destination) throws Exception {
                if (fptr.put_Summ(sum) < 0) {
                    checkError();
                }
                if (fptr.put_DiscountType(type) < 0) {
                    checkError();
                }
                if (fptr.put_Destination(destination) < 0) {
                    checkError();
                }
                if (fptr.Discount() < 0) {
                    checkError();
                }
            }

            private void charge(double sum, int type, int destination) throws Exception {
                if (fptr.put_Summ(sum) < 0) {
                    checkError();
                }
                if (fptr.put_DiscountType(type) < 0) {
                    checkError();
                }
                if (fptr.put_Destination(destination) < 0) {
                    checkError();
                }
                if (fptr.Charge() < 0) {
                    checkError();
                }
            }

            @Override
            protected void onPreExecute() {
                dialog = Utils.factoryDialog(activity, "Печать тест чека", null);
                dialog.show();
                log.setText("");
            }

            protected void onPostExecute(Void aVoid) {

                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                iActionE.action(errorString);
            }

            @Override
            protected Void doInBackground(Void... params) {
               // Random random = new Random();
                fptr = new Fptr();
                try {
                    fptr.create(activity.getApplication());

                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }

                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");

                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    publishProgress("OK");

                    // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                    // не являются ошибками, если мы хотим просто отменить чек
                    publishProgress("Отмена чека...");
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }
                    publishProgress("OK");



                    if (true) {
                        if (fptr.put_UserPassword("00000030") < 0) {
                            checkError();
                        }

                        publishProgress("Печать строк");
                        publishProgress("Нефискальный чек...");
                        printText("Я сразу смазал карту будня, плеснувши краску из стакана;\n"
                                + "Я показал на блюде студня косые скулы океана.\n"
                                + "На чешуе жестяной рыбы прочел я зовы новых губ.", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText("А вы", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText("ноктюрн сыграть", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                        printText("могли бы", IFptr.ALIGNMENT_RIGHT, IFptr.WRAP_WORD);
                        printText("на флейте водосточных труб?", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        publishProgress("OK");

                        publishProgress("Печать ШК");
                        printText("\nEAN8", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_EAN8, "40182735", 100);
                        printText("");
                        printText("\nEAN13", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_EAN13, "4007630000116", 200);
                        printText("");
                        printText("\nUPCA", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_UPCA, "697929110035", 100);
                        printText("");
                        printText("\nCODE39", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_CODE39, "ATOL.RU", 100);
                        printText("");
                        printText("\nPDF417", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_PDF417, "1234567", 200);
                        printText("");
                        printText("\nQR", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_QRCODE, "АТОЛ.РУ", 500);
                        printText("");
                        printText("\nITF-14", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_ITF14, "00012345600012", 100);
                        printText("");
                        printText("\nInterleaved 2 of 5", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                        printBarcode(IFptr.BARCODE_TYPE_INTERLEAVED_2_OF_5, "04062300106659", 100);
                        printText("");
                        printFooter();
                        publishProgress("OK");
                    }
                } catch (Exception e) {
                    publishProgress(e.toString());
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - PrintTestCheck\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }
        }.execute();
    }
}
