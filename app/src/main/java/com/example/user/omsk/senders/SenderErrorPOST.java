package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;

import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Date;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

public class SenderErrorPOST {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderErrorPOST.class);
    private String msg;
    private Settings mSettings;
    private Context application;



    public void send(MyApplication application, String msg) {
        this.application = application;
        boolean res = false;
        this.msg = msg;
        this.mSettings = Settings.core();
        new SenderWorkerTack().executeOnExecutor(THREAD_POOL_EXECUTOR);
    }



    String er205 = null;

    private String sendCore(String json) {

        ErrorS errorS = new ErrorS();
        errorS.tel = "0000";
        errorS.date = Utils.curDate();
        errorS.message = json;
        errorS.guid = SettingsUser.core().getUuidDevice();
        Gson sd = Utils.getGson();
        json = sd.toJson(errorS);

        return UtilsSender.postRequest(application, Utils.HTTP + mSettings.url + "/error/", json, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {
            }
        });
    }

    private class SenderWorkerTack extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            return sendCore(msg);
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (aBoolean != null) {
               // Toast.makeText(application, er205, Toast.LENGTH_SHORT).show();
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/error/:  " + aBoolean;
                log.error(msg);
                //new SenderErrorPOST().send(application,msg,Settings.core());
            } else {
                Toast.makeText(application, "сообщение отправлено на сервер!", Toast.LENGTH_SHORT).show();
            }
        }
    }

static     class ErrorS implements Serializable {
        public String guid;
        public String message;
        public String tel;
        Date date = null;
    }
}
