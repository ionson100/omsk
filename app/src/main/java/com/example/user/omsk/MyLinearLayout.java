package com.example.user.omsk;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * ion100 USER on 07.12.2017.
 */

public class MyLinearLayout extends LinearLayout {
    public MyLinearLayout(Context context) {
        super(context);
    }

    public MyLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //int colore;


    @Override
    public void setPressed(boolean pressed) {
        super.setPressed(pressed);

        for (int i = 0; i < this.getChildCount(); i++) {
            View v = this.getChildAt(i);
            if (v instanceof TextView) {
                TextView d = (TextView) v;
                if (pressed) {
                    d.setBackgroundResource(R.color.color97);

                } else {
                    d.setBackgroundResource(R.color.colore3);

                }
            }
            if (v instanceof LinearLayout) {
                LinearLayout ll = (LinearLayout) v;
                if (pressed) {
                    ll.setBackgroundResource(R.color.color97);
                } else {
                    ll.setBackgroundResource(R.color.colore3);
                }
            }
        }
    }


}


