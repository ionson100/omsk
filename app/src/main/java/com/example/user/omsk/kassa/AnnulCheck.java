package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CACH;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CONSTRUCTOR;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_DEBT;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_RETURN;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_RETURN_CONSTRUCTOR;


public class AnnulCheck extends AppCompatActivity {
    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AnnulCheck.class);
    List<MVisitStory> listVisits;
    private ListView mListView;
    private Settings mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annul_check);
        try {
            findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            mListView = (ListView) findViewById(R.id.list_history_check);
            mSettings = Settings.core();

             listVisits = Configure.getSession().getList(MVisitStory.class, null);
            List<TempCheck> mChecksDatas = new ArrayList<>();


            String dd = "";
            if (mSettings.getVisit() != null) {
                dd = mSettings.getVisit().idu;
            }


            for (MVisitStory visitStory : listVisits) {

                if (visitStory.idu.equals(dd)) continue;
                for (MChecksData checksData : visitStory.getCheckList()) {
                    if (checksData.visit_id == null) continue;
                    TempCheck tempCheck = new TempCheck(visitStory, checksData);
                    mChecksDatas.add(tempCheck);
                }
            }


            if (mSettings.getVisit() != null && mSettings.getVisit().point != null) {
                MVisitStory mVisitStory = mSettings.getVisit().getVisitStory();
                for (MChecksData checksData : mSettings.getVisit().checksDataList) {
                    TempCheck tempCheck = new TempCheck(mVisitStory, checksData);
                    mChecksDatas.add(tempCheck);
                }
            }

            List<MChecks> mChecksList = Configure.getSession().getList(MChecks.class, null);
            for (MChecks mChecks : mChecksList) {

                mChecks.checkData.mChecksLink = mChecks;
                TempCheck tempCheck = new TempCheck(null, mChecks.checkData);
                mChecksDatas.add(tempCheck);
            }


            Collections.sort(mChecksDatas, new Comparator<TempCheck>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public int compare(TempCheck lhs, TempCheck rhs) {
                    return Integer.compare(lhs.mChecksData.checkNumber, rhs.mChecksData.checkNumber);
                }
            });


            ListAdapterForCheckData listAdapterForCheckData = new ListAdapterForCheckData(this, R.layout.item_check_list, mChecksDatas, findViewById(R.id.itog_check));
            mListView.setAdapter(listAdapterForCheckData);

        } catch (Exception ex) {
            log.error(ex);
            finish();
        }

    }


    public static class TempCheck {

        public MVisitStory mVisitStory;
        public MChecksData mChecksData;

        public TempCheck(MVisitStory m, MChecksData c) {
            this.mChecksData = c;
            this.mVisitStory = m;
        }
    }

    class ListAdapterForCheckData extends ArrayAdapter<TempCheck> {

        @NonNull
        private final Context context;
        private final int resource;
        @NonNull
        private final List<TempCheck> objects;
        Map<Integer, View> map = new HashMap<>();
        private View view;

        public ListAdapterForCheckData(@NonNull Context context, @LayoutRes int resource, @NonNull List<TempCheck> objects, View view) {
            super(context, resource, objects);
            this.context = context;
            this.resource = resource;
            this.objects = objects;
            this.view = view;
            this.createItog();
        }

        private void createItog() {
            Pupper count_check = (Pupper) view.findViewById(R.id.check_itogo);
            Pupper prihod_check = (Pupper) view.findViewById(R.id.check_prihod);
            Pupper rachod_check = (Pupper) view.findViewById(R.id.check_rashod);
            Pupper cach = (Pupper) view.findViewById(R.id.check_cach);

            int count = 0;
            double prihod = 0, rashod = 0;
            Set<String> strings = new HashSet<>();
            for (TempCheck object : objects) {
                if (object == null) continue;
                if (strings.contains(object.mChecksData.idu)) continue;
                count = count + 1;
                if (object.mChecksData.type_check == TYPE_CHECK_CONSTRUCTOR ||
                        object.mChecksData.type_check == TYPE_CHECK_CACH ||
                        object.mChecksData.type_check == TYPE_CHECK_DEBT) {
                    prihod = prihod + object.mChecksData.totalPrice;
                }
                if (object.mChecksData.type_check == TYPE_CHECK_RETURN_CONSTRUCTOR ||
                        object.mChecksData.type_check == TYPE_CHECK_RETURN) {
                    rashod = rashod + object.mChecksData.totalPrice;
                }


            }
            count_check.setPairString("Итого чеков:", String.valueOf(count) + " шт.");
            prihod_check.setPairString("Приход:", String.valueOf(Utils.getStringDecimal(prihod) + " руб."));
            rachod_check.setPairString("Расход:", String.valueOf(Utils.getStringDecimal(rashod) + " руб."));
            cach.setPairString("Касса:", String.valueOf(Utils.getStringDecimal(prihod - rashod) + " руб."));
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View mView = convertView;

            final TempCheck p = getItem(position);


            if (p != null) {
                if (map.containsKey(p.mChecksData.checkNumber)) {
                    mView = map.get(p.mChecksData.checkNumber);
                } else {
                    mView = createViev(p, position);
                    map.put(p.mChecksData.checkNumber, mView);
                }

            }


            return mView;
        }

        @NonNull
        private View createViev(final TempCheck p, final int position) {
            View mView;
            mView = LayoutInflater.from(getContext()).inflate(resource, null);

            final TextView logTextView = (TextView) mView.findViewById(R.id.log);

            Button showButton = (Button) mView.findViewById(R.id.bt_show_check);
            final Button printDoubleButton = (Button) mView.findViewById(R.id.bt_print_duble);
            final Button deleteButton = (Button) mView.findViewById(R.id.bt_delete_check);

            Pupper check_number = (Pupper) mView.findViewById(R.id.check_number);
            final Pupper date = (Pupper) mView.findViewById(R.id.date);
            final Pupper date_delete = (Pupper) mView.findViewById(R.id.date_delete);
            Pupper doc_number = (Pupper) mView.findViewById(R.id.doc_number);
            Pupper point_name = (Pupper) mView.findViewById(R.id.point_name);
            Pupper total_amount = (Pupper) mView.findViewById(R.id.total_amount);
            Pupper total_price = (Pupper) mView.findViewById(R.id.total_price);
            Pupper type_order = (Pupper) mView.findViewById(R.id.type_order);
            Pupper debt_date = (Pupper) mView.findViewById(R.id.debt_date);
            Pupper relation = (Pupper) mView.findViewById(R.id.bt_delete_check_relation);


            Pupper sms = (Pupper) mView.findViewById(R.id.check_sms);
            Pupper email = (Pupper) mView.findViewById(R.id.check_email);
            Pupper notyfy = (Pupper) mView.findViewById(R.id.check_notyfy);

            if (p.mChecksData.notify > 0) {
                if (p.mChecksData.sms != null) {
                    sms.setVisibility(View.VISIBLE);
                    sms.setPairString("СМС номер:", p.mChecksData.sms);
                }

                if (p.mChecksData.email != null) {
                    email.setVisibility(View.VISIBLE);
                    email.setPairString("Емайл адрес:", p.mChecksData.email);
                }
                notyfy.setVisibility(View.VISIBLE);
                String d = " по смс";
                if (p.mChecksData.notify == 2) {
                    d = "по email";
                }
                notyfy.setPairString("Уведомление покупателя :", d);
            }


            if (p.mChecksData.is_zreport) {
                mView.setBackgroundResource(R.color.eacaca);
                printDoubleButton.setVisibility(View.GONE);
                deleteButton.setVisibility(View.GONE);
            }
            if (p.mChecksData.isReturn) {
                deleteButton.setVisibility(View.GONE);
                printDoubleButton.setVisibility(View.GONE);
            }
            if (p.mChecksData.type_check == TYPE_CHECK_RETURN || p.mChecksData.type_check == TYPE_CHECK_RETURN_CONSTRUCTOR) {
                point_name.setVisibility(View.GONE);
                total_price.setVisibility(View.GONE);
                total_amount.setVisibility(View.GONE);
                showButton.setVisibility(View.GONE);
                printDoubleButton.setVisibility(View.GONE);
                deleteButton.setVisibility(View.GONE);
                relation.setVisibility(View.VISIBLE);
                relation.setPairString("Отмена чека №:", String.valueOf(p.mChecksData.checkNumberDelete));
                mView.setBackgroundResource(R.color.colorc8);
            }


            final List<MProductBase> finalMProducts = p.mChecksData.productList;
            showButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogShowDataCheck dataCheck = new DialogShowDataCheck();
                    dataCheck.setmProductList(finalMProducts);
                    dataCheck.show(getSupportFragmentManager(), "skadjdjasd");
                }
            });


            printDoubleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PrintDoubeCheck.print(mSettings, logTextView, AnnulCheck.this, p);
                }
            });


            final View finalMView = mView;
            final Pupper finalDate_delete = date_delete;

            final boolean[] run = {true};

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public synchronized void onClick(View v) {
                    if (run[0] == false) return;
                    run[0] = false;


                    List<TempCheck> sd = Linq.toStream(objects).where(new Predicate<TempCheck>() {
                        @Override
                        public boolean apply(TempCheck t) {
                            return p.mChecksData.id_partial != null && t.mChecksData.id_partial != null && t.mChecksData.id_partial.equals(p.mChecksData.id_partial);
                        }
                    }).toList();


                    if (sd.size() > 1) {
                        deletMove();
                    } else {
                        deleteOne();
                    }
                }

                private void deletMove() {

                    final Set<String> strings = new HashSet<String>();
                    final List<AnnulCheck.TempCheck> tempChecks = Utils.getTempCheck(p.mChecksData.id_partial);
                    Utils.messageBox(getString(R.string.warning), getString(R.string.SAASASAS), AnnulCheck.this, new IActionE() {
                        @Override
                        public void action(Object o) {
                            PrintDeleteCoreCheckPartial.deleteReturn(mSettings, AnnulCheck.this, tempChecks, new IActionE() {
                                @Override
                                public void action(Object o) {

                                    List<MChecksData> checksDatas = (List<MChecksData>) o;

                                    List<MVisitStory> mVisitStories =  listVisits;
                                    for (MChecksData checksData : checksDatas) {
                                        if (checksData.errorText != null) {
                                            Utils.messageBox(getString(R.string.error), checksData.errorText, AnnulCheck.this, null);
                               //             final boolean[] run = {true};
                                        } else {

                                            for (MVisitStory mVisitStory : mVisitStories) {
                                                for (MChecksData data : mVisitStory.getCheckList()) {
                                                    data.date_delete = Utils.curDate();
                                                    data.date = data.date_delete;
                                                    if (data.idu.equals(checksData.checkUUIDDelete)) {
                                                        data.isReturn = true;
                                                        data.checkNumberDelete = checksData.checkNumber;
                                                        checksData.visit_id = mVisitStory.idu;
                                                        strings.add(mVisitStory.idu);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (mSettings.getVisit() != null) {
                                        for (final MChecksData checksData : checksDatas) {
                                            MChecksData s = Linq.toStream(mSettings.getVisit().checksDataList).firstOrDefault(new Predicate<MChecksData>() {
                                                @Override
                                                public boolean apply(MChecksData t) {
                                                    return t.idu.equals(checksData.checkUUIDDelete);
                                                }
                                            });
                                            if (s != null) {
                                                s.isReturn = true;
                                                mSettings.getVisit().checksDataList.add(checksData);

                                            }
                                        }
                                        Settings.save();
                                    }

                                    for (final String string : strings) {
                                        final MVisitStory story = Linq.toStream(mVisitStories).firstOrDefault(new Predicate<MVisitStory>() {
                                            @Override
                                            public boolean apply(MVisitStory t) {
                                                return t.idu.equals(string);
                                            }
                                        });

                                        if (story != null) {
                                            List<MChecksData> s = Linq.toStream(checksDatas).where(new Predicate<MChecksData>() {
                                                @Override
                                                public boolean apply(MChecksData t) {
                                                    return t.visit_id.equals(story.idu);
                                                }
                                            }).toList();

                                            story.getCheckList().addAll(s);
                                            story.setNewObject(true);
                                            Configure.getSession().update(story);
                                        }
                                    }


                                    sendBroadcast(new Intent(MainActivity.FAB));
                                    finish();

                                }
                            });

                        }
                    });

                }

                private void deleteOne() {
                    Utils.messageBoxE(AnnulCheck.this, "Аннулирование чека", "Аннулировать чек?", "Закрыть", "Удалить", new IActionE<View>() {
                        @Override
                        public void action(View v) {
                            run[0] = true;
                        }
                    }, new IActionE<View>() {
                        @Override
                        public void action(View v) {
                            PrintDeleteCheck.delete(mSettings, logTextView, AnnulCheck.this, p, new IActionE() {
                                @Override
                                public void action(Object o) {
                                    MChecksData checksData = (MChecksData) o;
                                    if (checksData.errorText != null) {
                                        Utils.messageBox(getString(R.string.error), checksData.errorText, AnnulCheck.this, null);
                                      //  final boolean[] run = {true};
                                    } else {
                                        p.mChecksData.isLast = false;
                                        p.mChecksData.isReturn = true;
                                        p.mChecksData.date_delete = Utils.curDate();
                                        finalMView.setBackgroundResource(R.color.d5caed);
                                        printDoubleButton.setVisibility(View.GONE);
                                        deleteButton.setVisibility(View.GONE);
                                        finalDate_delete.setVisibility(View.VISIBLE);
                                        finalDate_delete.setPairString(getString(R.string.adsad), Utils.simpleDateFormatE(p.mChecksData.date_delete));
                                        if (p.mChecksData.type_check == TYPE_CHECK_CONSTRUCTOR) {
                                            checksData.type_check = TYPE_CHECK_RETURN_CONSTRUCTOR;
                                        } else {
                                            checksData.type_check = TYPE_CHECK_RETURN;
                                            checksData.visit_id = p.mVisitStory.idu;
                                        }
                                        checksData.checkNumberDelete = p.mChecksData.checkNumber;
                                        checksData.checkUUIDDelete = p.mChecksData.idu;
                                        if (mSettings.getVisit() != null && mSettings.getVisit().idu != null) {

                                            if (p.mVisitStory.idu.equals(mSettings.getVisit().idu)) {


                                                mSettings.getVisit().checksDataList.add(checksData);
                                                MVisitStory mVisitStory = mSettings.getVisit().getVisitStory();
                                                mVisitStory.setNewObject(true);
                                                p.mVisitStory = mVisitStory;

                                                if (mVisitStory.id == 0) {
                                                    Configure.getSession().insert(p.mVisitStory);
                                                } else {
                                                    Configure.getSession().update(p.mVisitStory);
                                                }
                                                mSettings.getVisit().isNotSender = true;

                                            }
                                        } else {
                                            if (p.mChecksData.type_check == TYPE_CHECK_CONSTRUCTOR) {

                                                MChecks mChecks = new MChecks();
                                                mChecks.date = Utils.curDate();
                                                mChecks.checkData = checksData;
                                                Configure.getSession().insert(mChecks);
                                            } else {
                                                p.mVisitStory.getCheckList().add(checksData);
                                                p.mVisitStory.setNewObject(true);
                                                Configure.getSession().update(p.mVisitStory);
                                            }

                                        }

                                        TempCheck tempCheck = new TempCheck(p.mVisitStory, checksData);

                                        if (p.mChecksData.mChecksLink != null) {
                                            Configure.getSession().update(p.mChecksData.mChecksLink);
                                        }


                                        ListAdapterForCheckData.this.add(tempCheck);


                                        sendBroadcast(new Intent(MainActivity.FAB));

                                       // final boolean[] run = {true};
                                        createItog();
                                    }
                                }
                            });
                        }
                    }, true);
                }
            });


            check_number.setPairString(R.string.wewewe, String.valueOf(p.mChecksData.checkNumber));
            date.setPairString(getString(R.string.sdduusd), Utils.simpleDateFormatE(p.mChecksData.date));

            if (p.mChecksData.date_delete!=null) {
                date_delete.setVisibility(View.VISIBLE);
                date_delete.setPairString(getString(R.string.adsad), Utils.simpleDateFormatE(p.mChecksData.date_delete));
            }

            doc_number.setPairString(getString(R.string.sdjsyd), String.valueOf(p.mChecksData.docNumber));
            String pn = getString(R.string.rtetret);
            if (p.mVisitStory != null) {
                MPoint point = Configure.getSession().get(MPoint.class, p.mVisitStory.point_id);

                if (point != null) {
                    pn = point.name;
                }
            } else {
                pn = getString(R.string.uiusadd);
            }

            point_name.setPairString(getString(R.string.point), pn);

            double totalAmount = p.mChecksData.totalAmount;
            double totalPrice = p.mChecksData.totalPrice;

            total_amount.setPairString(R.string.total_amount, Utils.getStringDecimal(totalAmount));
            total_price.setPairString(R.string.djshdh, Utils.getStringDecimal(totalPrice));

            pn = getString(R.string.safaf);
            if (p.mChecksData.type_check == TYPE_CHECK_CONSTRUCTOR) {
                pn = getString(R.string.sapodqw);
            }

            if (p.mChecksData.type_check == TYPE_CHECK_RETURN_CONSTRUCTOR) {
                pn = getString(R.string.sapodqwretu);
            }
            if (p.mChecksData.type_check == TYPE_CHECK_CACH) {
                pn = getString(R.string.sapod);
            }
            if (p.mChecksData.type_check == TYPE_CHECK_DEBT) {
                pn = getString(R.string.asdeasdasd);

                debt_date.setVisibility(View.VISIBLE);
                Date d=Utils.intToDate(p.mChecksData.debt_data);
                debt_date.setPairString(getString(R.string.asldklasd), Utils.simpleDateFormatE(d));

            }
            if (p.mChecksData.type_check == TYPE_CHECK_RETURN || p.mChecksData.type_check == TYPE_CHECK_RETURN_CONSTRUCTOR) {
                pn = getString(R.string.sldjasd);
            }
            type_order.setPairString(getString(R.string.sdasdasd), pn);

            if (p.mChecksData.isReturn) {
                mView.setBackgroundResource(R.color.d5caed);
            }
            return mView;
        }
    }
}
