package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.LoggerE;
//import com.example.user.omsk.models.MError;
import com.example.user.omsk.models.MRoute;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.Gson;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class SenderUpdateRouteFromServer {
    public static org.apache.log4j.Logger log = Logger.getLogger(SenderUpdateRouteFromServer.class);
    private Settings mSettings;
    private Activity activity;
    private IActionE iAction;

    public void send( Activity activity, IActionE iAction) {
        this.mSettings = Settings.core();
        this.activity = activity;
        this.iAction = iAction;
        new SenderJobSetRouter().execute();
    }

    private String getParams(int date) {
        return "[\"" + SettingsUser.core().getUuidDevice() + "\"," + String.valueOf(date) + ",\"" + mSettings.currentVersionName + "\",\"" + "0000" + "\"]";
    }

    private class SenderJobSetRouter extends AsyncTask<String, Void, String> {

        ProgressDialog dialog;


        @Override
        protected void onPreExecute() {
            if (!Utils.isNetworkAvailable(activity, mSettings)) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, activity.getString(R.string.get_data_from_server), null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (dialog != null) {
                dialog.cancel();
            }
            if (aBoolean != null && iAction != null) {
                iAction.action(null);
            } else {
                Toast.makeText(activity, aBoolean, Toast.LENGTH_SHORT).show();

                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/synchronization_route_client/:  " + aBoolean;
                log.error(msg);
               // Configure.getSession().insert(new MError(msg));
                //new SenderErrorPOST().send(activity, msg, Settings.core());
            }

        }

        @Override
        protected String doInBackground(String... params) {

            if (isCancelled()) return null;
            final String[] res = {null};
            String message = getParams(0);
            LoggerE.logI(message);

            res[0] = UtilsSender.postRequest(activity.getApplication(), Utils.HTTP + mSettings.url + "/synchronization_route_client/", message, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {

                    App(str);

                }
            });
            return res[0];

        }

        void App(String str) {
            LoggerE.logI(str);
            Gson sd3 = Utils.getGson();
            tempRuoute res = sd3.fromJson(str, tempRuoute.class);
            ISession ses = Configure.getSession();

            ses.deleteTable("route");
            for (String s : res.route) {
                MRoute d = new MRoute();
                d.point_id = s;
                ses.insert(d);
            }
        }
    }

    class tempRuoute {
        public final List<String> route = new ArrayList<>();
    }
}
