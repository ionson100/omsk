package com.example.user.omsk.visitcontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogEditCountProduct;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.models.MFreePresentTYpes;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MStockRows;
import com.example.user.omsk.R;
import com.example.user.omsk.RecommendationSale;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;
import com.example.user.omsk.kassa.UtilsKassa;
import com.example.user.omsk.orm2.Configure;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VisitSale extends LinearLayout {

    public MainActivity mainActivity;
    public IActionE<Object> iActionE;
    private List<MStockRows> mStockRowsList;
    private Settings mSettings;
    private LinearLayout mListView;

    public VisitSale(Context context) {
        super(context);
        init();
    }

    public VisitSale(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VisitSale(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mSettings = Settings.core();
        mStockRowsList = Configure.getSession().getList(MStockRows.class, " point_id = ? ", mSettings.getVisit().point.idu);
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = vi.inflate(R.layout.visit_control_sale, null);


        mView.findViewById(R.id.bt_context_menu_sale).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                mainActivity.openContextMenu(VisitSale.this);
                // VisitSale.this.showContextMenu();
            }
        });


        mListView = (LinearLayout) mView.findViewById(R.id.list_selectted_product);
        activateListProduct();
        this.addView(mView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));


//        final MChecksData d = Linq.toStream(mSettings.getVisit().checksDataList).firstOrDefault(new Predicate<MChecksData>() {
//            @Override
//            public boolean apply(MChecksData t) {
//                return t.type_check == MChecksData.TYPE_CHECK_CACH;
//            }
//        });


        final boolean chectyes = Utils.getCheckSale(mSettings.getVisit().checksDataList);

        final boolean finalChectyes = chectyes;
        this.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {


                                                @Override
                                                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                                                    if (chectyes) {
                                                        menu.add(R.string.askjkjasd);
                                                    } else {
                                                        String name = "";
                                                        if (mSettings.getVisit().point.isRecomendationSale) {
                                                            name = getContext().getString(R.string.recommendations);
                                                        } else {
                                                            name = getContext().getString(R.string.recommendations_delete2);
                                                        }


                                                        menu.add(name).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

                                                            @Override
                                                            public boolean onMenuItemClick(MenuItem item) {

                                                                if (mSettings.getVisit().point.isRecomendationSale) {
                                                                    if (validateRecomendation()) {

                                                                        boolean d = new RecommendationSale(mSettings, mainActivity).compute();

                                                                        if (d) {
                                                                            mSettings.getVisit().point.isRecomendationSale = false;
                                                                        }
                                                                    }
                                                                } else {
                                                                    new RecommendationSale(mSettings, mainActivity).deConpute();
                                                                    mSettings.getVisit().point.isRecomendationSale = true;
                                                                }
                                                                Settings.save();

                                                                activateListProduct();
                                                                return true;
                                                            }
                                                        });
                                                        menu.add(R.string.add_product).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                                                            @Override
                                                            public boolean onMenuItemClick(MenuItem item) {

                                                                Settings.showFragment(StateSystem.VISIT_SHOW_STOCK_SELECT_PRODUCT_FOR_SALE, mainActivity);
                                                                return false;
                                                            }
                                                        });
                                                    }


                                                }
                                            }

        );


        if (chectyes) {
            this.setEnabled(false);
            mView.findViewById(R.id.eror_sale_check).setVisibility(VISIBLE);
        }


    }

    private boolean validateRecomendation() {
        if (mStockRowsList.size() == 0) {
            Utils.messageBox(getContext().getString(R.string.recommendations), getContext().getString(R.string.error_recomendation1), mainActivity, null);
            return false;
        } else {
            if (mStockRowsList.get(0).date != mSettings.getVisit().point.lastDateVisit) {
                Utils.messageBox(getContext().getString(R.string.recommendations), getContext().getString(R.string.error_recomendation1), mainActivity, null);
                return false;
            }
        }
        return true;
    }

    private void activateListProduct() {

        mListView.removeAllViews();

        Collections.sort(mSettings.getVisit().selectProductForSale, new Comparator<MProduct>() {
            @Override
            public int compare(MProduct lhs, MProduct rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        for (final MProduct mProduct : mSettings.getVisit().selectProductForSale) {

            View v = LayoutInflater.from(getContext()).inflate(R.layout.item_list_product_selected_for_sale_new_page, null);
            final TextView tt1 = (TextView) v.findViewById(R.id.name_poduct);
            final TextView tt2 = (TextView) v.findViewById(R.id.price_poduct);
            final TextView tt3 = (TextView) v.findViewById(R.id.count_poduct);
            final TextView tt4 = (TextView) v.findViewById(R.id.total_price_poduct);
            if (mSettings.getVisit() != null) {
                if (mProduct.amountSelfCore() - mProduct.getAmount_core() < 0) {
                    tt1.setBackgroundColor(mSettings.colorTextStock);
                    tt2.setBackgroundColor(mSettings.colorTextStock);
                    tt3.setBackgroundColor(mSettings.colorTextStock);
                    tt4.setBackgroundColor(mSettings.colorTextStock);
                }
            }
            if (tt1 != null) {
                String name = mProduct.name;
                if (mProduct.rule_id != null) {
                    MFreePresentTYpes d = Configure.getSession().get(MFreePresentTYpes.class, mProduct.rule_id);
                    if (d == null) {
                        d=new MFreePresentTYpes();
                        d.name = "не найдена";
                    }
                    name = name + "\n" + d.name;
                }

                tt1.setText(name);
            }
            if (tt2 != null) {
                tt2.setText(mProduct.getPrice());
            }
            if (tt3 != null) {
                tt3.setText(Utils.getStringDecimal(mProduct.getAmount_core()));
            }
            if (tt4 != null) {
                tt4.setText(Utils.getStringDecimal(mProduct.getTotalPrice()));
            }

            if (UtilsKassa.getMCheckDataCache(mSettings.getVisit().checksDataList) == null) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        tt1.setBackgroundResource(R.color.colore97);
                        tt2.setBackgroundResource(R.color.colore97);
                        tt3.setBackgroundResource(R.color.colore97);
                        tt4.setBackgroundResource(R.color.colore97);
                        DialogEditCountProduct product = new DialogEditCountProduct();
                        product.iActionEForSale = new IActionE<Object>() {
                            @Override
                            public void action(Object o) {
                                activateListProduct();
                            }
                        };
                        product.setProduct(mProduct);
                        product.setDismis(new IActionE<Object>() {
                            @Override
                            public void action(Object o) {
                                if (mSettings.getVisit() != null) {
                                    if (mProduct.amountSelfCore() - mProduct.getAmount_core() < 0) {
                                        tt1.setBackgroundColor(mSettings.colorTextStock);
                                        tt2.setBackgroundColor(mSettings.colorTextStock);
                                        tt3.setBackgroundColor(mSettings.colorTextStock);
                                        tt4.setBackgroundColor(mSettings.colorTextStock);
                                    } else {
                                        tt1.setBackgroundResource(R.color.colore3);
                                        tt2.setBackgroundResource(R.color.colore3);
                                        tt3.setBackgroundResource(R.color.colore3);
                                        tt4.setBackgroundResource(R.color.colore3);
                                    }
                                }
                            }
                        });
                        product.show(mainActivity.getSupportFragmentManager(), "edsfsdfsdf");
                    }
                });
            }
            mListView.addView(v);
        }
        if (iActionE != null) {
            iActionE.action(null);
        }
    }

}
