package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsGpsMap;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.DebtDuplet;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisitResults;
import com.example.user.omsk.models.MVisitStoryBase;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.pupper.Pupper;
import com.example.user.omsk.report.ItemView;

import java.util.Date;
import java.util.List;

public class DialogShowDataVisit extends DialogFragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DialogShowDataVisit.class);
    private IActionE mIActionE;
    private MVisitStoryBase mStoryBase;
    private Context context;
    private List<MProduct> mProductList;
    private boolean mIsStoryCore;
    private boolean mStoryNotCore;
    private Activity mActimvity;
    private View view;

    public void activateView(View view) {

        this.view = view;
    }

    public void setIActionsE(IActionE iActionE) {
        this.mIActionE = iActionE;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mIActionE != null) {
            mIActionE.action(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        return builder.create();
    }

    public static View getItemStoryVisit(View mView, final MVisitStoryBase storyBase, final Context context, List<MProduct> mProductList,
                                         boolean isStoryCore, boolean storyNotCore, final Activity activity) {
        //List<MVisitStory> visitStor = Configure.getSession().getList(MVisitStory.class, null);

        try {
            String mOrderTypeDebt = Configure.getSession().getList(MOrderType.class, " index1 = 1 ").get(0).idu;
            double totalDebt = Utils.getDebt(storyBase.debtList);
            for (DebtDuplet debtDuplet : storyBase.getDupletProductList()) {
                totalDebt = totalDebt + debtDuplet.amount;
            }
            if (totalDebt > 0) {
                Pupper total_debt = (Pupper) mView.findViewById(R.id.total_debt);
                total_debt.setVisibility(View.VISIBLE);
                total_debt.setPairString(context.getString(R.string.itogo_refund), Utils.getStringDecimal(totalDebt));
            }

            if (isStoryCore) {

            } else {
                if (storyBase.order_type_id != null && storyBase.order_type_id.equals(mOrderTypeDebt) && storyBase.status) {
                    Pupper status = (Pupper) mView.findViewById(R.id.status_refunt);
                    status.setPairString(context.getString(R.string.asasddasd), "");
                    status.setVisibility(View.VISIBLE);
                }
            }

            ItemView.addDupletDebt(storyBase, context, mView);

            ItemView.addDebtList(storyBase, context, mView);

            ImageView button_gps = (ImageView) mView.findViewById(R.id.fix_gps);
            if (storyBase.latitude == 0 || storyBase.longitude == 0) {
                button_gps.setVisibility(View.GONE);
            } else {
                button_gps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MPoint point = storyBase.getPoint();
                        if (point.id != 0) {
                            SettingsGpsMap gpsMap = SettingsGpsMap.core();
                            gpsMap.fix_latitude = storyBase.latitude;
                            gpsMap.fix_longitude = storyBase.longitude;
                            gpsMap.point_latitude = point.latitude;
                            gpsMap.point_longitude = point.longitude;
                            gpsMap.point_name = point.name;
                            gpsMap.stateSystem = Settings.getStateSystem();
                            SettingsGpsMap.save();
                            Settings.showFragment(StateSystem.MAP_ROUTE_STORY, (Activity) context);
                        }
                    }
                });
            }

            ItemView.addSaleProduct(storyBase, context, mView);

            ItemView.addBalanceProduct(storyBase, context, mProductList, mView);

            ItemView.addFreeProduct(storyBase, context, mView);

            ItemView.addActionProduct(storyBase, context, mView);

            ItemView.addNewOrder(storyBase, context, mView);

            ItemView.addPresent(storyBase, context, mView);

            if (storyBase.getFreeProductList().size() > 0 || storyBase.getActionProductList().size() > 0) {
                Pupper member = (Pupper) mView.findViewById(R.id.member_name);
                member.setVisibility(View.VISIBLE);
                member.setPairString(context.getString(R.string.member_pupper), storyBase.member_action);
            }
            //////////////////////////////////////////////////////////////////////////////////
            TextView namePoint = (TextView) mView.findViewById(R.id.visit_story_name_point);
            TextView nameTrader = (TextView) mView.findViewById(R.id.visit_story_trader);
            if (storyBase.trader != null) {
                nameTrader.setVisibility(View.VISIBLE);
                nameTrader.setText(storyBase.trader);
            }

            if (storyBase.concurent_actions != null && storyBase.concurent_actions.trim().length() > 0) {
                Pupper concurentAction = (Pupper) mView.findViewById(R.id.concurent_action);
                concurentAction.setVisibility(View.VISIBLE);
                concurentAction.setPairString(context.getString(R.string.concurent_actions), storyBase.concurent_actions);

            }


            Pupper description = (Pupper) mView.findViewById(R.id.visit_description);
            description.setPairString(R.string.commentary, storyBase.comment);


            Pupper startDate = (Pupper) mView.findViewById(R.id.visit_story_start_date);
            Date d_start=Utils.intToDate(storyBase.start);
            startDate.setPairString(R.string.visit_start, Utils.simpleDateFormatE(d_start));

            Pupper finisDate = (Pupper) mView.findViewById(R.id.visit_story_finish_date);
            Date d_finish=Utils.intToDate(storyBase.finish);
            finisDate.setPairString(R.string.finish_visit, Utils.simpleDateFormatE(d_finish));

            Pupper amountProduct = (Pupper) mView.findViewById(R.id.visit_story_amount_poduct);

            Pupper itogo = (Pupper) mView.findViewById(R.id.visit_story_itogo);

            MVisitResults types = Configure.getSession().get(MVisitResults.class, storyBase.visit_result_id);
            Pupper visitResult = (Pupper) mView.findViewById(R.id.visit_story_visit_result);

            visitResult.setPairString(R.string.visit_result_titl, types.name);

            Pupper visitOrder = (Pupper) mView.findViewById(R.id.visit_story_visit_order);
            Pupper manyArm = (Pupper) mView.findViewById(R.id.visit_story_visit_price_hadler);
            manyArm.setVisibility(View.GONE);

            Pupper debitState = (Pupper) mView.findViewById(R.id.debt1);


            debitState.setPairString(R.string.debt_text, Utils.getStringDecimal(storyBase.getPoint().debt));

            Pupper debitReturn = (Pupper) mView.findViewById(R.id.debt2);

            if (storyBase.getPoint().debt == 0) {
                debitState.setVisibility(View.GONE);
            }

            double resDebt = Utils.getDebt(storyBase.debtList);


            debitReturn.setPairString(R.string.return_debt, Utils.getStringDecimal(resDebt));
            if (resDebt == 0) {
                debitReturn.setVisibility(View.GONE);
            }

            MPoint point = storyBase.getPoint();
            namePoint.setText(point.name);
            double count = 0;
            double itogoP = 0;

            for (MProduct mSaleRowsStory : storyBase.getSalesProductList()) {
                count = count + mSaleRowsStory.getAmount_core();
                itogoP = itogoP + (mSaleRowsStory.getAmount_core() * mSaleRowsStory.price);
            }

            if (types.index != 0) {
                amountProduct.setPairString(R.string.itogo_count_products1, Utils.getStringDecimal(count));
                if (storyBase.order_type_id != null && storyBase.order_type_id.equals(Configure.getSession().getList(MOrderType.class, "index1 = 0").get(0).idu)) {
                    itogo.setPairString(R.string.itogo_price3, Utils.getStringDecimal(itogoP));
                } else {
                    itogo.setPairString(R.string.itogo_price43, Utils.getStringDecimal(itogoP));
                }

                MOrderType type = Configure.getSession().get(MOrderType.class, storyBase.order_type_id);
                if (type == null) {
                    visitOrder.setPairString(R.string.visit_order_type, "не найден");

                } else {
                    visitOrder.setPairString(R.string.visit_order_type, type.name);
                }


            } else {
                visitOrder.setVisibility(View.GONE);
                manyArm.setVisibility(View.GONE);
                amountProduct.setVisibility(View.GONE);
                itogo.setVisibility(View.GONE);
            }

            /////////////////////////// пробитые чекиж

            Pupper check_current = (Pupper) mView.findViewById(R.id.check_current);
            Pupper check_debt = (Pupper) mView.findViewById(R.id.check_debt);

            StringBuilder stringBuilder = new StringBuilder("");
            for (MChecksData checksData : storyBase.getCheckList()) {
                if (checksData.type_check == MChecksData.TYPE_CHECK_CACH && !checksData.isReturn) {
                    check_current.setPairString(context.getString(R.string.asasss), Utils.getStringDecimal(checksData.totalPrice) + " руб.");
                    check_current.setVisibility(View.VISIBLE);
                }
                if (checksData.type_check == MChecksData.TYPE_CHECK_DEBT && !checksData.isReturn) {
                    Date d=Utils.intToDate(checksData.debt_data);
                    stringBuilder.append("Долг за ").
                            append(Utils.simpleDateFormatForRouteE(d)).
                            append(" ").
                            append(Utils.getStringDecimal(checksData.totalPrice)).
                            append(" руб.\n");
                }
            }
            if (stringBuilder.length() > 0) {
                check_debt.setVisibility(View.VISIBLE);
                check_debt.setPairString(context.getString(R.string.asasf), stringBuilder.toString());
                check_debt.getTitul().setGravity(Gravity.TOP);
                check_debt.getValue().setGravity(Gravity.LEFT);
            }

        } catch (Exception ex) {

            log.error(ex);
            mView = LayoutInflater.from(context).inflate(R.layout.item_error, null);
            StackTraceElement[] stackTraceElements = ex.getStackTrace();
            StringBuilder builder = new StringBuilder();
            builder.append("Exception: ").append(ex.getClass().getName()).append("\n")
                    .append("Message: ").append(ex.getMessage()).append("\nStacktrace:\n");
            for (StackTraceElement element : stackTraceElements) {
                builder.append("\t").append(element.toString()).append("\n");
            }
            TextView messageError = (TextView) mView.findViewById(R.id.error_message);
            messageError.setText(builder.toString());

            TextView titul = (TextView) mView.findViewById(R.id.titul_error);
            titul.setText("Ошибка обработки: " + storyBase.getPoint().name);
        }

        mView.setTag(storyBase.getPoint());
        return mView;

    }


}
