package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.omsk.senders.UtilsSender;

import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;

//import com.example.user.omsk.models.MError;

public class FError extends Fragment {

    public static org.apache.log4j.Logger log = Logger.getLogger(FError.class);

    private ListView mListView;

    public FError() {

    }

    private List<String> list;

    private Settings mSettings;

    private void loaderList() throws Exception {
        list = Utils.getListError();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(FError.this.getContext(),
                android.R.layout.simple_list_item_1, list);
        mListView.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mSettings = Settings.core();
        View mView = inflater.inflate(R.layout.fragment_error, container, false);
        try {
            mListView = (ListView) mView.findViewById(R.id.history_errors);
            loaderList();
            mView.findViewById(R.id.bt_sent_toserver).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        UtilsSender.sendJournalError(mSettings, (MyApplication) getActivity().getApplication(), Utils.getListErrorFromSender());
                    } catch (Exception e) {
                        log.error(e);
                    }
                }
            });

            mView.findViewById(R.id.bt_clear_error_list).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File f = new File(MyApplication.logfile);
                    if (f.delete()) {

                        try {
                            loaderList();
                        } catch (Exception e) {
                            log.error(e);
                        }
                        Toast.makeText(getActivity(), R.string.filedeleted, Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } catch (Exception ex) {
            log.error(ex);
            Utils.sendMessage("FError:" + Utils.getErrorTrace(ex),  (MyApplication) getActivity().getApplication());
            if (MainActivity.isStateVisitIsVisit()) {
                Settings.showFragment(StateSystem.VISIT_HOME, getActivity());
            } else {
                Settings.showFragment(StateSystem.HOME, getActivity());
            }
        }
        return mView;
    }


}
