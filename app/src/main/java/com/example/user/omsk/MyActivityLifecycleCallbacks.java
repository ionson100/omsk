package com.example.user.omsk;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * ion100 on 14.12.2017.
 */

class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        action();
    }

    @Override
    public void onActivityStarted(Activity activity) {
        action();
    }

    @Override
    public void onActivityResumed(Activity activity) {
        action();
    }

    @Override
    public void onActivityPaused(Activity activity) {
        action();
    }

    @Override
    public void onActivityStopped(Activity activity) {
        action();
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        action();
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (activity.getClass() == MainActivity.class) {
            if (activity.isFinishing() == false) {
                activity.finish();
            }
        }
        action();
    }

    void action() {

    }
}
