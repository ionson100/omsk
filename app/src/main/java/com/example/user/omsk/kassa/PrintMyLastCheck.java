package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CACH;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CONSTRUCTOR;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_DEBT;
import static com.example.user.omsk.kassa.UtilsKassa.getStringDecimalE;

class PrintMyLastCheck {
    public static void printLast(final Settings mSettings, final Activity activity, final TextView log, final AnnulCheck.TempCheck tempCheck) {
        new AsyncTask<Void, String, Void>() {
            private IFptr fptr;
            private ProgressDialog dialog;
            private String errorText;
            private String textAfter;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            private void printText(String text, int alignment, int wrap) throws Exception {
                if (fptr.put_Caption(text) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError();
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError();
                }
                if (fptr.PrintString() < 0) {
                    checkError();
                }
            }

            private void printTextItogo(String text, int alignment, int wrap) throws Exception {


                if (fptr.put_FontDblHeight(true) < 0) {
                    checkError();
                }
                if (fptr.put_FontDblWidth(true) < 0) {
                    checkError();
                }
                if (fptr.put_FontBold(true) < 0) {
                    checkError();
                }
                if (fptr.put_Caption(text) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError();
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError();
                }
                fptr.AddTextField();

                fptr.PrintFormattedText();

            }

            @Override
            protected Void doInBackground(Void... params) {
               // Random random = new Random();
                fptr = new Fptr();
                try {
                    fptr.create(activity.getApplication());
                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                    // не являются ошибками, если мы хотим просто отменить чек
                    publishProgress("Отмена чека...");
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }
                    publishProgress("OK");
                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError();
                    }
                    printText("ДУБЛИКАТ ДОКУМЕНТА", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText("КАССОВЫЙ ЧЕК", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText("ПРИХОД", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
//                    String ss = "";
//                    if (tempCheck.mChecksData.type_check == TYPE_CHECK_CACH) {
//                        ss = activity.getString(R.string.asee);
//                    }
//                    if (tempCheck.mChecksData.type_check == TYPE_CHECK_DEBT) {
//                        ss = activity.getString(R.string.lad);
//                    }
//                    if (tempCheck.mChecksData.type_check == TYPE_CHECK_CONSTRUCTOR) {
//                        ss = activity.getString(R.string.sad);
//                    }
//                    printText(ss, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    List<MProductBase> mProductList = new ArrayList<>();
                    if (tempCheck.mChecksData.type_check == TYPE_CHECK_CACH) {
                        mProductList = new ArrayList<MProductBase>(tempCheck.mVisitStory.getSalesProductList());
                    }
                    if (tempCheck.mChecksData.type_check == TYPE_CHECK_CONSTRUCTOR) {
                        mProductList = tempCheck.mChecksData.productList;
                    }
                    if (tempCheck.mChecksData.type_check == TYPE_CHECK_DEBT) {
                        ISession session = Configure.getSession();
                        List<MDebt> mDebtList = session.getList(MDebt.class, " visit_id = ? ", tempCheck.mChecksData.order_debt_id);
                        for (MDebt debt : mDebtList) {
                            MProduct product = session.get(MProduct.class, debt.product_id);
                            if (product == null) continue;
                            product.setAmountCore(debt.amount);
                            product.price = debt.price;
                            mProductList.add(product);
                        }
                    }
                    Collections.sort(mProductList, new Comparator<MProductBase>() {
                        @Override
                        public int compare(MProductBase lhs, MProductBase rhs) {
                            return lhs.nameKKT.compareTo(rhs.nameKKT);
                        }
                    });
                    double total = 0;
                    double totalNds = 0;
                    for (MProductBase product : mProductList) {
                        printText(product.nameKKT, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText(getStringDecimalE(product.getAmount_core()) + "х" + getStringDecimalE(product.price)
                                + " =" + getStringDecimalE(product.getAmount_core() * product.price), IFptr.ALIGNMENT_RIGHT, IFptr.WRAP_WORD);
                        total = total + product.getAmount_core() * product.price;
                        double nds = ((product.getAmount_core() * product.price) / 100) * 18;
                        totalNds = totalNds + nds;
                        printText(" Сумма НДС 18%      =" + getStringDecimalE(nds), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    }
                    printTextItogo("ИТОГ =" + getStringDecimalE(total), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("ОПЛАТА", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(" НАЛИЧНЫМИ          =" + getStringDecimalE(total), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("ВСЕГО ОПЛАЧЕНО", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("НАЛИЧНЫМИ           =" + getStringDecimalE(total), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("ЭЛЕКТРОННЫМИ        =0.00", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Сумма НДС           =" + getStringDecimalE(totalNds), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Кассир:" + SettingsUser.core().getUserName(), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Торговая точка:", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    //ss = " не определена";
                    // MPoint dd = Configure.getSession().get(MPoint.class, tempCheck.mVisitStory.point_id);
                    // if (dd != null) {
                    //     ss = dd.name;
                    //  }
                    // printText(" " + ss, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(Utils.simpleDateFormatE(tempCheck.mChecksData.date), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Номер чека:" + tempCheck.mChecksData.checkNumber, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Номер  KKM:" + tempCheck.mChecksData.kmmNumber, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    publishProgress("OK");
                    publishProgress("OK");

                } catch (Exception e) {
                    publishProgress(e.toString());
                } finally {
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                log.setText("");
                dialog = Utils.factoryDialog(activity, "Печать копии последнего чека.", null);
                dialog.show();
                textAfter = log.getText().toString();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                if (errorText != null) {
                    Utils.messageBox(activity.getString(R.string.error), errorText, activity, null);
                } else {
                    log.setText(textAfter);
                }
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }
        }.execute();
    }
}
