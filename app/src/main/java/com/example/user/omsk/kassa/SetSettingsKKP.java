package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SetSettingsKKP {
    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(SetSettingsKKP.class);
    public static void set(final TextView log, final Activity activity, final Settings mSettings, final IActionE iActionE) {

        new AsyncTask<Void, String, Void>() {
            String mErrorString;
            private Fptr fptr;
            final ProgressDialog[] mDialog = new ProgressDialog[1];
            final List<String> mStringList = new ArrayList<>();

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected void onPreExecute() {
                mDialog[0] = Utils.factoryDialog(activity, "Изменение имени кассира", null);
                mDialog[0].show();
                log.setText("");
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                if (mDialog[0] != null) {
                    mDialog[0].cancel();
                    mDialog[0] = null;
                }
                if (mErrorString != null) {
                    Utils.messageBox(activity.getString(R.string.error), mErrorString, activity, null);
                } else {
                    Utils.messageBox(activity.getString(R.string.ok), activity.getString(R.string.sdfdfdf), activity, null);
                    log.setText("");
                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                fptr = new Fptr();

                try {
                    fptr.create(activity);
                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) { // загрузка настроек
                        checkError();
                    }
                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {// устанока соединения
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    publishProgress("Ввод паспорта...");
                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Установка программирования...");
                    if (fptr.put_Mode(4) < 0) {
                        checkError();
                    }
                    if (fptr.SetMode() < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Сохранение состояния...");
                    if (fptr.put_ValuePurpose(337) < 0) {// печать штрих кода
                        checkError();
                    }
                    if (fptr.put_Value(1) < 0) {
                        checkError();
                    }
                    if (fptr.SetValue() < 0) {
                        checkError();
                    }
                    publishProgress("Печатать штрихкод...");
                    if (fptr.put_ValuePurpose(57) < 0) {
                        checkError();
                    }
                    if (fptr.put_Value(1) < 0) {// печать сквозного номера документа
                        checkError();
                    }
                    if (fptr.SetValue() < 0) {
                        checkError();
                    }
                    if (fptr.put_ValuePurpose(83) < 0) {
                        checkError();
                    }
                    if (fptr.put_Value(3) < 0) {// печать при помощи PrintString
                        checkError();
                    }
                    if (fptr.SetValue() < 0) {
                        checkError();
                    }
                    publishProgress("порт офд...");
                    ////////////////////////////////////порт офд

                    if (fptr.put_ValuePurpose(301) < 0) {
                        checkError();
                    }
                    // if (Utils.OFD) {
                    if (fptr.put_Value(mSettings.ofd_port) < 0) {
                        checkError();
                    }
//                    } else {
//                        if (fptr.put_Value(19081) < 0) {
//                            checkError();
//                        }
//                    }

                    if (fptr.SetValue() < 0) {
                        checkError();
                    }
////////////////////////////////////////////////////////////////////////////////////////////////
                    publishProgress("wifi офд...");
                    if (fptr.put_ValuePurpose(302) < 0) {
                        checkError();
                    }
                    if (fptr.put_Value(3) < 0) {// wifi
                        checkError();
                    }
                    if (fptr.SetValue() < 0) {
                        checkError();
                    }
                    ////

                    if (fptr.put_ValuePurpose(277) < 0) {// повторная печать
                        checkError();
                    }
                    if (fptr.put_Value(1) < 0) {
                        checkError();
                    }
                    if (fptr.SetValue() < 0) {
                        checkError();
                    }

                    publishProgress("Производить инкасацию");
                    /////////////////// производить инкасацияю



                    if (fptr.put_ValuePurpose(55) < 0) {
                        checkError();
                    }
                    if (fptr.put_Value(1) < 0) {
                        checkError();
                    }
                    if (fptr.SetValue() < 0) {
                        checkError();
                    }







                    //////////////////////////

                    Date dat = Utils.curDate();

                    if (fptr.put_Time(dat) < 0) {
                        checkError();
                    }
                    int dd1 = fptr.SetTime();
                    if (dd1 == -3893) {
                        dd1 = fptr.SetTime();
                    }
                    if (dd1 < 0) {
                        checkError();
                    }
                    publishProgress("Set Time");
                    ////////////////////

                    if (fptr.put_Date(Utils.curDate()) < 0) {
                        checkError();
                    }
                    int dd = fptr.SetDate();
                    if (dd == -3893) {
                        dd = fptr.SetDate();
                    }
                    if (dd < 0) {
                        checkError();
                    }
                    publishProgress("Set Date");
                    ////////////////////////// dns ofd
                    publishProgress("dns офд...");
                    if (fptr.put_Caption("0.0.0.0") < 0) {
                        checkError();
                    }
                    if (fptr.put_CaptionPurpose(257) < 0) {
                        checkError();
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError();
                    }
                    ////////////////////////// url ofd

                    publishProgress("url офд...");
                    //if (Utils.OFD) {
                    if (fptr.put_Caption(mSettings.ofd_url) < 0) {
                        checkError();
                    }
//                    } else {
//                        if (fptr.put_Caption("185.170.204.85") < 0) {
//                            checkError();
//                        }
//                    }

                    if (fptr.put_CaptionPurpose(256) < 0) {
                        checkError();
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError();
                    }


                    ////////////////////////////////


                    if (fptr.put_Caption(SettingsUser.core().getUserName()) < 0) {
                        checkError();
                    }
                    if (fptr.put_CaptionPurpose(118) < 0) {
                        checkError();
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError();
                    }
                    publishProgress("save username...");
                    if (fptr.put_Caption(" ") < 0) {
                        checkError();
                    }
                    if (fptr.put_CaptionPurpose(72) < 0) {
                        checkError();
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError();
                    }
                    ////////////////////////////////////////////////////////////////
                    publishProgress("нах титул1");
                    if (fptr.put_Caption(" ") < 0) {
                        checkError();
                    }
                    if (fptr.put_CaptionPurpose(73) < 0) {
                        checkError();
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError();
                    }
                    publishProgress("нах титул2");
                    ///////////////////////////////////////
                    if (fptr.put_Caption(" ") < 0) {
                        checkError();
                    }
                    if (fptr.put_CaptionPurpose(69) < 0) {
                        checkError();
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError();
                    }
                    publishProgress("нах титул3");
                    if (fptr.put_Caption(" ") < 0) {
                        checkError();
                    }
                    if (fptr.put_CaptionPurpose(70) < 0) {
                        checkError();
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError();
                    }
                    publishProgress("нах тит4");

                } catch (Exception e) {
                    logE.error(e);
                    mErrorString = e.getMessage();
                    publishProgress(e.getMessage());
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - SetSettingsKKP\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }

        }.execute();
    }
}
