package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;

import com.example.user.omsk.auto.AutoOpenActivity;
import com.example.user.omsk.auto.MAuto;
import com.example.user.omsk.orm2.Configure;

import java.util.List;

/**
 * Прверка действия на открытый день
 * Created by USER on 11.07.2016.
 */
public class ValidateDay {
    public static boolean validate(Settings mSettings, Activity activity) {


        if (!SettingsUser.core().isAutorise()) {
            StartLoginActivity.start(activity);
            return false;
        }


        if (mSettings.isStart_dey()) {
            if (mSettings.useAuto) {


                List<MAuto> mAutos = Configure.getSession().getList(MAuto.class, " uid = ? and date1 = 0", mSettings.uid_auto);
                if (mAutos.size() > 0) {
                    if (mAutos.get(0).date1==null && mAutos.get(0).uid.equals(mSettings.uid_auto)) {
                        activity.startActivityForResult(new Intent(activity, AutoOpenActivity.class), AutoOpenActivity.RESULT);
                        return false;
                    }
                }
                return true;
            }
            return true;
        } else {
            String string = activity.getString(R.string.error_select_contragent);

            Utils.messageBox(activity.getString(R.string.error_label), string, activity, null);
            return false;
        }
    }
}
