package com.example.user.omsk.kassa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;

/**
 * ion100 on 02.10.2017.
 */

public class DialogPassword extends DialogFragment {


    private EditText editText;
    public IActionE<Object> objectIActionE;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_password, null);
        builder.setView(mView);
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        editText = (EditText) mView.findViewById(R.id.edit_password);
        mView.findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dd = editText.getText().toString();
                if (dd.equals(Settings.core().password)) {
                    if (objectIActionE != null) {
                        objectIActionE.action(null);
                    }
                    dismiss();
                }

            }
        });

        return builder.create();
    }

}
