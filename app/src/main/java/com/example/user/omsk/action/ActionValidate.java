package com.example.user.omsk.action;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.user.omsk.MyApplication;

import com.example.user.omsk.models.DeripaskeJs;
import com.example.user.omsk.models.MAction;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MOrderType;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MVisit;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.StateSystem;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;
import com.google.gson.annotations.SerializedName;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.NativeObject;
import org.mozilla.javascript.Scriptable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ActionValidate {

     static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ActionValidate.class);
    private static class TempFree {
        String product_id;
        double amount;
        double price;
        String permit_id;
        String sku;
        String name;
        double max_price;
    }

    private static class TempSale {
        String product_id;
        double amount;
        double price;
        String sku;
        String name;
        double max_price;
    }

    private static class TempProd {
        String product_id;
        String sku;
        String name;
        double max_price;
    }

    private static class TempVisit {
        public int curvisit;
        public int date ;
        @SerializedName("sales")
        List<TempSale> tempSales = new ArrayList<>();
        @SerializedName("actions")
        List<TempFree> tempAction = new ArrayList<>();
    }

    private static class TempParams {
        @SerializedName("promo_id")
        String promo_id;
        @SerializedName("product_list")
        List<TempProd> tempProdList = new ArrayList<>();
        @SerializedName("visit_list")
        List<TempVisit> tempVisits = new ArrayList<>();
    }


    static class TempProduct {
        public List<MProduct> mProductList = new ArrayList<>();
    }

    public static List<MAction> validate(MVisit mVisit, Activity activity) {

        TempParams tempParams = getTempParams(mVisit);

        Collections.sort(tempParams.tempVisits, new Comparator<TempVisit>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(TempVisit lhs, TempVisit rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });


        List<MAction> actionList = Configure.getSession().getList(MAction.class, null);
        List<MAction> res = new ArrayList<>();

        for (MAction mAction : actionList) {
            if (mAction.finish_action_new_22.getTime() < Utils.intToDate(Settings.core().getVisit().getStart()).getTime())
                continue;
            tempParams.promo_id = mAction.idu;
            double amount_acum = 0;
            for (TempFree action : tempParams.tempVisits.get(tempParams.tempVisits.size() - 1).tempAction) {
                if (action.permit_id.equals(mAction.idu)) {
                    amount_acum = amount_acum + action.amount;
                }
            }
            try {
                String json = Utils.getGson().toJson(tempParams);
                String ff = mAction.javascript;
                if (ff == null || ff.length() < 10) continue;
                Object[] params = new Object[]{json};
                Context rhino = Context.enter();
                rhino.setOptimizationLevel(-1);
                Scriptable scope = rhino.initStandardObjects();
                rhino.evaluateString(scope, ff, "JavaScript", 1, null);
                Object obj = scope.get("get", scope);

                if (obj instanceof Function) {
                    Function jsFunction = (Function) obj;
                    Object jsResult = jsFunction.call(rhino, scope, scope, params);
                    org.mozilla.javascript.NativeObject cc = (NativeObject) jsResult;

                    if (!cc.containsKey("amount")) continue;

                    double amount = (double) cc.get("amount");
                    org.mozilla.javascript.NativeObject xxx = (NativeObject) cc.get("products");

                    Map<String, Double> maper = new HashMap<>();

                    for (Object o : xxx.keySet()) {
                        Double dd = (Double) xxx.get(o);
                        if (dd == 0) continue;
                        maper.put(o.toString(), dd);
                    }
                    List<String> list = new ArrayList<>(maper.keySet());
                    DeripaskeJs deripaskeJs = new DeripaskeJs();
                    deripaskeJs.amount = amount;
                    deripaskeJs.amount_max = amount;//+ amount_acum;
                    deripaskeJs.products = list;
                    deripaskeJs.map = maper;
                    mAction.currentDeripaskeJs = deripaskeJs;
                    res.add(mAction);
                }
            } catch (Exception ex) {

                log.error(ex);
                new SenderErrorPOST().send((MyApplication) activity.getApplication(), mAction.name + ": " + ex.getMessage());
            } finally {
                Context.exit();
            }
        }
        return res;
    }

    @NonNull
    private static TempParams getTempParams(MVisit mVisit) {
        List<MProduct> mProducts =  Configure.getSession().getList(MProduct.class, " archive = 0 ");//

        TempParams tempParams = new TempParams();
        tempParams.tempVisits = new ArrayList<>();

        List<com.example.user.omsk.plan.TempProduct> tempProductList = Configure.getSession().
                getList(com.example.user.omsk.plan.TempProduct.class, " point_id = '" + mVisit.point.idu + "'");

        Collections.sort(tempProductList, new Comparator<com.example.user.omsk.plan.TempProduct>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(com.example.user.omsk.plan.TempProduct lhs, com.example.user.omsk.plan.TempProduct rhs) {
                return Integer.compare(lhs.visit_date,rhs.visit_date);

            }
        });

        Map<Integer, TempVisit> map = new HashMap<>();

        for (final com.example.user.omsk.plan.TempProduct tp : tempProductList) {
            MProduct p = Linq.toStream(mProducts).firstOrDefault(new Predicate<MProduct>() {
                @Override
                public boolean apply(MProduct t) {
                    return t.idu.equals(tp.product_id);
                }
            });
            if (p == null) continue;

            if (map.containsKey(tp.visit_date)) {

                TempVisit tempVisit = map.get(tp.visit_date);
                if (tp.permit_id == null) {
                    TempSale sale = new TempSale();
                    sale.amount = tp.amount;
                    sale.max_price = p.max_price;
                    sale.name = p.name;
                    sale.price = tp.price;
                    sale.sku = p.sku;
                    sale.product_id = p.idu;

                    tempVisit.tempSales.add(sale);
                } else {
                    TempFree free = new TempFree();
                    free.amount = tp.amount;
                    free.max_price = p.max_price;
                    free.name = p.name;
                    free.permit_id = tp.permit_id;
                    free.price = tp.price;
                    free.product_id = p.get_id();
                    free.sku = p.sku;

                    tempVisit.tempAction.add(free);
                }
            } else {
                TempVisit tempVisit = new TempVisit();
                tempVisit.date = tp.visit_date;
                if (tp.permit_id == null) {
                    TempSale sale = new TempSale();
                    sale.amount = tp.amount;
                    sale.max_price = p.max_price;
                    sale.name = p.name;
                    sale.price = tp.price;
                    sale.sku = p.sku;
                    sale.product_id = p.idu;

                    tempVisit.tempSales.add(sale);
                } else {
                    TempFree free = new TempFree();
                    free.amount = tp.amount;
                    free.max_price = p.max_price;
                    free.name = p.name;
                    free.permit_id = tp.permit_id;
                    free.price = tp.price;
                    free.product_id = p.get_id();
                    free.sku = p.sku;

                    tempVisit.tempAction.add(free);
                }
                map.put(tp.visit_date, tempVisit);

            }
        }

        List<TempVisit> lista = new ArrayList<>();

        for (Map.Entry<Integer, TempVisit> ss : map.entrySet()) {
            lista.add(ss.getValue());
        }

        Collections.sort(lista, new Comparator<TempVisit>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(TempVisit lhs, TempVisit rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });

        tempParams.tempVisits = lista;


        tempParams.tempProdList = new ArrayList<>();
        for (MProduct pp : mProducts) {
            TempProd tempProd = new TempProd();
            tempProd.product_id = pp.idu;
            tempProd.sku = pp.sku;
            tempProd.name = pp.name;
            tempProd.max_price = pp.max_price;
            tempParams.tempProdList.add(tempProd);
        }

        TempVisit currentTempVisit = new TempVisit();
        currentTempVisit.curvisit=1;
        currentTempVisit.date = mVisit.getStart();
        currentTempVisit.tempSales = new ArrayList<>();


        Map<String, MProduct> productMap = new HashMap<>();
        ISession ses = Configure.getSession();


        if (Settings.core().getVisit().order_type_id.equals(Configure.getSession().getList(MOrderType.class, " index1 = 0 ").get(0).idu)) {
            productMap = addSaleProducts(mVisit);
            addDebtProducts(productMap, ses);
        }
        if (Settings.core().getVisit().order_type_id.equals(Configure.getSession().getList(MOrderType.class, " index1 = 1 ").get(0).idu)) {
            addDebtProducts(productMap, ses);
        }

        for (Map.Entry<String, MProduct> ss : productMap.entrySet()) {
            MProduct product = ss.getValue();
            TempSale tempSale = new TempSale();
            tempSale.sku = product.sku;
            tempSale.amount = product.getAmount_core();
            tempSale.product_id = product.idu;
            tempSale.price = product.price;
            tempSale.name = product.name;
            tempSale.max_price = product.max_price;
            currentTempVisit.tempSales.add(tempSale);
        }


        currentTempVisit.tempAction = new ArrayList<>();
        tempParams.tempVisits.add(currentTempVisit);

        for (MProduct ap : mVisit.selectProductForActions) {
            TempFree f = new TempFree();
            f.sku = ap.sku;
            f.price = ap.price;
            f.permit_id = ap.rule_id;
            f.product_id = ap.idu;
            f.amount = ap.getAmount_core();
            if (ap != null) {
                f.name = ap.name;
                f.max_price = ap.max_price;
            }
            currentTempVisit.tempAction.add(f);
        }

        List<MVisitStory> mVisitStories =  ses.getList(MVisitStory.class, null);
        for (MVisitStory mv : mVisitStories) {

            if (mv.idu.equals(mVisit.idu)) continue;
            if (!mv.point_id.equals(mVisit.point.idu)) continue;


            TempVisit visit = new TempVisit();
            visit.date = mv.finish;

            for (MProduct mProduct : mv.getSalesProductList()) {

                TempSale sale = new TempSale();
                sale.amount = mProduct.getAmount_core();
                sale.max_price = mProduct.max_price;
                sale.name = mProduct.name;
                sale.price = mProduct.price;
                sale.product_id = mProduct.idu;
                sale.sku = mProduct.sku;
                visit.tempSales.add(sale);
            }


            for (MProduct ap : mv.getActionProductList()) {
                TempFree f = new TempFree();
                f.sku = ap.sku;
                f.price = ap.price;
                f.permit_id = ap.rule_id;
                f.product_id = ap.idu;
                f.amount = ap.getAmount_core();
                if (ap != null) {
                    f.name = ap.name;
                    f.max_price = ap.max_price;
                }
                visit.tempAction.add(f);
            }

            tempParams.tempVisits.add(visit);

        }
        return tempParams;
    }

    private static void addDebtProducts(Map<String, MProduct> productMap, ISession ses) {
        for (String s : Settings.core().getVisit().debtList) {
            List<MDebt> mDebtList = ses.getList(MDebt.class, " visit_id = ? ", s);
            for (MDebt mDebt : mDebtList) {
                MProduct product = ses.get(MProduct.class, mDebt.product_id);
                if (product == null) continue;
                product.setAmountCore(mDebt.amount);

                if (productMap.containsKey(product.idu)) {
                    double amount_core = productMap.get(product.idu).getAmount_core();

                    MProduct product1 = productMap.get(product.idu);
                    MProduct aa = Configure.getSession().get(MProduct.class, product.idu);
                    aa.setAmountCore(amount_core + product.getAmount_core());
                    productMap.put(product.idu, aa);

                } else {
                    productMap.put(product.idu, product);
                }
            }
        }
    }

    @NonNull
    private static Map<String, MProduct> addSaleProducts(MVisit mVisit) {
        Map<String, MProduct> productMap = new HashMap<>();
        for (MProduct product : mVisit.selectProductForSale) {
            if (productMap.containsKey(product.idu)) {
                double amount_core = productMap.get(product.idu).getAmount_core();
                productMap.get(product.idu).setAmountCore(amount_core + product.getAmount_core());
            } else {
                productMap.put(product.idu, product);
            }
        }
        return productMap;
    }

    static void showmenAction(LinearLayout mLayout, final List<MAction> mActionList, final Activity activity) {

        if (mActionList.size() % 2 != 0) {
            mActionList.add(new MAction());
        }
        for (int i = 0; i < mActionList.size(); i = i + 2) {
            View v = LayoutInflater.from(activity).inflate(R.layout.item_action_part, null);
            Button button1 = (Button) v.findViewById(R.id.bt1);
            Button button2 = (Button) v.findViewById(R.id.bt2);
            final MAction action1 = mActionList.get(i);
            MAction action2 = mActionList.get(i + 1);
            button1.setText(action1.name);
            if (action2._id == 0) {
                button2.setVisibility(View.INVISIBLE);
            } else {
                button2.setText(action2.name);
            }
            final int finalI = i;
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionCore(mActionList.get(finalI), activity);
                }
            });
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionCore(mActionList.get(finalI + 1), activity);
                }
            });

            mLayout.addView(v);
        }
    }

    private static void actionCore(MAction mAction, Activity activity) {
        SettingsStorageAction.core().mAction_new = mAction;
        SettingsStorageAction.save();
        Settings.showFragment(StateSystem.VISIT_STOCK_ACTION, activity);
    }
}

