package com.example.user.omsk.freesql;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;

public class SettingSql implements Serializable {
    public String sql = "";

    public static SettingSql core() {
        return (SettingSql) Reanimator.get(SettingSql.class);
    }

    public static void save() {
        Reanimator.save(SettingSql.class);
    }
}
