package com.example.user.omsk.models;

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

/**
 * Created by USER on 16.07.2018.
 */

@Table("summ_kkm")
public class MSummaKKM {

    @PrimaryKey("id")
    public int id;

    @Column("sumkkm")
    public double sumkkm;
}
