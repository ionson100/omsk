package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// последние продажи ( получаю с сервера)
public class MLastSale {

    @SerializedName("date")
    public Date date = null;

    @SerializedName("rows")
    public List<MSaleRows> mSaleRowsList = new ArrayList<>();
}
