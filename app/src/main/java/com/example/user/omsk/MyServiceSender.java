package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import org.apache.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;
//import com.example.user.omsk.Senders.ManualSenderSale;

public class MyServiceSender extends Service {
    public static org.apache.log4j.Logger log = Logger.getLogger(MyServiceSender.class);
    public MyServiceSender() {
    }

    Settings mSettings;
    private Timer mTimer;
    // boolean isOpt = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSettings = Settings.core();
        mTimer = new Timer();
        MyTimerTask mMyTimerTask = new MyTimerTask();
        mTimer.schedule(mMyTimerTask, 1000, 5 * 60000);//

        return Service.START_NOT_STICKY;
    }



    @Override
    public void onTaskRemoved(Intent rootIntent) {

        log.error("onTaskRemoved");
        try {
            MainActivity.mainActivityInstance.finish();
        } catch (Exception ex) {

        }


        stopSelf();
    }

    private synchronized void updateSender() {
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    SenderToServer.sender(mSettings, (MyApplication) getApplication());

                }
            }).start();
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onDestroy() {
        mTimer.cancel();
        super.onDestroy();
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            updateSender();
        }
    }
}
