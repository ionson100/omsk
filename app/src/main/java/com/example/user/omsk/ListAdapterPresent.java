package com.example.user.omsk;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.omsk.dialogs.DialogEditCountPresent;
import com.example.user.omsk.models.MPresent;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.myTextView.MyTextView;
import com.example.user.omsk.orm2.Configure;

import java.util.List;

/**
 * Created by USER on 11.04.2018.
 */

public class ListAdapterPresent extends ArrayAdapter<MPresent> {

    private Settings mSettings=Settings.core();
    @NonNull
    private final Context context;
    private final int resource;
    @NonNull
    private final List<MPresent> objects;
    private AppCompatActivity activity;

    public ListAdapterPresent(@NonNull Context context, @LayoutRes int resource, @NonNull List<MPresent> objects, AppCompatActivity activity) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View mView = convertView;
        if (mView == null) {
            mView = LayoutInflater.from(getContext()).inflate(resource, null);
        }
        MyTextView textName= (MyTextView) mView.findViewById(R.id.name_present);
        final MyTextView textAmount= (MyTextView) mView.findViewById(R.id.amount_present);
        textName.setText(objects.get(position).name);
        int i=0;
        for (MVisitPresent selectPresent : mSettings.getVisit().selectPresents) {
            if(selectPresent.id.equals(objects.get(position).id)){
                i=selectPresent.amount;
            }
        }
        textAmount.setText(String.valueOf(i));
        TextView description= (TextView) mView.findViewById(R.id.description_present);
        if(objects.get(position).description!=null){
            description.setText("Описание: "+objects.get(position).description);
        }

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogEditCountPresent f=  new DialogEditCountPresent();
                f.iActionE=new IActionE<Integer>() {
                    @Override
                    public void action(Integer o) {
                        textAmount.setText(String.valueOf(o));
                        boolean run=false;

                        for (MVisitPresent selectPresent : mSettings.getVisit().selectPresents) {
                            if(selectPresent.id.equals(objects.get(position).id)){
                                run=true;
                                selectPresent.amount=o;
                            }
                        }
                        if(run==false){
                            MVisitPresent present=new MVisitPresent();
                            present.id=objects.get(position).id;
                            present.amount=o;
                            mSettings.getVisit().selectPresents.add(present);
                            mSettings.save();
                        }
                        MVisitPresent nulls=null;
                        for (MVisitPresent selectPresent : mSettings.getVisit().selectPresents) {
                            if(selectPresent.amount==0){
                                nulls=selectPresent;
                            }
                        }
                        if(nulls!=null){
                            mSettings.getVisit().selectPresents.remove(nulls);
                            Configure.getSession().
                                    execSQL(" delete from visit_present where visit_id = ? and id = ?",
                                            mSettings.getVisit().idu,nulls.id);
                        }



                    }
                };
                for (MVisitPresent selectPresent : mSettings.getVisit().selectPresents) {
                    if(selectPresent.id.equals(objects.get(position).id)){
                        f.amount=selectPresent.amount;
                    }
                }

                f.show(activity.getSupportFragmentManager(), "fsdfasa");
            }
        });


        return mView;
    }
}
