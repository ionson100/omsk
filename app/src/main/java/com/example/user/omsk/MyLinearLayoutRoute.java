package com.example.user.omsk;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class MyLinearLayoutRoute extends LinearLayout {
    public MyLinearLayoutRoute(Context context) {
        super(context);
    }

    public MyLinearLayoutRoute(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MyLinearLayoutRoute(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setPressed(boolean pressed) {


        if (pressed) {
            this.setBackgroundResource(R.color.colore97);
        } else {
            this.setBackgroundResource(R.color.colore3);
        }
        super.setPressed(pressed);
    }

}
