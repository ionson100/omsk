package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CACH;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_DEBT;

public class PrintCheck {
    public static org.apache.log4j.Logger logE = org.apache.log4j.Logger.getLogger(PrintCheck.class);


    public static void pintCheckCore(final TextView log, final Activity activity, final Settings mSettings, final List<MProductBase> mProductList, final int type_check,
                                     final IActionE iActionE) {

        new AsyncTask<Void, String, Void>() {

            private MChecksData checksData = new MChecksData();
            private ProgressDialog dialog;
            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            private void openCheck(int type) throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }

                if (fptr.put_CheckType(type) < 0) {
                    checkError();
                }
                if (fptr.OpenCheck() < 0) {
                    checkError();
                }
            }


            private void closeCheck(int typeClose) throws Exception {
                if (fptr.put_TypeClose(typeClose) < 0) {
                    checkError();
                }
                if (fptr.CloseCheck() < 0) {
                    checkError();
                }
            }

            private void registrationFZ54(String name, double price, double quantity) throws Exception {

                if (fptr.put_TaxNumber(3) < 0) {
                    checkError();
                }
                if (fptr.put_Quantity(quantity) < 0) {
                    checkError();
                }
                if (fptr.put_Price(price) < 0) {
                    checkError();
                }
                if (fptr.put_PositionSum(quantity * price) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                    checkError();
                }
                if (fptr.put_Name(name) < 0) {
                    checkError();
                }
                if (fptr.Registration() < 0) {
                    checkError();
                }
            }

            private void reportZ() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REPORT_CLEAR) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError();
                }
                ZetReport mZetReprot = new ZetReportBuilder(fptr).build();
                MZReport m = new MZReport(mZetReprot);
                if (fptr.Report() < 0) {
                    checkError();
                }
                Configure.getSession().insert(m);
                ZetReportBuilder.clearReport();
            }

            private void printText(String text, int alignment, int wrap) throws Exception {
                if (fptr.put_Caption(text) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError();
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError();
                }
                if (fptr.PrintString() < 0) {
                    checkError();
                }
            }

            @Override
            protected Void doInBackground(Void... params) {


                fptr = new Fptr();


                try {
                    fptr.create(activity.getApplication());

                    publishProgress("Загрузка настроек...");
                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    publishProgress("Установка соединения...");
                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    publishProgress("OK");
                    publishProgress("Проверка связи...");
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    publishProgress("OK");

                    double max = 0;
                    for (MProductBase product : mProductList) {
                        max = max + product.price * product.getAmount_core();
                    }
                    if (max > mSettings.maxSummPrice) {
                        throw new Exception(activity.getString(R.string.maxprice) + "\n max price: (" + mSettings.maxSummPrice + " руб.) ");
                    }
                    // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                    // не являются ошибками, если мы хотим просто отменить чек
                    publishProgress("Отмена чека...");
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }
                    publishProgress("OK");
                    publishProgress("Фискальный чек...");
                    // Открываем чек продажи, попутно обработав превышение смены
                    publishProgress("Открытие чека...");
                    try {
                        openCheck(IFptr.CHEQUE_TYPE_SELL);
                    } catch (Exception e) {
                        // Проверка на превышение смены
                        if (fptr.get_ResultCode() == -3822) {
                            reportZ();
                            openCheck(IFptr.CHEQUE_TYPE_SELL);
                        } else {
                            throw e;
                        }
                    }
                    if (mSettings.getVisit() != null) {
                        ////////////////////////////////////////////// отправка данных емайл покупателю
                        if (mSettings.getVisit().point.email != null && mSettings.getVisit().notify == 2 && (type_check == TYPE_CHECK_CACH || type_check == TYPE_CHECK_DEBT)) {
                            if (fptr.put_FiscalPropertyNumber(1008) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyType(Fptr.FISCAL_PROPERTY_TYPE_STRING) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyValue(mSettings.getVisit().point.email) < 0) {
                                checkError();
                            }
                            if (fptr.WriteFiscalProperty() < 0) {
                                checkError();
                            }
                        }
                        if (mSettings.getVisit().point.sms != null && mSettings.getVisit().notify == 1 && (type_check == TYPE_CHECK_CACH || type_check == TYPE_CHECK_DEBT)) {
                            if (fptr.put_FiscalPropertyNumber(1008) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyType(Fptr.FISCAL_PROPERTY_TYPE_STRING) < 0) {
                                checkError();
                            }
                            if (fptr.put_FiscalPropertyValue(mSettings.getVisit().point.sms) < 0) {
                                checkError();
                            }
                            if (fptr.WriteFiscalProperty() < 0) {
                                checkError();
                            }
                        }
                    }
                    Collections.sort(mProductList, new Comparator<MProductBase>() {
                        @Override
                        public int compare(MProductBase lhs, MProductBase rhs) {
                            return lhs.name.compareTo(rhs.name);
                        }
                    });

                    double totalAmount = 0;
                    double totalPrice = 0;
                    for (MProductBase product : mProductList) {
                        publishProgress(product.nameKKT);
                        totalAmount = totalAmount + product.getAmount_core();
                        totalPrice = totalPrice + (product.getAmount_core() * product.price);
                        registrationFZ54(product.nameKKT, product.price, product.getAmount_core());
                        checksData.productList.add(product);
                    }


                    closeCheck(0);
                    publishProgress("Чек отпечатан");
                    if (mSettings.getVisit() != null) {
                        checksData.sms = mSettings.getVisit().point.sms;
                        checksData.email = mSettings.getVisit().point.email;
                        checksData.notify = mSettings.getVisit().notify;
                    }
                    checksData.type_check = type_check;
                    checksData.totalAmount = totalAmount;
                    checksData.totalPrice = totalPrice;
                    checksData.checkNumber = fptr.get_CheckNumber();
                    checksData.docNumber = fptr.get_DocNumber();
                    checksData.kmmNumber = fptr.get_SerialNumber();
                    mSettings.kkmNumber = fptr.get_SerialNumber();
                    Settings.save();
                    try {
                        //Date dd = StateKKM.getDate(fptr.get_Date(), fptr.get_Time());
                        checksData.date = Utils.curDate();//Utils.dateToInt(dd);
                    } catch (Exception ex) {
                        checksData.date = Utils.curDate();
                    }

                } catch (Exception e) {
                    logE.error(e);
                    fptr.CancelCheck();
                    publishProgress(e.getMessage());
                    checksData.errorText = e.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), mSettings.kkmNumber + "\n" +
                            "Ошибка ккм file  - PrintTestCheck\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                log.setText("");
                dialog = Utils.factoryDialog(activity, "Печать фискального чека", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                iActionE.action(checksData);
                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
            }

            @Override
            protected void onProgressUpdate(String... values) {
                if (values == null || values.length == 0) {
                    return;
                }
                logAdd(values[0]);
            }

            private void logAdd(String string) {
                String value = (String) log.getText();
                if (value.length() > 2096) {
                    value = value.substring(0, 2096);
                }
                log.setText(string + "\n" + value);
            }

        }.execute();
    }
}
