package com.example.user.omsk.linq2;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
public interface Function<D, TRes> {
    TRes foo(D t);
}
