package com.example.user.omsk.senders;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MVisitPresent;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.orm2.Configure;
import com.google.gson.Gson;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class SenderCommitDey {

    public static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderCommitDey.class);
    Activity mActivity;
    Settings mSettings;
    IActionE mIActionE;
    boolean is200;

    public void send(Activity activity,  IActionE<Boolean> actionE) {
        mActivity = activity;
        mSettings = Settings.core();
        mIActionE = actionE;
        SenderBase senderBase = new SenderBase();
        senderBase.execute();
    }


    private class SenderBase extends AsyncTask<Void, Void, String> {


        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {

            if (!Utils.isNetworkAvailable(mActivity, mSettings)) {
                cancel(true);
                Toast.makeText(mActivity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }


            dialog = Utils.factoryDialog(mActivity, mActivity.getString(R.string.close_day),null);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String aBoolean) {
            if (isCancelled()) return;
            if (dialog != null) {
                dialog.cancel();
            }
            if (aBoolean == null&&is200) {
                mIActionE.action(null);
            } else {
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/close_day/:  " + aBoolean;
                Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
                log.error(msg);
                new SenderErrorPOST().send((MyApplication) mActivity.getApplication(), msg);

            }
        }


        @Override
        protected String doInBackground(Void... params) {
            List<TempVisit> tempVisits=new ArrayList<>();
            List<MVisitStory> visitStoryList =  Configure.getSession().getList(MVisitStory.class, null);
            for (MVisitStory mVisitStory : visitStoryList) {
                try{
                    mVisitStory.gifts =Configure.getSession().getList(MVisitPresent.class," visit_id = ? ",mVisitStory.idu);
                }catch (Exception  ignored){

                }
                TempVisit tempVisit = TempVisit.getTempVisit(mVisitStory);
                tempVisits.add(tempVisit);
            }
            Gson sd = Utils.getGson();
            String json=sd.toJson(tempVisits);
            String str = null;
            try {
                str = Utils.HTTP + mSettings.url + "/close_day/?guid=" + URLEncoder.encode(SettingsUser.core().getUuidDevice(),
                        "UTF-8");
            } catch (Exception e) {
                log.error(e);
                return e.getMessage();
            }
            return UtilsSender.postRequest(mActivity.getApplication(),str,json, new IActionResponse() {
                @Override
                public void invoke(String str, int status) {
                    if(str.trim().equals("200")){
                        is200=true;
                    };
                }
            });

        }
    }
}
