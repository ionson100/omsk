package com.example.user.omsk.kassa;

import java.util.Date;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

public class ZetReport {
    // гуид планшета
    public String uuid;
    public String id;
    // дата совершения отчета
    public Date date = null;
    //приход
    public double receipt;
    //возврат прихода
    public double return_receipt;
    //денег в кассе
    public double cash;
    // выручка
    public double gain;
    // не обнуляемая сумма
    public double not_null_summ;
    // смена
    public int session;
    // номер гашения
    public int doc_number;
    // номер кассового аппарата;
    public String number_kkm;

    public int notSendToOfd;


    public Date firstDateOfd = null;

}
