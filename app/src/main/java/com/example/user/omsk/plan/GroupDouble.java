package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import java.io.Serializable;

public class GroupDouble implements Serializable {
    public String name;
    public double percent;
    public String grup_id;
}
