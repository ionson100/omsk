package com.example.user.omsk.dialogs;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.models.MStoryTaskPoint;
import com.example.user.omsk.models.MTask;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Configure;

import java.util.List;

public class DialogTaskVisit extends DialogFragment {

    private Settings mSettings;

    public DialogTaskVisit setSettings(Settings settings) {
        this.mSettings = settings;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;
        vi = LayoutInflater.from(getActivity());
        View v = vi.inflate(R.layout.dialog_task_visit_point, null);
        builder.setView(v);
        LinearLayout panel = (LinearLayout) v.findViewById(R.id.panel_task_base);
        List<MTask> mTasks = Configure.getSession().getList(MTask.class, null);

        for (final MTask mTask : mTasks) {
            View vs = LayoutInflater.from(getActivity()).inflate(R.layout.item_dialog_task_point, null);
            ((TextView) vs.findViewById(R.id.task_name)).setText(mTask.message);
            CheckBox checkBox = (CheckBox) vs.findViewById(R.id.task_checked);
            MStoryTaskPoint taskPoint = Linq.toStream(mSettings.getVisit().storyTaskPoints).firstOrDefault(new Predicate<MStoryTaskPoint>() {
                @Override
                public boolean apply(MStoryTaskPoint t) {
                    return t.task_id.equals(mTask.idu);
                }
            });
            if (taskPoint != null) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        MStoryTaskPoint p = new MStoryTaskPoint();
                        p.task_id = mTask.idu;
                        p.point_id = mSettings.getVisit().point.idu;
                        p.message = mTask.message;
                        p.visit_id = mSettings.getVisit().idu;
                        mSettings.getVisit().storyTaskPoints.add(p);
                    } else {
                        MStoryTaskPoint tp = Linq.toStream(mSettings.getVisit().storyTaskPoints).firstOrDefault(new Predicate<MStoryTaskPoint>() {
                            @Override
                            public boolean apply(MStoryTaskPoint t) {
                                return t.task_id.equals(mTask.idu);
                            }
                        });
                        if (tp != null) {
                            mSettings.getVisit().storyTaskPoints.remove(tp);
                        }
                    }
                }
            });
            panel.addView(vs);
        }

        v.findViewById(R.id.bt_task_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        v.findViewById(R.id.bt_task_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        Button deleteBt = (Button) v.findViewById(R.id.bt_task_delete);
        deleteBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSettings.getVisit().storyTaskPoints.clear();
                dismiss();
            }
        });
        return builder.create();
    }
}
