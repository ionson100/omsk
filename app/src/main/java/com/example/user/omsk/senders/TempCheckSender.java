package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TempCheckSender {

    public String sms;

    public String email;

    public int notify;

    public String uuid;

    public String idu;

    public int number_check_delete;

    public String uuid_check_delete;

    public int number_check;

    public int number_doc;

    public Date date = null;

    public String order_id;

    public int type_check;

    public String number_kkm;

    public double price_check;

    public boolean is_returned;

    public double amount_check;

    public List<TempProductSender> productList = new ArrayList<>();
}
