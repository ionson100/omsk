package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

// тип продажи
@Table("order_type")
public class MOrderType implements IUsingGuidId, Serializable {

    @PrimaryKey("id")
    public transient int id;
    @SerializedName("index")
    @Column("index1")
    public int index;
    @SerializedName("id")
    @Column("idu")
    public String idu;
    @SerializedName("name")
    @Column("name")
    public String name;

    public MOrderType() {
        this.idu = Utils.getUuid();
    }

    public String get_id() {
        return idu;
    }

    public static String get1C() {
        String dd = "none";
        List<MOrderType> mOrderType = Configure.getSession().getList(MOrderType.class, null);
        MOrderType type = Linq.toStream(mOrderType).firstOrDefault(new Predicate<MOrderType>() {
            @Override
            public boolean apply(MOrderType t) {
                return t.index == 2;
            }
        });
        if (type != null) {
            dd = type.idu;
        }
        return dd;
    }

    public static String getОтсрочка() {
        List<MOrderType> mOrderType = Configure.getSession().getList(MOrderType.class, null);
        MOrderType type = Linq.toStream(mOrderType).firstOrDefault(new Predicate<MOrderType>() {
            @Override
            public boolean apply(MOrderType t) {
                return t.index == 1;
            }
        });
        return type.idu;
    }
}
