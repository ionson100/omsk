package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import java.util.Date;

public class ValidateDateTimeServerVsClient {
    public static void validate(final Activity activity, Settings settings) {
        Date serverTime = settings.current_time_22;
        Date curTime = Utils.curDate();
        int df = (int) Math.abs(curTime.getTime() - serverTime.getTime());
        if (df <= 20) return;
        Utils.messageBox(activity.getString(R.string.warning), activity.getString(R.string.war1) +
                activity.getString(R.string.dateser) + Utils.simpleDateFormatE(serverTime) + "\n" +
                activity.getString(R.string.dsds) + Utils.simpleDateFormatE(curTime) + "\n" +
                activity.getString(R.string.fdfff), activity, new IActionE<View>() {
            @Override
            public void action(View v) {
                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
            }
        });
    }
}
