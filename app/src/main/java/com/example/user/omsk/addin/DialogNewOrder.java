package com.example.user.omsk.addin;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.omsk.R;
import com.example.user.omsk.Utils;
import com.example.user.omsk.models.MNewOrder;
import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.pupper.Pupper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ion100 on 19.09.2017.
 */

public class DialogNewOrder extends DialogFragment {


    private List<MNewOrder> mNewOrders;

    private MPoint mPoint;

    public void setActivate(MPoint point, List<MNewOrder> mNewOrders) {
        this.mNewOrders = mNewOrders;
        this.mPoint = point;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater vi;

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_new_order, null);

        v.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        TextView textViewName = (TextView) v.findViewById(R.id.point_name);
        textViewName.setText(mPoint.name);


        LinearLayout panelBase = (LinearLayout) v.findViewById(R.id.panel_base);

        List<MNewOrder> curList = new ArrayList<>();
        for (MNewOrder mm : mNewOrders) {
            if (mm.point_id.equals(mPoint.idu)) {
                curList.add(mm);
            }
        }
        Collections.sort(curList, new Comparator<MNewOrder>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(MNewOrder lhs, MNewOrder rhs) {
                return Integer.compare(lhs.date,rhs.date);
            }
        });

        for (MNewOrder mNewOrder : curList) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_new_order_item, null);
            LinearLayout panelOrderProducts = (LinearLayout) view.findViewById(R.id.panel_order_products);
            TextView textView = (TextView) view.findViewById(R.id.date_visit);
            Date d=Utils.intToDate(mNewOrder.date);
            textView.setText(getString(R.string.hjshadfads) + Utils.simpleDateFormatE(d));

            Collections.sort(mNewOrder.products, new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });
            for (MProduct product : mNewOrder.products) {
                View viewOrder = LayoutInflater.from(getActivity()).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) viewOrder.findViewById(R.id.name_s);
                TextView amountE = (TextView) viewOrder.findViewById(R.id.amount_s);
                TextView priceE = (TextView) viewOrder.findViewById(R.id.price_s);
                priceE.setVisibility(View.GONE);
                TextView itogo = (TextView) viewOrder.findViewById(R.id.itogo_s);
                itogo.setVisibility(View.GONE);
                panelOrderProducts.addView(viewOrder);
                name.setText(product.name);
                amountE.setText(String.valueOf(product.getAmount_core()));

            }
            panelBase.addView(view);
        }
        {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_new_order_item_itogo, null);


            TextView textView = (TextView) view.findViewById(R.id.date_visit);
            textView.setText(R.string.aszdasd);

            LinearLayout panelOrderProducts = (LinearLayout) view.findViewById(R.id.panel_order_products);

            Pupper order_itogo_pacheck_name = (Pupper) view.findViewById(R.id.order_itogo_pacheck_name);
            Pupper order_itogo_pacheck = (Pupper) view.findViewById(R.id.order_itogo_pacheck);

            Map<String, MProduct> map = new HashMap<>();
            for (MNewOrder mNewOrder : mNewOrders) {

                if (mNewOrder.point_id.equals(mPoint.get_id()) == false) continue;
                for (MProduct product : mNewOrder.products) {
                    if (map.containsKey(product.idu)) {
                        MProduct pp = map.get(product.idu);
                        pp.setAmountCore(pp.getAmount_core() + product.getAmount_core());
                    } else {
                        map.put(product.idu, product);
                    }
                }
            }
            List<MProduct> list = new ArrayList<>(map.values());


            Collections.sort(list, new Comparator<MProduct>() {
                @Override
                public int compare(MProduct lhs, MProduct rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });

            double itogoE = 0;

            order_itogo_pacheck_name.setPairString(getString(R.string.jjkdsjf), String.valueOf(list.size()));

            for (MProduct mProduct : list) {
                View viewOrder = LayoutInflater.from(getContext()).inflate(R.layout.item_product_story, null);
                TextView name = (TextView) viewOrder.findViewById(R.id.name_s);
                TextView amountE = (TextView) viewOrder.findViewById(R.id.amount_s);
                TextView priceE = (TextView) viewOrder.findViewById(R.id.price_s);
                priceE.setVisibility(View.GONE);
                TextView itogo = (TextView) viewOrder.findViewById(R.id.itogo_s);
                itogo.setVisibility(View.GONE);

                name.setTextColor(Color.BLACK);
                amountE.setTextColor(Color.BLACK);
                name.setText(mProduct.name);

                amountE.setText(String.valueOf(mProduct.getAmount_core()));
                panelOrderProducts.addView(viewOrder);
                itogoE = itogoE + mProduct.getAmount_core();
            }
            order_itogo_pacheck.setPairString(getString(R.string.kjksdasd), String.valueOf(itogoE));
            panelBase.addView(view);


        }


        builder.setView(v);

        return builder.create();
    }


}
