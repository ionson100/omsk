package com.example.user.omsk.senders;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.CheckAssholes;
import com.example.user.omsk.FHome;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.WorkingPromotions;
import com.example.user.omsk.auto.AutoOpenActivity;
import com.example.user.omsk.models.MTemplatePropety;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import org.json.JSONException;

import java.util.Date;
import java.util.List;

public class SenderSynchronizationPOST {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SenderSynchronizationPOST.class);
    public interface IActionPercent {
        void action(Integer percent);
    }

    private final Settings settings;
    private final MainActivity activity;
    private final boolean isDeleteAll;
    private final boolean isUserAvtorise;
    private ProgressDialog dialog;
    private Date lastDate = null;
    ;
    private SenderGetRouterGET.IActionRoute iActionRoute;

    public SenderSynchronizationPOST( Activity activity, boolean isDeleteAll, boolean isUserAvtorise) {
        this.settings = Settings.core();
        this.activity = (MainActivity) activity;
        this.isDeleteAll = isDeleteAll;
        this.isUserAvtorise = isUserAvtorise;
    }

    public void sendSynchronization(int lastDate, SenderGetRouterGET.IActionRoute iActionRoute) {
        this.lastDate = Utils.curDate();// lastDate;
        this.iActionRoute = iActionRoute;
        new SenderWorkerTack().execute(lastDate);

    }

    private String getParams(Date date) {
        return "[\"" + SettingsUser.core().getUuidDevice() + "\"," + String.valueOf(date.getTime()) + ",\"" + settings.currentVersionName + "\",\"" + "0000"+ "\"]";
    }

    @NonNull
    private String senderCore(String message, final IActionPercent iActionPercent) {

        String res = null;
        final String[] temp = {null};

        String add = "0";
        if (!isDeleteAll) {
            add = "1";
        }
        iActionPercent.action(0);


        res = UtilsSender.postRequest(activity.getApplication(), Utils.HTTP + settings.url + "/synchronization_client/?update=" + add, message, new IActionResponse() {
            @Override
            public void invoke(String str, int status) {
                try {
                    iActionPercent.action(9);
                    App(str, isDeleteAll, iActionPercent);
                } catch (JSONException e) {

                    temp[0] = e.getMessage();
                }
            }
        });
        if (temp[0] != null) {
            res = temp[0];
        }
        return res;
    }

    private void App(String res, boolean isDeleteAll, IActionPercent iActionPercent) throws JSONException {
        new innerSynchronise(settings).action(res, isDeleteAll, isUserAvtorise, iActionPercent, activity);
    }

    public class SenderWorkerTack extends AsyncTask<Integer, Integer, String> {

        @Override
        protected void onPreExecute() {

            if (!Utils.isNetworkAvailable(activity, settings)) {
                cancel(true);
                Toast.makeText(activity, R.string.avaalablenetwork, Toast.LENGTH_SHORT).show();
                return;
            }
            dialog = Utils.factoryDialog(activity, "Запрос на сервер", null);
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setIndeterminate(false);
            dialog.setProgress(0);
            dialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            dialog.setProgress(values[0]);
            if (values[0] >= 11) {
                dialog.setMessage("Обработка данных с сервера");
            }
            if (values[0] == 99) {
                dialog.setMessage("Очистка памяти");
            }
        }

        @Override
        protected void onPostExecute(String aBoolean) {

            if (isCancelled()) return;
            if (aBoolean == null) {
                Toast.makeText(activity, activity.getString(R.string.synch_ok), Toast.LENGTH_SHORT).show();
                //ValidateDateTimeServerVsClient.validate(activity, settings);
                WorkingPromotions promotions = new WorkingPromotions(settings, activity);
                promotions.start();
                CheckAssholes.check(activity);

                if (settings.useAuto && isDeleteAll && isUserAvtorise == false) {
                    activity.startActivityForResult(new Intent(activity, AutoOpenActivity.class), AutoOpenActivity.RESULT);
                }

                ISession ses;
                ses = Configure.getSession();
                List<MTemplatePropety> mTemplatePropetyList = ses.getList(MTemplatePropety.class, null);
                if (mTemplatePropetyList.size() > 0) {
                    MTemplatePropety mTemplatePropety = mTemplatePropetyList.get(mTemplatePropetyList.size() - 1);
                    mTemplatePropety.successSynchronize = true;
                    ses.update(mTemplatePropety);
                }


            } else {
                Utils.messageBox(activity.getString(R.string.error_synch1), activity.getString(R.string.error_syinch), activity, null);
                if (iActionRoute != null) {
                    iActionRoute.Action(null);
                }
                String msg = Utils.simpleDateFormatE(Utils.curDate()) + ": " + Utils.HTTP + Settings.core().url + "/synchronization_client/:  " + aBoolean;
               log.error(msg);
                new SenderErrorPOST().send((MyApplication) activity.getApplication(), msg);
            }

            if (dialog != null) {
                dialog.cancel();
            }
            Utils.getIntent(activity, MainActivity.class);// выброс на передний план
            activity.visibleFloatButton(settings.currentVersionName, settings.serverVersionName);// зажигаем лампочку если есть обновления


            try {
                List<Fragment> fHomeList = activity.getSupportFragmentManager().getFragments();
                for (int i = 0; i < fHomeList.size(); i++) {
                    Fragment fragment = fHomeList.get(i);
                    if (fragment instanceof FHome) {
                        ((FHome) fragment).visibleButtonUpdate(settings.currentVersionName, settings.serverVersionName);
                    }
                }
            } catch (Exception ignored) {

                log.error(ignored);
            }


        }

        @Override
        protected String doInBackground(Integer... params) {
            if (isCancelled()) {
                return null;
            }
            String message = getParams(lastDate);
            return senderCore(message, new IActionPercent() {
                @Override
                public void action(Integer percent) {
                    publishProgress(percent);
                }
            });
        }
    }
}

