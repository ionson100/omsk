package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;

import com.example.user.omsk.route.Route;


// запуск активности формирования маршрута
public class StartIntentRoute {
    public static void start(final Settings mSettings, final Activity activity) {
        if (ValidateDay.validate(mSettings, activity)) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SenderToServer.sender(mSettings, (MyApplication) activity.getApplication());
                        }
                    }).start();
                }
            });
            Intent intent = new Intent(activity, Route.class);
            activity.startActivity(intent);
        }
    }
}

