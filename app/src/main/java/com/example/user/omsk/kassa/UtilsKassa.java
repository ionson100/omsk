package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.models.MChecksData;
import com.example.user.omsk.models.MDebt;
import com.example.user.omsk.models.MProduct;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MVisitStory;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.linq2.Linq;
import com.example.user.omsk.linq2.Predicate;
import com.example.user.omsk.orm2.Configure;
import com.example.user.omsk.orm2.ISession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CACH;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_CONSTRUCTOR;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_DEBT;
import static com.example.user.omsk.models.MChecksData.TYPE_CHECK_RETURN;

public class UtilsKassa {

    public static void addError(Fptr fptr, List<String> stringList) {
        int rc = fptr.get_ResultCode();
        if (rc < 0) {
            String rd = fptr.get_ResultDescription(), bpd = null;
            if (rc == -6) {
                bpd = fptr.get_BadParamDescription();
            }
            if (bpd != null) {
                stringList.add(String.format("[%d] %s (%s)", rc, rd, bpd));
            } else {
                stringList.add(String.format("[%d] %s", rc, rd));
            }
        }
    }

//


    public static void closeCheck(int typeClose, Fptr fptr, List<String> stringList) {
        if (fptr.put_TypeClose(typeClose) < 0) {
            addError(fptr, stringList);
            return;
        }
        if (fptr.CloseCheck() < 0) {
            addError(fptr, stringList);
            return;
        }
    }

    public static void registrationFZ54(String name, double price, double quantity, int discountType,
                                        double discount, int taxNumber, Fptr fptr, List<String> stringList) {

        if (fptr.put_TaxNumber(3) < 0) {
            addError(fptr, stringList);
            return;
        }
        if (fptr.put_Quantity(quantity) < 0) {
            addError(fptr, stringList);
            return;
        }
        if (fptr.put_Price(price) < 0) {
            addError(fptr, stringList);
            return;
        }
        if (fptr.put_PositionSum(quantity * price) < 0) {
            addError(fptr, stringList);
            return;
        }
        if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
            addError(fptr, stringList);
            return;
        }
        if (fptr.put_Name(name) < 0) {
            addError(fptr, stringList);
            return;
        }
        if (fptr.Registration() < 0) {
            addError(fptr, stringList);
            return;
        }
    }


    public static void messageBoxStateKassa(StateKKM state, Activity activity) {

        StringBuilder sb = new StringBuilder("      АТОЛ 11Ф").append("\n\n");
        sb.append("Дата:                          ").append(Utils.simpleDateFormatE(state.date)).append("\n");
        //  sb.append("Время:                          ").append(state.time.toString()).append("\n");
        sb.append("ИНН:                           ").append(state.inn).append("\n");

        String sw = "Да";
        if (!state.batteryLow) {
            sw = "Нет";
        }

        sb.append("Требуется зарядка бат. :       ").append(sw).append("\n");
        sb.append("Номер кассы:                   ").append(state.serialNumer).append("\n");
        String s = "есть";
        if (state.checkPaperPresent == false) {
            s = "отсутствует";
        }
        sb.append("Наличие бумаги:                ").append(s).append("\n");
        s = "Отсутствует";
        if (state.fiscal) {
            s = "Присутствует";
        }
        sb.append("Фискализация:                  ").append(s).append("\n");
        sb.append("Оператор:                      ").append(state.operator).append("\n");
        sb.append("Номер чека:                    ").append(state.checkNumber).append("\n");
        sb.append("Состояние чека:                ").append(state.checkState).append("\n");
        sb.append("Не переданные в офд чеки:       ").append(state.notSenderOfd).append("\n");
        if (state.notSenderOfd > 0) {
            sb.append("Дата первого не переданного чека:  \n").append(Utils.simpleDateFormatE(state.notSenderFirstDate)).append("\n");
        }
        sb.append("Адрес ОФД:              ").append(state.ofdUrl).append("\n");
        sb.append("Порт  ОФД:              ").append(state.ofdPort).append("\n");


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
        TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
        TextView tmessage = (TextView) v.findViewById(R.id.message_core);


        Button button3 = (Button) v.findViewById(R.id.button3);
        builder.setView(v);
        final AlertDialog alert = builder.create();

        titl.setText("Состояние кассы:");
        tmessage.setText(sb.toString());

        button3.setText(activity.getString(R.string.close));
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }


    public static List<Tempik> getMProducts(MVisitStory mVisitStory) {

        List<Tempik> map = new ArrayList<>();
        MVisitStory visitStory = mVisitStory;
        for (MProduct product : visitStory.getSalesProductList()) {


            if (map.size() == 0) {
                Tempik t = new Tempik();
                t.price = product.price;
                t.product = product;
                t.amount = product.getAmount_core();
                map.add(t);
                break;
            }
            for (Tempik tempik : map) {
                if (tempik.product.idu.equals(product.idu) && tempik.price == product.price) {
                    double d = tempik.amount + product.getAmount_core();
                    map.add(tempik);
                } else {
                    Tempik t = new Tempik();
                    t.price = product.price;
                    t.product = product;
                    t.amount = product.getAmount_core();
                    map.add(t);
                    break;
                }
            }

        }

        List<MDebt> mDebtList = Configure.getSession().getList(MDebt.class, null);
        List<MProduct> mProductList = Configure.getSession().getList(MProduct.class, null);
        List<MProduct> list = new ArrayList<>();

        for (MDebt debt : mDebtList) {
            for (String s : visitStory.debtList) {
                if (debt.order_id.equals(s)) {
                    for (MProduct product : mProductList) {
                        if (product.idu.equals(debt.product_id)) {
                            product.price = debt.price;
                            product.setAmountCore(debt.amount);
                            list.add(product);
                            break;
                        }
                    }
                }
            }
        }
        for (MProduct product : list) {


            if (map.size() == 0) {
                Tempik t = new Tempik();
                t.price = product.price;
                t.product = product;
                t.amount = product.getAmount_core();
                map.add(t);
                break;
            }
            for (Tempik tempik : map) {
                if (tempik.product.idu.equals(product.idu) && tempik.price == product.price) {
                    double d = tempik.amount + product.getAmount_core();
                    map.add(tempik);
                } else {
                    Tempik t = new Tempik();
                    t.price = product.price;
                    t.product = product;
                    t.amount = product.getAmount_core();
                    map.add(t);
                    break;
                }
            }
        }

        Collections.sort(map, new Comparator<Tempik>() {
            @Override
            public int compare(Tempik lhs, Tempik rhs) {
                return lhs.product.name.compareTo(rhs.product.name);
            }
        });
        return map;
    }

    public static MChecksData getMCheckDataCache(List<MChecksData> list) {
        return Linq.toStream(list).firstOrDefault(new Predicate<MChecksData>() {
            @Override
            public boolean apply(MChecksData t) {
                return t.type_check == TYPE_CHECK_CACH && t.isReturn == false;//&& t.is_zreport == false
            }
        });
    }

    public static MChecksData getMChecksDataDebt(List<MChecksData> list, final String debt_order_id) {
        MChecksData checksData = Linq.toStream(list).firstOrDefault(new Predicate<MChecksData>() {
            @Override
            public boolean apply(MChecksData t) {
                if (t.order_debt_id == null) return false;
                return t.type_check == TYPE_CHECK_DEBT && t.order_debt_id.equals(debt_order_id) && t.isReturn == false;//&& t.is_zreport == false
            }
        });
        if (checksData == null) {
            List<MVisitStory> dList = Configure.getSession().getList(MVisitStory.class, null);
            for (MVisitStory mVisitStory : dList) {
                for (MChecksData data : mVisitStory.getCheckList()) {
                    if (data.order_debt_id == null) continue;
                    if (data.order_debt_id.equals(debt_order_id) && data.type_check == TYPE_CHECK_DEBT && data.isReturn == false) {//&& data.is_zreport == false

                        checksData = data;
                    }
                }
            }
        }
        return checksData;
    }

    public static void clearLast(Settings mSettings) {

        ISession session = Configure.getSession();
        List<MVisitStory> visitStoryList = session.getList(MVisitStory.class, null);
        for (MVisitStory mVisitStory : visitStoryList) {
            for (MChecksData checksData : mVisitStory.getCheckList()) {
                checksData.isLast = false;
            }
            if (mVisitStory.getCheckList().size() > 0) {
                session.update(mVisitStory);
            }
        }

        if (mSettings.getVisit() != null) {
            for (MChecksData checksData : mSettings.getVisit().checksDataList) {
                checksData.isLast = false;
            }
        }

        List<MChecks> mzReportList = session.getList(MChecks.class, null);
        for (MChecks mChecks : mzReportList) {
            mChecks.checkData.isLast = false;
            session.update(mChecks);
        }

    }

    public static String getStringDecimalE(double value) {
        return Utils.getStringDecimal(value).replace(',', '.');
    }

    public static void messageBoxConfiпPrintCheck(Activity activity, List<MProductBase> mProductList, int type_check, final IActionE iActionE, final IActionE cancelIActionE) {

        StringBuilder message = new StringBuilder();
        message.append(activity.getString(R.string.asdasdd));
        message.append(activity.getString(R.string.sdjhsd));

        String m = activity.getString(R.string.sadasddsd);
        if (type_check == TYPE_CHECK_CACH) {
            m = activity.getString(R.string.dsfdfsdfs);
        }

        if (type_check == TYPE_CHECK_DEBT) {
            m = activity.getString(R.string.sfdsffdf);
        }
        if (type_check == TYPE_CHECK_RETURN) {
            m = activity.getString(R.string.dasdd);
        }
        if (type_check == TYPE_CHECK_CONSTRUCTOR) {
            m = activity.getString(R.string.sdasdasddsd);
        }
        message.append(m);
        message.append("\n");

        message.append(activity.getString(R.string.posapod));

        int i = 0;
        double amount = 0;
        double itogo = 0;
        Collections.sort(mProductList, new Comparator<MProductBase>() {
            @Override
            public int compare(MProductBase lhs, MProductBase rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        for (MProductBase mProduct : mProductList) {
            if (mProduct.price == 0 || mProduct.getAmount_core() == 0) {
                Utils.messageBox(activity.getString(R.string.error), "Продукт - " + mProduct.name +
                        activity.getString(R.string.wsowie), activity, null);
                return;
            }
            int del = 7 - String.valueOf(++i).length();
            StringBuilder dl =new StringBuilder();
            for (int f = 0; f < del; f++) {
                dl.append(" ");
            }

            message.append("  ").append(i).append(".").append(dl).append(mProduct.name).append(" - ").append(mProduct.getAmount_core()).append(" ").append(mProduct.getUnit());

            message.append("\n");
            amount = amount + mProduct.getAmount_core();
            itogo = itogo + (mProduct.getAmount_core() * mProduct.price);
        }
        message.append("\n").
                append(activity.getString(R.string.itogo_amount)).append(Utils.getStringDecimal(amount)).
                append("\n").
                append(activity.getString(R.string.itogo_price2)).append(Utils.getStringDecimal(itogo)).append(activity.getString(R.string.rub)).
                append("\n");


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
        TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
        TextView tmessage = (TextView) v.findViewById(R.id.message_core);
        //Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        Button button3 = (Button) v.findViewById(R.id.button3);
        builder.setView(v);
        final AlertDialog alert = builder.create();

        titl.setText(activity.getString(R.string.confirm_check));
        tmessage.setText(message.toString());
        button2.setVisibility(View.VISIBLE);
        button2.setText(activity.getString(R.string.cancel_visit));
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cancelIActionE != null) {
                    cancelIActionE.action(null);
                }
                alert.dismiss();
            }
        });
        button3.setText(activity.getString(R.string.print_check));
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iActionE != null) {
                    iActionE.action(null);
                }
                alert.dismiss();
            }
        });

        alert.setCanceledOnTouchOutside(false);
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

                if (cancelIActionE != null) {
                    cancelIActionE.action(null);
                }
            }
        });
        alert.show();
    }

    public static void messageBoxConfiпPrintCheckPartal(Activity activity, List<AnnulCheck.TempCheck> tempChecks, IActionE actionE, final IActionE iActionE) {


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View v = LayoutInflater.from(activity).inflate(R.layout.dialog_message, null);
        TextView titl = (TextView) v.findViewById(R.id.titl_dialog);
        TextView tmessage = (TextView) v.findViewById(R.id.message_core);
       // Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        Button button3 = (Button) v.findViewById(R.id.button3);
        builder.setView(v);
        final AlertDialog alert = builder.create();

        titl.setText(activity.getString(R.string.confirm_check));
        tmessage.setText(activity.getString(R.string.asasasdasd));
        button2.setVisibility(View.VISIBLE);
        button2.setText(activity.getString(R.string.cancel_visit));
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        button3.setText(activity.getString(R.string.dewwerre));
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iActionE != null) {
                    iActionE.action(null);
                }
                alert.dismiss();
            }
        });

        alert.setCanceledOnTouchOutside(false);
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
        alert.show();
    }
}

