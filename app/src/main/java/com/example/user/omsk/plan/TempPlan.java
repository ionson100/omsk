package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.models.MPoint;
import com.example.user.omsk.R;
import com.example.user.omsk.Utils;

import java.util.HashMap;
import java.util.Map;


public class TempPlan {
    // сдловарь млл - пачек проданныч
    public final Map<String, Double> MMLMap = new HashMap<>();
    //точка
    public MPoint mPoint;
    public double mTotalRub;
    // план по акб
    public double mPlanACB;

    public double getPlanMML(String name, boolean isOld) {
        return Utils.getMmlAmount(name, isOld);
    }

    //public double mPlanMML;
    // общая сумма продажи по точке за период
    public double mAllSalePachka;
    public int mMml_group;
    public int mMedium_line;

    public double getPercentAKB() {
        double dd = 0;
        try {
            double p = mPlanACB / 100;
            if (p == 0) {
                return 0;
            }
            dd = mTotalRub / p;
        } catch (Exception ignored) {
        }
        return dd;
    }

    public int getColorMmlItems(String mmlName, double mmlPlan) {
        double dd = MMLMap.get(mmlName);
        if (mmlPlan == 0) {
            return R.color.d5caed;
        }
        if (dd >= mmlPlan) {
            return R.color.caede7;
        } else {
            if (dd == 0) {
                return R.color.d5caed;
            } else {
                return R.color.ede6ca;
            }

        }
    }

    public int getGreenItem(boolean isOld) {
        int res = 0;
        for (Map.Entry<String, Double> ss : MMLMap.entrySet()) {
            double d = ss.getValue();
            double dd = getPlanMML(ss.getKey(), isOld);
            if (dd == 0) continue;
            if (d >= dd) {
                res++;
            }
        }
        return res;
    }

    public void addMmlPlan(Map<String, Integer> map, boolean isOld) {


        for (Map.Entry<String, Double> ss : MMLMap.entrySet()) {
            double d = ss.getValue();
            double dd = getPlanMML(ss.getKey(), isOld);
            if (dd == 0) continue;
            if (d >= dd) {
                if (map.containsKey(ss.getKey().trim())) {
                    int ac = map.get(ss.getKey().trim());
                    map.put(ss.getKey().trim(), ac + 1);
                }
            }
        }

    }


    public int getColorMML(boolean isOld) {
        int i = 0;
        for (Map.Entry<String, Double> ss : MMLMap.entrySet()) {
            double d = ss.getValue();
            if (d >= getPlanMML(ss.getKey(), isOld)) {
                i++;
            }
        }

        if (mMml_group == 0 || i < mMml_group) {
            return R.color.d5caed;
        } else {
            return R.color.caede7;
        }
    }

    public int getColorABK() {
        if (getPercentAKB() >= 100) {
            return R.color.caede7;
        }
        return R.color.d5caed;
    }


}
