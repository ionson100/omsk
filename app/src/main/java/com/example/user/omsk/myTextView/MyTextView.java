package com.example.user.omsk.myTextView;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.example.user.omsk.MyApplication;

/**
 * ion100 04.12.2017.
 */

public class MyTextView extends android.support.v7.widget.AppCompatTextView {


    static int color = Color.parseColor("#213a7a");

    public MyTextView(Context context) {
        super(context);
        init();
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {

        this.setTypeface(MyApplication.getTypeface(getContext()));
        // this.setTextColor(color);

    }

}
