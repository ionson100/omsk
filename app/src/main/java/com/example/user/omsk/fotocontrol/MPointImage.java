package com.example.user.omsk.fotocontrol;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;


@Table("point_image")
public class MPointImage {
    @PrimaryKey("id")
    public int id;


    @Column("name")
    public String name;

    @Column("point_id")
    public String point_id;

    @Column("is_delet")
    public boolean is_delet;
}
