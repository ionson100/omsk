package com.example.user.omsk.setting.colorpicker.renderer;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/
import android.graphics.Canvas;

public class ColorWheelRenderOption {
	public int density;
	public float maxRadius;
	public float cSize, strokeWidth, alpha, lightness;
	public Canvas targetCanvas;
}