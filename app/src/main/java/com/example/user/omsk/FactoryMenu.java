package com.example.user.omsk;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.LinearLayout;

/**
 * ion100 on 01.12.2017.
 */

public class FactoryMenu {

    //LinearLayout basePanel;
    //private Activity activity;
    private NavigationView menu;

    public FactoryMenu(NavigationView menu, Activity activity) {

        this.menu = menu;
      //  basePanel = (LinearLayout) menu.findViewById(R.id.panel_base);
       // this.activity = activity;
    }

    public void activateHome() {
        menu.findViewById(R.id.menu22).setVisibility(View.VISIBLE);
        menu.findViewById(R.id.menu23).setVisibility(View.VISIBLE);
        menu.findViewById(R.id.part_2).setVisibility(View.INVISIBLE);
        menu.findViewById(R.id.part_3).setVisibility(View.GONE);

    }

    public void activatePoint() {

        menu.findViewById(R.id.menu22).setVisibility(View.VISIBLE);
        menu.findViewById(R.id.menu23).setVisibility(View.VISIBLE);
        menu.findViewById(R.id.part_2).setVisibility(View.VISIBLE);
        menu.findViewById(R.id.part_3).setVisibility(View.GONE);
    }

    public void activateProduct() {

        menu.findViewById(R.id.menu22).setVisibility(View.GONE);
        menu.findViewById(R.id.menu23).setVisibility(View.GONE);
        menu.findViewById(R.id.part_2).setVisibility(View.GONE);
        menu.findViewById(R.id.part_3).setVisibility(View.VISIBLE);
    }

    public LinearLayout getProdictPanelAddin() {
        return (LinearLayout) menu.findViewById(R.id.menu_product_addin);
    }

    public interface IMenuAction {
        void action(int id);
    }

    private IMenuAction iMenuAction;

    public void setMenuAction(IMenuAction menuAction) {
        this.iMenuAction = menuAction;
        activate();
    }

    private void activate() {

        menu.findViewById(R.id.menu1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu1);
                }
            }
        });
        menu.findViewById(R.id.menu2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu2);
                }
            }
        });
        menu.findViewById(R.id.menu3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu3);
                }
            }
        });
        menu.findViewById(R.id.menu3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu3);
                }
            }
        });
        menu.findViewById(R.id.menu4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu4);
                }
            }
        });
        menu.findViewById(R.id.menu5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu5);
                }
            }
        });
        menu.findViewById(R.id.menu6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu6);
                }
            }
        });
        menu.findViewById(R.id.menu7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu7);
                }
            }
        });
        menu.findViewById(R.id.menu8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu8);
                }
            }
        });
        menu.findViewById(R.id.menu9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu9);
                }
            }
        });
        menu.findViewById(R.id.menu10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu10);
                }
            }
        });
        menu.findViewById(R.id.menu11).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu11);
                }
            }
        });
        menu.findViewById(R.id.menu12).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu12);
                }
            }
        });
        menu.findViewById(R.id.menu13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu13);
                }
            }
        });
        menu.findViewById(R.id.menu14).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu14);
                }
            }
        });
        menu.findViewById(R.id.menu15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu15);
                }
            }
        });
        menu.findViewById(R.id.menu16).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu16);
                }
            }
        });
        menu.findViewById(R.id.menu17).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu17);
                }
            }
        });
        menu.findViewById(R.id.menu18).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu18);
                }
            }
        });
        menu.findViewById(R.id.menu19).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu19);
                }
            }
        });
        menu.findViewById(R.id.menu20).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu20);
                }
            }
        });
        menu.findViewById(R.id.menu21).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu21);
                }
            }
        });
        menu.findViewById(R.id.menu22).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu22);
                }
            }
        });
        menu.findViewById(R.id.menu23).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.menu23);
                }
            }
        });


        menu.findViewById(R.id.part_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.part_2);
                }
            }
        });


        menu.findViewById(R.id.part_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.part_2);
                }
            }
        });

        menu.findViewById(R.id.part_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iMenuAction != null) {
                    iMenuAction.action(R.id.part_2);
                }
            }
        });


    }

}
