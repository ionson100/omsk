package com.example.user.omsk;

import com.example.user.omsk.setting.Reanimator;

import java.io.Serializable;

/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/



public class SettingsGpsMap implements Serializable {

    public double point_latitude;

    public double point_longitude;

    public double fix_latitude;

    public double fix_longitude;

    public String point_name;

    public int stateSystem;

    public static SettingsGpsMap core() {
        return (SettingsGpsMap) Reanimator.get(SettingsGpsMap.class);
    }

    public static void save() {

        Reanimator.save(SettingsGpsMap.class);
    }
}
