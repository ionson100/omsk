package com.example.user.omsk;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.support.v4.app.FragmentActivity;

import com.example.user.omsk.models.MPoint;

import org.apache.log4j.Logger;

/**
 * Проверка что в  названии точке присутствует ее адрес
 */
public class CheckNamePoint {
    public static org.apache.log4j.Logger log = Logger.getLogger(CheckNamePoint.class);
    public static boolean check(MPoint point, FragmentActivity activity) {
        String name = point.name;
        String address = point.address;

        String[] add = address.split("\\s+");
        boolean res = false;
        for (String s : add) {
            if (s.equals(" ")) continue;
            if (name.toUpperCase().contains(s.toUpperCase())) {
                res = true;
                break;
            }
        }

        if (!res) {
            Utils.messageBox(activity.getString(R.string.error_label), activity.getString(R.string.error_name_point), activity, null);
        }

        return res;
    }
}
