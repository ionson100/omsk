package com.example.user.omsk.kassa;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.omsk.IActionE;
import com.example.user.omsk.ListAdapterStock;
import com.example.user.omsk.models.MProductBase;
import com.example.user.omsk.models.MProductConstructor;
import com.example.user.omsk.models.MProductGroup;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Transceiver;
import com.example.user.omsk.Utils;
import com.example.user.omsk.myListViewAbc.MyListViewAbc;
import com.example.user.omsk.orm2.Configure;

import java.util.ArrayList;
import java.util.List;

public class AddProductActivity extends AppCompatActivity implements ListAdapterStock.OnRowDataClick, ListAdapterStock.OnRowNavigateClick {

    public static final String KASSA = "kassa";
    private List<MProductBase> mProductList;
    private IActionE mIActionE;
    private Settings mSettings;
    private ListView mListView;
    private Transceiver mTransceiver;
    private SettingsKassa mSettingsKassa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_prodctivity);
        this.mSettings = Settings.core();
        this.mSettingsKassa = SettingsKassa.core();
        mListView = (ListView) findViewById(R.id.list_stock_e);
        findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mProductList = new ArrayList<MProductBase>(Configure.getSession().getList(MProductConstructor.class, " archive = 0  order by name"));
        initList(mProductList);
        createListABC();
        findViewById(R.id.bt_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSettingsKassa.constructorList.clear();
                mProductList = new ArrayList<MProductBase>(Configure.getSession().getList(MProductConstructor.class, " archive = 0  order by name"));
                initList(mProductList);
            }
        });

    }

    private void initList(List<MProductBase> mm) {

        ListAdapterStock listAdapterStock = new ListAdapterStock(this, R.layout.myrow_date_sale_product_constructor,
                new ArrayList<Object>(mm), new ArrayList<MProductBase>(mSettingsKassa.constructorList));
        listAdapterStock.setmIsUseKassa(true);
        listAdapterStock.setOnRowDataClick(this).setOnRowNavigateClick(this);
        mListView.setAdapter(listAdapterStock);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void createListABC() {
        MyListViewAbc listView = (MyListViewAbc) findViewById(R.id.listView_alphavit);
        listView.setIAction(new IActionE<String>() {
            @Override
            public void action(String o) {
                if (o.equals("All")) {
                    initList(mProductList);
                } else {
                    showSearchFirstChar(o);

                }
            }
        });

    }

    private void showSearchFirstChar(String s) {
        final List<MProductBase> points = new ArrayList<>();
        for (MProductBase point : mProductList) {
            if (point.name.trim().toLowerCase().startsWith(s.trim().toLowerCase())) {
                points.add(point);
            }
        }
        initList(points);
    }

    @Override
    public void onRowNavigateClick(MProductGroup mProductGroup, List<Object> objects) {
    }

    @Override
    public void onRowDataClick(final MProductBase mProduct, final View view, List<Object> objects) {

        DialogConstructorProduct dialog = new DialogConstructorProduct();
        dialog.setmProduct(mProduct, view, new IActionE() {
            @Override
            public void action(Object o) {
                TextView price = (TextView) view.findViewById(R.id.price_d);
                price.setText(Utils.getStringDecimal(mProduct.price));
                TextView amountCore = (TextView) view.findViewById(R.id.amount_d);
                amountCore.setText(Utils.getStringDecimal(mProduct.getAmount_core()));
                TextView itogo = (TextView) view.findViewById(R.id.itogo_d);
                itogo.setText(Utils.getStringDecimal(mProduct.price * mProduct.getAmount_core()));
                //CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox_d);
                CheckBox myCheck = (CheckBox) view.findViewById(R.id.checkbox_d);
                //  checkBox.setChecked(mProduct.isSelect);
                myCheck.setChecked(mProduct.isSelect);

            }
        });
        dialog.show(getSupportFragmentManager(), "sdasdii");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SettingsKassa.save();
    }
}
