package com.example.user.omsk.kassa;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.atol.drivers.fptr.Fptr;
import com.example.user.omsk.MyApplication;
import com.example.user.omsk.R;
import com.example.user.omsk.senders.SenderErrorPOST;
import com.example.user.omsk.Settings;
import com.example.user.omsk.SettingsKassa;
import com.example.user.omsk.Utils;

/**
 * ion100 on 02.10.2017.
 */

public class DialogLLCommandRequest extends DialogFragment {

    public static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DialogLLCommandRequest.class);
    public Activity activity;

    SettingsKassa settingsKassa;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        settingsKassa = SettingsKassa.core();
        activity = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_ll_command_reguest, null);
        builder.setView(mView);
        mView.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        final EditText editText = (EditText) mView.findViewById(R.id.edit_password);
        final EditText editText2 = (EditText) mView.findViewById(R.id.edit_command);


        mView.findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sq = editText.getText().toString();
                String sq2 = Settings.core().password;

                if (sq.equals(sq2)) {
                    String sq3 = editText2.getText().toString().trim();
                    if (sq3.length() > 0) {
                        click(editText2.getText().toString());
                        dismiss();
                    }

                }


            }
        });

        return builder.create();
    }

    private void click(final String command) {
        final String[] eror = {null};
        final String[] str = {""};
        new AsyncTask<Void, String, Void>() {
            private ProgressDialog dialog;
            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... params) {


                fptr = new Fptr();
                try {

                    fptr.create(activity);

                    if (fptr.put_DeviceSettings(SettingsKassa.core().settingsKassa) < 0) {
                        checkError();
                    }
                    if (fptr.put_DeviceEnabled(true) < 0) {//Установка соединения...
                        checkError();
                    }

                    if (fptr.GetStatus() < 0) {//Проверка связи
                        checkError();
                    }


                    if (fptr.put_CommandBuffer(command) < 0) {
                        checkError();
                    }
                    if (fptr.put_NeedResultFlag(true) < 0) {
                        checkError();
                    }
                    if (fptr.put_TimeoutACK(500) < 0) {
                        checkError();
                    }
                    if (fptr.put_TimeoutENQ(5000) < 0) {
                        checkError();
                    }
                    if (fptr.RunCommand() < 0) {
                        checkError();
                    }

                    str[0] = fptr.get_AnswerBuffer();
                 //   String ggg = fptr.get_CommandBuffer();
                    int ff = 34;


                } catch (Exception e) {

                    log.error(e);
                    eror[0] = e.getMessage();
                    new SenderErrorPOST().send((MyApplication) activity.getApplicationContext(), Settings.core().kkmNumber + "\n" +
                            "Ошибка ккм file  - DialogCommand\n" + e.getMessage());
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {

                dialog = Utils.factoryDialog(activity, "Command", null);
                dialog.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                if (dialog != null) {
                    dialog.cancel();
                    dialog = null;
                }
                if (eror[0] != null) {
                    Utils.messageBox(getString(R.string.error), "Command: " + command + "\n" + eror[0], activity, null);
                } else {
                    Utils.messageBox(getString(R.string.asssssas), str[0], activity, null);
                }
            }
        }.execute();
    }

}

