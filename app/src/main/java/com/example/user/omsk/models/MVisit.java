package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/


import com.example.user.omsk.SettingsUser;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Configure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.example.user.omsk.orm2.Configure.getSession;

// контейнер для данных визита что хранится в настройках приложения
public class MVisit implements Serializable {

    static final String not="Результат не определен.";
    public List<MChecksData> checksDataList = new ArrayList<>();
    public int stateLocation;
    public int distance;

    public boolean isAss;
    public int id;
    public String idu;
    public List<String> listVideoPath = new ArrayList<>();
    public double refund;
    public double latitude;
    public double payment = -1;
    public double longitude;
    public String visit_result_id;
    public String comment;
    public boolean secondary;
    public MPoint point;
    public String order_type_id;

    public List<MProduct> selectProductForSale = new ArrayList<>();
    public List<MProduct> selectProductForBalance = new ArrayList<>();
    public List<MProduct> selectProductForPresent = new ArrayList<>();
    public List<MProduct> selectProductForActions = new ArrayList<>();

    public List<MVisitPresent> selectPresents=new ArrayList<>();


    public List<String> debtList = new ArrayList<>();
    public List<MStoryTaskPoint> storyTaskPoints = new ArrayList<>();
    public List<String> listPhotoPath = new ArrayList<>();
    private int start;
    private int finish;
    public int scroll_balance_list;
    public int scroll_sale_list;
    public String member_action;
    public String concurent_actions;
    public int pressure;
    // только в рамках одного визита, после нажатия кнопки печать чистые продажи чтобы скрыть панеоль отпрaвки
    public boolean isNotSender;
    // показывает первый лив изит или нет
    public boolean isFirstVisit;
    public int notify;

    public List<MProduct> newOrderList = new ArrayList<>();

    public MVisit() {
        isFirstVisit = true;
        idu = Utils.getUuid();
    }

    public int getStart() {
        return start;
    }

    public void setStart(int value) {

      //  Date s=Utils.intToDate(value);
        if(this.start==0){
            this.start = value;
        }
    }

    public int getFinish() {
        return finish;
    }

    public void setFinish(int value) {
        if(this.finish==0){
            this.finish = value;
        }
    }

    public String getResultatVisit() {

        MVisitResults df = getSession().get(MVisitResults.class, visit_result_id);
        if (df != null) {
            return df.name;
        } else {
            return not ;
        }
    }

    public String getOrderType() {
        MOrderType mOrderType = getSession().get(MOrderType.class, order_type_id);
        if (mOrderType != null) {
            return mOrderType.name;
        } else {
            return not;
        }
    }

    public MOrderType getOrderTypeCore() {
        return getSession().get(MOrderType.class, order_type_id);
    }

    public MVisitResults getResultatVisitCore() {
        MVisitResults results = Configure.getSession().get(MVisitResults.class, visit_result_id);
        if (results == null) {
            results = new MVisitResults();
            results.name = not;
        }
        return results;
    }


    public double getSummTotal() {
        double total = 0;
        for (MProduct mProduct : this.selectProductForSale) {
            total = total + mProduct.getAmount_core() * mProduct.price;
        }
        return total;
    }

    public MVisitStory getVisitStory() {

        MVisitStory mVisitStory = new MVisitStory();


        mVisitStory.setNewOrderProductCore(this.newOrderList);
        mVisitStory.id = this.id;
        mVisitStory.idu = this.idu;
        mVisitStory.gifts=this.selectPresents;
        mVisitStory.notify = this.notify;
        mVisitStory.pressure = this.pressure;
        mVisitStory.distance = this.distance;
        mVisitStory.stateLocation = this.stateLocation;
        mVisitStory.member_action = this.member_action;
        mVisitStory.concurent_actions = this.concurent_actions;
        mVisitStory.isRecomendationBalance = this.point.isRecomendationBalance;
        mVisitStory.isRecomendationSale = this.point.isRecomendationSale;
        mVisitStory.setNewObject(true);
        mVisitStory.order_type_id = this.order_type_id;
        mVisitStory.finish = this.getFinish();
        mVisitStory.start = this.getStart();
        mVisitStory.uuid = SettingsUser.core().getUuidDevice();
        mVisitStory.debtList = this.debtList;
        mVisitStory.point_id = this.point.get_id();
        if (this.point.getContragent(Configure.getSession()) == null) {
            mVisitStory.connterpirty_id = "";
        } else {
            mVisitStory.connterpirty_id = this.point.getContragent(Configure.getSession()).get_id();
        }
        mVisitStory.latitude = this.latitude;
        mVisitStory.longitude = this.longitude;
        mVisitStory.visit_result_id = this.visit_result_id;
        mVisitStory.setCheck(this.checksDataList);
        mVisitStory.comment = this.comment;
        mVisitStory.payment = this.payment;
        mVisitStory.refund = this.refund;
//        if (this.isAss) {
//            mVisitStory.finish = Utils.dateToInt(new Date());
//        }
        mVisitStory.date_pessimise = Utils.curDate().getTime();
        ///////////////////////////////////////
        mVisitStory.setBalanceProductCore(this.selectProductForBalance);
        mVisitStory.setSalecProductCore(this.selectProductForSale);
        mVisitStory.setFreeProductCore(this.selectProductForPresent);
        mVisitStory.setActionProductCore(this.selectProductForActions);
        return mVisitStory;
    }
}
