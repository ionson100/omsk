package com.example.user.omsk.plan;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.ISession;
import com.example.user.omsk.orm2.Table;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Table("plan")
public class PlanE extends PlaneBase implements Serializable {
    public void insertMML(ISession ses) {
        for (Map.Entry<String, List<String>> ss : mml.entrySet()) {
            for (String s1 : ss.getValue()) {
                PlanMML planMML = new PlanMML();
                planMML.mml = ss.getKey();
                planMML.product_id = s1;
                ses.insert(planMML);
            }
        }
    }
}
