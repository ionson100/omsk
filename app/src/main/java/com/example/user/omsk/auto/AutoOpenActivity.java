package com.example.user.omsk.auto;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import android.app.Activity;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.omsk.MyApplication;
import com.example.user.omsk.DaData.ui.AutoCompleteActivity;
import com.example.user.omsk.IActionE;
import com.example.user.omsk.MainActivity;
import com.example.user.omsk.R;
import com.example.user.omsk.Settings;
import com.example.user.omsk.Utils;
import com.example.user.omsk.orientationimage.RotateImage;
import com.example.user.omsk.orm2.Configure;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

//import com.example.user.omsk.models.MError;

public class AutoOpenActivity extends AppCompatActivity {

    public static org.apache.log4j.Logger log = Logger.getLogger(AutoOpenActivity.class);
    private static final int CAMERA_REQUEST_OPEN_AUTO = 29478;

    public static String getPhotoDirectoryTempAuto() {

        String patch = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/tempauto";
        File file = new File(patch);
        if (file.exists() == false) {
            file.mkdir();
        }

        return patch;
    }

    public static String getPhotoDirectoryAuto() {

        String patch = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/auto";
        File file = new File(patch);
        if (file.exists() == false) {
            file.mkdir();
        }

        return patch;
    }


    private EditText editTextKM;
    public static int RESULT = 1200;
    private Settings mSettings;
    private MAuto mAuto;
    private EditText editTextAddress;
    private Spinner spinner;
    private LinearLayout panelPhoto;
    Button buttonFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        try {
            setContentView(R.layout.activity_auto_open);
            mSettings = Settings.core();
            final List<MAuto> mAutos = Configure.getSession().getList(MAuto.class, " uid = ?", mSettings.uid_auto);
            if (mAutos.size() == 0) {
                setResult(Activity.RESULT_CANCELED);
                Toast.makeText(this, "Не найдены данные для путевого листа!", Toast.LENGTH_SHORT).show();
                finish();
            }
            mAuto = mAutos.get(0);
            final TextView textView = (TextView) findViewById(R.id.last_km_label);

            spinner = (Spinner) findViewById(R.id.gos_numbers);
            editTextAddress = (EditText) findViewById(R.id.address);
            editTextKM = (EditText) findViewById(R.id.cur_km);
            panelPhoto = (LinearLayout) findViewById(R.id.panel_photo);


            /////////////////////////////////////
            List<String> data = new ArrayList<>();


            for (AutoNummber autoNummber : mAuto.autoNummbers) {
                data.add(autoNummber.number);
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


            spinner.setAdapter(adapter);
            // заголовок


            int start = 0;
            for (int i = 0; i < mAuto.autoNummbers.size(); i++) {
                if (mAuto.autoNummbers.get(i).selfi) {
                    start = i;
                }
            }

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                    mAuto.lastKm = mAuto.autoNummbers.get(position).km;
                    textView.setText("Последний километраж: " + String.valueOf(mAuto.lastKm));
                    mAuto.number = (String) spinner.getSelectedItem();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });


            spinner.setSelection(start);


            findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            });

            findViewById(R.id.bt_save).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClick(View v) {
                    save();

                }
            });

            findViewById(R.id.bt_address_gpg).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!Utils.isLocationServiceEnabled(AutoOpenActivity.this)) {
                        Utils.messageBox(getString(R.string.error), getString(R.string.kahjadas), AutoOpenActivity.this, new IActionE() {
                            @Override
                            public void action(Object o) {
                                Intent viewIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                AutoOpenActivity.this.startActivity(viewIntent);
                            }
                        });
                        return;
                    }

                    DialogDetermineGpsUser gpsUser = new DialogDetermineGpsUser();
                    gpsUser.iActionE = new IActionE<String>() {
                        @Override
                        public void action(String o) {
                            editTextAddress.setText(o);
                        }
                    };
                    gpsUser.show(getSupportFragmentManager(), "askdjuaisdu");
                }
            });

            buttonFoto = (Button) findViewById(R.id.bt_photo);
            buttonFoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String patch = getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(1).jpg";
                    Uri outputFileUri = Uri.fromFile(new File(patch));
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.putExtra("photo", true);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST_OPEN_AUTO);

                }
            });


            findViewById(R.id.bt_address_kladr).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivityForResult(new Intent(AutoOpenActivity.this, AutoCompleteActivity.class), 1962);
                }
            });

            editTextKM.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        mAuto.km2 = Integer.parseInt(s.toString().trim());
                    } catch (Exception ex) {
                        log.error(ex);
                        editTextKM.setError(ex.getMessage());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            findViewById(R.id.button_cancelling_list).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.messageBox(getString(R.string.warning), getString(R.string.xzxx), AutoOpenActivity.this, new IActionE() {
                        @Override
                        public void action(Object o) {
                            cancelateAutoList();
                        }
                    });
                }
            });


            showPhoto();
        } catch (SQLException ex) {
            String msg = Utils.getErrorTrace(ex);
            Utils.sendMessage("open auto:" + msg,  (MyApplication) getApplication());
            log.error(ex);
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    void cancelateAutoList() {

        if (mSettings.uid_auto != null) {
            List<MAuto> autoList = Configure.getSession().getList(MAuto.class, " uid = ?", mSettings.uid_auto);
            for (MAuto auto : autoList) {
                Configure.getSession().delete(auto);
            }
        }

        mSettings.uid_auto = "";
        mSettings.useAuto = false;
        Settings.save();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_OPEN_AUTO && resultCode == Activity.RESULT_OK) {

            showPhoto();
        }
        if (requestCode == 1962 && resultCode == Activity.RESULT_OK) {

            String s = data.getStringExtra("address");
            editTextAddress.setText(s);
        }
    }

    void showPhoto() {
        panelPhoto.removeAllViews();
//        String patch = getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(1).jpg";
        final File f = new File(getPhotoDirectoryTempAuto());
        File[] files = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().indexOf(mAuto.uid + "(1).jpg") != -1;
            }
        });
        if(files==null){
            return;
        }
        for (final File file : files) {
            if (!file.exists()) continue;

            final ImageView view = (ImageView) LayoutInflater.from(this).inflate(R.layout.image_view_present, null);

            Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Utils.IMAGE_WIDTH, Utils.IMAGE_HEIGHT);
            Bitmap res = RotateImage.rotationImage(bitmap, file.getAbsolutePath());
            if (res == null) {
                res = bitmap;
            }
            view.setImageBitmap(res);
            view.setTag(file.getPath());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String str = (String) view.getTag();
                    File file = new File(str);
                    if (file.exists() == false) {

                        Toast.makeText(AutoOpenActivity.this, "Файла не существует", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        String as = "file://" + "/" + str;
                        intent.setDataAndType(Uri.parse(as), "image/*");
                        AutoOpenActivity.this.startActivity(intent);
                    }
                }
            });


            view.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

                    menu.add(R.string.delete_photo).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            final String str = (String) view.getTag();
                            final File file = new File(str);
                            if (file.exists()) {

                                if (file.delete()) {
                                    showPhoto();
                                }
                            }
                            return true;
                        }
                    });

                }
            });

            panelPhoto.addView(view);
        }
        if (panelPhoto.getChildCount() == 0) {
            buttonFoto.setVisibility(View.VISIBLE);
        } else {
            buttonFoto.setVisibility(View.GONE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void save() {

        if (spinner.getSelectedItemPosition() == 0) {
            Utils.messageBox(getString(R.string.error), getString(R.string.dsasdasdd), AutoOpenActivity.this, null);
            return;
        }

        if (editTextKM.getText().toString().trim().length() == 0) {
            editTextKM.setError(getString(R.string.saasfasf));
            Utils.messageBox(getString(R.string.error), getString(R.string.saasfasf), AutoOpenActivity.this, null);
            return;
        }

        if (editTextAddress.getText().toString().trim().length() == 0) {
            editTextAddress.setError(getString(R.string.dasdasddsd));
            Utils.messageBox(getString(R.string.error), getString(R.string.dasdasddsd), AutoOpenActivity.this, null);
            return;
        }

        if (panelPhoto.getChildCount() != 1) {
            Utils.messageBox(getString(R.string.error), getString(R.string.wqeqweqwe), this, null);
            return;
        }
        try {
            mAuto.km1 = Integer.parseInt(editTextKM.getText().toString().trim());
        } catch (Exception ex) {
            log.error(ex);
            editTextKM.setError(ex.getMessage());
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            mAuto.km1 = 0;
            return;
        }


        if (mAuto.km1 < mAuto.lastKm) {
            Utils.messageBox(getString(R.string.error), getString(R.string.asdasdasd), this, null);
            return;
        }


        mAuto.date1 = Utils.curDate();
        mAuto.foto1 = getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(1).jpg";

        mAuto.address1 = editTextAddress.getText().toString().trim();
        mAuto.isSender = true;
        mAuto.number = (String) spinner.getSelectedItem();


        try {

            Utils.copyDirectoryOneLocationToAnotherLocation(
                    new File(getPhotoDirectoryTempAuto() + "/" + mAuto.uid + "(1).jpg"),
                    new File(getPhotoDirectoryAuto() + "/" + mAuto.uid + "(1).jpg"));
            Configure.getSession().update(mAuto);

            Settings.save();
            sendBroadcast(new Intent(MainActivity.FAB));

            setResult(Activity.RESULT_OK);
            finish();
        } catch (Exception ex) {

            log.error(ex);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
