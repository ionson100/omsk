package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.Utils;
import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.IUsingGuidId;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// хранилище типов цен по каждому продукту
@Table("price")
public class MPrice implements IUsingGuidId, Serializable {

    @PrimaryKey("id")
    public transient int id;
    @SerializedName("id")
    @Column("idu")
    public String idu;
    @SerializedName("price_type_id")
    @Column("price_type_id")
    public String price_type_id;
    @SerializedName("product_id")
    @Column("product_id")
    public String product_id;
    @SerializedName("value")
    @Column("value")
    public double value;

    public MPrice() {
        this.idu = Utils.getUuid();
    }

    public String get_id() {
        return idu;
    }
}
