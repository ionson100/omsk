package com.example.user.omsk.models;
/********************************************************************
 * Copyright © 2016-2017 OOO Bitnic                                 *
 * Created by OOO Bitnic on 08.02.16   corp@bitnic.ru               *
 * ******************************************************************/

import com.example.user.omsk.orm2.Column;
import com.example.user.omsk.orm2.PrimaryKey;
import com.example.user.omsk.orm2.Table;

import java.util.Date;

@Table("start_route")
public class MTemplatePropety {

    @PrimaryKey("id")
    public int id;

    // время открытия маршрута.
    @Column("date")
    public Date date = null;

    // символизирует что данные плана на период торгового дня получены
    @Column("send_plan")
    public boolean isSendPlan;

    // символизирует что данные плана ( прошлый месяц) на период торгового дня получены
    @Column("send_plan_old")
    public boolean isSendPlanOld;

    // символизирует что данные синхронизированы удачно и приложение не закрывалось ни пользователем ни системой
    @Column("synchronize")
    public boolean successSynchronize;
}
